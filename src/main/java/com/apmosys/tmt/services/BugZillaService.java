package com.apmosys.tmt.services;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.apmosys.tmt.dao.ProjectDao;
import com.apmosys.tmt.models.BugDetailsEntity;
import com.apmosys.tmt.models.BugTrackorToolEntity;
import com.apmosys.tmt.models.MannualTestCaseExecutionLogs;
import com.apmosys.tmt.models.ProjectDeatilsMeta;
import com.apmosys.tmt.repository.BugTrackerRepository;
import com.apmosys.tmt.utils.ServiceResponse;
import com.google.gson.Gson;


@Service
public class BugZillaService {
	@Autowired
	private BugTrackerRepository bugTrackerRepository;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	ProjectDao projectDao;

	public BugTrackorToolEntity getBugToolDetails(String toolName) {
		BugTrackorToolEntity bugTrackorTool = bugTrackerRepository.findByBugTrackingToolName(toolName).get(0);
		return bugTrackorTool;
	}
	
	public ProjectDeatilsMeta getBugToolDetails(Integer pID) {
		ProjectDeatilsMeta projectDeatils=new ProjectDeatilsMeta();
		try {
			projectDeatils = projectDao.getProjectDetails(pID);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return projectDeatils;
	}

	public ResponseEntity<Object> postCall(String url, JSONObject requsetBody, Integer pID) {
		//System.out.println(requsetBody.toString());
		ResponseEntity<Object> response = null;
		ProjectDeatilsMeta bugTrackorTool = getBugToolDetails(pID);
		if (bugTrackorTool != null) {
			//url = "http://192.168.0.70" + url;
			url = bugTrackorTool.getBugToolURL() + url;
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(requsetBody.toString(), headers);
			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
					.queryParam("Bugzilla_login", bugTrackorTool.getBugTool())
					.queryParam("Bugzilla_password", bugTrackorTool.getBugToolPassword());

			////System.out.println(uriBuilder.toUriString());
			////System.out.println(requsetBody.toString());

			response = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.POST, entity, Object.class);

		}
		return response;
	}

	public ResponseEntity<Object> postCallMultipart(String url, JSONObject requsetBody, Integer pID) throws IOException {
		//System.out.println(requsetBody.toString());
		ResponseEntity<Object> response = null;
		ProjectDeatilsMeta bugTrackorTool = getBugToolDetails(pID);
		if (bugTrackorTool != null) {
			url = bugTrackorTool.getBugToolURL() + url;
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.IMAGE_JPEG);
			HttpEntity<String> entity = new HttpEntity<String>(requsetBody.toString(), headers);
			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
					.queryParam("Bugzilla_login", bugTrackorTool.getBugTool())
					.queryParam("Bugzilla_password", bugTrackorTool.getBugToolPassword());

			//System.out.println(uriBuilder.toUriString());
			//System.out.println(requsetBody.toString());

			response = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.POST, entity, Object.class);
			//response=(ResponseEntity<Object>) restTemplate.postForObject(url, requestEntity, Object.class);
			//System.out.println("attachment for bugzilla........."+response);
		}
		return response;
	}

	public ResponseEntity<Object> putCall(String url, JSONObject requsetBody, Integer pID) {
		//System.out.println(requsetBody.toString());
		ResponseEntity<Object> response = null;
		ProjectDeatilsMeta bugTrackorTool = getBugToolDetails(pID);
		if (bugTrackorTool != null) {
			url = bugTrackorTool.getBugToolURL() + url;
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(requsetBody.toString(), headers);
			//System.out.println(requsetBody.toString());
			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
					.queryParam("Bugzilla_login", bugTrackorTool.getBugTool())
					.queryParam("Bugzilla_password", bugTrackorTool.getBugToolPassword());

			//System.out.println(uriBuilder.toUriString());
			//System.out.println(requsetBody.toString());

			response = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.PUT, entity, Object.class);
			//System.out.println(response.getBody());
		}
		return response;
	}

	public ResponseEntity<Object> getCall(String url, Integer pID) {
		ResponseEntity<Object> response = null;
		ProjectDeatilsMeta bugTrackorTool = getBugToolDetails(pID);
		if (bugTrackorTool != null) {
			url = bugTrackorTool.getBugToolURL() + url;
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(headers);
			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
					.queryParam("Bugzilla_login", bugTrackorTool.getBugTool())
					.queryParam("Bugzilla_password", bugTrackorTool.getBugToolPassword());
			response = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, entity, Object.class);
		}
		return response;
	}
	
	
	public ResponseEntity<Object> getProductDetails(String product, Integer pId) {
		String url = "/rest.cgi/product";
		ResponseEntity<Object> response = null;
		ProjectDeatilsMeta bugTrackorTool = getBugToolDetails(pId);
		if (bugTrackorTool != null) {
			url = bugTrackorTool.getBugToolURL() + url;
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(headers);
			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url).queryParam("names", product)
					.queryParam("Bugzilla_login", bugTrackorTool.getBugTool())
					.queryParam("Bugzilla_password", bugTrackorTool.getBugToolPassword());
			response = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, entity, Object.class);
			//System.out.println(uriBuilder.toString());
			//System.out.println(response.getBody());
		}
		return response;
	}

	/*
	 * public Integer createProject() { Integer pId = null; String url =
	 * "/rest/product"; JSONObject request = new JSONObject(); request.put("name",
	 * "TMT"); request.put("description", "TMT"); request.put("classification",
	 * "Unclassified"); request.put("is_open", true); request.put("has_unconfirmed",
	 * false); request.put("version", "unspecified"); ResponseEntity<Object>
	 * response = postCall(url, request); if (response != null) { //
	 * pId=response.getBody() } return pId; }
	 */

	/*
	 * public Integer createComponent() { Integer pId = null; String url =
	 * "/rest/component"; JSONObject request = new JSONObject();
	 * request.put("product", "TMT"); request.put("name", "ManagerPortal");
	 * request.put("description", "This is a new component");
	 * request.put("default_assignee", "admin@apmosys.in"); ResponseEntity<Object>
	 * response = postCall(url, request); if (response != null) { //
	 * pId=response.getBody() } return pId; }
	 */

	public ServiceResponse getAllBugs(String projectName, Integer pID){
		ServiceResponse sr=new ServiceResponse();
		String url = "/rest.cgi/bug";
		ResponseEntity<Object> response = getCall(url, pID);
		if (response != null && response.getStatusCode().toString().equals("200")) {
			try {
				Gson gson = new Gson();
				String json = gson.toJson(response.getBody());
				JSONParser parser = new JSONParser();
				Object obj = parser.parse(json);
				org.json.simple.JSONObject topObject = (org.json.simple.JSONObject) obj;
				//System.out.println(topObject.toString());
				// pId=Integer.parseInt(topObject.get("id").toString());
				//pId = topObject.get("id").toString();
				sr.setServiceResponse(topObject);
			} 
			catch (Exception e) {
				e.printStackTrace();
				sr.setServiceError("failed");
			}
		}
		return sr;
	}
	
	public String createBug(MannualTestCaseExecutionLogs exLogs) {
		//BugZillaService bs = new BugZillaService();
		//bs.getAllBugs();
		String pId = null;
		String url = "/rest.cgi/bug";
		JSONObject request = new JSONObject();
		request.put("product", exLogs.getProductName());
		request.put("component", exLogs.getComponentName());
		request.put("version", "unspecified");
		request.put("summary", exLogs.getBugSummary());
		String des = "Description: " + exLogs.getTestDescription() + "\n\nBug Summary: " + exLogs.getActualResult()
				+ "\n\nExpected Result: " + exLogs.getExpectedResult();
		request.put("description", exLogs.getBugDescription());
		request.put("op_sys", "All");
		request.put("rep_platform", "All");
		//request.put("status", "open");
		request.put("severity", exLogs.getBugSeverity());
		request.put("priority", exLogs.getBugPriority());
		ResponseEntity<Object> response = postCall(url, request, exLogs.getProjectId());
		if (response != null && response.getStatusCode().toString().equals("200")) {
			try {
				Gson gson = new Gson();
				String json = gson.toJson(response.getBody());
				JSONParser parser = new JSONParser();
				Object obj = parser.parse(json);
				org.json.simple.JSONObject topObject = (org.json.simple.JSONObject) obj;
				//System.out.println(topObject.toString());
				// pId=Integer.parseInt(topObject.get("id").toString());
				pId = topObject.get("id").toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return pId;
	}

	public String updateBug(MannualTestCaseExecutionLogs exLogs) {
		String pId = null;
		String url = "/rest.cgi/bug/" + exLogs.getBugId();
		//System.out.println(url);
		/*
		 * GIVES NULL POINTER EXCEPTION...
		 * Change By :- animesh
		 * chreating another if condition 
		 * 
		 */
		//---------------------------trial start
		if(exLogs.getBugStatus()==null) {
			JSONObject request = new JSONObject();
			JSONObject requestComment = new JSONObject();
			requestComment.put("body", exLogs.getBugDescription());
			request.put("comment", requestComment);
			request.put("severity", exLogs.getBugSeverity());
		    request.put("priority", exLogs.getBugPriority());
			ResponseEntity<Object> response = putCall(url, request, exLogs.getProjectId());
			if (response != null && response.getStatusCode().toString().equals("200")) {
				try {
					Gson gson = new Gson();
					String json = gson.toJson(response.getBody());
					JSONObject obj = new JSONObject(json);
					Integer pid = obj.getJSONArray("bugs").getJSONObject(0).getInt("id");
					pId = pid.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (response != null && response.getStatusCode().toString().equals("400")) {
				pId = "0";
			}
		
		}//if
		//---------------------------trial end
		
		else if((!exLogs.getBugStatus().equals(""))||(exLogs.getBugStatus()!=null)) {
			
			JSONObject request = new JSONObject();
			/*
			 * request.put("summary", exLogs.getActualResult()); String des =
			 * "Description: " + exLogs.getTestDescription() + "\n\nBug Summary: " +
			 * exLogs.getActualResult() + "\n\nExpected Result: " +
			 * exLogs.getExpectedResult();
			 */
			
			if(exLogs.getBugStatus().equals("Reopen")) {
				request.put("status", "REOPENED");
			}
			else if(exLogs.getBugStatus().equals("Closed")) {
				request.put("status", "VERIFIED");
				request.put("resolution", "FIXED");
			}
			JSONObject requestComment = new JSONObject();
			requestComment.put("body", exLogs.getBugDescription());
			request.put("comment", requestComment);

			request.put("severity", exLogs.getBugSeverity());
		    request.put("priority", exLogs.getBugPriority());
			ResponseEntity<Object> response = putCall(url, request, exLogs.getProjectId());
			if (response != null && response.getStatusCode().toString().equals("200")) {
				try {
					Gson gson = new Gson();
					String json = gson.toJson(response.getBody());
					JSONObject obj = new JSONObject(json);
					Integer pid = obj.getJSONArray("bugs").getJSONObject(0).getInt("id");
					pId = pid.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (response != null && response.getStatusCode().toString().equals("400")) {
				pId = "0";
			}
		}
		return pId;
	}
	
	public List<String> fetchBugComment(Integer bugID, Integer pID) {

		String pId = null;
		String url = "/rest.cgi/bug/" + bugID + "/comment";
		//System.out.println(url);
		List<String> list = new ArrayList<String>();
		ResponseEntity<Object> response = getCall(url,pID);
		if (response != null && response.getStatusCode().toString().equals("200")) {
			try {
				Gson gson = new Gson();
				String json = gson.toJson(response.getBody());
				JSONObject obj = new JSONObject(json);
				JSONObject obj2 = obj.getJSONObject("bugs");
				JSONObject obj3 = obj2.getJSONObject(bugID.toString());
				org.json.JSONArray obj4 = obj3.getJSONArray("comments");
				
				/*
				 * for (int i=0; i<obj4.length(); i++) { list.add( obj4.getString(i) ); }
				 */
				for (int i = 0; i < obj4.length(); i++) {
					list.add(obj4.getJSONObject(i).getString("text"));
		            ////System.out.println(post_id);
		        }
				pId = obj4.toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (response != null && response.getStatusCode().toString().equals("400")) {
			pId = "0";
		}
		return list;
	}
	
	/*
	 * public String reopenBug(Integer bugID) {
	 * 
	 * String pId = null; String url = "/rest/bug/" + bugID;
	 * //System.out.println(url); JSONObject request = new JSONObject();
	 * request.put("status", "CONFIRMED"); ResponseEntity<Object> response =
	 * putCall(url, request); if (response != null &&
	 * response.getStatusCode().toString().equals("200")) { try { Gson gson = new
	 * Gson(); String json = gson.toJson(response.getBody()); JSONObject obj = new
	 * JSONObject(json); Integer pid =
	 * obj.getJSONArray("bugs").getJSONObject(0).getInt("id"); pId = pid.toString();
	 * } catch (Exception e) { e.printStackTrace(); } } else if (response != null &&
	 * response.getStatusCode().toString().equals("400")) { pId = "0"; } return pId;
	 * }
	 */
	
	   public static String encodeToString(BufferedImage image, String type) {
	        String imageString = null;
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();

	        try {
	            ImageIO.write(image, type, bos);
	           byte[] imageBytes = bos.toByteArray();

//            BASE64Encoder encoder = new BASE64Encoder();
//	           imageString = encoder.encode(imageBytes);

	           byte[] encodedBytes = Base64.getEncoder().encode(imageBytes);
	           imageString =  new String(encodedBytes);

	            bos.close();
	        } 
	        catch (IOException e) {
	            e.printStackTrace();
	        }
	        return imageString;
	    }
		public String saveAttachmentToBugApi(String filepath, String bugId, Integer pID) throws URISyntaxException, IOException
		{
			String pId = null;
			String url = "/rest.cgi/bug/" + bugId + "/attachment";
			File file = null;
			try
			{
				file=new File(filepath);
				
				//convert file image to base64 string		
				BufferedImage img = ImageIO.read(new File(filepath));
				String imgstr;
		       if(file.getName().endsWith(".png")) 
		    	   imgstr = encodeToString(img, "png");
		       else				
		       imgstr = encodeToString(img, "jpeg");
		        
				JSONObject request = new JSONObject();
				request.put("ids", Integer.valueOf(bugId));
				if(file.getName().endsWith(".png")) 
				request.put("content_type", "image/png");
				else
				request.put("content_type", "image/jpeg");
				//request.put("is_patch", true);
				request.put("data", imgstr);
				request.put("file_name", file.getName());
				request.put("summary", file.getName());
				request.put("comment", "This is a new attachment comment");
				ResponseEntity<Object> response = postCallMultipart(url, request, pID);
				if (response != null && response.getStatusCode().toString().equals("201")) 
				{
					try 
					{
					/*  //shallow code , caller dosent need
						Gson gson = new Gson();
						String json = gson.toJson(response.getBody());
						JSONObject obj = new JSONObject(json);
						Integer pid = obj.getJSONArray("bugs").getJSONObject(0).getInt("id");
//						Integer pid = obj.getJSONArray("ids").getJSONObject(0).getInt("id");
						pId = pid.toString();
					*/
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
				} 
				else if (response != null && response.getStatusCode().toString().equals("400")) 
				{
					pId = "0"; 
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				file.delete();
			}
			return pId;

		}
	
	/*
	 public String saveAttachmentToBug(MultipartFile[] file, String bugId) throws IOException, ConnectionException 
	{
		Bug b = null;
		b.setComponent("suite");
		//Attachment attach = new Attachment(file[0].getBytes(), bugId);
		AttachmentFactory af = null;
		af.newAttachment().setBugID(485).setCreationDate(new Date()).setData(file[0].getBytes()).setCreator("Admin");
		BugzillaMethod bug;
		AddAttachment aa;

		BugTrackorToolEntity bugTrackorTool = getBugToolDetails("Bugzila");
		BugzillaConnector bc = null;
		bc.connectTo(bugTrackorTool.getIP(), bugTrackorTool.getUserid(), bugTrackorTool.getPassword());
		return bugId;

	}*/
	 
	 
	public File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}
	
	/*
	 * public String createBugAuto(ExecutionReportAutomation exLogs) {
	 * //System.out.println("------jhtfsdhdf"); String pId=null; String
	 * url="/rest/bug"; JSONObject request =new JSONObject(); request.put("product",
	 * exLogs.getProjectName()); request.put("component", "Application Testing");
	 * request.put("version", "unspecified");
	 * request.put("summary",exLogs.getTbugsummary()); String
	 * des="Description: "+exLogs.getTdescription()+"\n\nBug Summary: "+exLogs.
	 * getTbugsummary()+ "\n\nExpected Result: "+exLogs.getTexpectedResult();
	 * request.put("description", des); request.put("op_sys", "All");
	 * request.put("rep_platform", "All"); request.put("status", "open");
	 * request.put("severity",exLogs.getStatus());
	 * request.put("priority",exLogs.getTpriority()); //System.out.println(request);
	 * ResponseEntity<Object> response=postCall(url,request); if(response!=null &&
	 * response.getStatusCode().toString().equals("200")) { try { Gson gson=new
	 * Gson(); String json = gson.toJson(response.getBody()); JSONParser parser =
	 * new JSONParser(); Object obj=parser.parse(json); org.json.simple.JSONObject
	 * topObject = (org.json.simple.JSONObject) obj;
	 * //System.out.println(topObject.toString());
	 * //pId=Integer.parseInt(topObject.get("id").toString());
	 * pId=topObject.get("id").toString(); } catch (Exception e) {
	 * e.printStackTrace(); } } return pId; }
	 */
	
	/*
	 * public static void main(String args[]) { BugZillaService bs=new
	 * BugZillaService(); bs.fetchBugComment(14); }
	 */
	public BugDetailsEntity getBugStatus(String pName,Integer pID, String bugID) {
		BugDetailsEntity be=new BugDetailsEntity();
		String status="";
		String severity="";
		String url = "/rest.cgi/bug/"+bugID;
		try {
			ResponseEntity<Object> response = getCall(url, pID);
			if (response != null && response.getStatusCode().toString().equals("200")) {
				try {
					Gson gson = new Gson();
					String json = gson.toJson(response.getBody());
					JSONObject obj = new JSONObject(json);
					org.json.JSONArray issueArray = obj.getJSONArray("bugs");
					JSONObject projectObj=issueArray.getJSONObject(0);
					if(pName.equals(projectObj.getString("product"))) {
						String bugStatus=projectObj.getString("status");
						String bugResolution=projectObj.getString("resolution");
						if(("REOPENED").equals(bugStatus)) {
							status="Reopen";
						}
						else if(("VERIFIED").equals(bugStatus) && ("FIXED").equals(bugResolution)) {
						  status="Closed"; 
						}
						 
						else if(("RESOLVED").equals(bugStatus) && ("FIXED").equals(bugResolution)) {
							status="Resolved";
						}
						else if(("CONFIRMED").equals(bugStatus)) {
							status="Open";
						}
						be.setBugStatus(status);
						String bugPriority=projectObj.getString("priority");
						if(bugPriority.equals("High") || bugPriority.equals("Low")) {
							be.setBugPriority(bugPriority);
						}
						String bugSeverity=projectObj.getString("severity");
						if(bugSeverity.equals("blocker") || bugSeverity.equals("critical")
								|| bugSeverity.equals("minor") || bugSeverity.equals("major")){
							be.setBugSeverity(StringUtils.capitalize(bugSeverity));
						}
						
					}
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return be;
	}
	
	/*
	 * public ResponseEntity<Object> getCall(String url, Integer pID,
	 * ProjectDeatilsMeta pm) { ResponseEntity<Object> response = null;
	 * ProjectDeatilsMeta bugTrackorTool=new ProjectDeatilsMeta(); if(pm!=null) {
	 * bugTrackorTool = pm; } else { bugTrackorTool = getBugToolDetails(pID); } if
	 * (bugTrackorTool != null) { url = bugTrackorTool.getBugToolURL() + url;
	 * HttpHeaders headers = new HttpHeaders(); headers.set("Authorization",
	 * bugTrackorTool.getBugToolApiToken());
	 * headers.setContentType(MediaType.APPLICATION_JSON); HttpEntity<String> entity
	 * = new HttpEntity<String>(headers); UriComponentsBuilder uriBuilder =
	 * UriComponentsBuilder.fromHttpUrl(url); response =
	 * restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, entity,
	 * Object.class); } return response; }
	 */
}