package com.apmosys.tmt.services;

import java.util.Hashtable;

import javax.naming.ldap.LdapContext;


import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.springframework.stereotype.Service;

@Service
public class CheckAuthService {
	
	private static LdapContext ctx = null;
	private boolean authval;

	public boolean authenticate(String uname, String pass) {
		String username = uname;

		String password = pass;

		String base = "OU=Users Accounts,DC=axisb,DC=com";
		String dn = "uid=" + username + "," + base;

		String ldapURL = "ldap://10.9.80.6:389";

		// Setup environment for authenticating

		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.SECURITY_AUTHENTICATION, "Simple");

		env.put(Context.SECURITY_PRINCIPAL, "AXISB\\" + uname + "");
		env.put(Context.SECURITY_CREDENTIALS, "" + pass + "");
		env.put(Context.PROVIDER_URL, ldapURL);
		
	
		try

		{

			DirContext authContext =

			new InitialDirContext(env);
			authval= true;
			// user is authenticated

		}

		catch (AuthenticationException ex)

		{
			authval= false;
			// Authentication failed

		}

		catch (NamingException ex)

		{

			ex.printStackTrace();

		}
		return authval;
	}

}