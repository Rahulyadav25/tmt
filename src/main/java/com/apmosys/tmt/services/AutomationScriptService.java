package com.apmosys.tmt.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.apmosys.tmt.BO.AutomationTestSuitesBO;
import com.apmosys.tmt.BO.ComputerDetailsBO;
import com.apmosys.tmt.BO.JenkinsProjectDetails;
import com.apmosys.tmt.dao.AutomationRunDao;
import com.apmosys.tmt.models.AutomationRunLogs;
import com.apmosys.tmt.models.AutomationRunTable;
import com.apmosys.tmt.models.EndUserSystemDetails;
import com.apmosys.tmt.models.UpdateLogs;
import com.apmosys.tmt.repository.EndUserSystemDetailsRepository;
import com.apmosys.tmt.repository.UpdateLogsRepository;
import com.apmosys.tmt.utils.ServiceResponse;
//import com.cdancy.jenkins.rest.JenkinsClient;
//import com.cdancy.jenkins.rest.domain.system.SystemInfo;
import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.JenkinsTriggerHelper;
import com.offbytwo.jenkins.model.Computer;
import com.offbytwo.jenkins.model.JobWithDetails;

import jakarta.transaction.Transactional;


@Service
public class AutomationScriptService {
	
	@Value("${jenkins.server.url}")
	private String jenkinsUrl;
	
	@Value("${jenkins.server.pwd}")
	private String jenkinsPwd;
	
	@Value("${jenkins.server.user}")
	private String penkinsUser;
	
	@Value("${automationPath}")
	private String automationPath;
	
	@Value("${automation.master.path}")
	private String automationMaster;
	
	@Value("${dependencyPath}")
	private String dependencyPath;
	
//	@Value("${jenkins.slave.workspace.windows}")
//	private String jenkinsSlaveWindowsPath;
//	
//	@Value("${jenkins.slave.workspace.linux}")
//	private String jenkinsSlaveLinuxPath;
	
	@Value("${jenkins.job.copyTMTFiles}")
	private String jenkinsCopyFileJob;
	@Autowired
	private AutomationRunDao automationRunDao;
	
	@Autowired
	private EndUserSystemDetailsRepository endUserSystemDetailsRepo;
	
	@Autowired
	private UpdateLogsRepository updateLogsRepository;
	
	@Autowired
	private AutomationService automationService;

	public ServiceResponse getSlaveMechine() {
		String serviceStatus = "400";
		JenkinsServer jenkins = null;
		Map<String, Computer> map = null;
		List<ComputerDetailsBO> compList = new ArrayList<ComputerDetailsBO>();
		ServiceResponse response = new ServiceResponse();
		String s = null;
		try {
			
			
			jenkins = new JenkinsServer(new URI(jenkinsUrl), penkinsUser, jenkinsPwd);
			if (jenkins != null) {
				JobWithDetails  sss=jenkins.getJob("Copy_TMT_Files");
				System.out.println(jenkins.getJobs());
				map = jenkins.getComputers();
				for (Entry<String, Computer> e : map.entrySet()) {
					ComputerDetailsBO detailsBO = new ComputerDetailsBO();
					Computer com = e.getValue();
					detailsBO.setComputerName(com.getDisplayName());
      				detailsBO.setIsOffline(com.details().getOffline());
					try {
						
						
						if(com.details().getOffline()== false) {
							Map<String, Map> mapL=com.details().getMonitorData();
							Map l = new HashMap<String, String>();
							l.put("k", mapL.get("hudson.node_monitors.ArchitectureMonitor"));
							s = l.get("k").toString();
						}
						detailsBO.setComputerOS((s!=null)?s:null);						
					}
					catch(Exception e1) {
						e1.printStackTrace();
					}
					addEndUserSystemDetails(com.getDisplayName());
					compList.add(detailsBO);
				}
				serviceStatus = "200";
				response.setServiceResponse(compList);
			}

		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block664
			e1.printStackTrace();
		}
		catch (Exception e1) {
			// TODO Auto-generated catch block664
			e1.printStackTrace();
		}
		response.setServiceStatus(serviceStatus);
		return response;
	}
	
	@Transactional
	public Boolean addEndUserSystemDetails(String compList) {
		
		try {
				Integer eusID=endUserSystemDetailsRepo.findEUSDByName(compList);
				if(eusID==null)
				{
					
					//new added
					   
					   SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
					    Date date = new Date();  
					    Timestamp timestamp2 = new Timestamp(date.getTime());
					    System.out.println(formatter.format(timestamp2));
					   
					EndUserSystemDetails eu=new EndUserSystemDetails();
					eu.setName(compList);
					eu.setCreatedOn(new Date());
					eu.setUpdatedOn(timestamp2);
					endUserSystemDetailsRepo.save(eu);
				}
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	/*
	 * public Boolean addEndUserSystemDetails(List<ComputerDetailsBO> compList) {
	 * 
	 * try { for(ComputerDetailsBO cd : compList) { EndUserSystemDetails
	 * eus=endUserSystemDetailsRepo.findByName(cd.getComputerName()); if(eus==null)
	 * { eus.setName(cd.getComputerName()); endUserSystemDetailsRepo.save(eus); } }
	 * } catch(Exception e) { e.printStackTrace(); } return null;
	 * 
	 * }
	 */
	public ServiceResponse startScript(Integer pId, String pName) {
		System.out.println("-----------------------In startScript method of AutomationSCriptS----------------------");
		JenkinsServer jenkins = null;
		JenkinsTriggerHelper helper = null;
		String Status = "Failed";
		ServiceResponse response = new ServiceResponse();
		AutomationRunTable runDetails = new AutomationRunTable();
	
		
		try {
			
			jenkins = new JenkinsServer(new URI(jenkinsUrl), penkinsUser, jenkinsPwd);
			if (jenkins != null) {
				runDetails = automationRunDao.createRun(pName);
//				jenkins.getJobs().get(pName).details().build(true);
				helper=new JenkinsTriggerHelper(jenkins);
				Status=helper.triggerJobAndWaitUntilFinished(pName, true).getResult().toString();
				/*
				 * if (runDetails.getRunId() != null) { Status = "Running"; }
				 */
			}
			if (runDetails.getRunId() != null) {
				if("FAILURE".equals(Status)) {
					automationRunDao.updateRunHistoryStatus(runDetails.getRunId(), "Fail");
				}
				else if("CANCELLED".equals(Status)) {
					automationRunDao.updateRunHistoryStatus(runDetails.getRunId(), "Stopped");
				}
			}
			response.setServiceStatus("200");
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something Went Wrong";
			response.setServiceStatus("400");
			if (runDetails.getRunId() != null) {
				automationRunDao.updateRunHistoryStatus(runDetails.getRunId(), "Stopped");
			}
		}
		response.setServiceResponse(Status);
		return response;

	}
	public ServiceResponse testConnection(String pName, String slave,String applicationName) {
		ServiceResponse response = new ServiceResponse();
		JenkinsServer jenkins = null;
		JenkinsTriggerHelper helper = null;
		String Status = "Fail";
		String jobXml="<project>\n"
				+ "  <actions/>\n"
				+ "  <description></description>\n"
				+ "  <keepDependencies>false</keepDependencies>\n"
				+ "  <properties>\n"
				+ "    <hudson.plugins.copyartifact.CopyArtifactPermissionProperty plugin=\"copyartifact@1.45.3\">\n"
				+ "      <projectNameList>\n"
				+ "        <string>Copy_TMT_Files</string>\n"
				+ "      </projectNameList>\n"
				+ "    </hudson.plugins.copyartifact.CopyArtifactPermissionProperty>\n"
				+ "  </properties>\n"
				+ "  <scm class=\"hudson.scm.NullSCM\"/>\n"
				+ "  <assignedNode>"+slave.split("--")[0]+"</assignedNode>\n"
				+ "  <canRoam>false</canRoam>\n"
				+ "  <disabled>false</disabled>\n"
				+ "  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>\n"
				+ "  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>\n"
				+ "  <triggers/>\n"
				+ "  <concurrentBuild>false</concurrentBuild>\n"
				+ "  <builders>\n"
				+ "    <hudson.plugins.copyartifact.CopyArtifact plugin=\"copyartifact@1.45.3\">\n"
				+ "      <project>Copy_TMT_Files</project>\n"
				+ "      <filter>"+pName+"/*</filter>\n"
				+ "      <target>../</target>\n"
				+ "      <excludes></excludes>\n"
				+ "      <selector class=\"hudson.plugins.copyartifact.WorkspaceSelector\"/>\n"
				+ "      <doNotFingerprintArtifacts>false</doNotFingerprintArtifacts>\n"
				+ "    </hudson.plugins.copyartifact.CopyArtifact>\n"
				+ "  </builders>\n"
				+ "  <publishers/>\n"
				+ "  <buildWrappers/>\n"
				+ "</project>";
		try {
			jenkins = new JenkinsServer(new URI(jenkinsUrl), penkinsUser, jenkinsPwd);
			if (jenkins != null) {
//				System.out.println(jenkins.getJobXml("TestConnection"));
				try {
					System.out.println("Test connection created-------------");
					jenkins.createJob("TestConnection", jobXml, true);
				} catch (IOException e) {
					System.out.println("updating-------------");
					jenkins.updateJob("TestConnection", jobXml, true);
				}
				helper=new JenkinsTriggerHelper(jenkins);
				Status=helper.triggerJobAndWaitUntilFinished("TestConnection", true).getResult().toString();
				if("FAILURE".equals(Status) || "CANCELLED".equals(Status)){
					response.setServiceStatus(ServiceResponse.STATUS_FAIL);
				}
				else if("SUCCESS".equals(Status)) {
					response.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				}
			}
			response.setServiceStatus("200");
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something Went Wrong";
			response.setServiceStatus("400");
		}
		response.setServiceResponse(Status);
		return response;

	}
	/*
	 * public ServiceResponse startScript(Integer pId, String pName) { //
	 * System.out.
	 * println("-----------------------In startScript method of AutomationSCriptS----------------------"
	 * ); JenkinsServer jenkins = null; String Status = "Failed"; ServiceResponse
	 * response = new ServiceResponse(); try { jenkins = new JenkinsServer(new
	 * URI(jenkinsUrl), penkinsUser, jenkinsPwd); if (jenkins != null) {
	 * AutomationRunTable runDetails = automationRunDao.createRun(pName);
	 * jenkins.getJobs().get(pName).details().build(true); Thread.sleep(5000);
	 * 
	 * if (runDetails.getRunId() != null) { Status = "Running"; } }
	 * 
	 * } catch (Exception e) { e.printStackTrace(); Status = "Something went wrong";
	 * response.setServiceStatus("400");
	 * 
	 * } response.setServiceResponse(Status);
	 * 
	 * return response;
	 * 
	 * }
	 */

	public ServiceResponse stopScript(Integer pId, String pName) {
		JenkinsServer jenkins = null;
		String Status = "Failed";
		ServiceResponse response = new ServiceResponse();
		try {
			AutomationRunTable runTable = automationRunDao.getCurrentRun(pName);
			if (runTable.getRunId() != 0 && runTable.getStatus().equalsIgnoreCase("RUNNING")) {
				jenkins = new JenkinsServer(new URI(jenkinsUrl), penkinsUser, jenkinsPwd);
				if (jenkins != null) {
					//automationRunDao.stopJenkinsProject(pName,"", runTable.getpId(),jenkins);
					//Thread.sleep(5000);
					try {
						jenkins.getJob(pName).details().getLastBuild().Stop(true);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
					
					Integer count = automationRunDao.updateStopStatus(runTable.getRunId());
					if (count != null) {
						Status = "Stoped";
					} else {
						Status = "flag not updated";
					}
				}
				response.setServiceStatus("200");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");

		}
		response.setServiceResponse(Status);
		return response;

	}

	public ServiceResponse getCurrentRun(String project) {
		ServiceResponse response = new ServiceResponse();
		try {
			AutomationRunTable runTable = automationRunDao.getCurrentRun(project);
			response.setServiceResponse(runTable);
			response.setServiceStatus("200");
		} catch (Exception e) {
			e.printStackTrace();
			String Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceError(Status);

		}
		return response;
	}

	public ServiceResponse currentRunDetails(String project) {
		ServiceResponse response = new ServiceResponse();
		try {
			List<Object> list = new ArrayList<Object>();
			AutomationRunTable runTable = automationRunDao.getCurrentRun(project);
			List<AutomationRunLogs> logs = automationRunDao.getCurrentRunLogs(project);
			list.add(runTable);
			list.add(logs);
			response.setServiceResponse(list);
			response.setServiceStatus("200");
		} catch (Exception e) {
			e.printStackTrace();
			String Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceError(Status);

		}
		return response;
	}

	/*
	 * public ServiceResponse createJenkinsProject(JenkinsProjectDetails
	 * projectDetails) throws IOException { ServiceResponse response = new
	 * ServiceResponse(); JenkinsServer jenkins = null; String cmd = null; String
	 * status = "Unable to Change Node"; String statusCode = "404"; if
	 * (projectDetails.getOsType().equals("linux")) { cmd = "<hudson.tasks.Shell>" +
	 * " <command>\r" + "java -jar AUTOMATION.jar</command>" +
	 * "</hudson.tasks.Shell>"; } else if
	 * (projectDetails.getOsType().equals("window")) { cmd =
	 * "<hudson.tasks.BatchFile>" + " <command> \r" +
	 * "java -jar AUTOMATION.jar</command>" + "</hudson.tasks.BatchFile>"; } String
	 * jobXml = "<?xml version='1.1' encoding='UTF-8'?>" + "<project>" +
	 * "<actions/>" + "<description></description>" + "<displayName>" +
	 * projectDetails.getpName() + "</displayName>" +
	 * "<keepDependencies>false</keepDependencies>" + "<properties/>" +
	 * "<scm class='hudson.scm.NullSCM'/>" + "<assignedNode>" +
	 * projectDetails.getSlaveMachineName() + "</assignedNode>" +
	 * "<canRoam>false</canRoam>" + "<disabled>false</disabled>" +
	 * "<blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>"
	 * + "<blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>" +
	 * " <triggers/>" + " <concurrentBuild>false</concurrentBuild>" + "<builders>" +
	 * cmd + "</builders>" + "<publishers/>" + "<buildWrappers/>" + "</project>";
	 * try { jenkins = new JenkinsServer(new URI(jenkinsUrl), penkinsUser,
	 * jenkinsPwd); try { if (jenkins != null && projectDetails.getpName() != null
	 * && !projectDetails.getpName().equals("")) {
	 * jenkins.createJob(projectDetails.getpName(), jobXml, true); status =
	 * "Prject Has been assigined to " + projectDetails.getSlaveMachineName() +
	 * " Node"; statusCode = "200"; } } catch (IOException e) {
	 * System.out.println("updating-------------"); if (jenkins != null &&
	 * projectDetails.getpName() != null && !projectDetails.getpName().equals("")) {
	 * jenkins.updateJob(projectDetails.getpName(), jobXml, true); status =
	 * "Prject Has been changed to " + projectDetails.getSlaveMachineName() +
	 * " Node"; statusCode = "300"; } } catch (Exception e) { e.printStackTrace(); }
	 * } catch (Exception e) { e.printStackTrace(); }
	 * response.setServiceStatus(statusCode); response.setServiceResponse(status);
	 * return response; }
	 */

	public ServiceResponse getTestSuitesName(String projectName) {

		ServiceResponse serviceResponse = new ServiceResponse();
		String status = "400";
		DataFormatter dataFormatter = new DataFormatter();
		List<AutomationTestSuitesBO> list = new ArrayList<AutomationTestSuitesBO>();
		List<Integer> processCellVal=new ArrayList<Integer>();
		
		try {
			File mainSheet = new File(
					automationPath + File.separator + projectName + File.separator + "Main_Controller.xlsx");
			if (mainSheet.exists()) {
				XSSFWorkbook myWorkBook = new XSSFWorkbook(mainSheet);
				XSSFSheet mySheet = myWorkBook.getSheetAt(0);
				XSSFSheet dataSheet = myWorkBook.getSheet("DataSheet");
				//System.out.println(mySheet.getFirstRowNum());
				int first = 0;
				Map<String, Integer> map = new HashMap<String, Integer>();
				
				int firstDS = 0;
				Map<String, Integer> mapDS = new HashMap<String, Integer>();
				
				for (Row row : mySheet) {
					if (first == 0) {
						first = first + 1;
						XSSFRow row1 = mySheet.getRow(0);
						short minColIx = row.getFirstCellNum(); // get the first column index for a row
						short maxColIx = row.getLastCellNum(); // get the last column index for a row
						//System.out.println(minColIx+"==="+maxColIx);
						for (short colIx = minColIx; colIx < maxColIx; colIx++) { // loop from first to last index
							XSSFCell cell = row1.getCell(colIx); // get the cell
							// System.out.println(cell.getStringCellValue()+"---"+cell.getColumnIndex());
							map.put(cell.getStringCellValue(), cell.getColumnIndex()); // add the cell contents (name of
																						// column) and cell index to the
																						// map
							if(cell.getStringCellValue().contains("Process")) {
								processCellVal.add(cell.getColumnIndex());
							}
						}
					} else {
						//System.out.println(map);
						//System.out.println(map.get("ApplicationID"));
						AutomationTestSuitesBO suitesBO = new AutomationTestSuitesBO();
						suitesBO.setTestSuitesName(
								dataFormatter.formatCellValue(row.getCell(map.get("ApplicationID"))));
						suitesBO.setIsRun(dataFormatter.formatCellValue(row.getCell(map.get("RunStatus"))));
						
						List<String> procesVal=new ArrayList<String>();
						for(Integer pcv:processCellVal) {
							String pname=dataFormatter.formatCellValue(row.getCell(pcv));
							if(!((pname.equals(null))||(pname.equals("")))) {
								procesVal.add(pname);
							}
						}
						// --------------Get data from 'DataSheet' sheet(start)----------------- //
						List<String> dataSheetName=new ArrayList<String>();
						for (Row rowDS : dataSheet) {
							if (firstDS == 0) {
								firstDS = firstDS + 1;
								XSSFRow row1DS = dataSheet.getRow(0);
								short minColIxDS = rowDS.getFirstCellNum(); // get the first column index for a row
								short maxColIxDS = rowDS.getLastCellNum(); // get the last column index for a row
								for (short colIxDS = minColIxDS; colIxDS < maxColIxDS; colIxDS++) { // loop from first to last index
									XSSFCell cellDS = row1DS.getCell(colIxDS); // get the cell
									mapDS.put(cellDS.getStringCellValue(), cellDS.getColumnIndex()); // add the cell contents (name of
																								// column) and cell index to the
																								// map
								}
							} else {
								for(String pv:procesVal) {
									String processName=dataFormatter.formatCellValue(rowDS.getCell(mapDS.get("Process")));
									if(!((pv.equals(null))||(pv.equals("")))) {
										if(pv.equals(processName)) {
											dataSheetName.add(dataFormatter.formatCellValue(rowDS.getCell(mapDS.get("TestFlow_Path"))));
										}
									}
									
								}
							}
						}
						
						for(String dsn:dataSheetName) {
							File dsnFile=new File(automationPath + File.separator + projectName 
									+ File.separator + "DataSheets" + File.separator + dsn);
							if(dsnFile.exists()) {
								suitesBO.setIsFilePresent(true);
							}
							else {
								suitesBO.setIsFilePresent(false);
								break;
							}
						}
						// --------------Get data fr0m 'DataSheet' sheet(start------------------ //
						
						list.add(suitesBO);
					}
				}
				status = "200";
				serviceResponse.setServiceResponse(list);
			} else {
				status = "201";
			}
		} catch (Exception e) {
			status = "301";
			e.printStackTrace();
		}
		serviceResponse.setServiceStatus(status);
		return serviceResponse;
	}

	public void updateMainController(String datasheetsList, JenkinsProjectDetails projectdetails) {
		//JenkinsServer jenkins = null;		//02_05_2020                
		String cancelstatus;
		File dir;
		String zipDirName;
		//String ostype=projectdetails.getOsType();
//		System.out.println("Calling-------------------------------------updateMainController");
		ServiceResponse serviceResponse = new ServiceResponse();
		String status = "400";
		DataFormatter dataFormatter = new DataFormatter();
		List<AutomationTestSuitesBO> list = new ArrayList<AutomationTestSuitesBO>();
		Integer endUserSystemID;
		try {
			File mainSheet = new File(automationPath + File.separator + projectdetails.getpName() + File.separator
					+ "Main_Controller.xlsx");
			mainSheet.setWritable(true); //02_05_2020
			if (mainSheet.exists()) {
				FileInputStream fis = new FileInputStream(mainSheet);
				System.out.println(datasheetsList);
				XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
				XSSFSheet mySheet = myWorkBook.getSheetAt(0);
				//System.out.println(mySheet.getFirstRowNum());
				int first = 0;
				Map<String, Integer> map = new HashMap<String, Integer>();
				for (Row row : mySheet) {
					if (first == 0) {
						first = first + 1;
						XSSFRow row1 = mySheet.getRow(0);
						short minColIx = row.getFirstCellNum(); // get the first column index for a row
						short maxColIx = row.getLastCellNum(); // get the last column index for a row
						for (short colIx = minColIx; colIx < maxColIx; colIx++) { // loop from first to last index
							XSSFCell cell = row1.getCell(colIx); // get the cell
							// System.out.println(cell.getStringCellValue()+"---"+cell.getColumnIndex());
							map.put(cell.getStringCellValue(), cell.getColumnIndex()); // add the cell contents (name of
																						// column) and cell index to the
																						// map
						}
					} else {
						AutomationTestSuitesBO suitesBO = new AutomationTestSuitesBO();
						suitesBO.setTestSuitesName(
								dataFormatter.formatCellValue(row.getCell(map.get("ApplicationID"))));
						if (datasheetsList.equals(suitesBO.getTestSuitesName())) {
//							System.out.println(dataFormatter.formatCellValue(row.getCell(map.get("RunStatus"))) + "===");
							/*
							 * if(dataFormatter.formatCellValue(row.getCell(map.get("RunStatus"))).
							 * equalsIgnoreCase("N")) {
							 * automationService.updateProjectLogsTable(projectdetails.getpId()); }
							 */
							row.getCell(map.get("RunStatus")).setCellValue("Y");
							automationService.updateProjectLogsTable(projectdetails.getpId());

						} else {
							row.getCell(map.get("RunStatus")).setCellValue("N");
						}
						list.add(suitesBO);
						
					}
				}
				
				status = "200";
				serviceResponse.setServiceResponse(list);
				fis.close();
				FileOutputStream output_file = new FileOutputStream(mainSheet);
				myWorkBook.write(output_file);
				output_file.close();
			} else {
				status = "201";
			}

			//jenkins call
//			System.out.println("Calling Jenkins-----------------------------------");
			//ostype=System.getProperty("os.name");
			/*
			 * endUserSystemID=endUserSystemDetailsRepo.findEUSDByName(projectdetails.
			 * getSlaveMachineName());
			 * 
			 * String isUpdated=checkForUpdateFileTransfer(endUserSystemID,
			 * projectdetails.getpId()); if(isUpdated=="Update") {
			 * //fileTMTtoJenkinsServer(true,projectdetails.getSlaveMachineName(),
			 * projectdetails.getpName()); createJenkinsProject(projectdetails,true);
			 * updateSystemLogsTable(endUserSystemID, projectdetails.getpId()); } else {
			 * //fileTMTtoJenkinsServer(false,projectdetails.getSlaveMachineName(),
			 * projectdetails.getpName()); createJenkinsProject(projectdetails,false); }
			 */
			 
			
			createJenkinsProject(projectdetails,true);
			
			/*try {
				//jenkins = new JenkinsServer(new URI(jenkinsUrl), penkinsUser, jenkinsPwd);//02_05_2020 
				System.out.println(projectdetails.getSlaveMachineName().equals("master"));
				if (projectdetails.getSlaveMachineName().equals("master")) 
				{
					File dependency=new File(dependencyPath);
					File slave=new File(automationPath+File.separator+projectdetails.getpName());
					File master=new File(automationMaster+File.separator+projectdetails.getpName());
					try
					{
						//FileUtils.copyDirectory(dependency, master);
						//copyFileUsingStream(dependency, master);
						copyDir(dependencyPath, automationMaster+File.separator+projectdetails.getpName(), true);
						System.out.println("dependency files copied to workspace.....");
						FileUtils.copyDirectory(slave, master);
						//copyFileUsingStream(slave, master);
						//copyDir(automationPath+File.separator+projectdetails.getpName(), automationMaster+File.separator+projectdetails.getpName(), true);
						System.out.println("project files copied to workspace.....");
					}
					catch(Exception e) {
						
					}
					//jenkins.getJobs().get(projectdetails.getpName()).details().build(true);//02_05_2020 
					
				} else {

					//zip the existing folder in master
					dir = new File(automationPath + File.separator + projectdetails.getpName());
					zipDirName = automationPath + File.separator + projectdetails.getpName() + ".zip";

					String isProjectZipped = zipDirectory(dir, zipDirName);
					System.out.println("done    " + isProjectZipped);

					if (isProjectZipped.equals("zipped")) {

						if (ostype.equalsIgnoreCase("windows")||ostype.equalsIgnoreCase("window"))
							cancelstatus = transferToSlave(projectdetails.getSlaveMachineName(),
									projectdetails.getpName(), jenkinsSlaveWindowsPath);
						else
							cancelstatus = transferToSlave(projectdetails.getSlaveMachineName(),
									projectdetails.getpName(), jenkinsSlaveLinuxPath);

						System.out.println("script going to start........" + cancelstatus);
						if (cancelstatus.equals("SUCCESS"))
							startScript(projectdetails.getpId(), projectdetails.getpName());

					} else {

					}

				}
			} catch (Exception e) {
				status = "301";
				e.printStackTrace();
			}*/
			
		} catch (Exception e) {
			status = "301";
			e.printStackTrace();
		}
	}
	
	public String createJenkinsProject(JenkinsProjectDetails projectDetails, Boolean toUpdate) throws IOException {
		//ServiceResponse response = new ServiceResponse();
		JenkinsServer jenkins = null;
		String cmd = null;
		String status = "Unable to Change Node";
		String statusCode = "404";
		String copyArtifact="";
		
		if (projectDetails.getOsType().toLowerCase().contains("linux")) {
			cmd = "<hudson.tasks.Shell>" + " <command>\r" + "java -jar AUTOMATION.jar</command>"
					+ "</hudson.tasks.Shell>";
		} else if (projectDetails.getOsType().toLowerCase().contains("windows")) {
			cmd = "<hudson.tasks.BatchFile>" + " <command> \r" + "java -jar AUTOMATION.jar</command>"
					+ "</hudson.tasks.BatchFile>";
		}
		if(toUpdate==true) {
			copyArtifact= 
					"    <hudson.plugins.copyartifact.CopyArtifact plugin=\"copyartifact@1.44\">\n" + 
							"      <project>"+jenkinsCopyFileJob+"</project>\n" + 
							"      <filter>"+projectDetails.getpName()+"/*,"+projectDetails.getpName()+"/DataSheets/*</filter>\n" + 
							"      <target>../</target>\n" + 
							"      <excludes></excludes>\n" + 
							"      <selector class=\"hudson.plugins.copyartifact.WorkspaceSelector\"/>\n" + 
							"      <doNotFingerprintArtifacts>false</doNotFingerprintArtifacts>\n" + 
							"    </hudson.plugins.copyartifact.CopyArtifact>\n" ;
		};
		String jobXml = "<?xml version='1.1' encoding='UTF-8'?>\n" + 
				"<project>\n" + 
				"  <actions/>\n" + 
				"  <description>copy attachment to end user machine</description>\n" + 
				"  <keepDependencies>false</keepDependencies>\n" + 
				"  <properties>\n" + 
				"    <hudson.plugins.copyartifact.CopyArtifactPermissionProperty plugin=\"copyartifact@1.44\">\n" + 
				"      <projectNameList>\n" + 
				"        <string>"+jenkinsCopyFileJob+"</string>\n" + 
				"      </projectNameList>\n" + 
				"    </hudson.plugins.copyartifact.CopyArtifactPermissionProperty>\n" + 
				"    <hudson.plugins.svn__partial__release__mgr.ui.JobPropertyImpl plugin=\"svn-partial-release-mgr@1.0.1\"/>\n" + 
				"  </properties>\n" + 
				"  <scm class=\"hudson.scm.NullSCM\"/>\n" + 
				"  <assignedNode>"+projectDetails.getSlaveMachineName()+"</assignedNode>\n" + 
				"  <canRoam>false</canRoam>\n" + 
				"  <disabled>false</disabled>\n" + 
				"  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>\n" + 
				"  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>\n" + 
				"  <triggers/>\n" + 
				"  <concurrentBuild>false</concurrentBuild>\n" + 
				"  <builders>\n" +copyArtifact+ 
				"    <sp.sd.fileoperations.FileOperationsBuilder plugin=\"file-operations@1.9\">\n" + 
				"      <fileOperations>\n" + 
				"        <sp.sd.fileoperations.FolderCopyOperation>\n" + 
				"          <sourceFolderPath>../Framework</sourceFolderPath>\n" + 
				"          <destinationFolderPath></destinationFolderPath>\n" + 
				"        </sp.sd.fileoperations.FolderCopyOperation>\n" + 
				"      </fileOperations>\n" + 
				"    </sp.sd.fileoperations.FileOperationsBuilder>\n" +cmd+ 
				"  </builders>\n" + 
				"  <publishers/>\n" + 
				"  <buildWrappers/>\n" + 
				"</project>";
		try {
			jenkins = new JenkinsServer(new URI(jenkinsUrl), penkinsUser, jenkinsPwd);
			try {
				if (jenkins != null && projectDetails.getpName() != null && !projectDetails.getpName().equals("")) {
					jenkins.createJob(projectDetails.getpName(), jobXml, true);
					status = "Project Has been assigined to " + projectDetails.getSlaveMachineName() + " Node";
					statusCode = "200";
				}
			} catch (IOException e) {
//				System.out.println("updating-------------");
				if (jenkins != null && projectDetails.getpName() != null && !projectDetails.getpName().equals("")) {
					jenkins.updateJob(projectDetails.getpName(), jobXml, true);
					status = "Project Has been changed to " + projectDetails.getSlaveMachineName() + " Node";
					statusCode = "300";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * response.setServiceStatus(statusCode); response.setServiceResponse(status);
		 * return response;
		 */
//		System.out.println("--------------------------"+status);
		return status;
	}
	
	public String fileTMTtoJenkinsServer(Boolean toUpdate, String node, String pName) {
		String status="File tranfer job not built...";
		JenkinsServer jenkins = null;
		JenkinsTriggerHelper help = null;
		//String isCancelled = "CANCELLED";
		String fromTMTJenkinsCopyXml = "<?xml version='1.1' encoding='UTF-8'?>\n" + 
				"<project>\n" + 
				"  <actions/>\n" + 
				"  <description>copy attachment to end user machine</description>\n" + 
				"  <keepDependencies>false</keepDependencies>\n" + 
				"  <properties>\n" + 
				"    <hudson.plugins.copyartifact.CopyArtifactPermissionProperty plugin=\"copyartifact@1.44\">\n" + 
				"      <projectNameList>\n" + 
				"        <string>"+jenkinsCopyFileJob+"</string>\n" + 
				"      </projectNameList>\n" + 
				"    </hudson.plugins.copyartifact.CopyArtifactPermissionProperty>\n" + 
				"    <hudson.plugins.svn__partial__release__mgr.ui.JobPropertyImpl plugin=\"svn-partial-release-mgr@1.0.1\"/>\n" + 
				"  </properties>\n" + 
				"  <scm class=\"hudson.scm.NullSCM\"/>\n" + 
				"  <assignedNode>"+node+"</assignedNode>\n" + 
				"  <canRoam>false</canRoam>\n" + 
				"  <disabled>false</disabled>\n" + 
				"  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>\n" + 
				"  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>\n" + 
				"  <triggers/>\n" + 
				"  <concurrentBuild>false</concurrentBuild>\n" + 
				"  <builders>\n" + 
				"    <sp.sd.fileoperations.FileOperationsBuilder plugin=\"file-operations@1.9\">\n" + 
				"      <fileOperations>\n" + 
				"        <sp.sd.fileoperations.FolderCopyOperation>\n" + 
				"          <sourceFolderPath>${WORKSPACE}/Framework</sourceFolderPath>\n" + 
				"          <destinationFolderPath>${WORKSPACE}/"+pName+"</destinationFolderPath>\n" + 
				"        </sp.sd.fileoperations.FolderCopyOperation>\n" + 
				"      </fileOperations>\n" + 
				"    </sp.sd.fileoperations.FileOperationsBuilder>\n" + 
				"  </builders>\n" + 
				"  <publishers/>\n" + 
				"  <buildWrappers/>\n" + 
				"</project>";
		try {
			jenkins = new JenkinsServer(new URI(jenkinsUrl), penkinsUser, jenkinsPwd);
			try {
				if(toUpdate==true) {
					fromTMTJenkinsCopyXml = "<?xml version='1.1' encoding='UTF-8'?>\n" + 
							"<project>\n" + 
							"  <actions/>\n" + 
							"  <description>copy attachment to end user machine</description>\n" + 
							"  <keepDependencies>false</keepDependencies>\n" + 
							"  <properties>\n" + 
							"    <hudson.plugins.copyartifact.CopyArtifactPermissionProperty plugin=\"copyartifact@1.44\">\n" + 
							"      <projectNameList>\n" + 
							"        <string>"+jenkinsCopyFileJob+"</string>\n" + 
							"      </projectNameList>\n" + 
							"    </hudson.plugins.copyartifact.CopyArtifactPermissionProperty>\n" + 
							"    <hudson.plugins.svn__partial__release__mgr.ui.JobPropertyImpl plugin=\"svn-partial-release-mgr@1.0.1\"/>\n" + 
							"  </properties>\n" + 
							"  <scm class=\"hudson.scm.NullSCM\"/>\n" + 
							"  <assignedNode>"+node+"</assignedNode>\n" + 
							"  <canRoam>false</canRoam>\n" + 
							"  <disabled>false</disabled>\n" + 
							"  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>\n" + 
							"  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>\n" + 
							"  <triggers/>\n" + 
							"  <concurrentBuild>false</concurrentBuild>\n" + 
							"  <builders>\n" + 
							"    <hudson.plugins.copyartifact.CopyArtifact plugin=\"copyartifact@1.44\">\n" + 
							"      <project>"+jenkinsCopyFileJob+"</project>\n" + 
							"      <filter>"+pName+"</filter>\n" + 
							"      <target></target>\n" + 
							"      <excludes></excludes>\n" + 
							"      <selector class=\"hudson.plugins.copyartifact.WorkspaceSelector\"/>\n" + 
							"      <doNotFingerprintArtifacts>false</doNotFingerprintArtifacts>\n" + 
							"    </hudson.plugins.copyartifact.CopyArtifact>\n" + 
							"    <sp.sd.fileoperations.FileOperationsBuilder plugin=\"file-operations@1.9\">\n" + 
							"      <fileOperations>\n" + 
							"        <sp.sd.fileoperations.FolderCopyOperation>\n" + 
							"          <sourceFolderPath>${WORKSPACE}/Framework</sourceFolderPath>\n" + 
							"          <destinationFolderPath>${WORKSPACE}/"+pName+"</destinationFolderPath>\n" + 
							"        </sp.sd.fileoperations.FolderCopyOperation>\n" + 
							"      </fileOperations>\n" + 
							"    </sp.sd.fileoperations.FileOperationsBuilder>\n" + 
							"  </builders>\n" + 
							"  <publishers/>\n" + 
							"  <buildWrappers/>\n" + 
							"</project>";
				}
				jenkins.createJob("file_transfer", fromTMTJenkinsCopyXml, true);
//				System.out.println("New job created.....");

				help = new JenkinsTriggerHelper(jenkins);
				help.triggerJobAndWaitUntilFinished("file_transfer");
//				System.out.println("New job built.....");
				
				status="File transfer job built and run..";
			} catch (Exception e) {
				jenkins.updateJob("file_transfer", fromTMTJenkinsCopyXml, true);
//				System.out.println("Job updated.....");
			}
			help = new JenkinsTriggerHelper(jenkins);
			//isCancelled = (help.triggerJobAndWaitUntilFinished("file_transfer").getResult()).toString();
//			System.out.println("Job built.....");

			status="File transfer job updated and run..";
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return automationMaster;
		
	}
	public String checkForUpdateFileTransfer(Integer endUserSystemID,Integer projectID) {
		UpdateLogs ul=new UpdateLogs();
		String status="DonotUpdate";
		try {
			ul=updateLogsRepository.findByProjectIdAndEndusersystemId(projectID, endUserSystemID);
				
			if(ul!=null) {
				Date sheetUptDt=ul.getLastModified();
				Date enduserUptDt=ul.getLastUpdated();
				if(sheetUptDt.compareTo(enduserUptDt)>0) {
					status="Update";
				}
			}
			else {
				status="Update";
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return status;
		
	}
	
	public void updateSystemLogsTable(Integer endUserSystemID,Integer projectID) {
		List<UpdateLogs> ulList=null;
		Date projectUpdateTime = null;
		Date dt=new Date();
		
		try {
			ulList=updateLogsRepository.findByProjectId(projectID);
			
			if(ulList.get(0)!=null) {
				projectUpdateTime=ulList.get(0).getLastModified();
			}
			
			for(UpdateLogs ul : ulList) {
				if(ul.getEndusersystemId()==endUserSystemID) {
					ul.setLastUpdated(dt);
					updateLogsRepository.save(ul);
					break;
				}
				else if(ul.getEndusersystemId()==null) {
					ul.setEndusersystemId(endUserSystemID);
					ul.setLastUpdated(dt);
					updateLogsRepository.save(ul);
					break;
				}
				else {
					UpdateLogs ulnew=new UpdateLogs();
					ulnew.setProjectId(projectID);
					ulnew.setLastModified(projectUpdateTime);
					ulnew.setEndusersystemId(endUserSystemID);
					ulnew.setLastUpdated(dt);
					updateLogsRepository.save(ulnew);
					break;
				}
			}
			//return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			//return false;
		}
		
		
	}

//zip the project folder for transfer to slave

	List<String> filesListInDir = new ArrayList<String>();

	public void populateFilesList(File dir) throws IOException {
		File[] files = dir.listFiles();
		for (File file : files) {
			if (file.isFile()) {
				filesListInDir.add(file.getAbsolutePath());
			} else {
				populateFilesList(file);
			}
		}
	}

	public String zipDirectory(File dir, String zipDirName) {
		String status = "zipped";
		try {
			populateFilesList(dir);
			FileOutputStream fos = new FileOutputStream(zipDirName);
			ZipOutputStream zos = new ZipOutputStream(fos);
			for (String filePath : filesListInDir) {
//				System.out.println("Zipping " + filePath); // for ZipEntry we need to keep only relative file path, so
															// we used substring on absolute path
				ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length() + 1, filePath.length()));
				zos.putNextEntry(ze);
				FileInputStream fis = new FileInputStream(filePath);
				byte[] buffer = new byte[1024];
				int len;
				while ((len = fis.read(buffer)) > 0) {
					zos.write(buffer, 0, len);
				}
				zos.closeEntry();
				fis.close();
			}
			zos.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
			status = "not zipped";
		}
		return status;
	}

	//04_05_2020
	public void copyDir(String src, String dest, boolean overwrite) {
	    try {
//	    	System.out.println("able to execute "+Files.isWritable(Paths.get(dest)));
//	    	System.out.println("able to execute "+Files.isWritable(Paths.get(src)));
	        Files.walk(Paths.get(src)).forEach(a -> {
	            Path b = Paths.get(dest, a.toString().substring(src.length()));
	            try {
	                if (!a.toString().equals(src))
	                    Files.copy(a, b, overwrite ? new CopyOption[]{StandardCopyOption.REPLACE_EXISTING} : new CopyOption[]{});
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        });
	    } catch (IOException e) {
	        //permission issue
	        e.printStackTrace();
	    }
	}
	
	public void copyFileUsingStream(File source, File dest) throws IOException {
	    InputStream is = null;
	    OutputStream os = null;
	    try {
	        is = new FileInputStream(source);
	        os = new FileOutputStream(dest);
	        byte[] buffer = new byte[1024];
	        int length;
	        while ((length = is.read(buffer)) > 0) {
	            os.write(buffer, 0, length);
	        }
	    } finally {
	        is.close();
	        os.close();
	    }
	}
	
	public String transferToSlave(String slave, String projectName, String slaveworkspace) {
		JenkinsServer jenkins = null;
		JenkinsTriggerHelper help = null;
		String isCancelled = "CANCELLED";
		//BuildWithDetails build = new BuildWithDetails();

		String slaveCopyXml = "<?xml version='1.1' encoding='UTF-8'?>\r\n" + "<project>\r\n" + "  <actions/>\r\n"
				+ "  <description></description>\r\n" + "  <keepDependencies>false</keepDependencies>\r\n"
				+ "  <properties/>\r\n" + "  <scm class=\"hudson.scm.NullSCM\"/>\r\n" + "  <assignedNode>" + slave
				+ "</assignedNode>\r\n" + "  <canRoam>false</canRoam>\r\n" + "  <disabled>false</disabled>\r\n"
				+ "  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>\r\n"
				+ "  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>\r\n" + "  <triggers/>\r\n"
				+ "  <concurrentBuild>false</concurrentBuild>\r\n" + "  <builders>\r\n"
				+ "    <sp.sd.fileoperations.FileOperationsBuilder plugin=\"file-operations@1.7\">\r\n"
				+ "      <fileOperations>\r\n" + "        <sp.sd.fileoperations.FileUnZipOperation>\r\n"
				+ "          <filePath>" + projectName + ".zip</filePath>\r\n"
				+ "          <targetLocation>.</targetLocation>\r\n"
				+ "        </sp.sd.fileoperations.FileUnZipOperation>\r\n"
				+ "        <sp.sd.fileoperations.FolderCopyOperation>\r\n" + "          <sourceFolderPath>"
				+ slaveworkspace + "/Framework</sourceFolderPath>\r\n"
				+ "          <destinationFolderPath>${WORKSPACE}/" + projectName + "</destinationFolderPath>\r\n"
				+ "        </sp.sd.fileoperations.FolderCopyOperation>\r\n"
				+ "        <sp.sd.fileoperations.FolderCopyOperation>\r\n" + "          <sourceFolderPath>${WORKSPACE}/"
				+ projectName + "</sourceFolderPath>\r\n" + "          <destinationFolderPath>" + slaveworkspace + "/"
				+ projectName + "</destinationFolderPath>\r\n"
				+ "        </sp.sd.fileoperations.FolderCopyOperation>\r\n" + "      </fileOperations>\r\n"
				+ "    </sp.sd.fileoperations.FileOperationsBuilder>\r\n" + "  </builders>\r\n" + "  <publishers/>\r\n"
				+ "  <buildWrappers>\r\n"
				+ "    <com.michelin.cio.hudson.plugins.copytoslave.CopyToSlaveBuildWrapper plugin=\"copy-to-slave@1.4.4\">\r\n"
				+ "      <includes>ICICI.zip</includes>\r\n" + "      <excludes></excludes>\r\n"
				+ "      <flatten>false</flatten>\r\n" + "      <includeAntExcludes>false</includeAntExcludes>\r\n"
				+ "      <hudsonHomeRelative>false</hudsonHomeRelative>\r\n"
				+ "      <relativeTo>userContent</relativeTo>\r\n"
				+ "    </com.michelin.cio.hudson.plugins.copytoslave.CopyToSlaveBuildWrapper>\r\n"
				+ "  </buildWrappers>\r\n" + "</project>";
		try {
			jenkins = new JenkinsServer(new URI(jenkinsUrl), penkinsUser, jenkinsPwd);
			try {
				jenkins.createJob("file_transfer", slaveCopyXml, true);
//				System.out.println("New job created.....");
//jenkins.getJobs().get("file_transfer").details().build(true);

				help = new JenkinsTriggerHelper(jenkins);
				help.triggerJobAndWaitUntilFinished("file_transfer");
//System.out.println("New job built.....");
			} catch (Exception e) {
				jenkins.updateJob("file_transfer", slaveCopyXml, true);
//				System.out.println("Job updated.....");
//jenkins.getJobs().get("file_transfer").details().build(true);
//System.out.println("Job built.....");
			}
			help = new JenkinsTriggerHelper(jenkins);
			isCancelled = (help.triggerJobAndWaitUntilFinished("file_transfer").getResult()).toString();
//			System.out.println("Job built.....");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return isCancelled;
	}

	
	 public static void main(String args[]) throws IOException, URISyntaxException
	  { //copyDir(dependencyPath, "C:/Program Files (x86)/Jenkins/workspace/ICICI",true);
		 AutomationScriptService a=new AutomationScriptService();
		 a.getTestSuitesName("Autotest1");
		 
		 String projectName="ICICI";
		 String slaveworkspace="D:/Lipu";
		 String slave="TMT_Node";
		 String slaveCopyXml = "<?xml version='1.1' encoding='UTF-8'?>\r\n" + "<project>\r\n" + "  <actions/>\r\n"
					+ "  <description></description>\r\n" + "  <keepDependencies>false</keepDependencies>\r\n"
					+ "  <properties/>\r\n" + "  <scm class=\"hudson.scm.NullSCM\"/>\r\n" + "  <assignedNode>" + slave
					+ "</assignedNode>\r\n" + "  <canRoam>false</canRoam>\r\n" + "  <disabled>false</disabled>\r\n"
					+ "  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>\r\n"
					+ "  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>\r\n" + "  <triggers/>\r\n"
					+ "  <concurrentBuild>false</concurrentBuild>\r\n" + "  <builders>\r\n"
					+ "    <sp.sd.fileoperations.FileOperationsBuilder plugin=\"file-operations@1.7\">\r\n"
					+ "      <fileOperations>\r\n" + "        <sp.sd.fileoperations.FileUnZipOperation>\r\n"
					+ "          <filePath>" + projectName + ".zip</filePath>\r\n"
					+ "          <targetLocation>.</targetLocation>\r\n"
					+ "        </sp.sd.fileoperations.FileUnZipOperation>\r\n"
					+ "        <sp.sd.fileoperations.FolderCopyOperation>\r\n" + "          <sourceFolderPath>"
					+ slaveworkspace + "/Framework</sourceFolderPath>\r\n"
					+ "          <destinationFolderPath>${WORKSPACE}/" + projectName + "</destinationFolderPath>\r\n"
					+ "        </sp.sd.fileoperations.FolderCopyOperation>\r\n"
					+ "        <sp.sd.fileoperations.FolderCopyOperation>\r\n" + "          <sourceFolderPath>${WORKSPACE}/"
					+ projectName + "</sourceFolderPath>\r\n" + "          <destinationFolderPath>" + slaveworkspace + "/"
					+ projectName + "</destinationFolderPath>\r\n"
					+ "        </sp.sd.fileoperations.FolderCopyOperation>\r\n" + "      </fileOperations>\r\n"
					+ "    </sp.sd.fileoperations.FileOperationsBuilder>\r\n" + "  </builders>\r\n" + "  <publishers/>\r\n"
					+ "  <buildWrappers>\r\n"
					+ "    <com.michelin.cio.hudson.plugins.copytoslave.CopyToSlaveBuildWrapper plugin=\"copy-to-slave@1.4.4\">\r\n"
					+ "      <includes>ICICI.zip</includes>\r\n" + "      <excludes></excludes>\r\n"
					+ "      <flatten>false</flatten>\r\n" + "      <includeAntExcludes>false</includeAntExcludes>\r\n"
					+ "      <hudsonHomeRelative>false</hudsonHomeRelative>\r\n"
					+ "      <relativeTo>userContent</relativeTo>\r\n"
					+ "    </com.michelin.cio.hudson.plugins.copytoslave.CopyToSlaveBuildWrapper>\r\n"
					+ "  </buildWrappers>\r\n" + "</project>";
		  //JenkinsServer jenkins = new JenkinsServer(new URI("http://192.168.1.104:8080/"), "admin", "admin");
		  //jenkins.getJobs().get("copy_to_linux_slave").details().build(true);
		  //jenkins.createJob("file_transfer", slaveCopyXml, true);
//		  System.out.println(jenkins.getJobXml("TestConnection"));
		 //getSlaveMechine();
	  }
	 
}