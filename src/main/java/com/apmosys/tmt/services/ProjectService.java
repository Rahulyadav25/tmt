package com.apmosys.tmt.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.SessionFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.apmosys.tmt.BO.AssiginRunBo;
import com.apmosys.tmt.BO.CreateRunBo;
import com.apmosys.tmt.BO.DefectStatusBo;
import com.apmosys.tmt.BO.MannualReRunDetailsByRnd;
import com.apmosys.tmt.BO.MannualRunDetailsData;
import com.apmosys.tmt.BO.MannualTestGroupAndCase;
import com.apmosys.tmt.BO.OverallRunActionStatus;
import com.apmosys.tmt.BO.OverallRunStatus;
import com.apmosys.tmt.BO.RedmineDTO;
import com.apmosys.tmt.BO.ResourceDateWiseExecution;
import com.apmosys.tmt.BO.ResourceRequestBo;
import com.apmosys.tmt.BO.ResourceResponseBo;
import com.apmosys.tmt.BO.UploadTestCaseBo;
import com.apmosys.tmt.BO.resourceexecution;
import com.apmosys.tmt.dao.AttachmentDao;
import com.apmosys.tmt.dao.DefectDao;
import com.apmosys.tmt.dao.MannualExecutionLogsDao;
import com.apmosys.tmt.dao.MannualReRunDao;
import com.apmosys.tmt.dao.MannualRunDao;
import com.apmosys.tmt.dao.MannualTestCaseDao;
import com.apmosys.tmt.dao.MannualTestGroupDAO;
import com.apmosys.tmt.dao.MannualTestSuitesDAO;
import com.apmosys.tmt.dao.ProjectDao;
import com.apmosys.tmt.dao.ProjectDocumentDao;
import com.apmosys.tmt.dao.ResourceDao;
import com.apmosys.tmt.models.AccessDetail;
import com.apmosys.tmt.models.AttachmentFetch;
import com.apmosys.tmt.models.AutomationMetadata;
import com.apmosys.tmt.models.AutomationTestCaseExecutionLogs;
//import com.apmosys.tmt.models.AutomationTestCaseExecutionLogs;
import com.apmosys.tmt.models.BugDetailsEntity;
import com.apmosys.tmt.models.BugTrackorToolEntity;
import com.apmosys.tmt.models.DefectsDeatils;
import com.apmosys.tmt.models.IntegratedBugToolEntity;
import com.apmosys.tmt.models.MannualReRunTable;
import com.apmosys.tmt.models.MannualRunTable;
import com.apmosys.tmt.models.MannualTestCaseExecutionLogs;
import com.apmosys.tmt.models.MannualTestCaseTable;
import com.apmosys.tmt.models.MannualTestGroup;
import com.apmosys.tmt.models.MannualTestSuites;
import com.apmosys.tmt.models.OverViewTestSuitesDeatils;
import com.apmosys.tmt.models.OverviewProjectDetails;
import com.apmosys.tmt.models.ProjectAccessEntity;
import com.apmosys.tmt.models.ProjectDeatilsMeta;
import com.apmosys.tmt.models.ProjectDocument;
import com.apmosys.tmt.models.Projectwisedetail;
import com.apmosys.tmt.models.ResouecesData;
import com.apmosys.tmt.models.RobosoftJiraAutomationModel;
import com.apmosys.tmt.models.StatusDefectGraph;
import com.apmosys.tmt.models.Testrunwisedetails;
import com.apmosys.tmt.models.UpdateLogs;
import com.apmosys.tmt.models.UserDetailsEntity;
import com.apmosys.tmt.models.defectseverity;
import com.apmosys.tmt.models.projectwisefailstatus;
import com.apmosys.tmt.models.runwisestatus;
import com.apmosys.tmt.models.userRole;
//import com.apmosys.tmt.repository.AutomationExecutionLogsRepo;
import com.apmosys.tmt.repository.AutomationMetaRepository;
import com.apmosys.tmt.repository.BugDetailsRepository;
import com.apmosys.tmt.repository.BugTrackerRepository;
import com.apmosys.tmt.repository.IntegratedBugToolRepository;
import com.apmosys.tmt.repository.MannualExecutionLogsRepo;
import com.apmosys.tmt.repository.MannualRunRepository;
import com.apmosys.tmt.repository.MannualTestCaseRepository;
import com.apmosys.tmt.repository.MannualTestCaselogsRepository;
import com.apmosys.tmt.repository.MannualTestGroupRepo;
import com.apmosys.tmt.repository.MannualTestSuitesRepo;
import com.apmosys.tmt.repository.ProjectAccessRepository;
import com.apmosys.tmt.repository.ProjectRepository;
import com.apmosys.tmt.repository.UpdateLogsRepository;
import com.apmosys.tmt.repository.UserDetailsRepository;
import com.apmosys.tmt.repository.UserFailedLoginsRepo;
import com.apmosys.tmt.repository.configurationRepository;
import com.apmosys.tmt.utils.ConstantParameter;
import com.apmosys.tmt.utils.ServiceResponse;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

import jakarta.servlet.http.HttpSession;

@Service
public class ProjectService {

	@Value("${fileLocationPath}")
	private String attachmentFolderPath;
	@Value("${templateFile}")
	String templateFile;
	@Value("${temporaryPath}")
	String temporaryPath;
	@Value("${automationPath}")
	private String autoProjectPath;
	@Autowired
	private HttpSession session;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ProjectDao projectDao;
	@Autowired
	MannualTestCaseDao mannualTestCaseDao;
	@Autowired
	MannualTestSuitesDAO mannualTestSuitesDAO;
	@Autowired
	MannualTestGroupDAO mannualTestGroupDAO;
	@Autowired
	MannualReRunDao mannualReRunDao;
	@Autowired
	MannualRunDao mannualRunDao;
	@Autowired
	MannualExecutionLogsDao mannualExecutionLogsDao;

	@Autowired
	public configurationRepository congifrepo;
	@Autowired
	private ProjectDocumentDao projectDocumentDao;
	@Autowired
	private MannualTestCaseRepository mannualTestCaseRepo;
	@Autowired
	private MannualTestCaselogsRepository mannualTestCaselogsRepo;
	@Autowired
	private BugZillaService bugZillaService;
	@Autowired
	private AttachmentDao attachmentDao;
	@Autowired
	private JiraService jiraService;
//	@Autowired
//	private AutoJiraService autoJiraService;
	@Autowired
	private MantisService mantisService;
	@Autowired
	private MannualRunRepository mannualRunRepository;

	@Autowired
	private UserDetailsRepository userRepository;

	@Autowired
	private BugTrackerRepository bugRepo;

	@Autowired
	private MannualExecutionLogsRepo executionLogsRepo;

//	@Autowired
//	private AutomationExecutionLogsRepo automationExecutionLogsRepo;

	@Autowired
	private ProjectAccessRepository projectAccessRepository;

	@Autowired
	private MannualTestGroupRepo mannualTestGroupRepo;

	@Autowired
	private MannualTestSuitesRepo mannualTestSuitesDRepo;

	@Autowired
	private BugDetailsRepository bugDetailsRepo;

	@Autowired
	private IntegratedBugToolRepository integratedBugToolRepo;

	@Autowired
	private DefectDao defectDao;

	@Autowired
	private ResourceDao resourceDao;

	@Autowired
	private AutomationMetaRepository automationMetaRepository;

	@Autowired
	private UpdateLogsRepository updateLogsRepository;

	@Autowired
	private MantisServiceBajaj mantisServiceBajaj;

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	private UserFailedLoginsRepo userFailedLoginsRepo;

	@Autowired
	private RedmineService redmineService;

	public ServiceResponse updateBugDetailsAPI(String projectName, Integer projectID) {
		ServiceResponse response = new ServiceResponse();
		List<BugDetailsEntity> bugs = new ArrayList<BugDetailsEntity>();
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			bugs = bugDetailsRepo.findByProjectID(projectID);
			if (!bugs.isEmpty()) {
				ProjectDeatilsMeta projectDeatils = projectDao.getProjectDetails(projectID);
				if (StringUtils.isNotEmpty(projectDeatils.getToolName())
						&& StringUtils.isNotEmpty(projectDeatils.getBugToolURL())) {

					IntegratedBugToolEntity tool = integratedBugToolRepo
							.findByBugToolName(projectDeatils.getToolName());

					if (tool != null) {

						for (BugDetailsEntity b : bugs) {
							try {
								// //System.out.println("bug id: "+b.getBugID() +" priority:
								// "+b.getBugPriority());
								if (tool.getToolid() == b.getBugtoolID()) {
									if (("Bugzilla").equals(tool.getBugToolName())) {
										BugDetailsEntity apiBugStatus = bugZillaService.getBugStatus(projectName,
												projectID, b.getBugID());
										b.setBugStatus(apiBugStatus.getBugStatus());
										b.setBugSeverity(apiBugStatus.getBugSeverity());
										b.setBugPriority(apiBugStatus.getBugPriority());
										b.setUpdatedBy(detailsEntity.getId());
										bugDetailsRepo.save(b);
									} else if (("Mantis").equals(tool.getBugToolName())) {
										BugDetailsEntity apiBugStatus = mantisService.getBugStatus(projectName,
												projectID, b.getBugID(), projectDeatils);
										b.setBugStatus(apiBugStatus.getBugStatus());
										b.setBugSeverity(apiBugStatus.getBugSeverity());
										b.setBugPriority(apiBugStatus.getBugPriority());
										b.setUpdatedBy(detailsEntity.getId());
										bugDetailsRepo.save(b);
									} // else if
									else if (("Jira").equalsIgnoreCase(tool.getBugToolName())) {
										BugDetailsEntity apiBugStatus = jiraService.getBugStatus(projectName, projectID,
												b.getBugID(), projectDeatils);
										b.setBugStatus(apiBugStatus.getBugStatus());
										b.setBugPriority(apiBugStatus.getBugPriority());
										b.setUpdatedBy(detailsEntity.getId());
										bugDetailsRepo.save(b);
									} // else if
								}
							} // inner try
							catch (Exception e) {
								e.printStackTrace();
							} // inner catch
						} // for

					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public ServiceResponse getProjectBugStatus(String projectName, Integer projectID) {
		ServiceResponse response = new ServiceResponse();
		List<MannualTestCaseTable> testcases = new ArrayList<MannualTestCaseTable>();
		List<MannualTestCaseExecutionLogs> testlogs = new ArrayList<MannualTestCaseExecutionLogs>();
		try {
			ProjectDeatilsMeta projectDeatils = projectDao.getProjectDetails(projectID);
			if (StringUtils.isNotEmpty(projectDeatils.getToolName())
					&& StringUtils.isNotEmpty(projectDeatils.getBugToolURL())) {

				testcases = mannualTestCaseDao.getAllTestCasesByProjectID(projectID);
				if (!testcases.isEmpty()) {
					if (("Mantis").equalsIgnoreCase(projectDeatils.getToolName())) {
						for (MannualTestCaseTable tc : testcases) {
							/*
							 * String apiBugStatus=mantisService.getBugStatus(projectName, projectID,
							 * tc.getBugID(), projectDeatils); if(StringUtils.isNotEmpty(apiBugStatus)) {
							 * //tc.setBugStatus(apiBugStatus);
							 * mannualTestCaseRepo.updateBugStatus(apiBugStatus, tc.getSrID()); }
							 */
							BugDetailsEntity apiBugStatus = mantisService.getBugStatus(projectName, projectID,
									tc.getBugID(), projectDeatils);
							if (apiBugStatus != null) {
								mannualTestCaseRepo.updateBugDetails(apiBugStatus.getBugStatus(),
										apiBugStatus.getBugSeverity(), apiBugStatus.getBugPriority(), tc.getSrID());
							}
						}
					} else if (("Bugzilla").equalsIgnoreCase(projectDeatils.getToolName())) {
						for (MannualTestCaseTable tc : testcases) {
							BugDetailsEntity apiBugStatus = bugZillaService.getBugStatus(projectName, projectID,
									tc.getBugID().toString());
							if (apiBugStatus != null) {
								// tc.setBugStatus(apiBugStatus);
								mannualTestCaseRepo.updateBugDetails(apiBugStatus.getBugStatus(),
										apiBugStatus.getBugSeverity(), apiBugStatus.getBugPriority(), tc.getSrID());
							}
						}
					}
					// mannualTestCaseRepo.save(testcases);
				}

				testlogs = mannualExecutionLogsDao.getAllExecutedTestCasesByProjectID(projectID);
				if (!testlogs.isEmpty()) {
					if (("Mantis").equalsIgnoreCase(projectDeatils.getToolName())) {
						for (MannualTestCaseExecutionLogs tc : testlogs) {
							BugDetailsEntity apiBugStatus = mantisService.getBugStatus(projectName, projectID,
									tc.getBugId(), projectDeatils);
							if (apiBugStatus != null) {
								mannualTestCaselogsRepo.updateBugDetails(apiBugStatus.getBugStatus(),
										apiBugStatus.getBugSeverity(), apiBugStatus.getBugPriority(), tc.getId());
							}
						}
					} else if (("Bugzilla").equalsIgnoreCase(projectDeatils.getToolName())) {
						for (MannualTestCaseExecutionLogs tc : testlogs) {
							/*
							 * @ChangesBy --> Animesh adding only one if condition to check if bugId is null
							 * dont proceed
							 */
							// //System.out.println(" this needs to be written: "+tc.getBugId());
							if (!tc.getBugId().equalsIgnoreCase("NA") && !tc.getBugId().equalsIgnoreCase("null")) {
								// System.out.println(" inside if : "+tc.getBugId());
								BugDetailsEntity apiBugStatus = bugZillaService.getBugStatus(projectName, projectID,
										tc.getBugId());
								if (apiBugStatus != null) {
									// tc.setBugStatus(apiBugStatus);
									mannualTestCaselogsRepo.updateBugDetails(apiBugStatus.getBugStatus(),
											apiBugStatus.getBugSeverity(), apiBugStatus.getBugPriority(), tc.getId());
								} // if

							} // if
						} // for
							// //System.out.println("done .. getProjectBugStatus");

					}
					// mannualTestCaselogsRepo.save(testlogs);
				}
			}

			response.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
			response.setServiceResponse("Success");
			response.setServiceError("");
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceError("Something went wrong");
			response.setServiceStatus(ServiceResponse.STATUS_FAIL);
		}
		updateBugDetailsAPI(projectName, projectID);
		return response;

	}

	public ServiceResponse getBugzillaBugsProductWise(String projectName, Integer projectID) {
		ServiceResponse response = new ServiceResponse();
		List bugs = new ArrayList<>();
		try {
			ProjectDeatilsMeta projectDeatils = projectDao.getProjectDetails(projectID);
			if (projectDeatils.getToolName().equalsIgnoreCase("Bugzilla")) {
				bugs.add("Bugzilla");
				bugs.add(bugZillaService.getAllBugs(projectName, projectID));
				bugs.add(projectDeatils);
			} else if (projectDeatils.getToolName().equalsIgnoreCase("Mantis")) {
				bugs.add("Mantis");
				bugs.add(mantisService.getProjectBugs(projectName, projectID));
				bugs.add(projectDeatils);
			} else if (projectDeatils.getToolName().equalsIgnoreCase("Jira")) {
				bugs.add("Jira");
				bugs.add(jiraService.getProjectBugs(projectName, projectID));
				bugs.add(projectDeatils);
			} else if (projectDeatils.getToolName().equalsIgnoreCase("Redmine")) {
				bugs.add("Redmine");
				RedmineDTO dto = new RedmineDTO();
				dto.setIdentifier(projectName);
				dto.setTmtProjectID(projectID);
				bugs.add(redmineService.getProjectIssues(dto));
				bugs.add(projectDeatils);
			}
			/*
			 * /----------------------------------------------------------------------------
			 * -start for gettting else if
			 * (projectDeatils.getToolName().equalsIgnoreCase("Jira")) { bugs.add("Jira");
			 * bugs.add(jiraService.getProjectBugs(projectName, projectID));
			 * bugs.add(projectDeatils); }
			 */
			// -----------------------------------------------------------------------------start
			// for gettting
			response.setServiceStatus("200");
			response.setServiceResponse(bugs);
			response.setServiceError("");
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceResponse("Something went wrong");
			response.setServiceError(ServiceResponse.STATUS_FAIL);
		}

		return response;

	}

	public String createDirectory(ProjectDeatilsMeta uploadData) {
		String result = null;
		String desktopPath = System.getProperty("user.home") + File.separator + "Desktop";
		String rootFolder = desktopPath + File.separator + "AutomationTools" + File.separator
				+ uploadData.getProjectName();
		// System.out.println("temp" + rootFolder);
		File dir = new File(rootFolder);// creating folder
		if (!dir.exists()) {
			dir.mkdirs();
			result = rootFolder;
		}
		// TODO Auto-generated method stub
		return result;
	}

	public ServiceResponse saveProject(ProjectDeatilsMeta uploadData, int userid) {
		// System.out.println("save projecty" + uploadData);
		ServiceResponse response = new ServiceResponse();
		UserDetailsEntity roleEn = (UserDetailsEntity) session.getAttribute("user");
		Integer role = roleEn.getUser_role();
		Integer count = 0;
		String count1 = "";
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			if (role == 100 || role == 101) {
				if (uploadData.getProjectName() != null && uploadData.getApplicationName() != null
						&& uploadData.getProjectStartDate() != null) {
					if (uploadData.getProjectName().length() >= 50 && uploadData.getApplicationName().length() >= 150) {
						Status = "Application Name and Project Name are too long";
						response.setServiceStatus("400");
						response.setServiceResponse(Status);
					} else if (uploadData.getProjectName().length() >= 50) {
						Status = "Project Name is too long";
						response.setServiceStatus("400");
						response.setServiceResponse(Status);
					} else if (uploadData.getApplicationName().length() >= 50) {
						Status = "Application Name is too long";
						response.setServiceStatus("400");
						response.setServiceResponse(Status);
					} else {
						count = (Integer) projectDao.saveProjectMetaDetails(uploadData);
						count1 = congifrepo.giveProjectAccessCreate(uploadData.getProjectID(), userid,
								uploadData.getCreatedBy());
//						count1 = congifrepo.giveProjectAccessCreate(uploadData.getProjectID(), userid,
//								uploadData.getCreatedBy());
						if (count == 2) {
							Status = "Your project is already exists";
							response.setServiceStatus("400");
							response.setServiceResponse(Status);

						}

						else {
							Status = "Your Project has been created Successfully";
							response.setServiceStatus("200");
							response.setServiceResponse(Status);

						}

					}

				}

				else {
					Status = "Kindly filled all the required Field";
				}
			} else {
				response.setServiceResponse(Status);
				response.setServiceError("Do Not Edit Other user ");
				response.setServiceResponse(ServiceResponse.STATUS_FAIL);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Status = "Your project is already exists";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

			// TODO: handle exception
		}
		return response;

	}

	public ServiceResponse getAllProject() {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<ProjectDeatilsMeta> projectList = new ArrayList<>();
		try {
			projectList = projectDao.getAllProject();
			response.setServiceStatus("200");
			response.setServiceResponse(projectList);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getOverViewProjectDetails(int roleid, int userid) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<OverviewProjectDetails> projectList = new ArrayList<>();
		try {
			String who = congifrepo.userRole(roleid);
			List<AccessDetail> access = null;

			if (who.equals("Admin")) {
				projectList = projectDao.overViewProjectDetailsAdmin();
			} else {
				projectList = projectDao.overViewProjectDetails(userid);
			}

			response.setServiceStatus("200");
			response.setServiceResponse(projectList);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	/*
	 * public ServiceResponse uploadTestSuites(MultipartFile file, MannualTestSuites
	 * testsuites) { ServiceResponse response=new ServiceResponse(); Integer i=0;
	 * Map<String, List<MannualTestCaseTable>> map=new HashedMap<String,
	 * List<MannualTestCaseTable>>(); List<UploadTestCaseBo> testCaseList=new
	 * ArrayList<UploadTestCaseBo>(); List<MannualTestCaseTable>
	 * testCases=readManualTestCases(file); for (MannualTestCaseTable details1 :
	 * testCases) { if (details1 != null) {
	 * 
	 * List<MannualTestCaseTable> values = testCases.stream() .filter(x ->
	 * x.getTestGroupName().equals(details1.getTestGroupName()))
	 * .collect(Collectors.toList()); map.put(details1.getTestGroupName(), values);
	 * } } if(!map.isEmpty()) { MannualTestSuites
	 * suites=mannualTestSuitesDAO.createTestSuites(testsuites); for
	 * (Map.Entry<String, List<MannualTestCaseTable>> map1 : map.entrySet()) {
	 * MannualTestGroup testGroup=new MannualTestGroup();
	 * testGroup.setGroupName(map1.getKey()); testGroup.setIsActive("Y");
	 * testGroup.setCreatedBy(suites.getCreatedBy());
	 * testGroup.setTestSuitesId(suites.getTestSuitesId()); MannualTestGroup
	 * group=mannualTestGroupDAO.createTestGroup(testGroup); if(group!=null) {
	 * UploadTestCaseBo testCaseBo=new UploadTestCaseBo();
	 * testCaseBo.setTestGroupName(map1.getKey());
	 * testCaseBo.setTestGroupId(group.getTestGroupId());
	 * testCaseBo.setTestCases(map1.getValue()); testCaseList.add(testCaseBo); } }
	 * i=insertTestCase(testCaseList); } if(i==1) {
	 * response.setServiceResponse("Testcases Uploaded");
	 * response.setServiceStatus("200"); } else {
	 * response.setServiceResponse("Testcases not uploaded");
	 * response.setServiceStatus("400"); } return response; }
	 */

	public ServiceResponse uploadTestSuites(MultipartFile file, MannualTestSuites testsuites) {
		ServiceResponse response = new ServiceResponse();
		Integer i = 0;
		Map<String, List<MannualTestCaseTable>> map = new HashedMap<String, List<MannualTestCaseTable>>();
		List<UploadTestCaseBo> testCaseList = new ArrayList<UploadTestCaseBo>();
		try {
			System.out.println("ProjectService.uploadTestSuites()...........uploading 1");
			MannualTestSuites suite = null;
			suite = mannualTestSuitesDRepo.findByTestSuitesNameAndProjectIdAndIsDeleted(testsuites.getTestSuitesName(),
					testsuites.getProjectId(), "N");

			if (suite == null) {
				List<MannualTestCaseTable> testCases = readManualTestCases(file, testsuites.getTestSuitesName());
				System.out.println("ProjectService.uploadTestSuites()...........uploading 2");
				for (MannualTestCaseTable details1 : testCases) {
					if (details1 != null) {

						List<MannualTestCaseTable> values = testCases.stream()
								.filter(x -> x.getTestGroupName().equals(details1.getTestGroupName()))
								.collect(Collectors.toList());
						map.put(details1.getTestGroupName(), values);
					}
					System.out.println("ProjectService.uploadTestSuites()...........uploading 3");
				}
				if (!map.isEmpty()) {
					System.out.println("ProjectService.uploadTestSuites()...........uploading 4");
					MannualTestSuites suites = mannualTestSuitesDAO.createTestSuites(testsuites);
					for (Map.Entry<String, List<MannualTestCaseTable>> map1 : map.entrySet()) {
						MannualTestGroup testGroup = new MannualTestGroup();
						testGroup.setGroupName(map1.getKey());
						testGroup.setIsActive("Y");
						testGroup.setCreatedBy(suites.getCreatedBy());
						testGroup.setTestSuitesId(suites.getTestSuitesId());
						MannualTestGroup group = mannualTestGroupDAO.createTestGroup(testGroup);
						if (group != null) {
							UploadTestCaseBo testCaseBo = new UploadTestCaseBo();
							testCaseBo.setTestGroupName(map1.getKey());
							testCaseBo.setTestGroupId(group.getTestGroupId());
							testCaseBo.setTestCases(map1.getValue());
							testCaseList.add(testCaseBo);
						}
						System.out.println("ProjectService.uploadTestSuites()...........uploading 5");
					}
					i = insertTestCase(testCaseList);
					System.out.println("ProjectService.uploadTestSuites()...........uploading 6 i=" + i);
				}
				if (i == 1) {
					response.setServiceResponse("Testcases Uploaded");
					response.setServiceStatus("200");
				} else {
					response.setServiceResponse("Testcases not uploaded");
					response.setServiceStatus("400");
				}
				// System.out.println("kindly check 1");
			} else {
				// System.out.println("testsuites with doc......getTestSuitesName()..." +
				// testsuites.getTestSuitesName()
				// + ".. is deleted.." + suite.getIsDeleted());

				System.out.println("ProjectService.uploadTestSuites()...........uploading 7 i=" + i);
				if (suite.getIsDeleted().equals("Y")) {
					// System.out.println("... tec doc 1");
					List<MannualTestCaseTable> testCases = readManualTestCases(file, testsuites.getTestSuitesName());
					// System.out.println("... tec doc 2");

					for (MannualTestCaseTable details1 : testCases) {
						if (details1 != null) {
							// System.out.println("... tec doc 3");

							List<MannualTestCaseTable> values = testCases.stream()
									.filter(x -> x.getTestGroupName().equals(details1.getTestGroupName()))
									.collect(Collectors.toList());
							map.put(details1.getTestGroupName(), values);
						}
					}
					System.out.println("ProjectService.uploadTestSuites()...........uploading 8 ");
					if (!map.isEmpty()) {
						// System.out.println("... tec doc 4");

						MannualTestSuites suites = mannualTestSuitesDAO.createTestSuites(testsuites);
						// System.out.println("... tec doc 5");

						for (Map.Entry<String, List<MannualTestCaseTable>> map1 : map.entrySet()) {
							MannualTestGroup testGroup = new MannualTestGroup();
							testGroup.setGroupName(map1.getKey());
							testGroup.setIsActive("Y");
							testGroup.setCreatedBy(suites.getCreatedBy());
							testGroup.setTestSuitesId(suites.getTestSuitesId());
							MannualTestGroup group = mannualTestGroupDAO.createTestGroup(testGroup);
							if (group != null) {
								UploadTestCaseBo testCaseBo = new UploadTestCaseBo();
								testCaseBo.setTestGroupName(map1.getKey());
								testCaseBo.setTestGroupId(group.getTestGroupId());
								testCaseBo.setTestCases(map1.getValue());
								testCaseList.add(testCaseBo);
							}
							System.out.println("ProjectService.uploadTestSuites()...........uploading 9 ");
						}
						i = insertTestCase(testCaseList);
					}
					if (i == 1) {
						response.setServiceResponse("Testcases Uploaded");
						response.setServiceStatus("200");
					} else {
						response.setServiceResponse("Testcases not uploaded");
						response.setServiceStatus("400");
					}
				} else {
					suite.setCheckForTestSuite("TestSuite already exists");
					System.out.println("ProjectService.uploadTestSuites()...........uploading 10 ");
				}
				// System.out.println("Kindly check ");

			}
		} catch (Exception e) {
			System.out.println("ProjectService.uploadTestSuites()...........uploading 11");
			e.printStackTrace();
			// System.out.println("12...");
			response.setServiceStatus("400");
			response.setServiceResponse("TestSuite already exists");

			// TODO: handle exception
		}
		return response;

	}

	public Integer insertTestCase(List<UploadTestCaseBo> testCaseList) {
		Integer i = 0;
		for (UploadTestCaseBo uploadTestCaseBo : testCaseList) {
			for (MannualTestCaseTable testCase1 : uploadTestCaseBo.getTestCases()) {
				testCase1.setTestGroupId(uploadTestCaseBo.getTestGroupId());
				try {
					mannualTestCaseDao.addNewTestId(testCase1);
					i = 1;
				} catch (DataIntegrityViolationException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
					// System.out.println("eee.." + e);
					// TODO: handle exception
				}
			}
		}
		return i;

	}

	/*
	 * public List<MannualTestCaseTable> readManualTestCases(MultipartFile file) {
	 * // TODO Auto-generated method stubWorkbook workbook=null; Workbook workbook =
	 * null; List<MannualTestCaseTable> dataBOsList = null; try { int v = 10;
	 * //System.out.println(v); DataFormatter dataFormatter = new DataFormatter();
	 * // FileInputStream fis=new FileInputStream(file.getInputStream());
	 * XSSFWorkbook myWorkBook = new XSSFWorkbook(file.getInputStream()); XSSFSheet
	 * mySheet = myWorkBook.getSheetAt(0); dataBOsList = new ArrayList<>();
	 * List<String> excelHeaderList = getHeaders(file); Object[] validateExcel =
	 * checkExeclHeader(excelHeaderList); if ((boolean) validateExcel[0]) {
	 * List<String> list = (List<String>) validateExcel[1];
	 * ////System.out.println(list); Map<String, Integer> headerSeq =
	 * assignHeaderSeqNumber(list); int first = 0; for (Row row : mySheet) {
	 * ////System.out.println(first); if (first == 0) { first = first + 1;
	 * 
	 * } else { MannualTestCaseTable caseDataBO = new MannualTestCaseTable(); for
	 * (Map.Entry<String, Integer> heading : headerSeq.entrySet()) { //int cellType
	 * = row.getCell(headerSeq.get(heading.getKey())).getCellType(); Integer
	 * headingCount=heading.getValue(); if (headingCount.equals(0)) {
	 * caseDataBO.setSrNo(dataFormatter.formatCellValue(row.getCell(headerSeq.get(
	 * heading.getKey())))); } else if (headingCount.equals(2)) {
	 * caseDataBO.setTestGroupName(dataFormatter.formatCellValue(row.getCell(
	 * headerSeq.get(heading.getKey())))); } else if(headingCount.equals(3)) {
	 * caseDataBO.setScenarioID(dataFormatter.formatCellValue(row.getCell(headerSeq.
	 * get(heading.getKey())))); } else if(headingCount.equals(4)) {
	 * caseDataBO.setScenarios(dataFormatter.formatCellValue(row.getCell(headerSeq.
	 * get(heading.getKey())))); } else if(headingCount.equals(5)) {
	 * caseDataBO.setFunctionality(dataFormatter.formatCellValue(row.getCell(
	 * headerSeq.get(heading.getKey())))); } else if(headingCount.equals(6)) {
	 * caseDataBO.setTestID(dataFormatter.formatCellValue(row.getCell(headerSeq.get(
	 * heading.getKey())))); } else if(headingCount.equals(7)) {
	 * caseDataBO.setTestDescription(dataFormatter.formatCellValue(row.getCell(
	 * headerSeq.get(heading.getKey())))); } else if(headingCount.equals(8)) {
	 * caseDataBO.setTestCaseType(dataFormatter.formatCellValue(row.getCell(
	 * headerSeq.get(heading.getKey())))); } else if(headingCount.equals(9)) {
	 * caseDataBO.setTestData(dataFormatter.formatCellValue(row.getCell(headerSeq.
	 * get(heading.getKey())))); } else if(headingCount.equals(10)) {
	 * caseDataBO.setSteps(dataFormatter.formatCellValue(row.getCell(headerSeq.get(
	 * heading.getKey())))); } else if(headingCount.equals(11)) {
	 * caseDataBO.setExpectedResult(dataFormatter.formatCellValue(row.getCell(
	 * headerSeq.get(heading.getKey())))); } else {
	 * 
	 * } //System.out.println(); } String serno= caseDataBO.getSrNo(); String
	 * testgroupname=caseDataBO.getTestGroupName(); String
	 * scenarioId=caseDataBO.getScenarioID(); String
	 * scenarios=caseDataBO.getScenarios(); String
	 * functionality=caseDataBO.getFunctionality(); String
	 * testId=caseDataBO.getTestID(); String
	 * testDescription=caseDataBO.getTestDescription(); String
	 * testCaseType=caseDataBO.getTestCaseType(); String
	 * testData=caseDataBO.getTestData(); String steps=caseDataBO.getSteps(); String
	 * exceptedResult=caseDataBO.getExpectedResult();
	 * 
	 * if(serno==null || serno.isEmpty() || serno.equals("")|| testgroupname==null
	 * || testgroupname.isEmpty() || testgroupname.equals("") || scenarioId==null ||
	 * scenarioId.isEmpty() || scenarioId.equals("") || scenarios==null ||
	 * scenarios.isEmpty() || scenarios.equals("") || functionality==null ||
	 * functionality.isEmpty() || functionality.equals("")|| testId==null ||
	 * testId.isEmpty() || testId.equals("") || testDescription==null ||
	 * testDescription.isEmpty() || testDescription.equals("") || testCaseType==null
	 * || testCaseType.isEmpty() || testCaseType.equals("")|| steps==null ||
	 * steps.isEmpty() || steps.equals("") || exceptedResult==null ||
	 * exceptedResult.isEmpty() || exceptedResult.equals("")) {
	 * 
	 * //System.out.println("Data is empty");
	 * 
	 * 
	 * } else {
	 * 
	 * //System.out.println(caseDataBO.getTesterName());
	 * dataBOsList.add(caseDataBO); } }
	 * 
	 * } } else { //System.out.println("Invalid Excel"); } } catch (Exception e) {
	 * e.printStackTrace(); } return dataBOsList; }
	 */

	public List<MannualTestCaseTable> readManualTestCases(MultipartFile file, String suiteNameFS) {
		// TODO Auto-generated method stubWorkbook workbook=null;
		Workbook workbook = null;
		List<MannualTestCaseTable> dataBOsList = null;
		try {
			int v = 10;
			// System.out.println(v);
			DataFormatter dataFormatter = new DataFormatter();
			// FileInputStream fis=new FileInputStream(file.getInputStream());
			Workbook myWorkBookXSSF = null;
			Workbook myWorkBookHSSF = null;
			Sheet mySheet = null;
			String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());

			if (fileExtension.equalsIgnoreCase("xlsx")) {
				myWorkBookXSSF = new XSSFWorkbook(file.getInputStream());
				mySheet = myWorkBookXSSF.getSheetAt(0);
			} else if (fileExtension.equalsIgnoreCase("xls")) {
				POIFSFileSystem fs = new POIFSFileSystem(file.getInputStream());
				myWorkBookHSSF = new HSSFWorkbook(fs);
				mySheet = myWorkBookHSSF.getSheetAt(0);
			}

			dataBOsList = new ArrayList<>();
			List<String> excelHeaderList = getHeaders(file);
			Object[] validateExcel = checkExeclHeader(excelHeaderList);
			if ((boolean) validateExcel[0]) {
				List<String> list = (List<String>) validateExcel[1];
				// //System.out.println(list);
				Map<String, Integer> headerSeq = assignHeaderSeqNumber(list);
				int first = 0;
				for (Row row : mySheet) {
					// //System.out.println(first);
					if (first == 0) {
						first = first + 1;

					} else {
						MannualTestCaseTable caseDataBO = new MannualTestCaseTable();
						for (Map.Entry<String, Integer> heading : headerSeq.entrySet()) {
							// int cellType = row.getCell(headerSeq.get(heading.getKey())).getCellType();
							Integer headingCount = heading.getValue();
							if (headingCount.equals(0)) {
								caseDataBO.setSrNo(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(1)) {
								caseDataBO.setTestSuitesName(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(2)) {
								caseDataBO.setTestGroupName(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(3)) {
								caseDataBO.setScenarioID(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(4)) {
								caseDataBO.setScenarios(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(5)) {
								caseDataBO.setFunctionality(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(6)) {
								caseDataBO.setTestID(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(7)) {
								caseDataBO.setTestDescription(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(8)) {
								caseDataBO.setTestCaseType(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
								// System.out.println("cs tects case type=====" + caseDataBO.getTestCaseType());
							} else if (headingCount.equals(9)) {
								caseDataBO.setTestData(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(10)) {
								caseDataBO.setSteps(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(11)) {
								caseDataBO.setExpectedResult(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
								// System.out.println("Expeneted data.");
							} else if (headingCount.equals(
									12)) {/*
											 * String tcbro=dataFormatter.formatCellValue(row.getCell(headerSeq.get(
											 * heading.getKey( )))); //System.out.println("broser..."+tcbro);
											 * caseDataBO.setBrowser(dataFormatter.formatCellValue(row.getCell(
											 * headerSeq.get (heading.getKey()))));
											 * //System.out.println(caseDataBO.getBrowser()+"broswer");
											 */
								caseDataBO.setActualRsult(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(13)) {
								String tcbro = dataFormatter
										.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));
								// System.out.println("broser..." + tcbro);
								caseDataBO.setBrowser(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
								// System.out.println(caseDataBO.getBrowser() + "broswer");
							} else {

							}
							// System.out.println();
						}
						String serno = caseDataBO.getSrNo();
						String testgroupname = caseDataBO.getTestGroupName();
						String scenarioId = caseDataBO.getScenarioID();// ""
						String scenarios = caseDataBO.getScenarios();
						String functionality = caseDataBO.getFunctionality();
						String testId = caseDataBO.getTestID();// ""
						String testDescription = caseDataBO.getTestDescription();// ""
						String testCaseType = caseDataBO.getTestCaseType();// ""
						String testData = caseDataBO.getTestData();// ""
						String steps = caseDataBO.getSteps();// ""
						String exceptedResult = caseDataBO.getExpectedResult();// ""

						if (serno == null || serno.isEmpty() || serno.equals("") || testgroupname == null
								|| testgroupname.isEmpty() || testgroupname.equals("") || scenarioId == null
								|| scenarioId.isEmpty() || scenarioId.equals("") || scenarios == null
								|| scenarios.isEmpty() || scenarios.equals("") || functionality == null
								|| functionality.isEmpty() || functionality.equals("") || testId == null
								|| testId.isEmpty() || testId.equals("") || testDescription == null
								|| testDescription.isEmpty() || testDescription.equals("") || testCaseType == null
								|| testCaseType.isEmpty() || testCaseType.equals("") || steps == null || steps.isEmpty()
								|| steps.equals("") || exceptedResult == null || exceptedResult.isEmpty()
								|| exceptedResult.equals("")) {

							// System.out.println("Data is empty");

						} else {
							// System.out.println(
							// ".................................( ^ _ 0
							// )...................................................................");
							// System.out.println("caseDataBo.getTestSuitesName......" +
							// caseDataBO.getTestSuitesName()
							// + " ...suiteNameFS..." + suiteNameFS);
							if (suiteNameFS.equals(caseDataBO.getTestSuitesName())
									|| suiteNameFS.equalsIgnoreCase(caseDataBO.getTestSuitesName()))

							{
								// System.out.println(caseDataBO.getTesterName());

								dataBOsList.add(caseDataBO);
							} else {
								// System.out.println("dont want same data");
							}
						}
					}

				}
			} else {
				// System.out.println("Invalid Excel");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dataBOsList;
	}

	public List<String> getHeaders(MultipartFile file) {
		XSSFWorkbook myWorkBook;
		List<String> headers = new ArrayList<String>();
		Workbook myWorkBookXSSF = null;
		Workbook myWorkBookHSSF = null;
		Sheet mySheet = null;
		String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());

		try {
			/*
			 * myWorkBook = new XSSFWorkbook(file.getInputStream()); XSSFSheet mySheet =
			 * myWorkBook.getSheetAt(0);
			 */
			if (fileExtension.equalsIgnoreCase("xlsx")) {
				myWorkBookXSSF = new XSSFWorkbook(file.getInputStream());
				mySheet = myWorkBookXSSF.getSheetAt(0);
			} else if (fileExtension.equalsIgnoreCase("xls")) {
				POIFSFileSystem fs = new POIFSFileSystem(file.getInputStream());
				myWorkBookHSSF = new HSSFWorkbook(fs);
				mySheet = myWorkBookHSSF.getSheetAt(0);
			}
			Row headerRow = mySheet.getRow(0);
			Iterator<Cell> cells = headerRow.cellIterator();
			while (cells.hasNext()) {
				Cell cell = (Cell) cells.next();
				RichTextString value = cell.getRichStringCellValue();
				if (value.getString() != null && !value.getString().equals(""))
					headers.add(value.getString());
				else
					break;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return headers;

	}

	public Object[] checkExeclHeader(List<String> headers) {
		Boolean isRightExcel = true;
		String[] preDefinedHeader = ConstantParameter.MannualExcelHeaderList;
		List<String> finalHaeding = new ArrayList<String>();
		ArrayList<String> preDefinedHeaderList = new ArrayList<String>(Arrays.asList(preDefinedHeader));
		for (String header : preDefinedHeaderList) {
			if (!headers.contains(header)) {
				isRightExcel = false;
				break;
			} else {
				finalHaeding.add(header);
			}
		}
		Object[] object = new Object[2];
		object[0] = isRightExcel;
		object[1] = finalHaeding;

		return object;

	}

	private Map<String, Integer> assignHeaderSeqNumber(List<String> headers) {
		Map<String, Integer> headerSeqNumber = new HashMap<String, Integer>();

		int i = 0;
		for (String columnName : headers) {
			headerSeqNumber.put(columnName, i);
			i++;
		}

		return headerSeqNumber;

	}

	/*
	 * public ServiceResponse importTestCases(MultipartFile file, String groupId,
	 * String uploadType) { ServiceResponse response = new ServiceResponse();
	 * Integer i = 0; List<UploadTestCaseBo> testCaseList = new
	 * ArrayList<UploadTestCaseBo>(); List<MannualTestCaseTable> testCases =
	 * readManualTestCases(file);
	 * 
	 * try { Integer groupid = Integer.parseInt(groupId); MannualTestGroup testGroup
	 * = mannualTestGroupDAO.getTestGroupDetails(groupid); if
	 * (uploadType.equals("replace")) {
	 * mannualTestCaseDao.deleteTestCaseBygId(groupid); } if (testGroup != null) {
	 * List<MannualTestCaseTable> values = testCases.stream() .filter(x ->
	 * x.getTestGroupName().equals(testGroup.getGroupName()))
	 * .collect(Collectors.toList());
	 * 
	 * UploadTestCaseBo testCaseBo = new UploadTestCaseBo();
	 * testCaseBo.setTestGroupId(groupid); testCaseBo.setTestCases(values);
	 * testCaseList.add(testCaseBo); i = insertTestCase(testCaseList); if (i == 1) {
	 * response.setServiceResponse("Testcases Uploaded");
	 * response.setServiceStatus("200"); } else {
	 * response.setServiceResponse("Unable to import TestCase");
	 * response.setServiceStatus("300"); } } } catch (Exception e) {
	 * e.printStackTrace();
	 * response.setServiceResponse("Unable to Process Your Request");
	 * response.setServiceStatus("400"); // TODO: handle exception } return
	 * response; }
	 */
	public ServiceResponse importTestCases(MultipartFile file, String groupId, String uploadType, String suiteNameFS) {
		ServiceResponse response = new ServiceResponse();
		Integer i = 0;
		List<UploadTestCaseBo> testCaseList = new ArrayList<UploadTestCaseBo>();
		List<MannualTestCaseTable> testCases = readManualTestCases(file, suiteNameFS);

		try {

			if (testCases.size() > 0) {
				Integer groupid = Integer.parseInt(groupId);
				MannualTestGroup testGroup = mannualTestGroupDAO.getTestGroupDetails(groupid);
				if (uploadType.equals("replace")) {
					mannualTestCaseDao.deleteTestCaseBygId(groupid);
				}
				if (testGroup != null) {
					List<MannualTestCaseTable> values = testCases.stream()
							.filter(x -> x.getTestGroupName().equals(testGroup.getGroupName()))
							.collect(Collectors.toList());

					UploadTestCaseBo testCaseBo = new UploadTestCaseBo();
					testCaseBo.setTestGroupId(groupid);
					testCaseBo.setTestCases(values);
					testCaseList.add(testCaseBo);
					i = insertTestCase(testCaseList);
					if (i == 1) {
						response.setServiceResponse("Testcases Uploaded");
						response.setServiceStatus("200");
					} else {
						response.setServiceResponse("Unable to import Test Case");
						response.setServiceStatus("300");
					}
				}
			} else {
				response.setServiceResponse("Unable to import Test Case");
				response.setServiceStatus("300");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceResponse("Unable to Process Your Request");
			response.setServiceStatus("400");
			// TODO: handle exception
		}
		return response;
	}

	public ServiceResponse importTestSuites(MultipartFile file, String testSuitesId, String uploadType,
			String testSuiteName) {
		ServiceResponse response = new ServiceResponse();
		Integer i = 0;
		Map<String, List<MannualTestCaseTable>> map = new HashedMap<String, List<MannualTestCaseTable>>();
		List<UploadTestCaseBo> testCaseList = new ArrayList<UploadTestCaseBo>();
		List<MannualTestCaseTable> testCases = readManualTestCases(file, testSuiteName);
		try {
			if (testCases.size() > 0) {
				Integer testSuitesid = Integer.parseInt(testSuitesId);
				List<MannualTestGroup> testGroup = mannualTestGroupDAO.getTestGroup(testSuitesid);
				if (uploadType.equals("replace")) {
					if (!testGroup.isEmpty()) {
						for (MannualTestGroup mannualTestGroup : testGroup) {
							String status = projectDao.deletegroup(mannualTestGroup.getTestGroupId());
							if (status.equalsIgnoreCase("Success"))
								mannualTestCaseDao.deleteTestCaseBygId(mannualTestGroup.getTestGroupId());
						}
					}
				}
				for (MannualTestCaseTable details1 : testCases) {
					if (details1 != null) {

						List<MannualTestCaseTable> values = testCases.stream()
								.filter(x -> x.getTestGroupName().equals(details1.getTestGroupName()))
								.collect(Collectors.toList());
						map.put(details1.getTestGroupName(), values);
					}
				}
				if (!map.isEmpty()) {
					for (Map.Entry<String, List<MannualTestCaseTable>> map1 : map.entrySet()) {
						MannualTestGroup testGroupbo = new MannualTestGroup();
						testGroupbo.setGroupName(map1.getKey());
						testGroupbo.setIsActive("Y");
						testGroupbo.setCreatedBy("");
						testGroupbo.setTestSuitesId(testSuitesid);
						MannualTestGroup group = mannualTestGroupDAO.checkAndCreateGroup(testGroupbo);
						if (group != null) {
							UploadTestCaseBo testCaseBo = new UploadTestCaseBo();
							testCaseBo.setTestGroupName(map1.getKey());
							testCaseBo.setTestGroupId(group.getTestGroupId());
							testCaseBo.setTestCases(map1.getValue());
							testCaseList.add(testCaseBo);
						}
					}
					i = insertTestCase(testCaseList);
				}
				if (i == 1) {
					response.setServiceResponse("New Test Cases are added");
					response.setServiceStatus("200");
				} else {
					response.setServiceResponse("Unable to import Test Case");
					response.setServiceStatus("300");
				}
			} else {
				response.setServiceResponse("Unable to import Test Case");
				response.setServiceStatus("300");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceResponse("Unable to Process Your Request");
			response.setServiceStatus("400");
			// TODO: handle exception
		}
		return response;
	}

	public ServiceResponse importTestSuitess(MultipartFile file, String testSuitesId, String uploadType,
			String testSuiteName) {
		ServiceResponse response = new ServiceResponse();
		Integer i = 0;
		Map<String, List<MannualTestCaseTable>> map = new HashedMap<String, List<MannualTestCaseTable>>();
		List<UploadTestCaseBo> testCaseList = new ArrayList<UploadTestCaseBo>();
		List<MannualTestCaseTable> testCases = readManualTestCases(file, testSuiteName);
		try {
			if (testCases.size() > 0) {
				Integer testSuitesid = Integer.parseInt(testSuitesId);
				List<MannualTestGroup> testGroup = mannualTestGroupDAO.getTestGroup(testSuitesid);
				if (uploadType.equals("replace")) {
					if (!testGroup.isEmpty()) {
						for (MannualTestGroup mannualTestGroup : testGroup) {
							String status = projectDao.deletegroup(mannualTestGroup.getTestGroupId());
							if (status.equalsIgnoreCase("Success"))
								mannualTestCaseDao.deleteTestCaseBygId(mannualTestGroup.getTestGroupId());
						}
					}
				}
				for (MannualTestCaseTable details1 : testCases) {
					if (details1 != null) {

						List<MannualTestCaseTable> values = testCases.stream()
								.filter(x -> x.getTestGroupName().equals(details1.getTestGroupName()))
								.collect(Collectors.toList());
						map.put(details1.getTestGroupName(), values);
					}
				}
				if (!map.isEmpty()) {
					for (Map.Entry<String, List<MannualTestCaseTable>> map1 : map.entrySet()) {
						MannualTestGroup testGroupbo = new MannualTestGroup();
						testGroupbo.setGroupName(map1.getKey());
						testGroupbo.setIsActive("Y");
						testGroupbo.setCreatedBy("");
						testGroupbo.setTestSuitesId(testSuitesid);
						MannualTestGroup group = mannualTestGroupDAO.checkAndCreateGroup(testGroupbo);
						if (group != null) {
							UploadTestCaseBo testCaseBo = new UploadTestCaseBo();
							testCaseBo.setTestGroupName(map1.getKey());
							testCaseBo.setTestGroupId(group.getTestGroupId());
							testCaseBo.setTestCases(map1.getValue());
							testCaseList.add(testCaseBo);
						}
					}
					i = insertTestCase(testCaseList);
				}
				if (i == 1) {
					response.setServiceResponse("Replaced Test Cases Successfully");
					response.setServiceStatus("200");
				} else {
					response.setServiceResponse("Unable to import Test Case");
					response.setServiceStatus("300");
				}
			} else {
				response.setServiceResponse("Unable to import Test Case");
				response.setServiceStatus("300");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceResponse("Unable to Process Your Request");
			response.setServiceStatus("400");
			// TODO: handle exception
		}
		return response;
	}
	/*
	 * public ServiceResponse importTestSuites(MultipartFile file, String
	 * testSuitesId, String uploadType) { ServiceResponse response = new
	 * ServiceResponse(); Integer i = 0; Map<String, List<MannualTestCaseTable>>
	 * map=new HashedMap<String, List<MannualTestCaseTable>>();
	 * List<UploadTestCaseBo> testCaseList = new ArrayList<UploadTestCaseBo>();
	 * List<MannualTestCaseTable> testCases = readManualTestCases(file); try {
	 * Integer testSuitesid = Integer.parseInt(testSuitesId); List<MannualTestGroup>
	 * testGroup = mannualTestGroupDAO.getTestGroup(testSuitesid); if
	 * (uploadType.equals("replace")) { if(!testGroup.isEmpty()) { for
	 * (MannualTestGroup mannualTestGroup : testGroup) { String status =
	 * projectDao.deletegroup(mannualTestGroup.getTestGroupId());
	 * if(status.equalsIgnoreCase("Success"))
	 * mannualTestCaseDao.deleteTestCaseBygId(mannualTestGroup.getTestGroupId()); }
	 * } } for (MannualTestCaseTable details1 : testCases) { if (details1 != null) {
	 * 
	 * List<MannualTestCaseTable> values = testCases.stream() .filter(x ->
	 * x.getTestGroupName().equals(details1.getTestGroupName()))
	 * .collect(Collectors.toList()); map.put(details1.getTestGroupName(), values);
	 * } } if(!map.isEmpty()) { for (Map.Entry<String, List<MannualTestCaseTable>>
	 * map1 : map.entrySet()) { MannualTestGroup testGroupbo=new MannualTestGroup();
	 * testGroupbo.setGroupName(map1.getKey()); testGroupbo.setIsActive("Y");
	 * testGroupbo.setCreatedBy(""); testGroupbo.setTestSuitesId(testSuitesid);
	 * MannualTestGroup group=mannualTestGroupDAO.checkAndCreateGroup(testGroupbo);
	 * if(group!=null) { UploadTestCaseBo testCaseBo=new UploadTestCaseBo();
	 * testCaseBo.setTestGroupName(map1.getKey());
	 * testCaseBo.setTestGroupId(group.getTestGroupId());
	 * testCaseBo.setTestCases(map1.getValue()); testCaseList.add(testCaseBo); } }
	 * i=insertTestCase(testCaseList); } if (i == 1) {
	 * response.setServiceResponse("Testcases Uploaded");
	 * response.setServiceStatus("200"); } else {
	 * response.setServiceResponse("Unable to import TestCase");
	 * response.setServiceStatus("300"); }
	 * 
	 * } catch (Exception e) { e.printStackTrace();
	 * response.setServiceResponse("Unable to Process Your Request");
	 * response.setServiceStatus("400"); // TODO: handle exception } return
	 * response; }
	 */

	public ServiceResponse getTestSuits(Integer projectID) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<OverViewTestSuitesDeatils> testSuites = new ArrayList<>();
		UserDetailsEntity roleEn = (UserDetailsEntity) session.getAttribute("user");

		try {

			testSuites = mannualTestSuitesDAO.getTestSuitesByProject(projectID);
			response.setServiceStatus("200");
			response.setServiceResponse(testSuites);

		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getResourseEffortsstake(ResourceRequestBo resourceRequestBo) {

		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			Map<String, List<resourceexecution>> exeTestCase = mannualExecutionLogsDao
					.getResourseEfforts1(resourceRequestBo, "executed");
			// System.out.println(exeTestCase);
			if (!exeTestCase.isEmpty()) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(exeTestCase);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("No Data Available");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}

		} catch (Exception e) {
			e.printStackTrace();
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;

	}

	public ServiceResponse getTestSuitsDetails(Integer testSuitesId) {
		String Status = "No TestSuites";
		ServiceResponse response = new ServiceResponse();
		try {
			MannualTestSuites testSuites = mannualTestSuitesDAO.getTestSuitsDetails(testSuitesId);
			response.setServiceStatus("200");
			response.setServiceResponse(testSuites);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getTestGroupDetails(Integer groupId) {
		String Status = "No TestGroup";
		ServiceResponse response = new ServiceResponse();
		try {
			MannualTestGroup testGroup = mannualTestGroupDAO.getTestGroupDetails(groupId);
			response.setServiceStatus("200");
			response.setServiceResponse(testGroup);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getTestGroupAndCases(Integer testSuitesId) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		Map<String, MannualTestGroupAndCase> testgroup = new HashedMap();
		try {
			testgroup = mannualTestGroupDAO.getTestGroupAndCases(testSuitesId);
			response.setServiceStatus("200");
			response.setServiceResponse(testgroup);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getGroupTestCasesBysuitesFiltered(Integer testSuitesId, String filterParams) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		Map<String, MannualTestGroupAndCase> testgroup = new HashedMap();
		String colName = filterParams.split("-")[0];
		String colVal = filterParams.split("-")[1];
		try {
			testgroup = mannualTestGroupDAO.getGroupTestCasesBysuitesFiltered(testSuitesId, colName, colVal);
			response.setServiceStatus("200");
			response.setServiceResponse(testgroup);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getAllTestCassesBySuitesId(Integer testSuitesId) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<MannualTestCaseTable> testCases = new ArrayList<MannualTestCaseTable>();
		try {
			testCases = mannualTestGroupDAO.getAllTestCassesBySuitesId(testSuitesId);
			response.setServiceStatus("200");
			response.setServiceResponse(testCases);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getAllTestCassesByTestRun(Integer rerunId) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<MannualTestCaseTable> testCases = new ArrayList<MannualTestCaseTable>();
		try {
			testCases = mannualTestGroupDAO.getAllTestCassesByTestRun(rerunId);
			response.setServiceStatus("200");
			response.setServiceResponse(testCases);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getFailTestCasesByReRunId(Integer reRunId, Integer runId) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<MannualTestCaseTable> testCases = new ArrayList<MannualTestCaseTable>();
		try {
			testCases = mannualTestGroupDAO.getFailTestCasesByReRunId(reRunId, runId);
			response.setServiceStatus("200");
			response.setServiceResponse(testCases);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse getFailResolvedTestCasesByReRunId(Integer reRunId, Integer runId) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<MannualTestCaseTable> testCases = new ArrayList<MannualTestCaseTable>();
		try {
			testCases = mannualTestGroupDAO.getFailResolvedTestCasesByReRunId(reRunId, runId);
			response.setServiceStatus("200");
			response.setServiceResponse(testCases);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong!!";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse createTestGroup(MannualTestGroup mannualTestGroup) {
		String Status = "Something went wrong!!";
		ServiceResponse response = new ServiceResponse();
		try {

			// //System.out.println("checkfrop...."+checkfrop);
			Status = "New Test Group has been created Successfully";
			response.setServiceStatus("200");
			response.setServiceResponse(Status);
			if (mannualTestGroup.getGroupName().length() >= 50) {
				Status = "Test Group Name is too long";
				response.setServiceStatus("400");
				response.setServiceResponse(Status);
			} else {
				MannualTestGroup checktcgrp = null;
				checktcgrp = mannualTestGroupRepo.findBytestSuitesIdAndGroupName(mannualTestGroup.getTestSuitesId(),
						mannualTestGroup.getGroupName());
				// System.out.println("checktcgrp..." + checktcgrp + "ok.... done chng");
				if (checktcgrp == null) {
					mannualTestGroupDAO.createTestGroup(mannualTestGroup);
					Status = "New Test Group has been created Successfully";
					// System.out.println("group sucessfully.......");
					response.setServiceStatus("200");
					response.setServiceResponse(Status);
				} else if (checktcgrp.getGroupName().equals(mannualTestGroup.getGroupName())) {
					mannualTestGroupDAO.createTestGroup(mannualTestGroup);

					Status = "New Test Group has been created Successfully";
					response.setServiceStatus("200");
					response.setServiceResponse(Status);
				}

				else {
					Status = "Test Group already exists!!!";
					response.setServiceStatus("400");
					response.setServiceResponse(Status);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Test Group already exists!!!";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

			// TODO: handle exception
		}
		return response;

	}

	public ServiceResponse updateTestGroup(MannualTestGroup testGroup) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			MannualTestGroup checktcgrp = null;
			if (testGroup.getGroupName().length() >= 46) {
				Status = "Test Group Name is too long";
				response.setServiceStatus("400");
				response.setServiceResponse(Status);
			} else {
				checktcgrp = mannualTestGroupRepo.findBytestSuitesIdAndGroupName(testGroup.getTestSuitesId(),
						testGroup.getGroupName());
				// System.out.println("checktcgrp..." + checktcgrp + "ok.... done chng");
				if (checktcgrp == null) {
					mannualTestGroupDAO.updateTestGroup(testGroup);

					Status = "Test Group Updated Sucessfully";
					// System.out.println("group sucessfully.......");
					response.setServiceStatus("200");
					response.setServiceResponse(Status);
				} else if (checktcgrp.getGroupName().equals(testGroup.getGroupName())) {
					mannualTestGroupDAO.updateTestGroup(testGroup);

					Status = "Test Group Updated Sucessfully";
					response.setServiceStatus("200");
					response.setServiceResponse(Status);
				}

				else {
					Status = "Duplicate Test Group Name";
					response.setServiceStatus("400");
					response.setServiceResponse(Status);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Duplicate Test Group Name";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

			// TODO: handle exception
		}
		return response;
	}

	public ServiceResponse getTestSuitsByPid(Integer projectID) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<MannualTestSuites> testSuites = new ArrayList<>();
		try {
			testSuites = mannualTestSuitesDAO.getTestSuitsByPid(projectID);
			response.setServiceStatus("200");
			response.setServiceResponse(testSuites);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getTestGroup(Integer testSuitesId) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<MannualTestGroup> testgroup = new ArrayList<>();
		try {
			testgroup = mannualTestGroupDAO.getTestGroup(testSuitesId);
			response.setServiceStatus("200");
			response.setServiceResponse(testgroup);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getTestCases(Integer gropuID) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<MannualTestCaseTable> testCases = new ArrayList<>();
		try {
			testCases = mannualTestCaseDao.getTestCases(gropuID);
			response.setServiceStatus("200");
			response.setServiceResponse(testCases);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse createTestSuites(MannualTestSuites suites) {
		ServiceResponse response = new ServiceResponse();

		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			// mannualTestSuitesDAO.createTestSuites(suites);
			if (suites.getTestSuitesName().length() >= 46) {
				Status = "TestSuites name is too long";
				response.setServiceStatus("400");
			} else {
				MannualTestSuites suitesNew = mannualTestSuitesDAO.checkAndCreateSuiteBySuiteName(suites);
				if (suitesNew.getCheckForTestSuite().equals("newly Created")) {
					Status = "New TestSuites has been created Successfully";
				} else {
					Status = suitesNew.getCheckForTestSuite();
					response.setServiceStatus("200");
				}

			}
			response.setServiceStatus("200");
			response.setServiceResponse(Status);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "TestSuite already exists";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

			// TODO: handle exception
		}
		return response;
	}

	public ServiceResponse retrieveTestSuite(MannualTestSuites suites, String decision) {
		ServiceResponse response = new ServiceResponse();

		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		if (decision.equals("retrieve")) {
			try {
				// mannualTestSuitesDAO.updateDeletedTestSuite(suites.getCreatedBy(),
				// suites.getTestSuitesName());
				Status = "Your testSuite has been retrieved Successfully";
				response.setServiceStatus("200");
				response.setServiceResponse(Status);
			} catch (Exception e) {
				e.printStackTrace();
				// Status = "TestSuite already exists";
				response.setServiceStatus("400");
				response.setServiceResponse(Status);

				// TODO: handle exception
			}
		} else if (decision.equals("new")) {
			try {
				MannualTestSuites suitesNew = mannualTestSuitesDAO.createTestSuites(suites);
				Status = "New TestSuites has been created Successfully";
				response.setServiceStatus("200");
				response.setServiceResponse(Status);
			} catch (Exception e) {
				e.printStackTrace();
				// Status = "TestSuite already exists";
				response.setServiceStatus("400");
				response.setServiceResponse(Status);

				// TODO: handle exception
			}
		}

		return response;
	}

	public ServiceResponse updateTestSuites(MannualTestSuites suites, String projectwiserole) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Something Went Wrong..We are Unable to Process Your Request";
		UserDetailsEntity roleEn = (UserDetailsEntity) session.getAttribute("user");
		// System.out.println("test suites...update.." + projectwiserole);
		try {
			if (projectwiserole.equals("Manager") || projectwiserole.equals("Tester")) {
				if (suites.getTestSuitesName().length() >= 46) {

					Status = "TestSuites name is too long";
					response.setServiceStatus("400");
					// Status = "TestSuite Updated Successfully";
				} else {
					Status = mannualTestSuitesDAO.updateTestSuites(suites);
					response.setServiceStatus("200");
				}

				response.setServiceResponse(Status);
			} else {

				Status = "Can not update admin";
				response.setServiceStatus("400");
				response.setServiceResponse(Status);
			}
		} catch (Exception e) {

			e.printStackTrace();
			Status = "TestSuites Not Updated";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

			// TODO: handle exception
		}
		return response;
	}

	public ServiceResponse deleteTestCase(Integer testcaseSID) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			mannualTestCaseDao.deleteTestCase(testcaseSID);
			Status = "Test Case Deleted Successfully";
			response.setServiceStatus("200");
			response.setServiceResponse(Status);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Test Case Not Deleted";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

			// TODO: handle exception
		}
		return response;
	}

	public ServiceResponse deleteMulTestCase(Integer[] testcaseSID) {
		ServiceResponse response = new ServiceResponse();

		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			String check = mannualTestCaseDao.deleteMulTestCase(testcaseSID);
			if (check == "deleted") {
				Status = "Test Cases Deleted Successfully";
			} else {
				Status = "  Not Deleted";
			}
			response.setServiceStatus("200");
			response.setServiceResponse(Status);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Test Cases Not Deleted";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

			// TODO: handle exception
		}
		return response;
	}

	public ServiceResponse addNewTestId(MannualTestCaseTable testcase) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			mannualTestCaseDao.addNewTestId(testcase);
			testcase.setCreatedDate(new Date());
			Status = "Test Case Added Successfully";
			response.setServiceStatus("200");
			response.setServiceResponse(Status);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Test Case Not Added";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse updateTestCase(MannualTestCaseTable testcase) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			MannualTestCaseTable caseTable = mannualTestCaseDao.updateTestCase(testcase);
			if (caseTable != null) {
				Status = "Test Case Added Successfully";
			}
			response.setServiceStatus("200");
			response.setServiceResponse(Status);
			// System.out.println("caseTable..." + caseTable.getTestDescription());
		} catch (DataIntegrityViolationException e) {
			Status = "Same Test Case ID already present!! Kindly Check..";
			response.setServiceResponse(Status);
			response.setServiceStatus("400");
			// TODO: handle exception
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Test Case Not Added";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse createRun(CreateRunBo testRunObj) {
		String status = "Unable to Start Run";
		List<MannualTestCaseExecutionLogs> executionLogsList = new ArrayList<MannualTestCaseExecutionLogs>();
		SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
		ServiceResponse response = new ServiceResponse();
		String runnames = testRunObj.getRunName();
		Integer round = testRunObj.getRound();
		// System.out.println();
		// System.out.println(runnames);
		try {
			List<MannualRunTable> findname = mannualRunRepository.findByRunName(runnames);
			if (findname.isEmpty() || round != 1) {
				MannualRunTable runTable = new MannualRunTable();
				runTable.setId(testRunObj.getId());
				runTable.setRunName(testRunObj.getRunName());
				runTable.setProjectId(testRunObj.getProjectId());
				runTable.setTestSuitesId(testRunObj.getTestSuitesId());
				runTable.setCreatedBy(testRunObj.getCreatedBy());
				runTable.setIsActive(testRunObj.getIsActive());
				runTable.setRunFlag(1);
				if (!testRunObj.getTestCaseList().isEmpty()) {
					MannualRunTable createRun = mannualRunDao.createRun(runTable);
					if (createRun != null) {
						MannualReRunTable mannualReRunTable = new MannualReRunTable();
						mannualReRunTable.setRunId(createRun.getId());
						mannualReRunTable.setRound(testRunObj.getRound());
						mannualReRunTable.setRunName(testRunObj.getRunName());
						mannualReRunTable.setProjectId(testRunObj.getProjectId());
						mannualReRunTable.setTestSuitesId(testRunObj.getTestSuitesId());
						if (testRunObj.getTargetEndDate() != null) {
							// System.out.println(testRunObj.getTargetEndDate());
							mannualReRunTable
									.setTargetEndDate(format2.format(format1.parse(testRunObj.getTargetEndDate())));
						}
						mannualReRunTable.setStatus("Running");
						mannualReRunTable.setIsActive(createRun.getIsActive());
						mannualReRunTable.setCreatedBy(testRunObj.getCreatedBy());
						mannualReRunTable.setDescription(testRunObj.getDescription());

						MannualReRunTable mannualRunDetails = mannualReRunDao.createRun(mannualReRunTable);
						if (mannualRunDetails != null) {
							// System.out.println("testRunObj.getTestCaseList();...." +
							// testRunObj.getTestCaseList());
							List<MannualTestCaseTable> inputList = testRunObj.getTestCaseList();
							if (inputList != null) {
								Date dt = new Date();
								for (MannualTestCaseTable caseTable : inputList) {
									MannualTestCaseExecutionLogs logs = new MannualTestCaseExecutionLogs();
									logs.setReRunId(mannualRunDetails.getId());
									logs.setScenarioID(caseTable.getScenarioID());
									logs.setProjectId(testRunObj.getProjectId());
									logs.setTestSuitesId(testRunObj.getTestSuitesId());
									logs.setTestGroupId(Integer.valueOf(caseTable.getTestGroupId()));
									logs.setScenarios(caseTable.getScenarios());
									logs.setTestCaseSrId(Integer.valueOf(caseTable.getSrID()));
									logs.setTestCaseSrNo(Integer.valueOf(Integer.valueOf(caseTable.getSrNo())));
									if (mannualReRunTable.getRound() > 1) {
										logs.setAbsoluteTCID(Integer.valueOf(caseTable.getAbsoluteTCID()));
									} else if (mannualReRunTable.getRound() == 1) {
										logs.setAbsoluteTCID(Integer.valueOf(caseTable.getSrID()));
									}
									if (caseTable.getBrowser() == null) {
										logs.setBrowser("Chrome");

									} else {

										if (caseTable.getBrowser().equals("FireFox")
												|| caseTable.getBrowser().equals("IE")
												|| caseTable.getBrowser().equals("Chrome")) {
											logs.setBrowser(caseTable.getBrowser());
										} else {
											logs.setBrowser("Chrome");
										}
									}
									logs.setExpectedResult(caseTable.getExpectedResult());
									logs.setActualResult(caseTable.getActualRsult());
									logs.setFunctionality(caseTable.getFunctionality());
									logs.setTestCaseType(caseTable.getTestCaseType());
									logs.setTestDescription(caseTable.getTestDescription());
									logs.setTestData(caseTable.getTestData());
									logs.setSteps(caseTable.getSteps());
									logs.setTesterId(testRunObj.getAssignTo());
									logs.setCreatedTimeStamp(dt);
									logs.setUpdatedTimeStamp(dt);
									logs.setTestID(caseTable.getTestID());
									logs.setTestCaseAction("Not Started");
									logs.setBugId(
											(caseTable.getBugID() != null) ? caseTable.getBugID().toString() : null);
									logs.setBugSeverity(
											(caseTable.getBugSeverity() != null) ? caseTable.getBugID().toString()
													: null);
									logs.setBugPriority(
											(caseTable.getBugPriority() != null) ? caseTable.getBugID().toString()
													: null);

									executionLogsList.add(logs);

								}
								mannualExecutionLogsDao.saveTestCaseForRun(executionLogsList);
							}

							status = "Successfully Created a Run as " + mannualReRunTable.getRunName() + "_Round"
									+ mannualReRunTable.getRound();
							response.setServiceStatus("200");
						}
					}
				} else {
					status = "Unable to start run kindly add testcase first";
					response.setServiceStatus("400");
				}
			} else {
				status = "Duplicate Test Run Name";
				response.setServiceStatus("400");
			}
		} catch (Exception e) {
			e.printStackTrace();
			status = "Something went wrong";
			response.setServiceStatus("400");
		}
		response.setServiceResponse(status);
		return response;

	}

	public ServiceResponse getActiveRun(Integer projectId) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<MannualReRunTable> activeRun = mannualReRunDao.getActiveRun(projectId);
			for (int i = 0; i < activeRun.size(); i++) {
				activeRun.get(i).getNA();
				// System.out.println("mmmmmm......" + activeRun.get(i).getNA());
			}

			response.setServiceStatus("200");
			if (activeRun != null)
				response.setServiceResponse(activeRun);
			else {
				response.setServiceResponse("No Active Run");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse getCompletedRun(Integer projectId) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			Map<String, MannualReRunDetailsByRnd> completedRun = mannualReRunDao.getCompletedRunNew(projectId);

			response.setServiceStatus("200");
			if (completedRun != null)
				response.setServiceResponse(completedRun);
			else {
				response.setServiceResponse("No Active Run");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse stopActiveRun(MannualReRunTable mannualReRunTable) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			MannualReRunTable activeRun = mannualReRunDao.stopActiveRun(mannualReRunTable);
			response.setServiceStatus("200");
			if (activeRun != null)
				response.setServiceResponse(activeRun);
			else {
				response.setServiceResponse("No Active Run");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse getrerunnameByrun(Integer runid) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<MannualReRunTable> activeRun = mannualReRunDao.getrerunnameByrun(runid);
			response.setServiceStatus("200");
			if (activeRun != null)
				response.setServiceResponse(activeRun);
			else {
				response.setServiceResponse("No Active Run");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;

	}

	public ServiceResponse getLastRerunnameByrun(Integer runid, Integer suiteId) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		// List<MannualReRunTable> completeRun=new ArrayList<MannualReRunTable>();
		try {
			// MannualReRunTable maxRun = mannualReRunDao.getLastRerunnameByrun(runid);
			List<MannualReRunTable> allRun = mannualReRunDao.getAllCompleteRunByRun(runid);
			/*
			 * for(MannualReRunTable rerun : allRun) {
			 * if(!rerun.getStatus().equals("Running")) { completeRun.add(rerun); } }
			 */
			MannualReRunTable maxRun = allRun.get(0);
			MannualTestSuites ts = mannualTestSuitesDAO.getTestSuitsDetails(suiteId);
			response.setServiceStatus("200");
			if (maxRun != null) {
				try {
					// System.out.println("ts....." + ts.getIsActive() + ".....yss..." +
					// ts.getIsDeleted());
					if ((ts.getIsActive().equals("Y")) && (!ts.getIsDeleted().equals("Y"))) {
						if ((!maxRun.getStatus().equals("Running")) && (!ts.getIsDeleted().equals("Y"))) {
							response.setServiceResponse(allRun);
						} else {
							if (ts.getIsDeleted().equals("Y")) {
								response.setServiceStatus("202");
								response.setServiceResponse(
										"Sorry You Can't Re-run This As The Respective Test Suite Is Deleted ");
							}
							if (maxRun.getStatus().equals("Running")) {
								response.setServiceStatus("202");
								response.setServiceResponse("Sorry You Can't Re-run This As There Is One Active Run ");
							}
						}
					}

					else {
						if (ts.getIsDeleted().equals("Y")) {
							response.setServiceStatus("202");
							response.setServiceResponse(
									"Sorry You Can't Re-run This As The Respective Test Suite Is Deleted ");

						} else if (!(ts.getIsActive().equals("Y"))) {
							response.setServiceStatus("202");
							response.setServiceResponse(
									"Sorry You Can't Re-run This As The Respective Test Suite Is Not Active ");
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					response.setServiceStatus("202");
					response.setServiceResponse("Sorry You Can't Re-run This As The Respective Test Suite Is Deleted ");

				}

			} else {
				response.setServiceResponse("No Active Run");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;

	}

	/*
	 * public ServiceResponse getLastRerunnameByrun(Integer runid, Integer suiteId)
	 * { ServiceResponse response = new ServiceResponse(); String Status =
	 * "Some Things Went Wrong..We are Unable to Process Your Request";
	 * //List<MannualReRunTable> completeRun=new ArrayList<MannualReRunTable>(); try
	 * { //MannualReRunTable maxRun = mannualReRunDao.getLastRerunnameByrun(runid);
	 * List<MannualReRunTable> allRun =
	 * mannualReRunDao.getAllCompleteRunByRun(runid);
	 * 
	 * for(MannualReRunTable rerun : allRun) {
	 * if(!rerun.getStatus().equals("Running")) { completeRun.add(rerun); } }
	 * 
	 * MannualReRunTable maxRun=allRun.get(0); MannualTestSuites
	 * ts=mannualTestSuitesDAO.getTestSuitsDetails(suiteId);
	 * response.setServiceStatus("200"); if (maxRun != null) { try {
	 * //System.out.println("ts....."+ts.getIsActive()+".....yss..."+ts.getIsDeleted
	 * () ); if((ts.getIsActive().equals("Y"))&&(!ts.getIsDeleted().equals("Y"))) {
	 * if((!maxRun.getStatus().equals("Running"))&&(!ts.getIsDeleted().equals("Y")))
	 * { response.setServiceResponse(allRun); } else {
	 * if(ts.getIsDeleted().equals("Y")) { response.setServiceStatus("202");
	 * response.
	 * setServiceResponse("Sorry you Can't Re Run this as the respective Test Suite is deleted "
	 * ); } if(maxRun.getStatus().equals("Running")) {
	 * response.setServiceStatus("202"); response.
	 * setServiceResponse("Sorry you Can't Re Run this as There is one Active Run "
	 * ); } } }
	 * 
	 * else { if(ts.getIsDeleted().equals("Y")) { response.setServiceStatus("202");
	 * response.
	 * setServiceResponse("Sorry you Can't Re Run this as the respective Test Suite is deleted "
	 * );
	 * 
	 * } else if(!(ts.getIsActive().equals("Y"))) {
	 * response.setServiceStatus("202"); response.
	 * setServiceResponse("Sorry you Can't Re Run this as the respective Test Suite is not active "
	 * ); } } } else { response.setServiceResponse("No Active Run"); } } catch
	 * (Exception e) { e.printStackTrace(); response.setServiceStatus("400");
	 * response.setServiceResponse(Status);
	 * 
	 * } return response;
	 * 
	 * }
	 */

	public ServiceResponse deleteActiveRun(Integer reRunId) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Unable to delete Active Run";
		try {
			Integer i = mannualReRunDao.deleteActiveRun(reRunId);
			if (i == 1) {
				response.setServiceStatus("200");
				Status = "Succesfully deleted Run";
				List<MannualTestCaseExecutionLogs> lExecutionLogs = mannualExecutionLogsDao
						.getTCExecutionLogsByRerunID(reRunId);
				mannualExecutionLogsDao.deleteActiveTestCasesForRun(reRunId);
				for (MannualTestCaseExecutionLogs ml : lExecutionLogs) {
					updateDeletedTCStatusPFNE(ml);
				}
			} else {
				Status = "Unable to delete Active Run";
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
		}
		response.setServiceResponse(Status);
		return response;
	}

	public ServiceResponse deleteRoundOneRun(Integer id) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Unable to delete Active Run";
		try {
			// System.out.println("calling delete mannualruntable service, id is " + id);
			Integer i = mannualRunDao.deleteRoundOneRun(id);
			if (i == 1) {
				response.setServiceStatus("200");
				Status = "Succesfully deleted Run";
				mannualExecutionLogsDao.deleteActiveTestCasesForRun(id);
			} else {
				Status = "Unable to delete Active Run";
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
		}
		response.setServiceResponse(Status);
		return response;
	}

	public ServiceResponse getTestGroupAndCasesByRun(Integer runId) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		Map<String, MannualRunDetailsData> runTestCaseDetails = new HashedMap<String, MannualRunDetailsData>();
		try {
			runTestCaseDetails = mannualExecutionLogsDao.getTestGroupAndCasesByRun(runId);
			response.setServiceStatus("200");
			response.setServiceResponse(runTestCaseDetails);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getTodoRunList(String useId, String projectId) {
		String Status = "No Test Cases Assigned";
		ServiceResponse response = new ServiceResponse();
		List<AssiginRunBo> assiginRun = null;
		try {
			assiginRun = mannualExecutionLogsDao.getTodoRunList(useId, projectId);
			for (AssiginRunBo arr : assiginRun) {
				String date = arr.getTargetDate();
				if (date != null) {
					date = date.substring(0, 10);
					// System.out.println("date..." + date);
					SimpleDateFormat formDate = new SimpleDateFormat("yyyy-MM-dd");

					// String strDate = formDate.format(System.currentTimeMillis()); // option 1
					String strDate = formDate.format(new Date()); // option 2
					// System.out.println("strDate" + strDate);
					if (strDate.compareTo(date) < 0) {
						// //System.out.println("blue color");
						arr.setColordate("green");
					} else {
						// //System.out.println("red color");
						arr.setColordate("red");

					}
				} else {
					date = "";
				}
				arr.setTargetDate(date);

			}
			if (assiginRun != null) {
				response.setServiceStatus("200");
				response.setServiceResponse(assiginRun);
			} else {
				response.setServiceStatus("No Test Cases Assigned");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getTodoTestCaseList(String userId, String runId) {
		String Status = "No Test Cases Assigned";
		ServiceResponse response = new ServiceResponse();
		Map<String, MannualRunDetailsData> toDoList = null;
		try {
			toDoList = mannualExecutionLogsDao.getTodoTestCaseList(userId, Integer.parseInt(runId));
			if (!toDoList.isEmpty()) {
				response.setServiceStatus("200");
				response.setServiceResponse(toDoList);
			} else {
				response.setServiceStatus("No Test Cases Assigned");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public String saveLatestTestcaseStatus(MannualTestCaseExecutionLogs dbLogs) {
		String status = "Execution Data Has been Updated";

		try {
			// Integer round=mannualReRunDao.getCurrentRoundCount(dbLogs.getReRunId());

			try {
				/*
				 * Integer srID=dbLogs.getTestCaseSrId(); for(int r=1; r<round; r++) {
				 * srID=mannualExecutionLogsDao.getAbsoluteTestcaseID(srID); }
				 * 
				 * try { mannualTestCaseDao.updateTestCaseLatestStatus(srID,
				 * dbLogs.getStatus()); } catch(Exception e) { e.printStackTrace();
				 * status=status+" but it would not reflect in Dashboard."; }
				 */
				/*
				 * mannualTestCaseDao.updateTestCaseLatestStatus(dbLogs.getAbsoluteTCID(),
				 * dbLogs.getStatus(), dbLogs.getTestCaseAction(), ((dbLogs.getBugId()!=null) ||
				 * (dbLogs.getBugId()!=""))?Integer.valueOf(dbLogs.getBugId()):0,
				 * dbLogs.getBugStatus(), dbLogs.getBugSeverity(), dbLogs.getBugPriority());
				 */
				mannualTestCaseDao.updateTestCaseLatestStatus(dbLogs);

			} catch (Exception e2) {
				e2.printStackTrace();
				status = status + " but it would not reflect in Dashboard.";
			}

		} catch (Exception e1) {
			e1.printStackTrace();
			status = status + " but it would not reflect in Dashboard.";
		}
		// System.out.println(status);
		return status;
	}

	// automation
	public String saveLatestTestcaseStatus1(AutomationTestCaseExecutionLogs arLogs) {
		String status = "Execution Data Has been Updated";

		try {
			// Integer round=mannualReRunDao.getCurrentRoundCount(dbLogs.getReRunId());

			try {
				/*
				 * Integer srID=dbLogs.getTestCaseSrId(); for(int r=1; r<round; r++) {
				 * srID=mannualExecutionLogsDao.getAbsoluteTestcaseID(srID); }
				 * 
				 * try { mannualTestCaseDao.updateTestCaseLatestStatus(srID,
				 * dbLogs.getStatus()); } catch(Exception e) { e.printStackTrace();
				 * status=status+" but it would not reflect in Dashboard."; }
				 */
				/*
				 * mannualTestCaseDao.updateTestCaseLatestStatus(dbLogs.getAbsoluteTCID(),
				 * dbLogs.getStatus(), dbLogs.getTestCaseAction(), ((dbLogs.getBugId()!=null) ||
				 * (dbLogs.getBugId()!=""))?Integer.valueOf(dbLogs.getBugId()):0,
				 * dbLogs.getBugStatus(), dbLogs.getBugSeverity(), dbLogs.getBugPriority());
				 */
				mannualTestCaseDao.updateTestCaseLatestStatus1(arLogs);

			} catch (Exception e2) {
				e2.printStackTrace();
				status = status + " but it would not reflect in Dashboard.";
			}

		} catch (Exception e1) {
			e1.printStackTrace();
			status = status + " but it would not reflect in Dashboard.";
		}
		// System.out.println(status);
		return status;
	}

	private void saveToBugDetails(String bugID, Integer bugToolID, MannualTestCaseExecutionLogs browserLogs) {
		BugDetailsEntity b = null;
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String toolName = bugRepo.findByBugToolId(bugToolID);
			IntegratedBugToolEntity bugTool = integratedBugToolRepo.findByBugToolName(toolName);
			if (bugTool.getToolid() != null) {
				b = bugDetailsRepo.findByBugIDAndBugtoolIDAndProjectID(bugID, bugTool.getToolid(),
						browserLogs.getProjectId());
			}
			if (b == null) {
				b = new BugDetailsEntity();
				b.setBugID(bugID.toString());
				b.setBugtoolID(bugTool.getToolid());
				b.setProjectID(browserLogs.getProjectId());
				b.setTcSrID(browserLogs.getAbsoluteTCID());
				b.setBugSummary(browserLogs.getBugSummary());
				b.setBugDescription(browserLogs.getBugDescription());
				b.setBugSeverity(browserLogs.getBugSeverity());
				b.setBugPriority(browserLogs.getBugPriority());
				b.setBugStatus(browserLogs.getBugStatus() == null ? "Open" : browserLogs.getBugStatus());
				b.setCreatedBy(detailsEntity.getId());
				b.setCreatedOn(new Date());
				bugDetailsRepo.save(b);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// automation
	private void saveToBugDetails1(String bugID, Integer bugToolID, AutomationTestCaseExecutionLogs browserLogs) {
		BugDetailsEntity b = null;
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String toolName = bugRepo.findByBugToolId(bugToolID);

			IntegratedBugToolEntity bugTool = integratedBugToolRepo.findByBugToolName(toolName);
			if (bugTool.getToolid() != null) {
				b = bugDetailsRepo.findByBugIDAndBugtoolIDAndProjectID(bugID, bugTool.getToolid(),
						Integer.parseInt(browserLogs.getProjectId()));
			}
			if (b == null) {

				b = new BugDetailsEntity();
				b.setBugID(bugID.toString());
				b.setBugtoolID(bugTool.getToolid());
				b.setProjectID(Integer.parseInt(browserLogs.getProjectId()));
				b.setTcSrID(Integer.parseInt(browserLogs.getTestID()));

				b.setBugSummary(browserLogs.getApiBugSummary());
				b.setBugDescription(browserLogs.getTestDescription());
				b.setBugSeverity(browserLogs.getBugSeverity());
				b.setBugPriority(browserLogs.getBugPriority());
				b.setBugStatus(browserLogs.getBugStatus() == null ? "Open" : browserLogs.getBugStatus());
				b.setCreatedBy(detailsEntity.getId());
				b.setCreatedOn(new Date());
				bugDetailsRepo.save(b);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public ServiceResponse saveTestExceution(MannualTestCaseExecutionLogs exLogs) {
		// System.out.println("ProjectService :: saveTestExceution :: "+exLogs);
		ServiceResponse response = new ServiceResponse();
		String bugId = null;
		List<Object> list = null;
		String Status = "Execution Data has been Updated";
		try {
			list = new ArrayList<Object>();
			/*
			 * String bugSum=exLogs.getBugSummary(); exLogs.setBugSummary("");
			 */
			MannualTestCaseExecutionLogs caseTable = mannualExecutionLogsDao.saveTestExceution(exLogs);
			if (caseTable.getBugId() == null || caseTable.getBugId().equals("")) {
				// caseTable.setBugId(exLogs.getBugId());
			}
			caseTable.setBugType(exLogs.getBugType());
			if (caseTable != null) {

				// if (caseTable.getStatus().equalsIgnoreCase("fail"))
				if (caseTable.getTestCaseAction().equalsIgnoreCase("completed")) {
					// System.out.println(
					// caseTable.getBugType() + "====" + caseTable.getBugId() + "bug id of tool from
					// DB...");
					// System.out.println("bug id from browser..." + exLogs.getBugId());

					if (caseTable.getBugType().equals("0")) {
						caseTable.setBugId("");
						exLogs.setBugId("");
					}

					if ((exLogs.getBugId() == null) || (exLogs.getBugId().equals(""))) {
						try {
							// caseTable.getBugId()==null || caseTable.getBugId().equals("")
							// //System.out.println("testing before raising new issue, overlapping existing
							// issue"+caseTable.getBugId());
							if (caseTable.getBugType().equals("0")
									&& (caseTable.getBugId() == null || caseTable.getBugId().equals(""))) {
								// System.out.println("tool name : " + exLogs.getToolName());
								// BugTrackorToolEntity bugtool =
								// bugRepo.findByBugTrackingToolName(exLogs.getToolName()).get(0);
								if (exLogs.getToolName().equalsIgnoreCase("Bugzilla")) {
									bugId = bugZillaService.createBug(exLogs);
									Status = "Execution Data has been Updated and bug has been raised in Bugzilla";
								} else if (exLogs.getToolName().equalsIgnoreCase("Jira")) {
									bugId = jiraService.insert_In_Jira(exLogs);
									Status = "Execution Data has been Updated and bug has been raised in Jira";
									// System.out.println("created in jira...." + bugId);
								} else if (exLogs.getToolName().equalsIgnoreCase("Mantis")) {
									bugId = mantisServiceBajaj.createIssue(exLogs);
									Status = "Execution Data has been Updated and bug has been raised in Mantis";
									// System.out.println("created in Mantis...." + bugId);
								} else if (exLogs.getToolName().equalsIgnoreCase("Redmine")) {
									RedmineDTO dto = new RedmineDTO();
									dto.setTmtProjectID(exLogs.getProjectId());
									dto.setProjectId(exLogs.getRedmineProjectId());
									dto.setTrackerId(1);
									dto.setStatusId(Integer.parseInt(exLogs.getRedmineStatus()));
									dto.setPriorityId(Integer.parseInt(exLogs.getRedminePriority()));
									dto.setSubject(exLogs.getBugSummary());
									dto.setDescription(exLogs.getBugDescription());

									System.out.println(dto);

									ServiceResponse serviceResponse = redmineService.createIssueByProjectId(dto);
									bugId = serviceResponse.getServiceResponse().toString();
								}
							} else if (caseTable.getBugType().equals("1")) {
								// System.out.println(exLogs.getBugId() + "====");
								if ((exLogs.getBugId() != null)) {
									if (("Bugzilla").equalsIgnoreCase(exLogs.getToolName())) {
										/*
										 * if((exLogs.getBugStatus().equalsIgnoreCase("Reopen"))||
										 * (exLogs.getBugStatus().equalsIgnoreCase("Closed"))) {
										 */

										if (StringUtils.isNotEmpty(exLogs.getBugStatus())) {
											bugId = bugZillaService.updateBug(exLogs);
										}

									} else if (("jira").equalsIgnoreCase(exLogs.getToolName())) {
//										temporarly comment
										// bugId = jiraService.updateIssue(exLogs, exLogs.getProductName());
										bugId = exLogs.getBugId();
									} else if (("Mantis").equalsIgnoreCase(exLogs.getToolName())) {
										bugId = mantisServiceBajaj.updateIssue(exLogs);
									} else {
										bugId = exLogs.getBugId();
									}
								}

								if (bugId != "0")
									Status = "Execution Data has been Updated and Bug Has been Updated in your existing Bug";
								else
									Status = "Executtion Data has been Updated but unable to create bug as"
											+ exLogs.getBugId() + " is not present";
							}
						} catch (Exception e) {
							e.printStackTrace();
							Status = "Execution Data has been Updated but unable to create bugID!!!";
							// list.add(Status);
						}
					} else if ((exLogs.getBugId() != null) || (!(exLogs.getBugId().equals("")))) {
						// //System.out.println("bug id....." + exLogs.getBugId());
						ProjectDeatilsMeta p = projectRepository.findByprojectID(caseTable.getProjectId());
						String b = bugRepo.findByBugToolId(Integer.valueOf(p.getIsJiraEnable()));
						// //System.out.println("bugtool found of name....." + b);
						try {
							if (caseTable.getBugType().equals("1")) {
								// System.out.println(exLogs.getBugId() + "===");
								if (exLogs.getBugId() != null) {
									if (b.equalsIgnoreCase("bugzilla")) {
										/*
										 * if((exLogs.getBugStatus().equalsIgnoreCase("Reopen"))||
										 * (exLogs.getBugStatus().equalsIgnoreCase("Closed"))) {
										 */

										/*
										 * @change made by -animesh removing null checking for status as it is not
										 * letting update if there is status null with conditon update all other than
										 * status
										 */
										// if (StringUtils.isNotEmpty(exLogs.getBugStatus())) {
										bugId = bugZillaService.updateBug(exLogs);
										// }

									} else if (b.equalsIgnoreCase("jira")) {
										// temporarly commented-ani
										// bugId = jiraService.updateIssue(exLogs, p.getProjectName());
										String RorC = "";
										if (caseTable.getBugStatus().equalsIgnoreCase("Reopen")
												|| caseTable.getBugStatus().equalsIgnoreCase("Re-open"))
											RorC = "R";
										else
											RorC = "C";
										jiraService.bugReopen(caseTable.getProjectId(), exLogs.getBugId(),
												exLogs.getBugDescription(), RorC);
										bugId = exLogs.getBugId();
										// System.out.println("project name for jira ..........." + p.getProjectName());

									} else if (b.equalsIgnoreCase("Mantis")) {
										bugId = mantisService.updateIssue(exLogs);
										// System.out.println("project name for mantis ..........." +
										// p.getProjectName());

									} else {
										bugId = exLogs.getBugId();
									}
								}
								if (bugId != "0") {
									if (StringUtils.isNotEmpty(exLogs.getBugStatus())) {
										if (exLogs.getBugStatus().equals("Reopen")
												|| exLogs.getBugStatus().equalsIgnoreCase("Re-open")) {
											Status = "Execution data has been updated and Bug has been reopened";
										} else if (exLogs.getBugStatus().equals("Closed")) {
											Status = "Execution data has been updated and Bug has been closed";
										} else {
											Status = "Execution Data has been Updated and Bug Has been Updated in your existing Bug";
										}
									}

								} else {
									Status = "Execution Data has been Updated but unable to create bug as"
											+ exLogs.getBugId() + " is not present";
								}

							}
						} catch (Exception e) {
							e.printStackTrace();
							Status = "Data has been saved but unable to create bugID!!!";
						}
					}
					if (bugId != null) {
						caseTable.setBugId(bugId.toString());
						// caseTable.setBugSummary(bugSum);
						mannualExecutionLogsDao.saveBugId(caseTable);

					} else {
						if (StringUtils.isNotEmpty(caseTable.getBugId())) {
							bugId = caseTable.getBugId();
						}
					}
				}
				try {
					saveLatestTestcaseStatus(caseTable);// update status in Manual Testcase Table
					if (bugId != null) {
						saveToBugDetails(bugId, exLogs.getBugToolId(), caseTable);
					}
				} catch (Exception e) {
					System.err.println("Could not be saved to Test case table!!");
				}

				list.add(Status);
				list.add(bugId);
			}
			response.setServiceStatus("200");
			response.setServiceResponse(list);
		} catch (Exception e) {
			list = new ArrayList<Object>();
			e.printStackTrace();
			Status = "Test Case Execution Not Saved";
			response.setServiceStatus("400");
			list.add(Status);
			// response.setServiceResponse(Status);

		}
		response.setServiceResponse(list);
		return response;
	}

	// added later
	public ServiceResponse saveTestExceution1(AutomationTestCaseExecutionLogs arLogs) {

		// System.out.println("ProjectService :: saveTestExceution :: "+exLogs);
		ServiceResponse response = new ServiceResponse();
		String bugId = null;
		List<Object> list = null;
		String Status = "Execution Data has been Updated";
		try {
			list = new ArrayList<Object>();
			/*
			 * String bugSum=exLogs.getBugSummary(); exLogs.setBugSummary("");
			 */
			AutomationTestCaseExecutionLogs caseTable = mannualExecutionLogsDao.saveTestExceution1(arLogs);
			if (caseTable.getBugId() == null || caseTable.getBugId().equals("")) {
				// caseTable.setBugId(exLogs.getBugId());
			}
			caseTable.setBugType(arLogs.getBugType());
			if (caseTable != null) {

				// if (caseTable.getStatus().equalsIgnoreCase("fail"))
				if (caseTable.getTestCaseAction().equalsIgnoreCase("completed")) {
					// System.out.println(
					// caseTable.getBugType() + "====" + caseTable.getBugId() + "bug id of tool from
					// DB...");
					// System.out.println("bug id from browser..." + exLogs.getBugId());

					if (caseTable.getBugType().equals("0")) {
						caseTable.setBugId("");
						arLogs.setBugId("");
					}

					if ((arLogs.getBugId() == null) || (arLogs.getBugId().equals(""))) {
						try {
							// caseTable.getBugId()==null || caseTable.getBugId().equals("")
							// //System.out.println("testing before raising new issue, overlapping existing
							// issue"+caseTable.getBugId());
							if (caseTable.getBugType().equals("0")
									&& (caseTable.getBugId() == null || caseTable.getBugId().equals(""))) {
								// System.out.println("tool name : " + exLogs.getToolName());
								// BugTrackorToolEntity bugtool =
								// bugRepo.findByBugTrackingToolName(exLogs.getToolName()).get(0);
								if (arLogs.getToolName().equalsIgnoreCase("Bugzilla")) {
									// bugId = bugZillaService.createBug(arLogs);
									Status = "Execution Data has been Updated and bug has been raised in Bugzilla";
								} else if (arLogs.getToolName().equalsIgnoreCase("Jira")) {
									// bugId = autoJiraService.insert_In_Jira(arLogs);
									Status = "Execution Data has been Updated and bug has been raised in Jira";
									// System.out.println("created in jira...." + bugId);
								} else if (arLogs.getToolName().equalsIgnoreCase("Mantis")) {
									// bugId = mantisServiceBajaj.createIssue(arLogs);
									Status = "Execution Data has been Updated and bug has been raised in Mantis";
									// System.out.println("created in Mantis...." + bugId);
								}
							} else if (caseTable.getBugType().equals("1")) {
								// System.out.println(exLogs.getBugId() + "====");
								if ((arLogs.getBugId() != null)) {
									if (("Bugzilla").equalsIgnoreCase(arLogs.getToolName())) {
										/*
										 * if((exLogs.getBugStatus().equalsIgnoreCase("Reopen"))||
										 * (exLogs.getBugStatus().equalsIgnoreCase("Closed"))) {
										 */

										if (StringUtils.isNotEmpty(arLogs.getBugStatus())) {
											// bugId = bugZillaService.updateBug(arLogs);
										}

									} else if (("jira").equalsIgnoreCase(arLogs.getToolName())) {
//										temporarly comment
										// bugId = jiraService.updateIssue(exLogs, exLogs.getProductName());
										bugId = arLogs.getBugId();
									} else if (("Mantis").equalsIgnoreCase(arLogs.getToolName())) {
										// bugId = mantisServiceBajaj.updateIssue(arLogs);
									} else {
										// bugId = arLogs.getBugId();
									}
								}

								if (bugId != "0")
									Status = "Execution Data has been Updated and Bug Has been Updated in your existing Bug";
								else
									Status = "Executtion Data has been Updated but unable to create bug as"
											+ arLogs.getBugId() + " is not present";
							}
						} catch (Exception e) {
							e.printStackTrace();
							Status = "Execution Data has been Updated but unable to create bugID!!!";
							// list.add(Status);
						}
					} else if ((arLogs.getBugId() != null) || (!(arLogs.getBugId().equals("")))) {
						// //System.out.println("bug id....." + exLogs.getBugId());
						int projectId = Integer.parseInt(caseTable.getProjectId());
						ProjectDeatilsMeta p = projectRepository.findByprojectID(projectId);
						String b = bugRepo.findByBugToolId(Integer.valueOf(p.getIsJiraEnable()));
						// //System.out.println("bugtool found of name....." + b);
						try {
							if (caseTable.getBugType().equals("1")) {
								// System.out.println(exLogs.getBugId() + "===");
								if (arLogs.getBugId() != null) {
									if (b.equalsIgnoreCase("bugzilla")) {
										/*
										 * if((exLogs.getBugStatus().equalsIgnoreCase("Reopen"))||
										 * (exLogs.getBugStatus().equalsIgnoreCase("Closed"))) {
										 */

										/*
										 * @change made by -animesh removing null checking for status as it is not
										 * letting update if there is status null with conditon update all other than
										 * status
										 */
										// if (StringUtils.isNotEmpty(exLogs.getBugStatus())) {
										// bugId = bugZillaService.updateBug(arLogs);pranik
										// }

									} else if (b.equalsIgnoreCase("jira")) {
										// temporarly commented-ani
										// bugId = jiraService.updateIssue(exLogs, p.getProjectName());
										String RorC = "";
										if (caseTable.getBugStatus().equalsIgnoreCase("Reopen")
												|| caseTable.getBugStatus().equalsIgnoreCase("Re-open"))
											RorC = "R";
										else
											RorC = "C";
										jiraService.bugReopen(Integer.parseInt(caseTable.getProjectId()),
												arLogs.getBugId(), arLogs.getTestDescription(), RorC);
										bugId = arLogs.getBugId();
										// System.out.println("project name for jira ..........." + p.getProjectName());

									} else if (b.equalsIgnoreCase("Mantis")) {
										// bugId = mantisService.updateIssue(arLogs);
										// System.out.println("project name for mantis ..........." +
										// p.getProjectName());

									} else {
										bugId = arLogs.getBugId();
									}
								}
								if (bugId != "0") {
									if (StringUtils.isNotEmpty(arLogs.getBugStatus())) {
										if (arLogs.getBugStatus().equals("Reopen")
												|| arLogs.getBugStatus().equalsIgnoreCase("Re-open")) {
											Status = "Execution data has been updated and Bug has been reopened";
										} else if (arLogs.getBugStatus().equals("Closed")) {
											Status = "Execution data has been updated and Bug has been closed";
										} else {
											Status = "Execution Data has been Updated and Bug Has been Updated in your existing Bug";
										}
									}

								} else {
									Status = "Execution Data has been Updated but unable to create bug as"
											+ arLogs.getBugId() + " is not present";
								}

							}
						} catch (Exception e) {
							e.printStackTrace();
							Status = "Data has been saved but unable to create bugID!!!";
						}
					}
					if (bugId != null) {
						caseTable.setBugId(bugId.toString());
						// caseTable.setBugSummary(bugSum);
						mannualExecutionLogsDao.saveBugId1(caseTable);

					} else {
						if (StringUtils.isNotEmpty(caseTable.getBugId())) {
							bugId = caseTable.getBugId();
						}
					}
				}
				try {
					saveLatestTestcaseStatus1(caseTable);// update status in Manual Testcase Table
					if (bugId != null) {
						saveToBugDetails1(bugId, Integer.parseInt(arLogs.getBugToolId()), caseTable);
					}
				} catch (Exception e) {
					System.err.println("Could not be saved to Test case table!!");
				}

				list.add(Status);
				list.add(bugId);
			}
			response.setServiceStatus("200");
			response.setServiceResponse(list);
		} catch (Exception e) {
			list = new ArrayList<Object>();
			e.printStackTrace();
			Status = "Test Case Execution Not Saved";
			response.setServiceStatus("400");
			list.add(Status);
			// response.setServiceResponse(Status);

		}
		response.setServiceResponse(list);
		return response;
	}

	public ServiceResponse saveTestExceutionRobo(RobosoftJiraAutomationModel robo) {
		ServiceResponse serviceResponse = new ServiceResponse();

		String bugId = null;

		if (robo.getRunID() != null && robo.getStatus() != null) {

			try {

				ProjectDeatilsMeta projectDetails = jiraService.projectCheck(robo);

				if (projectDetails != null) {

					bugId = jiraService.insert_In_Jira_Robo(robo, projectDetails);
					if (bugId != null && bugId != "") {
						serviceResponse.setServiceResponse(bugId);
						serviceResponse.setServiceStatus("Success");
					} else {
						serviceResponse.setServiceError(bugId);
						serviceResponse.setServiceStatus("Something went wrong");
					}

				} else {
					serviceResponse.setServiceError("Project is unavailable in APTMT");
					serviceResponse.setServiceStatus("Create a project in APTMT with the same KEY of jira project");

				}

			}

			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return serviceResponse;
	}

	/*
	 * public ServiceResponse saveAttachment(MultipartFile file, Integer testCaseID,
	 * String uploadedBy) throws IOException {
	 * 
	 * ServiceResponse serviceResponse = new ServiceResponse(); try { String
	 * attachmentName = file.getOriginalFilename(); String
	 * content=file.getContentType(); byte[] fileData=file.getBytes();
	 * if(file.getSize()<=10485760 && file.getSize()>=0) { long
	 * fileSize=file.getSize();
	 * serviceResponse=attachmentDao.saveAttachment(attachmentName,fileData,fileSize
	 * ,uploadedBy,testCaseID); }
	 * 
	 * } catch (Exception ex) { ex.printStackTrace(); }
	 * 
	 * 
	 * return serviceResponse; }
	 */
	public ServiceResponse saveAttachment(MultipartFile[] file, Integer testCaseID, String uploadedBy,
			Integer projectId) throws IOException {

		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			for (int i = 0; i < file.length; i++) {
				String attachmentName = file[i].getOriginalFilename();
				String content = file[i].getContentType();
				byte[] fileData = file[i].getBytes();
				if (file[i].getSize() <= 10485760 && file[i].getSize() >= 0) {
					long fileSize = file[i].getSize();
					serviceResponse = attachmentDao.saveAttachment(attachmentName, fileData, fileSize, uploadedBy,
							testCaseID);
				}
			}
			// System.out.println("........." + projectId);
			if (projectId != 0) {
				ProjectDeatilsMeta p = projectRepository.findByprojectID(projectId);
				String bugTool = bugRepo.findByBugToolId(Integer.valueOf(p.getIsJiraEnable()));
				MannualTestCaseExecutionLogs bugId = executionLogsRepo.findById(testCaseID).orElseGet(null);

				ProjectDeatilsMeta bugTrackorTool = jiraService.getBugToolDetails(projectId);

				if (bugTool.equalsIgnoreCase("jira")) {
					BugTrackorToolEntity bugtool = bugRepo.findByBugTrackingToolName(bugTool).get(0);
					try {
						jiraService.addAttachmentToIssue(file, bugTrackorTool, bugId);
						serviceResponse.setServiceResponse(serviceResponse + " and in Jira as well..");
					} catch (Exception e) {
						serviceResponse.setServiceResponse(serviceResponse + " but not in jira..");
						e.printStackTrace();
					}
				} else if (bugTool.equalsIgnoreCase("bugzilla")) {
					for (int j = 0; j < file.length; j++) {
						bugZillaService.saveAttachmentToBugApi(saveAttachmentToFile(file[j]), bugId.getBugId(),
								bugId.getProjectId());
					}

				} else if (("Mantis").equalsIgnoreCase(bugTool)) {
					mantisService.addAttachmentToBugApi(file, bugId);

				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return serviceResponse;
	}

	public String saveAttachmentToFile(MultipartFile file) throws IOException {
		String response = null;
		try {
			String filename = file.getOriginalFilename();
			File fileAttachment = new File(temporaryPath);
			if (!fileAttachment.exists()) {
				fileAttachment.mkdirs();
			}
			byte[] bytes = file.getBytes();
			Path path = Paths.get(temporaryPath + File.separator + filename, new String[0]);
			Files.write(path, bytes, new OpenOption[0]);
			response = path.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public ServiceResponse changeAssiginTo(String newAssiginId, String logsId) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			MannualTestCaseExecutionLogs caseTable = mannualExecutionLogsDao.changeAssiginTo(newAssiginId, logsId);
			if (caseTable != null) {
				Status = "Test Case has been assigned to new ID";
			}
			response.setServiceStatus("200");
			response.setServiceResponse(Status);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Unable to Assign Test Case";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse changeMulAssiginTo(List<Integer> logsId, String mulAssiginName) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			if (logsId != null) {
				for (Integer id : logsId) {
					Integer caseTable = mannualExecutionLogsDao.changeMulAssiginTo(id, mulAssiginName);
				}
				Status = "Test Case has been assigned to new ID";
				response.setServiceStatus("200");
				response.setServiceResponse(Status);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Unable to Assign Test Case";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse getOverallRunStatus(Integer runId) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			OverallRunStatus runStatus = mannualExecutionLogsDao.getOverallRunStatus(runId);
			Status = "Test Case has been assigned to new ID";
			response.setServiceStatus("200");
			response.setServiceResponse(runStatus);
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse getOverallRunActionStatus(Integer runId) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			OverallRunActionStatus runStatus = mannualExecutionLogsDao.getOverallRunActionStatus(runId);
			response.setServiceStatus("200");
			response.setServiceResponse(runStatus);
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse getOverallDefecttatus(Integer runId) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			DefectStatusBo defectStatusBo = mannualExecutionLogsDao.getOverallDefecttatus(runId);
			response.setServiceStatus("200");
			response.setServiceResponse(defectStatusBo);
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse getAllRun(Integer projectId) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<MannualRunTable> activeRun = mannualReRunDao.getActiveRunRerun(projectId);
			response.setServiceStatus("200");
			if (activeRun != null)
				response.setServiceResponse(activeRun);
			else {
				response.setServiceResponse("No Active Run");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse getDetailsReportByRun(Integer runId) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<MannualTestCaseExecutionLogs> runTestCaseDetails = null;
		String formattedTime = "";
		try {
			runTestCaseDetails = mannualExecutionLogsDao.getDetailsReportByRun(runId);
			for (MannualTestCaseExecutionLogs arr : runTestCaseDetails) {
				if (arr.getReportExcutionTime() != null) {
					int hours = Integer.parseInt(arr.getReportExcutionTime()) / 3600;
					int secondsLeft = Integer.parseInt(arr.getReportExcutionTime()) - hours * 3600;
					int minutes = secondsLeft / 60;
					int seconds = secondsLeft - minutes * 60;

					if (hours < 10)
						formattedTime += "0";
					formattedTime += hours + ":";

					if (minutes < 10)
						formattedTime += "0";
					formattedTime += minutes + ":";

					if (seconds < 10)
						formattedTime += "0";
					formattedTime += seconds;

				} else {
					formattedTime = null;
				}
				arr.setReportExcutionTime(formattedTime);
				// System.out.println("timeinsecond.." + formattedTime);
				formattedTime = "";
			}
			response.setServiceStatus("200");
			response.setServiceResponse(runTestCaseDetails);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getProject(String projectname, int projectid) {

		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			ProjectDeatilsMeta projectmeta = projectRepository.findByprojectID(projectid);
			// System.out.println("getProject=============" +
			// projectmeta.getProjectStartDate());
			if (projectmeta != null) {
				response.setServiceStatus("200");
				response.setServiceResponse(projectmeta);
			} else {
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}

		} catch (Exception e) {
			e.printStackTrace();
			Status = "Your Project Has already Exist";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse editsaveProject(ProjectDeatilsMeta projectDeatilsMeta) {
		/*
		 * //System.out.println("save projecty" + uploadData); ServiceResponse response
		 * = new ServiceResponse(); Integer count = 0; String Status =
		 * "Some Things Went Wrong..We are Unable to Process Your Request"; try { if
		 * (uploadData.getProjectName() != null && uploadData.getApplicationName() !=
		 * null && uploadData.getProjectStartDate() != null) { count = (Integer)
		 * projectDao.saveProjectMetaDetails(uploadData); Status =
		 * "Your Project Has been created Sucessfully"; } else { Status =
		 * "Kindly filled all the required Field"; } response.setServiceStatus("200");
		 * response.setServiceResponse(Status); } catch (Exception e) {
		 * e.printStackTrace(); Status = "Your Project Has already Exist";
		 * response.setServiceStatus("400"); response.setServiceResponse(Status);
		 * 
		 * // TODO: handle exception } return response;
		 */
		return null;
	}

	public ServiceResponse getdealayedRun(Integer projectId) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<MannualReRunTable> completedRun = mannualReRunDao.getRundelaystatus(projectId);
			for (MannualReRunTable arr : completedRun) {
				String date = arr.getTargetEndDate();
				Double exexutiontime = arr.getTimeTake();
				if (exexutiontime != null) {

					Double day = exexutiontime / (24 * 3600);

					exexutiontime = exexutiontime % (24 * 3600);
					Double hour = exexutiontime / 3600;

					exexutiontime %= 3600;
					Double minutes = exexutiontime / 60;

					long mStrippedValue = 0;
					long mStrippedValues = 0;
					long mStrippedValuess = 0;

					// System.out.println("day" + day + ".....hour..." + hour + "...miniutes.." +
					// minutes);
					if (day != null) {
						// String dayscount=day.toString();
						mStrippedValue = new Double(day).longValue();
						// days=Integer.parseInt(dayscount);

					}
					if (hour != null) {
						// String hourcount=hour.toString();
						mStrippedValues = new Double(hour).longValue();
						// hours=Integer.parseInt(hourcount);
					}
					if (minutes != null) {
						// String minutescount=minutes.toString();
						mStrippedValuess = new Double(minutes).longValue();
						// minutess=Integer.parseInt(minutescount);
					}
					String strd = Long.toString(mStrippedValue);
					String strh = Long.toString(mStrippedValues);
					String strm = Long.toString(mStrippedValuess);
					arr.setTimetakendhm(strd + "day: " + strh + "hrs: " + strm + "min");
					// System.out.println("day......" + mStrippedValue + "hor...." + mStrippedValues
					// + "...minutes.remove."
					// + mStrippedValuess);
				} else {
					arr.setTimetakendhm("0day: 0hrs: 0min");
				}
				if (date != null) {
					date = date.substring(0, 10);
				} else if (date == null) {
					date = "";
				}

				arr.setTargetEndDate(date);

			}

			response.setServiceStatus("200");
			if (completedRun != null)
				response.setServiceResponse(completedRun);
			else {
				response.setServiceResponse("No Active Run");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse getInactiveRun(Integer projectId) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<MannualReRunTable> completedRun = mannualReRunDao.getInactiveRun(projectId);

			for (MannualReRunTable arr : completedRun) {
				Double exexutiontime = arr.getTimeTake();

				if (exexutiontime != null) {
					Double day = exexutiontime / (24 * 3600);
					// System.out.println("Time of execution.day..." + day + "... execution time.."
					// + exexutiontime);
					exexutiontime = exexutiontime % (24 * 3600);
					Double hour = exexutiontime / 3600;

					exexutiontime %= 3600;
					Double minutes = exexutiontime / 60;

					long mStrippedValue = 0;
					long mStrippedValues = 0;
					long mStrippedValuess = 0;

					// System.out.println("day" + day + ".....hour..." + hour + "...miniutes.." +
					// minutes);
					if (day != null) {
						// String dayscount=day.toString();
						mStrippedValue = new Double(day).longValue();
						// days=Integer.parseInt(dayscount);

					}
					if (hour != null) {
						// String hourcount=hour.toString();
						mStrippedValues = new Double(hour).longValue();
						// hours=Integer.parseInt(hourcount);
					}
					if (minutes != null) {
						// String minutescount=minutes.toString();
						mStrippedValuess = new Double(minutes).longValue();
						// minutess=Integer.parseInt(minutescount);
					}
					String strd = Long.toString(mStrippedValue);
					String strh = Long.toString(mStrippedValues);
					String strm = Long.toString(mStrippedValuess);
					arr.setTimetakendhm(strd + "day: " + strh + "hrs: " + strm + "min");
					// System.out.println("day......" + mStrippedValue + "hor...." + mStrippedValues
					// + "...minutes.remove."
					// + mStrippedValuess);
				} else {
					arr.setTimetakendhm("0day: 0hrs: 0min");
				}
				String date = arr.getTargetEndDate();
				if (date != null) {
					date = date.substring(0, 10);
				} else {
					date = "";
				}
				arr.setTargetEndDate(date);

			}
			response.setServiceStatus("200");
			if (completedRun != null)
				response.setServiceResponse(completedRun);
			else {
				response.setServiceResponse("No Active Run");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse checkTSuites(Integer testsuiteid) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			MannualTestSuites ts = mannualTestSuitesDAO.getTestSuitsDetails(testsuiteid);

			if (ts.getIsDeleted().equals("Y")) {
				response.setServiceStatus("202");
				response.setServiceResponse("Sorry You Can't Active This As The Respective Test Suite Is Deleted ");
			} else {
				response.setServiceStatus("200");
				response.setServiceResponse("Success");

			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse("Fail");
		}
		return response;
	}

	public ServiceResponse deleteProject(int projectid, String isPtype) {
		// Session session = sessionFactory.getCurrentSession();
//		Session session=entityManager.unwrap(Session.class);
		ServiceResponse response = new ServiceResponse();
		ProjectDeatilsMeta projectmeta = null;
		Integer pdelete = 0;
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			ProjectDeatilsMeta serachprojectautofalse = projectRepository.findByprojectID(projectid);

			if (isPtype.equals("automation")) {
				if (serachprojectautofalse.getIsMannual().equals("false")) {
					List<UpdateLogs> updatelogtable = updateLogsRepository.findByProjectId(projectid);
					if (updatelogtable.isEmpty()) {
					} else {
						Integer updatelogdeletforauto = updateLogsRepository.deleteByAutoUpdateLogProjectID(projectid);

					}
					AutomationMetadata projectdeleteauto = automationMetaRepository.findByProjectid(projectid);

					if (projectdeleteauto != null) {
						Integer autopodelete = automationMetaRepository.deleteByAutoProjectID(projectid);
					}
					projectmeta = projectDao.deleteprojectbyid(projectid);
					pdelete = projectAccessRepository.deleteByProjectID(projectid);
					if (projectmeta != null) {
						response.setServiceStatus("200");
						response.setServiceResponse(projectmeta);
					} else {
						response.setServiceStatus("400");
						response.setServiceResponse("Something went wrong");

					}

				} else if (serachprojectautofalse.getIsMannual().equals("true")) {
					Integer autopodelete = 0;
					List<UpdateLogs> updatelogtable = updateLogsRepository.findByProjectId(projectid);
					if (updatelogtable.isEmpty()) {

					} else {
						Integer updatelogdeletforauto = updateLogsRepository.deleteByAutoUpdateLogProjectID(projectid);

					}
					AutomationMetadata projectdeleteauto = automationMetaRepository.findByProjectid(projectid);

					if (projectdeleteauto != null) {
						autopodelete = automationMetaRepository.deleteByAutoProjectID(projectid);
					} else {

					}

					projectRepository.updateAutodataone("false", projectid);
					/*
					 * updatesdata.setIsAutomation("false"); session.update(updatesdata);
					 */
					if (autopodelete != null) {
						response.setServiceStatus("200");
						response.setServiceResponse(projectmeta);
					} else {
						response.setServiceStatus("400");
						response.setServiceResponse("Something went wrong");

					}

				} else {

				}

			} else if (isPtype.equals("mannual")) {
				if (serachprojectautofalse.getIsAutomation().equals("false")) {
					ProjectDeatilsMeta projectmetas = projectDao.deleteprojectbyid(projectid);
					Integer pdeletes = projectAccessRepository.deleteByProjectID(projectid);

					if (projectmetas != null) {
						response.setServiceStatus("200");
						response.setServiceResponse(projectmetas);
					} else {
						response.setServiceStatus("400");
						response.setServiceResponse("Something went wrong");

					}

				} else if (serachprojectautofalse.getIsAutomation().equals("true")) {
					projectRepository.updateAutodata("false", projectid);
					// updatesdata.setIsMannual("false");
					// session.update(updatesdata);
					response.setServiceStatus("200");
					// response.setServiceResponse(projectmeta);

				}
			}

			else {
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");

			}

		} catch (Exception e) {
			e.printStackTrace();
			Status = "Your Project Has already Exist";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public Boolean forceRename(File source, File target) throws IOException {
		Boolean isAutoProjectRenamed = false;
		source.setExecutable(true);
		// if (target.exists()) target.delete();
		if (target.exists())
			FileUtils.deleteQuietly(target);
		isAutoProjectRenamed = source.renameTo(target);

		/*
		 * if(isAutoProjectRenamed==false) { FileUtils.copyDirectory(source, target); }
		 */
		return isAutoProjectRenamed;
	}

	public ServiceResponse saveEditProject(ProjectDeatilsMeta projectDeatilsMeta) {
		ServiceResponse response = new ServiceResponse();
		Integer count = 0;
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		UserDetailsEntity roleEn = (UserDetailsEntity) session.getAttribute("user");
		Integer role = roleEn.getUser_role();
		ProjectDeatilsMeta projectDeatilsMeta2 = null;
		try {
			// System.out.println("projectDeatilsMeta......." +
			// projectDeatilsMeta.getProjectStartDate()
			// + " projectDeatilsMeta.getProjectEnd...." +
			// projectDeatilsMeta.getProjectEndDate());
			if (projectDeatilsMeta.getProjectName() != null && projectDeatilsMeta.getApplicationName() != null
					&& projectDeatilsMeta.getProjectStartDate() != null) {
				// projectDeatilsMeta2=projectRepository.findByprojectName(projectDeatilsMeta.getProjectName());
				projectDeatilsMeta2 = projectRepository.findByprojectID(projectDeatilsMeta.getProjectID());

				Status = projectDao.updateProject(projectDeatilsMeta);
				// Status = "Your Project Has been updated Sucessfully";
			} else {
				Status = "Kindly filled all the required Field";
			}
			response.setServiceStatus("200");
			response.setServiceResponse(Status);
			if (("Success").equals(Status)) {
				if (!(projectDeatilsMeta2.getProjectName().equals(projectDeatilsMeta.getProjectName()))
						&& (projectDeatilsMeta2.getIsAutomation().equals("true"))) {
					try {
						File fileAutoProject = new File(
								autoProjectPath + File.separator + projectDeatilsMeta2.getProjectName());
						File fileAutoProjectToRename = new File(
								autoProjectPath + File.separator + projectDeatilsMeta.getProjectName());
						if (fileAutoProject.exists()) {
							Boolean r = forceRename(fileAutoProject, fileAutoProjectToRename);
							// System.out.println("Auto project: " + projectDeatilsMeta2.getProjectName() +
							// " renamed to: "
							// + projectDeatilsMeta.getProjectName() + "----" + r);
						}

					} catch (Exception e) {
						System.err.println("Not able to rename folder after changing project name!!!");
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Your Project Has already Exist";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

			// TODO: handle exception
		}
		return response;
	}

	/*
	 * public ServiceResponse saveEditProject(ProjectDeatilsMeta projectDeatilsMeta)
	 * { ServiceResponse response = new ServiceResponse(); Integer count = 0; String
	 * Status = "Some Things Went Wrong..We are Unable to Process Your Request";
	 * UserDetailsEntity roleEn=(UserDetailsEntity)session.getAttribute("user");
	 * Integer role=roleEn.getUser_role();
	 * 
	 * 
	 * try { //System.out.println();
	 * //System.out.println("projectDeatilsMeta......."+projectDeatilsMeta.
	 * getProjectStartDate()
	 * +" projectDeatilsMeta.getProjectEnd...."+projectDeatilsMeta.getProjectEndDate
	 * ()); if (projectDeatilsMeta.getProjectName() != null &&
	 * projectDeatilsMeta.getApplicationName() != null &&
	 * projectDeatilsMeta.getProjectStartDate() != null) { Status =
	 * projectDao.updateProject(projectDeatilsMeta); // Status =
	 * "Your Project Has been updated Sucessfully"; } else { Status =
	 * "Kindly filled all the required Field"; } response.setServiceStatus("200");
	 * response.setServiceResponse(Status); } catch (Exception e) {
	 * e.printStackTrace(); Status = "Your Project Has already Exist";
	 * response.setServiceStatus("400"); response.setServiceResponse(Status);
	 * 
	 * // TODO: handle exception } return response; }
	 */

	public ServiceResponse deleteTestSuites(String updatedByEmail, Integer testsuitesid, String testsuitesname) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			// Status = projectDao.deleteTestSuites(testsuitesid);
			Status = projectDao.updateTestSuitesAsDeleted(updatedByEmail, testsuitesid, testsuitesname);

			if (Status != "") {
				response.setServiceStatus("200");
				response.setServiceResponse(Status);
			} else {
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}

		} catch (Exception e) {
			e.printStackTrace();
			Status = "";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse testrunwisedetails(Integer projectid) {
		String Status = "no status";
		ServiceResponse response = new ServiceResponse();
		List<Testrunwisedetails> runTestCaseDetails = null;
		// List<Object[]> runTestCaseDetails = null;
		try {
			runTestCaseDetails = mannualExecutionLogsDao.testrunwisedetails(projectid);
			/* for(Testrunwisedetails testrun) */
			/*
			 * runTestCaseDetails = mannualExecutionLogsDao.testrunwisedetails(projectid);
			 */
			response.setServiceStatus("200");
			response.setServiceResponse(runTestCaseDetails);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse runwisedetails(String runlist) {
		String Status = "no status";
		ServiceResponse response = new ServiceResponse();
		List<runwisestatus> runTestCaseDetails = null;
		runlist = runlist.replaceAll(",", "','");
		try {

			runTestCaseDetails = mannualExecutionLogsDao.runwisestatuscheck(runlist);
			for (runwisestatus test : runTestCaseDetails) {
				test.setRunname("R" + test.getRunid() + "_" + test.getRunname());
			}

			response.setServiceStatus("200");
			response.setServiceResponse(runTestCaseDetails);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse projectrunwisedetails(Integer projectid) {
		String Status = "no status";
		ServiceResponse response = new ServiceResponse();
		List<runwisestatus> runTestCaseDetails = null;
		try {
			runTestCaseDetails = mannualExecutionLogsDao.runwisestatus(projectid);
			for (runwisestatus test : runTestCaseDetails) {
				test.setRunname("R" + test.getRerunId() + "_" + test.getRunname());
			}
			// //System.out.println("set
			// posible.."+runTestCaseDetails.get(0).getIncomplete());
			response.setServiceStatus("200");
			response.setServiceResponse(runTestCaseDetails);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse runwisedetails(Integer projectid) {
		String Status = "no status";
		ServiceResponse response = new ServiceResponse();
		List<runwisestatus> runTestCaseDetails = null;
		try {
			runTestCaseDetails = mannualExecutionLogsDao.runwisestatus(projectid);
			for (runwisestatus test : runTestCaseDetails) {
				test.setRunname("R" + test.getRunid() + "_" + test.getRunname());
			}

			response.setServiceStatus("200");
			response.setServiceResponse(runTestCaseDetails);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse getProjectSelf(int projectid) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			ProjectDeatilsMeta projectmeta = projectRepository.findByprojectID(projectid);
			ProjectDeatilsMeta projectdates = projectDao.findByMinutesDate(projectid);

			projectmeta.setDelayTimeProject(projectdates.getDelayTimeProject());

			if (projectmeta != null) {
				response.setServiceStatus("200");
				response.setServiceResponse(projectmeta);
			} else {
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}

		} catch (

		Exception e) {
			e.printStackTrace();
			Status = "sorry";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse runwisefailstatus(String runlist) {
		String Status = "no status";
		ServiceResponse response = new ServiceResponse();
		List<runwisestatus> runTestCaseDetails = null;
		runlist = runlist.replaceAll(",", "','");
		try {
			List<projectwisefailstatus> projectdata = mannualExecutionLogsDao.runwisefailstatus(runlist);
			for (projectwisefailstatus test : projectdata) {
				test.setRunname("R" + test.getRunid() + "_" + test.getRunname());
			}

			if (projectdata != null) {
				response.setServiceStatus("200");
				response.setServiceResponse(projectdata);
			} else {
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}

		} catch (Exception e) {
			e.printStackTrace();
			Status = "sorry";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse projectwisefailstatus(int projectid) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<projectwisefailstatus> projectdata = mannualExecutionLogsDao.projectwisefailstatus(projectid);
			for (projectwisefailstatus test : projectdata) {
				test.setRunname("R" + test.getRunId() + "_" + test.getRunname());
			}

			if (projectdata != null) {
				response.setServiceStatus("200");
				response.setServiceResponse(projectdata);
			} else {
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}

		} catch (Exception e) {
			e.printStackTrace();
			Status = "sorry";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	private static DecimalFormat df2 = new DecimalFormat("#.##");

	public ServiceResponse getProjectDetails(int projectid) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		Projectwisedetail projectdata = new Projectwisedetail();
		try {
			Long totaltestsuites = projectDao.getProjecttestsuites(projectid);
			projectdata.setTotaltestsuites(totaltestsuites);
			Long totaltestcases = projectDao.getProjecttestcases(projectid);
			projectdata.setTotaltestcases(totaltestcases);
			String[] totalrun = projectDao.getProjectrun(projectid);
			projectdata.setTotalrun(totalrun[0] == null ? 0l : Long.valueOf(totalrun[0]));
			projectdata.setpjname(totalrun[1]);
			Long currentrun = projectDao.getProjectcurrentrun(projectid);
			projectdata.setCurrentrun(currentrun);
			Long totalexecuted = projectDao.getProjecttotalexecuted(projectid);
			projectdata.setTotalexecuted(totalexecuted);
			Long totalpass = projectDao.getprojecttotalpass(projectid);
			// System.out.println("Total Pass" + totalpass);
			projectdata.setTotalexecuted(totalexecuted);
			float test = totalpass;
			float test1 = totalexecuted;
			projectdata.setSuccesspercent(df2.format((test * 100) / test1));

			if (projectdata != null) {
				response.setServiceStatus("200");
				response.setServiceResponse(projectdata);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Status = "sorry";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse deletegroup(Integer groupId) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			Status = projectDao.deletegroup(groupId);
			;
			if (Status != "") {
				response.setServiceStatus("200");
				response.setServiceResponse(Status);
			} else {
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public void updateDeletedTCStatusPFNE(MannualTestCaseExecutionLogs tc) {
		MannualTestCaseExecutionLogs tcStatus;
		String status = "Test Execution Log deleted";
		try {
			tcStatus = mannualExecutionLogsDao.getTCLatestStatusFromTimestamp(tc.getAbsoluteTCID());
			mannualTestCaseDao.updateTestCaseLatestStatus(tcStatus);
		}

		catch (Exception e) {
			e.printStackTrace();
			status = status + " but it would not reflect in Dashboard.";
		}
		// System.out.println(status + "--" + tc.getId());

	}

	public ServiceResponse deletetestruncaseid(MannualTestCaseExecutionLogs tcToBeDeleted) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			Status = projectDao.deletetestruncaseid(tcToBeDeleted.getId());

			if (Status != "") {
				if (("Success").equals(Status)) {
					updateDeletedTCStatusPFNE(tcToBeDeleted);

				}
				response.setServiceStatus("200");
				response.setServiceResponse(Status);
			} else {
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse uploadDocuments(MultipartFile file, String pId, String pName, String documentsName,
			String isPublic, String createdBy) {
		ServiceResponse serviceResponse = new ServiceResponse();
		List<ProjectDocument> dOccurrence = null;

		try {
			// System.out.println();
			// System.out.println("1.....Doc....");
			String filename = file.getOriginalFilename();
			String Location = attachmentFolderPath + File.separator + pName + File.separator + documentsName;
			ProjectDocument document = new ProjectDocument();
			document.setProjectId(Integer.parseInt(pId));
			document.setDocumentName(filename);
			document.setFilePath(Location);
			document.setDocumentsType(documentsName);
			document.setIsPublic(isPublic);
			document.setUploadBy(createdBy);
			// System.out.println("2.....Doc....");

			List<ProjectDocument> existingDocs = projectDocumentDao
					.checkForExistingEntryByPrimary(Integer.parseInt(pId), documentsName, filename);
			int l = existingDocs.size();
			if (l > 0) {
				serviceResponse.setServiceResponse("Duplicate");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
				serviceResponse.setServiceError("");
				return serviceResponse;
			}

			/*
			 * List<ProjectDocument> dOccurrence
			 * =projectDocumentDao.getProjectDocsByPidAndDocType(pId,documentsName);
			 */
			if (isPublic.equals("Y")) {
				dOccurrence = projectDocumentDao.getProjectDocsByPidAndDocTypeIsPublicY(pId, documentsName, createdBy,
						isPublic);

			} else if (isPublic.equals("N")) {

				dOccurrence = projectDocumentDao.getProjectDocsByPidAndDocTypeIsPublicN(pId, documentsName, createdBy,
						isPublic);

			}
			int length = dOccurrence.size();

			if (documentsName.equals("Other Document")) {
				if (length == 5) {

					serviceResponse.setServiceResponse("Max Number Of Occurences");
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					serviceResponse.setServiceError("");
					return serviceResponse;
				}

			} else {
				if (length == 3) {
					serviceResponse.setServiceResponse("Max Number Of Occurences");
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					serviceResponse.setServiceError("");
					return serviceResponse;
				}
			}
			Integer i = projectDocumentDao.saveDocumentDetails(document);
			// System.out.println("3.....Doc....");

			if (i == 0) {
				serviceResponse.setServiceResponse("File Already Exist");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			} else {
				File fileAttachment = new File(Location);
				if (!fileAttachment.exists()) {
					fileAttachment.mkdirs();
				}
				byte[] bytes = file.getBytes();
				Path path = Paths.get(Location + File.separator + filename, new String[0]);
				Files.write(path, bytes, new OpenOption[0]);
				serviceResponse.setServiceResponse("File uploaded successfully");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				serviceResponse.setServiceError("");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			String i = e.getMessage();
			// System.out.println("In SQLException<<<<<<<<.....Value Of i=" + i +
			// ">>>>>>>");
			if (i == "1796")
				serviceResponse.setServiceResponse("Duplicate File");

			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			serviceResponse.setServiceError("");
		}

		catch (Exception e) {
			e.printStackTrace();
			String i = e.getLocalizedMessage();
			// System.out.println("In
			// MySQLIntegrityConstraintViolationException<<<<<<<<.....Value Of i=" + i +
			// ">>>>>>>");
			// System.out.println("4.....Doc....");
			serviceResponse.setServiceResponse("Could not upload file");
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			serviceResponse.setServiceError("");
		}
		return serviceResponse;
	}

	public ServiceResponse getPublicDocument() {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<ProjectDocument> publicDocument = projectDocumentDao.getPublicDocument();
			if (publicDocument != null) {
				response.setServiceStatus("200");
				response.setServiceResponse(publicDocument);
			} else {
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}

		} catch (Exception e) {
			e.printStackTrace();
			Status = "sorry";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse getProjectDocument(Integer pId) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<ProjectDocument> projectDocument = projectDocumentDao.getProjectDocument(pId);
			if (projectDocument != null) {
				response.setServiceStatus("200");
				response.setServiceResponse(projectDocument);
			} else {
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}

		} catch (Exception e) {
			e.printStackTrace();
			Status = "sorry";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ProjectDocument projectDocumentDao(Integer id) {
		return projectDocumentDao.projectDocumentDao(id);
	}

	public Resource loadFileAsResource(ProjectDocument document) throws FileNotFoundException {
		Resource resource = null;
		try {
			// String Location = attachmentFolderPath +
			// File.separator+document.getProjectName()+File.separator+
			// document.getDocumentsType()+ File.separator+document.getDocumentName();

			String Location = document.getFilePath() + File.separator + document.getDocumentName();
			File file = new File(Location);

			if (file.exists()) {
				resource = new FileSystemResource(Location);
			}
		} catch (Exception ex) {
			// throw new FileNotFoundException("File not found ");
		}
		return resource;
	}

	public Resource templatefile(ProjectDocument document) throws FileNotFoundException {
		Resource resource = null;
		try {
			String Location = document.getFilePath() + File.separator + document.getDocumentName();
			File file = new File(Location);

			if (file.exists()) {
				resource = new FileSystemResource(Location);
			}
		} catch (Exception ex) {
			// throw new FileNotFoundException("File not found ");
		}
		return resource;
	}

	public ServiceResponse getallActiveRun(Integer projectID) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<MannualReRunTable> activeRun = mannualReRunDao.getActiveRunrunning(projectID);
			for (MannualReRunTable test : activeRun) {
				test.setRunName("R" + test.getId() + "_" + test.getRunName());
			}

			response.setServiceStatus("200");
			if (activeRun != null)
				response.setServiceResponse(activeRun);
			else {
				response.setServiceResponse("No Active Run");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;

	}

	public ServiceResponse configProjects(Integer user_role, String userid) {
		String Status = "No Project";

		// System.out.println(user_role);
		ServiceResponse response = new ServiceResponse();
		List<ProjectDeatilsMeta> projectList = new ArrayList<>();
		try {
			if (user_role == 100) {
				projectList = projectDao.getAllProject();
				if (projectList.size() == 0) {
					// System.out.println("Size Zero");
					response.setServiceStatus(ServiceResponse.STATUS_FAIL);
					response.setServiceResponse("");
					response.setServiceError("Data not found");

				} else {
					// System.out.println("Size not Zero");
					response.setServiceResponse(projectList);
					response.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

				}

			} else if (user_role == 101) {
				/*
				 * //System.out.println(user_role); //System.out.println(userid);
				 */
				projectList = projectDao.getAllProjectForManager(userid);
				// System.out.println(projectList.size());
				if (projectList.size() == 0) {
					// System.out.println("Size Zero");
					response.setServiceStatus(ServiceResponse.STATUS_FAIL);
					response.setServiceResponse("");
					response.setServiceError("Data not found");

				} else {
					// System.out.println("Size not Zero");
					response.setServiceResponse(projectList);
					response.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

				}

			} else {
				Status = "Something went wrong";
				response.setServiceStatus("400");
				response.setServiceResponse(Status);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getDocumentTypes() {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<String> documentTypes = projectDocumentDao.getDocumentTypes();
			if (documentTypes != null) {
				response.setServiceStatus("200");
				response.setServiceResponse(documentTypes);
			} else {
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}

		} catch (Exception e) {
			e.printStackTrace();
			Status = "sorry";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}

		return response;
	}

	public ServiceResponse deleteDocument(int id, String filePath, String documentName) {
		// TODO Auto-generated method stub
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			Integer i = projectDocumentDao.deleteDocumentById(id);
			if (i == 0) {
				response.setServiceResponse("File Doesn't Exist");
				response.setServiceStatus(ServiceResponse.STATUS_FAIL);
			} else {
				try {
					String Location = filePath + File.separator + documentName;
					File fileAttachment = new File(Location);
					if (!fileAttachment.exists()) {
						// System.out.println("<<<<<<<File Not Exists>>>>>");
					} else {
						fileAttachment.delete();
						response.setServiceResponse("File deleted successfully");
						response.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
						response.setServiceError("");
					}
				}

				catch (Exception e) {
					e.printStackTrace();
					response.setServiceResponse("Could not Delete file");
					response.setServiceStatus(ServiceResponse.STATUS_FAIL);
					response.setServiceError("");

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceResponse("Could not delete file");
			response.setServiceStatus(ServiceResponse.STATUS_FAIL);
			response.setServiceError("");
		}

		return response;
	}

	public ServiceResponse getResourcewiseexecution(int runid) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<ResourceDateWiseExecution> resourcewise = mannualReRunDao.testerExecution(runid);
			if (resourcewise != null) {
				response.setServiceStatus("200");
				response.setServiceResponse(resourcewise);
			} else {
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}

		} catch (Exception e) {
			e.printStackTrace();
			Status = "sorry";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}

		return response;
	}

	public ServiceResponse getRoletypebyproject(Integer projectid, int id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			String usertype = congifrepo.getRoletypebyproject(projectid, id);

			if (usertype != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(usertype);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Readonly");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}

		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}
	/*
	 * public ServiceResponse getResourseEfforts(ResourceRequestBo
	 * resourceRequestBo) { ServiceResponse serviceResponse = new ServiceResponse();
	 * //System.out.println(resourceRequestBo.getFromDate()+"           "
	 * +resourceRequestBo.getToDate()); try { Map<String, List<ResourceResponseBo>>
	 * map = mannualExecutionLogsDao.getResourseEfforts(resourceRequestBo);
	 * 
	 * if (!map.isEmpty()) { serviceResponse.setServiceError("");
	 * serviceResponse.setServiceResponse(map);
	 * serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
	 * 
	 * } else { serviceResponse.setServiceError("");
	 * serviceResponse.setServiceResponse("No Data Available");
	 * serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
	 * 
	 * }
	 * 
	 * 
	 * 
	 * } catch (Exception e) { e.printStackTrace();
	 * serviceResponse.setServiceError("Something Went Wrong");
	 * serviceResponse.setServiceResponse(e.getMessage());
	 * serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
	 * 
	 * } return serviceResponse; }
	 */

	// Lipsa
	public ServiceResponse getResourseEfforts(ResourceRequestBo resourceRequestBo) {
		ServiceResponse serviceResponse = new ServiceResponse();
		// //System.out.println(resourceRequestBo.getFromDate()+"
		// "+resourceRequestBo.getToDate());
		try {
			Map<String, List<ResourceResponseBo>> exeTestCase = mannualExecutionLogsDao
					.getResourseEfforts(resourceRequestBo, "executed");
			/*
			 * Map<String, List<ResourceResponseBo>>
			 * freshRun=mannualExecutionLogsDao.getResourseEfforts(resourceRequestBo,
			 * "fresh_run"); Map<String, List<ResourceResponseBo>>[] map = new Map[]
			 * {exeTestCase, freshRun};
			 * 
			 * Map<String, List<ResourceResponseBo>> merged = new HashMap<>();
			 * for(Map<String, List<ResourceResponseBo>> input:map){ for(Entry<String,
			 * List<ResourceResponseBo>> e:input.entrySet()){ merged.merge(e.getKey(),
			 * e.getValue(), (v1,v2)->{return v1+","+v2;}); } }
			 */
			// map.putAll(freshRun);
			/*
			 * freshRun.forEach( (key, value) -> exeTestCase.merge( key, value, (v1, v2) ->
			 * v1.equals(v2) ? v1 : v1 + "," + v2) );
			 */
			/*
			 * Stream combined = Stream.concat(exeTestCase.entrySet().stream(),
			 * freshRun.entrySet().stream());
			 */

			if (!exeTestCase.isEmpty()) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(exeTestCase);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("No Data Available");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}

		} catch (Exception e) {
			e.printStackTrace();
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	// Lipsa
	public ServiceResponse getResourseEffortsFreshRun(ResourceRequestBo resourceRequestBo) {
		ServiceResponse serviceResponse = new ServiceResponse();
		// //System.out.println(resourceRequestBo.getFromDate()+"
		// "+resourceRequestBo.getToDate());
		try {
			Map<String, List<ResourceResponseBo>> freshRun = mannualExecutionLogsDao
					.getResourseEfforts(resourceRequestBo, "fresh_run");
			if (!freshRun.isEmpty()) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(freshRun);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("No Data Available");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}

		} catch (Exception e) {
			e.printStackTrace();
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse tableResourceEfforts(ResourceRequestBo resourceRequestBo) {
		ServiceResponse serviceResponse = new ServiceResponse();

		try {
			List<ResourceResponseBo> map = mannualExecutionLogsDao.tableResourceEfforts(resourceRequestBo);

			if (!map.isEmpty()) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(map);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("No Data Available");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse updateTestGroupID(Integer caseId, Integer groupId) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			MannualTestCaseTable oldCaseData = mannualTestCaseRepo.findTestCaseBySrID(caseId);
			Integer srNo = mannualTestCaseRepo.findMaxTestCasesId(groupId);
			if (oldCaseData.getTestGroupId() != groupId) {

				oldCaseData.setTestGroupId(groupId);
				if (srNo != null)
					oldCaseData.setSrNo(String.valueOf(srNo + 1));
				else
					oldCaseData.setSrNo(String.valueOf(1));
				mannualTestCaseRepo.save(oldCaseData);
			}
			response.setServiceStatus("200");
			response.setServiceResponse(oldCaseData);
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse getComponetDetails(String pName, Integer pId) {
		ServiceResponse response = new ServiceResponse();
		List<Object> list = new ArrayList<Object>();
		ResponseEntity<Object> object = null;
		String Status = "Something Went Wrong..We are Unable to Process Your Request..";
		try {
			ProjectDeatilsMeta projectDeatils = projectDao.getProjectDetails(pId);
			if (projectDeatils != null) {
				if (projectDeatils.getToolName().equalsIgnoreCase("Bugzilla")) {
					try {
						object = bugZillaService.getProductDetails(pName, pId);
						if (object.getStatusCodeValue() == 200) {
							list.add(projectDeatils);
							list.add(object.getBody());
							response.setServiceStatus("200");
							response.setServiceResponse(list);

						}
					} catch (Exception e) {
						response.setServiceError(ServiceResponse.STATUS_FAIL);
						response.setServiceStatus("201");
						response.setServiceResponse("Kindly create a product in Bugzilla");
					}
				} else if (projectDeatils.getToolName().equalsIgnoreCase("Jira")) {
					try {
						list.add(projectDeatils);
						// JiraService jiraService=new JiraService();
						List issueTypeAndPriorities = jiraService.getJiraIssueType(projectDeatils.getBugTool(),
								projectDeatils.getBugToolPassword(), projectDeatils.getBugToolURL(), pName);
						list.add(issueTypeAndPriorities);
						List epics = jiraService.getJiraEpics(projectDeatils.getBugTool(),
								projectDeatils.getBugToolPassword(), projectDeatils.getBugToolURL(), pName);
						list.add(epics);
						response.setServiceStatus("200");
						response.setServiceResponse(list);
					} catch (Exception e) {
						response.setServiceError(ServiceResponse.STATUS_FAIL);
						response.setServiceStatus("201");
						response.setServiceResponse("Kindly create a project in Jira");
						list.add("Kindly create a project in Jira");
					}

				} else if (projectDeatils.getToolName().equalsIgnoreCase("Mantis")) {
					try {
						response = mantisService.getProjectBugs(projectDeatils.getProjectName(),
								projectDeatils.getProjectID());
						if (response == null) {
							response.setServiceError(ServiceResponse.STATUS_FAIL);
							response.setServiceStatus("201");
							response.setServiceResponse("Kindly create a project in Mantis");
							list.add("Kindly create a project in Mantis");
						} else {
							list.add(projectDeatils);
							// JiraService jiraService=new JiraService();
							response.setServiceStatus("200");
							response.setServiceResponse(list);
						}
					} catch (Exception e) {
						response.setServiceError(ServiceResponse.STATUS_FAIL);
						response.setServiceStatus("201");
						response.setServiceResponse("Kindly create a project in Mantis");
						list.add("Kindly create a project in Mantis");
					}

				} else if (projectDeatils.getToolName().equalsIgnoreCase("Redmine")) {
					try {
						// System.out.println("Inside Redmine");
						RedmineDTO redmineDTO = new RedmineDTO();
						redmineDTO.setTmtProjectID(pId);
						redmineDTO.setIdentifier(pName);
						response = redmineService.getRedmineProjectByIdentifier(redmineDTO);
						if (response == null) {
							response.setServiceError(ServiceResponse.STATUS_FAIL);
							response.setServiceStatus("201");
							response.setServiceResponse("Kindly create a project in Redmine");
							list.add("Kindly create a project in Redmine");
						} else {
							list.add(projectDeatils);
							response.setServiceStatus("200");
							response.setServiceResponse(list);
						}
					} catch (Exception e) {
						e.printStackTrace();
						response.setServiceError(ServiceResponse.STATUS_FAIL);
						response.setServiceStatus("201");
						response.setServiceResponse("Kindly create a project in Redmine");
						list.add("Kindly create a project in Redmine");
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse fetchBugComment(Integer pId, Integer bugID) {
		ServiceResponse response = new ServiceResponse();
		List<Object> list = new ArrayList<Object>();
		ResponseEntity<Object> object = null;
		String Status = "Something Went Wrong..We are Unable to Process Your Request..";
		try {
			ProjectDeatilsMeta projectDeatils = projectDao.getProjectDetails(pId);
			if (projectDeatils != null) {
				if (projectDeatils.getToolName().equalsIgnoreCase("Bugzilla")) {
					try {
						response.setServiceResponse(bugZillaService.fetchBugComment(bugID, pId));
						/*
						 * object = bugZillaService.fetchBugComment(bugID); if
						 * (object.getStatusCodeValue() == 200) { //list.add(projectDeatils);
						 * //list.add(object.getBody()); response.setServiceStatus("200");
						 * response.setServiceResponse(bugZillaService.fetchBugComment(bugID));
						 * 
						 * }
						 */
					} catch (Exception e) {
						response.setServiceError(ServiceResponse.STATUS_FAIL);
						response.setServiceStatus("201");
						response.setServiceResponse("Something went wrong!!");
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse reopenBugzillaIssue(String pName, Integer pId, Integer bId) {
		ServiceResponse response = new ServiceResponse();
		List<Object> list = new ArrayList<Object>();
		ResponseEntity<Object> object = null;
		String Status = "Something Went Wrong..We are Unable to Process Your Request..";
		try {
			ProjectDeatilsMeta projectDeatils = projectDao.getProjectDetails(pId);
			if (projectDeatils != null) {
				if (projectDeatils.getToolName().equalsIgnoreCase("Bugzilla")) {
					try {
						object = bugZillaService.getProductDetails(pName, pId);
						if (object.getStatusCodeValue() == 200) {
							list.add(projectDeatils);
							list.add(object.getBody());
							response.setServiceStatus("200");
							response.setServiceResponse(list);

						}
					} catch (Exception e) {
						response.setServiceError(ServiceResponse.STATUS_FAIL);
						response.setServiceStatus("201");
						response.setServiceResponse("Kindly create a product in Bugzilla");
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public ServiceResponse attchmentFetch(Integer testId) {
		ServiceResponse response = new ServiceResponse();
		List<Object> list = new ArrayList<Object>();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<AttachmentFetch> attchmentFet = attachmentDao.attchmentFetch(testId);
			// System.out.println("response.." + response);
			response.setServiceStatus("200");
			response.setServiceResponse(attchmentFet);

		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public List<AttachmentFetch> getImage(Integer testId, String attachname) {
		Resource resource = null;
		List<AttachmentFetch> attchmentFet = attachmentDao.attchmentFetchdown(testId, attachname);
		return attchmentFet;
	}

	// added sakti
	public ServiceResponse attchmentFetch1(Integer testId, String bugId) {
		ServiceResponse response = new ServiceResponse();
		List<Object> list = new ArrayList<Object>();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<AttachmentFetch> attchmentFet1 = attachmentDao.attchmentFetch1(testId, bugId);
			// System.out.println("response.." + response);
			response.setServiceStatus("200");
			response.setServiceResponse(attchmentFet1);

		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;
	}

	public Resource getTemplateFile() {
		Resource resource = null;
		String Location = templateFile;
		File file = new File(Location);
		try {

			if (file.exists()) {
				resource = new FileSystemResource(Location);
			}
		} catch (Exception ex) {
			try {
				throw new FileNotFoundException("File not found ");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return resource;

	}

	public ServiceResponse getfiletrOptionData(Integer runId, String status) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		Map<String, MannualRunDetailsData> runTestCaseDetails = new HashedMap<String, MannualRunDetailsData>();
		try {
			runTestCaseDetails = mannualExecutionLogsDao.getTestGroupAndCasesByRunAssignstatus(runId, status);
			response.setServiceStatus("200");
			response.setServiceResponse(runTestCaseDetails);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse deleteScreenshot(Integer testcaseID, String attachname) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		Resource resource = null;
		try {
			List<AttachmentFetch> attchmentFet = attachmentDao.deleteScreenshot(testcaseID, attachname);

			response.setServiceStatus("200");
			response.setServiceResponse(attchmentFet);

		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);

		}
		return response;

	}

	public ServiceResponse testrunwisedetailsoverview(Integer projectid) {
		String Status = "no status";
		ServiceResponse response = new ServiceResponse();
		List<Object[]> runTestCaseDetails = null;
		ArrayList<ArrayList<Object>> runTestCaseDetailsover = new ArrayList<ArrayList<Object>>();
		try {
			runTestCaseDetails = mannualExecutionLogsDao.testrunwisedetailsOverview(projectid);
			for (Object[] obj : runTestCaseDetails) {
				ArrayList<Object> arr = new ArrayList<Object>();
				try {
					String runname = "R" + obj[1] + "_" + obj[2];
					arr.add(runname);
				} catch (Exception e) {
					String runname = "R" + obj[11] + "_" + obj[2];
					arr.add(runname);
				}

				/*
				 * String round=obj[2]+"_"+obj[15]; arr.add(round); arr.add(obj[0].toString());
				 * arr.add(obj[6]); arr.add( obj[7]); arr.add(obj[8]); arr.add( obj[9]);
				 * arr.add( obj[4]); arr.add( obj[3]); arr.add(obj[12]); arr.add( obj[10]);
				 * arr.add(obj[11]); arr.add( obj[13]); arr.add( obj[16]); arr.add( obj[17]);
				 * arr.add(obj[18]);
				 */
				String round = obj[2] + "_" + obj[16];
				arr.add(round);
				arr.add(obj[0].toString());
				arr.add(obj[6]);
				arr.add(obj[7]);
				arr.add(obj[8]);
				arr.add(obj[9]);
				arr.add(obj[10]);
				arr.add(obj[40]);
				arr.add(obj[4]);
				arr.add(obj[3]);
				arr.add(obj[23]);
				arr.add(obj[24]);
				arr.add(obj[25]);
				arr.add(obj[26]);
				arr.add(obj[33]);// status blocked
				arr.add(obj[34]);
				arr.add(obj[35]);
				arr.add(obj[36]);
				arr.add(obj[37]);
				arr.add(obj[38]);
				arr.add(obj[39]);
				arr.add(obj[13]);
				arr.add(obj[11]);
				arr.add(obj[12]);
				arr.add(obj[14]);
				arr.add(obj[17]);
				arr.add(obj[18]);
				arr.add(obj[19]);
				arr.add(obj[20]);
				arr.add(obj[27]);// priority Must to excute
				arr.add(obj[28]);
				arr.add(obj[29]);
				arr.add(obj[30]);// complexity high
				arr.add(obj[31]);
				arr.add(obj[32]);
				/*
				 * arr.add(obj[0].toString()); arr.add(obj[6]); arr.add(obj[7]);
				 * arr.add(obj[8]); arr.add(obj[9]); arr.add(obj[10]); arr.add(obj[4]);
				 * arr.add(obj[3]); arr.add(obj[23]); arr.add(obj[24]); arr.add(obj[25]);
				 * arr.add(obj[26]); arr.add(obj[33]);//status blocked arr.add(obj[34]);
				 * arr.add(obj[35]); arr.add(obj[36]); arr.add(obj[37]); arr.add(obj[38]);
				 * arr.add(obj[39]); arr.add(obj[13]); arr.add(obj[11]); arr.add(obj[12]);
				 * arr.add(obj[14]); arr.add(obj[17]); arr.add(obj[18]); arr.add(obj[19]);
				 * arr.add(obj[20]); arr.add(obj[27]);// priority Must to excute
				 * arr.add(obj[28]); arr.add(obj[29]); arr.add(obj[30]);// complexity high
				 * arr.add(obj[31]); arr.add(obj[32]);
				 */
				/*
				 * arr.add(obj[33]);// priority Must to excute arr.add(obj[34]);
				 * arr.add(obj[35]); arr.add(obj[36]);// complexity high arr.add(obj[37]);
				 * arr.add(obj[38]);
				 */

				runTestCaseDetailsover.add(arr);
			}
			response.setServiceStatus("200");
			response.setServiceResponse(runTestCaseDetailsover);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse getShowProjectDetails(String projectid) {
		ServiceResponse response = new ServiceResponse();

		try {
			List<ProjectAccessEntity> projectuser = projectDao.getShowProjectDetails(projectid);
			response.setServiceStatus("200");
			response.setServiceResponse(projectuser);

		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse("");

		}
		return response;
	}

	public ServiceResponse datewiserun(String startdate, String enddate, int projectid) throws ParseException {
		String Status = "no status";
		ServiceResponse response = new ServiceResponse();
		List<Object[]> runTestCaseDetails = null;
		String date1[] = startdate.split("/");
		String startdate1 = date1[2] + "/" + date1[0] + "/" + date1[1];
		String date2[] = enddate.split("/");
		String enddate1 = date2[2] + "/" + date2[0] + "/" + date2[1];
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd");
		java.util.Date date = sdf1.parse(startdate1);
		java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
		java.util.Date date12 = sdf1.parse(enddate1);
		java.sql.Date sqlEndDate = new java.sql.Date(date12.getTime());
		List<runwisestatus> runwisestatus = new ArrayList<runwisestatus>();
		try {
			runTestCaseDetails = mannualTestCaselogsRepo.datewiserundetails(sqlStartDate, sqlEndDate, projectid);
			for (Object[] test : runTestCaseDetails) {
				runwisestatus testt = new runwisestatus();
				testt.setTotalcases(Integer.valueOf(test[0].toString()));
				testt.setRunid(Integer.valueOf(test[12].toString()));
				testt.setRunname("R" + test[11] + "_" + test[2]);
				testt.setTotalcompleted(Integer.valueOf(test[3].toString()));
				testt.setTotalonhold(Integer.valueOf(test[4].toString()));
				testt.setTotalunderreview(Integer.valueOf(test[5].toString()));
				testt.setTotalexecuted(Integer.valueOf(test[6].toString()));
				testt.setTotalnotstarted(Integer.valueOf(test[7].toString()));

				if (test[8] == null) {
					testt.setTotalmanhours((float) 0.0);
				} else {
					testt.setTotalmanhours(Float.valueOf(test[8].toString()));
				}
				testt.setTotalNe(Integer.valueOf(test[9].toString()));
				testt.setIncomplete(Integer.valueOf(test[10].toString()));
				testt.setRound(Integer.valueOf(test[11].toString()));
				testt.setRoundrunname(testt.getRunname() + "_" + testt.getRound());
				runwisestatus.add(testt);
			}
			response.setServiceStatus("200");
			response.setServiceResponse(runwisestatus);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse datewiserunfailstatus(String startdate, String enddate, int projectid)
			throws ParseException {

		String Status = "no status";
		ServiceResponse response = new ServiceResponse();
		List<Object[]> runTestCaseDetails = null;
		String date1[] = startdate.split("/");
		String startdate1 = date1[2] + "/" + date1[0] + "/" + date1[1];
		String date2[] = enddate.split("/");
		String enddate1 = date2[2] + "/" + date2[0] + "/" + date2[1];
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd");
		java.util.Date date = sdf1.parse(startdate1);
		java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
		java.util.Date date12 = sdf1.parse(enddate1);
		java.sql.Date sqlEndDate = new java.sql.Date(date12.getTime());
		List<projectwisefailstatus> executionLogs = new ArrayList<>();
		try {
			runTestCaseDetails = mannualTestCaselogsRepo.datewiserunfailstatus(sqlStartDate, sqlEndDate, projectid);
			for (Object[] objects : runTestCaseDetails) {
				projectwisefailstatus test = new projectwisefailstatus();
				test.setTotalrunfail(Integer.valueOf(objects[0].toString()));
				test.setTotalcriticalopen(Integer.valueOf(objects[1].toString()));
				test.setTotalcriticalclosed(Integer.valueOf(objects[2].toString()));
				test.setTotalcriticalreopen(Integer.valueOf(objects[3].toString()));
				test.setTotalcriticalresolved(Integer.valueOf(objects[4].toString()));
				test.setTotalblockeropen(Integer.valueOf(objects[5].toString()));
				test.setTotalblockerclosed(Integer.valueOf(objects[6].toString()));
				test.setTotalblockerreopen(Integer.valueOf(objects[7].toString()));
				test.setTotalblockerresolved(Integer.valueOf(objects[8].toString()));
				test.setTotalmajoropen(Integer.valueOf(objects[9].toString()));
				test.setTotalmajorclosed(Integer.valueOf(objects[10].toString()));
				test.setTotalmajorreopen(Integer.valueOf(objects[11].toString()));
				test.setTotalmajorresolved(Integer.valueOf(objects[12].toString()));
				test.setTotalminoropen(Integer.valueOf(objects[13].toString()));
				test.setTotalminorclosed(Integer.valueOf(objects[14].toString()));
				test.setTotalminorreopen(Integer.valueOf(objects[15].toString()));
				test.setTotalminorresolved(Integer.valueOf(objects[16].toString()));
				test.setRunId(Integer.valueOf(objects[17].toString()));
				test.setRunname("R" + objects[17] + "_" + objects[18]);
				test.setRound(Integer.valueOf(objects[19].toString()));
				test.setRoundrunname(test.getRunname() + "_" + test.getRound());
				/*
				 * test.setTotalrunfail(Integer.valueOf(objects[0].toString()));
				 * test.setTotalmajor(Integer.valueOf(objects[1].toString()));
				 * test.setTotalminor(Integer.valueOf(objects[2].toString()));
				 * test.setTotalcritical(Integer.valueOf(objects[3].toString()));
				 * test.setTotalblocker(Integer.valueOf(objects[4].toString()));
				 * test.setRunid(Integer.valueOf(objects[5].toString()));
				 * test.setRunname("R"+objects[5]+"_"+objects[6]);
				 * test.setRound(Integer.valueOf(objects[7].toString()));
				 * test.setTotalopen(Integer.valueOf(objects[8].toString()));
				 * test.setTotalclosed(Integer.valueOf(objects[9].toString()));
				 * test.setTotalreopen(Integer.valueOf(objects[10].toString()));
				 * test.setRoundrunname(test.getRunname() + "_" + test.getRound());
				 */
				executionLogs.add(test);
			}
			response.setServiceStatus("200");
			response.setServiceResponse(executionLogs);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public Integer insertExecutionLog(List<MannualTestCaseExecutionLogs> caseExecutionLogs) {
		Integer i = 0;

		try {
			mannualExecutionLogsDao.saveTestCaseForRun(caseExecutionLogs);
			i = 1;
		} catch (DataIntegrityViolationException e) {

		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return i;
	}

	public ServiceResponse uploadTestRunExecution(MultipartFile file, MannualRunTable runTable, String round,
			String targetEndDate, String description, String sameTestSuite) {

		// System.out.println("in project service for uploading test run");
		try {
			DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			Date date = (Date) formatter.parse(targetEndDate);
			SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			targetEndDate = newFormat.format(date);
		} catch (Exception e) {
			// System.out.println("error is 0 " + e.toString());
		}
		// System.out.println("in project service target end date " + targetEndDate);
		ServiceResponse response = new ServiceResponse();
		Integer i = 0, k = 0;
		Map<String, List<MannualTestCaseExecutionLogs>> mapObj = new HashedMap<String, List<MannualTestCaseExecutionLogs>>();
		List<MannualTestGroup> mannualTestGroups = new ArrayList<MannualTestGroup>();

		List<MannualTestCaseTable> testCases = readManualTestCases(file, runTable.getSuiteName());
		List<MannualTestCaseTable> caseTablesList = new ArrayList<MannualTestCaseTable>();
		List<MannualTestCaseExecutionLogs> caseExecutionLogs = readTestRunExecution(file, runTable.getSuiteName(),
				sameTestSuite);

		List<MannualTestCaseExecutionLogs> values = null;
		for (MannualTestCaseExecutionLogs details1 : caseExecutionLogs) {
			if (details1 != null) {

				values = caseExecutionLogs.stream().filter(x -> x.getGroupName().equals(details1.getGroupName()))
						.collect(Collectors.toList());
				mapObj.put(details1.getGroupName(), values);

			}
		}

		if (!mapObj.isEmpty()) {
			MannualRunTable runTables = mannualRunDao.createRunTable(runTable);
			MannualReRunTable testGroup = new MannualReRunTable();
			testGroup.setRunName(runTable.getRunName());
			testGroup.setStatus("Running");
			testGroup.setRunId(runTable.getId());
			testGroup.setProjectId(runTable.getProjectId());
			testGroup.setRound(Integer.parseInt(round));
			testGroup.setIsActive(runTable.getIsActive());
			testGroup.setCreatedBy(runTables.getCreatedBy());
			testGroup.setTestSuitesId(runTables.getTestSuitesId());
			testGroup.setTargetEndDate(targetEndDate);
			testGroup.setDescription(description);

			MannualReRunTable reRunTable = mannualReRunDao.createRun(testGroup);
			MannualTestSuites testSuitesObj = null;
			MannualTestGroup testGroupObj = null;
			UserDetailsEntity usrData = userRepository.findById(Integer.valueOf(runTable.getCreatedBy())).orElse(null);
			if (usrData != null) {
				// System.out.println("user data is " + usrData.getUserID() + " " +
				// usrData.getUsername());
			}

			// System.out.println("caseExecutionLogs.size..." + caseExecutionLogs.size());
			for (int j = 0; j < caseExecutionLogs.size(); j++) {

				MannualTestSuites testSuites = new MannualTestSuites();
				testSuites.setTestSuitesName(caseExecutionLogs.get(j).getSuiteName());
				testSuites.setProjectId(runTable.getProjectId());
				testSuites.setIsActive(runTable.getIsActive());
				testSuites.setCreatedBy(usrData.getUsername());
				testSuites.setCreatedDate(new Date());
				testSuitesObj = mannualTestSuitesDAO.checkAndCreateSuites(testSuites);

				MannualTestGroup testGroupList = new MannualTestGroup();
				testGroupList.setTestSuitesId(testSuitesObj.getTestSuitesId());
				testGroupList.setGroupName(caseExecutionLogs.get(j).getGroupName());
				testGroupList.setIsActive(runTable.getIsActive());
				testGroupList.setCreatedBy(usrData.getUsername());
				testGroupList.setCreatedDate(new Date());
				testGroupObj = mannualTestGroupDAO.checkAndCreateGroup(testGroupList);

				caseExecutionLogs.get(j).setProjectId(runTable.getProjectId());
				caseExecutionLogs.get(j).setReRunId(reRunTable.getId());
				caseExecutionLogs.get(j).setTestSuitesId(testSuitesObj.getTestSuitesId());
				caseExecutionLogs.get(j).setTestGroupId(testGroupObj.getTestGroupId());

				MannualTestCaseTable caseTable = new MannualTestCaseTable();
				caseTable.setTestGroupId(testGroupObj.getTestGroupId());
				caseTable.setSrNo(String.valueOf(caseExecutionLogs.get(j).getTestCaseSrNo()));
				caseTable.setTestID(caseExecutionLogs.get(j).getTestID());
				Date exeDate = caseExecutionLogs.get(j).getDateOfExec();
				if (exeDate == null || exeDate.equals("")) {
					// System.out.println("date of exe in casetBLE");
				} else {
					caseTable.setDateOfExec(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
							.format(caseExecutionLogs.get(j).getDateOfExec()));

				}
				caseTable.setBrowser(caseExecutionLogs.get(j).getBrowser());
				caseTable.setFunctionality(caseExecutionLogs.get(j).getFunctionality());
				caseTable.setTestCaseType(caseExecutionLogs.get(j).getTestCaseType());
				caseTable.setTestDescription(caseExecutionLogs.get(j).getTestDescription());
				caseTable.setTestData(caseExecutionLogs.get(j).getTestData());
				caseTable.setSteps(caseExecutionLogs.get(j).getSteps());
				caseTable.setExpectedResult(caseExecutionLogs.get(j).getExpectedResult());
				caseTable.setActualRsult(caseExecutionLogs.get(j).getActualResult());
				caseTable.setStatus(caseExecutionLogs.get(j).getStatus());
				caseTable.setTesterComment(caseExecutionLogs.get(j).getTesterComment());
				caseTable.setTesterName(caseExecutionLogs.get(j).getTesterName());
				caseTable.setModule(caseExecutionLogs.get(j).getModule());
				caseTable.setSubModule(caseExecutionLogs.get(j).getSubModule());
				caseTable.setScenarioID(caseExecutionLogs.get(j).getScenarioID());
				caseTable.setScenarios(caseExecutionLogs.get(j).getScenarios());
				caseTable.setPriority(caseExecutionLogs.get(j).getPriority());
				caseTable.setComplexity(caseExecutionLogs.get(j).getComplexity());
				caseTable.setCreatedBy(Integer.parseInt(runTable.getCreatedBy()));
				caseTable.setAction(caseExecutionLogs.get(j).getTestCaseAction());

				Date createdTimeStamp = caseExecutionLogs.get(j).getCreatedTimeStamp();
				if (createdTimeStamp == null || createdTimeStamp.equals("")) {
					// System.out.println("date of create timestamp");
				} else {
					caseTable.setCreatedDate(caseExecutionLogs.get(j).getCreatedTimeStamp());
				}
				Long executionTime = caseExecutionLogs.get(j).getExecutionTime();
				if (executionTime == null || executionTime.equals("")) {
					// System.out.println("date of exe time");
				} else {
					caseTable.setExecutionTime(String.valueOf(executionTime));
				}

				caseTablesList.add(caseTable);

				/*
				 * for(int l=0;i<caseTablesList.size();l++) {
				 * caseExecutionLogs.get(j).setTestCaseSrId(caseTablesList); }
				 */
			}
			i = insertExecutionLog(caseExecutionLogs);
			k = insertTestCaseList(caseTablesList);
		}

		if (i == 1) {
			response.setServiceResponse("Excel file has been uploaded successfully. Total " + caseExecutionLogs.size()
					+ " Tescases has been uploaded in " + runTable.getRunName());
			;
			response.setServiceStatus("200");
		} else {
			response.setServiceResponse("Testcases Not Uploaded");
			response.setServiceStatus("400");
		}

		return response;
	}

	public ServiceResponse uploadTestRunExecutions(List<MannualTestCaseExecutionLogs> mannualTestCaseExecutionLogs,
			MannualRunTable testRun, String round, String targetEndDate, String description, String isActive) {

		try {
			// System.out.println("" + mannualTestCaseExecutionLogs.get(0).getDateOfExec());
			DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			Date date = (Date) formatter.parse(targetEndDate);
			SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			targetEndDate = newFormat.format(date);
		} catch (Exception e) {
			// System.out.println("error is 0 " + e.toString());
		}
		ServiceResponse response = new ServiceResponse();
		try {
			List<MannualTestCaseExecutionLogs> addlogs = new ArrayList<>();
			MannualTestSuites testSuitesObj = null;
			MannualTestGroup testGroupObj = null;
			List<MannualTestCaseTable> caseTablesList = new ArrayList<MannualTestCaseTable>();
			UserDetailsEntity usrData = userRepository.findById(Integer.valueOf(testRun.getCreatedBy())).orElse(null);
			if (usrData != null) {
				// System.out.println("user data is " + usrData.getUserID() + " " +
				// usrData.getUsername());
			}

			Integer findlatindex = mannualTestCaseRepo.findLastRowTS();

			for (int j = 0; j < mannualTestCaseExecutionLogs.size(); j++) {

				MannualTestSuites testSuites = new MannualTestSuites();
				testSuites.setTestSuitesName(mannualTestCaseExecutionLogs.get(j).getSuiteName());
				testSuites.setProjectId(testRun.getProjectId());
				testSuites.setIsActive(testRun.getIsActive());
				testSuites.setCreatedBy(usrData.getUsername());
				testSuites.setCreatedDate(new Date());
				testSuitesObj = mannualTestSuitesDAO.checkAndCreateSuites(testSuites);

				MannualTestGroup testGroupList = new MannualTestGroup();
				testGroupList.setTestSuitesId(testSuitesObj.getTestSuitesId());
				testGroupList.setGroupName(mannualTestCaseExecutionLogs.get(j).getGroupName());
				testGroupList.setIsActive(testRun.getIsActive());
				testGroupList.setCreatedBy(usrData.getUsername());
				testGroupList.setCreatedDate(new Date());
				testGroupObj = mannualTestGroupDAO.checkAndCreateGroup(testGroupList);

				// System.out.println("testGroupObj.." + testGroupObj.getTestGroupId());
				MannualTestCaseTable caseTable = new MannualTestCaseTable();
				caseTable.setTestGroupId(testGroupObj.getTestGroupId());
				caseTable.setSrNo(String.valueOf(mannualTestCaseExecutionLogs.get(j).getTestCaseSrNo()));
				caseTable.setTestID(mannualTestCaseExecutionLogs.get(j).getTestID());
				if (mannualTestCaseExecutionLogs.get(j).getDateOfExec() == null
						|| mannualTestCaseExecutionLogs.get(j).getDateOfExec().equals("")
						|| mannualTestCaseExecutionLogs.get(j).getDateOfExec().equals("NaN-aN-aN")) {
					Date exeDate = mannualTestCaseExecutionLogs.get(j).getDateOfExec();
					// System.out.println("exeDate.." + exeDate);
					if (exeDate == null || exeDate.equals("") || exeDate.equals("NaN-aN-aN")) {
						// System.out.println("date of exe in casetBLE ");
					} else {
						caseTable.setDateOfExec(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
								.format(mannualTestCaseExecutionLogs.get(j).getDateOfExec()));
					}
				}
				//// System.out.println("(mannualTestCaseExecutionLogs.get(j).getStatus()...fail/pasd
				//// statusta..."
				// + mannualTestCaseExecutionLogs.get(j).getStatus());
				caseTable.setBrowser(mannualTestCaseExecutionLogs.get(j).getBrowser());
				caseTable.setFunctionality(mannualTestCaseExecutionLogs.get(j).getFunctionality());
				caseTable.setTestCaseType(mannualTestCaseExecutionLogs.get(j).getTestCaseType());
				caseTable.setTestDescription(mannualTestCaseExecutionLogs.get(j).getTestDescription());
				caseTable.setTestData(mannualTestCaseExecutionLogs.get(j).getTestData());
				caseTable.setSteps(mannualTestCaseExecutionLogs.get(j).getSteps());
				caseTable.setExpectedResult(mannualTestCaseExecutionLogs.get(j).getExpectedResult());
				caseTable.setActualRsult(mannualTestCaseExecutionLogs.get(j).getActualResult());
				caseTable.setStatus(mannualTestCaseExecutionLogs.get(j).getStatus());
				caseTable.setTesterComment(mannualTestCaseExecutionLogs.get(j).getTesterComment());
				caseTable.setTesterName(mannualTestCaseExecutionLogs.get(j).getTesterName());
				caseTable.setModule(mannualTestCaseExecutionLogs.get(j).getModule());
				caseTable.setSubModule(mannualTestCaseExecutionLogs.get(j).getSubModule());
				caseTable.setScenarioID(mannualTestCaseExecutionLogs.get(j).getScenarioID());
				caseTable.setScenarios(mannualTestCaseExecutionLogs.get(j).getScenarios());
				caseTable.setPriority(mannualTestCaseExecutionLogs.get(j).getPriority());
				caseTable.setComplexity(mannualTestCaseExecutionLogs.get(j).getComplexity());
				caseTable.setCreatedBy(Integer.parseInt(testRun.getCreatedBy()));
				Date createdTimeStamp = mannualTestCaseExecutionLogs.get(j).getCreatedTimeStamp();
				if (createdTimeStamp == null || createdTimeStamp.equals("")) {
					// System.out.println("date of create timestamp");
				} else {
					caseTable.setCreatedDate(mannualTestCaseExecutionLogs.get(j).getCreatedTimeStamp());
				}
				Long executionTime = mannualTestCaseExecutionLogs.get(j).getExecutionTime();
				if (executionTime == null || executionTime.equals("")) {
				} else {
					caseTable.setExecutionTime(String.valueOf(executionTime));
				}

				caseTablesList.add(caseTable);
			}
			MannualTestCaseTable i = insertTestCaseListUTE(caseTablesList);

			if (i.getMessageerror().equals("Duplicate test case id")) {
				response.setServiceResponse("Duplicate test case id");
				response.setServiceStatus("400");

			} else if (i.getMessageerror().equals("save data")) {
				testRun.setTestGroupId(testGroupObj.getTestGroupId());
				MannualRunTable runTables = mannualRunDao.createRunTable(testRun);
				MannualReRunTable testGroup = new MannualReRunTable();
				testGroup.setRunName(testRun.getRunName());
				testGroup.setStatus("Running");
				testGroup.setRunId(testRun.getId());
				testGroup.setProjectId(testRun.getProjectId());
				testGroup.setRound(Integer.parseInt(round));
				testGroup.setIsActive(testRun.getIsActive());
				testGroup.setCreatedBy(runTables.getCreatedBy());
				testGroup.setTestSuitesId(runTables.getTestSuitesId());
				testGroup.setTargetEndDate(targetEndDate);
				testGroup.setDescription(description);

				MannualReRunTable reRunTable = mannualReRunDao.createRun(testGroup);

				List<MannualTestCaseTable> shownewAddData = mannualTestCaseRepo.findNewAddID(findlatindex);

				if (shownewAddData.isEmpty() || shownewAddData.equals("")) {
					response.setServiceResponse("Testcases Not Uploaded");
					response.setServiceStatus("400");
				} else {
					int count = 0;
					for (int k = 0; k < mannualTestCaseExecutionLogs.size(); k++) {
						mannualTestCaseExecutionLogs.get(k).setTestCaseSrId(shownewAddData.get(k).getSrID());
						mannualTestCaseExecutionLogs.get(k).setAbsoluteTCID(shownewAddData.get(k).getSrID());
						mannualTestCaseExecutionLogs.get(k).setReRunId(reRunTable.getId());
						mannualTestCaseExecutionLogs.get(k).setTestGroupId(testGroupObj.getTestGroupId());
						mannualTestCaseExecutionLogs.get(k).setProjectId(reRunTable.getProjectId());
						mannualTestCaseExecutionLogs.get(k).setTestGroupId(testGroupObj.getTestGroupId());
						mannualTestCaseExecutionLogs.get(k).setTestSuitesId(reRunTable.getTestSuitesId());

						String testerID = null;
						try {
							if (StringUtils.isNotEmpty(mannualTestCaseExecutionLogs.get(k).getTesterName())) {
								testerID = userRepository
										.findByName(mannualTestCaseExecutionLogs.get(k).getTesterName()).getId()
										.toString();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						mannualTestCaseExecutionLogs.get(k).setTesterId(testerID);

						MannualTestCaseExecutionLogs saveInManualLog = mannualTestCaselogsRepo
								.save(mannualTestCaseExecutionLogs.get(k));

						count++;
						addlogs.add(saveInManualLog);

					}
					if (count != 0) {
						response.setServiceResponse("Excel file has been uploaded successfully. Total " + addlogs.size()
								+ " Tescases has been uploaded in " + testRun.getRunName());

						response.setServiceStatus("200");
					} else {
						response.setServiceResponse("Run not created");
						response.setServiceStatus("400");
					}
				}

			}
		}

		catch (Exception e) {
			e.printStackTrace();
			// System.out.println("e..." + e);
		}
		return response;
	}

	public Integer insertTestCaseList(List<MannualTestCaseTable> testCaseList) {
		Integer i = 0;

		for (MannualTestCaseTable testCase1 : testCaseList) {
			try {
				mannualTestCaseDao.addNewTestId(testCase1);
				i = 1;
			} catch (DataIntegrityViolationException e) {

			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}

		}
		return i;

	}

	public MannualTestCaseTable insertTestCaseListUTE(List<MannualTestCaseTable> testCaseList) {
		Integer i = 0;
		// List<MannualTestCaseTable> addAllTestId=null;

		MannualTestCaseTable addId = new MannualTestCaseTable();
		for (MannualTestCaseTable testCase1 : testCaseList) {
			try {

				addId = mannualTestCaseDao.addNewTestIds(testCase1);
				// System.out.println("add id ==="+addId);
				// i = 1;
				// addAllTestId.add(addId);
				// //System.out.println("addAllTestId..."+addAllTestId);
			} catch (DataIntegrityViolationException e) {
				e.printStackTrace();

			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}

		}
		return addId;

	}

	public ServiceResponse uploadPreview(MultipartFile file, MannualRunTable runTable, String sameTestSuite) {
		ServiceResponse response = new ServiceResponse();
		List<MannualRunTable> findname = mannualRunRepository.findByRunName(runTable.getRunName());
		// System.out.println("1..finname..." + findname);
		if (findname.isEmpty()) {

			Map<String, List<MannualTestCaseExecutionLogs>> mapObj = new HashedMap<String, List<MannualTestCaseExecutionLogs>>();
			List<MannualTestCaseExecutionLogs> caseExecutionLogs = readTestRunExecution(file, runTable.getSuiteName(),
					sameTestSuite);

			/*
			 * for(int i=0;i<=caseExecutionLogs.size();i++) {
			 * //System.out.println("caseExecutionLogs.uploadpreview..."+caseExecutionLogs.
			 * get (i).getErrorMessage()); }
			 */

			if (sameTestSuite.equals("true")) {
				if (caseExecutionLogs.size() > 0) {
					response.setServiceResponse(caseExecutionLogs);
					response.setServiceStatus("200");
				} else {
					/*
					 * if(caseExecutionLogs.get(0).getErrorMessage().
					 * equals("Test suite name is empty.") ||
					 * caseExecutionLogs.get(0).getErrorMessage().equals("Test group name is empty."
					 * ) || caseExecutionLogs.get(0).getErrorMessage().equals("Scenario is empty.")
					 * ||
					 * caseExecutionLogs.get(0).getErrorMessage().equals("Test Case Type is empty.")
					 * 
					 * ||caseExecutionLogs.get(0).getErrorMessage().equals("Functionality is empty."
					 * ) ||
					 * caseExecutionLogs.get(0).getErrorMessage().equals("Test Case Id is empty.")
					 * ||
					 * caseExecutionLogs.get(0).getErrorMessage().equals("Actual Result is empty.")
					 * || caseExecutionLogs.get(0).getErrorMessage().equals("Status is empty.") ||
					 * caseExecutionLogs.get(0).getErrorMessage().equals("ScenarioId is empty.") ||
					 * caseExecutionLogs.get(0).getErrorMessage().equals("Complexity is empty.") ||
					 * caseExecutionLogs.get(0).getErrorMessage().equals("Priority is empty.") ) {
					 * response.setServiceResponse(caseExecutionLogs.get(0).getErrorMessage());
					 * response.setServiceStatus("401"); } else {
					 */

					response.setServiceResponse(
							"Sorry! Could not upload file. Please verify upload file once and make sure you are uploading same module as selected test suite.");
					response.setServiceStatus("400");

				}
			}

			//// System.out.println("caseExecutionLogs.." + caseExecutionLogs.size());
			if (sameTestSuite.equals("false")) {
				if (caseExecutionLogs.size() > 0) {
					response.setServiceResponse(caseExecutionLogs);
					response.setServiceStatus("200");
				}
			}

		} else {
			response.setServiceStatus("400");
			response.setServiceResponse("Sorry!Run Name already exists. Please edit the field.");

		}
		return response;
	}

	/*
	 * public ServiceResponse uploadPreview(MultipartFile file, MannualRunTable
	 * runTable,String sameTestSuite) { ServiceResponse response = new
	 * ServiceResponse(); List<MannualRunTable> findname =
	 * mannualRunRepository.findByRunName(runTable.getRunName());
	 * //System.out.println("1..finname..."+findname); if(findname.isEmpty()) {
	 * 
	 * Map < String, List < MannualTestCaseExecutionLogs >> mapObj = new HashedMap <
	 * String, List < MannualTestCaseExecutionLogs >> (); List <
	 * MannualTestCaseExecutionLogs > caseExecutionLogs=readTestRunExecution(file,
	 * runTable.getSuiteName(),sameTestSuite);
	 * 
	 * if(sameTestSuite.equals("true")) {
	 * if(caseExecutionLogs.get(0).getErrorMessage().
	 * equals("Test suite name is empty.") ||
	 * caseExecutionLogs.get(0).getErrorMessage().equals("Test group name is empty."
	 * ) || caseExecutionLogs.get(0).getErrorMessage().equals("Scenario is empty.")
	 * ||
	 * caseExecutionLogs.get(0).getErrorMessage().equals("Test Case Type is empty.")
	 * 
	 * ||caseExecutionLogs.get(0).getErrorMessage().equals("Functionality is empty."
	 * ) ||
	 * caseExecutionLogs.get(0).getErrorMessage().equals("Test Case Id is empty.")
	 * ||
	 * caseExecutionLogs.get(0).getErrorMessage().equals("Actual Result is empty.")
	 * || caseExecutionLogs.get(0).getErrorMessage().equals("Status is empty.") ||
	 * caseExecutionLogs.get(0).getErrorMessage().equals("ScenarioId is empty.") ||
	 * caseExecutionLogs.get(0).getErrorMessage().equals("Complexity is empty.") ||
	 * caseExecutionLogs.get(0).getErrorMessage().equals("Priority is empty.") ) {
	 * response.setServiceResponse(caseExecutionLogs.get(0).getErrorMessage());
	 * response.setServiceStatus("401"); } else {
	 * 
	 * if (caseExecutionLogs.size()>0) {
	 * response.setServiceResponse(caseExecutionLogs);
	 * response.setServiceStatus("200"); } else {
	 * 
	 * 
	 * 
	 * 
	 * response.
	 * setServiceResponse("Sorry! Could not upload file. Please verify upload file once and make sure you are uploading same module as selected test suite."
	 * ); response.setServiceStatus("400"); }
	 * 
	 * }
	 * 
	 * //System.out.println("caseExecutionLogs.."+caseExecutionLogs.size());
	 * if(sameTestSuite.equals("false")) { if (caseExecutionLogs.size()>0) {
	 * response.setServiceResponse(caseExecutionLogs);
	 * response.setServiceStatus("200"); } }
	 * 
	 * 
	 * } else { response.setServiceStatus("400"); response.
	 * setServiceResponse("Sorry!Run Name already exists. Please edit the field.");
	 * 
	 * } return response; }
	 */

	public Object[] checkExeclHeaderForRunExecution(List<String> headers) {
		Boolean isRightExcel = true;
		String[] preDefinedHeader = ConstantParameter.MannualRunExcelHeaderList;
		List<String> finalHaeding = new ArrayList<String>();
		ArrayList<String> preDefinedHeaderList = new ArrayList<String>(Arrays.asList(preDefinedHeader));
		for (String header : preDefinedHeaderList) {
			if (!headers.contains(header.trim().toString())) {
				isRightExcel = false;
				break;
			} else {
				finalHaeding.add(header);
			}
		}
		Object[] object = new Object[2];
		object[0] = isRightExcel;
		object[1] = finalHaeding;

		return object;

	}

	public List<MannualTestCaseExecutionLogs> readTestRunExecution(MultipartFile file, String suiteName,
			String sameTestSuite) {
		List<MannualTestCaseExecutionLogs> dataBOsList = null;
		try {
			int v = 10;
			// System.out.println(v);
			DataFormatter dataFormatter = new DataFormatter(); //
			// FileInputStream fis=new FileInputStream(file.getInputStream());
			XSSFWorkbook myWorkBook = new XSSFWorkbook(file.getInputStream());
			XSSFSheet mySheet = myWorkBook.getSheetAt(0);

			dataBOsList = new ArrayList<>();
			List<String> excelHeaderList = getHeaders(file);
			Object[] validateExcel = checkExeclHeaderForRunExecution(excelHeaderList);
			if ((boolean) validateExcel[0]) {
				List<String> list = (List<String>) validateExcel[1]; //
				// System.out.println(list);
				Map<String, Integer> headerSeq = assignHeaderSeqNumber(list);
				int first = 0;
				for (Row row : mySheet) { //
					// System.out.println(first);
					if (first == 0) {
						first = first + 1;

					} else {
						MannualTestCaseExecutionLogs caseDataBO = new MannualTestCaseExecutionLogs();
						for (Map.Entry<String, Integer> heading : headerSeq.entrySet()) {
							// int cellType = row.getCell(headerSeq.get(heading.getKey())).getCellType();
							Integer headingCount = heading.getValue();
							if (headingCount.equals(0)) {

								String testCaseNo = dataFormatter
										.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));
								if (testCaseNo.equals("") || testCaseNo == null || testCaseNo.isEmpty()) {
									// System.out.println("testCaseNo of exe");
								} else {
									caseDataBO.setTestCaseSrNo(Integer.parseInt(dataFormatter
											.formatCellValue(row.getCell(headerSeq.get(heading.getKey())))));
								}

							} else if (headingCount.equals(1)) {
								String suitename = dataFormatter
										.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));

								caseDataBO.setSuiteName(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
								caseDataBO.setModule(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(2)) {
								String suitegropuname = dataFormatter
										.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));
								caseDataBO.setGroupName(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
								caseDataBO.setSubModule(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));

							} else if (headingCount.equals(3)) {
								String setsenariosname = dataFormatter
										.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));
								caseDataBO.setScenarioID(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(4)) {
								caseDataBO.setScenarios(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(5)) {
								caseDataBO.setFunctionality(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(6)) {
								caseDataBO.setTestID(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(7)) {
								caseDataBO.setTestDescription(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(8)) {
								caseDataBO.setTestCaseType(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(9)) {
								caseDataBO.setTestData(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(10)) {
								String suitesstes = dataFormatter
										.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));

								caseDataBO.setSteps(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(11)) {
								String getKey = heading.getKey();
								Integer headerSeqObj = headerSeq.get(getKey);
								Cell cell = row.getCell(headerSeqObj);
								String exepencetdfeesult = dataFormatter
										.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));
								caseDataBO.setExpectedResult(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(12)) {
								String ckcBrowser = dataFormatter
										.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));
								if (ckcBrowser.equals("Chrome") || ckcBrowser.equals("IE")
										|| ckcBrowser.equals("FireFox") || ckcBrowser.equals("")
										|| ckcBrowser.isEmpty()) {
									caseDataBO.setBrowser(dataFormatter
											.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
								} else {
									caseDataBO.setBrowser("Chrome");
								}
							} else if (headingCount.equals(13)) {
								String ptName = dataFormatter
										.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));
								try {
									if (StringUtils.isNotEmpty(ptName)) {
										// String ptID=userRepository.findByName(ptName).getId().toString();
										caseDataBO.setTcPreparedBy(ptName);
									} else {
										// System.out.println("---Prepared by Tester name is empty---");
									}
								} catch (Exception e) {
									e.printStackTrace();
								}

							} else if (headingCount.equals(14)) {
								try {
									String executionTime = dataFormatter
											.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));
									if (StringUtils.isNotEmpty(executionTime)) {
										Long temp;
										String[] h1 = executionTime.split(":");

										long hour = Integer.parseInt(h1[0]);
										long minute = Integer.parseInt(h1[1]);
										long second = Integer.parseInt(h1[2]);

										temp = second + (60 * minute) + (3600 * hour);

										//// System.out.println("secondsss" + temp);
										caseDataBO.setPreparationTime(temp);
										//// System.out.println("execution time" + temp);
									} else {
										// System.out.println("Preparation time is empty");
									}
								} catch (Exception e) {
									// System.out.println("error is Preparation time" + e.toString());
								}
								//// System.out.println("----TC Prepared By---- : " +
								//// caseDataBO.getPreparationTime());
							} else if (headingCount.equals(15)) {
								try {
									String dateOfExecution = dataFormatter
											.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));
									if (dateOfExecution.equals("") || dateOfExecution == null
											|| dateOfExecution.isEmpty()
											|| ("00/00/0000 00:00").equals(dateOfExecution)) {
										// System.out.println("date of exe is empty");
									} else {
										DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
										Date date = (Date) formatter.parse(dateOfExecution);
										SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
										String dateOfExe = newFormat.format(date);
										caseDataBO.setDateOfExec(newFormat.parse(dateOfExe));
									}
								} catch (Exception e) {
									// System.out.println("error is 1 " + e.toString());
								}
							} else if (headingCount.equals(16)) {
								caseDataBO.setActualResult(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
								String actuallresult = dataFormatter
										.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));

							} else if (headingCount.equals(17)) {
								try {
									String executionTime = dataFormatter
											.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));
									if (executionTime.equals("") || executionTime == null || executionTime.isEmpty()) {
										// System.out.println("execution time is empty");
									} else {
										/*
										 * Time t = Time.valueOf(executionTime); long l = t.getTime();
										 */
										Long temp;
										String[] h1 = executionTime.split(":");

										long hour = Integer.parseInt(h1[0]);
										long minute = Integer.parseInt(h1[1]);
										long second = Integer.parseInt(h1[2]);

										temp = second + (60 * minute) + (3600 * hour);

										//// System.out.println("secondsss" + temp);
										caseDataBO.setExecutionTime(temp);
										//// System.out.println("execution time" + temp);
									}
								} catch (Exception e) {
									// System.out.println("error is 2 " + e.toString());
								}

							} else if (headingCount.equals(18)) {
								String getKey = heading.getKey();
								Integer headerSeqObj = headerSeq.get(getKey);
								Cell cell = row.getCell(headerSeqObj);

								caseDataBO.setTesterComment(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(19)) {
								caseDataBO.setTesterName(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(20)) {
								caseDataBO.setIsActive(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(21)) {

								caseDataBO.setBugSeverity(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(22)) {
								caseDataBO.setBugPriority(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(23)) {
								caseDataBO.setBugStatus(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(24)) {
								caseDataBO.setBugId(
										dataFormatter.formatCellValue(row.getCell(headerSeq.get(heading.getKey()))));
							} else if (headingCount.equals(25)) {
								String SrId = dataFormatter
										.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));
								if (SrId.equals("") || SrId == null || SrId.isEmpty()) {
									// System.out.println("sr id empty");
								} else {
									caseDataBO.setTestCaseSrId(Integer.parseInt(SrId));
								}
							} else if (headingCount.equals(26)) {

								String status = dataFormatter
										.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));
								if (status.equals("") || status.isEmpty()) {
								} else if (status.equalsIgnoreCase("Blocked-Undelivered")
										|| status.contentEquals("Blocked-Undelivered")) {
									caseDataBO.setStatus("Blocked-Undelivered");
									caseDataBO.setTestCaseAction("On Hold");

								} else if (status.equalsIgnoreCase("Blocked-Testdata")
										|| status.contentEquals("Blocked-Testdata")) {
									caseDataBO.setStatus("Blocked-Testdata");
									caseDataBO.setTestCaseAction("On Hold");

								} else if (status.equalsIgnoreCase("NA") || status.contentEquals("NA")) {
									caseDataBO.setStatus("NA");
									caseDataBO.setTestCaseAction("N/A");

								} else if (status.equalsIgnoreCase("pass") || status.contentEquals("passed")) {
									caseDataBO.setStatus("Pass");
									caseDataBO.setTestCaseAction("Completed");

								} else if (status.equalsIgnoreCase("fail") || status.contentEquals("failed")) {
									caseDataBO.setStatus("Fail");
									caseDataBO.setTestCaseAction("Completed");
								} else if (status.equalsIgnoreCase("Blocked-Showstopper")
										|| status.contentEquals("Blocked-Showstopper")) {
									caseDataBO.setStatus("Blocked-Showstopper");
									caseDataBO.setTestCaseAction("On Hold");

								} else if (status.equalsIgnoreCase("Deferredr") || status.contentEquals("Deferred")) {
									caseDataBO.setStatus("Deferred");
									caseDataBO.setTestCaseAction("N/A");

								} else if (status.equalsIgnoreCase("Blocked-FSD not clear")
										|| status.contentEquals("Blocked-FSD not clear")) {
									caseDataBO.setStatus("Blocked-FSD not clear");
									caseDataBO.setTestCaseAction("On Hold");

								} else if (status.equalsIgnoreCase("NE") || status.contentEquals("NE")) {
									caseDataBO.setStatus("NE");
									caseDataBO.setTestCaseAction("Not Started");

								} else if (status.equalsIgnoreCase("On Hold") || status.contentEquals("On Hold")) {
									caseDataBO.setStatus("On Hold");
									caseDataBO.setTestCaseAction("On Hold");

								} else if (status.equalsIgnoreCase("Duplicate") || status.contentEquals("Duplicate")) {
									caseDataBO.setStatus("Duplicate");
									caseDataBO.setTestCaseAction("N/A");

								} else if (status.equalsIgnoreCase("Out of Scope")
										|| status.contentEquals("Out of Scope")) {

									caseDataBO.setStatus("Out of Scope");
									caseDataBO.setTestCaseAction("N/A");

								} else if (status.equalsIgnoreCase("Partially Pass")
										|| status.contentEquals("partially pass")) {

									caseDataBO.setStatus("Partially Pass");
									caseDataBO.setTestCaseAction("Pass");

								}
							} else if (headingCount.equals(27)) {
								String testcaseation = dataFormatter
										.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));
								if (testcaseation.equals("") || testcaseation == null || testcaseation.isEmpty()) {
									// System.out.println("testcaseation is null");
								} else if (testcaseation.equals("Completed") || testcaseation.equals("On Hold")
										|| testcaseation.equals("Not Started") || testcaseation.equals("N/A")) {
									caseDataBO.setTestCaseAction(testcaseation);
								}
							} else if (headingCount.equals(28)) {
								String compexity = dataFormatter
										.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));
								if (compexity.equals("") || compexity == null || compexity.isEmpty()) {
									// System.out.println("compexity is empty");
								} else if (compexity.equalsIgnoreCase("Must to execute")
										|| compexity.equalsIgnoreCase("Need to execute")
										|| compexity.equalsIgnoreCase("Nice to execute")
										|| compexity.contentEquals("Must to execute")
										|| compexity.contentEquals("Need to execute")
										|| compexity.contentEquals("Nice to execute")) {

									caseDataBO.setComplexity(compexity);
								}
							} else if (headingCount.equals(29)) {

								String priority = dataFormatter
										.formatCellValue(row.getCell(headerSeq.get(heading.getKey())));
								if (priority.equals("") || priority == null || priority.isEmpty()) {
									// System.out.println("priority id empty");
								} else if (priority.equalsIgnoreCase("High") || priority.equalsIgnoreCase("Low")
										|| priority.equalsIgnoreCase("Medium") || priority.contentEquals("High")
										|| priority.contentEquals("Low") || priority.contentEquals("Medium")) {
									{
										caseDataBO.setPriority(priority);
									}
								} else {

								}
							}
							String testRunName = caseDataBO.getSuiteName();
							String testGroupName = caseDataBO.getGroupName();
							String scenario = caseDataBO.getScenarios();
							String testCaseType = caseDataBO.getTestCaseType();
							String functionality = caseDataBO.getFunctionality();
							String testCaseId = caseDataBO.getTestID();
							String status = caseDataBO.getStatus();
							String scenarioId = caseDataBO.getScenarioID();
							String actualResult = caseDataBO.getActualResult();
							String priority = caseDataBO.getPriority();
							String complexity = caseDataBO.getComplexity();
							if (testRunName == null || testRunName.isEmpty() || testRunName == ""
									|| testGroupName == null || testGroupName.isEmpty() || testGroupName == ""
									|| scenario == null || scenario.isEmpty() || scenario == "" || testCaseType == null
									|| testCaseType.isEmpty() || testCaseType == "" || functionality == null
									|| functionality.isEmpty() || functionality == "" || testCaseId == null
									|| testCaseId.isEmpty() || testCaseId == "" || actualResult == null
									|| actualResult.isEmpty() || actualResult == "" || status == null
									|| status.isEmpty() || status == "" || scenarioId == null || scenarioId.isEmpty()
									|| scenarioId == "" || priority == "" || priority == null || priority.isEmpty()
									|| complexity == "" || complexity == null || complexity.isEmpty()) {
								//// System.out.println("data is empty.");
								if (testRunName == null || testRunName.isEmpty() || testRunName == "") {

									//// System.out.println("Test suite name is empty.");
									if (caseDataBO != null) {
										String str = testCaseId + "== Test suite name is empty";
										caseDataBO.setErrorMessage(str);
										//// System.out.println("str..."+str);
									}
									// dataBOsList.addAll("Test suite name is empty.");

								} else if (testGroupName == null || testGroupName.isEmpty() || testGroupName == "") {
									//// System.out.println("Test group name is empty.");
									if (caseDataBO != null) {
										String str = testCaseId + " === Test group name is empty.";
										caseDataBO.setErrorMessage(str);
										//// System.out.println("str..."+str);
									}

								} else if (scenario == null || scenario.isEmpty() || scenario == "") {
									// System.out.println("Scenario is empty.");
									if (caseDataBO != null) {
										String str = testCaseId + "== Scenario is empty.";
										caseDataBO.setErrorMessage(str);
									}
									// caseDataBO.setErrorMessage("");

								} else if (testCaseType == null || testCaseType.isEmpty() || testCaseType == "") {
									//// System.out.println("Test Case Type is empty.");
									if (caseDataBO != null) {
										String str = testCaseId + "== Test Case Type is empty.";
										caseDataBO.setErrorMessage(str);
										// //System.out.println("str..."+str);
									}
									// caseDataBO.setErrorMessage("Test Case Type is empty.");

								} else if (functionality == null || functionality.isEmpty() || functionality == "") {
									//// System.out.println("Functionality is empty.");
									if (caseDataBO != null) {
										String str = testCaseId + "== Functionality is empty.";
										caseDataBO.setErrorMessage(str);
										// //System.out.println("str..."+str);
									}
									// caseDataBO.setErrorMessage("Functionality is empty.");

								} else if (testCaseId == null || testCaseId.isEmpty() || testCaseId == "") {
									//// System.out.println("Test Case Id is empty.");
									if (caseDataBO != null) {
										String str = testCaseId + "== Test Case Id is empty.";
										caseDataBO.setErrorMessage(str);
										// System.out.println("str..."+str);
									}
									// caseDataBO.setErrorMessage("Test Case Id is empty.");

								} else if (actualResult == null || actualResult.isEmpty() || actualResult == "") {
									//// System.out.println("Actual Result is empty.");
									if (caseDataBO != null) {
										String str = testCaseId + "== Actual Result is empty.";
										caseDataBO.setErrorMessage(str);
										// //System.out.println("str..."+str);
									}
									// caseDataBO.setErrorMessage("Actual Result is empty.");

								} else if (status == null || status.isEmpty() || status == "") {
									//// System.out.println("Status is empty.");
									if (caseDataBO != null) {
										String str = testCaseId + "== Status is empty.";
										caseDataBO.setErrorMessage(str);
										// //System.out.println("str..."+str);
									}
									// caseDataBO.setErrorMessage("Status is empty.//");

								} else if (scenarioId == null || scenarioId.isEmpty() || scenarioId == "") {
									// System.out.println("ScenarioId is empty.");
									if (caseDataBO != null) {
										String str = testCaseId + "== ScenarioId is empty.";
										caseDataBO.setErrorMessage(str);
									}
									// caseDataBO.setErrorMessage("ScenarioId is empty.");

								} else if (complexity == "" || complexity == null || complexity.isEmpty()) {
									//// System.out.println("Complexity is empty.");
									if (caseDataBO != null) {
										String str = testCaseId + "== Complexity is empty.";
										caseDataBO.setErrorMessage(str);

										// //System.out.println("str..."+str);
									}
									// caseDataBO.setErrorMessage("Complexity is empty.");

								} else if (priority == "" || priority == null || priority.isEmpty()) {
									//// System.out.println("Priority is empty.");
									if (caseDataBO != null) {
										String str = testCaseId + "== Priority is empty.";
										caseDataBO.setErrorMessage(str);
										// //System.out.println("str..."+str);
									}
									// caseDataBO.setErrorMessage("Priority is empty.");

								}
								//// System.out.println("set error meassage"+caseDataBO.getErrorMessage());

							} else {
								// System.out.println(
								// "sameTestSuite 1 name is " + sameTestSuite + "testRunName.." + testRunName);
								if (sameTestSuite == "true" || sameTestSuite.equalsIgnoreCase("true")) {
									if (suiteName == caseDataBO.getSuiteName()
											|| suiteName.equalsIgnoreCase(caseDataBO.getSuiteName())) {
										dataBOsList.add(caseDataBO);
									}
								}
								if (sameTestSuite == "false" || sameTestSuite.equalsIgnoreCase("false")) {
									if (suiteName == caseDataBO.getSuiteName()
											|| suiteName.equalsIgnoreCase(caseDataBO.getSuiteName())) {
										// System.out.println("dont want same data");
									} else {
										// System.out.println("sameTestSuite 2 name is " + sameTestSuite);

										dataBOsList.add(caseDataBO);
										// System.out.println("databolist name is " + dataBOsList.size());
									}
								}

							}
						}

					}
				}
			} else {
				// System.out.println("Invalid Excel");
			}
			// System.out.println("dataBOsList..size.."+dataBOsList.size());
			/*
			 * for(int w=0;w<=dataBOsList.size();w++) {
			 * //System.out.println("set error meassage 2.."+dataBOsList.get(w).
			 * getErrorMessage());}
			 */} catch (Exception e) {
			e.printStackTrace();
			// System.out.println("catch exception " + e.toString());
		}
		return dataBOsList;
	}

	public ServiceResponse findDuplicate(String runName) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<MannualRunTable> findname = mannualRunRepository.findByRunName(runName);
			if (findname.isEmpty()) {
				response.setServiceStatus("200");
				response.setServiceResponse("");
			} else {
				response.setServiceStatus("400");
				response.setServiceResponse("Sorry!Run Name already exists. Please edit the same. ");
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse getDefectProjectDetails(Integer projectid) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<DefectsDeatils> defectgroup = new ArrayList<>();
		try {
			//System.out.println("Porjchect defect serveices..................................");
			defectgroup = defectDao.getDefectProjectDetails(projectid);
			response.setServiceStatus("200");
			response.setServiceResponse(defectgroup);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "Something went wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getdefectseveritygraph(Integer projectid) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<defectseverity> defectgroup = new ArrayList<>();
		try {
			// System.out.println("Porjchect defect
			// serveices..................................");
			defectgroup = defectDao.getdefectseveritygraph(projectid);
			response.setServiceStatus("200");
			response.setServiceResponse(defectgroup);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "SomeThings Went Wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getAllProjectNameForMAnager(Integer userid) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		try {

			UserDetailsEntity ue = userRepository.findById(userid).orElse(null);
			List<ProjectDeatilsMeta> projectdata = projectRepository.findByCreatedBy(ue.getName());
			response.setServiceStatus("200");
			if (projectdata != null)
				response.setServiceResponse(projectdata);
			else {
				response.setServiceResponse("Project not available");
			}

		} catch (Exception e) {
			e.printStackTrace();
			Status = "SomeThings Went Wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getMangetCreatedUserList(Integer userid) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		try {

			List<userRole> ue = resourceDao.getMangetCreatedUserList(userid);
			response.setServiceStatus("200");
			if (ue != null)
				response.setServiceResponse(ue);
			else {
				response.setServiceResponse("User not available");
			}

		} catch (Exception e) {
			e.printStackTrace();
			Status = "SomeThings Went Wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getDatewiseUserForResouces(ResourceRequestBo resourceRequestBo) {
		ServiceResponse serviceResponse = new ServiceResponse();
		// //System.out.println(resourceRequestBo.getFromDate()+"
		// "+resourceRequestBo.getToDate());
		try {
			Map<String, List<ResouecesData>> exeTestCase = resourceDao.getDatewiseUserForResouces(resourceRequestBo,
					"executed");

			if (!exeTestCase.isEmpty()) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(exeTestCase);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("No Data Available");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}

		} catch (Exception e) {
			e.printStackTrace();
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse getResorcesEffort(ResourceRequestBo resourceRequestBo) {

		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			Map<String, List<ResouecesData>> exeTestCase = resourceDao.getResorcesEffort(resourceRequestBo);
			// //System.out.println("resouces ........========|||||(0 ^ -).."+exeTestCase);
			if (!exeTestCase.isEmpty()) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(exeTestCase);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("No Data Available");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;

	}

	public ServiceResponse getAllProjectName() {

		ServiceResponse serviceResponse = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<ProjectDeatilsMeta> projectdata = resourceDao.getAllProjectName();
			serviceResponse.setServiceStatus("200");
			if (projectdata != null)
				serviceResponse.setServiceResponse(projectdata);
			else {
				serviceResponse.setServiceResponse("Project not available");
			}

		} catch (Exception e) {
			e.printStackTrace();
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;

	}

	public ServiceResponse getUserListOfResouce() {

		ServiceResponse serviceResponse = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			List<userRole> usrinfo = resourceDao.getUserListOfResouce();
			serviceResponse.setServiceStatus("200");
			if (usrinfo != null)
				serviceResponse.setServiceResponse(usrinfo);
			else {
				serviceResponse.setServiceResponse("No User Data Available");
			}

		} catch (Exception e) {
			e.printStackTrace();
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;

	}

	public ServiceResponse getUserListOfResouceByProjectIDs(List<Integer> pIDs, Integer userRole, Integer userID) {

		ServiceResponse serviceResponse = new ServiceResponse();
		String Status = "Something Went Wrong..We are Unable to Process Your Request";
		List<userRole> usrinfo = new ArrayList<userRole>();
		try {
			if (userRole == 100) {
				usrinfo = resourceDao.getUserListOfResouceByProjectIDs(pIDs);
			} else if (userRole == 101) {
				usrinfo = resourceDao.getUserListOfResouceByProjectIDandManagerID(pIDs, userID);
			}

			serviceResponse.setServiceStatus("200");
			if (usrinfo != null)
				serviceResponse.setServiceResponse(usrinfo);
			else {
				serviceResponse.setServiceResponse("No User Data Available");
			}

		} catch (Exception e) {
			e.printStackTrace();
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;

	}

	public ServiceResponse tableResourceEffortsAdmin(ResourceRequestBo resourceRequestBo) {
		ServiceResponse serviceResponse = new ServiceResponse();

		try {
			List<Object[]> map = resourceDao.tableResourceEffortsAdmin(resourceRequestBo);

			if (!map.isEmpty()) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(map);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("No Data Available");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse tableResourceEffortsForManager(ResourceRequestBo resourceRequestBo) {
		ServiceResponse serviceResponse = new ServiceResponse();

		try {
			List<Object[]> map = resourceDao.tableResourceEffortsForManager(resourceRequestBo);

			if (!map.isEmpty()) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(map);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("No Data Available");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse getstuatudefectsgraph(Integer projectid) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<StatusDefectGraph> defectgroup = new ArrayList<>();
		try {
			// System.out.println("Porjchect status
			// serveices..................................");
			defectgroup = defectDao.getstuatudefectsgraph(projectid);
			response.setServiceStatus("200");
			response.setServiceResponse(defectgroup);
		} catch (Exception e) {
			e.printStackTrace();
			Status = "SomeThings Went Wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse unLockUser(String userId) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		try {
			int c = userFailedLoginsRepo.updateFailedLoginCount(userId, 0);
			response.setServiceStatus("200");
			if (c != 0)
				response.setServiceResponse("sucess");
			else
				response.setServiceResponse("no such user id");
		} catch (Exception e) {
			e.printStackTrace();
			Status = "SomeThings Went Wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	/*
	 * public ServiceResponse generatebugAutomation(ExecutionReportAutomation
	 * exLogs) { ServiceResponse response = new ServiceResponse(); String
	 * bugId=null; if(exLogs.getTbugtool().equalsIgnoreCase("bugzila")) {
	 * bugId=bugZillaService.createBugAuto(exLogs);
	 * response.setServiceStatus("200"); response.setServiceResponse(bugId); } else
	 * if(exLogs.getTbugtool().equalsIgnoreCase("jira")) { BugTrackorToolEntity
	 * bugtool=bugRepo.findByBugTrackingToolName(exLogs.getTbugtool()).get(0); try {
	 * bugId=jiraService.insert_In_JiraAuto(exLogs, bugtool); } catch
	 * (URISyntaxException | IOException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } response.setServiceStatus("200");
	 * response.setServiceResponse(bugId); } return response;
	 * 
	 * }
	 */
	public ServiceResponse getAllBugsStatus(String userId) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		try {
			int c = userFailedLoginsRepo.updateFailedLoginCount(userId, 0);
			response.setServiceStatus("200");
			if (c == 1)
				response.setServiceResponse("sucess");
			else
				response.setServiceResponse("no such user id");
		} catch (Exception e) {
			e.printStackTrace();
			Status = "SomeThings Went Wrong";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;

	}

	public ServiceResponse getProjectToolDetails(int projectid) {
		ServiceResponse response = new ServiceResponse();
		try {
			List<Object[]> objectList = projectRepository.getProjectToolDetails(projectid);

			if (!objectList.isEmpty()) {

				List<ResourceResponseBo> list = new ArrayList<>();

				for (Object[] object : objectList) {
					ResourceResponseBo responseBo = new ResourceResponseBo();
					responseBo.setToolName(object[1].toString());
					list.add(responseBo);
				}

				response.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				response.setServiceResponse(list);

			} else {
				response.setServiceStatus(ServiceResponse.STATUS_FAIL);
				response.setServiceResponse("Project not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus(ServiceResponse.STATUS_FAIL);
			response.setServiceResponse("SomeThings Went Wrong");
		}
		return response;
	}

}