package com.apmosys.tmt.services;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apmosys.tmt.dao.DashboardDao;
import com.apmosys.tmt.models.ProjectDetailsAuto;
import com.apmosys.tmt.models.TestSuitesData;
import com.apmosys.tmt.models.dashboardprojectdetailsAuto;
import com.apmosys.tmt.models.dashboardprojectwisedetail;
import com.apmosys.tmt.models.maindashboardproject;
import com.apmosys.tmt.repository.configurationRepository;
import com.apmosys.tmt.utils.ServiceResponse;

@Service
public class DashboardService {
	@Autowired
	public configurationRepository congifrepo;	
	@Autowired
	public DashboardDao dashboarddao;	
	private static DecimalFormat df2 = new DecimalFormat("#.##");
	public ServiceResponse projectholeview(int id,int role, String isActive,int pageNo,int PageSize) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<dashboardprojectwisedetail> maindashboard1= new ArrayList<dashboardprojectwisedetail>();
		List<maindashboardproject> maindashboard  = new ArrayList<maindashboardproject>();
		try {
			String who=congifrepo.userRole(role);
//			String who="Admin";
				if(who.equals("Admin"))
				{
					maindashboard = dashboarddao.projectholeview(pageNo,PageSize); 
					System.out.println("maindashboardmaindashboardmaindashboardmaindashboard");
					

				}
				else
				{
					maindashboard = dashboarddao.projectholeviewaccess(id); 
					System.out.println("hii Except Admin");
					 for(maindashboardproject b:maindashboard){  
						    maindashboard1 = dashboarddao.projectviewdata(Integer.valueOf(b.getProjectid()),isActive);
						    int getAllocationtester=dashboarddao.getTotalTester(Integer.valueOf(b.getProjectid()));
						    for(dashboardprojectwisedetail test:maindashboard1)
						       {
						    	b.setTotalRunTestcases(test.getTotaltestcases());
						    	b.setTotalcurrentrunpass(test.getTotalpass());
						    	b.setTotalcurrentrunfail(test.getTotalfail());
						    	b.setTotalcompleted(test.getTotalcompleted());
						    	b.setTotalnotstarted(test.getTotalnotstarted());
						    	b.setTotalonhold(test.getTotalonhold());
						    	b.setTotalunderreview(test.getTotalunderreview());
						    	b.setTotalNA(test.getTotalNA());
						    	b.setTotalcurrentrunmajor(test.getTotalcurrentrunmajor());
						    	b.setTotalcurrentrunminor(test.getTotalcurrentrunminor());
						    	b.setTotalcurrentrunblocker(test.getTotalcurrentrunblocker());
						    	b.setTotalcurrentruncritical(test.getTotalcurrentruncritical());
						    	b.setManhours(test.getSetTotalmanhours());
						    	b.setTotalbugs(test.getTotalbugs());
						    	b.setTotalallotedresource(getAllocationtester);
						    	b.setTotalactionincomplete(test.getTotalactionincomplete());
						    	float test1=test.getTotalpass();
								//float test2=test.getTotalcompleted();
						    	float test2=b.getTotaltestcases();
								if(test2!=0)
								b.setSuccessrate(df2.format((test1*100)/test2));
								b.setOpenBug(test.getOpenBug());
								b.setCloseBug(test.getCloseBug());
								b.setReOpenBug(test.getReOpenBug());
								b.setResolvedBug(test.getResolvedBug());
								b.setDay(test.getDayspend());
								b.setHour(test.getHourspend());
								b.setMinute(test.getMinutespend());
						    	/*//b.setTotaltestcases(b.getTotaltestcases());
						    	System.out.println("Test caese total"+b.getTotaltestcases());
						    	b.setTotalcurrentrunpass(test.getTotalpass());
						    	b.setTotalcurrentrunfail(test.getTotalfail());
						    	//b.setTotalcompleted(test.getTotalcompleted());
						    	b.setTotalnotstarted(test.getTotalnotstarted());
						    	b.setTotalonhold(test.getTotalonhold());
						    	b.setTotalunderreview(test.getTotalunderreview());
						    	b.setTotalNA(test.getTotalNA());
						    	b.setTotalcurrentrunmajor(test.getTotalcurrentrunmajor());
						    	b.setTotalcurrentrunminor(test.getTotalcurrentrunminor());
						    	b.setTotalcurrentrunblocker(test.getTotalcurrentrunblocker());
						    	b.setTotalcurrentruncritical(test.getTotalcurrentruncritical());
						    	b.setManhours(test.getSetTotalmanhours().toString());
						    	b.setTotalbugs(test.getTotalbugs());
						    	b.setTotalallotedresource(getAllocationtester);
						    	float test1=test.getTotalpass();
								float test2=test.getTotalcompleted();
								b.setSuccessrate(df2.format((test1*100)/test2));
								b.setOpenBug(test.getOpenBug());
								b.setCloseBug(test.getCloseBug());
								b.setReOpenBug(test.getReOpenBug());
								b.setDay(test.getDayspend());
								b.setHour(test.getHourspend());
								b.setMinute(test.getMinutespend());*/
						       }
						    } 
				}
				
			if(maindashboard!=null)
			{
				response.setServiceStatus("200");
				System.out.print("maindashboard:"+maindashboard);
				response.setServiceResponse(maindashboard);
			}
			else
			{
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}
	
	public ServiceResponse projectviewdata(int projectid,int role, String isActive) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		List<dashboardprojectwisedetail> maindashboard  = new ArrayList<>();
		try {
			String who=congifrepo.userRole(role);
//			System.out.println(who);
				if(who.equals("Admin"))
				{
					maindashboard = dashboarddao.projectviewdata(projectid,isActive); 
					
				}
				else
				{
					//maindashboard = projectDao.projectholeviewAccesswise(id); 
				}
				
			if(maindashboard!=null)
			{
				response.setServiceStatus("200");
				response.setServiceResponse(Status);
				response.setServiceResponse(maindashboard);
			}
			else
			{
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}
	public ServiceResponse projectholeviewAuto(int userid,int role,int start) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
//		System.out.println("projectholeviewAutoRunning");
		List<dashboardprojectdetailsAuto> maindashboard  = new ArrayList<>();
		try {
			String who=congifrepo.userRole(role);
//			System.out.println(who);
				//if(who.equals("Admin"))
				//{
					maindashboard = dashboarddao.projectviewdataAuto(who,userid,start); 
					
				//}
				//else
				//{
					//maindashboard = projectDao.projectholeviewAccesswise(id); 
				//}
				
			if(maindashboard!=null)
			{
				response.setServiceStatus("200");
				response.setServiceResponse(Status);
				response.setServiceResponse(maindashboard);
			}
			else
			{
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}
	public ServiceResponse getRunStatus(int projectid, int id, int user_role) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
//		System.out.println("overview running");
		List<TestSuitesData> maindashboard  = new ArrayList<>();
		try {
			String who=congifrepo.userRole(user_role);
//			System.out.println(who);
				
				maindashboard = dashboarddao.getRunStatus(projectid); 
					
			if(maindashboard!=null)
			{
				response.setServiceStatus("200");
				response.setServiceResponse(Status);
				response.setServiceResponse(maindashboard);
			}
			else
			{
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}
}
