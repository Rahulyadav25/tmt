package com.apmosys.tmt.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.apmosys.tmt.BO.ExecutionReportAutomation;
import com.apmosys.tmt.BO.ExtendedReportBo;
import com.apmosys.tmt.dao.AutomationLogsDao;
import com.apmosys.tmt.dao.AutomationRunDao;
import com.apmosys.tmt.models.AutomationMetadata;
import com.apmosys.tmt.models.AutomationRunLogs;
import com.apmosys.tmt.models.AutomationRunTable;
import com.apmosys.tmt.models.AutomationScreenshot;
import com.apmosys.tmt.models.BugTrackorToolEntity;
import com.apmosys.tmt.models.ExecutionStepsBo;
import com.apmosys.tmt.models.ProjectDeatilsMeta;
import com.apmosys.tmt.models.UpdateLogs;
import com.apmosys.tmt.repository.AutomationMetaRepository;
import com.apmosys.tmt.repository.AutomationScreenshotRepository;
import com.apmosys.tmt.repository.BugTrackerRepository;
import com.apmosys.tmt.repository.ProjectRepository;
import com.apmosys.tmt.repository.UpdateLogsRepository;
import com.apmosys.tmt.utils.ConstantParameter;
import com.apmosys.tmt.utils.ServiceResponse;

@Service
public class AutomationService {

	@Value("${automationPath}")
	private String automationPath;
	@Value("${dependencyPath}")
	private String dependencyPath;
	@Autowired
	AutomationRunDao automationRunDao;
	@Autowired
	AutomationLogsDao automatioLogsDao;
	@Autowired
	AutomationScreenshotRepository automationScreenshotRepository;
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	ProjectRepository projectrepo;
	@Autowired
	private BugTrackerRepository bugRepo;
	@Autowired
	private UpdateLogsRepository updateLogsRepository;
	@Autowired
	private AutomationMetaRepository AutomationMetaRepository;

	public Integer dependencyFileCount = 0;

	public ServiceResponse checkControllerFile(String projectName) {

		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			File fileAuto = new File(automationPath + File.separator + projectName);
			if (!fileAuto.exists()) {
				fileAuto.mkdirs();
			}
			File[] listOfFiles = fileAuto.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					if (listOfFiles[i].getName().equals("Main_Controller.xlsx")) {
						serviceResponse.setServiceResponse("Controller found");
						break;
					} else {
						serviceResponse.setServiceResponse("Controller not found");
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	public ServiceResponse checkDataSheetFile(String projectName, String sheetname) {

		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			File dataSheet = new File(automationPath + File.separator + projectName + File.separator + "DataSheets");
			if (!dataSheet.exists()) {
				dataSheet.mkdirs();
			}
			//			System.out.println("datasheet..." + dataSheet);
			File[] listOfFiles = dataSheet.listFiles();
			//			System.out.println("listOfFiles..." + listOfFiles.length);
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					if (listOfFiles[i].getName().equals(sheetname)) {
						serviceResponse.setServiceResponse("Datasheet found");
						break;
					} else {
						serviceResponse.setServiceResponse("Datasheet not found");
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	public ServiceResponse checkDatasheetsFile(String projectName) {

		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			File fileAuto = new File(automationPath + File.separator + projectName);
			if (!fileAuto.exists()) {
				fileAuto.mkdirs();
			}
			File[] listOfFiles = fileAuto.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					if (listOfFiles[i].getName().equals("Main_Controller.xlsx")) {
						serviceResponse.setServiceResponse("Controller found");
						break;
					} else {
						serviceResponse.setServiceResponse("Controller not found");
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	public ServiceResponse uploadDataSheets(MultipartFile file, String projectName, int projectid) {

		ServiceResponse serviceResponse = new ServiceResponse();
		Boolean inUpdateLogs;
		String status;
		try {
			int result = 0;
			String filename = file.getOriginalFilename();
			File fileAuto = new File(automationPath + File.separator + projectName + File.separator + "DataSheets");
			if (!fileAuto.exists()) {
				fileAuto.mkdirs();
			}
			byte[] bytes = file.getBytes();
			Path path = Paths.get(fileAuto + File.separator + filename, new String[0]);
			Files.write(path, bytes, new OpenOption[0]);
			// add jars and drivers
			int first2 = 0;
			File[] listOfFiles = fileAuto.listFiles();
			try {

				for (int i = 0; i < listOfFiles.length; i++) {
					if (listOfFiles[i].isFile()) {
						// ---------------
						XSSFWorkbook myWorkBook1 = new XSSFWorkbook(automationPath + File.separator + projectName
								+ File.separator + "DataSheets" + File.separator + listOfFiles[i].getName());
						XSSFSheet mySheet1 = myWorkBook1.getSheetAt(1);
						//						System.out.println(listOfFiles[i].getName());
						int rowshet = 0;
						for (Row row : mySheet1) {
							if (rowshet == 0) {
							} else {
								for (Cell c : row) {
//									if (c.getCellType() != CellType.BLANK) {
									if (c.getCellType() != CellType.BLANK) {
										first2 = first2 + 1;
										break;
									}
								}
							}
							/* count=+1; */
							rowshet = +1;
						}

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			//-------------------------
			File dependency = new File(dependencyPath);
			File controller = new File(automationPath + File.separator + projectName);
			File[] listOfFiles1 = controller.listFiles();
			/*
			 * for (int i = 0; i < listOfFiles1.length; i++) { if (listOfFiles1[i].isFile())
			 * { if(listOfFiles1[i].getName().equals("Main_Controller.xlsx")) {
			 * if(dependencyFileCount==0) { FileUtils.copyDirectory(dependency, controller);
			 * dependencyFileCount++; } break; } } }
			 */
			XSSFWorkbook myWorkBook = new XSSFWorkbook(
					automationPath + File.separator + projectName + File.separator + "Main_Controller.xlsx");
			XSSFSheet mySheet = myWorkBook.getSheetAt(0);
			int first = -1;

			for (Row row : mySheet) {
				first = first + 1;
			}
			AutomationMetadata metadata = new AutomationMetadata();
			metadata.setProjectid(projectid);
			metadata.setProjectname(projectName);
			metadata.setLocationofDatasheet(fileAuto.toString());
			metadata.setNoOfDatasheet(listOfFiles.length);
			metadata.setNoOftestSuites(first);
			metadata.setNoOfTestcases(first2);
			result = automationRunDao.saveAutoSheetDetails(metadata);
			if (result == 1) {
				// serviceResponse.setServiceResponse("Datasheets uploaded successfully");
				status = "Datasheet uploaded successfully.";
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				serviceResponse.setServiceError("");
			} else {
				// serviceResponse.setServiceResponse("Datasheets uploaded successfully,db
				// insertion fails");
				status = "Datasheet uploaded successfully,db insertion fails.";
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				serviceResponse.setServiceError("");
			}

			inUpdateLogs = updateProjectLogsTable(projectid);
			/*
			 * if(inUpdateLogs==true) { status=status+" Sheet update time inserted."; } else
			 * { status=status+" Sheet update time not inserted."; }
			 */

		} catch (Exception e) {
			e.printStackTrace();
			// serviceResponse.setServiceResponse("Could not upload file");
			status = "Could not upload file";
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			serviceResponse.setServiceError("");
		}
		serviceResponse.setServiceResponse(status);
		return serviceResponse;
	}

	public Boolean updateProjectLogsTable(Integer projectID) {
		List<UpdateLogs> ul = null;
		Date dt = new Date();
		try {
			ul = updateLogsRepository.findByProjectId(projectID);
			if (ul.isEmpty()) {
				UpdateLogs ulnew = new UpdateLogs();
				ulnew.setProjectId(projectID);
				ulnew.setLastModified(dt);
				updateLogsRepository.save(ulnew);
			} else {
				for (UpdateLogs u : ul) {
					u.setLastModified(dt);
				}
				updateLogsRepository.saveAll(ul);
			}

			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public ServiceResponse getProjectAutoFiles(String projectName) {

		ServiceResponse serviceResponse = new ServiceResponse();

		File projectPath = new File(automationPath + File.separator + projectName);
		if (!projectPath.exists()) {
			projectPath.mkdirs();
		}
		File[] listOfFiles1 = projectPath.listFiles();

		File dataSheet = new File(automationPath + File.separator + projectName + File.separator + "DataSheets");
		if (!dataSheet.exists()) {
			dataSheet.mkdirs();
		}
		File[] listOfFiles2 = dataSheet.listFiles();
		Integer totalFile = listOfFiles1.length + listOfFiles2.length;
		String[] fileName = new String[totalFile];
		int z = 1;
		try {
			for (int i = 0; i < listOfFiles1.length; i++) {
				if (listOfFiles1[i].isFile()) {
					if (listOfFiles1[i].getName().equals("Main_Controller.xlsx")) {
						fileName[0] = "Main_Controller.xlsx";
						break;
					} else {
						fileName[0] = "Main Controller not added yet";
						continue;
					}
				}
			}

			int count = 0;
			for (int i = 0; i < listOfFiles2.length; i++) {
				if (listOfFiles2[i].isFile()) {
					String extension = FilenameUtils.getExtension(listOfFiles2[i].getName());
					if ((extension.equals("xlsx")) || (extension.equals("xls"))) {
						if (!(listOfFiles2[i].getName().contains("~$"))) {
							fileName[z] = listOfFiles2[i].getName();
							//							System.out.println(z + "          " + fileName[z]);
							z++;
							count++;
						}
					}
					if (count == 0) {
						fileName[1] = "No datasheet found";
						// continue;
					}
				}
			}
			serviceResponse.setServiceResponse(fileName);
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
			serviceResponse.setServiceError("");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	public Resource downloadAutoFile(String type, String fileName, String projectName) {

		Resource resource = null;
		String stringFile = null;
		File file = null;
		//byte[] buffer = new byte[1024];
		//BufferedInputStream bis;
		try {
			/*
			 * if(type.equals("controller")) { servletResponse.setContentType(
			 * "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			 * servletResponse.setHeader("Content-Disposition", "attachment; filename=\""+
			 * fileName+"\""); InputStream inputStream = new FileInputStream(new
			 * File(automationPath+File.separator+projectName+File.separator+fileName)); int
			 * nRead; while ((nRead = inputStream.read()) != -1) {
			 * servletResponse.getWriter().write(nRead); }
			 * //serviceResponse.setServiceResponse(servletResponse); }
			 */
			if (type.equals("controller")) {
				stringFile = automationPath + File.separator + projectName + File.separator + fileName;
				file = new File(automationPath + File.separator + projectName + File.separator + fileName);
			}
			if (type.equals("datasheets")) {
				stringFile = automationPath + File.separator + projectName + File.separator + "DataSheets"
						+ File.separator + fileName;
				file = new File(automationPath + File.separator + projectName + File.separator + "DataSheets"
						+ File.separator + fileName);
			}

			if (file.exists()) {
				resource = new FileSystemResource(stringFile);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resource;
	}

	public ServiceResponse deleteAutomationFile(String type, String fileName, String projectName) {
		ServiceResponse response = new ServiceResponse();
		File file = null;
		int first2 = 0;
		String status = "";
		try {
			if (type.equals("controller")) {
				// stringFile = automationPath + File.separator + projectName + File.separator +
				// fileName;
				file = new File(automationPath + File.separator + projectName + File.separator + fileName);
			}
			if (type.equals("datasheets")) {
				// stringFile = automationPath + File.separator + projectName + File.separator +
				// "DataSheets"
				// + File.separator + fileName;
				file = new File(automationPath + File.separator + projectName + File.separator + "DataSheets"
						+ File.separator + fileName);
			}
			/*
			 * File dataSheet = new File(automationPath + File.separator + projectName +
			 * File.separator + "DataSheets"); File[] listOfFiles = dataSheet.listFiles();
			 * for (int i = 0; i < listOfFiles.length; i++) { if (listOfFiles[i].isFile()) {
			 * //--------------- XSSFWorkbook myWorkBook1 = new XSSFWorkbook(automationPath
			 * + File.separator + projectName + File.separator + "DataSheets" +
			 * File.separator + listOfFiles[i].getName()); XSSFSheet mySheet1 =
			 * myWorkBook1.getSheetAt(1); System.out.println(listOfFiles[i].getName()); int
			 * rowshet = 0; if(listOfFiles[i].getName().equals(fileName)) { for (Row row :
			 * mySheet1) { if (rowshet == 0) { } else { for (Cell c : row) { if
			 * (c.getCellType() != CellType.BLANK) { first2 = first2 + 1; break; } } }
			 * count=+1; rowshet = +1; }
			 * 
			 * } } System.out.println("first2..."+first2); }
			 */

			if (file.exists()) {
				if (file.delete()) {
					//					System.out.println("new code");
					try {
						if (type.equals("datasheets")) {
							System.out.println("new if part of the code..");

							File dataSheet = new File(
									automationPath + File.separator + projectName + File.separator + "DataSheets");
							File[] listOfFiles = dataSheet.listFiles();
							for (int i = 0; i < listOfFiles.length; i++) {
								if (listOfFiles[i].isFile()) {

									XSSFWorkbook myWorkBook1 = new XSSFWorkbook(automationPath + File.separator
											+ projectName + File.separator + "DataSheets" + File.separator
											+ listOfFiles[i].getName());
									XSSFSheet mySheet1 = myWorkBook1.getSheetAt(1);
									//									System.out.println(listOfFiles[i].getName());
									int rowshet = 0;
									for (Row row : mySheet1) {
										if (rowshet == 0) {
										} else {
											for (Cell c : row) {
												if (c.getCellType() != CellType.BLANK) {
													first2 = first2 + 1;
													break;
												}
											}
										}
										/* count=+1; */
										rowshet = +1;
									}
									System.out.println("first2.."+first2);
								}
							}

							//							System.out.println("first2...Outside for loop...."+first2);

							// AutomationMetadata autdata=new AutomationMetadata();
							AutomationMetadata datatestcasefind = AutomationMetaRepository
									.findByProjectname(projectName);
							int numberoftc = datatestcasefind.getNoOfTestcases();
							int s1 = numberoftc - first2;
							//							System.out.println("s1..." + s1);
							if (datatestcasefind != null) {
								AutomationMetaRepository.updateDataone(datatestcasefind.getProjectid(), s1);
							}

						}

					} catch (Exception e) {
						e.printStackTrace();
					}

					if (type.equals("controller")) {
						// AutomationMetadata aut=new AutomationMetadata();
						AutomationMetadata autofind = AutomationMetaRepository.findByProjectname(projectName);
						if (autofind != null) {
							AutomationMetaRepository.updateData(autofind.getProjectid());

						} else {

						}
					}

					status = "File deleted successfully...";
					response.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
					response.setServiceError("");
				} else {
					status = "File can not be deleted!!!";
					response.setServiceStatus(ServiceResponse.STATUS_FAIL);
					response.setServiceError(status);
				}
			} else {
				status = "File does not exist!!";
				response.setServiceStatus(ServiceResponse.STATUS_FAIL);
				response.setServiceError(status);
			}
		} catch (Exception e) {
			e.printStackTrace();
			status = "File can not be deleted!!!";
			response.setServiceStatus(ServiceResponse.STATUS_FAIL);
			response.setServiceError(status);
		}
		//		System.out.println("Deleted file status : " + status);
		response.setServiceResponse(status);
		return response;
	}

	public ServiceResponse getAutomationRunReport(String pName) {
		ServiceResponse serviceResponse = new ServiceResponse();
		// ArrayList<AutomationRunTable> runTable=new ArrayList<AutomationRunTable>();
		List<AutomationRunTable> list = new ArrayList<>();
		List<Object[]> Objlist = new ArrayList<>();


		try {
			Objlist = automationRunDao.getAutomationRunReport(pName);

			/*
			 * for(AutomationRunTable arr:list ) { String status=arr.getStatus();
			 * if(status=="Success" || status.equals("Success")) {
			 * arr.setStatus("Complete"); } else if(status=="Fail" ||
			 * status.equals("Fail")){ arr.setStatus("Incomplete"); } //Object[] obj= new
			 * Object[list.size()][];
			 * 
			 * } Object[][] objs = new Object[list.size()][];
			 * System.out.println("list.get(i).getRunId().."+list.get(0).getRunId());
			 * for(int i= 0; i < list.size(); i++) {
			 * System.out.println("list.get(i).getRunId().."+list.get(i));
			 * 
			 * 
			 * Objlist.get(i)[0] = list.get(i).getRunId(); Objlist.get(i)[1] =
			 * list.get(i).getApplicationID(); Objlist.get(i)[2] =
			 * list.get(i).getStart_time(); Objlist.get(i)[3] = list.get(i).getEnd_time();
			 * Objlist.get(i)[4] = list.get(i).getTotal_time(); Objlist.get(i)[5] =
			 * list.get(i).getStatus();
			 * 
			 * }
			 */			


			//			List<String> scenariosid=null;
			//			for(Object[] arr:Objlist)
			//			{
			////				System.out.println("set values....."+arr[5].toString());
			//				/*
			//				 * String sucess=arr[5].toString(); if(sucess.equals("Success")) {
			//				 * arr[5].equals("Comlepeted"); }
			//				 */
			//				
			//				
			//				List<ExecutionStepsBo> newvalue=automationRunDao.getPassoutinfomation(arr[0].toString());
			//				
			//				for(int i=0;i<=newvalue.size();i++)	
			//				{
			//					
			//						if(scenariosid.get(i) != newvalue.get(i).getScenarioId()){
			//							scenariosid.set(i, newvalue.get(i).getScenarioId());
			//							System.out.println("scenariosid..."+scenariosid);
			//					        break;
			//						
			//						/*
			//						 * if(scenariosid) scenariosid=newvalue.get(i).getScenarioId();
			//						 */
			//					}
			//				}
			//				
			//				System.out.println("newva,ues..."+newvalue);
			//				
			//				
			//			}

			serviceResponse.setServiceResponse(Objlist);
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
			serviceResponse.setServiceError("");

		} catch (Exception e) {
			e.printStackTrace();  
		}
		return serviceResponse; 
	}
	//pranik
	public ServiceResponse getDefectReport(String pName) {
		ServiceResponse serviceResponse = new ServiceResponse();
		// ArrayList<AutomationRunTable> runTable=new ArrayList<AutomationRunTable>();
		List<AutomationRunTable> list = new ArrayList<>();
		List<Object[]> Objlist = new ArrayList<>();


		try {
			Objlist = automationRunDao.getDefectReport(pName);

			/*
			 * for(AutomationRunTable arr:list ) { String status=arr.getStatus();
			 * if(status=="Success" || status.equals("Success")) {
			 * arr.setStatus("Complete"); } else if(status=="Fail" ||
			 * status.equals("Fail")){ arr.setStatus("Incomplete"); } //Object[] obj= new
			 * Object[list.size()][];
			 * 
			 * } Object[][] objs = new Object[list.size()][];
			 * System.out.println("list.get(i).getRunId().."+list.get(0).getRunId());
			 * for(int i= 0; i < list.size(); i++) {
			 * System.out.println("list.get(i).getRunId().."+list.get(i));
			 * 
			 * 
			 * Objlist.get(i)[0] = list.get(i).getRunId(); Objlist.get(i)[1] =
			 * list.get(i).getApplicationID(); Objlist.get(i)[2] =
			 * list.get(i).getStart_time(); Objlist.get(i)[3] = list.get(i).getEnd_time();
			 * Objlist.get(i)[4] = list.get(i).getTotal_time(); Objlist.get(i)[5] =
			 * list.get(i).getStatus();
			 * 
			 * }
			 */			


			//			List<String> scenariosid=null;
			//			for(Object[] arr:Objlist)
			//			{
			////				System.out.println("set values....."+arr[5].toString());
			//				/*
			//				 * String sucess=arr[5].toString(); if(sucess.equals("Success")) {
			//				 * arr[5].equals("Comlepeted"); }
			//				 */
			//				
			//				
			//				List<ExecutionStepsBo> newvalue=automationRunDao.getPassoutinfomation(arr[0].toString());
			//				
			//				for(int i=0;i<=newvalue.size();i++)	
			//				{
			//					
			//						if(scenariosid.get(i) != newvalue.get(i).getScenarioId()){
			//							scenariosid.set(i, newvalue.get(i).getScenarioId());
			//							System.out.println("scenariosid..."+scenariosid);
			//					        break;
			//						
			//						/*
			//						 * if(scenariosid) scenariosid=newvalue.get(i).getScenarioId();
			//						 */
			//					}
			//				}
			//				
			//				System.out.println("newva,ues..."+newvalue);
			//				
			//				
			//			}

			serviceResponse.setServiceResponse(Objlist);
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
			serviceResponse.setServiceError("");

		} catch (Exception e) {
			e.printStackTrace();  
		}
		return serviceResponse; 
	}
	public ServiceResponse getAllDefectReport(String runId,String projectName) {
		ServiceResponse serviceResponse = new ServiceResponse();
		// ArrayList<AutomationRunTable> runTable=new ArrayList<AutomationRunTable>();
		List<AutomationRunTable> list = new ArrayList<>();
		List<Object[]> Objlist = new ArrayList<>();


		try {
			Objlist = automationRunDao.getAllDefectReport(runId,projectName);

			

			serviceResponse.setServiceResponse(Objlist);
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
			serviceResponse.setServiceError("");

		} catch (Exception e) {
			e.printStackTrace();  
		}
		return serviceResponse; 
	}
	public ServiceResponse getExtendedReport(String runId,String scenario) {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			int count = 0;
			if (runId != null) {

				List<ExecutionStepsBo> executionStepsBos = automatioLogsDao
						.getExecutionStepsByRunid(Integer.parseInt(runId));
				Map<String, ExecutionReportAutomation> map = automatioLogsDao
						.getExecutionLogsByRunid(Integer.parseInt(runId), scenario);
				Map<String, List<ExtendedReportBo>> testcaseData = new HashMap<>();
				Map<String, List<ExtendedReportBo>> scenarioData = new HashMap<>();

				List<ExecutionStepsBo> stepsBo = new ArrayList<ExecutionStepsBo>();
				List<ExtendedReportBo> reportList = new ArrayList<ExtendedReportBo>();

				for (ExecutionStepsBo executionSteps : executionStepsBos) {

					try {
						stepsBo.add(executionSteps);

						if ((executionSteps.getControl().equalsIgnoreCase("V"))
								|| (executionSteps.getControl().contains("D"))|| (executionSteps.getControl().contains("C"))
								|| (executionSteps.getControl().contains("T"))|| (executionSteps.getControl().contains("E"))) {
							ExtendedReportBo reportBo = new ExtendedReportBo();

//							try {
//								reportBo.setBrowser((map.get(executionSteps.getLogsId()).getBrowserType() != null)
//										? map.get(executionSteps.getLogsId()).getBrowserType()
//												: null);
//							}
//							catch(Exception e) {
//								e.printStackTrace();
//							}
							if (map.get(executionSteps.getLogsId()) != null) {
								reportBo.setBrowser(map.get(executionSteps.getLogsId()).getBrowserType());
								reportBo.setBugsummary(map.get(executionSteps.getLogsId()).getTbugsummary());
								reportBo.setDescription(map.get(executionSteps.getLogsId()).getTdescription());
								reportBo.setExpectedresult(map.get(executionSteps.getLogsId()).getTexpectedResult());
								reportBo.setSummary(map.get(executionSteps.getLogsId()).getTsummary());
								reportBo.setSeverity(map.get(executionSteps.getLogsId()).getTseverity());
								// reportBo.setActualresult(map.get(executionSteps.getLogsId()).getTexpectedResult());
								reportBo.setPriority(map.get(executionSteps.getLogsId()).getTpriority());
								reportBo.setStatus(map.get(executionSteps.getLogsId()).getTstatus());
								ProjectDeatilsMeta data = projectrepo
										.findByprojectName(map.get(executionSteps.getLogsId()).getProjectName());
								count++;
								String id = data.getIsJiraEnable();
								if (!(StringUtils.isBlank(id))) {
									BugTrackorToolEntity bugtool = bugRepo.findByToolid(Integer.valueOf(id));
									if (bugtool != null) {
										reportBo.setBugtool(
												(bugtool.getBugTrackingToolName() != null) ? bugtool.getBugTrackingToolName()
														: null);
									}
								}
								reportBo.setProjectname(data.getProjectName());
								reportBo.setSteps(stepsBo);
								//System.out.println("executionSteps.getLogsId().."+executionSteps.getLogsId());
								reportBo.setVerify(map.get(executionSteps.getLogsId()));
								reportList.add(reportBo);
								stepsBo = new ArrayList<ExecutionStepsBo>();

							} else {
								reportBo.setBrowser("NO DATA");
								reportBo.setBugsummary("NO DATA");
								reportBo.setDescription("NO DATA");
								reportBo.setExpectedresult("NO DATA");
								reportBo.setSummary("NO DATA");
								reportBo.setSeverity("NO DATA");
								reportBo.setPriority("NO DATA");
								reportBo.setStatus("NO DATA");
								reportBo.setBugtool("NO DATA");
								reportBo.setProjectname("NO DATA");
								
								ProjectDeatilsMeta data = projectrepo
										.findByprojectName(map.get(executionSteps.getLogsId()).getProjectName());
							//	System.out.println("ProjectDeatilsMeta"+data);

								count++;
								String id = data.getIsJiraEnable();
								if (!(StringUtils.isBlank(id))) {
									BugTrackorToolEntity bugtool = bugRepo.findByToolid(Integer.valueOf(id));
									if (bugtool != null) {
										reportBo.setBugtool(
												(bugtool.getBugTrackingToolName() != null) ? bugtool.getBugTrackingToolName()
														: null);
									}
								}
								reportBo.setProjectname(data.getProjectName());
								reportBo.setSteps(stepsBo);
								//System.out.println("executionSteps.getLogsId().."+executionSteps.getLogsId());
								reportBo.setVerify(map.get(executionSteps.getLogsId()));
								reportList.add(reportBo);
								stepsBo = new ArrayList<ExecutionStepsBo>();
							}
						//	System.out.print("executionSteps.getLogsId()"+executionSteps.getLogsId());
						//	System.out.print("map.get(executionSteps.getLogsId())"+map.get(executionSteps.getLogsId()));
							//System.out.print("map.get(executionSteps.getLogsId()).getBrowserType()"+map.get(executionSteps.getLogsId()).getBrowserType());

						//	reportBo.setBrowser(map.get(executionSteps.getLogsId()).getBrowserType());

							//System.out.println("map.get(executionSteps.getLogsId()).getTstatus().."+map.get(executionSteps.getLogsId()).getTstatus());

							
							
						}
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
				for (ExtendedReportBo report : reportList) { //gives 25
					try {

						List<ExtendedReportBo> values = reportList.stream()
								.filter(x -> x.getVerify().getTestCaseId().equals(report.getVerify().getTestCaseId()))
								.collect(Collectors.toList());
						//System.out.println("reportList.stream()\r\n" + ".filter(x -> x.getVerify().getTestCaseId().equals(report.getVerify().getTestCaseId()))\r\n" + 
						//	".collect(Collectors.toList())"+reportList.stream()
						//.filter(x -> x.getVerify().getTestCaseId().equals(report.getVerify().getTestCaseId()))
						//.collect(Collectors.toList()));
						testcaseData.put(report.getVerify().getTestCaseId(), values);
		              //  System.out.print("values"+values);

						List<ExtendedReportBo> valuesScenario = reportList.stream()
								.filter(x -> x.getVerify().getScenarioId().equals(report.getVerify().getScenarioId()))
								.collect(Collectors.toList());
						scenarioData.put(report.getVerify().getScenarioId(), valuesScenario);
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}

				/*
				 * for (Map.Entry<String, List<ExtendedReportBo>> entry :
				 * testcaseData.entrySet()) { Map<String, List<ExtendedReportBo>> values =
				 * testcaseData.stream() .filter(x -> x.equals(entry.))
				 * .collect(Collectors.toList());
				 * testcaseData.put(report.getVerify().getTestCaseId(), values); }
				 */
				/*
				 * for (ExecutionStepsBo executionSteps: executionStepsBos) {
				 * stepsBo.add(executionSteps);
				 * if(executionSteps.getControl().equalsIgnoreCase("V")) {
				 * System.out.println("abc"); ExtendedReportBo reportBo=new ExtendedReportBo();
				 * reportBo.setSteps(stepsBo);
				 * reportBo.setVerify(map.get(executionSteps.getLogsId()));
				 * reportList.add(reportBo); stepsBo=new ArrayList<ExecutionStepsBo>(); } }
				 */
				List<Map<String, List<ExtendedReportBo>>> reportData = new ArrayList<Map<String, List<ExtendedReportBo>>>();
				reportData.add(testcaseData);
				reportData.add(scenarioData);
				serviceResponse.setServiceResponse(reportData);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				serviceResponse.setServiceError(202);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}
	public Map<String,Integer> getTotalScenariosTestCasesStepsCounts(String runId,String scenario) {
		Map<String,Integer> m=null;
		try {
			int count = 0;
			if (runId != null) {

				List<ExecutionStepsBo> executionStepsBos = automatioLogsDao
						.getExecutionStepsByRunid(Integer.parseInt(runId));

				Map<String, ExecutionReportAutomation> map = automatioLogsDao
						.getExecutionLogsByRunid(Integer.parseInt(runId), scenario);

				Map<String, List<ExtendedReportBo>> testcaseData = new HashMap<>();
				Map<String, List<ExtendedReportBo>> scenarioData = new HashMap<>();

				List<ExecutionStepsBo> stepsBo = new ArrayList<ExecutionStepsBo>();
				List<ExtendedReportBo> reportList = new ArrayList<ExtendedReportBo>();

				for (ExecutionStepsBo executionSteps : executionStepsBos) {

					try {
						stepsBo.add(executionSteps);

						if ((executionSteps.getControl().equalsIgnoreCase("V"))
								|| (executionSteps.getControl().contains("D"))) {
							ExtendedReportBo reportBo = new ExtendedReportBo();

							try {
								reportBo.setBrowser((map.get(executionSteps.getLogsId()).getBrowserType() != null)
										? map.get(executionSteps.getLogsId()).getBrowserType()
												: null);
							}
							catch(Exception e) {
								e.printStackTrace();
							}

							reportBo.setBugsummary(map.get(executionSteps.getLogsId()).getTbugsummary());
							reportBo.setDescription(map.get(executionSteps.getLogsId()).getTdescription());
							reportBo.setExpectedresult(map.get(executionSteps.getLogsId()).getTexpectedResult());
							reportBo.setSummary(map.get(executionSteps.getLogsId()).getTsummary());
							reportBo.setSeverity(map.get(executionSteps.getLogsId()).getTseverity());
							// reportBo.setActualresult(map.get(executionSteps.getLogsId()).getTexpectedResult());
							reportBo.setPriority(map.get(executionSteps.getLogsId()).getTpriority());
							reportBo.setStatus(map.get(executionSteps.getLogsId()).getTstatus());
							//System.out.println("map.get(executionSteps.getLogsId()).getTstatus().."+map.get(executionSteps.getLogsId()).getTstatus());

							ProjectDeatilsMeta data = projectrepo
									.findByprojectName(map.get(executionSteps.getLogsId()).getProjectName());
							count++;
							String id = data.getIsJiraEnable();
							if (!(StringUtils.isBlank(id))) {
								BugTrackorToolEntity bugtool = bugRepo.findByToolid(Integer.valueOf(id));
								if (bugtool != null) {
									reportBo.setBugtool(
											(bugtool.getBugTrackingToolName() != null) ? bugtool.getBugTrackingToolName()
													: null);
								}
							}
							reportBo.setProjectname(data.getProjectName());
							reportBo.setSteps(stepsBo);
							//System.out.println("executionSteps.getLogsId().."+executionSteps.getLogsId());
							reportBo.setVerify(map.get(executionSteps.getLogsId()));
							reportList.add(reportBo);
							stepsBo = new ArrayList<ExecutionStepsBo>();
						}
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
				for (ExtendedReportBo report : reportList) { //gives 25
					try {

						List<ExtendedReportBo> values = reportList.stream()
								.filter(x -> x.getVerify().getTestCaseId().equals(report.getVerify().getTestCaseId()))
								.collect(Collectors.toList());
						testcaseData.put(report.getVerify().getTestCaseId(), values);
						List<ExtendedReportBo> valuesScenario = reportList.stream()
								.filter(x -> x.getVerify().getScenarioId().equals(report.getVerify().getScenarioId()))
								.collect(Collectors.toList());
						scenarioData.put(report.getVerify().getScenarioId(), valuesScenario);
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}

				m=new HashMap<String, Integer>();
				m.put("scenarios", scenarioData.size());
				Integer testcases=0;
				Integer testcasesP=0;
				HashMap<Integer, Integer> dummy=new HashMap<>();
				Integer steps=0;
				int dumm=0;
				for (List<ExtendedReportBo>  val : scenarioData.values()) {
					testcases += val.size();
					
					for(ExtendedReportBo x: val) {
						steps+= x.getSteps().size();
						if(x.getVerify().getStatus().equalsIgnoreCase("pass"))
							testcasesP++;
						else
							dummy.put(dumm, 1);
					}
					dumm++;
				}	
				Integer scenarioF=dummy.size();

				m.put("testCases", testcases);
				m.put("steps", steps);
				m.put("testcasesP", testcasesP);
				m.put("scenarioF", scenarioF);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return m;
	}

	public ServiceResponse getExtentReportScreenshot(Integer screenshotId) {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			if (screenshotId != null) {
				AutomationScreenshot screenshot = automationScreenshotRepository.findByScreenshotId(screenshotId);
				serviceResponse.setServiceResponse(screenshot);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				serviceResponse.setServiceError("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	public ServiceResponse getEnvironmentAnalysis(String runId) {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			if (runId != null) {
				Object environment = automatioLogsDao.getEnvironmentByRunid(Integer.parseInt(runId));
				serviceResponse.setServiceResponse(environment);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				serviceResponse.setServiceError("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	public List<String> getHeadersFromSheet(MultipartFile file, String sheetName) {
		XSSFWorkbook myWorkBook;
		List<String> headers = new ArrayList<String>();
		Workbook myWorkBookXSSF = null;
		Workbook myWorkBookHSSF = null;
		Sheet mySheet = null;
		String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());

		try {
			/*
			 * myWorkBook = new XSSFWorkbook(file.getInputStream()); XSSFSheet mySheet =
			 * myWorkBook.getSheetAt(0);
			 */
			if (fileExtension.equalsIgnoreCase("xlsx")) {
				myWorkBookXSSF = new XSSFWorkbook(file.getInputStream());
				mySheet = myWorkBookXSSF.getSheet(sheetName);
			} else if (fileExtension.equalsIgnoreCase("xls")) {
				POIFSFileSystem fs = new POIFSFileSystem(file.getInputStream());
				myWorkBookHSSF = new HSSFWorkbook(fs);
				mySheet = myWorkBookHSSF.getSheet(sheetName);
			}
			Row headerRow = mySheet.getRow(0);
			Iterator<Cell> cells = headerRow.cellIterator();
			while (cells.hasNext()) {
				Cell cell = (Cell) cells.next();
				RichTextString value = cell.getRichStringCellValue();
				if (value.getString() != null && !value.getString().equals(""))
					headers.add(value.getString());
				else
					break;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			//			System.err.println("---NO headers can be fetched from Main Controller as it is not of proper format---");
		}

		return headers;

	}

	public Boolean checkSheetHeader(List<String> headers, String[] preDefinedHeader) {
		Boolean isRightExcel = true;
		ArrayList<String> preDefinedHeaderList = new ArrayList<String>(Arrays.asList(preDefinedHeader));
		for (String header : preDefinedHeaderList) {
			if (!headers.contains(header)) {
				isRightExcel = false;
				break;
			}
		}
		return isRightExcel;

	}

	public ServiceResponse uploadControllerAuto(MultipartFile file, String projectName, int projectid) {

		ServiceResponse serviceResponse = new ServiceResponse();
		Boolean inUpdateLogs;
		List<String> mainControllerHeaderList = new ArrayList<String>();
		List<String> dataSheetHeaderList = new ArrayList<String>();
		List<String> dbHeaderList = new ArrayList<String>();
		String status = null;
		try {
			int result = 0;
			// String filename=file.getOriginalFilename();
			File fileAuto = new File(automationPath + File.separator + projectName);
			File[] listOfFiles = fileAuto.listFiles();
			if (!fileAuto.exists()) {
				fileAuto.mkdirs();
			}
			try {
				mainControllerHeaderList = getHeadersFromSheet(file, "MainController");
				Boolean matchMC = checkSheetHeader(mainControllerHeaderList, ConstantParameter.AutoMainController);
				if (true == matchMC) {
					try {
						dataSheetHeaderList = getHeadersFromSheet(file, "DataSheet");
						Boolean matchDS = checkSheetHeader(dataSheetHeaderList, ConstantParameter.AutoDataSheet);
						if (true == matchDS) {
							try {
								dbHeaderList = getHeadersFromSheet(file, "DB_Connection");
								Boolean matchDB = checkSheetHeader(dbHeaderList, ConstantParameter.AutoDBConnection);
								if (true == matchDB) {
									byte[] bytes = file.getBytes();
									Path path = Paths.get(fileAuto + File.separator + "Main_Controller.xlsx",
											new String[0]);
									Files.write(path, bytes, new OpenOption[0]);
									// add jars and drivers
									/*
									 * File dataSheets= new
									 * File(automationPath+File.separator+projectName+File.separator+"DataSheets");
									 * File[] listOfFiles1 = dataSheets.listFiles(); File dependency=new
									 * File(dependencyPath); if(dependencyFileCount==0) {
									 * if(dataSheets.list().length>0) { FileUtils.copyDirectory(dependency,
									 * fileAuto); dependencyFileCount++; } }
									 */
									// For Datasheet-----------------------------------------
									File dataSheets = new File(automationPath + File.separator + projectName
											+ File.separator + "DataSheets");
									File[] listOfFiles1 = dataSheets.listFiles();
									int first2 = 0;
									for (int i = 0; i < listOfFiles1.length; i++) {
										if (listOfFiles1[i].isFile()) {

											XSSFWorkbook myWorkBook1 = new XSSFWorkbook(automationPath + File.separator
													+ projectName + File.separator + "DataSheets" + File.separator
													+ listOfFiles1[i].getName());
											XSSFSheet mySheet1 = myWorkBook1.getSheetAt(1);
											//											System.out.println(listOfFiles1[i].getName());
											int rowshet = 0;
											for (Row row : mySheet1) {
												if (rowshet == 0) {
												} else {
													for (Cell c : row) {
														if (c.getCellType() != CellType.BLANK) {
															first2 = first2 + 1;
															break;
														}
													}
												}
												/* count=+1; */
												rowshet = +1;
											}

										}
									}
									// For Main controller----------------------------------------
									XSSFWorkbook myWorkBook = new XSSFWorkbook(
											fileAuto + File.separator + "Main_Controller.xlsx");
									XSSFSheet mySheet = myWorkBook.getSheetAt(0);
									int first = -1;

									for (Row row : mySheet) {
										first = first + 1;
									}

									AutomationMetadata metadata = new AutomationMetadata();
									metadata.setProjectid(projectid);
									metadata.setProjectname(projectName);
									metadata.setLocationofDatasheet(dataSheets.toString());
									metadata.setNoOfDatasheet(dataSheets.list().length);
									metadata.setNoOftestSuites(first);
									metadata.setNoOfTestcases(first2);
									result = automationRunDao.saveAutoSheetDetails(metadata);
									if (result == 1) {
										// serviceResponse.setServiceResponse("Controller uploaded successfully");
										status = "Controller uploaded successfully";
										serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
										serviceResponse.setServiceError("");
									} else {
										// serviceResponse.setServiceResponse("Controller uploaded successfully but
										// unable to insert DB");
										status = "Controller uploaded successfully but unable to insert DB";
										serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
										serviceResponse.setServiceError("");
									}
									inUpdateLogs = updateProjectLogsTable(projectid);
									/*
									 * if(inUpdateLogs==true) { status=status+" Sheet update time inserted."; } else
									 * { status=status+" Sheet update time not inserted."; }
									 */

								} else {
									status = "Invalid Main Controller!! Required headers are not present in sheet : 'DB_Connection'.";
								}
							} catch (Exception e) {
								e.printStackTrace();
								status = "Invalid Main Controller!! Sheet : 'DB_Connection' does not exist in excel file.";
							}
						} else {
							status = "Invalid Main Controller!! Required headers are not present in sheet : 'DataSheet'.";
						}
					} catch (Exception e) {
						e.printStackTrace();
						status = "Invalid Main Controller!! Sheet : 'DataSheet' does not exist in excel file.";
					}
				} else {
					status = "Invalid Main Controller!! Required headers are not present in sheet : 'MainController'.";
				}

			} catch (Exception e) {
				e.printStackTrace();
				status = "Invalid Main Controller!! Sheet : 'MainController' does not exist in excel file.";
			}

		} catch (Exception e) {
			e.printStackTrace();
			// serviceResponse.setServiceResponse("Could not upload file");
			status = "Something went wrong!!";
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			serviceResponse.setServiceError("");
		}
		serviceResponse.setServiceResponse(status);
		return serviceResponse;
	}

	public List<AutomationRunLogs> getRunLogsByRunID(String runId) {

		return automationRunDao.getRunLogsByRunId(runId);
	}

	public AutomationScreenshot getss(Integer id) {
		AutomationScreenshot screenshot=new AutomationScreenshot();
		try {

			screenshot = automationScreenshotRepository.getss(id);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return screenshot;
	}
	public List<String> getScenarioRunwise(String runID) {
		List<String> executionStepsBos=null;
		try {
			executionStepsBos = automatioLogsDao
					.getScenarioRunwise(Integer.parseInt(runID));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return executionStepsBos;
	}
	public List<String> getTotalScenariosTestCasesSteps(String runID) {
		List<String> executionStepsBos=null;
		try {
			executionStepsBos = automatioLogsDao
					.getScenarioRunwise(Integer.parseInt(runID));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return executionStepsBos;
	}
	public Map<String, Long> getTotalScenariosTestCasesStepsCountsLogwise(String runID) {
		try {
			return  automationRunDao.getScenarioTestCaseSteps(runID);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}