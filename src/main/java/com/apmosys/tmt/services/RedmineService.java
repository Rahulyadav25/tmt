package com.apmosys.tmt.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.apmosys.tmt.BO.RedmineDTO;
import com.apmosys.tmt.models.BugTrackorToolEntity;
import com.apmosys.tmt.models.ProjectDeatilsMeta;
import com.apmosys.tmt.repository.BugTrackerRepository;
import com.apmosys.tmt.repository.ProjectRepository;
import com.apmosys.tmt.utils.ServiceResponse;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

@Service
public class RedmineService {

//	@Value("${redmineUrl}")
//	private String redmineUrl;

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private BugTrackerRepository bugTrackerRepository;

	public ServiceResponse createIssueByProjectId(RedmineDTO redmineDTO) {

		ServiceResponse serviceResponse = new ServiceResponse();

		try {

			ProjectDeatilsMeta projectmeta = projectRepository.findByprojectID(redmineDTO.getTmtProjectID());
			BugTrackorToolEntity bugTool = bugTrackerRepository
					.findByToolid(Integer.parseInt(projectmeta.getIsJiraEnable()));

			String body = "{\n" + "  \"issue\": {\n" + "    \"project_id\":" + redmineDTO.getProjectId() + ",\n"
					+ "	\"tracker_id\":" + redmineDTO.getTrackerId() + ",\n" + "    \"subject\":\""
					+ redmineDTO.getSubject() + "\",\n" + "	\"description\":\"" + redmineDTO.getDescription() + "\",\n"
					+ "	\"status_id\" :" + redmineDTO.getStatusId() + ",\n" + "    \"priority_id\":"
					+ redmineDTO.getPriorityId() + "\n" + "  }\n" + "}";
//			 System.out.println(body);
			Unirest.get(bugTool.getIP() + "/projects.json");
			HttpResponse<JsonNode> response = Unirest.post(bugTool.getIP() + "/issues.json")
					.basicAuth(bugTool.getUserid(), bugTool.getPassword()).header("Content-Type", "application/json")
					.body(body).asJson();

//			System.out.println(response.getBody());

//			System.out.println(response.getStatus());
//			System.out.println(response.getStatusText());
			if (response.getStatus() == 201) {
				JSONObject jsonObject = response.getBody().getObject();
				serviceResponse.setServiceResponse(jsonObject.getJSONObject("issue").get("id"));
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
//				System.out.println(serviceResponse.getServiceResponse().toString());
			} else {
				serviceResponse.setServiceResponse("Failed to create new Issue in redmine");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
				serviceResponse.setServiceError(response.getBody().toString());

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	public ServiceResponse getAllRedmineProjects(RedmineDTO redmineDTO) {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {

			BugTrackorToolEntity bugTool = bugTrackerRepository.findByToolid(redmineDTO.getToolid());

			if (bugTool != null) {
				HttpResponse<JsonNode> response = Unirest.get(bugTool.getIP() + "/projects.json")
						.basicAuth(bugTool.getUserid(), bugTool.getPassword())
						.header("Content-Type", "application/json").asJson();

				// System.out.println(response.getBody());
				// System.out.println(response.getStatus());

				if (response.getStatus() == 200) {
					JSONObject jsonObject = response.getBody().getObject();

					// System.out.println(jsonObject.get("projects").toString());
					serviceResponse.setServiceResponse(jsonObject.get("projects").toString());
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				} else {
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					serviceResponse.setServiceError(response.getBody().toString());

				}
			} else {
				serviceResponse.setServiceResponse("Invalid toolid");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	public ServiceResponse getAllRedmineTrackers(RedmineDTO redmineDTO) {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {

			BugTrackorToolEntity bugTool = bugTrackerRepository.findByToolid(redmineDTO.getToolid());

			if (bugTool != null) {
				HttpResponse<JsonNode> response = Unirest.get(bugTool.getIP() + "/trackers.json")
						.basicAuth(bugTool.getUserid(), bugTool.getPassword())
						.header("Content-Type", "application/json").asJson();

				if (response.getStatus() == 200) {
					JSONObject jsonObject = response.getBody().getObject();

					serviceResponse.setServiceResponse(jsonObject.get("trackers").toString());
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				} else {
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					serviceResponse.setServiceError(response.getBody().toString());

				}
			} else {
				serviceResponse.setServiceResponse("Invalid toolid");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	public ServiceResponse getAllRedmineStatuses(RedmineDTO redmineDTO) {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {

			BugTrackorToolEntity bugTool = bugTrackerRepository.findByToolid(redmineDTO.getToolid());

			if (bugTool != null) {
				HttpResponse<JsonNode> response = Unirest.get(bugTool.getIP() + "/issue_statuses.json")
						.basicAuth(bugTool.getUserid(), bugTool.getPassword())
						.header("Content-Type", "application/json").asJson();

				if (response.getStatus() == 200) {
					JSONObject jsonObject = response.getBody().getObject();

					serviceResponse.setServiceResponse(jsonObject.get("issue_statuses").toString());
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
					System.out.println(serviceResponse);
				} else {
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					serviceResponse.setServiceError(response.getBody().toString());

				}
			} else {
				serviceResponse.setServiceResponse("Invalid toolid");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	public ServiceResponse getAllRedmineIssuePriorities(RedmineDTO redmineDTO) {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {

			BugTrackorToolEntity bugTool = bugTrackerRepository.findByToolid(redmineDTO.getToolid());

			if (bugTool != null) {
				HttpResponse<JsonNode> response = Unirest.get(bugTool.getIP() + "/enumerations/issue_priorities.json")
						.basicAuth(bugTool.getUserid(), bugTool.getPassword())
						.header("Content-Type", "application/json").asJson();

				if (response.getStatus() == 200) {
					JSONObject jsonObject = response.getBody().getObject();

					serviceResponse.setServiceResponse(jsonObject.get("issue_priorities").toString());
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				} else {
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					serviceResponse.setServiceError(response.getBody().toString());

				}
			} else {
				serviceResponse.setServiceResponse("Invalid toolid");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	public ServiceResponse getAllRedmineUsers(RedmineDTO redmineDTO) {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {

			BugTrackorToolEntity bugTool = bugTrackerRepository.findByToolid(redmineDTO.getToolid());

			if (bugTool != null) {
				HttpResponse<JsonNode> response = Unirest.get(bugTool.getIP() + "/users.json")
						.basicAuth(bugTool.getUserid(), bugTool.getPassword())
						.header("Content-Type", "application/json").asJson();

				if (response.getStatus() == 200) {
					JSONObject jsonObject = response.getBody().getObject();

					serviceResponse.setServiceResponse(jsonObject.get("users").toString());
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				} else {
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					serviceResponse.setServiceError(response.getBody().toString());

				}
			} else {
				serviceResponse.setServiceResponse("Invalid toolid");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	public ServiceResponse getRedmineProjectByIdentifier(RedmineDTO redmineDTO) {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			// System.out.println(redmineDTO);
			ProjectDeatilsMeta projectmeta = projectRepository.findByprojectID(redmineDTO.getTmtProjectID());
			if (projectmeta == null) {
				serviceResponse.setServiceResponse(null);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
				serviceResponse.setServiceError("Invalid tmt project ID");
				return serviceResponse;
			}
			BugTrackorToolEntity bugTool = bugTrackerRepository
					.findByToolid(Integer.parseInt(projectmeta.getIsJiraEnable()));

			// System.out.println(bugTool);

			String url = "/projects" + "/" + projectmeta.getProjectName() + ".json";

			// System.out.println(url);

			if (bugTool != null) {
				HttpResponse<JsonNode> response = Unirest.get(bugTool.getIP() + url)
						.basicAuth(bugTool.getUserid(), bugTool.getPassword())
						.header("Content-Type", "application/json").asJson();

//				 System.out.println(response.getBody());
//				 System.out.println(response.getStatus());

				if (response.getStatus() == 200) {
					JSONObject jsonObject = response.getBody().getObject();

					// System.out.println(jsonObject.get("projects").toString());
					serviceResponse.setServiceResponse(jsonObject.get("project").toString());
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				} else {
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					serviceResponse.setServiceError(response.getBody().toString());

				}
			} else {
				serviceResponse.setServiceResponse("Invalid toolid");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	public ServiceResponse getProjectIssues(RedmineDTO redmineDTO) {

		ServiceResponse serviceResponse = new ServiceResponse();

		try {
			ServiceResponse response = getRedmineProjectByIdentifier(redmineDTO);

			if (response.getServiceResponse() != null) {
				JSONObject jsonObject = new JSONObject(response.getServiceResponse().toString());

				ProjectDeatilsMeta projectmeta = projectRepository.findByprojectID(redmineDTO.getTmtProjectID());
				if (projectmeta != null) {
					BugTrackorToolEntity bugTool = bugTrackerRepository
							.findByToolid(Integer.parseInt(projectmeta.getIsJiraEnable()));

					if (bugTool != null) {
						String projectId = String.valueOf(jsonObject.getInt("id"));
						HttpResponse<JsonNode> redmineResponse = Unirest
								.get(bugTool.getIP() + "/issues.json?sort=id&project_id=" + projectId)
								.basicAuth(bugTool.getUserid(), bugTool.getPassword())
								.header("Content-Type", "application/json").asJson();

						JSONObject json = redmineResponse.getBody().getObject();
						serviceResponse.setServiceResponse(json.get("issues").toString());
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

					} else {
						serviceResponse.setServiceResponse(null);
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
						serviceResponse.setServiceError("Invalid bug tool id ");
						return serviceResponse;
					}

				} else {
					serviceResponse.setServiceResponse(null);
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					serviceResponse.setServiceError("Invalid tmt project ID");
					return serviceResponse;
				}
			} else {
				serviceResponse.setServiceResponse("Invalid project identifier OR tmt project ID");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	public ServiceResponse getProjectIssuesStatus(RedmineDTO redmineDTO) {
		ServiceResponse response = new ServiceResponse();
		try {
			ServiceResponse serviceResponse = new ServiceResponse();
			serviceResponse = getProjectIssues(redmineDTO);
			Map<Integer, String> redmineBugs = new HashMap<Integer, String>();
			List<RedmineDTO> list = new ArrayList<>();

			if (serviceResponse.getServiceResponse() != null) {
				JSONArray jsonArray = new JSONArray(serviceResponse.getServiceResponse().toString());

				jsonArray.forEach((json) -> {
					// System.out.println(json);
					JSONObject object = new JSONObject(json.toString());
					JSONObject status = new JSONObject(object.get("status").toString());

					System.out.println(object.get("id"));
					System.out.println(status.get("name"));
					RedmineDTO dto = new RedmineDTO();
					dto.setProjectId(object.getInt("id"));
					dto.setStatusName(status.getString("name"));
		//			redmineBugs.put(object.getInt("id"), status.getString("name"));
					list.add(dto);
				});
				System.out.println(redmineBugs);
			}

			response.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
			response.setServiceResponse(list);
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus(ServiceResponse.STATUS_FAIL);
			response.setServiceResponse("Something went wrong");
		}
		return response;
	}

}
