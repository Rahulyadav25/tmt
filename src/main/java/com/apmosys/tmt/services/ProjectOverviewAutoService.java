package com.apmosys.tmt.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apmosys.tmt.dao.ProjectDao;
import com.apmosys.tmt.dao.ProjectOverviewAutoDao;
import com.apmosys.tmt.models.ProjectDeatilsMeta;
import com.apmosys.tmt.models.ProjectDetailsAuto;
import com.apmosys.tmt.models.TestSuitesData;
import com.apmosys.tmt.repository.ProjectRepository;
import com.apmosys.tmt.repository.configurationRepository;
import com.apmosys.tmt.utils.ServiceResponse;

@Service
public class ProjectOverviewAutoService {
	@Autowired
	public ProjectOverviewAutoDao overvieAutoDao;
	@Autowired
	public configurationRepository congifrepo;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ProjectDao projectDao;

	public ServiceResponse getoverviewdetails(int projectid, int id, int user_role) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		//System.out.println("overview running");
		List<ProjectDetailsAuto> maindashboard  = new ArrayList<>();
		try {
			String who=congifrepo.userRole(user_role);
			//System.out.println(who);
				
				maindashboard = overvieAutoDao.getoverviewdetails(projectid);  
					
			if(maindashboard!=null)
			{
				response.setServiceStatus("200");
				response.setServiceResponse(Status);
				response.setServiceResponse(maindashboard);
			}
			else
			{
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}
	
	
	public ServiceResponse getSummaryDetails(int projectid, int id,int user_role) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		//System.out.println("overview running");
		List<TestSuitesData> maindashboard  = new ArrayList<>();
		try {
			String who=congifrepo.userRole(user_role);
			//System.out.println(who);
				
				maindashboard = overvieAutoDao.getSummaryDetails(projectid); 
					
			if(maindashboard!=null)
			{ 
				response.setServiceStatus("200");
				response.setServiceResponse(Status);
				response.setServiceResponse(maindashboard);
			}
			else
			{
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}
	public ServiceResponse getLogDetails(int projectid, int id, int user_role,String fromDate, String toDate) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		//System.out.println("overview running");
		List<TestSuitesData> maindashboard  = new ArrayList<>();
		try {
			String who=congifrepo.userRole(user_role);
			//System.out.println(who);
				
				maindashboard = overvieAutoDao.getLogDetails(projectid, fromDate, toDate); 
					
			if(maindashboard!=null)
			{ 
				response.setServiceStatus("200");
				response.setServiceResponse(Status);
				response.setServiceResponse(maindashboard);
			}
			else
			{
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}
	public ServiceResponse getallActiveRun(int projectid, int id, int user_role) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		//System.out.println("overview running");
		List<TestSuitesData> maindashboard  = new ArrayList<>();
		try {
			String who=congifrepo.userRole(user_role);
			//System.out.println(who);
				
				maindashboard = overvieAutoDao.getallActiveRun(projectid); 
					
			if(maindashboard!=null)
			{ 
				response.setServiceStatus("200");
				response.setServiceResponse(Status);
				response.setServiceResponse(maindashboard);
			}
			else
			{
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}
	public ServiceResponse runwiseAutodetails(String runlist, int projectid, int id,  int user_role){
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		//System.out.println("overview running");
		List<TestSuitesData> maindashboard  = new ArrayList<>();
		try {
			String who=congifrepo.userRole(user_role);
			//System.out.println(who);
				
				maindashboard = overvieAutoDao.runwiseAutodetails(projectid,runlist); 
					
			if(maindashboard!=null)
			{ 
				response.setServiceStatus("200");
				response.setServiceResponse(Status);
				response.setServiceResponse(maindashboard);
			}
			else
			{
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}
	public ServiceResponse getProjectSelf(int projectid) {
		ServiceResponse response = new ServiceResponse();
		String Status = "Some Things Went Wrong..We are Unable to Process Your Request";
		try {
			ProjectDeatilsMeta projectmeta = projectRepository.findByprojectID(projectid);
			ProjectDeatilsMeta projectdates=projectDao.findByMinutesDate(projectid);
			projectmeta.setDelayTimeProject(projectdates.getDelayTimeProject());
			
			if (projectmeta != null) {
				response.setServiceStatus("200");
				response.setServiceResponse(projectmeta);
			} else {
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}

		} catch (Exception e) {
			e.printStackTrace();
			Status = "sorry";
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}
}
