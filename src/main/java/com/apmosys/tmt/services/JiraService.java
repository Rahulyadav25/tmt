package com.apmosys.tmt.services;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.apmosys.tmt.BO.ExecutionReportAutomation;
import com.apmosys.tmt.dao.ProjectDao;
import com.apmosys.tmt.models.BugDetailsEntity;
import com.apmosys.tmt.models.BugTrackorToolEntity;
import com.apmosys.tmt.models.JiraBugs;
import com.apmosys.tmt.models.MannualTestCaseExecutionLogs;
import com.apmosys.tmt.models.ProjectDeatilsMeta;
import com.apmosys.tmt.models.RobosoftJiraAutomationModel;
import com.apmosys.tmt.models.RunWiseBusStatusForExport;
import com.apmosys.tmt.repository.BugDetailsRepository;
import com.apmosys.tmt.repository.BugTrackerRepository;
import com.apmosys.tmt.repository.MannualExecutionLogsRepo;
import com.apmosys.tmt.repository.MannualReRunRepository;
import com.apmosys.tmt.repository.ProjectRepository;
import com.apmosys.tmt.utils.ServiceResponse;
import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.IssueType;
import com.atlassian.jira.rest.client.api.domain.Project;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import io.atlassian.util.concurrent.Promise;
@Service
public class JiraService {

	@Autowired
	private RestTemplate restTemplate;

	public String jiraUser, jiraPassword, jiraURL, jiraIssueId;

	@Autowired
	private BugTrackerRepository bugTrackerRepository;

	@Autowired
	ProjectDao projectDao;

	@Autowired
	MannualReRunRepository mannualReRunRepository;

	@Autowired
	MannualExecutionLogsRepo mannualExecutionLogsRepo;

	@Autowired
	BugDetailsRepository bugDetailsRepository;
	
	@Autowired
	ProjectRepository prorep;
	
	
	@Value("${jiraType}")
	private String jiraType;

	private BugTrackorToolEntity getBugToolDetails(String toolName) {
		// System.out.println(bugTrackerRepository);
		BugTrackorToolEntity bugTrackorTool = bugTrackerRepository.findByBugTrackingToolName(toolName).get(0);
		return bugTrackorTool;
	}

//	public BugTrackorToolEntity getBugToolDetailsAuto(String toolIdString) {
//		// System.out.println(bugTrackerRepository);
//		int toolIdInteger=Integer.parseInt(toolIdString);  
//		BugTrackorToolEntity bugTrackorTool = bugTrackerRepository.findByToolid(toolIdInteger);
//		return bugTrackorTool;
//	}
	public ProjectDeatilsMeta getBugToolDetails(Integer pID) {
		ProjectDeatilsMeta projectDeatils = new ProjectDeatilsMeta();
		try {
			projectDeatils = projectDao.getProjectDetails(pID);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return projectDeatils;
	}
	//new added auto
	private ProjectDeatilsMeta getBugToolDetailsAuto1(String projectName) {
		ProjectDeatilsMeta projectDeatils = new ProjectDeatilsMeta();
		try {
			projectDeatils = projectDao.getProjectDetailsauto1(projectName);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return projectDeatils;
	}

	private JiraRestClient getJiraRestClient() {
		JiraRestClient restClient = null;
		try {
			BugTrackorToolEntity bugTrackorToolEntity = getBugToolDetails("Jira");
			restClient = new AsynchronousJiraRestClientFactory().createWithBasicHttpAuthentication(
					URI.create(bugTrackorToolEntity.getIP()), bugTrackorToolEntity.getUserid(),
					bugTrackorToolEntity.getPassword());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return restClient;

	}

	public List getJiraIssueType(String userId, String password, String jiraTool, String pName) {
		jiraUser = userId;
		jiraPassword = password;
		jiraURL = jiraTool;

		String issueType[] = new String[100];
		String priorityName[] = new String[10];
		String[] priorityId = new String[10];
		List name = new ArrayList();
		int i = 0;
		int j = 0;
		try { // for issues
			HttpResponse<JsonNode> response = null;
			if(jiraType.equalsIgnoreCase("atlassian"))
			{
			 response = Unirest.get(jiraURL+"/rest/api/3/issuetype")
					.basicAuth(userId, password)
					.header("Accept", "application/json")
					.asJson();
			}
			if(jiraType.equalsIgnoreCase("robosoft"))
			{
			 response = Unirest.get(jiraURL + "/rest/api/2/issuetype").basicAuth(userId, password)
					.header("Accept", "application/json").asJson();
			}

			System.out.println(response.getBody().isArray());
			System.out.println(response.getBody());

			JSONArray allIssuesJson = response.getBody().getArray();

			System.out.println(allIssuesJson.get(0));
			issueType=new String[allIssuesJson.length()];
			for (int i1 = 0; i1 < allIssuesJson.length(); i1++) {
				JSONObject o = allIssuesJson.getJSONObject(i1);
				System.out.println(o.get("name"));

				issueType[i1] = (String) o.get("name");
			}
			// for priorites
			// /*
			
			HttpResponse<JsonNode> response2 = null;
			if(jiraType.equalsIgnoreCase("atlassian"))
			{
			 response2 = Unirest.get(jiraURL+"/rest/api/3/priority")
					.basicAuth(userId, password)
					.header("Accept", "application/json")
					.asJson();
			}
			
			if(jiraType.equalsIgnoreCase("robosoft"))
			{
			 response2 = Unirest.get(jiraURL + "/rest/api/2/priority").basicAuth(userId, password)
					.header("Accept", "application/json").asJson();
			
			}
			System.out.println(response2.getBody());
			JSONArray allIssuesJson2 = response2.getBody().getArray();

			System.out.println(allIssuesJson2.get(0));

			for (int i1 = 0; i1 < allIssuesJson2.length(); i1++) {
				JSONObject o = allIssuesJson2.getJSONObject(i1);

				System.out.println(o.get("name"));
				System.out.println(o.get("id"));

				priorityName[i1] = (String) o.get("name");
				priorityId[i1] = (String) o.get("id");
			}
			// */
			String[][] jira = { priorityName, priorityId };
			List<String> distinctElements = Arrays.asList(issueType).stream().distinct().collect(Collectors.toList());
			name.add(distinctElements);
			name.add(jira);

			/*
			 * //old code not working : JiraRestClient restClient = null; restClient = new
			 * AsynchronousJiraRestClientFactory().
			 * createWithBasicHttpAuthentication(URI.create(jiraTool), userId, password);
			 * 
			 * //IssueRestClient issueClient = restClient .getIssueClient();
			 * 
			 * Promise<Project> project = restClient.getProjectClient().getProject(pName);
			 * for (IssueType type : project.get().getIssueTypes()) {
			 * issueType[i]=type.getName(); i++; }
			 * ////System.out.println("skdkjs"+project.get().getIssueTypes());
			 * 
			 * 
			 * Promise<Iterable<Priority>> p =
			 * restClient.getMetadataClient().getPriorities(); for (Priority pr : p.get()) {
			 * priorityName[j]=pr.getName(); priorityId[j]=pr.getId().toString(); j++;
			 * 
			 * } String [][] jira= {priorityName, priorityId}; name.add(issueType);
			 * name.add(jira);
			 * 
			 */
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return name;
	}

	public List getJiraEpics(String userId, String password, String jiraTool, String pName) {
		jiraUser = userId;
		jiraPassword = password;
		jiraURL = jiraTool;
		List<String> epics = new ArrayList();
		try { // for issues
			HttpResponse<JsonNode> response = Unirest
					.get(jiraURL + "/rest/api/2/search?jql=issuetype=Epic&project=" + pName).basicAuth(userId, password)
					.header("Accept", "application/json").asJson();

			System.out.println(response.getBody().isArray());
			System.out.println(response.getBody());

			JSONArray a = response.getBody().getObject().getJSONArray("issues");

			for (int x = 0; x < a.length(); x++) {
				epics.add((String) a.getJSONObject(x).get("key"));
			}
		} // try
		catch (Exception e) {
			e.printStackTrace();
		}
		return epics;
	}

	public String insert_In_Jira(MannualTestCaseExecutionLogs exLogs) throws URISyntaxException, IOException {
		// System.out.println("JiraService:: method: insert_In_Jira param val:"+exLogs);
		String id = null;
		JiraRestClient restClient = null;
		Long IssueTypeKey = null;
		String projectKey = exLogs.getProductName();
		// System.out.println("product name"+projectKey);
		// BugTrackorToolEntity bugTrackorTool=getBugToolDetails("Zira");
		try {

			ProjectDeatilsMeta bugTrackorTool = getBugToolDetails(exLogs.getProjectId());

		
			if(jiraType.equalsIgnoreCase("robosoft"))
			{
				
				String keyname = exLogs.getProductName().toString();
				String summary = exLogs.getBugSummary();
				String priortyid = exLogs.getJiraPriorityId().toString();
				String epicName = exLogs.getEpicName().toString();
				String issueName = exLogs.getComponentName();
				String stepsMultiline =	exLogs.getSteps().toString();
				String stepSingleLine=stepsMultiline.replaceAll("[\r\n]+", " ");
				System.out.println("stepSingleLine : " + stepSingleLine);
				
				String testDataMultiline =	exLogs.getTestData();
				String testDataSingleLine=testDataMultiline.replaceAll("[\r\n]+", " ");
				System.out.println("testDataSingleLine : " + testDataSingleLine);
				
				String testDescMultiline =	exLogs.getTestDescription();
				String testDescSingleLine=testDescMultiline.replaceAll("[\r\n]+", " ");
				System.out.println("testDescSingleLine : " + testDescSingleLine);
				
				String expectedResultMultiline =	exLogs.getExpectedResult();
				String expectedResultSingleLine=expectedResultMultiline.replaceAll("[\r\n]+", " ");
				System.out.println("expectedResultSingleLine : " + expectedResultSingleLine);
				
				String actualResultMultiline =	exLogs.getActualResult();
				String actualResultSingleLine=actualResultMultiline.replaceAll("[\r\n]+", " ");
				System.out.println("actualResultSingleLine : " + actualResultSingleLine);
				
				
				
				String testerCommentMultiline = exLogs.getTesterComment();
				String testerCommentSingleline=testerCommentMultiline.replaceAll("[\r\n]+", " ");
				System.out.println("testerCommentSingleline : " + testerCommentSingleline);
				
				
				
				
				
				String desc = "Scenario:"+" "+exLogs.getScenarios()+"Functionality:"+" "+exLogs.getFunctionality()+"Browser :"+" "+exLogs.getBrowser()+"Steps:"+" "+stepSingleLine+"Test Data :"+" "+testDataSingleLine+"Test Description :"+" "+testDescSingleLine+"Expected Result :"+" "+expectedResultSingleLine+"Actual Result:"+" "+actualResultSingleLine+"Tester Comment:"+""+testerCommentSingleline+" ";
				System.out.println("desc : " + desc);
				
				 	
				String body ="{\r\n" + 
						"    \"fields\": {\r\n" + 
						"        \"project\": {\r\n" + 
						"            \"key\": \""+keyname+"\"\r\n" + 
						"        },\r\n" + 
						"        \"summary\": \""+summary+"\",\r\n" + 
						"        \"description\": \"Scenario: "+exLogs.getScenarios()+" \\n Functionality: "+exLogs.getFunctionality()+" \\n Browser : "+exLogs.getBrowser()+" \\n Steps: "+stepSingleLine+"  \\n Test Data : "+testDataSingleLine+" \\n Test Description : "+testDescSingleLine+" \\n Expected Result : "+expectedResultSingleLine+" \\n Actual Result: "+actualResultSingleLine+" \\n Tester Comment: "+testerCommentSingleline+"\",\r\n" + 
						"       \r\n" + 
						"       \"issuetype\": {\r\n" + 
						"            \"name\": \""+issueName+"\"\r\n" + 
						"        },\r\n" + 
						"        \"priority\": {\r\n" + 
						"            \"id\": \""+priortyid+"\"\r\n" + 
						"        },\r\n" + 
						"        \"customfield_10703\": \"Sub module is required.\",\r\n" + 
						"        \"customfield_10006\": \""+epicName+"\"\r\n" + 
						"    }\r\n" + 
						"}";
				System.out.println("body : " + body);
				
				

			HttpResponse<String> response = Unirest.post("https://jira.robosoftin.com/rest/api/2/issue")
					  .basicAuth(bugTrackorTool.getBugTool(), bugTrackorTool.getBugToolPassword())
					  .header("Content-Type", "application/json")
					  .body(body)
					  .asString();

			
			System.out.println(response.getBody());

			
			String resBody = response.getBody();
			System.out.println("resBody : " + resBody);
			JSONObject jsonObject = new JSONObject(resBody);

			System.out.println("key : " + jsonObject.getString("key"));
			
			 id = jsonObject.getString("key");

			 System.out.println("id : " + id);
			}
			
			
			if(jiraType.equalsIgnoreCase("atlassian"))
			{
				JsonNodeFactory jnf = JsonNodeFactory.instance;
				ObjectNode payload = jnf.objectNode();
				{

					ObjectNode update = payload.putObject("update");
					{
					}

					ObjectNode fields = payload.putObject("fields");
					{
						if (exLogs.getEpicName() != null || !exLogs.getBugType().equalsIgnoreCase("epic")) {

							ObjectNode parent = fields.putObject("parent");
							{
								parent.put("key", exLogs.getEpicName().toString());
							}
						} // if
						if (exLogs.getBugType().equalsIgnoreCase("bug")) {
							ObjectNode priority = fields.putObject("priority");
							{
								priority.put("id", exLogs.getJiraPriorityId().toString());
								// priority.put("id", exLogs.getBugPriority());//make sure to convert to string
							}
						}
						ObjectNode project = fields.putObject("project");
						{
							project.put("key", exLogs.getProductName());
							// parent.put("key", "Proj1");
						}

						fields.put("summary", exLogs.getBugSummary());

						ObjectNode issuetype = fields.putObject("issuetype");
						{
							if (exLogs.getComponentName().equalsIgnoreCase("bug"))
								issuetype.put("name", "Bug");
							// issuetype.put("id", "10003");
							if (exLogs.getComponentName().equalsIgnoreCase("subtask"))
								issuetype.put("name", "Subtask");
							// issuetype.put("id", "10005");
							if (exLogs.getComponentName().equalsIgnoreCase("task"))
								issuetype.put("name", "Task");
							// issuetype.put("id", "10002");
							if (exLogs.getComponentName().equalsIgnoreCase("story"))
								issuetype.put("name", "Story");
							// issuetype.put("id", "10001");
							if (exLogs.getComponentName().equalsIgnoreCase("epic"))
								issuetype.put("name", "Epic");
							// issuetype.put("id", "10000");
						}
						// /*
						ObjectNode description = fields.putObject("description");
						{
							description.put("type", "doc");
							description.put("version", 1);
							ArrayNode content = description.putArray("content");
							ObjectNode content0 = content.addObject();
							{
								content0.put("type", "paragraph");
								content = content0.putArray("content");
								content0 = content.addObject();
								{
									String desc = "Scenario: " + exLogs.getScenarios() + "\n" + "Functionality: "
											+ exLogs.getFunctionality() + "\n" + "Browser : " + exLogs.getBrowser()
											+ " os version: " + exLogs.getOsVersion() + "\n" + "Steps:\n "
											+ exLogs.getSteps() + "\n" + "Test Data : " + exLogs.getTestData() + "\n"
											+ "Test Description : " + exLogs.getTestDescription() + "\n"
											+ "Expected Result : " + exLogs.getExpectedResult() + "\nActual Result: "
											+ exLogs.getActualResult() + "\nTester Comment: " + exLogs.getTesterComment();
									content0.put("text", desc);
									content0.put("type", "text");
								}
							}
						}

						// */
					}
				}

				// /*/ Connect Jackson ObjectMapper to Unirest
				Unirest.setObjectMapper(new ObjectMapper() {
					private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper = new com.fasterxml.jackson.databind.ObjectMapper();

					public <T> T readValue(String value, Class<T> valueType) {
						try {
							return jacksonObjectMapper.readValue(value, valueType);
						} catch (IOException e) {
							throw new RuntimeException(e);
						}
					}

					public String writeValue(Object value) {
						try {
							return jacksonObjectMapper.writeValueAsString(value);
						} catch (JsonProcessingException e) {
							throw new RuntimeException(e);
						}
					}
				});
				// */
				System.out.println("payload:"+payload);
				// This code sample uses the 'Unirest' library:
				// http://unirest.io/java.html
				// HttpResponse<com.mashape.unirest.http.JsonNode> response =
				// Unirest.post(bugTrackorTool.getBugToolURL()+"/rest/api/3/search?jql=project=ANI&maxResults=-1")
				HttpResponse<com.mashape.unirest.http.JsonNode> response = Unirest
						.post(bugTrackorTool.getBugToolURL() + "/rest/api/3/issue")
						.basicAuth(bugTrackorTool.getBugTool(), bugTrackorTool.getBugToolPassword())
						.header("Accept", "application/json").header("Content-Type", "application/json").body(payload)
						.asJson();

				System.out.println(response.getBody());

				JSONObject JsonIdAskey = response.getBody().getObject();
				// sample
				// {"self":"https://animeshsoni.atlassian.net/rest/api/3/issue/10016","id":"10016","key":"ANI-14"}

				System.out.println(JsonIdAskey.get("key"));
				id = (String) JsonIdAskey.get("key");
				
			}

		} catch (Exception e) {
			e.printStackTrace();
			// System.out.println(e);
		}

		finally {
			try {
				// restClient.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return id;
	}

	
	
	public String insert_In_Jira_Robo(RobosoftJiraAutomationModel robo,ProjectDeatilsMeta projectDetails) throws URISyntaxException, IOException {
		// System.out.println("JiraService:: method: insert_In_Jira param val:"+exLogs);
		String id = null;
		JiraRestClient restClient = null;
		Long IssueTypeKey = null;
		String resBody= null;
		
		 
		try {

			ProjectDeatilsMeta bugTrackorTool = getBugToolDetailsAuto1(robo.getKeyname());
		//	ProjectDeatilsMeta bugTrackorTool = getBugToolDetails(exLogs.getProjectId());
//			BugTrackorToolEntity bugTrackorTool = getBugToolDetailsAuto(projectDetails.getIsJiraEnable());

		
			if(jiraType.equalsIgnoreCase("robosoft"))
			{
				
				String runID = robo.getRunID();
				 String keyname= projectDetails.getProjectName();
				 String summary= robo.getSummary();
				 String description= robo.getDescription();
				 String issueName= robo.getIssueName();
				 String priortyid= robo.getPriortyid();
				 String subModule= robo.getSubModule();
				 String epicName= robo.getEpicName();
				 String status= robo.getStatus();
				
				 
				 
				 	
				String body ="{\r\n" + 
						"    \"fields\": {\r\n" + 
						"        \"project\": {\r\n" + 
						"            \"key\": \""+keyname+"\"\r\n" + 
						"        },\r\n" + 
						"        \"summary\": \""+summary+"\",\r\n" + 
						"        \"description\": \""+description+ "\",\r\n" + 
						"       \"issuetype\": {\r\n" + 
						"            \"name\": \""+issueName+"\"\r\n" + 
						"        },\r\n" + 
						"        \"priority\": {\r\n" + 
						"            \"id\": \""+priortyid+"\"\r\n" + 
						"        },\r\n" + 
						"        \"customfield_10703\": \"Sub module is required.\",\r\n" + 
						"        \"customfield_10006\": \""+epicName+"\"\r\n" + 
						"    }\r\n" + 
						"}";
				System.out.println("body : " + body);
				

//			HttpResponse<String> response = Unirest.post("https://jira.robosoftin.com/rest/api/2/issue")
//					  .basicAuth(bugTrackorTool.getUserid(), bugTrackorTool.getPassword())
//					  .header("Content-Type", "application/json")
//					  .body(body)
//					  .asString();
				
				HttpResponse<String> response = Unirest.post("https://jira.robosoftin.com/rest/api/2/issue")
						  .basicAuth(bugTrackorTool.getBugTool(), bugTrackorTool.getBugToolPassword())
						  .header("Content-Type", "application/json")
						  .body(body)
						  .asString();
			
			

			
			System.out.println(response.getBody());

			
			 resBody = response.getBody();
			System.out.println("resBody : " + resBody);
			JSONObject jsonObject = new JSONObject(resBody);

			System.out.println("key : " + jsonObject.getString("key"));
			
			 id = jsonObject.getString("key");

			 System.out.println("id : " + id);
			}
			
			if(jiraType.equalsIgnoreCase("atlassian"))
			{
				
				JsonNodeFactory jnf = JsonNodeFactory.instance;
				ObjectNode payload = jnf.objectNode();
				{

					ObjectNode update = payload.putObject("update");
					{
					}

					ObjectNode fields = payload.putObject("fields");
					{
						if (robo.getEpicName() != null ) {

							ObjectNode parent = fields.putObject("parent");
							{
								parent.put("key", robo.getEpicName().toString());
							}
						} // if
//						if (robo.getIssueName().equalsIgnoreCase("bug")) {
//							ObjectNode priority = fields.putObject("priority");
//							{
//								priority.put("id", robo.getPriortyid().toString());
//								// priority.put("id", exLogs.getBugPriority());//make sure to convert to string
//							}
//						}
						ObjectNode project = fields.putObject("project");
						{
							project.put("key", projectDetails.getProjectName());
							// parent.put("key", "Proj1");
						}
						
						

						fields.put("summary", robo.getSummary());

						ObjectNode issuetype = fields.putObject("issuetype");
						{
							if (robo.getIssueName().equalsIgnoreCase("bug"))
								issuetype.put("name", "Bug");
							// issuetype.put("id", "10003");
							if (robo.getIssueName().equalsIgnoreCase("subtask"))
								issuetype.put("name", "Subtask");
							// issuetype.put("id", "10005");
							if (robo.getIssueName().equalsIgnoreCase("task"))
								issuetype.put("name", "Task");
							// issuetype.put("id", "10002");
							if (robo.getIssueName().equalsIgnoreCase("story"))
								issuetype.put("name", "Story");
							// issuetype.put("id", "10001");
							if (robo.getIssueName().equalsIgnoreCase("epic"))
								issuetype.put("name", "Epic");
							// issuetype.put("id", "10000");
						}
						// /*
						ObjectNode description = fields.putObject("description");
						{
							description.put("type", "doc");
							description.put("version", 1);
							ArrayNode content = description.putArray("content");
							ObjectNode content0 = content.addObject();
							{
								content0.put("type", "paragraph");
								content = content0.putArray("content");
								content0 = content.addObject();
								{
									String desc = robo.getDescription();
									content0.put("text", desc);
									content0.put("type", "text");
								}
							}
						}

						// */
					}
				}

				// /*/ Connect Jackson ObjectMapper to Unirest
				Unirest.setObjectMapper(new ObjectMapper() {
					private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper = new com.fasterxml.jackson.databind.ObjectMapper();

					public <T> T readValue(String value, Class<T> valueType) {
						try {
							return jacksonObjectMapper.readValue(value, valueType);
						} catch (IOException e) {
							throw new RuntimeException(e);
						}
					}

					public String writeValue(Object value) {
						try {
							return jacksonObjectMapper.writeValueAsString(value);
						} catch (JsonProcessingException e) {
							throw new RuntimeException(e);
						}
					}
				});
				// */

				// This code sample uses the 'Unirest' library:
				// http://unirest.io/java.html
				// HttpResponse<com.mashape.unirest.http.JsonNode> response =
				// Unirest.post(bugTrackorTool.getBugToolURL()+"/rest/api/3/search?jql=project=ANI&maxResults=-1")
//				HttpResponse<com.mashape.unirest.http.JsonNode> response = Unirest
//						.post(bugTrackorTool.getBugToolURL() + "/rest/api/3/issue")
//						.basicAuth(bugTrackorTool.getUserid(), bugTrackorTool.getPassword())
//						.header("Accept", "application/json").header("Content-Type", "application/json").body(payload)
//						.asJson();
				
				HttpResponse<com.mashape.unirest.http.JsonNode> response = Unirest
						.post(bugTrackorTool.getBugToolURL() + "/rest/api/3/issue")
						.basicAuth(bugTrackorTool.getBugTool(), bugTrackorTool.getBugToolPassword())
						.header("Accept", "application/json").header("Content-Type", "application/json").body(payload)
						.asJson();

				resBody = response.getBody().toString();
				System.out.println(response.getBody());

				JSONObject JsonIdAskey = response.getBody().getObject();
				// sample
				// {"self":"https://animeshsoni.atlassian.net/rest/api/3/issue/10016","id":"10016","key":"ANI-14"}

				System.out.println(JsonIdAskey.get("key"));
				id = (String) JsonIdAskey.get("key");
				
			}
		

		} catch (Exception e) {
			e.printStackTrace();
			// System.out.println(e);
		}

		finally {
			try {
				// restClient.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(id==null)
		{
		return resBody;
	}
		else
		{
			return id;
		}
	}
	public ProjectDeatilsMeta projectCheck(RobosoftJiraAutomationModel robo){	
		
		String projectName = robo.getKeyname();
		ProjectDeatilsMeta projectDetails = prorep.findByprojectName(projectName);
		
			return projectDetails;
		
	}
	
	/*
	 * //---------------------------------------------------------------------------
	 * --start for gettting //for bugs (getAllBugs)
	 * 
	 * public ResponseEntity<Object> getCall(String url, Integer pID,
	 * ProjectDeatilsMeta pm) { ResponseEntity<Object> response = null;
	 * ProjectDeatilsMeta bugTrackorTool=new ProjectDeatilsMeta(); if(pm!=null) {
	 * bugTrackorTool = pm; } else { bugTrackorTool = getBugToolDetails(pID); } if
	 * (bugTrackorTool != null) { url = bugTrackorTool.getBugToolURL() + url;
	 * HttpHeaders headers = new HttpHeaders(); headers.set("Authorization",
	 * bugTrackorTool.getBugToolApiToken());
	 * headers.setContentType(MediaType.APPLICATION_JSON); HttpEntity<String> entity
	 * = new HttpEntity<String>(headers); UriComponentsBuilder uriBuilder =
	 * UriComponentsBuilder.fromHttpUrl(url); response =
	 * restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, entity,
	 * Object.class); } return response; }
	 * 
	 * public ServiceResponse getProjectBugs(String projectName, Integer pID){
	 * ServiceResponse sr=new ServiceResponse(); ProjectDeatilsMeta
	 * bugTrackorTool=null; //sample:
	 * http://localhost:8088/rest/api/2/search?jql=project=PROJ String url =
	 * "/rest/api/2/search?jql=project="+projectName; ResponseEntity<Object>
	 * response = getCall(url, pID, bugTrackorTool); if (response != null &&
	 * response.getStatusCode().toString().equals("200")) { try { Gson gson = new
	 * Gson(); String json = gson.toJson(response.getBody()); JSONParser parser =
	 * new JSONParser(); Object obj = parser.parse(json); org.json.simple.JSONObject
	 * topObject = (org.json.simple.JSONObject) obj;
	 * //System.out.println(topObject.toString()); //
	 * pId=Integer.parseInt(topObject.get("id").toString()); //pId =
	 * topObject.get("id").toString(); sr.setServiceResponse(topObject); } catch
	 * (Exception e) { e.printStackTrace(); sr.setServiceError("failed"); } } return
	 * sr; }
	 * //---------------------------------------------------------------------------
	 * --end for gettting
	 */
	private String createIssue(IssueRestClient issueClient, String projectKey, String issuseSummary, long IssueTypeKey,
			long priorityId) {
		// System.out.println("JiraService:: method: createIssue(IssueRestClient
		// "+issueClient +", String"+ projectKey +","
		// + " String "+ issuseSummary+", long "+IssueTypeKey+" , long
		// "+priorityId+")");
		String id = "error";
		try {

			IssueInput newIssue = new IssueInputBuilder(projectKey, IssueTypeKey, issuseSummary)
					.setPriorityId(priorityId).build();
			/* .setPriority(priority) */
			return issueClient.createIssue(newIssue).claim().getKey();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	public String addAttachmentToIssue(MultipartFile[] file, ProjectDeatilsMeta bugtool,
			MannualTestCaseExecutionLogs bugId) {
		try {
			/*
			 * JiraRestClient restClient = null; final java.net.URI AttachmentUri = new
			 * java.net.URI(bugtool.getIP()+"/rest/api/2/issue/"+bugId.getBugId()+
			 * "/attachments");
			 * //System.out.println(bugtool.getIP()+"  "+bugtool.getUserid()+"  "+bugtool.
			 * getPassword()); restClient = new AsynchronousJiraRestClientFactory()
			 * .createWithBasicHttpAuthentication(URI.create(bugtool.getIP()),
			 * bugtool.getUserid() , bugtool.getPassword()); for(int i=0;i<file.length;i++)
			 * { String attachmentName = file[i].getOriginalFilename(); InputStream
			 * inputStream = new ByteArrayInputStream(file[i].getBytes());
			 * //System.out.println("file converted from multipart....");
			 * restClient.getIssueClient().addAttachment(AttachmentUri, inputStream,
			 * attachmentName);
			 * //System.out.println("Attachment saved in jira with issue id:" +
			 * bugId.getBugId()); }
			 */

			String uri = bugtool.getBugToolURL() + "/rest/api/2/issue/" + bugId.getBugId() + "/attachments";
			for (int i = 0; i < file.length; i++) {
				// InputStream inputStream = new ByteArrayInputStream(file[i].getBytes());
				// //System.out.println("file converted from multipart....");
				// restClient.getIssueClient().addAttachment(AttachmentUri, inputStream,
				// attachmentName);
				// System.out.println("Attachment saved in jira with issue id:" +
				// bugId.getBugId());
				File tempFile = File.createTempFile("_attachment_", file[i].getOriginalFilename()); // _jira_23534645_TMT
				file[i].transferTo(tempFile);
				tempFile.deleteOnExit();
				System.out.println(tempFile.getAbsolutePath());
				HttpResponse response = Unirest.post(uri).basicAuth(bugtool.getBugTool(), bugtool.getBugToolPassword())
						.header("Accept", "application/json").header("X-Atlassian-Token", "no-check")
						.field("file", tempFile).asJson();
				System.out.println(response.getBody());
				tempFile.delete();
			} // for

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(file[0].getOriginalFilename());
		}
		return "";
	}
	public ServiceResponse addAttachmentToIssueAuto(MultipartFile[] file, ProjectDeatilsMeta bugtool,
			String bugId) {
		ServiceResponse serviceResponse=new ServiceResponse();
		try {
			

			String uri = bugtool.getBugToolURL() + "/rest/api/2/issue/" + bugId + "/attachments";
			for (int i = 0; i < file.length; i++) {
				// InputStream inputStream = new ByteArrayInputStream(file[i].getBytes());
				// //System.out.println("file converted from multipart....");
				// restClient.getIssueClient().addAttachment(AttachmentUri, inputStream,
				// attachmentName);
				// System.out.println("Attachment saved in jira with issue id:" +
				// bugId.getBugId());
				File tempFile = File.createTempFile("_attachment_", file[i].getOriginalFilename()); // _jira_23534645_TMT
				file[i].transferTo(tempFile);
				tempFile.deleteOnExit();
				System.out.println(tempFile.getAbsolutePath());
				HttpResponse response = Unirest.post(uri).basicAuth(bugtool.getBugTool(), bugtool.getBugToolPassword())
						.header("Accept", "application/json").header("X-Atlassian-Token", "no-check")
						.field("file", tempFile).asJson();
				System.out.println(response.getBody());
				tempFile.delete();
				
				if(response.getBody()!=null ||response.getBody()!="")
				{
					serviceResponse.setServiceResponse("Attachment Uploaded Successfully");
					serviceResponse.setServiceStatus("Sucess");
				}
				

			} // for

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(file[0].getOriginalFilename());
		}
		return serviceResponse;
	}

	public String updateIssue1(MannualTestCaseExecutionLogs exLogs, String projectName) {
		// System.out.println("JiraService:: method:
		// updateIssue(MannualTestCaseExecutionLogs exLogs, String projectName)");
		JiraRestClient restClient = null;
		Long IssueTypeKey = null;
		try {
			restClient = getJiraRestClient();
			Promise<Project> project = restClient.getProjectClient().getProject(projectName);

			// System.out.println(project.get().getIssueTypes());
			for (IssueType type : project.get().getIssueTypes()) {
				if (type.getName().equalsIgnoreCase("Bug")) {
					IssueTypeKey = type.getId();
				}

			}
			// System.out.println("project name..."+projectName+"......issue
			// key....."+IssueTypeKey+".......
			// description......."+exLogs.getTestDescription());
			IssueInput updateIssue = new IssueInputBuilder(projectName, IssueTypeKey, exLogs.getTestDescription())
					.build();
			restClient.getIssueClient().updateIssue(exLogs.getBugId(), updateIssue);
			// System.out.println("in try..........");
			return exLogs.getBugId();

		} catch (Exception e) {
			e.printStackTrace();
			// System.out.println("in catch.........");
			return "";
		}

	}

	public String insert_In_JiraAuto(ExecutionReportAutomation exLogs, BugTrackorToolEntity bugtool)
			throws URISyntaxException, IOException {
		String id = null;
		JiraRestClient restClient = null;
		Long IssueTypeKey = null;
		String projectKey = exLogs.getProjectName();
		try {
			restClient = new AsynchronousJiraRestClientFactory().createWithBasicHttpAuthentication(
					URI.create(bugtool.getIP()), bugtool.getUserid(), bugtool.getPassword());
			IssueRestClient issueClient = restClient.getIssueClient();
			projectKey = exLogs.getProjectName();
			Promise<Project> project = restClient.getProjectClient().getProject(exLogs.getProjectName());

			// System.out.println(project.get().getIssueTypes());
			for (IssueType type : project.get().getIssueTypes()) {
				if (type.getName().equalsIgnoreCase("Bug")) {
					IssueTypeKey = type.getId();
				}

			}
			id = createIssue(issueClient, projectKey, exLogs.getTdescription(), IssueTypeKey, 1);
			jiraIssueId = id;
		} catch (Exception e) {
			e.printStackTrace();
			// System.out.println(e);
		}

		finally {
			try {
				restClient.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return id;
	}

	public Object getProjectBugs(String projectName, Integer projectID) {

		ServiceResponse sr = new ServiceResponse();
		List<JiraBugs> listOfBugs = null;
		try {

			ProjectDeatilsMeta bugTrackorTool = getBugToolDetails(projectID);

			/*
			 * bugs without epics
			 */
			HttpResponse<JsonNode> response = Unirest
					.get(bugTrackorTool.getBugToolURL() + "/rest/api/2/search?jql=project="
							+ bugTrackorTool.getProjectName() + "%20AND%20issuetype=bug&maxResults=2000")
					.basicAuth(bugTrackorTool.getBugTool(), bugTrackorTool.getBugToolPassword())
					.header("Accept", "application/json").asJson();

			// JsonNode j = response.getBody();
			// JSONObject o = response.getBody().getObject();

			JSONArray arr = response.getBody().getObject().getJSONArray("issues");
	//		System.out.println("arr**"+arr);
			listOfBugs = new ArrayList<JiraBugs>();

			JiraBugs bugs = null;

			for (int x = 0; x < arr.length(); x++) {

				try {
					bugs = new JiraBugs();
					JSONObject op = arr.getJSONObject(x);
					JSONObject field = op.getJSONObject("fields");
					JSONObject issuetype = field.getJSONObject("issuetype");
					JSONObject priority = field.getJSONObject("priority");
					JSONObject status = field.getJSONObject("status");

					bugs.setBugId((String) op.get("key"));
					try {

						bugs.setDesc((String) field.get("description"));
					} catch (Exception e) {
					}

					bugs.setCreated((String) field.get("created"));
					try {

						bugs.setSummary((String) field.get("summary"));
					} catch (Exception e) {
						// TODO: handle exception
					}

					bugs.setBugType((String) issuetype.get("name"));
					bugs.setPriority((String) priority.get("name"));
					bugs.setStatus((String) status.get("name"));
					// if(bugs.getBugType().equalsIgnoreCase("bug"))
					listOfBugs.add(bugs);

				} catch (Exception e) {
					// e.printStackTrace();
					System.out.println(e.getMessage());
				}
			}
			/*
			 * bugs with all epics
			 */
//			HttpResponse<JsonNode> response1 = Unirest
//					.get("https://appreciate.atlassian.net/rest/api/3/search?jql=project="
//							+ bugTrackorTool.getProjectName() + "%20AND%20issuetype=Epic&maxResults=200")
//					.basicAuth("animesh.soni@apmosys.in", "xiLT7nPDrtGUCl3luVtYA324")
//					.header("Accept", "application/json").asJson();

			HttpResponse<JsonNode> response1 = Unirest
					.get(bugTrackorTool.getBugToolURL() + "/rest/api/2/search?jql=project="
							+ bugTrackorTool.getProjectName() + "%20AND%20issuetype=Epic&maxResults=200")
					.basicAuth(bugTrackorTool.getBugTool(), bugTrackorTool.getBugToolPassword())
					.header("Accept", "application/json").asJson();
			// JsonNode j = response.getBody();
			// JSONObject o = response.getBody().getObject();
			JSONArray issues1 = response1.getBody().getObject().getJSONArray("issues");

			ArrayList<String> epics = new ArrayList<String>();

			for (int x = 0; x < issues1.length(); x++) {
				JSONObject op = issues1.getJSONObject(x);
				Object key = op.get("key");
				epics.add((String) key);

				// JSONObject field= op.getJSONObject("fields");
				// JSONObject issuetype = field.getJSONObject("issuetype");
				//
				// JSONObject priority = field.getJSONObject("priority");
				// JSONObject status = field.getJSONObject("status");
				// String created =(String)field.get("created");
				//
				// String issuetypename = (String)issuetype.get("name");
				// System.out.println(issuetypename);
				// try {
				//
				// String summary =(String)field.get("summary");
				// String description =(String)field.get("description");
				// }catch (Exception e) {
				// System.out.println(e.getMessage());
				// }
				//
				// String priorityname = (String)priority.get("name");
				// String statusname = (String)status.get("name");

				// JSONObject wl= field.getJSONObject("worklog");
				// JSONArray qe= wl.getJSONArray("worklogs");
				// try {
				//
				// JSONObject o111=((JSONObject)qe.get(0));
				// System.out.println(o111.get("issueId"));
				// }catch (Exception e) {
				// e.getMessage();
				// }
			} // for

			System.out.println("all epics.."+epics);

			for (String bug : epics) {
//				HttpResponse<JsonNode> response2 = Unirest
//						.get("https://appreciate.atlassian.net/rest/agile/1.0/epic/" + bug + "/issue" + "?jql=project="
//								+ bugTrackorTool.getProjectName() + "%20AND%20issuetype=bug&maxResults=2000")
//						.basicAuth("animesh.soni@apmosys.in", "xiLT7nPDrtGUCl3luVtYA324")
//						.header("Accept", "application/json").asJson();
				HttpResponse<String> response2 = null;
				if(jiraType.equalsIgnoreCase("atlassian"))
				{
				
				 response2 = Unirest.get(bugTrackorTool.getBugToolURL() + "/rest/api/2/search?jql=Parent="+bug+"%20AND%20issuetype=bug&maxResults=2000")
                          .basicAuth(bugTrackorTool.getBugTool(), bugTrackorTool.getBugToolPassword())
						  .asString();
				}
				if(jiraType.equalsIgnoreCase("robosoft"))
				{
				 response2 = Unirest.get(bugTrackorTool.getBugToolURL() + "rest/api/2/search?&jql='Epic+Link'="+bug+"%20AND%20issuetype=bug&maxResults=2000")
					      //.header("Authorization", "Basic YXBtb3N5cy5saXBpbmEueEByb2Jvc29mdGluLmNvbTpBQDIwc3lzTGlw")
						 .basicAuth(bugTrackorTool.getBugTool(), bugTrackorTool.getBugToolPassword()) 
					      .asString();
					
				}
				String resb = response2.getBody().toString();
				
				JSONObject jsonObject = new JSONObject(resb);
				JSONArray epicIssues = jsonObject.getJSONArray("issues");				
				for (int x = 0; x < epicIssues.length(); x++) {
					bugs = new JiraBugs();
					JSONObject op1 = epicIssues.getJSONObject(x);
					JSONObject field = op1.getJSONObject("fields");
					JSONObject issuetype = field.getJSONObject("issuetype");
					JSONObject priority = field.getJSONObject("priority");
					JSONObject status = field.getJSONObject("status");
					bugs.setBugId((String) op1.get("key"));
					try {

						bugs.setDesc((String) field.get("description"));
					} catch (Exception e) {
					}

					bugs.setCreated((String) field.get("created"));
					try {

						bugs.setSummary((String) field.get("summary"));
					} catch (Exception e) {
						// TODO: handle exception
					}

					bugs.setBugType((String) issuetype.get("name"));
					bugs.setPriority((String) priority.get("name"));
					bugs.setStatus((String) status.get("name"));

					// if(bugs.getBugType().equalsIgnoreCase("bug"))
					listOfBugs.add(bugs);
				} // for
			} // outer for
				// System.out.println(bugs1);
				// System.out.println(bugs1.size());

			/*
			 * // HttpResponse<com.mashape.unirest.http.JsonNode> response =
			 * Unirest.get(bugTrackorTool.getBugToolURL()+"/rest/agile/1.0/epic/none/issue")
			 * HttpResponse<com.mashape.unirest.http.JsonNode> response =
			 * Unirest.get(bugTrackorTool.getBugToolURL()+"/rest/api/3/search?jql=project="+
			 * bugTrackorTool.getProjectName()+"&maxResults=-1")
			 * .basicAuth(bugTrackorTool.getBugTool(), bugTrackorTool.getBugToolPassword())
			 * .header("Accept", "application/json") .asJson(); //
			 * System.out.println(response.getBody().getObject().getJSONObject("issues"));
			 * // System.out.println(response.getBody().getObject().getString("issues")); //
			 * System.out.println(response.getBody().getObject().get("issues"));
			 * 
			 * // sr.setServiceResponse(response.getBody().getObject().get("issues")) ; //
			 * JsonArray arr= response.getBody().getObject().get("issues");
			 * 
			 * JSONArray a= response.getBody().getObject().getJSONArray("issues"); Gson gson
			 * = new Gson(); String json = gson.toJson(a); // System.out.print(json);
			 * JSONParser parser = new JSONParser(); Object obj; obj = parser.parse(json);
			 * org.json.simple.JSONObject topObject = (org.json.simple.JSONObject) obj;
			 * sr.setServiceResponse(topObject);
			 * 
			 */
			sr.setServiceResponse(listOfBugs);
		} catch (UnirestException e) {
			sr.setServiceError("failed");
			e.printStackTrace();
		}
		return sr;
	}

	public BugDetailsEntity getBugStatus(String pName, Integer pID, String bugID, ProjectDeatilsMeta pm) {

		ProjectDeatilsMeta bugTrackorTool = getBugToolDetails(pID);
		BugDetailsEntity be = new BugDetailsEntity();
		HttpResponse<JsonNode> response;
		try {
			response = Unirest.get(bugTrackorTool.getBugToolURL() + "/rest/agile/1.0/issue/" + bugID)
					.basicAuth(bugTrackorTool.getBugTool(), bugTrackorTool.getBugToolPassword())
					.header("Accept", "application/json").asJson();

			JSONObject j = (JSONObject) response.getBody().getObject().get("fields");

			String priority = (String) ((JSONObject) (j.get("priority"))).get("name");
			String status = (String) ((JSONObject) (j.get("status"))).get("name");

			System.out.println(priority);
			System.out.println(status);
			be.setBugPriority(priority);
			be.setBugStatus(status);
		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return be;
	}// getBugStatus

	public BugDetailsEntity bugReopen(Integer pID, String bugID, String comment, String RorC) {

		ProjectDeatilsMeta bugTrackorTool = getBugToolDetails(pID);
		BugDetailsEntity be = new BugDetailsEntity();
		HttpResponse<JsonNode> response;
		try {

			// get the statis transistion id by name of status.. sence it could be different
			// for different accounts
			// and only can be set through id
			response = Unirest
					.get(bugTrackorTool.getBugToolURL() + "/rest/api/2/issue/" + bugID
							+ "/transitions?expand=transitions.fields")
					.basicAuth(bugTrackorTool.getBugTool(), bugTrackorTool.getBugToolPassword())
					.header("Accept", "application/json").asJson();

			JSONArray a = response.getBody().getObject().getJSONArray("transitions");
			System.out.println("Transition fields:"+a);
			String reopenId = null;
			String closeId = null;
			if (RorC.equals("R")) {
				for (int x = 0; x < a.length(); x++) {
					JSONObject v = (JSONObject) (a.get(x));
					String name = (String) v.get("name");
					 if(name.equalsIgnoreCase("Re-Open")||name.equalsIgnoreCase("To Do"))
					//if (name.equalsIgnoreCase("REOPENED"))
						reopenId = (String) v.get("id");
					 
					System.out.println(v.get("id") + "" + v.get("name"));
				} // for
			} else if (RorC.equals("C")) {

				for (int x = 0; x < a.length(); x++) {
					JSONObject v = (JSONObject) (a.get(x));
					String name = (String) v.get("name");
					if (name.equalsIgnoreCase("Close") || name.equalsIgnoreCase("Done"))
						closeId = (String) v.get("id");
					System.out.println(v.get("id") + "" + v.get("name"));
				} // for

			} // else if
			String ID="";
			if (reopenId != null)
			{
				ID=	reopenId.toString();
			}
			else if (closeId != null)
			{
				
					ID=	closeId.toString();
				
			}
			String playload2 = "{\r\n" + 
					"  \"update\": {\r\n" + 
					"    \"comment\": [\r\n" + 
					"      {\r\n" + 
					"        \"add\": {\r\n" + 
					"          \"body\": \""+comment+"\"\r\n" + 
					"        }\r\n" + 
					"      }\r\n" + 
					"    ]\r\n" + 
					"  },\r\n" + 
					"  \r\n" + 
					"  \"transition\": {\r\n" + 
					"    \"id\": \""+ID+"\"\r\n" + 
					"  }\r\n" + 
					"}";

			// Changing Status
			JsonNodeFactory jnf1 = JsonNodeFactory.instance;
			ObjectNode payload1 = jnf1.objectNode();
			{
				ObjectNode transition = payload1.putObject("transition");
				{
					// transition.put("name", "In Progress");
					if (reopenId != null)
						transition.put("id", reopenId.toString());
					else if (closeId != null)
						transition.put("id", closeId.toString());
				}
			}

			// Connect Jackson ObjectMapper to Unirest
			Unirest.setObjectMapper(new ObjectMapper() {
				private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper = new com.fasterxml.jackson.databind.ObjectMapper();

				public <T> T readValue(String value, Class<T> valueType) {
					try {
						return jacksonObjectMapper.readValue(value, valueType);
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				}

				public String writeValue(Object value) {
					try {
						return jacksonObjectMapper.writeValueAsString(value);
					} catch (JsonProcessingException e) {
						throw new RuntimeException(e);
					}
				}
			});
			System.out.println("payload1"+payload1);
			System.out.println("payload2"+playload2);

			HttpResponse<JsonNode> response1 = Unirest
					.post(bugTrackorTool.getBugToolURL() + "/rest/api/2/issue/" + bugID + "/transitions")
					.basicAuth(bugTrackorTool.getBugTool(), bugTrackorTool.getBugToolPassword())
					.header("Accept", "application/json").header("Content-Type", "application/json").body(playload2)
					.asJson();

			System.out.println("status of status changed in jira" + response1.getStatus());
////Pranik comment last api start
//			// Adding Comment
//			JsonNodeFactory jnf2 = JsonNodeFactory.instance;
//			ObjectNode payload2 = jnf2.objectNode();
//			{
//				ObjectNode body = payload2.putObject("body");
//				{
//					body.put("type", "doc");
//					body.put("version", 1);
//					ArrayNode content = body.putArray("content");
//					ObjectNode content0 = content.addObject();
//					{
//						content0.put("type", "paragraph");
//						content = content0.putArray("content");
//						content0 = content.addObject();
//						{
//							content0.put("text", comment);
//							content0.put("type", "text");
//						}
//					}
//				}
//			}
//
//			// Connect Jackson ObjectMapper to Unirest
//			Unirest.setObjectMapper(new ObjectMapper() {
//				private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
//
//				public <T> T readValue(String value, Class<T> valueType) {
//					try {
//						return jacksonObjectMapper.readValue(value, valueType);
//					} catch (IOException e) {
//						throw new RuntimeException(e);
//					}
//				}
//
//				public String writeValue(Object value) {
//					try {
//						return jacksonObjectMapper.writeValueAsString(value);
//					} catch (JsonProcessingException e) {
//						throw new RuntimeException(e);
//					}
//				}
//			});
//
//			
//			System.out.println("payload2"+payload2);
//
//			// This code sample uses the 'Unirest' library:
//			HttpResponse<JsonNode> response2 = Unirest
//					.post(bugTrackorTool.getBugToolURL() + "/rest/api/3/issue/" + bugID + "/comment")
//					.basicAuth(bugTrackorTool.getBugTool(), bugTrackorTool.getBugToolPassword())
//					.header("Accept", "application/json").header("Content-Type", "application/json").body(payload2)
//					.asJson();
//			System.out.println("status of comment added to  jira" + response2.getStatus());
//			//Pranik comment last api end

		} catch (UnirestException e) {
			e.printStackTrace();
		}
		return be;
	}// getBugStatus

	public List<RunWiseBusStatusForExport> getAllBugsWithstatus(String pID) {
		List<RunWiseBusStatusForExport> list = new ArrayList<>();
		RunWiseBusStatusForExport runAndBugsStatus = null;
		try {

			List<Object[]> l = mannualReRunRepository.getidAndRunName(Integer.parseInt(pID));

			for (Object[] o : l) {
				try {
					runAndBugsStatus = new RunWiseBusStatusForExport();
					runAndBugsStatus.setRunId((Integer) o[0]);
					runAndBugsStatus.setRunName((String) o[1]);

					List<String> bugs = mannualExecutionLogsRepo.getBugsByRunId((Integer) o[0]);

					for (String bugId : bugs) {
						try {

							List<String> status = bugDetailsRepository.getBugStatusByBugId(bugId);

							if (status.get(0).equalsIgnoreCase("closed")
									|| status.get(0).equalsIgnoreCase("Closed (2)"))
								runAndBugsStatus.setClosed(runAndBugsStatus.getClosed() + 1);
							if (status.get(0).equalsIgnoreCase("to do"))
								runAndBugsStatus.setToDo(runAndBugsStatus.getToDo() + 1);
							if (status.get(0).equalsIgnoreCase("in progress"))
								runAndBugsStatus.setInPrgress(runAndBugsStatus.getInPrgress() + 1);
							if (status.get(0).equalsIgnoreCase("on hold"))
								runAndBugsStatus.setOnHold(runAndBugsStatus.getOnHold() + 1);
							if (status.get(0).equalsIgnoreCase("reopened") || status.get(0).equalsIgnoreCase("re-open"))
								runAndBugsStatus.setReOpened(runAndBugsStatus.getReOpened() + 1);
							if (status.get(0).equalsIgnoreCase("done"))
								runAndBugsStatus.setDone(runAndBugsStatus.getDone() + 1);
							if (status.get(0).equalsIgnoreCase("duplicate"))
								runAndBugsStatus.setDuplicate(runAndBugsStatus.getDuplicate() + 1);
							if (status.get(0).equalsIgnoreCase("not an issue"))
								runAndBugsStatus.setNotAnIssue(runAndBugsStatus.getNotAnIssue() + 1);
						} // inner try
						catch (Exception e) {
							e.printStackTrace();
						}

					} // for(String bugId : bugs)

					list.add(runAndBugsStatus);
				} // outer try
				catch (Exception e) {
					e.printStackTrace();
				} // catch
			} // for(Object[] o : l)
		} // most outer try
		catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}// getAllBugsWithstatus
}// class
