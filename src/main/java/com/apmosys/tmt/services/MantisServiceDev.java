package com.apmosys.tmt.services;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.apmosys.tmt.dao.ProjectDao;
import com.apmosys.tmt.models.BugDetailsEntity;
import com.apmosys.tmt.models.MannualTestCaseExecutionLogs;
import com.apmosys.tmt.models.ProjectDeatilsMeta;
import com.apmosys.tmt.utils.ServiceResponse;
import com.google.gson.Gson;


@Service
public class MantisServiceDev {	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	ProjectDao projectDao;
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private BugZillaService bugzillaService;
	
	public ProjectDeatilsMeta getBugToolDetails(Integer pID) {
		ProjectDeatilsMeta projectDeatils=new ProjectDeatilsMeta();
		try {
			projectDeatils = projectDao.getProjectDetails(pID);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return projectDeatils;
	}

	public ResponseEntity<Object> postCall(String url, JSONObject requsetBody, Integer pID) {
		//System.out.println(requsetBody.toString());
		ResponseEntity<Object> response = null;
		ProjectDeatilsMeta bugTrackorTool = getBugToolDetails(pID);
		if (bugTrackorTool != null) {
			//url = "http://192.168.0.70" + url;
			url = bugTrackorTool.getBugToolURL() + url;
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", bugTrackorTool.getBugToolApiToken());
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(requsetBody.toString(), headers);
			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url);

			response = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.POST, entity, Object.class);

		}
		return response;
	}

	public ResponseEntity<Object> postCallMultipart(String url, JSONObject requsetBody, Integer pID) throws IOException {
		//System.out.println(requsetBody.toString());
		ResponseEntity<Object> response = null;
		ProjectDeatilsMeta bugTrackorTool = getBugToolDetails(pID);
		if (bugTrackorTool != null) {
			url = bugTrackorTool.getBugToolURL() + url;
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.IMAGE_JPEG);
			HttpEntity<String> entity = new HttpEntity<String>(requsetBody.toString(), headers);
			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url);

			//System.out.println(uriBuilder.toUriString());
			//System.out.println(requsetBody.toString());

			response = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.POST, entity, Object.class);
			//response=(ResponseEntity<Object>) restTemplate.postForObject(url, requestEntity, Object.class);
			//System.out.println("attachment for mantis........."+response);
		}
		return response;
	}

	public ResponseEntity<Object> patchCall(String url, JSONObject requsetBody, Integer pID) {
		//System.out.println(requsetBody.toString());
		ResponseEntity<Object> response = null;
		ProjectDeatilsMeta bugTrackorTool = getBugToolDetails(pID);
		if (bugTrackorTool != null) {
			url = bugTrackorTool.getBugToolURL() + url;
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", bugTrackorTool.getBugToolApiToken());
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(requsetBody.toString(), headers);
			//System.out.println(requsetBody.toString());
			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url);

			//System.out.println(uriBuilder.toUriString());
			//System.out.println(requsetBody.toString());
			
			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
			requestFactory.setConnectTimeout(5000);
			requestFactory.setReadTimeout(5000);
			restTemplate.setRequestFactory(requestFactory);
			
			response = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.PATCH, entity, Object.class);
			//System.out.println(response.getBody());
		}
		return response;
	}

	public ResponseEntity<Object> getCall(String url, Integer pID, ProjectDeatilsMeta pm) {
		ResponseEntity<Object> response = null;
		ProjectDeatilsMeta bugTrackorTool=new ProjectDeatilsMeta();
		if(pm!=null) {
			bugTrackorTool = pm;
		}
		else {
			bugTrackorTool = getBugToolDetails(pID);
		}
		if (bugTrackorTool != null) {
			url = bugTrackorTool.getBugToolURL() + url;
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", bugTrackorTool.getBugToolApiToken());
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(headers);
			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url);
			response = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, entity, Object.class);
		}
		return response;
	}
	
	public BugDetailsEntity getBugStatus(String pName,Integer pID, Integer bugID, ProjectDeatilsMeta pm) {
		BugDetailsEntity be=new BugDetailsEntity();
		String status="";
		String url = "/api/rest/issues/"+bugID;
		try {
			ResponseEntity<Object> response = getCall(url, pID, pm);
			if (response != null && response.getStatusCode().toString().equals("200")) {
				try {
					Gson gson = new Gson();
					String json = gson.toJson(response.getBody());
					JSONObject obj = new JSONObject(json);
					org.json.JSONArray issueArray = obj.getJSONArray("issues");
					JSONObject projectObj=issueArray.getJSONObject(0).getJSONObject("project");
					if(pName.equals(projectObj.getString("name"))) {
						String bugStatus=issueArray.getJSONObject(0).getJSONObject("status")
								.getString("name");
						String bugResolution=issueArray.getJSONObject(0).getJSONObject("resolution")
								.getString("name");
						if(("confirmed").equals(bugStatus) && ("reopened").equals(bugResolution)) {
							status="Reopen";
						}
						else if(("closed").equals(bugStatus) && ("fixed").equals(bugResolution)) {
							status="Closed";
						}
						else if(("resolved").equals(bugStatus) && ("fixed").equals(bugResolution)) {
							status="Resolved";
						}
						else if(("confirmed").equals(bugStatus) && ("open").equals(bugResolution)) {
							status="Open";
						}
						be.setBugStatus(status);
						
						String bugPriority=issueArray.getJSONObject(0).getJSONObject("priority").getString("name");
						if(bugPriority.equals("high") || bugPriority.equals("low")) {
							be.setBugPriority(StringUtils.capitalize(bugPriority));
						}
						
						String bugSeverity=issueArray.getJSONObject(0).getJSONObject("severity").getString("name");
						if(bugSeverity.equals("minor") || bugSeverity.equals("major")){
							be.setBugSeverity(StringUtils.capitalize(bugSeverity));
						}
						else if(bugSeverity.equals("block")) {
							be.setBugSeverity("Blocker");
						}
						else if(bugSeverity.equals("crash")) {
							be.setBugSeverity("Critical");
						}
					}
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return be;
	}
	
	public ServiceResponse getProjectBugs(String projectName, Integer pID){
		ServiceResponse sr=new ServiceResponse();
		ProjectDeatilsMeta bugTrackorTool=null;
		String url = "/api/rest/issues?project_name="+projectName;
		ResponseEntity<Object> response = getCall(url, pID, bugTrackorTool);
		if (response != null && response.getStatusCode().toString().equals("200")) {
			try {
				Gson gson = new Gson();
				String json = gson.toJson(response.getBody());
				JSONParser parser = new JSONParser();
				Object obj = parser.parse(json);
				org.json.simple.JSONObject topObject = (org.json.simple.JSONObject) obj;
				//System.out.println(topObject.toString());
				// pId=Integer.parseInt(topObject.get("id").toString());
				//pId = topObject.get("id").toString();
				sr.setServiceResponse(topObject);
			} catch (Exception e) {
				e.printStackTrace();
				sr.setServiceError("failed");
			}
		}
		return sr;
	}
	
	public String createIssue(MannualTestCaseExecutionLogs exLogs) {
		String pID = null;
		String url = "/api/rest/issues";
		
		JSONObject projectDet = new JSONObject();
		projectDet.put("name", exLogs.getProductName());
		
		JSONObject categoryDet = new JSONObject();
		categoryDet.put("name", "General");
		
		JSONObject statusDet = new JSONObject();
		statusDet.put("name", "confirmed");
		
		JSONObject priorityDet = new JSONObject();
		priorityDet.put("name", exLogs.getBugPriority().toLowerCase());
		
		JSONObject severityDet = new JSONObject();
		if(("Critical").equalsIgnoreCase(exLogs.getBugSeverity())) {
			severityDet.put("name", "crash");
		}
		else if(("Blocker").equalsIgnoreCase(exLogs.getBugSeverity())) {
			severityDet.put("name", "block");
		}
		else {
			severityDet.put("name", exLogs.getBugSeverity().toLowerCase());
		}
				
		JSONObject request = new JSONObject();
		request.put("summary", exLogs.getBugSummary());
		request.put("description", exLogs.getBugDescription());
		request.put("project", projectDet);
		request.put("status", statusDet);
		request.put("category", categoryDet);
		request.put("priority", priorityDet);
		request.put("severity", severityDet);
				
		ResponseEntity<Object> response = postCall(url, request, exLogs.getProjectId());
		//System.out.println(response.getStatusCode().toString());
		if (response != null && response.getStatusCode().toString().equals("201")) {
			try {
				Gson gson = new Gson();
				String json = gson.toJson(response.getBody());
				JSONObject obj = new JSONObject(json);
				Integer pid = obj.getJSONObject("issue").getInt("id");
				pID=pid.toString();
						
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return pID;
	}

	public String updateIssue(MannualTestCaseExecutionLogs exLogs) {
		String pId = null;
		String url = "/api/rest/issues/" + exLogs.getBugId();
		//System.out.println(url);
		if(StringUtils.isNotEmpty(exLogs.getBugStatus())) {
			
			JSONObject request = new JSONObject();
			
			JSONObject statusDet = new JSONObject();
			JSONObject resolutionDet = new JSONObject();
			JSONObject noteText = new JSONObject();
			
			JSONArray noteDet = new JSONArray();
			
			JSONObject priorityDet = new JSONObject();
			priorityDet.put("name", exLogs.getBugPriority().toLowerCase());
			
			JSONObject severityDet = new JSONObject();
			if(("Critical").equalsIgnoreCase(exLogs.getBugSeverity())) {
				severityDet.put("name", "crash");
			}
			else if(("Blocker").equalsIgnoreCase(exLogs.getBugSeverity())) {
				severityDet.put("name", "block");
			}
			else {
				severityDet.put("name", exLogs.getBugSeverity().toLowerCase());
			}
			
			if(exLogs.getBugStatus().equals("Reopen")) {
				statusDet.put("name", "confirmed");
				resolutionDet.put("name", "reopened");
			}
			else if(exLogs.getBugStatus().equals("Closed")) {
				statusDet.put("name", "closed");
				resolutionDet.put("name", "fixed");
			}
			
			noteText.put("text",exLogs.getBugDescription());
			noteDet.add(noteText);
			request.put("notes", noteDet);
			request.put("status", statusDet);
			request.put("resolution", resolutionDet);
			request.put("priority", priorityDet);
			request.put("severity", severityDet);
					

			ResponseEntity<Object> response = patchCall(url, request, exLogs.getProjectId());
			if (response != null && response.getStatusCode().toString().equals("200")) {
				try {
					Gson gson = new Gson();
					String json = gson.toJson(response.getBody());
					JSONObject obj = new JSONObject(json);
					Integer pid = obj.getJSONArray("issues").getInt(0);
					pId=pid.toString();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (response != null && response.getStatusCode().toString().equals("400")) {
				pId = "0";
			}
		}
		else {
			pId=exLogs.getBugId();
		}
		return pId;
	}
	
	public void addAttachmentToBugApi(MultipartFile[] file, MannualTestCaseExecutionLogs bugId) {
		String status="0";
		String url = "/api/rest/issues/"+bugId.getBugId()+"/files";
		try {
			
			JSONArray fileDet = new JSONArray();
			for(int j = 0; j < file.length; j++) {
				JSONObject fileContent = new JSONObject();
				File fileN=new File(projectService.saveAttachmentToFile(file[j]));
				//convert file image to base64 string		
				BufferedImage img = ImageIO.read(fileN);
				String imgstr = bugzillaService.encodeToString(img, "jpeg");
				
				fileContent.put("name", file[j].getOriginalFilename());
				fileContent.put("content",imgstr);
				fileDet.add(fileContent);
			}
			JSONObject request = new JSONObject();
			request.put("files", fileDet);
			ResponseEntity<Object> response = postCall(url, request, bugId.getProjectId());
			if (response != null && response.getStatusCode().toString().equals("201")) {
				
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
				
	}


	
	
}
