package com.apmosys.tmt.services;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apmosys.tmt.BO.MyProfileDataBO;
import com.apmosys.tmt.BO.ProjectAccessList;
import com.apmosys.tmt.dao.DashboardDao;
import com.apmosys.tmt.dao.MyProfileDAO;
import com.apmosys.tmt.dao.ProjectDao;
import com.apmosys.tmt.models.AccessDetail;
import com.apmosys.tmt.models.BugTrackorToolEntity;
import com.apmosys.tmt.models.DateWiseGraphData;
import com.apmosys.tmt.models.IntegratedBugToolEntity;
import com.apmosys.tmt.models.MannualTestCaseExecutionLogs;
import com.apmosys.tmt.models.ProjectAccessEntity;
import com.apmosys.tmt.models.ProjectDeatilsMeta;
import com.apmosys.tmt.models.ProjectEntitlement;
import com.apmosys.tmt.models.RoleEntity;
import com.apmosys.tmt.models.RunWiseGraphData;
import com.apmosys.tmt.models.UserDetails;
import com.apmosys.tmt.models.UserDetailsEntity;
import com.apmosys.tmt.models.userRole;
import com.apmosys.tmt.repository.AdminRepository;
import com.apmosys.tmt.repository.BugTrackerRepository;
import com.apmosys.tmt.repository.IntegratedBugToolRepository;
import com.apmosys.tmt.repository.MannualExecutionLogsRepo;
import com.apmosys.tmt.repository.ProjectRepository;
import com.apmosys.tmt.repository.UserDetailsRepository;
import com.apmosys.tmt.repository.confiRepository;
/*import com.apmosys.tmt.models.UserBo;*/
import com.apmosys.tmt.repository.configurationRepository;
/*import com.apmosys.tmt.repository.mainRepository;*/
import com.apmosys.tmt.utils.ServiceResponse;

import jakarta.servlet.http.HttpSession;

@Service
public class configurationService {

	@Autowired
	private HttpSession session;
	
	@Autowired
	public configurationRepository congifrepo;
	
	@Autowired
	public confiRepository confirepo;

	@Autowired
	public AdminRepository adminRepository;

	@Autowired
	public DashboardDao dashboarddao;

	@Autowired
	public ProjectDao projectDao;
	
	@Autowired
	public UserDetailsRepository userDetailsRepository;
	
	@Autowired
	public MannualExecutionLogsRepo mannualExecutionLogsRepo;
	
	@Autowired
	public ProjectRepository projectRepository;
	
	@Autowired
	public BugTrackerRepository bugTrackerRepo;
	
	@Autowired
	public IntegratedBugToolRepository integratedBugToolRepo;
	
	@Autowired
	public MyProfileDAO myProfileDAO;
	
	Logger log = LoggerFactory.getLogger(configurationService.class.getName());

	public ServiceResponse getAllUser(int id, int role) {
		List<userRole> userlist = null;
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			String usertype = congifrepo.userRole(role);
			if (usertype.equalsIgnoreCase("Admin")) {
				userlist = congifrepo.getAllUseradmin();
			} else {
				userlist = congifrepo.getUserDetailsbyMID(id);
			}
			if (userlist != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(userlist);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}

		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;

	}

	public ServiceResponse getUserDetailsbyid(String user) {
		log.info("------------getUserDetailsbyid--------" + user);

		ServiceResponse serviceResponse = new ServiceResponse();
		try {

			List<userRole> userlist = congifrepo.getUserbyID(Integer.valueOf(user));
			//System.out.println("result" + userlist);
			if (userlist != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(userlist);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}

		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;

	}

	public ServiceResponse createuser(userRole user) {
		ServiceResponse serviceResponse = new ServiceResponse();
		log.info("------------createuser--------" + user);

		List<UserDetailsEntity> roleId = congifrepo.findByCreator(user.getUserCreatedBy());
		String userroltype = user.getUserrole();
		Integer roleIds = roleId.get(0).getUser_role();
		Integer userroletpe = Integer.valueOf(userroltype);
		// UserDetails
		// usercheck=adminRepository.findByUserIDAndUsernameAndNameAndUser_role(user.getUserid(),user.getUsername(),user.getName(),user.getUsername());
		// List<UserDetailsEntity> usercheck=confirepo.findByUsername(user.getUserid());
		
		 String userid = user.getUserid();
	     Pattern pattern = Pattern.compile("^.*(?=.*[0-9])(?=.*[a-zA-Z])[a-zA-Z0-9]+$");
	     Matcher match = pattern.matcher(userid);
	    // boolean b = m.matches();
	     boolean validateuserid = match.find();
	     //System.out.println(validateuserid);
			     if ((validateuserid)==false){
			        serviceResponse.setServiceError("");
			serviceResponse.setServiceResponse("Invalid Userid");
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

	    }
	     
	     else {
	    	 List<UserDetailsEntity> usercheck = confirepo.findByUserIDOrUsernameOrNameOrIsActive(user.getUserid(),user.getUsername(),user.getName(),user.getIsactive());


		if (!(usercheck.isEmpty())) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse("User already Exist");
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
		} else {
			if (user.getName() != null && user.getUsername() != null && user.getUserrole() != null) {
				try {
					if (userroletpe == 100) {
						if (roleIds == 100) {

							String response = congifrepo.createUser(user);
							if (response.equals("Success")) {
								serviceResponse.setServiceResponse(response);
								serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

							} else {
								serviceResponse.setServiceError("");
								serviceResponse.setServiceResponse("Manager cannot create Admin");
								serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

							}
						} else {
							serviceResponse.setServiceError("");
							serviceResponse.setServiceResponse("Only Admin has the permission to insert Admin");
							serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
						}
					}

					else if (userroletpe == 101) {
						if (roleIds == 100 || roleIds == 101) {
							String response = congifrepo.createUser(user);
							if (response.equals("Success")) {
								serviceResponse.setServiceError("");
								serviceResponse.setServiceResponse(response);
								serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

							} else {
								serviceResponse.setServiceError("");
								serviceResponse.setServiceResponse("Unable to insert");
								serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

							}
						} else {
							serviceResponse.setServiceError("");
							serviceResponse
									.setServiceResponse("Only Admin/manager has the permission to insert Manager");
							serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

						}
					} else {
						if (roleIds == 100 || roleIds == 101) {
							String response = congifrepo.createUser(user);
							if (response.equals("Success")) {
								serviceResponse.setServiceError("");
								serviceResponse.setServiceResponse(response);
								serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

							} else {
								serviceResponse.setServiceError("");
								serviceResponse.setServiceResponse("Unable to insert");
								serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

							}
						} else {
							serviceResponse.setServiceError("");
							serviceResponse.setServiceResponse("Only Admin/manager has the permission to insert user");
							serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

						}

					}

				} catch (Exception e) {
					serviceResponse.setServiceError("Something Went Wrong");
					serviceResponse.setServiceResponse(e.getMessage());
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

				}

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Please fill all the details");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			}
		}
	     }
		return serviceResponse;
	}

	public String getUseridbyname(String creator) {
		String userid = congifrepo.getUserByMailid(creator);
		return userid;
	}

	public ServiceResponse updateUser(userRole user) {
		ServiceResponse serviceResponse = new ServiceResponse();
		log.info("------------updateUser--------");
		Serializable count = 0;
		UserDetailsEntity userEntity = new UserDetailsEntity();
		userEntity.setUserID(user.getUserid());
		userEntity.setIsActive(user.getIsactive());
		//System.out.println(user.getIsactive());
		userEntity.setUsername(user.getUsername());
		userEntity.setName(user.getName());
		//System.out.println("========================" + user.getUserrole());
		if (user.getUserrole().equals("Tester")) {
			userEntity.setUser_role(102);
		}
		if (user.getUserrole().equals("Manager")) {
			userEntity.setUser_role(101);
		}
		if (user.getUserrole().equals("Admin")) {
			userEntity.setUser_role(100);
		}
		//System.out.println("----updateUser role-" + userEntity.getUser_role());
		try {
			count = congifrepo.updateUser(userEntity, user.getUserid());
			if (count != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(count);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse deleteUser(int id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		String count = "";
		UserDetailsEntity roleEn = (UserDetailsEntity) session.getAttribute("user");
		Integer role = roleEn.getUser_role();
		UserDetails usersrole = adminRepository.findById(id).orElse(null);
		List<MannualTestCaseExecutionLogs> findId=mannualExecutionLogsRepo.findByTesterId(String.valueOf(id));
		//System.out.println();
	//System.out.println(findId.size()); 
		Integer s = Integer.valueOf(usersrole.getUser_role());
		try {
			if (s == 100) {
				if (role == 100) {
						if(findId.isEmpty())
						{
								count = congifrepo.DeleteUser(id);
								if (count != null) {
									serviceResponse.setServiceResponse(count);
									serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
			
								} else {
									serviceResponse.setServiceError("Has not permission to delete the User");
									serviceResponse.setServiceResponse(count);
									serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			
								}
						}
						else {
							
							serviceResponse.setServiceResponse(count);
							serviceResponse.setServiceResponse("This user can't be deleted !!");
							serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
						}

				} else {
					serviceResponse.setServiceError("");
					serviceResponse.setServiceError("Has not permission to delete the User");
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
				}
			}

			else {
				if (role == 100 || role == 101) {
					
					UserDetailsEntity mangerID=userDetailsRepository.findById(id).orElse(null);
					if(findId.isEmpty())
					{
							
							if(mangerID.getManagerID()==Integer.valueOf(roleEn.getId())  || roleEn.getUser_role() == 100)
								{
											count = congifrepo.DeleteUser(id);
											
											if (count != null) {
												serviceResponse.setServiceError("");
												serviceResponse.setServiceResponse(count);
												serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
						
											} else {
												serviceResponse.setServiceError("");
												serviceResponse.setServiceResponse(count);
												serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
						
											}
										 
								}
								else {
									serviceResponse.setServiceError("");
									serviceResponse.setServiceResponse("This user is updated by admin,you cann't delete this user");
									serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
								}
					}
				
				else {
					serviceResponse.setServiceError("");
					serviceResponse.setServiceResponse("This user can't be deleted !!");
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
				}
				}
				else {
					serviceResponse.setServiceError("");
					serviceResponse.setServiceResponse("Has not permission to delete the User");
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
				}
			}		
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse getAccessList(int userid, int roleid) {
		ServiceResponse serviceResponse = new ServiceResponse();

		String who = congifrepo.userRole(roleid);
		log.info("------------getAccessList--------" + who);
		List<AccessDetail> access = null;
		try {
			log.info("------------getAccessList--------" + userid);
			log.info("------------getAccessList--------" + who);
			if (who.equals("Admin")) {
				access = congifrepo.AdminDetailsAccess();

			} else {
				access = congifrepo.GetDetailsAccess(userid);
			}
			if (access != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(access);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse saveaccess(ProjectAccessList accessList) {
		UserDetailsEntity roleEn = (UserDetailsEntity) session.getAttribute("user");
		Integer role = roleEn.getUser_role();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String current=sdf.format(new Date());
		Serializable count = 0;
		String status = "success";
		String status1 = "success";
		List<ProjectAccessEntity> accesEntities = new ArrayList<ProjectAccessEntity>();
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			Date current1=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(current);
			if (role == 100 || role == 101) {
				if (!accessList.getManager().isEmpty()) {
					for (String pId : accessList.getManager()) {
						ProjectAccessEntity accessEntity = new ProjectAccessEntity();
						accessEntity.setProjectID(Integer.parseInt(pId));
						accessEntity.setUserID(Integer.parseInt(accessList.getUserid()));
						accessEntity.setAccesstype("Manager");
						accessEntity.setAccessGivenBy(roleEn.getName());
						accessEntity.setAccessDate(current);
						accesEntities.add(accessEntity);
					}
				}
				if (!accessList.getReadonly().isEmpty()) {
					for (String pId : accessList.getReadonly()) {
						ProjectAccessEntity accessEntity = new ProjectAccessEntity();
						accessEntity.setProjectID(Integer.parseInt(pId));
						accessEntity.setUserID(Integer.parseInt(accessList.getUserid()));
						accessEntity.setAccesstype("Readonly");
						accessEntity.setAccessGivenBy(roleEn.getName());
						accessEntity.setAccessDate(current);
						accesEntities.add(accessEntity);
					}
				}
				if (!accessList.getTester().isEmpty()) {
					for (String pId : accessList.getTester()) {
						ProjectAccessEntity accessEntity = new ProjectAccessEntity();
						accessEntity.setProjectID(Integer.parseInt(pId));
						accessEntity.setUserID(Integer.parseInt(accessList.getUserid()));
						accessEntity.setAccesstype("Tester");
						accessEntity.setAccessGivenBy(roleEn.getName());
						accessEntity.setAccessDate(current);
						accesEntities.add(accessEntity);
					}
				}
				status = congifrepo.giveProjectAccess(accesEntities, Integer.parseInt(accessList.getUserid()));
				if (status != "") {
					if (status.equals("success")) {
						serviceResponse.setServiceError("");
						serviceResponse.setServiceResponse("success");
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
					} else if (status.equals("Duplicate")) {
						//System.out.println("Already exist");
						serviceResponse.setServiceError("");
						serviceResponse.setServiceResponse("Already exist");
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
					} else {
						serviceResponse.setServiceError("");
						serviceResponse.setServiceResponse("Something Went Wrong");
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

					}
				}
			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Unable to give access");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			}
		}
		// }

		catch (Exception e) {
			e.printStackTrace();
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse getUserByProjectId(Integer pid) {
		List<userRole> userlist = null;
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			userlist = congifrepo.getUserByProjectId(pid);
			if (userlist != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(userlist);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}

		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;

	}

	public ServiceResponse saverole(String rolename) {
		Object count = 0;
		ServiceResponse serviceResponse = new ServiceResponse();
		log.info("-------saveaccess-----updateUser--------" + rolename);
		try {
			count = congifrepo.saveRole(rolename);
			if (count != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(count);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse getAllrole() {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			List<RoleEntity> access = congifrepo.getRoles();
			if (access != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(access);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse deleterole(int userid) {
		ServiceResponse serviceResponse = new ServiceResponse();
		log.info("------------updateUser--------");
		Serializable count = 0;
		try {
			count = congifrepo.deleterole(userid);
			if (count != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(count);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse removeAccess(String projectid, int userid) {
		ServiceResponse serviceResponse = new ServiceResponse();
		log.info("------------updateUser--------");
		Serializable count = 0;
		projectid = projectid.replaceAll(",", "','");
		try {
			count = congifrepo.removeAccess(projectid, userid);
			if (count != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(count);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse getRoletype(int role) {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			UserDetailsEntity roleEn = (UserDetailsEntity) session.getAttribute("user");
			String usertype = congifrepo.userRole(role);
			System.out.println("roleEn.getUser_role()....................." + roleEn.getUser_role());
			if (roleEn.getUser_role() == 100 || roleEn.getUser_role() == 101) {

				if (usertype != null) {
					serviceResponse.setServiceError("");
					serviceResponse.setServiceResponse(usertype);
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

				}
			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}

		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;

	}

	public ServiceResponse getAllmanager() {
		List<userRole> userlist = null;
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			userlist = congifrepo.getAllmanager();
			/*
			 * try { String usertype = congifrepo.userRole(role);
			 * if(usertype.equalsIgnoreCase("Admin")) { userlist =
			 * congifrepo.getAllUseradmin(); }
			 */

			if (userlist != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(userlist);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}

		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse EditUser(userRole user) {
		ServiceResponse serviceResponse = new ServiceResponse();
		UserDetailsEntity roleEn = (UserDetailsEntity) session.getAttribute("user");
		log.info("------------edituser--------" + user.getUserrole());
		try {
			if (user.getUserrole().equals("100")) {
				if (roleEn.getUser_role() == 100) {
					
									String response = congifrepo.EditUser(user);
									if (response.equals("Success")) {
										serviceResponse.setServiceError("");
										serviceResponse.setServiceResponse(response);
										serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				
									} else {
										serviceResponse.setServiceError("");
										serviceResponse.setServiceResponse("Unable to insert");
										serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
				
									}
							
				} else {
					serviceResponse.setServiceError("");
					serviceResponse.setServiceResponse("Only Admin can insert Admin type user");
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

				}
			}

			else if (user.getUserrole().equals("101")) {
				if (roleEn.getUser_role() == 100 || roleEn.getUser_role() == 101) {
					UserDetailsEntity mangerID=userDetailsRepository.findById(user.getId()).orElse(null);
					if(mangerID.getManagerID()==Integer.valueOf(user.getCreatorid())  || roleEn.getUser_role() == 100)
					{
					
								String response = congifrepo.EditUser(user);
								if (response.equals("Success")) {
									serviceResponse.setServiceError("");
									serviceResponse.setServiceResponse(response);
									serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
			
								} else {
									serviceResponse.setServiceError("");
									serviceResponse.setServiceResponse("Unable to modified");
									serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			
								}
								}
					else {
							serviceResponse.setServiceError("");
							serviceResponse.setServiceResponse("This user can not modified");
							serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
							}
						}

					else {
						serviceResponse.setServiceError("");
						serviceResponse.setServiceResponse("Admin or Manager can insert Manager type user");
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					}

			} else {
				if (roleEn.getUser_role() == 100 || roleEn.getUser_role() == 101) {
					UserDetailsEntity mangerID=userDetailsRepository.findById(user.getId()).orElse(null);
					//System.out.println(mangerID.getManagerID());
					if(mangerID.getManagerID()==Integer.valueOf(user.getCreatorid())  || roleEn.getUser_role() == 100)
					{
					
					String response = congifrepo.EditUser(user);
					if (response.equals("Success")) {
						serviceResponse.setServiceError("");
						serviceResponse.setServiceResponse(response);
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

					} else {
						serviceResponse.setServiceError("");
						serviceResponse.setServiceResponse("Unable to modified");
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

					}
					}
					else {
						serviceResponse.setServiceError("");
						serviceResponse.setServiceResponse("This user can not modified");
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					}
				} else {
					serviceResponse.setServiceError("");
					serviceResponse.setServiceResponse("Admin or Manager can only add user");
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

				}
			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;

	}

	public ServiceResponse getAccessListbyidonly(int userid) {
		ServiceResponse serviceResponse = new ServiceResponse();

		List<AccessDetail> access = null;
		try {
			access = congifrepo.GetDetailsAccess(userid);

			if (access != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(access);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse createbugTrackorSer(BugTrackorToolEntity dataTool) {
		ServiceResponse serviceResponse = new ServiceResponse();
		
		if (dataTool.getBugTrackingToolName() != null && dataTool.getIP() != null && dataTool.getIsactive() != null
				&& dataTool.getUserid() != null && dataTool.getPassword() != null && dataTool.getUserDefinedToolName() != null) {
			try {
				//System.out.println("11 integration ...."+dataTool.getBugTrackingToolName());
				//BugTrackorToolEntity checkForTool=bugTrackerRepo.findByBugTrackingToolName(dataTool.getBugTrackingToolName()).get(0);
				BugTrackorToolEntity checkForTool=bugTrackerRepo.findByBugTrackingToolNameAndUserDefinedToolName
						(dataTool.getBugTrackingToolName(), dataTool.getUserDefinedToolName());
				//System.out.println("12 integration ....");

				if(checkForTool==null)
				{
					String response = congifrepo.createbugTrackor(dataTool);
					if (response.equals("Success")) {
						serviceResponse.setServiceError("");
						serviceResponse.setServiceResponse(response);
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

					} else if (response.equals("already exist")) {
						serviceResponse.setServiceError("");
						serviceResponse.setServiceResponse("already exist");
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

					}
				}
				else {
					serviceResponse.setServiceError("");
					serviceResponse.setServiceResponse("already exist");
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
				}

			} catch (Exception e) {
				serviceResponse.setServiceError("Something Went Wrong");
				//System.out.println("11 chath integration ....");

				serviceResponse.setServiceResponse(e.getMessage());
				e.printStackTrace();
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} else {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse("Please Fill all fields");
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
		}
		return serviceResponse;

	}
	
	public ServiceResponse getIntegratedBugTool() {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			List<IntegratedBugToolEntity> list = integratedBugToolRepo.findAll();

			if (list != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(list);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}

		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	
	public ServiceResponse ShowBugTrackor() {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			List<BugTrackorToolEntity> list = congifrepo.bugTrackorAll();

			if (list != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(list);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}

		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse ShowBugTrackornyid(int id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {

			BugTrackorToolEntity list = congifrepo.bugTrackorbytoolid(id);

			if (list != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(list);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}

		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse EditbugTrackor(BugTrackorToolEntity bg) {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			String response = congifrepo.bugTrackoredit(bg);

			if (response.equals("Success")) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(response);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
			return serviceResponse;
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse showbugTrackorActive() {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			List<BugTrackorToolEntity> list = congifrepo.bugTrackorAllActive();

			if (list != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(list);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}

		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse deleteBugTool(int id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		String count = "success";
		try {
			count = congifrepo.removeBugTool(id);
			if (count != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(count);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse ProjectWiseGraph(int id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		String count = "success";
		List<DateWiseGraphData> datewisegraph = new ArrayList<DateWiseGraphData>();
		try {
			datewisegraph = congifrepo.ProjectWiseGraph(id);
			if (datewisegraph != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(datewisegraph);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse RunwiseGraph(int id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		String count = "success";
		List<RunWiseGraphData> datewisegraph = new ArrayList<RunWiseGraphData>();
		try {
			datewisegraph = congifrepo.RunwiseGraph(id);
			if (datewisegraph != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(datewisegraph);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse OverallGraph() {
		ServiceResponse serviceResponse = new ServiceResponse();
		String count = "success";
		List<DateWiseGraphData> datewisegraph = new ArrayList<DateWiseGraphData>();
		try {
			datewisegraph = congifrepo.OverallGraph();
			if (datewisegraph != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(datewisegraph);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse getUserDetailmax() {
		ServiceResponse serviceResponse = new ServiceResponse();
		String count = "0";
		try {
			count = congifrepo.getMaxid();
			if (count != null) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(count);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}

	public ServiceResponse getadminViewEntitlement(Integer userId, Integer userRole,String projectwiserole,String username) {
		String Status = "No Project";
		ServiceResponse response = new ServiceResponse();
		Map<String, ProjectEntitlement> maindashboard =new HashedMap<String, ProjectEntitlement>();
		try {
			String who = congifrepo.userRole(userRole);
			if (who.equals("Admin")) {
				maindashboard = congifrepo.getProjectDetailsForAdmin();
				
			}
			else if (who.equals("Manager")) {
				maindashboard = congifrepo.getProjectDetailsFormanager(String.valueOf(userId),username);
				
			}
			else 
			{
			
			}

			if (maindashboard != null) {
				response.setServiceStatus("200");
				response.setServiceResponse(maindashboard);
			} else {
				response.setServiceStatus("400");
				response.setServiceResponse("Something went wrong");
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setServiceStatus("400");
			response.setServiceResponse(Status);
		}
		return response;
	}

	public ServiceResponse deleteGiveAccessUserEntitlement(Integer projectid, Integer userid, String projectwiserole,Integer userrole) {

		ServiceResponse serviceResponse = new ServiceResponse();
		String count = "fail";
		ProjectEntitlement pe=new ProjectEntitlement();
		try {
			if(userrole ==100)
			{
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("This is an admin user ,access for this user can not be modified");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			}
			else {
					count = congifrepo.deleteGiveAccessUserEntitlement(projectid,userid,projectwiserole);
					if (count.equals("Success")) {
						UserDetails udf=new UserDetails();
						udf=adminRepository.findById(userid).orElse(null);
						pe.setUsername(udf.getName());
						serviceResponse.setServiceError("");
						serviceResponse.setServiceResponse(pe);
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
		
					} else {
						serviceResponse.setServiceError("");
						serviceResponse.setServiceResponse("Data Not Found");
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
		
					}
					}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	
	}

	

	public ServiceResponse giveAccessUserEntitlement(ProjectAccessEntity pae) {
		ServiceResponse serviceResponse = new ServiceResponse();
		ProjectEntitlement pe=new ProjectEntitlement();
		UserDetailsEntity roleEn = (UserDetailsEntity) session.getAttribute("user");
		String accessCreatedBy=roleEn.getName();
		ProjectAccessEntity count =null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String current=sdf.format(new Date());
		
		try {
			if(pae.getUserrole() ==100)
			{
					serviceResponse.setServiceError("Access cann't be modify for Admin user");
					serviceResponse.setServiceResponse("Access cann't be modify for Admin user");
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
				}
			
				else {
				Date current1=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(current);
				//System.out.println();
				//System.out.println("accessCreatedBy.."+accessCreatedBy+" current1"+current);
				pae.setAccessGivenBy(accessCreatedBy);
				pae.setAccessDate(current);
				count = congifrepo.giveAccessUserEntitlement(pae);
				if (count != null) {
					pe.setAccessType(count.getAccesstype());
					pe.setProjectId(count.getProjectID().toString());
					pe.setUserid(count.getUserID().toString());
					UserDetails udf=new UserDetails();
					udf=adminRepository.findById(Integer.valueOf(pe.getUserid())).orElse(null);
					
					pe.setUsername(udf.getName());
					serviceResponse.setServiceError("");
					serviceResponse.setServiceResponse(pe);
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
	
				} else {
				serviceResponse.setServiceError("Data Not Found");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
				}
				
			}
			
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;
	}
	
	public ServiceResponse changeProjectStatus(Integer projectID,String isActive,Integer user_role) {

		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			if(user_role ==100 || user_role==101)
			{
				ProjectDeatilsMeta details = new ProjectDeatilsMeta();
				details=projectRepository.findByProjectID(projectID);
				details.setIsActive(isActive);
				projectRepository.save(details);
				
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(true);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				
				
				
			}
			else {
			
				serviceResponse.setServiceError("Unauthorized user");
				serviceResponse.setServiceResponse("Only admin or Manager can change status of project");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;

	}
	public ServiceResponse getMyProjectsData(Integer userid, Integer userRoleID) {

		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			if(userid==null || userRoleID==null) {
				serviceResponse.setServiceError("User ID or User Role ID not found!!");
				serviceResponse.setServiceResponse("");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			}
			else {
				List<MyProfileDataBO> mpData=new ArrayList<MyProfileDataBO>();
				if(userRoleID ==100)
				{
					try {
						mpData=myProfileDAO.getMyProjectsDataAdmin(userid);
						serviceResponse.setServiceError("");
						serviceResponse.setServiceResponse(mpData);
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
					}
					catch(Exception e) {
						e.printStackTrace();
						serviceResponse.setServiceError("Something Went Wrong");
						serviceResponse.setServiceResponse(e.getMessage());
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					}
				}
				else {
					try {
						mpData=myProfileDAO.getMyProjectsDataNonAdmin(userid);
						serviceResponse.setServiceError("");
						serviceResponse.setServiceResponse(mpData);
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
					}
					catch(Exception e) {
						e.printStackTrace();
						serviceResponse.setServiceError("Something Went Wrong");
						serviceResponse.setServiceResponse(e.getMessage());
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					}
					
				}
			}
			
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;

	}
}
