package com.apmosys.tmt.services;
/*package com.apmosys.pheonix.services;

import java.io.Console;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apmosys.pheonix.Codec;
import com.apmosys.pheonix.models.User;
import com.apmosys.pheonix.repository.UserRepository;
import com.apmosys.pheonix.utils.DBconnections;
import com.apmosys.pheonix.utils.ServiceResponse;




@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CheckAuthService checkAuthService;
	
	@Autowired
	private Codec codec;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private HttpSession session; 
	
	
	
	
	@SuppressWarnings("unused")
	public ServiceResponse userLogin(User user, String remoteAddr) {
		ServiceResponse serviceResponse=new ServiceResponse();
		System.out.println("Get called to userlogin service***");
		try
		{
		String password=codec.decrypt(user.getPassword());
		User usr = userRepository.findUser(user.getEmail(),password);
		System.out.println(password + " "+user.getEmail());
		
		if(usr!=null)
		{
			//boolean isValid = checkAuthService.authenticate(user.getEmail(), password);
			boolean isValid=true;
			
				session = request.getSession();
				session.invalidate();
				session = request.getSession(true);
				session.setMaxInactiveInterval(15 * 60);
				session.setAttribute("user", usr);
				
				serviceResponse.setServiceResponse(usr);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				serviceResponse.setServiceError("");
				return serviceResponse;
		}
			else {
				serviceResponse.setServiceResponse("");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
				serviceResponse.setServiceError("Username or Password Invalid");
				return serviceResponse;
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return serviceResponse;
	}
		
	
	public ServiceResponse getAllUsers() {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {

			List<User> userlist = userRepository.findAll();
			
			
			
			if (!userlist.isEmpty()) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(userlist);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				
			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
				
			}
			
		

		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			
		}
		return serviceResponse;

	}

	
	
	
	
	
	
	@SuppressWarnings("unused")
	
	public ServiceResponse saveUser(User user) {

		ServiceResponse serviceResponse = new ServiceResponse();
		try {

			if (!userRepository.getUserByUserId(user.getUserId()).isEmpty()) {
				serviceResponse.setServiceResponse("User with UserId : " + user.getUserId() + " already exist");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
				serviceResponse.setServiceError("user already created");

			} else {
				
				user.setCreatedOn(new Date());
				userRepository.save(user);
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("User Successfully created");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			}
			
		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}

		return serviceResponse;
	}

	

	public ServiceResponse updateUser(String userId, String userName, String userRole) {

		ServiceResponse serviceResponse = new ServiceResponse();
		User user = new User();
		try {
			
			user=userRepository.findUser(userId);
			user.setUserId(userId);
			user.setName(userName);
			user.setRole(userRole);
			userRepository.save(user);
	
			serviceResponse.setServiceError("");
			serviceResponse.setServiceResponse("User Successfully updated");
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}

		return serviceResponse;
	}

	public ServiceResponse getAllUsers() {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {

			List<User> userlist = userRepository.findAll();
			if (!userlist.isEmpty()) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(userlist);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				
			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
				
			}

		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			
		}
		return serviceResponse;

	}

	public ServiceResponse deleteUser(User user) {
		
		ServiceResponse serviceResponse = new ServiceResponse();
		try {
			
			userRepository.delete(user);
			serviceResponse.setServiceError("");
			serviceResponse.setServiceResponse("User with " + user.getUserId() + " is successfully deleted");
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
			
		} catch (Exception e) {
			e.printStackTrace();
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
		}
		return serviceResponse;
	}

	public ServiceResponse getADUser(String userId) {
		
		ServiceResponse serviceResponse = new ServiceResponse();
		User user=new User();
		
		try(Connection conn = DBconnections.getConnectionUnideskFor118()){
			
			String newQuery = "select full_name,designation,department_name,phone_number,email from app_user where user_id=?";
			PreparedStatement preparedStatement = conn.prepareStatement(newQuery);
			preparedStatement.setString(1, userId);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			
			if (resultSet.next()) {
				
				user.setFullName(resultSet.getString(1));
				user.setDesignation(resultSet.getString(2));
				user.setDepartment(resultSet.getString(3));
				user.setPhoneNumber(resultSet.getString(4));
				user.setEmail(resultSet.getString(5));
				
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(user);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				
			}else{
				
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("User Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			}
			
			
		}
		catch (Exception e) {
			e.printStackTrace();
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			
		}
		
		return serviceResponse;
		
	}
	
	
	
	
}
*/