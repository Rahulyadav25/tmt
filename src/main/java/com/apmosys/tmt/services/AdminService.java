package com.apmosys.tmt.services;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apmosys.tmt.Codec;
import com.apmosys.tmt.models.Admin;
import com.apmosys.tmt.models.ProjectDeatilsMeta;
import com.apmosys.tmt.models.User;
import com.apmosys.tmt.models.UserDetails;
import com.apmosys.tmt.models.UserDetailsEntity;
import com.apmosys.tmt.models.UserDetailsEntityDTO;
import com.apmosys.tmt.models.UserFailedLogins;
import com.apmosys.tmt.models.UserSessionDetails;
import com.apmosys.tmt.models.userRole;
import com.apmosys.tmt.repository.AdminRepository;
import com.apmosys.tmt.repository.ProjectRepository;
import com.apmosys.tmt.repository.UserFailedLoginsRepo;
import com.apmosys.tmt.repository.UserRepository;
import com.apmosys.tmt.repository.UserSessionsRepo;
import com.apmosys.tmt.repository.configurationRepository;
import com.apmosys.tmt.utils.ServiceResponse;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
@Service
public class AdminService {

	@Autowired
	public AdminRepository adminRepository;

	@Autowired
	private CheckAuthService checkAuthService;

	@Autowired
	private Codec codec;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private HttpSession session; 

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserSessionsRepo userSessionRepo;

	@Autowired
	private configurationRepository configurationRepository;

	@Autowired
	private	ProjectRepository projectRepository;

	@Autowired
	private	UserFailedLoginsRepo userFailedLoginsRepo;

	private static final Random RANDOM = new SecureRandom();
	private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefg0123456789:;@#$&*%+=-[]{}><?hijklmnopqrstuvwxyz";

	public int passwordCheckCount=0;

	@SuppressWarnings("unused")
	public ServiceResponse userLogin(Admin admin, String remoteAddr) {
		ServiceResponse serviceResponse=new ServiceResponse();
		String email="";

		System.out.println("Get called to userlogin service*** " + admin.getPassword());
		try
		{
			String password=codec.decrypt(admin.getPassword());
			System.out.println("password==================" +password);
			email=admin.getMailId();
			if(email.contains(" "))
			
				{
			        return new ServiceResponse( "E-Mail cannot contain white spaces." );
//					 S1  =  new ServiceResponse( "Password cannot contain white spaces." );
			    }
			
			if( password.contains( " " ) )
		    {
		        return new ServiceResponse( "Password cannot contain white spaces." );
//				 S1  =  new ServiceResponse( "Password cannot contain white spaces." );
		    }

			UserDetailsEntity usr = adminRepository.findUserIgnoreCase(admin.getMailId(),password);
			UserFailedLogins ufl=null;
			//System.out.println("ues..."+usr);
			if(usr!=null) {
				ufl=userFailedLoginsRepo.findById(usr.getUserID()).orElse(null);
				UserSessionDetails usd = userSessionRepo.findById(usr.getUserID()).orElse(null);

				//for restrict on more than 5 attempts
				if(ufl==null || ufl.getFailedAttemptCount() < 5  ) {
//					for restrict multiple logins of 1 user
					if(usd == null || usd.getActive_status().equalsIgnoreCase("N") 
							|| (new java.util.Date().getTime() - usd.getLast_check().getTime()) > 15000){
						//set failed attempts to 0
						if(ufl==null) {
							UserFailedLogins userFailedLoginEntity=new UserFailedLogins();
							userFailedLoginEntity.setUserId(usr.getUserID());
							userFailedLoginEntity.setFailedAttemptCount(0);
							userFailedLoginsRepo.save(userFailedLoginEntity);
						}
						else 
						userFailedLoginsRepo.updateFailedLoginCount(ufl.getUserId(),0);
						
						
						String token = generatePassword(10);
						//to hard code autohentication token
					//	String token = "token";
						
						session = request.getSession();
						session.invalidate();
						session = request.getSession(true);
						session.setMaxInactiveInterval(30 * 60);
						session.setAttribute("user", usr);
						session.setAttribute("tokenID", token);
						usr.setTokenID(token);
						

						if(usd == null){
							//create new User session 
							usd= new UserSessionDetails();
							usd.setActive_status("Y");
							usd.setSession_key(session.getId());
							usd.setUserId(usr.getUserID());
							usd.setLast_check(new Date());
							userSessionRepo.save(usd);
							System.out.println("new session.. created");
							
						}
						else { 
							long lastCheckedGap=new java.util.Date().getTime();
							long x=usd.getLast_check().getTime();
							long y=new java.util.Date().getTime() - usd.getLast_check().getTime();
							//update new details 
							int count= userSessionRepo.updateSession(usr.getUserID(), session.getId(), "Y" , new Date());
						//	System.out.println("updated session.. count "+count); 
						}
						UserDetailsEntityDTO encryptedUser = codec.encryptUserDetails(usr);
						serviceResponse.setServiceResponse(encryptedUser);
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
						serviceResponse.setServiceError("");
					}//inner if(usd == null ...
					//comment for multiple session
					else {
						//redirect with message
						serviceResponse.setServiceResponse("");
						serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
						serviceResponse.setServiceError("Already logged in on another browser");
					}
				}//if(ufl==null || ufl.getFailedAttemptCount() < 5  )
				else {

					//redirect with message
					serviceResponse.setServiceResponse("");
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					serviceResponse.setServiceError("Account Lock");
				}
				usr.setPassword("**");
				return serviceResponse;
			}
			else {
				UserDetailsEntity usr2 = adminRepository.findUserIgnoreCase2(admin.getMailId(),password);
				if(usr2!=null) {
					usr2=null;
					//		System.out.println("inactive");
					serviceResponse.setServiceResponse("");
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					serviceResponse.setServiceError("Inactive user");
					System.out.println(serviceResponse);
					return serviceResponse;
				}
				else {
					//System.out.println("invalid");
					serviceResponse.setServiceResponse("");
					serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
					serviceResponse.setServiceError("Adminname or Password Invalid");
					System.out.println(serviceResponse);
					//					userFailedLoginsRepo.
					UserDetailsEntity usr3 = adminRepository.findUserByOnlyMail(admin.getMailId());
					if(usr3!=null ) {
//						ufl=userFailedLoginsRepo.findOne(usr3.getUserID());
						ufl=userFailedLoginsRepo.findById(usr3.getUserID()).orElse(null);
						if(ufl==null ) {
							UserFailedLogins userFailedLoginEntity=new UserFailedLogins();
							userFailedLoginEntity.setUserId(usr3.getUserID());
							userFailedLoginEntity.setFailedAttemptCount(1);
							userFailedLoginsRepo.save(userFailedLoginEntity);
						}else {
							if(ufl.getFailedAttemptCount() > 5) {
								serviceResponse.setServiceError("Account Lock");
							}
							else
							userFailedLoginsRepo.updateFailedLoginCount(ufl.getUserId(),ufl.getFailedAttemptCount()+1);
						}//else
					}//if(usr3!=null )
					return serviceResponse;
				}//else
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return serviceResponse;

	}



	public ServiceResponse getAllAdmins() {
		ServiceResponse serviceResponse = new ServiceResponse();
		try {

			List<UserDetails> userlist = adminRepository.findAll();

			if (!userlist.isEmpty()) {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(userlist);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			} else {
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Data Not Found");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

			}



		} catch (Exception e) {
			serviceResponse.setServiceError("Something Went Wrong");
			serviceResponse.setServiceResponse(e.getMessage());
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		return serviceResponse;

	}


	public ServiceResponse createUser(User user) {
		ServiceResponse serviceResponse = new ServiceResponse();
		user.setPassword("apmosys@123");
		User response=userRepository.save(user);
		serviceResponse.setServiceError("");
		serviceResponse.setServiceResponse(response);
		serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
		return serviceResponse;
	}



	public static String generatePassword(int length) {
		StringBuilder returnValue = new StringBuilder(length);
		for (int i = 0; i < length; i++) {
			returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
		}
		return new String(returnValue);
	}

	public ServiceResponse sessionAvailable() {

		ServiceResponse serviceResponse=new ServiceResponse();
		session=request.getSession(false);
		if (session==null)
		{

			serviceResponse.setServiceResponse(false);
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);

		}
		else
		{
			if(session.getAttribute("user")==null) {
				serviceResponse.setServiceResponse(false);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
			}
			else {
				//session.setMaxInactiveInterval(5*60);
				serviceResponse.setServiceResponse(true);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
				serviceResponse.setServiceError("");
			}

		}
		return serviceResponse;
	}

	public ServiceResponse sessionKills()
	{
		ServiceResponse serviceResponse = new ServiceResponse();
		if(session!=null)
			session.invalidate();
		serviceResponse.setServiceResponse("Sucess");
		return serviceResponse;

	}

	public ServiceResponse updatePassword(String currentPass, String newPass, String userName)
	{
		ServiceResponse serviceResponse = new ServiceResponse();
		try
		{
			UserDetails user = adminRepository.findByUsername(userName);
			if(currentPass.equals(user.getPassword()))
			{
				user.setPassword(newPass);
				UserDetails usr=adminRepository.save(user);
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Password changed successfully");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
			}
			else
			{
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("Kindly enter correct password !!!");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
			}
			return serviceResponse;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return serviceResponse;
	}
	public ServiceResponse checkDBpassword(String currentPass, String userName)
	{
		ServiceResponse serviceResponse = new ServiceResponse();
		UserDetails user=new UserDetails();
		String dbPassword;
		try
		{
			if(passwordCheckCount==0)
				user = adminRepository.findByUsername(userName);
			dbPassword=user.getPassword();
			if(currentPass.equals(dbPassword))
			{
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("True");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
			}
			else
			{
				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse("False");
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
			}
			return serviceResponse;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			serviceResponse.setServiceError("False");
			serviceResponse.setServiceResponse("");
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_FAIL);
		}
		return serviceResponse;
	}


	public ServiceResponse getAccountDetails(String usename, String userrole, String managerId, String username) {
		System.out.println("AdminService.getAccountDetails()");
		ServiceResponse serviceResponse = new ServiceResponse();
		try
		{
			List<userRole> user = configurationRepository.getAccountDetails(usename,userrole,managerId);
			System.out.println("User : "+user);
			serviceResponse.setServiceError("");
			serviceResponse.setServiceResponse(user);
			serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return serviceResponse;
	}

	/*
	 * public ServiceResponse getchecklicense() { ServiceResponse serviceResponse =
	 * new ServiceResponse(); try { String str= serviceResponse.setServiceError("");
	 * serviceResponse.setServiceResponse();
	 * serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS); }
	 * catch(Exception e) { e.printStackTrace(); } return serviceResponse; }
	 */

	public ServiceResponse getMyEntitlementFnct(String userId,String role) {
		ServiceResponse serviceResponse = new ServiceResponse();
		Integer userroles=Integer.parseInt(role);
		List<userRole> adminuser=new ArrayList<>();
		try
		{
			if(userroles==100)
			{
				List<ProjectDeatilsMeta> userRoles=projectRepository.getAllProject();

				for(int i=0;i<userRoles.size();i++)
				{
					userRole userIds=new userRole();
					userIds.setProjectName(userRoles.get(i).getProjectName());
					userIds.setAccessType("Manager");
					adminuser.add(userIds);

				}
				serviceResponse.setServiceResponse(adminuser);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);

			}
			else {
				List<userRole> user = configurationRepository.getMyEntitlementFnct(userId);

				serviceResponse.setServiceError("");
				serviceResponse.setServiceResponse(user);
				serviceResponse.setServiceStatus(ServiceResponse.STATUS_SUCCESS);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return serviceResponse;
	}//


	public void updateSessionLastCheck() {
		session = request.getSession();
		UserSessionDetails usd=null;
		if(session!=null) {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			if(detailsEntity !=null) {
				
				usd = userSessionRepo.findById(detailsEntity.getUserID()).orElse(null);

			usd.setLast_check(new Date());
			int count= userSessionRepo.updateSession(usd.getUserId(),usd.getSession_key(), "Y" , new Date());
		//	System.out.println("updated session.. count "+count); 
			}
		}//if
		else
			System.out.println("Unale to Update session.. count "); 

	}//updateSessionLastCheck


}