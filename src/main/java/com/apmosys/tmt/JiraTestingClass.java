package com.apmosys.tmt;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class JiraTestingClass {
	public static void main(String[] args) throws UnirestException, ParseException {

		/*
		//	HttpResponse<JsonNode> response = Unirest.get("https://appreciate.atlassian.net/rest/api/3/search?jql=project=AP&maxResults=-1")
		//			  .basicAuth("animesh.soni@apmosys.in", "xiLT7nPDrtGUCl3luVtYA324")
		//			  .header("Accept", "application/json")
		//			  .asJson();
		HttpResponse<JsonNode> response = Unirest.get("https://appreciate.atlassian.net/rest/agile/1.0/epic/none/issue?jql=project=AP&maxResults=200")
				.basicAuth("animesh.soni@apmosys.in", "xiLT7nPDrtGUCl3luVtYA324")
				.header("Accept", "application/json")
				.asJson();

		JsonNode j=   response.getBody();
		JSONObject o= response.getBody().getObject();
		JSONArray a= response.getBody().getObject().getJSONArray("issues");
		int ll=a.length();
		for(int x=0; x<ll;x++) {
			JSONObject op=a.getJSONObject(x);

			Object key= op.get("key");

			JSONObject field= op.getJSONObject("fields");
			JSONObject issuetype = field.getJSONObject("issuetype");

			JSONObject priority = field.getJSONObject("priority");
			JSONObject status = field.getJSONObject("status");
			String description =(String)field.get("description");
			String created =(String)field.get("created");
			String summary =(String)field.get("summary");

			String issuetypename = (String)issuetype.get("name");
			String priorityname = (String)priority.get("name");
			String statusname = (String)status.get("name");

			JSONObject wl= field.getJSONObject("worklog");
			JSONArray qe= wl.getJSONArray("worklogs");
			try {

				JSONObject o111=((JSONObject)qe.get(0));
				System.out.println(o111.get("issueId"));
			}catch (Exception e) {
				e.getMessage();
			}
		}
		Gson gson = new Gson();
		String json = gson.toJson(a);
		//	System.out.print(json);
		JSONParser parser = new JSONParser();
		Object obj;
		obj = parser.parse(json);
		org.json.simple.JSONObject topObject = (org.json.simple.JSONObject) obj;
		System.out.println(response.getBody());

		 */
		//			  .asJson();
//		HttpResponse<JsonNode> response = Unirest.get("https://appreciate.atlassian.net/rest/api/3/search?jql=project=AP%20AND%20issuetype=Epic&maxResults=100")
//				.basicAuth("animesh.soni@apmosys.in", "xiLT7nPDrtGUCl3luVtYA324")
//				.header("Accept", "application/json")
//				.asJson();
		HttpResponse<JsonNode> response = Unirest.get("https://appreciate.atlassian.net/rest/agile/1.0/issue/ AP-2231")
				.basicAuth("animesh.soni@apmosys.in", "xiLT7nPDrtGUCl3luVtYA324")
				.header("Accept", "application/json")
				.asJson();
		JsonNode j=   response.getBody();
		JSONObject j11=(JSONObject)response.getBody().getObject().get("fields");

		String priority1=(String)((JSONObject)(j11.get("priority"))).get("name");
		String status1=(String)((JSONObject)(j11.get("status"))).get("name");

		JSONObject o= response.getBody().getObject();
		JSONArray a= response.getBody().getObject().getJSONArray("issues");
		int ll=a.length();
		ArrayList<String> bugs= new ArrayList<String>();
		ArrayList<String> bugs1= new ArrayList<String>();
		for(int x=0; x<ll;x++) {

			JSONObject op=a.getJSONObject(x);
			Object key= op.get("key");
			bugs.add((String)key);
			JSONObject field= op.getJSONObject("fields");
			JSONObject issuetype = field.getJSONObject("issuetype");

			JSONObject priority = field.getJSONObject("priority");
			JSONObject status = field.getJSONObject("status");
			String created =(String)field.get("created");

			String issuetypename = (String)issuetype.get("name");
			System.out.println(issuetypename);
			try {

				String summary =(String)field.get("summary");
				String description =(String)field.get("description");
			}catch (Exception e) {
				System.out.println(e.getMessage());
			}

			String priorityname = (String)priority.get("name");
			String statusname = (String)status.get("name");

			//			JSONObject wl= field.getJSONObject("worklog");
			//			JSONArray qe= wl.getJSONArray("worklogs");
			//			try {
			//				
			//				JSONObject o111=((JSONObject)qe.get(0));
			//				System.out.println(o111.get("issueId"));
			//			}catch (Exception e) {
			//				e.getMessage();
			//			}
		}//for
		System.out.println(bugs);
		for(String bug : bugs) {
			HttpResponse<JsonNode> response1 = Unirest.get("https://appreciate.atlassian.net/rest/agile/1.0/epic/"+bug+"/issue")
					.basicAuth("animesh.soni@apmosys.in", "xiLT7nPDrtGUCl3luVtYA324")
					.header("Accept", "application/json")
					.asJson() ;
			JsonNode j1=   response1.getBody();
			JSONObject o1= response1.getBody().getObject();
			JSONArray a1 = response1.getBody().getObject().getJSONArray("issues");
			int ll1=a1.length();
			for(int x=0; x<ll1;x++) {

				JSONObject op1=a1.getJSONObject(x);
				Object key= op1.get("key");
				bugs1.add((String)key);
				//				JSONObject field= op1.getJSONObject("fields");
				//				JSONObject issuetype = field.getJSONObject("issuetype");
				//
				//				JSONObject priority = field.getJSONObject("priority");
				//				JSONObject status = field.getJSONObject("status");
				//				String created =(String)field.get("created");
				//				String issuetypename = (String)issuetype.get("name");
				try {
					//					String summary =(String)field.get("summary");
					//					String description =(String)field.get("description");
				}catch (Exception e) {
					System.out.println(e.getMessage());
				}
				//
				//				String priorityname = (String)priority.get("name");
				//				String statusname = (String)status.get("name");
			}//for 
		}//outer for
		System.out.println(bugs1);
		System.out.println(bugs1.size());
	}
}
