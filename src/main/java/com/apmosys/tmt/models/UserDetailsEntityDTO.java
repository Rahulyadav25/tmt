package com.apmosys.tmt.models;

public class UserDetailsEntityDTO {

	private String id;
	private String userID;
	private String username;
	// private String password;
	private String name;
	// private String userrole;
	private String isActive;
	private String managerID;
	private String hasAccess;
	private String createdBy;
	private String tokenID;
	private String user_role;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getManagerID() {
		return managerID;
	}

	public void setManagerID(String managerID) {
		this.managerID = managerID;
	}

	public String getHasAccess() {
		return hasAccess;
	}

	public void setHasAccess(String hasAccess) {
		this.hasAccess = hasAccess;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getTokenID() {
		return tokenID;
	}

	public void setTokenID(String tokenID) {
		this.tokenID = tokenID;
	}

	public String getUser_role() {
		return user_role;
	}

	public void setUser_role(String user_role) {
		this.user_role = user_role;
	}

	@Override
	public String toString() {
		return "UserDetailsEntityDTO [id=" + id + ", userID=" + userID + ", username=" + username + ", name=" + name
				+ ", isActive=" + isActive + ", managerID=" + managerID + ", hasAccess=" + hasAccess + ", createdBy="
				+ createdBy + ", tokenID=" + tokenID + ", user_role=" + user_role + "]";
	}
}
