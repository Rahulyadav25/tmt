package com.apmosys.tmt.models;

import java.util.Arrays;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;

@Entity
@Table
public class Attachment {

		@Id
//		@GeneratedValue(strategy = GenerationType.AUTO)
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "aID")
		private Integer aID;
		
		@Column(name = "testCaseID")
		private Integer testCaseID;
		
		@Column(name = "attachment_name")
		private String attachmentName;
		
		@Column(name = "size")
		private String size;
				
		@Column(name = "byte_content")
		private byte[] attachment;
		
		@Column(name = "uploaded_date")
		private Date uploadedDate;
		
		@Column(name = "uploaded_by")
		private String uploadedBy;
				
		public Attachment()
		{
			
		}

		public Integer getaID() {
			return aID;
		}

		public void setaID(Integer aID) {
			this.aID = aID;
		}

		public Integer getTestCaseID() {
			return testCaseID;
		}

		public void setTestCaseID(Integer testCaseID) {
			this.testCaseID = testCaseID;
		}

		public String getAttachmentName() {
			return attachmentName;
		}

		public void setAttachmentName(String attachmentName) {
			this.attachmentName = attachmentName;
		}

		public String getSize() {
			return size;
		}

		public void setSize(String size) {
			this.size = size;
		}

		public byte[] getAttachment() {
			return attachment;
		}

		public void setAttachment(byte[] attachment) {
			this.attachment = attachment;
		}

		public Date getUploadedDate() {
			return uploadedDate;
		}

		public void setUploadedDate(Date uploadedDate) {
			this.uploadedDate = uploadedDate;
		}

		public String getUploadedBy() {
			return uploadedBy;
		}

		public void setUploadedBy(String uploadedBy) {
			this.uploadedBy = uploadedBy;
		}

		@Override
		public String toString() {
			return "Attachment [aID=" + aID + ", testCaseID=" + testCaseID + ", attachmentName=" + attachmentName
					+ ", size=" + size + ", attachment=" + Arrays.toString(attachment) + ", uploadedDate="
					+ uploadedDate + ", uploadedBy=" + uploadedBy + "]";
		}

		
}
