package com.apmosys.tmt.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import jakarta.persistence.Cacheable;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@DynamicInsert
@DynamicUpdate
@Cacheable(false)
@Entity
/* @Table(name="TMTTools.Project_MetaTable") */
@Table(name = "Project_MetaTable")
public class ProjectDeatilsMeta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer projectID;
	private String createdBy;
	private Date createdTime;
	private String isActive;
	private String isJiraEnable;
	private String projectLocation;
	private String projectName;
	private String senarioNumber;
	private String testCaseNumber;
	private Date updatedTime;
	private String module;
	private String isAutomation;
	private String isMannual;
	private String submodule;
	private String platform;
	private String subplatform;
	private String version;
	private String updatedBy;
	private String projectManager;
	private Date projectStartDate;
	private Date ProjectEndDate;
	private String applicationName;
	@Transient
	private String toolName;
	@Transient
	private String toolId;
	@Transient
	private String bugTool;
	@Transient
	private String bugToolPassword;
	@Transient
	private String bugToolURL;
	@Transient
	private String bugToolApiToken;
	@Transient
	private String oldprojectname;
	@Transient
	private String DelayTimeProject;

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

	public String getDelayTimeProject() {
		return DelayTimeProject;
	}

	public void setDelayTimeProject(String delayTimeProject) {
		DelayTimeProject = delayTimeProject;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsJiraEnable() {
		return isJiraEnable;
	}

	public void setIsJiraEnable(String isJiraEnable) {
		this.isJiraEnable = isJiraEnable;
	}

	public String getProjectLocation() {
		return projectLocation;
	}

	public void setProjectLocation(String projectLocation) {
		this.projectLocation = projectLocation;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getSenarioNumber() {
		return senarioNumber;
	}

	public void setSenarioNumber(String senarioNumber) {
		this.senarioNumber = senarioNumber;
	}

	public String getTestCaseNumber() {
		return testCaseNumber;
	}

	public void setTestCaseNumber(String testCaseNumber) {
		this.testCaseNumber = testCaseNumber;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getIsAutomation() {
		return isAutomation;
	}

	public void setIsAutomation(String isAutomation) {
		this.isAutomation = isAutomation;
	}

	public String getIsMannual() {
		return isMannual;
	}

	public void setIsMannual(String isMannual) {
		this.isMannual = isMannual;
	}

	public String getSubmodule() {
		return submodule;
	}

	public void setSubmodule(String submodule) {
		this.submodule = submodule;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getSubplatform() {
		return subplatform;
	}

	public void setSubplatform(String subplatform) {
		this.subplatform = subplatform;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getProjectManager() {
		return projectManager;
	}

	public void setProjectManager(String projectManager) {
		this.projectManager = projectManager;
	}

	public Date getProjectStartDate() {
		return projectStartDate;
	}

	public String getToolName() {
		return toolName;
	}

	public void setToolName(String toolName) {
		this.toolName = toolName;
	}

	public String getToolId() {
		return toolId;
	}

	public void setToolId(String toolId) {
		this.toolId = toolId;
	}

	public void setProjectStartDate(Date projectStartDate) {
		this.projectStartDate = projectStartDate;
	}

	public void setProjectEndDate(Date projectEndDate) {
		ProjectEndDate = projectEndDate;
	}

	public void setProjectStartDate(String ProjectStartDate) {
		Date date1 = null;
		try {
			date1 = new SimpleDateFormat("MM/dd/yyyy").parse(ProjectStartDate);
		} catch (ParseException e) {
			try {
				date1 = new SimpleDateFormat("yyyy-MM-dd").parse(ProjectStartDate);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		java.sql.Date sqlDate = new java.sql.Date(date1.getTime());
		projectStartDate = sqlDate;
	}

	public Date getProjectEndDate() {
		return ProjectEndDate;
	}

	public void setProjectEndDate(String projectEndDate) {
		Date date1 = null;
		try {
			date1 = new SimpleDateFormat("MM/dd/yyyy").parse(projectEndDate);
		} catch (ParseException e) {
			try {
				date1 = new SimpleDateFormat("yyyy-MM-dd").parse(projectEndDate);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		java.sql.Date sqlDate = new java.sql.Date(date1.getTime());
		ProjectEndDate = sqlDate;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getBugTool() {
		return bugTool;
	}

	public void setBugTool(String bugTool) {
		this.bugTool = bugTool;
	}

	public String getBugToolPassword() {
		return bugToolPassword;
	}

	public void setBugToolPassword(String bugToolPassword) {
		this.bugToolPassword = bugToolPassword;
	}

	public String getBugToolURL() {
		return bugToolURL;
	}

	public void setBugToolURL(String bugToolURL) {
		this.bugToolURL = bugToolURL;
	}

	public String getBugToolApiToken() {
		return bugToolApiToken;
	}

	public void setBugToolApiToken(String bugToolApiToken) {
		this.bugToolApiToken = bugToolApiToken;
	}

	public String getOldprojectname() {
		return oldprojectname;
	}

	public void setOldprojectname(String oldprojectname) {
		this.oldprojectname = oldprojectname;
	}

	@Override
	public String toString() {
		return "ProjectDeatilsMeta [projectID=" + projectID + ", createdBy=" + createdBy + ", createdTime="
				+ createdTime + ", isActive=" + isActive + ", isJiraEnable=" + isJiraEnable + ", projectLocation="
				+ projectLocation + ", projectName=" + projectName + ", senarioNumber=" + senarioNumber
				+ ", testCaseNumber=" + testCaseNumber + ", updatedTime=" + updatedTime + ", module=" + module
				+ ", isAutomation=" + isAutomation + ", isMannual=" + isMannual + ", submodule=" + submodule
				+ ", platform=" + platform + ", subplatform=" + subplatform + ", version=" + version + ", updatedBy="
				+ updatedBy + ", projectManager=" + projectManager + ", projectStartDate=" + projectStartDate
				+ ", ProjectEndDate=" + ProjectEndDate + ", applicationName=" + applicationName + ", toolName="
				+ toolName + ", toolId=" + toolId + ", bugTool=" + bugTool + ", bugToolPassword=" + bugToolPassword
				+ ", bugToolURL=" + bugToolURL + ", bugToolApiToken=" + bugToolApiToken + ", oldprojectname="
				+ oldprojectname + ", DelayTimeProject=" + DelayTimeProject + "]";
	}



}
