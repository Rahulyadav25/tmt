package com.apmosys.tmt.models;

public class RobosoftJiraAutomationModel {
 	
	private String runID;
	private String keyname;
	private String summary;
	private String description;
	private String issueName;
	private	String priortyid;
	private String subModule;
	private String epicName;
	private String status;
	
	
	public RobosoftJiraAutomationModel() {
		super();
	}
	public RobosoftJiraAutomationModel(String runID, String keyname, String summary, String description,
			String issueName, String priortyid, String subModule, String epicName, String status) {
		super();
		this.runID = runID;
		this.keyname = keyname;
		this.summary = summary;
		this.description = description;
		this.issueName = issueName;
		this.priortyid = priortyid;
		this.subModule = subModule;
		this.epicName = epicName;
		this.status = status;
	}
	
	public String getRunID() {
		return runID;
	}
	public void setRunID(String runID) {
		this.runID = runID;
	}
	public String getKeyname() {
		return keyname;
	}
	public void setKeyname(String keyname) {
		this.keyname = keyname;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIssueName() {
		return issueName;
	}
	public void setIssueName(String issueName) {
		this.issueName = issueName;
	}
	public String getPriortyid() {
		return priortyid;
	}
	public void setPriortyid(String priortyid) {
		this.priortyid = priortyid;
	}
	public String getSubModule() {
		return subModule;
	}
	public void setSubModule(String subModule) {
		this.subModule = subModule;
	}
	public String getEpicName() {
		return epicName;
	}
	public void setEpicName(String epicName) {
		this.epicName = epicName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "RobosoftJiraAutomationModel [runID=" + runID + ", keyname=" + keyname + ", summary=" + summary
				+ ", description=" + description + ", issueName=" + issueName + ", priortyid=" + priortyid
				+ ", subModule=" + subModule + ", epicName=" + epicName + ", status=" + status + "]";
	}
	
}
