package com.apmosys.tmt.models;
public class RunWiseGraphData 
{
private Integer runid;
private Integer pass;
private Integer fail;
private Integer retest;
private Integer blocker;
private Integer totalcase;
private Integer untested;
private String runname;
public String getRunname() {
	return runname;
}
public void setRunname(String runname) {
	this.runname = runname;
}
public Integer getUntested() {
	return untested;
}
public void setUntested(Integer untested) {
	this.untested = untested;
}
public Integer getRunid() {
	return runid;
}
public void setRunid(Integer runid) {
	this.runid = runid;
}
public Integer getPass() {
	return pass;
}
public void setPass(Integer pass) {
	this.pass = pass;
}
public Integer getFail() {
	return fail;
}
public void setFail(Integer fail) {
	this.fail = fail;
}
public Integer getRetest() {
	return retest;
}
public void setRetest(Integer retest) {
	this.retest = retest;
}
public Integer getBlocker() {
	return blocker;
}
public void setBlocker(Integer blocker) {
	this.blocker = blocker;
}
public Integer getTotalcase() {
	return totalcase;
}
public void setTotalcase(Integer totalcase) {
	this.totalcase = totalcase;
}

	
}
