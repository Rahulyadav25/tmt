package com.apmosys.tmt.models;

import java.util.ArrayList;
import java.util.List;

public class TestSuitesData {

	private String projectid;
	private String projectname;
	private String scenarioID;
	private Integer totalTestSuites;
	private Integer totalPassed;
	private Integer totalFailed;
	private Integer runId;
	private Integer totalTestCases;
	TestSuiteDefectTracker defectTrackers= new TestSuiteDefectTracker();
	
	

	public TestSuiteDefectTracker getDefectTrackers() {
		return defectTrackers;
	}
	public void setDefectTrackers(TestSuiteDefectTracker defectTrackers) {
		this.defectTrackers = defectTrackers;
	}
	public Integer getTotalTestCases() {
		return totalTestCases;
	}
	public void setTotalTestCases(Integer totalTestCases) {
		this.totalTestCases = totalTestCases;
	}
	public Integer getRunId() {
		return runId;
	}
	public void setRunId(Integer runId) {
		this.runId = runId;
	}
	public String getProjectid() {
		return projectid;
	}
	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	public String getScenarioID() {
		return scenarioID;
	}
	public void setScenarioID(String scenarioID) {
		this.scenarioID = scenarioID;
	}
	public Integer getTotalTestSuites() {
		return totalTestSuites;
	}
	public void setTotalTestSuites(Integer totalTestSuites) {
		this.totalTestSuites = totalTestSuites;
	}
	public Integer getTotalPassed() {
		return totalPassed;
	}
	public void setTotalPassed(Integer totalPassed) {
		this.totalPassed = totalPassed;
	}
	public Integer getTotalFailed() {
		return totalFailed;
	}
	public void setTotalFailed(Integer totalFailed) {
		this.totalFailed = totalFailed;
	}
	

	
}