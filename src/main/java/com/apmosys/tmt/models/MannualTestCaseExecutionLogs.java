package com.apmosys.tmt.models;

import java.util.Date;

//import javax.persistence.Cacheable;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@DynamicInsert
@DynamicUpdate
@Cacheable(false)
@Entity
@Table(name = "MannualTestCaseExecutionLogs")
public class MannualTestCaseExecutionLogs {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Integer reRunId;
	private Integer projectId;
	@Transient
	private Integer bugToolId;
	private Integer testSuitesId;
	private Integer testGroupId;	
	@Transient
	private String groupName;
	@Transient
	private String suiteName;
	
	@Column(name = "SrID")
	private Integer testCaseSrId;
	@Column(name = "absoluteSrID")
	private Integer absoluteTCID;



	private String scenarios;
	private String functionality;
	private String testID;
	private String testCaseType;
	private String testDescription;
	private String browser;
	private String testerId;
	private String testData;
	private String steps;
	private String expectedResult;
	private String actualResult;
	private String status;
	@Transient
	private String testerName;
	private String bugSeverity;
	private String bugPriority;
	private String bugStatus;
	private String testerComment;
	private Date dateOfExec;
	private Long executionTime;
	private String issueIntegrationId;
	private String bugId;
	private Date createdTimeStamp;
	private Date updatedTimeStamp;
	private String isActive;
	private String testCaseAction;

	@Transient
	private String toolName;
	@Transient
	private String productName;
	@Transient
	private String componentName;
	@Transient
	private String bugType;
	@Transient
	private Long jiraPriorityId;
	@Column(name = "bug_summary")
	private String bugSummary;
	@Transient
	private String bugDescription = null;
	@Transient
	private Long preparationTime;
	@Transient
	private String tcPreparedBy;

	@Transient
	private String epicName;
	@Transient
	private String osVersion;
	
	
	
	
	
	
	
	
	
	public String getSuiteName() {
		return suiteName;
	}

	@Transient
	private String teamstring;



	public String getTeamstring() {
		return teamstring;
	}



	public void setTeamstring(String teamstring) {
		this.teamstring = teamstring;
	}



	public void setSuiteName(String suiteName) {
		this.suiteName = suiteName;
	}

	@Transient
	private String ErrorMessage;

	
	@Column(name = "srNo")
	private Integer testCaseSrNo;
	private String scenarioID;
	private String module;
	
	public String getErrorMessage() {
		return ErrorMessage;
	}



	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

	private String subModule;
	@Transient
	private String reportExcutionTime;
	@Transient
	private String isassign;
	private String bugAge;
	@Column(name = "priority")
	private String priority;
	@Column(name = "complexity")
	private String complexity;
	
	@Transient
	private String redminePriority;
	@Transient
	private Integer redmineProjectId;
	@Transient
	private String redmineStatus;

	public String getBugAge() {
		return bugAge;
	}



	public void setBugAge(String bugAge) {
		this.bugAge = bugAge;
	}



	public String getPriority() {
		return priority;
	}



	public void setPriority(String priority) {
		this.priority = priority;
	}



	public String getComplexity() {
		return complexity;
	}



	public void setComplexity(String complexity) {
		this.complexity = complexity;
	}



	public String getReportExcutionTime() {
		return reportExcutionTime;
	}



	public void setReportExcutionTime(String reportExcutionTime) {
		this.reportExcutionTime = reportExcutionTime;
	}



	public String getIsassign() {
		return isassign;
	}



	public void setIsassign(String isassign) {
		this.isassign = isassign;
	}



	public String getModule() {
		return module;
	}



	public void setModule(String module) {
		this.module = module;
	}



	public String getSubModule() {
		return subModule;
	}



	public void setSubModule(String subModule) {
		this.subModule = subModule;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBugStatus() {
		return bugStatus;
	}

	public void setBugStatus(String bugStatus) {
		this.bugStatus = bugStatus;
	}
	public Integer getReRunId() {
		return reRunId;
	}

	public void setReRunId(Integer reRunId) {
		this.reRunId = reRunId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}



	public Integer getTestSuitesId() {
		return testSuitesId;
	}



	public void setTestSuitesId(Integer testSuitesId) {
		this.testSuitesId = testSuitesId;
	}



	public Integer getTestGroupId() {
		return testGroupId;
	}



	public void setTestGroupId(Integer testGroupId) {
		this.testGroupId = testGroupId;
	}



	public String getGroupName() {
		return groupName;
	}



	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}



	public Integer getTestCaseSrId() {
		return testCaseSrId;
	}



	public void setTestCaseSrId(Integer testCaseSrId) {
		this.testCaseSrId = testCaseSrId;
	}



	public Integer getTestCaseSrNo() {
		return testCaseSrNo;
	}



	public void setTestCaseSrNo(Integer testCaseSrNo) {
		this.testCaseSrNo = testCaseSrNo;
	}



	public Integer getBugToolId() {
		return bugToolId;
	}



	public void setBugToolId(Integer bugToolId) {
		this.bugToolId = bugToolId;
	}



	public String getScenarioID() {
		return scenarioID;
	}



	public void setScenarioID(String scenarioID) {
		this.scenarioID = scenarioID;
	}



	public String getScenarios() {
		return scenarios;
	}



	public void setScenarios(String scenarios) {
		this.scenarios = scenarios;
	}



	public String getFunctionality() {
		return functionality;
	}



	public void setFunctionality(String functionality) {
		this.functionality = functionality;
	}



	public String getTestID() {
		return testID;
	}



	public void setTestID(String testID) {
		this.testID = testID;
	}



	public String getTestCaseType() {
		return testCaseType;
	}



	public void setTestCaseType(String testCaseType) {
		this.testCaseType = testCaseType;
	}



	public String getTestDescription() {
		return testDescription;
	}



	public void setTestDescription(String testDescription) {
		this.testDescription = testDescription;
	}



	public String getBrowser() {
		return browser;
	}



	public void setBrowser(String browser) {
		this.browser = browser;
	}



	public String getTesterId() {
		return testerId;
	}



	public void setTesterId(String testerId) {
		this.testerId = testerId;
	}



	public String getTestData() {
		return testData;
	}



	public void setTestData(String testData) {
		this.testData = testData;
	}



	public String getSteps() {
		return steps;
	}



	public void setSteps(String steps) {
		this.steps = steps;
	}



	public String getExpectedResult() {
		return expectedResult;
	}



	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}



	public String getActualResult() {
		return actualResult;
	}



	public void setActualResult(String actualResult) {
		this.actualResult = actualResult;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public String getTesterName() {
		return testerName;
	}



	public void setTesterName(String testerName) {
		this.testerName = testerName;
	}



	public String getBugSeverity() {
		return bugSeverity;
	}



	public void setBugSeverity(String bugSeverity) {
		this.bugSeverity = bugSeverity;
	}



	public String getBugPriority() {
		return bugPriority;
	}



	public void setBugPriority(String bugPriority) {
		this.bugPriority = bugPriority;
	}



	public String getTesterComment() {
		return testerComment;
	}



	public void setTesterComment(String testerComment) {
		this.testerComment = testerComment;
	}



	



	public Date getDateOfExec() {
		return dateOfExec;
	}



	public void setDateOfExec(Date dateOfExec) {
		this.dateOfExec = dateOfExec;
	}



	public Long getExecutionTime() {
		return executionTime;
	}



	public void setExecutionTime(Long executionTime) {
		this.executionTime = executionTime;
	}



	public String getIssueIntegrationId() {
		return issueIntegrationId;
	}



	public void setIssueIntegrationId(String issueIntegrationId) {
		this.issueIntegrationId = issueIntegrationId;
	}



	public String getBugId() {
		return bugId;
	}



	public void setBugId(String bugId) {
		this.bugId = bugId;
	}



	public Date getCreatedTimeStamp() {
		return createdTimeStamp;
	}



	public void setCreatedTimeStamp(Date createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}



	public Date getUpdatedTimeStamp() {
		return updatedTimeStamp;
	}



	public void setUpdatedTimeStamp(Date updatedTimeStamp) {
		this.updatedTimeStamp = updatedTimeStamp;
	}



	public String getIsActive() {
		return isActive;
	}



	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}



	public String getTestCaseAction() {
		return testCaseAction;
	}



	public void setTestCaseAction(String testCaseAction) {
		this.testCaseAction = testCaseAction;
	}



	public String getToolName() {
		return toolName;
	}



	public void setToolName(String toolName) {
		this.toolName = toolName;
	}



	public String getProductName() {
		return productName;
	}



	public void setProductName(String productName) {
		this.productName = productName;
	}



	public String getComponentName() {
		return componentName;
	}



	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}



	public String getBugType() {
		return bugType;
	}



	public void setBugType(String bugType) {
		this.bugType = bugType;
	}


	public Long getJiraPriorityId() {
		return jiraPriorityId;
	}



	public void setJiraPriorityId(Long jiraPriorityId) {
		this.jiraPriorityId = jiraPriorityId;
	}



	public Integer getAbsoluteTCID() {
		return absoluteTCID;
	}



	public void setAbsoluteTCID(Integer absoluteTCID) {
		this.absoluteTCID = absoluteTCID;
	}



	public String getBugSummary() {
		return bugSummary;
	}



	public void setBugSummary(String bugSummary) {
		this.bugSummary = bugSummary;
	}



	public String getBugDescription() {
//		if (toolName!=null && toolName.equalsIgnoreCase("jira") && bugStatus!=null 
//				&& !bugStatus.equalsIgnoreCase("Reopen") 
//				&& !bugStatus.equalsIgnoreCase("Re-open") && !bugStatus.equalsIgnoreCase("closed")  )
//			return "Scenario: "+ scenarios +"\n" +"Functionality: "+functionality+ "\n"+"Browser : "+browser + " os version: "+osVersion+"\n"+"Steps:\n "+steps + "\n"+"Test Data : " + testData + "\n" +"Test Description : "+ testDescription + "\n" +"Expected Result : "+ expectedResult+"\nActual Result: "+actualResult;
		return bugDescription;
	}



	public void setBugDescription(String bugDescription) {
		this.bugDescription = bugDescription;
	}



	public Long getPreparationTime() {
		return preparationTime;
	}



	public void setPreparationTime(Long preparationTime) {
		this.preparationTime = preparationTime;
	}



	public String getTcPreparedBy() {
		return tcPreparedBy;
	}



	public void setTcPreparedBy(String tcPreparedBy) {
		this.tcPreparedBy = tcPreparedBy;
	}



	



	public String getEpicName() {
		return epicName;
	}



	public void setEpicName(String epicName) {
		this.epicName = epicName;
	}



	public String getOsVersion() {
		return osVersion;
	}



	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}



	public String getRedminePriority() {
		return redminePriority;
	}



	public void setRedminePriority(String redminePriority) {
		this.redminePriority = redminePriority;
	}



	public Integer getRedmineProjectId() {
		return redmineProjectId;
	}



	public void setRedmineProjectId(Integer redmineProjectId) {
		this.redmineProjectId = redmineProjectId;
	}



	public String getRedmineStatus() {
		return redmineStatus;
	}



	public void setRedmineStatus(String redmineStatus) {
		this.redmineStatus = redmineStatus;
	}



	@Override
	public String toString() {
		return "MannualTestCaseExecutionLogs [id=" + id + ", reRunId=" + reRunId + ", projectId=" + projectId
				+ ", bugToolId=" + bugToolId + ", testSuitesId=" + testSuitesId + ", testGroupId=" + testGroupId
				+ ", groupName=" + groupName + ", suiteName=" + suiteName + ", testCaseSrId=" + testCaseSrId
				+ ", absoluteTCID=" + absoluteTCID + ", scenarios=" + scenarios + ", functionality=" + functionality
				+ ", testID=" + testID + ", testCaseType=" + testCaseType + ", testDescription=" + testDescription
				+ ", browser=" + browser + ", testerId=" + testerId + ", testData=" + testData + ", steps=" + steps
				+ ", expectedResult=" + expectedResult + ", actualResult=" + actualResult + ", status=" + status
				+ ", testerName=" + testerName + ", bugSeverity=" + bugSeverity + ", bugPriority=" + bugPriority
				+ ", bugStatus=" + bugStatus + ", testerComment=" + testerComment + ", dateOfExec=" + dateOfExec
				+ ", executionTime=" + executionTime + ", issueIntegrationId=" + issueIntegrationId + ", bugId=" + bugId
				+ ", createdTimeStamp=" + createdTimeStamp + ", updatedTimeStamp=" + updatedTimeStamp + ", isActive="
				+ isActive + ", testCaseAction=" + testCaseAction + ", toolName=" + toolName + ", productName="
				+ productName + ", componentName=" + componentName + ", bugType=" + bugType + ", jiraPriorityId="
				+ jiraPriorityId + ", bugSummary=" + bugSummary + ", bugDescription=" + bugDescription
				+ ", preparationTime=" + preparationTime + ", tcPreparedBy=" + tcPreparedBy + ", epicName=" + epicName
				+ ", osVersion=" + osVersion + ", teamstring=" + teamstring + ", ErrorMessage=" + ErrorMessage
				+ ", testCaseSrNo=" + testCaseSrNo + ", scenarioID=" + scenarioID + ", module=" + module
				+ ", subModule=" + subModule + ", reportExcutionTime=" + reportExcutionTime + ", isassign=" + isassign
				+ ", bugAge=" + bugAge + ", priority=" + priority + ", complexity=" + complexity + ", redminePriority="
				+ redminePriority + ", redmineProjectId=" + redmineProjectId + ", redmineStatus=" + redmineStatus + "]";
	}
	
	
	


}
