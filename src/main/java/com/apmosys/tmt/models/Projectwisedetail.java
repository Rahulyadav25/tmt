package com.apmosys.tmt.models;

public class Projectwisedetail {
	private Long totaltestcases;
	private Long totaltestsuites;
	private Long totalrun;
	private String pjname;
	private Long totalexecuted;
	private Long executedpercentage;
	private Long currentrun;
	private String successpercent;
	public String getSuccesspercent() {
		return successpercent;
	}
	public void setSuccesspercent(String string) {
		this.successpercent = string;
	}
	public String getpjname() {
		return pjname;
	}
	public void setpjname(String projectname) {
		this.pjname = projectname;
	}
	public Long getCurrentrun() {
		return currentrun;
	}
	public void setCurrentrun(Long currentrun) {
		this.currentrun = currentrun;
	}
	public Long getTotaltestcases() {
		return totaltestcases;
	}
	public void setTotaltestcases(Long totaltestcases2) {
		this.totaltestcases = totaltestcases2;
	}
	public Long getTotaltestsuites() {
		return totaltestsuites;
	}
	public void setTotaltestsuites(Long totaltestsuites) {
		this.totaltestsuites = totaltestsuites;
	}
	public Long getTotalrun() {
		return totalrun;
	}
	public void setTotalrun(Long totalrun) {
		this.totalrun = totalrun;
	}
	
	public Long getTotalexecuted() {
		return totalexecuted;
	}
	public void setTotalexecuted(Long totalexecuted) {
		this.totalexecuted = totalexecuted;
	}
	public Long getExecutedpercentage() {
		return executedpercentage;
	}
	public void setExecutedpercentage(Long executedpercentage) {
		this.executedpercentage = executedpercentage;
	}
	
	

}
