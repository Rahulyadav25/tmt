package com.apmosys.tmt.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
@Entity
@Table(name="BugIntegrationTool")
public class BugTrackorToolEntity {
	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Toolid")
	private Integer toolid;
	
	public Integer getToolid() {
		return toolid;
	}
	public void setToolid(Integer toolid) {
		this.toolid = toolid;
	}
	@Column(name = "BuGTrackingToolName")
	private String bugTrackingToolName;
	
	@Column(name = "user_defined_bugtool_name")
	private String userDefinedToolName;
	
	@Column(name = "is_Active")
	private String isactive;
	
	@Column(name = "Userid")
	private String Userid;
	
	public String getUserid() {
		return Userid;
	}
	public void setUserid(String userid) {
		Userid = userid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Column(name = "password")
	private String password;
	public String getIsactive() {
		return isactive;
	}
	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}
	@Column(name = "IP")
	private String IP;
	
	@Column(name = "api_token")
	private String apiToken;
	
	public String getBugTrackingToolName() {
		return bugTrackingToolName;
	}
	public void setBugTrackingToolName(String bugTrackingToolName) {
		this.bugTrackingToolName = bugTrackingToolName;
	}
	public String getUserDefinedToolName() {
		return userDefinedToolName;
	}
	public void setUserDefinedToolName(String userDefinedToolName) {
		this.userDefinedToolName = userDefinedToolName;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getApiToken() {
		return apiToken;
	}
	public void setApiToken(String apiToken) {
		this.apiToken = apiToken;
	}
	
	
}
