package com.apmosys.tmt.models;

import java.util.Date;

public class ResouecesData {

	private String UserName;
	private Integer NotStarted;
	private Integer onhold;
	private Integer Na;
	private Integer UnderReview;
	private Integer Completed;
	private Integer TotalCount;
	private Date ExcutionDate;
	private Integer TotalCountPrepared;


	public Date getExcutionDate() {
		return ExcutionDate;
	}

	public void setExcutionDate(Date excutionDate) {
		ExcutionDate = excutionDate;
	}

	public Integer getTotalCount() {
		return TotalCount;
	}

	public void setTotalCount(Integer totalCount) {
		TotalCount = totalCount;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public Integer getNotStarted() {
		return NotStarted;
	}

	public void setNotStarted(Integer notStarted) {
		NotStarted = notStarted;
	}

	public Integer getOnhold() {
		return onhold;
	}

	public void setOnhold(Integer onhold) {
		this.onhold = onhold;
	}

	public Integer getNa() {
		return Na;
	}

	public void setNa(Integer na) {
		Na = na;
	}

	public Integer getUnderReview() {
		return UnderReview;
	}

	public void setUnderReview(Integer underReview) {
		UnderReview = underReview;
	}

	public Integer getCompleted() {
		return Completed;
	}

	public void setCompleted(Integer completed) {
		Completed = completed;
	}

	public Integer getTotalCountPrepared() {
		return TotalCountPrepared;
	}

	public void setTotalCountPrepared(Integer totalCountPrepared) {
		TotalCountPrepared = totalCountPrepared;
	}

}
