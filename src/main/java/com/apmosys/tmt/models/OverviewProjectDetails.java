package com.apmosys.tmt.models;

public class OverviewProjectDetails {
private Integer projectId;
private String projectName;
private Long testSuitesNo;
private Long testGroupNo;
private Long testCaseNo;
public Integer getProjectId() {
	return projectId;
}
public void setProjectId(Integer projectId) {
	this.projectId = projectId;
}
public String getProjectName() {
	return projectName;
}
public void setProjectName(String projectName) {
	this.projectName = projectName;
}
public Long getTestSuitesNo() {
	return testSuitesNo;
}
public void setTestSuitesNo(Long testSuitesNo) {
	this.testSuitesNo = testSuitesNo;
}
public Long getTestGroupNo() {
	return testGroupNo;
}
public void setTestGroupNo(Long testGroupNo) {
	this.testGroupNo = testGroupNo;
}
public Long getTestCaseNo() {
	return testCaseNo;
}
public void setTestCaseNo(Long testCaseNo) {
	this.testCaseNo = testCaseNo;
}




}
