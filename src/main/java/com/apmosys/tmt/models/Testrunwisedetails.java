package com.apmosys.tmt.models;

public class Testrunwisedetails {
private Integer runid;
private Integer totalcases;
private String runname;
private Integer totalpass;
private Integer totalfail;
private Integer totalunexecuted;
private Integer totalexecuted;
private Integer totalmajor;
private Integer totalminor;
private Integer totalcritical;
private Integer totalblocker;
private Integer totalcompleted;
private Integer totalonhold;
private Integer totalNe;
private Integer totalunderreview;
private Integer totalna;
private Integer round;
private Integer open;
private Integer closed;
private Integer reopen;
private Integer resolved;
private String status;
private String isActive;
private Integer blockshowtopper;
private Integer blockedundelivered;
private Integer na;
private Integer blockedtestdata;
private Integer mustte;
private Integer needte;
private Integer nicete;
private Integer high;
private Integer medium;
private Integer low;
private Integer blockednotanclear;
private Integer ne;
private Integer deferred;
private Integer onhold;
private Integer duplicate;
private Integer OutofScope;
private Integer partiallypass;
private Integer totalactionincomplete;


public Integer getPartiallypass() {
	return partiallypass;
}
public void setPartiallypass(Integer partiallypass) {
	this.partiallypass = partiallypass;
}
public Integer getTotalactionincomplete() {
	return totalactionincomplete;
}
public void setTotalactionincomplete(Integer totalactionincomplete) {
	this.totalactionincomplete = totalactionincomplete;
}
public Integer getBlockednotanclear() {
	return blockednotanclear;
}
public void setBlockednotanclear(Integer blockednotanclear) {
	this.blockednotanclear = blockednotanclear;
}
public Integer getNe() {
	return ne;
}
public void setNe(Integer ne) {
	this.ne = ne;
}
public Integer getDeferred() {
	return deferred;
}
public void setDeferred(Integer deferred) {
	this.deferred = deferred;
}
public Integer getOnhold() {
	return onhold;
}
public void setOnhold(Integer onhold) {
	this.onhold = onhold;
}
public Integer getDuplicate() {
	return duplicate;
}
public void setDuplicate(Integer duplicate) {
	this.duplicate = duplicate;
}
public Integer getOutofScope() {
	return OutofScope;
}
public void setOutofScope(Integer outofScope) {
	OutofScope = outofScope;
}
public Integer getBlockshowtopper() {
	return blockshowtopper;
}
public void setBlockshowtopper(Integer blockshowtopper) {
	this.blockshowtopper = blockshowtopper;
}
public Integer getHigh() {
	return high;
}
public void setHigh(Integer high) {
	this.high = high;
}
public Integer getMedium() {
	return medium;
}
public void setMedium(Integer medium) {
	this.medium = medium;
}
public Integer getLow() {
	return low;
}
public void setLow(Integer low) {
	this.low = low;
}
public Integer getBlockedundelivered() {
	return blockedundelivered;
}
public void setBlockedundelivered(Integer blockedundelivered) {
	this.blockedundelivered = blockedundelivered;
}
public Integer getNa() {
	return na;
}
public void setNa(Integer na) {
	this.na = na;
}
public Integer getBlockedtestdata() {
	return blockedtestdata;
}
public void setBlockedtestdata(Integer blockedtestdata) {
	this.blockedtestdata = blockedtestdata;
}
public Integer getMustte() {
	return mustte;
}
public void setMustte(Integer mustte) {
	this.mustte = mustte;
}
public Integer getNeedte() {
	return needte;
}
public void setNeedte(Integer needte) {
	this.needte = needte;
}
public Integer getNicete() {
	return nicete;
}
public void setNicete(Integer nicete) {
	this.nicete = nicete;
}
public Integer getTotalna() {
	return totalna;
}
public void setTotalna(Integer totalna) {
	this.totalna = totalna;
}

public String getIsActive() {
	return isActive;
}
public void setIsActive(String isActive) {
	this.isActive = isActive;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public Integer getOpen() {
	return open;
}
public void setOpen(Integer open) {
	this.open = open;
}
public Integer getClosed() {
	return closed;
}
public void setClosed(Integer closed) {
	this.closed = closed;
}
public Integer getReopen() {
	return reopen;
}
public void setReopen(Integer reopen) {
	this.reopen = reopen;
}
public Integer getResolved() {
	return resolved;
}
public void setResolved(Integer resolved) {
	this.resolved = resolved;
}
private String runround;
public String getRunround() {
	return runround;
}
public void setRunround(String runround) {
	this.runround = runround;
}
public Integer getRound() {
	return round;
}
public void setRound(Integer round) {
	this.round = round;
}
public Integer getTotalmajor() {
	return totalmajor;
}
public void setTotalmajor(Integer totalmajor) {
	this.totalmajor = totalmajor;
}
public Integer getTotalminor() {
	return totalminor;
}
public void setTotalminor(Integer totalminor) {
	this.totalminor = totalminor;
}
public Integer getTotalcritical() {
	return totalcritical;
}
public void setTotalcritical(Integer totalcritical) {
	this.totalcritical = totalcritical;
}
public Integer getTotalblocker() {
	return totalblocker;
}
public void setTotalblocker(Integer totalblocker) {
	this.totalblocker = totalblocker;
}
public Integer getTotalcompleted() {
	return totalcompleted;
}
public void setTotalcompleted(Integer totalcompleted) {
	this.totalcompleted = totalcompleted;
}
public Integer getTotalonhold() {
	return totalonhold;
}
public void setTotalonhold(Integer totalonhold) {
	this.totalonhold = totalonhold;
}
public Integer getTotalNe() {
	return totalNe;
}
public void setTotalNe(Integer totalNe) {
	this.totalNe = totalNe;
}
public Integer getTotalunderreview() {
	return totalunderreview;
}
public void setTotalunderreview(Integer totalunderreview) {
	this.totalunderreview = totalunderreview;
}
public Integer getTotalnotstarted() {
	return totalnotstarted;
}
public void setTotalnotstarted(Integer totalnotstarted) {
	this.totalnotstarted = totalnotstarted;
}
private Integer totalnotstarted;
public Integer getTotalexecuted() {
	return totalexecuted;
}
public void setTotalexecuted(Integer totalexecuted) {
	this.totalexecuted = totalexecuted;
}
private float totalmanhours;

public Integer getRunid() {
	return runid;
}
public void setRunid(Integer runid) {
	this.runid = runid;
}
public Integer getTotalcases() {
	return totalcases;
}
public void setTotalcases(Integer totalcases) {
	this.totalcases = totalcases;
}
public String getRunname() {
	return runname;
}
public void setRunname(String runname) {
	this.runname = runname;
}
public Integer getTotalpass() {
	return totalpass;
}
public void setTotalpass(Integer totalpass) {
	this.totalpass = totalpass;
}
public Integer getTotalfail() {
	return totalfail;
}
public void setTotalfail(Integer totalfail) {
	this.totalfail = totalfail;
}
public Integer getTotalunexecuted() {
	return totalunexecuted;
}
public void setTotalunexecuted(Integer totalunexecuted) {
	this.totalunexecuted = totalunexecuted;
}
public float getTotalmanhours() {
	return totalmanhours;
}
public void setTotalmanhours(float totalmanhours) {
	this.totalmanhours = totalmanhours;
}

}
