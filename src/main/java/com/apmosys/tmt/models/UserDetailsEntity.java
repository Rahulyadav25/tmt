package com.apmosys.tmt.models;

import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@Cacheable(false)
@Entity
@Table(name = "UserDetailsEntity")
public class UserDetailsEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer id;
	@Column(name = "userID")
	private String userID;

	@Column(name = "userName")
	private String username;

	@Column(name = "password")
	private String password;

	@Column(name = "name")
	private String name;

	@Column(name = "userRole")
	private Integer userrole;
	@Column(name = "isActive")
	private String isActive;

	@Column(name = "managerID")
	private Integer managerID;
	@Transient
	private String hasAcccess;

	@Column(name = "createdBy")
	private Integer createdBy;

	@Transient
	private String tokenID;

	public Integer getUserrole() {
		return userrole;
	}

	public void setUserrole(Integer userrole) {
		this.userrole = userrole;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getTokenID() {
		return tokenID;
	}

	public void setTokenID(String tokenID) {
		this.tokenID = tokenID;
	}

	public String getHasAcccess() {
		return hasAcccess;
	}

	public void setHasAcccess(String hasAcccess) {
		this.hasAcccess = hasAcccess;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String string) {
		this.userID = string;
	}

	public Integer getManagerID() {
		return managerID;
	}

	public void setManagerID(Integer managerID) {
		this.managerID = managerID;
	}

	public UserDetailsEntity() {

	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getUser_role() {
		return userrole;
	}

	public void setUser_role(Integer integer) {
		this.userrole = integer;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "UserDetailsEntity [id=" + id + ", userID=" + userID + ", username=" + username + ", password="
				+ password + ", name=" + name + ", userrole=" + userrole + ", isActive=" + isActive + ", managerID="
				+ managerID + ", hasAcccess=" + hasAcccess + ", createdBy=" + createdBy + ", tokenID=" + tokenID + "]";
	}
	
}
