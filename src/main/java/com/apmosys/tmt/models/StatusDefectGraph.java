package com.apmosys.tmt.models;

public class StatusDefectGraph {

	private String Monthname;
	private Integer open;
	private Integer reopen;
	private Integer Closed;
	private Integer Resolved;
	public String getMonthname() {
		return Monthname;
	}
	public void setMonthname(String monthname) {
		Monthname = monthname;
	}
	public Integer getOpen() {
		return open;
	}
	public void setOpen(Integer open) {
		this.open = open;
	}
	public Integer getReopen() {
		return reopen;
	}
	public void setReopen(Integer reopen) {
		this.reopen = reopen;
	}
	public Integer getClosed() {
		return Closed;
	}
	public void setClosed(Integer closed) {
		Closed = closed;
	}
	public Integer getResolved() {
		return Resolved;
	}
	public void setResolved(Integer resolved) {
		Resolved = resolved;
	}

}
