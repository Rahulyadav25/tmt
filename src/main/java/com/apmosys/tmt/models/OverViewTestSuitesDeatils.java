package com.apmosys.tmt.models;

public class OverViewTestSuitesDeatils {
	private Integer projectId;
	private String projectName;
	private Integer testSuitesId;
	private String testSuitesName;
	private Long testGroupNo;
	private Long testCaseNo;
//	private Integer testGroupNo;
//	private Integer testCaseNo;
	private String isActive;
	
	
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public Integer getTestSuitesId() {
		return testSuitesId;
	}
	public void setTestSuitesId(Integer testSuitesId) {
		this.testSuitesId = testSuitesId;
	}
	public String getTestSuitesName() {
		return testSuitesName;
	}
	public void setTestSuitesName(String testSuitesName) {
		this.testSuitesName = testSuitesName;
	}
	public Long getTestGroupNo() {
		return testGroupNo;
	}
	public void setTestGroupNo(Long testGroupNo) {
		this.testGroupNo = testGroupNo;
	}
	public Long getTestCaseNo() {
		return testCaseNo;
	}
	public void setTestCaseNo(Long testCaseNo) {
		this.testCaseNo = testCaseNo;
	}
//	public Integer getTestGroupNo() {
//		return testGroupNo;
//	}
//	public void setTestGroupNo(Integer testGroupNo) {
//		this.testGroupNo = testGroupNo;
//	}
//	public Integer getTestCaseNo() {
//		return testCaseNo;
//	}
//	public void setTestCaseNo(Integer testCaseNo) {
//		this.testCaseNo = testCaseNo;
//	}
	
}
