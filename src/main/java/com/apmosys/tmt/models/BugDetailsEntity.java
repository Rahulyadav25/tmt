package com.apmosys.tmt.models;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;

@Entity
@Table(name="BugDetails")
public class BugDetailsEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tmt_bug_id")
	private Integer tmtBugID;
	
	@Column(name = "bug_id")
	private String bugID;
	
	@Column(name = "bugtool_id")
	private Integer bugtoolID;

	@Column(name = "project_id")
	private Integer projectID;
	
	@Column(name = "tc_sr_id")
	private Integer tcSrID;
	
	@Column(name = "bug_summary")
	private String bugSummary;
	
	@Column(name = "bug_description")
	private String bugDescription;
	
	@Column(name = "bug_severity")
	private String bugSeverity;
	
	@Column(name = "bug_priority")
	private String bugPriority;
	
	@Column(name = "bug_status")
	private String bugStatus;
	
	@Column(name = "created_by")
	private Integer createdBy;
	
	@Column(name = "updated_by")
	private Integer updatedBy;
	
	@Column(name = "created_on")
	private Date createdOn;
	
	@Column(name = "updated_on")
	private Date updatedOn;
	
	public BugDetailsEntity(){
		
	}

	public Integer getTmtBugID() {
		return tmtBugID;
	}

	public void setTmtBugID(Integer tmtBugID) {
		this.tmtBugID = tmtBugID;
	}

	public String getBugID() {
		return bugID;
	}

	public void setBugID(String bugID) {
		this.bugID = bugID;
	}

	public Integer getBugtoolID() {
		return bugtoolID;
	}

	public void setBugtoolID(Integer bugtoolID) {
		this.bugtoolID = bugtoolID;
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

	public Integer getTcSrID() {
		return tcSrID;
	}

	public void setTcSrID(Integer tcSrID) {
		this.tcSrID = tcSrID;
	}

	public String getBugSummary() {
		return bugSummary;
	}

	public void setBugSummary(String bugSummary) {
		this.bugSummary = bugSummary;
	}

	public String getBugDescription() {
		return bugDescription;
	}

	public void setBugDescription(String bugDescription) {
		this.bugDescription = bugDescription;
	}

	public String getBugSeverity() {
		return bugSeverity;
	}

	public void setBugSeverity(String bugSeverity) {
		this.bugSeverity = bugSeverity;
	}

	public String getBugPriority() {
		return bugPriority;
	}

	public void setBugPriority(String bugPriority) {
		this.bugPriority = bugPriority;
	}

	public String getBugStatus() {
		return bugStatus;
	}

	public void setBugStatus(String bugStatus) {
		this.bugStatus = bugStatus;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	
}
