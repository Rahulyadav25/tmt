package com.apmosys.tmt.models;


import java.util.List;
import java.util.Map;



import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


public class OrderMethodExcelView extends AbstractXlsView {

//	@Override
	protected void buildExcelDocument(Map<String, Object> model, 
			Workbook workbook,
			HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		//extract data from model

//		@SuppressWarnings("unchecked")
//		List<OrderMethod> oml=(List<OrderMethod>) model.get("OrderMethodList");
//		Workbook workbook,
		//set name 
		res.addHeader("Content-Disposition","attachment;filename=OrderMethods.xlsx");
		//create sheet
		Sheet sheet=workbook.createSheet("orderMethodsList");
		//create 1st row as headings
		Row r1=sheet.createRow(0);
		r1.createCell(0).setCellValue("ID");
		r1.createCell(1).setCellValue("O M MODE");
		r1.createCell(2).setCellValue("O M CODE");
		r1.createCell(3).setCellValue("O M TYPE");
		r1.createCell(4).setCellValue("O M ACCEPT");
		r1.createCell(5).setCellValue("O M DESCRIPTION");
		//CREATE data rows
		Integer rowVal=1;
//		for(OrderMethod om : oml) {
			Row r=sheet.createRow(1);
			r.createCell(0).setCellValue("sadqwe");
			r.createCell(1).setCellValue("sadasd");
			r.createCell(2).setCellValue("sadsad");
			r.createCell(3).setCellValue("sadsad");

//			List<String> omAcc=om.getOrderMAccept();// 	ACCEPT   RETURN
//			String omAccAsString="";//   ""  "-ACCEPT"  "-ACCEPT-RETURN"
//			for(String a : omAcc) {
//				omAccAsString=omAccAsString+ "-"+ a;
//			}//for
			r.createCell(4).setCellValue("sad");
			r.createCell(5).setCellValue("sad");
//		}//for
	}//metod


}//class
