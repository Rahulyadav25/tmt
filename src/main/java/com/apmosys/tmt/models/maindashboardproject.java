package com.apmosys.tmt.models;

public class maindashboardproject {
private Integer totaltestsuites;
private String projectname;
private String projectid;
private String applicationName;
private Integer totalgroup;
private Integer totaltestcases;
private Integer totalRunTestcases;
private Integer totalrun;
private Integer totalActiverun;

private Integer totalinActiverun;
private Integer totalExecuted;
private Integer totalonhold;
private Integer totalunderreview;
@Override
public String toString() {
	return "maindashboardproject [totaltestsuites=" + totaltestsuites + ", projectname=" + projectname + ", projectid="
			+ projectid + ", applicationName=" + applicationName + ", totalgroup=" + totalgroup + ", totaltestcases="
			+ totaltestcases + ", totalRunTestcases=" + totalRunTestcases + ", totalrun=" + totalrun
			+ ", totalActiverun=" + totalActiverun + ", totalinActiverun=" + totalinActiverun + ", totalExecuted="
			+ totalExecuted + ", totalonhold=" + totalonhold + ", totalunderreview=" + totalunderreview
			+ ", totalcompleted=" + totalcompleted + ", totalnotstarted=" + totalnotstarted + ", totalNA=" + totalNA
			+ ", totalactionincomplete=" + totalactionincomplete + ", manhours=" + manhours + ", totalcurrentrunpass="
			+ totalcurrentrunpass + ", totalcurrentrunfail=" + totalcurrentrunfail + ", totalcurrentrunmajor="
			+ totalcurrentrunmajor + ", totalcurrentrunminor=" + totalcurrentrunminor + ", totalcurrentruncritical="
			+ totalcurrentruncritical + ", totalcurrentrunblocker=" + totalcurrentrunblocker + ", totalbugs="
			+ totalbugs + ", totaltime=" + totaltime + ", totalallotedresource=" + totalallotedresource
			+ ", successrate=" + successrate + ", totalruncompleted=" + totalruncompleted + ", openBug=" + openBug
			+ ", closeBug=" + closeBug + ", reOpenBug=" + reOpenBug + ", resolvedBug=" + resolvedBug + ", day=" + day
			+ ", hour=" + hour + ", minute=" + minute + "]";
}
private Integer totalcompleted;
private Integer totalnotstarted;
private Integer totalNA;
private Integer totalactionincomplete;


public String getProjectid() {
	return projectid;
}
public void setProjectid(String projectid) {
	this.projectid = projectid;
}
public String getApplicationName() {
	return applicationName;
}
public void setApplicationName(String applicationName) {
	this.applicationName = applicationName;
}
private String manhours;
public String getManhours() {
	return manhours;
}
public void setManhours(String manhours) {
	this.manhours = manhours;
}
public Integer getTotalinActiverun() {
	return totalinActiverun;
}
public void setTotalinActiverun(Integer totalinActiverun) {
	this.totalinActiverun = totalinActiverun;
}
public Integer getTotalactionincomplete() {
	return totalactionincomplete;
}
public void setTotalactionincomplete(Integer totalactionincomplete) {
	this.totalactionincomplete = totalactionincomplete;
}
private Integer totalcurrentrunpass;
private Integer totalcurrentrunfail;
private Integer totalcurrentrunmajor;
private Integer totalcurrentrunminor;
private Integer totalcurrentruncritical;
private Integer totalcurrentrunblocker;
private Integer totalbugs;
private Integer totaltime;
private Integer totalallotedresource;
private String successrate;
private Integer totalruncompleted;
private Integer openBug;
private Integer closeBug;
private Integer reOpenBug;
private Integer resolvedBug;
private Integer day;
private Integer hour;
private Integer minute;

public Integer getDay() {
	return day;
}
public void setDay(Integer day) {
	this.day = day;
}
public Integer getHour() {
	return hour;
}
public void setHour(Integer hour) {
	this.hour = hour;
}
public Integer getMinute() {
	return minute;
}
public void setMinute(Integer minute) {
	this.minute = minute;
}
public Integer getOpenBug() {
	return openBug;
}
public void setOpenBug(Integer openBug) {
	this.openBug = openBug;
}
public Integer getCloseBug() {
	return closeBug;
}

public Integer getTotalRunTestcases() {
	return totalRunTestcases;
}
public void setTotalRunTestcases(Integer totalRunTestcases) {
	this.totalRunTestcases = totalRunTestcases;
}
public void setCloseBug(Integer closeBug) {
	this.closeBug = closeBug;
}
public Integer getReOpenBug() {
	return reOpenBug;
}
public void setReOpenBug(Integer reOpenBug) {
	this.reOpenBug = reOpenBug;
}
public Integer getTotaltestsuites() {
	return totaltestsuites;
}
public void setTotaltestsuites(Integer totaltestsuites) {
	this.totaltestsuites = totaltestsuites;
}
public Integer getTotalgroup() {
	return totalgroup;
}
public void setTotalgroup(Integer totalgroup) {
	this.totalgroup = totalgroup;
}
public Integer getTotaltestcases() {
	return totaltestcases;
}
public void setTotaltestcases(Integer totaltestcases) {
	this.totaltestcases = totaltestcases;
}
public Integer getTotalrun() {
	return totalrun;
}
public void setTotalrun(Integer totalrun) {
	this.totalrun = totalrun;
}
public Integer getTotalActiverun() {
	return totalActiverun;
}
public void setTotalActiverun(Integer totalActiverun) {
	this.totalActiverun = totalActiverun;
}
public Integer getTotalExecuted() {
	return totalExecuted;
}
public void setTotalExecuted(Integer totalExecuted) {
	this.totalExecuted = totalExecuted;
}
public Integer getTotalonhold() {
	return totalonhold;
}
public void setTotalonhold(Integer totalonhold) {
	this.totalonhold = totalonhold;
}
public Integer getTotalunderreview() {
	return totalunderreview;
}
public void setTotalunderreview(Integer totalunderreview) {
	this.totalunderreview = totalunderreview;
}
public Integer getTotalcompleted() {
	return totalcompleted;
}
public void setTotalcompleted(Integer totalcompleted) {
	this.totalcompleted = totalcompleted;
}
public Integer getTotalnotstarted() {
	return totalnotstarted;
}
public void setTotalnotstarted(Integer totalnotstarted) {
	this.totalnotstarted = totalnotstarted;
}
public Integer getTotalNA() {
	return totalNA;
}
public void setTotalNA(Integer totalNA) {
	this.totalNA = totalNA;
}
public Integer getTotalcurrentrunpass() {
	return totalcurrentrunpass;
}
public void setTotalcurrentrunpass(Integer totalcurrentrunpass) {
	this.totalcurrentrunpass = totalcurrentrunpass;
}
public Integer getTotalcurrentrunfail() {
	return totalcurrentrunfail;
}
public void setTotalcurrentrunfail(Integer totalcurrentrunfail) {
	this.totalcurrentrunfail = totalcurrentrunfail;
}
public Integer getTotalcurrentrunmajor() {
	return totalcurrentrunmajor;
}
public void setTotalcurrentrunmajor(Integer totalcurrentrunmajor) {
	this.totalcurrentrunmajor = totalcurrentrunmajor;
}
public Integer getTotalcurrentrunminor() {
	return totalcurrentrunminor;
}
public void setTotalcurrentrunminor(Integer totalcurrentrunminor) {
	this.totalcurrentrunminor = totalcurrentrunminor;
}
public Integer getTotalcurrentruncritical() {
	return totalcurrentruncritical;
}
public void setTotalcurrentruncritical(Integer totalcurrentruncritical) {
	this.totalcurrentruncritical = totalcurrentruncritical;
}
public Integer getTotalcurrentrunblocker() {
	return totalcurrentrunblocker;
}
public void setTotalcurrentrunblocker(Integer totalcurrentrunblocker) {
	this.totalcurrentrunblocker = totalcurrentrunblocker;
}
public Integer getTotalbugs() {
	return totalbugs;
}
public void setTotalbugs(Integer totalbugs) {
	this.totalbugs = totalbugs;
}
public Integer getTotaltime() {
	return totaltime;
}
public void setTotaltime(Integer totaltime) {
	this.totaltime = totaltime;
}
public Integer getTotalallotedresource() {
	return totalallotedresource;
}
public void setTotalallotedresource(Integer getAllocationtester) {
	this.totalallotedresource = getAllocationtester;
}
public String getSuccessrate() {
	return successrate;
}
public void setSuccessrate(String successrate) {
	this.successrate = successrate;
}
public Integer getTotalruncompleted() {
	return totalruncompleted;
}
public void setTotalruncompleted(Integer totalruncompleted) {
	this.totalruncompleted = totalruncompleted;
}
public String getProjectname() {
	return projectname;
}
public void setProjectname(String projectname) {
	this.projectname = projectname;
}
public Integer getResolvedBug() {
	return resolvedBug;
}
public void setResolvedBug(Integer resolvedBug) {
	this.resolvedBug = resolvedBug;
}

}
