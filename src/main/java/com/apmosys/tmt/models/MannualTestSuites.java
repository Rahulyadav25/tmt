package com.apmosys.tmt.models;

import java.util.Date;

//import javax.persistence.Cacheable;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import jakarta.persistence.Cacheable;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@DynamicInsert
@DynamicUpdate
@Cacheable(false)
@Entity
@Table(name="Mannual_TestSuites_Table")
public class MannualTestSuites {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer testSuitesId;
	private Integer projectId;
	private String testSuitesName;
	private Date createdDate;
	private Date updatedDate;
	private String createdBy;
	private String updatedBy;
	private String isActive;
	private String isDeleted;
	@Transient
	private String checkForTestSuite;
	public Integer getTestSuitesId() {
		return testSuitesId;
	}
	public void setTestSuitesId(Integer testSuitesId) {
		this.testSuitesId = testSuitesId;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public String getTestSuitesName() {
		return testSuitesName;
	}
	public void setTestSuitesName(String testSuitesName) {
		this.testSuitesName = testSuitesName;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getCheckForTestSuite() {
		return checkForTestSuite;
	}
	public void setCheckForTestSuite(String checkForTestSuite) {
		this.checkForTestSuite = checkForTestSuite;
	}
	@Override
	public String toString() {
		return "MannualTestSuites [id=" + testSuitesId + ", projectId=" + projectId + ", testSuitesName=" + testSuitesName
				+ ", createdDate=" + createdDate + ", updatedDate=" + updatedDate + ", creadtedBy=" + createdBy
				+ ", updatedBy=" + updatedBy + ", isActivce=" + isActive + "]";
	}
	

}
