package com.apmosys.tmt.models;

import java.math.BigInteger;
import java.util.Date;

//import javax.persistence.Cacheable;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import jakarta.persistence.Cacheable;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@DynamicInsert
@DynamicUpdate
@Cacheable(false)
@Entity
@Table(name="MannualReRunTable")
public class MannualReRunTable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Integer projectId;
	private Integer testSuitesId;
	private Integer runId;
	private Integer round;
	private String runName;
	private String startDate;
	private String targetEndDate;
	private Date actualEndDate;
	private String status;
	private String isActive;
	private String createdBy;
	private String updatedBy;
	private Date createdDate;
	private Date updatedDate;
	private String description;
	@Transient
	private BigInteger testCaseNo;
	@Transient
	private String delayby;
	public String getDelayby() {
		return delayby;
	}
	public void setDelayby(String delayby) {
		this.delayby = delayby;
	}
	@Transient
	private BigInteger completed;
	@Transient
	private BigInteger onHold;
	@Transient
	private BigInteger underReview;
	@Transient
	private BigInteger totalExecuted;
	@Transient
	private BigInteger NotStarted;
	@Transient
	private BigInteger totalactionpass;
	@Transient
	private BigInteger totalimcomete;
	public BigInteger getTotalimcomete() {
		return totalimcomete;
	}
	public void setTotalimcomete(BigInteger totalimcomete) {
		this.totalimcomete = totalimcomete;
	}
	public BigInteger getTotalactionpass() {
		return totalactionpass;
	}
	public void setTotalactionpass(BigInteger totalactionpass) {
		this.totalactionpass = totalactionpass;
	}
	@Transient
	private BigInteger NA;
	@Transient
	private BigInteger Pass;
	@Transient
	private BigInteger Fail;
	@Transient
	private Double timeTake=0.0d;
	@Transient
	private String testSActiveStatus;
	@Transient
	private String timetakendhm;
	
	
	
	public String getTimetakendhm() {
		return timetakendhm;
	}
	public void setTimetakendhm(String timetakendhm) {
		this.timetakendhm = timetakendhm;
	}
	public String getTestSActiveStatus() {
		return testSActiveStatus;
	}
	public void setTestSActiveStatus(String testSActiveStatus) {
		this.testSActiveStatus = testSActiveStatus;
	}
	public BigInteger getNA() {
		return NA;
	}
	public void setNA(BigInteger nA) {
		NA = nA;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public Integer getTestSuitesId() {
		return testSuitesId;
	}
	public void setTestSuitesId(Integer testSuitesId) {
		this.testSuitesId = testSuitesId;
	}
	public String getRunName() {
		return runName;
	}
	@Transient
	private String rerunname;
	public String getRerunname() {
		return rerunname;
	}
	public void setRerunname(String rerunname) {
		this.rerunname = rerunname;
	}
	public void setRunName(String runName) {
		this.runName = runName;
	}
	
	public Integer getRunId() {
		return runId;
	}
	public void setRunId(Integer runId) {
		this.runId = runId;
	}
	public Integer getRound() {
		return round;
	}
	public void setRound(Integer round) {
		this.round = round;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getTargetEndDate() {
		return targetEndDate;
	}
	public void setTargetEndDate(String targetEndDate) {
		this.targetEndDate = targetEndDate;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	
	public Date getActualEndDate() {
		return actualEndDate;
	}
	public void setActualEndDate(Date actualEndDate) {
		this.actualEndDate = actualEndDate;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public BigInteger getTestCaseNo() {
		return testCaseNo;
	}
	public void setTestCaseNo(BigInteger testCaseNo) {
		this.testCaseNo = testCaseNo;
	}
	
	public BigInteger getPass() {
		return Pass;
	}
	public void setPass(BigInteger pass) {
		Pass = pass;
	}
	public BigInteger getFail() {
		return Fail;
	}
	public void setFail(BigInteger fail) {
		Fail = fail;
	}
	public BigInteger getCompleted() {
		return completed;
	}
	public void setCompleted(BigInteger completed) {
		this.completed = completed;
	}
	public BigInteger getOnHold() {
		return onHold;
	}
	public void setOnHold(BigInteger onHold) {
		this.onHold = onHold;
	}
	public BigInteger getUnderReview() {
		return underReview;
	}
	public void setUnderReview(BigInteger underReview) {
		this.underReview = underReview;
	}
	public BigInteger getTotalExecuted() {
		return totalExecuted;
	}
	public void setTotalExecuted(BigInteger totalExecuted) {
		this.totalExecuted = totalExecuted;
	}
	public BigInteger getNotStarted() {
		return NotStarted;
	}
	public void setNotStarted(BigInteger notStarted) {
		NotStarted = notStarted;
	}
	
	public Double getTimeTake() {
		return timeTake;
	}
	public void setTimeTake(Double timeTake) {
		this.timeTake = timeTake;
	}
	
	@Override
	public String toString() {
		return "MannualRunDetails [id=" + id + ", projectId=" + projectId + ", testSuitesId=" + testSuitesId
				+ ", runName=" + runName + ", startDate=" + startDate + ", targetEndDate=" + targetEndDate
				+ ", actualEndDate=" + actualEndDate + ", status=" + status + ", isActive=" + isActive + ", createdBy="
				+ createdBy + ", updatedBy=" + updatedBy + ", createdDate=" + createdDate + ", updatedDate="
				+ updatedDate + ", description=" + description + ", testCaseNo=" + testCaseNo + ", completed="
				+ completed + ", onHold=" + onHold + ", underReview=" + underReview + ", totalExecuted=" + totalExecuted
				+ ", NotStarted=" + NotStarted + "]";
	}
	
	
	
}
