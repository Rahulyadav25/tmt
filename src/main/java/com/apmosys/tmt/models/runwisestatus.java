package com.apmosys.tmt.models;

public class runwisestatus {
private Integer runid;
private Integer totalcases;
private String runname;
private Integer totalcompleted;
private Integer totalonhold;
private Integer totalNe;
private String roundrunname;
private Integer round;
private Integer totalNA;
private Integer rerunId;
private Integer incomplete;
public Integer getIncomplete() {
	return incomplete;
}
public void setIncomplete(Integer incomplete) {
	this.incomplete = incomplete;
}
public Integer getRerunId() {
	return rerunId;
}
public void setRerunId(Integer rerunId) {
	this.rerunId = rerunId;
}
public Integer getTotalNA() {
	return totalNA;
}
public void setTotalNA(Integer totalNA) {
	this.totalNA = totalNA;
}
public String getRoundrunname() {
	return roundrunname;
}
public void setRoundrunname(String roundrunname) {
	this.roundrunname = roundrunname;
}
public Integer getRound() {
	return round;
}
public void setRound(Integer round) {
	this.round = round;
}
public Integer getTotalNe() {
	return totalNe;
}
public void setTotalNe(Integer totalNe) {
	this.totalNe = totalNe;
}
public Integer getTotalcompleted() {
	return totalcompleted;
}
public void setTotalcompleted(Integer totalcompleted) {
	this.totalcompleted = totalcompleted;
}
public Integer getTotalonhold() {
	return totalonhold;
}
public void setTotalonhold(Integer totalonhold) {
	this.totalonhold = totalonhold;
}
public Integer getTotalunderreview() {
	return totalunderreview;
}
public void setTotalunderreview(Integer totalunderreview) {
	this.totalunderreview = totalunderreview;
}
public Integer getTotalnotstarted() {
	return totalnotstarted;
}
public void setTotalnotstarted(Integer totalnotstarted) {
	this.totalnotstarted = totalnotstarted;
}
private Integer totalunderreview;
private Integer totalexecuted;
private Integer totalnotstarted;
public Integer getTotalexecuted() {
	return totalexecuted;
}
public void setTotalexecuted(Integer totalexecuted) {
	this.totalexecuted = totalexecuted;
}
private float totalmanhours;

public Integer getRunid() {
	return runid;
}
public void setRunid(Integer runid) {
	this.runid = runid;
}
public Integer getTotalcases() {
	return totalcases;
}
public void setTotalcases(Integer totalcases) {
	this.totalcases = totalcases;
}
public String getRunname() {
	return runname;
}
public void setRunname(String runname) {
	this.runname = runname;
}

public float getTotalmanhours() {
	return totalmanhours;
}
public void setTotalmanhours(float totalmanhours) {
	this.totalmanhours = totalmanhours;
}

}
