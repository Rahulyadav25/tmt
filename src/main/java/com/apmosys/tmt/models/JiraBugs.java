package com.apmosys.tmt.models;

/**
 * @author Animesh
 *
 */
public class JiraBugs {
	
	private String bugId;
	private String status;
	private String priority;
	private String bugType;
	private String summary;
	private String desc;
	private String created;
	
	
	public String getBugId() {
		return bugId;
	}
	public void setBugId(String bugId) {
		this.bugId = bugId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getBugType() {
		return bugType;
	}
	public void setBugType(String bugType) {
		this.bugType = bugType;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	
	@Override
	public String toString() {
		return "JiraBugs [bugId=" + bugId + ", status=" + status + ", priority=" + priority + ", bugType=" + bugType
				+ ", summary=" + summary + ", desc=" + desc + ", created=" + created + "]";
	}
	
	

}
