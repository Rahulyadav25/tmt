package com.apmosys.tmt.models;

import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

//import javax.persistence.Cacheable;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.persistence.Transient;
@Cacheable(false)
@Entity
/*@Table(name="TMTTools.ProjectAccessTable")*/
@Table(name="ProjectAccessTable")
public class ProjectAccessEntity {

	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "srNo")
	public Integer srNo;
	
	@Column(name = "projectID")
	private Integer projectID;
	@Column(name = "userDetailsId")
	private Integer userDetailsId;
	@Column(name = "AccessType")
	private String accesstype;
	
	@Column(name = "Access_Given_By")
	private String AccessGivenBy;

	@Column(name = "Access_Date")
	private String accessDate;
	
	@Transient
	private Integer userrole;
		
	public Integer getUserDetailsId() {
		return userDetailsId;
	}
	public void setUserDetailsId(Integer userDetailsId) {
		this.userDetailsId = userDetailsId;
	}
	public Integer getUserrole() {
		return userrole;
	}
	public void setUserrole(Integer userrole) {
		this.userrole = userrole;
	}
	public String getAccessGivenBy() {
		return AccessGivenBy;
	}
	public void setAccessGivenBy(String accessGivenBy) {
		AccessGivenBy = accessGivenBy;
	}
	public String getAccesstype() {
		return accesstype;
	}
	public Integer getSrNo() {
		return srNo;
	}
	public void setSrNo(Integer srNo) {
		this.srNo = srNo;
	}
	public void setAccesstype(String string) {
		this.accesstype = string;
	}
	public String getAccessDate() {
		return accessDate;
	}
	public void setAccessDate(String accessDate) {
		this.accessDate = accessDate;
	}
	/*public Integer getSrNo() {
		return srNo;
	}
	public void setSrNo(Integer srNo) {
		this.srNo = srNo;
	}*/
	public Integer getProjectID() {
		return projectID;
	}
	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}
	public Integer getUserID() {
		return userDetailsId;
	}
	public void setUserID(Integer userID) {
		this.userDetailsId = userID;
	}
	
	
}
