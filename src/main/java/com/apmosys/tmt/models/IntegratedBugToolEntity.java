package com.apmosys.tmt.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
@Entity
@Table(name="IntegratedBugTool")

public class IntegratedBugToolEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer toolid;
	
	@Column(name = "bug_tool_name")
	private String bugToolName;
	
	public IntegratedBugToolEntity() {
		
	}

	public Integer getToolid() {
		return toolid;
	}

	public void setToolid(Integer toolid) {
		this.toolid = toolid;
	}

	public String getBugToolName() {
		return bugToolName;
	}

	public void setBugToolName(String bugToolName) {
		this.bugToolName = bugToolName;
	}
	
}
