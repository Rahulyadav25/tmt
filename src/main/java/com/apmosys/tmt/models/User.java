package com.apmosys.tmt.models;

import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
@Cacheable(false)
@Entity
@Table(name="app_user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "first_name")
	private String first_name;
	
	@Column(name = "last_name")
	private String last_name;
	


	@Column(name = "email")
	private String email;
	
	
	@Column(name="password")
	private String password;

	@Column(name="full_name")
	private String full_name;
	
	@Column(name="last_login")
	private String last_login;

	@Column(name="operator_id")
	private String operator_id;
	
	@Column(name="designation")
	private String designation;
	
	@Column(name="department_name")
	private String department_name;
	
	@Column(name="company_name")
	private String company_name;
	
	@Column(name="country")
	private String country;

	@Column(name="phone_number")
	private String phone_number;
	
	@Column(name="created_by")
	private String created_by;
	
	
	@Column(name="user_id")
	private String user_id;
	
	
	
	public String getFirst_name() {
		return first_name;
	}




	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}




	public String getLast_name() {
		return last_name;
	}




	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}




	public String getUser_id() {
		return user_id;
	}




	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}




	public String getCreated_by() {
		return created_by;
	}




	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}




	public String getOperator_id() {
		return operator_id;
	}




	public void setOperator_id(String operator_id) {
		this.operator_id = operator_id;
	}




	public String getDesignation() {
		return designation;
	}




	public void setDesignation(String designation) {
		this.designation = designation;
	}




	public String getDepartment_name() {
		return department_name;
	}




	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}




	public String getCompany_name() {
		return company_name;
	}




	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}




	public String getCountry() {
		return country;
	}




	public void setCountry(String country) {
		this.country = country;
	}




	public String getPhone_number() {
		return phone_number;
	}




	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}




	public String getManager() {
		return manager;
	}




	public void setManager(String manager) {
		this.manager = manager;
	}




	public String getCreated_on() {
		return created_on;
	}




	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}




	public String getContact_name() {
		return contact_name;
	}




	public void setContact_name(String contact_name) {
		this.contact_name = contact_name;
	}




	@Column(name="manager")
	private String manager;
	
	@Column(name="created_on")
	private String created_on;
	
	@Column(name="contact_name")
	private String contact_name;

	


	

	



	public User(){
		
	}




	public Integer getId() {
		return id;
	}




	public void setId(Integer id) {
		this.id = id;
	}



	public String getEmail() {
		
		return email;
	}




	public void setEmail(String email) {
		this.email = email;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}


	public String getFull_name() {
		return full_name;
	}




	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}
	
	public String getLast_login() {
		return last_login;
	}




	public void setLast_login(String last_login) {
		this.last_login = last_login;
	}


	/*public LocalDateTime getUpdateDateTime() {
		return updateDateTime;
	}




	public void setUpdateDateTime(LocalDateTime updateDateTime) {
		this.updateDateTime = updateDateTime;
	}*/

	
	
}
