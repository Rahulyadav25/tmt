package com.apmosys.tmt.models;

public class defectseverity {
	private Integer Critical;
	private Integer Blocker;

	private Integer Major;

	private Integer Minor;

	public Integer getCritical() {
		return Critical;
	}

	public void setCritical(Integer critical) {
		Critical = critical;
	}

	public Integer getBlocker() {
		return Blocker;
	}

	public void setBlocker(Integer blocker) {
		Blocker = blocker;
	}

	public Integer getMajor() {
		return Major;
	}

	public void setMajor(Integer major) {
		Major = major;
	}

	public Integer getMinor() {
		return Minor;
	}

	public void setMinor(Integer minor) {
		Minor = minor;
	}
	
	
	

	

}
