package com.apmosys.tmt.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.persistence.Transient;

@Entity
@Table(name = "logs")
public class AutomationRunLogs {
	@Id
	private Integer id;
	@Column(name="RUN_ID")
	private Integer runId;
	@Column(name="project_name")
	private String project_name;
	@Column(name="Application_Name")
	private String applicationName;
	@Column(name="Process")
	private String process;
	@Column(name="ScenarioID")
	private String scenarioID;
	@Column(name="TestCaseID")
	private String testCaseID;
	@Column(name="Response_Time")
	private String responseTime;
	@Column(name="Expected_Result")
	private String expectedResult;
	@Column(name="Actual_Result")
	private String actualResult;
	@Column(name="status")
	private String status;
	@Column(name="Timestamp")
	private String timestamp;
	@Column(name="Description")
	private String description;
	@Column(name="BrowserType")
	private String browserType;
	@Column(name="ScreenshotID")
	private String screenshotID;
	//pranik 
	@Column(name="TestcaseType")
	private String TestcaseType;
	@Column(name="Step")
	private String Step;
	@Transient
	private String screenshotName;
	
	@Transient
	private byte[] screenshotImg;
//changed
	@Column(name="IP_HOST_ZONE")
	private String ip_host_zone;
	@Column(name="scenario_description")
	private String scenario_description;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getRunId() {
		return runId;
	}
	public void setRunId(Integer runId) {
		this.runId = runId;
	}
	public String getProject_name() {
		return project_name;
	}
	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	
	public String getProcess() {
		return process;
	}
	public void setProcess(String process) {
		this.process = process;
	}
	public String getScenarioID() {
		return scenarioID;
	}
	public void setScenarioID(String scenarioID) {
		this.scenarioID = scenarioID;
	}
	public String getTestCaseID() {
		return testCaseID;
	}
	public void setTestCaseID(String testCaseID) {
		this.testCaseID = testCaseID;
	}
	public String getResponseTime() {
		return responseTime;
	}
	public void setResponseTime(String responseTime) {
		this.responseTime = responseTime;
	}
	public String getExpectedResult() {
		return expectedResult;
	}
	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}
	public String getActualResult() {
		return actualResult;
	}
	public void setActualResult(String actualResult) {
		this.actualResult = actualResult;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBrowserType() {
		return browserType;
	}
	public void setBrowserType(String browserType) {
		this.browserType = browserType;
	}
	public String getScreenshotID() {
		return screenshotID;
	}
	public void setScreenshotID(String screenshotID) {
		this.screenshotID = screenshotID;
	}
	public String getScreenshotName() {
		return screenshotName;
	}
	public void setScreenshotName(String screenshotName) {
		this.screenshotName = screenshotName;
	}
	public byte[] getScreenshotImg() {
		return screenshotImg;
	}
	public void setScreenshotImg(byte[] screenshotImg) {
		this.screenshotImg = screenshotImg;
	}
	public String getIp_host_zone() {
		return ip_host_zone;
	}
	public void setIp_host_zone(String ip_host_zone) {
		this.ip_host_zone = ip_host_zone;
	}
	public String getScenario_description() {
		return scenario_description;
	}
	public void setScenario_description(String scenario_description) {
		this.scenario_description = scenario_description;
	}
	public String getTestcaseType() {
		return TestcaseType;
	}
	public void setTestcaseType(String testcaseType) {
		TestcaseType = testcaseType;
	}
	public String getStep() {
		return Step;
	}
	public void setStep(String step) {
		Step = step;
	}
	
}