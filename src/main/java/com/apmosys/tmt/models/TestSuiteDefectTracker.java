package com.apmosys.tmt.models;

public class TestSuiteDefectTracker {
	
	private String projectId;
	private String runId;
	private String testcaseId;
	private String defectId;
	private String severity;
	private String Status;
	
	private Integer totalOpen;
	private Integer totalReOpen;
	private Integer totalClosed;
	private Integer totalBlocker;
	private Integer totalMinor;
	private Integer totalMajor;
	private Integer totalCritical;
	
	public Integer getTotalOpen() {
		return totalOpen;
	}
	public void setTotalOpen(Integer totalOpen) {
		this.totalOpen = totalOpen;
	}
	public Integer getTotalReOpen() {
		return totalReOpen;
	}
	public void setTotalReOpen(Integer totalReOpen) {
		this.totalReOpen = totalReOpen;
	}
	public Integer getTotalClosed() {
		return totalClosed;
	}
	public void setTotalClosed(Integer totalClosed) {
		this.totalClosed = totalClosed;
	}
	public Integer getTotalBlocker() {
		return totalBlocker;
	}
	public void setTotalBlocker(Integer totalBlocker) {
		this.totalBlocker = totalBlocker;
	}
	public Integer getTotalMinor() {
		return totalMinor;
	}
	public void setTotalMinor(Integer totalMinor) {
		this.totalMinor = totalMinor;
	}
	public Integer getTotalMajor() {
		return totalMajor;
	}
	public void setTotalMajor(Integer totalMajor) {
		this.totalMajor = totalMajor;
	}
	public Integer getTotalCritical() {
		return totalCritical;
	}
	public void setTotalCritical(Integer totalCritical) {
		this.totalCritical = totalCritical;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getRunId() {
		return runId;
	}
	public void setRunId(String runId) {
		this.runId = runId;
	}
	public String getTestcaseId() {
		return testcaseId;
	}
	public void setTestcaseId(String testcaseId) {
		this.testcaseId = testcaseId;
	}
	public String getDefectId() {
		return defectId;
	}
	public void setDefectId(String defectId) {
		this.defectId = defectId;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	
	
	
	

}
