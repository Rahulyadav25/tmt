package com.apmosys.tmt.models;

import java.util.List;

import jakarta.persistence.Cacheable;

//import javax.persistence.Cacheable;

@Cacheable(false)
public class ProjectEntitlement {
	
	 private String projectId;
	 private String projectName;
	 private String isMannual;
	 private String isAutomation;
	 private String isActive;
	
	public String getProjectaccessid() {
		return projectaccessid;
	}
	public void setProjectaccessid(String projectaccessid) {
		this.projectaccessid = projectaccessid;
	}
	private String projectManager;
	 private String projectaccessid;
	 private List<ProjectEntitlement> userlist;

	 public List<ProjectEntitlement> getUserlist() {
		return userlist;
	}
	public void setUserlist(List<ProjectEntitlement> userlist) {
		this.userlist = userlist;
	}
	public String getIsAutomation() {
		return isAutomation;
	}
	public void setIsAutomation(String isAutomation) {
		this.isAutomation = isAutomation;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getProjectManager() {
		return projectManager;
	}
	public void setProjectManager(String projectManager) {
		this.projectManager = projectManager;
	}
	private String applicationName;
	 private String projectStartDate;
	 private String projectEndDate;
	 private String accessProject;
	 private String accessCreatedBy;
	 
	private String userid;
		private Integer id;
		private String name;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		private String username;
		private Integer createdRoleID;
		private String createdDate;
		private String userroleAdmin;	
		private String userCreatedBy;
	    private String userrole;
	    private String isactive;
	    private String password;
	    private String creatorid;
	    private String roleType;
	  
	    private String AccessType;
	  
	    
	    public String getAccessCreatedBy() {
			return accessCreatedBy;
		}
		public void setAccessCreatedBy(String accessCreatedBy) {
			this.accessCreatedBy = accessCreatedBy;
		}
	public String getUserid() {
			return userid;
		}
		public void setUserid(String userid) {
			this.userid = userid;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public Integer getCreatedRoleID() {
			return createdRoleID;
		}
		public void setCreatedRoleID(Integer createdRoleID) {
			this.createdRoleID = createdRoleID;
		}
		public String getCreatedDate() {
			return createdDate;
		}
		public void setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
		}
		public String getUserroleAdmin() {
			return userroleAdmin;
		}
		public void setUserroleAdmin(String userroleAdmin) {
			this.userroleAdmin = userroleAdmin;
		}
		public String getUserCreatedBy() {
			return userCreatedBy;
		}
		public void setUserCreatedBy(String userCreatedBy) {
			this.userCreatedBy = userCreatedBy;
		}
		public String getUserrole() {
			return userrole;
		}
		public void setUserrole(String userrole) {
			this.userrole = userrole;
		}
		public String getIsactive() {
			return isactive;
		}
		public void setIsactive(String isactive) {
			this.isactive = isactive;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getCreatorid() {
			return creatorid;
		}
		public void setCreatorid(String creatorid) {
			this.creatorid = creatorid;
		}
		public String getRoleType() {
			return roleType;
		}
		public void setRoleType(String roleType) {
			this.roleType = roleType;
		}
		public String getAccessType() {
			return AccessType;
		}
		public void setAccessType(String accessType) {
			AccessType = accessType;
		}
	public String getAccessProject() {
		return accessProject;
	}
	public void setAccessProject(String accessProject) {
		this.accessProject = accessProject;
	}
	public String getProjectEndDate() {
		return projectEndDate;
	}
	public void setProjectEndDate(String projectEndDate) {
		this.projectEndDate = projectEndDate;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getIsMannual() {
		return isMannual;
	}
	public void setIsMannual(String isMannual) {
		this.isMannual = isMannual;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getProjectStartDate() {
		return projectStartDate;
	}
	public void setProjectStartDate(String projectStartDate) {
		this.projectStartDate = projectStartDate;
	}
	 

}
