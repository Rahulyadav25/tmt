package com.apmosys.tmt.models;

//import javax.persistence.Cacheable;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.persistence.Transient;


import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@DynamicInsert
@DynamicUpdate
@Cacheable(false)
@Entity
@Table(name = "Run_history")
public class AutomationRunTable {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name="RUN_ID")
private Integer runId;
private String project_name;
@Column(name="applicationID")
private String applicationID;
private String status;
private String start_time;
private String end_time;
private String total_time;
@Column(name="PID")
private Integer pId;
@Transient
private String passedoutvalue;
@Transient
private Integer passousecount;
@Transient
private Integer totalcount;

public Integer getPassousecount() {
	return passousecount;
}
public void setPassousecount(Integer passousecount) {
	this.passousecount = passousecount;
}
public Integer getTotalcount() {
	return totalcount;
}
public void setTotalcount(Integer totalcount) {
	this.totalcount = totalcount;
}
public Integer getRunId() {
	return runId;
}
public String getPassedoutvalue() {
	return passedoutvalue;
}
public void setPassedoutvalue(String passedoutvalue) {
	this.passedoutvalue = passedoutvalue;
}
public void setRunId(Integer runId) {
	this.runId = runId;
}
public String getProject_name() {
	return project_name;
}
public void setProject_name(String project_name) {
	this.project_name = project_name;
}
public String getApplicationID() {
	return applicationID;
}
public void setApplicationID(String applicationID) {
	this.applicationID = applicationID;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getStart_time() {
	return start_time;
}
public void setStart_time(String start_time) {
	this.start_time = start_time;
}
public String getEnd_time() {
	return end_time;
}
public void setEnd_time(String end_time) {
	this.end_time = end_time;
}
public String getTotal_time() {
	return total_time;
}
public void setTotal_time(String total_time) {
	this.total_time = total_time;
}
public Integer getpId() {
	return pId;
}
public void setpId(Integer pId) {
	this.pId = pId;
}

}
