package com.apmosys.tmt.models;

import java.util.Date;

/*import java.sql.Date;*/

public class DefectsDeatils {
	private String runName;
	private Date created_on;
	private String bug_id;
	private String bug_status;
	private String bug_severity;
	private String bugage;
	
	public String getRunName() {
		return runName;
	}
	public void setRunName(String runName) {
		this.runName = runName;
	}
	public Date getCreated_on() {
		return created_on;
	}
	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}
	public String getBug_id() {
		return bug_id;
	}
	public void setBug_id(String bug_id) {
		this.bug_id = bug_id;
	}
	public String getBug_status() {
		return bug_status;
	}
	public void setBug_status(String bug_status) {
		this.bug_status = bug_status;
	}
	public String getBug_severity() {
		return bug_severity;
	}
	public void setBug_severity(String bug_severity) {
		this.bug_severity = bug_severity;
	}
	public String getBugage() {
		return bugage;
	}
	public void setBugage(String bugage) {
		this.bugage = bugage;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getBug_summary() {
		return bug_summary;
	}
	public void setBug_summary(String bug_summary) {
		this.bug_summary = bug_summary;
	}
	
	private String module;
	private String bug_summary;
	
	private String createdDate;
	private String bugId;
	private String bugStatus;
	private String bugSeverity;
	private String createdBy;

	
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getBugId() {
		return bugId;
	}
	public void setBugId(String bugId) {
		this.bugId = bugId;
	}
	public String getBugStatus() {
		return bugStatus;
	}
	public void setBugStatus(String bugStatus) {
		this.bugStatus = bugStatus;
	}
	public String getBugSeverity() {
		return bugSeverity;
	}
	public void setBugSeverity(String bugSeverity) {
		this.bugSeverity = bugSeverity;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	
	
	
}