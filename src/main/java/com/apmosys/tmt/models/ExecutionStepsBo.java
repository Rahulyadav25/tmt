package com.apmosys.tmt.models;

//import javax.persistence.Cacheable;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@DynamicInsert
@DynamicUpdate
@Cacheable(false)
@Entity
@Table(name = "executionsteps")
public class ExecutionStepsBo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "RUN_ID")
	private String runId;
	@Column(name = "StepsNo")
	private String stepNo;
	@Column(name = "ScenarioID")
	private String scenarioId;
	@Column(name = "TestCaseID")
	private String testCaseId;
	@Column(name = "module")
	private String module;
	@Column(name = "datafield")
	private String dataField;
	@Column(name = "testdata")
	private String testData;
	@Column(name = "timestamp")
	private String timeStamp;
	@Column(name = "Control")
	private String control;
	@Column(name = "logs_id")
	private String logsId;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRunId() {
		return runId;
	}
	public void setRunId(String runId) {
		this.runId = runId;
	}
	public String getStepNo() {
		return stepNo;
	}
	public void setStepNo(String stepNo) {
		this.stepNo = stepNo;
	}
	public String getScenarioId() {
		return scenarioId;
	}
	public void setScenarioId(String scenarioId) {
		this.scenarioId = scenarioId;
	}
	public String getTestCaseId() {
		return testCaseId;
	}
	public void setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getDataField() {
		return dataField;
	}
	public void setDataField(String dataField) {
		this.dataField = dataField;
	}
	public String getTestData() {
		return testData;
	}
	public void setTestData(String testData) {
		this.testData = testData;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getControl() {
		return control;
	}
	public void setControl(String control) {
		this.control = control;
	}
	public String getLogsId() {
		return logsId;
	}
	public void setLogsId(String logsId) {
		this.logsId = logsId;
	}
	@Override
	public String toString() {
		return "ExecutionStepsBo [id=" + id + ", runId=" + runId + ", stepNo=" + stepNo + ", scenarioId=" + scenarioId
				+ ", testCaseId=" + testCaseId + ", module=" + module + ", dataField=" + dataField + ", testData="
				+ testData + ", timeStamp=" + timeStamp + ", control=" + control + ", logsId=" + logsId + "]";
	}
	

}
