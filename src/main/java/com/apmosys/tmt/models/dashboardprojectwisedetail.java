package com.apmosys.tmt.models;

public class dashboardprojectwisedetail {
	private String projectid;
	private String projectname;
	private String applicationName;
	private String setTotalmanhours;	
	private Integer totalExecuted=0;
	private Integer totalonhold=0;
	private Integer totalpass=0;
	private Integer totalfail=0;
	private Integer totalunderreview=0;
	private Integer totalcompleted=0;
	private Integer totalnotstarted=0;
	private Integer totalNA=0;
	private Integer totalcurrentrunmajor=0;
	private Integer totalcurrentrunminor=0;
	private Integer totalcurrentruncritical=0;
	private Integer totalcurrentrunblocker=0;
	private Integer totalbugs=0;
	private Integer totaltime=0;
	private Integer totalallotedresource=0;
	private Integer successrate=0;;
	private Integer totalruncompleted;
	private Integer openBug=0;
	private Integer closeBug=0;
	private Integer reOpenBug=0;
	private Integer resolvedBug=0;
	private Integer dayspend=0;
	private Integer totaltestcases;
	private Integer hourspend=0;
	private Integer minutespend=0;
//	private Integer totalactionpass=0;
	private Integer totalactionincomplete=0;

	public String getSetTotalmanhours() {
		return setTotalmanhours;
	}
	public void setSetTotalmanhours(String setTotalmanhours) {
		this.setTotalmanhours = setTotalmanhours;
	}
	public String getProjectid() {
		return projectid;
	}
	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public Integer getTotaltestcases() {
		return totaltestcases;
	}
	public void setTotaltestcases(Integer totaltestcases) {
		this.totaltestcases = totaltestcases;
	}
	public Integer getTotalpass() {
		return totalpass;
	}
	public void setTotalpass(Integer totalpass) {
		this.totalpass = totalpass;
	}
	public Integer getTotalfail() {
		return totalfail;
	}
	public void setTotalfail(Integer totalfail) {
		this.totalfail = totalfail;
	}

	public Integer getTotalactionincomplete() {
		return totalactionincomplete;
	}
	public void setTotalactionincomplete(Integer totalactionincomplete) {
		this.totalactionincomplete = totalactionincomplete;
	}
	public Integer getHourspend() {
		return hourspend;
	}
	public void setHourspend(Integer hourspend) {
		this.hourspend = hourspend;
	}
	public Integer getDayspend() {
		return dayspend;
	}
	public void setDayspend(Integer dayspend) {
		this.dayspend = dayspend;
	}

	public Integer getMinutespend() {
		return minutespend;
	}
	public void setMinutespend(Integer minutespend) {
		this.minutespend = minutespend;
	}
	public Integer getTotalExecuted() {
		return totalExecuted;
	}
	public void setTotalExecuted(Integer totalExecuted) {
		this.totalExecuted = totalExecuted;
	}
	public Integer getTotalonhold() {
		return totalonhold;
	}
	public void setTotalonhold(Integer totalonhold) {
		this.totalonhold = totalonhold;
	}
	public Integer getTotalunderreview() {
		return totalunderreview;
	}
	public void setTotalunderreview(Integer totalunderreview) {
		this.totalunderreview = totalunderreview;
	}
	public Integer getTotalcompleted() {
		return totalcompleted;
	}
	public void setTotalcompleted(Integer totalcompleted) {
		this.totalcompleted = totalcompleted;
	}
	public Integer getTotalnotstarted() {
		return totalnotstarted;
	}
	public void setTotalnotstarted(Integer totalnotstarted) {
		this.totalnotstarted = totalnotstarted;
	}
	public Integer getTotalNA() {
		return totalNA;
	}
	public void setTotalNA(Integer totalNA) {
		this.totalNA = totalNA;
	}
	
	public Integer getTotalcurrentrunmajor() {
		return totalcurrentrunmajor;
	}
	public void setTotalcurrentrunmajor(Integer totalcurrentrunmajor) {
		this.totalcurrentrunmajor = totalcurrentrunmajor;
	}
	public Integer getTotalcurrentrunminor() {
		return totalcurrentrunminor;
	}
	public void setTotalcurrentrunminor(Integer totalcurrentrunminor) {
		this.totalcurrentrunminor = totalcurrentrunminor;
	}
	public Integer getTotalcurrentruncritical() {
		return totalcurrentruncritical;
	}
	public void setTotalcurrentruncritical(Integer totalcurrentruncritical) {
		this.totalcurrentruncritical = totalcurrentruncritical;
	}
	public Integer getTotalcurrentrunblocker() {
		return totalcurrentrunblocker;
	}
	public void setTotalcurrentrunblocker(Integer totalcurrentrunblocker) {
		this.totalcurrentrunblocker = totalcurrentrunblocker;
	}
	public Integer getTotalbugs() {
		return totalbugs;
	}
	public void setTotalbugs(Integer totalbugs) {
		this.totalbugs = totalbugs;
	}
	public Integer getTotaltime() {
		return totaltime;
	}
	public void setTotaltime(Integer totaltime) {
		this.totaltime = totaltime;
	}
	public Integer getTotalallotedresource() {
		return totalallotedresource;
	}
	public void setTotalallotedresource(Integer totalallotedresource) {
		this.totalallotedresource = totalallotedresource;
	}
	public Integer getSuccessrate() {
		return successrate;
	}
	public void setSuccessrate(Integer successrate) {
		this.successrate = successrate;
	}
	public Integer getTotalruncompleted() {
		return totalruncompleted;
	}
	public void setTotalruncompleted(Integer totalruncompleted) {
		this.totalruncompleted = totalruncompleted;
	}
	/*public void setSetTotalmanhours(Float valueOf) {
		this.setTotalmanhours = valueOf;
		
	}*/
	public Integer getOpenBug() {
		return openBug;
	}
	public void setOpenBug(Integer openBug) {
		this.openBug = openBug;
	}
	public Integer getCloseBug() {
		return closeBug;
	}
	public void setCloseBug(Integer closeBug) {
		this.closeBug = closeBug;
	}
	public Integer getReOpenBug() {
		return reOpenBug;
	}
	public void setReOpenBug(Integer reOpenBug) {
		this.reOpenBug = reOpenBug;
	}
	public Integer getResolvedBug() {
		return resolvedBug;
	}
	public void setResolvedBug(Integer resolvedBug) {
		this.resolvedBug = resolvedBug;
	}
	

}
