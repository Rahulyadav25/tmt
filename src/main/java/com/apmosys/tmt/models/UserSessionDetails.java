package com.apmosys.tmt.models;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;

@Entity
@Table(name = "User_session_details")
public class UserSessionDetails {
	@Id
	@Column(name = "user_id")
	private String userId;

	@Column(name = "session_key")
	private String session_key;

	@Column(name = "active_status")
	private String active_status;

	@Column(name = "last_check")
	private Date last_check;

	@Override
	public String toString() {
		return "UserSessionDetails [userId=" + userId + ", session_key=" + session_key + ", active_status="
				+ active_status + ", last_check=" + last_check + "]";
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSession_key() {
		return session_key;
	}

	public void setSession_key(String session_key) {
		this.session_key = session_key;
	}

	public String getActive_status() {
		return active_status;
	}

	public void setActive_status(String active_status) {
		this.active_status = active_status;
	}

	public Date getLast_check() {
		return last_check;
	}

	public void setLast_check(Date last_check) {
		this.last_check = last_check;
	}

}
