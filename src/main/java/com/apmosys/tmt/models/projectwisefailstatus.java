package com.apmosys.tmt.models;

public class projectwisefailstatus {
	private Integer totalrunfail;
	private Integer totalmajor;
	private Integer totalminor;
	private Integer totalcritical;
	private Integer totalblocker;
	private Integer totalopen;
	private Integer totalreopen;
	private Integer totalclosed;
	private Integer runId;
	private Integer totalcriticalopen;
	private Integer totalcriticalclosed;
	private Integer totalcriticalreopen;
	private Integer totalcriticalresolved;
	private Integer totalblockeropen;
	private Integer totalblockerclosed;
	private Integer totalblockerreopen;
	private Integer totalblockerresolved;
	private Integer totalmajoropen;
	private Integer totalmajorclosed;
	private Integer totalmajorreopen;
	private Integer totalmajorresolved;
	private Integer totalminoropen;
	private Integer totalminorclosed;
	private Integer totalminorreopen;
	private Integer totalminorresolved;
	public Integer getRunId() {
		return runId;
	}
	public void setRunId(Integer runId) {
		this.runId = runId;
	}
	public Integer getTotalopen() {
		return totalopen;
	}
	public void setTotalopen(Integer totalopen) {
		this.totalopen = totalopen;
	}
	public Integer getTotalreopen() {
		return totalreopen;
	}
	public void setTotalreopen(Integer totalreopen) {
		this.totalreopen = totalreopen;
	}
	public Integer getTotalclosed() {
		return totalclosed;
	}
	public void setTotalclosed(Integer totalclosed) {
		this.totalclosed = totalclosed;
	}
	private Integer round;
	private String runname;
	private String roundrunname;
	public Integer getRound() {
		return round;
	}
	public void setRound(Integer round) {
		this.round = round;
	}
	public String getRoundrunname() {
		return roundrunname;
	}
	public void setRoundrunname(String roundrunname) {
		this.roundrunname = roundrunname;
	}
	private Integer runid;
	public Integer getTotalrunfail() {
		return totalrunfail;
	}
	public void setTotalrunfail(Integer totalrunfail) {
		this.totalrunfail = totalrunfail;
	}
	public Integer getTotalmajor() {
		return totalmajor;
	}
	public void setTotalmajor(Integer totalmajor) {
		this.totalmajor = totalmajor;
	}
	public Integer getTotalminor() {
		return totalminor;
	}
	public void setTotalminor(Integer totalminor) {
		this.totalminor = totalminor;
	}
	public Integer getTotalcritical() {
		return totalcritical;
	}
	public void setTotalcritical(Integer totalcritical) {
		this.totalcritical = totalcritical;
	}
	public Integer getTotalblocker() {
		return totalblocker;
	}
	public void setTotalblocker(Integer totalblocker) {
		this.totalblocker = totalblocker;
	}
	public String getRunname() {
		return runname;
	}
	public void setRunname(String runname) {
		this.runname = runname;
	}
	public Integer getRunid() {
		return runid;
	}
	public void setRunid(Integer runid) {
		this.runid = runid;
	}
	public Integer getTotalcriticalopen() {
		return totalcriticalopen;
	}
	public void setTotalcriticalopen(Integer totalcriticalopen) {
		this.totalcriticalopen = totalcriticalopen;
	}
	public Integer getTotalcriticalclosed() {
		return totalcriticalclosed;
	}
	public void setTotalcriticalclosed(Integer totalcriticalclosed) {
		this.totalcriticalclosed = totalcriticalclosed;
	}
	public Integer getTotalcriticalreopen() {
		return totalcriticalreopen;
	}
	public void setTotalcriticalreopen(Integer totalcriticalreopen) {
		this.totalcriticalreopen = totalcriticalreopen;
	}
	public Integer getTotalcriticalresolved() {
		return totalcriticalresolved;
	}
	public void setTotalcriticalresolved(Integer totalcriticalresolved) {
		this.totalcriticalresolved = totalcriticalresolved;
	}
	public Integer getTotalblockeropen() {
		return totalblockeropen;
	}
	public void setTotalblockeropen(Integer totalblockeropen) {
		this.totalblockeropen = totalblockeropen;
	}
	public Integer getTotalblockerclosed() {
		return totalblockerclosed;
	}
	public void setTotalblockerclosed(Integer totalblockerclosed) {
		this.totalblockerclosed = totalblockerclosed;
	}
	public Integer getTotalblockerreopen() {
		return totalblockerreopen;
	}
	public void setTotalblockerreopen(Integer totalblockerreopen) {
		this.totalblockerreopen = totalblockerreopen;
	}
	public Integer getTotalblockerresolved() {
		return totalblockerresolved;
	}
	public void setTotalblockerresolved(Integer totalblockerresolved) {
		this.totalblockerresolved = totalblockerresolved;
	}
	public Integer getTotalmajoropen() {
		return totalmajoropen;
	}
	public void setTotalmajoropen(Integer totalmajoropen) {
		this.totalmajoropen = totalmajoropen;
	}
	public Integer getTotalmajorclosed() {
		return totalmajorclosed;
	}
	public void setTotalmajorclosed(Integer totalmajorclosed) {
		this.totalmajorclosed = totalmajorclosed;
	}
	public Integer getTotalmajorreopen() {
		return totalmajorreopen;
	}
	public void setTotalmajorreopen(Integer totalmajorreopen) {
		this.totalmajorreopen = totalmajorreopen;
	}
	public Integer getTotalmajorresolved() {
		return totalmajorresolved;
	}
	public void setTotalmajorresolved(Integer totalmajorresolved) {
		this.totalmajorresolved = totalmajorresolved;
	}
	public Integer getTotalminoropen() {
		return totalminoropen;
	}
	public void setTotalminoropen(Integer totalminoropen) {
		this.totalminoropen = totalminoropen;
	}
	public Integer getTotalminorclosed() {
		return totalminorclosed;
	}
	public void setTotalminorclosed(Integer totalminorclosed) {
		this.totalminorclosed = totalminorclosed;
	}
	public Integer getTotalminorreopen() {
		return totalminorreopen;
	}
	public void setTotalminorreopen(Integer totalminorreopen) {
		this.totalminorreopen = totalminorreopen;
	}
	public Integer getTotalminorresolved() {
		return totalminorresolved;
	}
	public void setTotalminorresolved(Integer totalminorresolved) {
		this.totalminorresolved = totalminorresolved;
	}

}
