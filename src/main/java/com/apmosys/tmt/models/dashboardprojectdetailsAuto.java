package com.apmosys.tmt.models;

import java.util.ArrayList;
import java.util.List;

public class dashboardprojectdetailsAuto {


	public Integer getMinuteSpent() {
		return minuteSpent;
	}
	public void setMinuteSpent(Integer minuteSpent) {
		this.minuteSpent = minuteSpent;
	}

	private String projectid;
	private String projectname;
	private String applicationName;
	

	private String setTotalmanhours;
	private String scenarioID;
	private Integer toalCompletedScenario;
	
	
	public String getScenarioID() {
		return scenarioID;
	}
	public void setScenarioID(String scenarioID) {
		this.scenarioID = scenarioID;
	}
	public Integer getPassedLog() {
		return passedLog;
	}
	public void setPassedLog(Integer passedLog) {
		this.passedLog = passedLog;
	}
	public Integer getFailedLog() {
		return failedLog;
	}
	public void setFailedLog(Integer failedLog) {
		this.failedLog = failedLog;
	}

	private Integer passedLog;
	private Integer failedLog;
	private Integer totalFreshRun;
	private Integer totalCompletedRun;
	private Integer totalFailedRun;
	private Integer totalPass;
	private Integer totalFail;
	private String totalSuccessRate;
	private String runRate;
	private Integer totalLogCount;
	private Integer stoppedRun;
	private Integer running;
	private Integer noOfDatasheet;
	private Integer noOftestSuites;
	private Integer noOfTestcases;
	private Integer totalTestCases;
	private Integer avgTime;
	private Integer daySpent;
	private Integer hourSpent;
	private Integer minuteSpent;
	
	
	
	public Integer getDaySpent() {
		return daySpent;
	}
	public void setDaySpent(Integer daySpent) {
		this.daySpent = daySpent;
	}
	public Integer getHourSpent() {
		return hourSpent;
	}
	public void setHourSpent(Integer hourSpent) {
		this.hourSpent = hourSpent;
	}
	public Integer getMinutespent() {
		return minuteSpent;
	}
	public void setMinutespent(Integer minutespent) {
		this.minuteSpent = minutespent;
	}
	public Integer getAvgTime() {
		return avgTime;
	}
	public void setAvgTime(Integer avgTime) {
		this.avgTime = avgTime;
	}

	List<TestSuitesData> projTestSuitesList= new ArrayList<TestSuitesData>();
	
	
	
	public List<TestSuitesData> getProjTestSuitesList() {
		return projTestSuitesList;
	}
	public void setProjTestSuitesList(List<TestSuitesData> projTestSuitesList) {
		this.projTestSuitesList = projTestSuitesList;
	}
	public Integer getRunning() {
		return running;
	}
	public void setRunning(Integer running) {
		this.running = running;
	}
	public Integer getStoppedRun() {
		return stoppedRun;
	}
	public void setStoppedRun(Integer stoppedRun) {
		this.stoppedRun = stoppedRun;
	}
	public Integer getTotalLogCount() {
		return totalLogCount;
	}
	public void setTotalLogCount(Integer totalLogCount) {
		this.totalLogCount = totalLogCount;
	}
	
	private Integer totalCompleted;
	public String getRunRate() {
		return runRate;
	}
	public void setRunRate(String runRate) {
		this.runRate = runRate;
	}
	private Integer totalOnHold;
	private Integer underReview;
	private Integer totalNotStarted;
	private Integer totalNotApplicable;
	private Integer minor;
	private Integer major;
	private Integer critical;
	private Integer blocker;
	public String getProjectid() {
		return projectid;
	}
	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getSetTotalmanhours() {
		return setTotalmanhours;
	}
	public void setSetTotalmanhours(String setTotalmanhours) {
		this.setTotalmanhours = setTotalmanhours;
	}
	public Integer getTotalFreshRun() {
		return totalFreshRun;
	}
	public void setTotalFreshRun(Integer totalFreshRun) {
		this.totalFreshRun = totalFreshRun;
	}
	public Integer getTotalCompletedRun() {
		return totalCompletedRun;
	}
	public void setTotalCompletedRun(Integer totalCompletedRun) {
		this.totalCompletedRun = totalCompletedRun;
	}
	public Integer getTotalFailedRun() {
		return totalFailedRun;
	}
	public void setTotalFailedRun(Integer totalFailedRun) {
		this.totalFailedRun = totalFailedRun;
	}
	public Integer getTotalPass() {
		return totalPass;
	}
	public void setTotalPass(Integer totalPass) {
		this.totalPass = totalPass;
	}
	public Integer getTotalFail() {
		return totalFail;
	}
	public void setTotalFail(Integer totalFail) {
		this.totalFail = totalFail;
	}
	public Integer getTotalTestCases() {
		return totalTestCases;
	}
	public void setTotalTestCases(Integer totalTestCases) {
		this.totalTestCases = totalTestCases;
	}
	public Integer getTotalCompleted() {
		return totalCompleted;
	}
	public void setTotalCompleted(Integer totalCompleted) {
		this.totalCompleted = totalCompleted;
	}
	public Integer getTotalOnHold() {
		return totalOnHold;
	}
	public void setTotalOnHold(Integer totalOnHold) {
		this.totalOnHold = totalOnHold;
	}
	public Integer getUnderReview() {
		return underReview;
	}
	public void setUnderReview(Integer underReview) {
		this.underReview = underReview;
	}
	public Integer getTotalNotStarted() {
		return totalNotStarted;
	}
	public void setTotalNotStarted(Integer totalNotStarted) {
		this.totalNotStarted = totalNotStarted;
	}
	public Integer getTotalNotApplicable() {
		return totalNotApplicable;
	}
	public void setTotalNotApplicable(Integer totalNotApplicable) {
		this.totalNotApplicable = totalNotApplicable;
	}
	public Integer getMinor() {
		return minor;
	}
	public void setMinor(Integer minor) {
		this.minor = minor;
	}
	public Integer getMajor() {
		return major;
	}
	public void setMajor(Integer major) {
		this.major = major;
	}
	public Integer getCritical() {
		return critical;
	}
	public void setCritical(Integer critical) {
		this.critical = critical;
	}
	public Integer getBlocker() {
		return blocker;
	}
	public void setBlocker(Integer blocker) {
		this.blocker = blocker;
	}
	public String getTotalSuccessRate() {
		return totalSuccessRate;
	}
	public void setTotalSuccessRate(String totalSuccessRate) {
		this.totalSuccessRate = totalSuccessRate;
	}
	public Integer getNoOfDatasheet() {
		return noOfDatasheet;
	}
	public void setNoOfDatasheet(Integer noOfDatasheet) {
		this.noOfDatasheet = noOfDatasheet;
	}
	public Integer getNoOftestSuites() {
		return noOftestSuites;
	}
	public void setNoOftestSuites(Integer noOftestSuites) {
		this.noOftestSuites = noOftestSuites;
	}
	public Integer getNoOfTestcases() {
		return noOfTestcases;
	}
	public void setNoOfTestcases(Integer noOfTestcases) {
		this.noOfTestcases = noOfTestcases;
	}
	public void setNoOfScenarioCompleted(Integer valueOf) {
		// TODO Auto-generated method stub
		
	}
	public Integer getToalCompletedScenario() {
		return toalCompletedScenario;
	}
	public void setToalCompletedScenario(Integer toalCompletedScenario) {
		this.toalCompletedScenario = toalCompletedScenario;
	} 

	
}
