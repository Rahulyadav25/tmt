package com.apmosys.tmt.models;

import java.util.ArrayList;
import java.util.List;

public class ProjectDetailsAuto {
	private String projectid;
	private String projectname;
	private String scenarioID;
	private Integer totalLogCount;
	private Integer passedLog;
	private Integer failedLog;
	private Integer freshRun;
	private Integer completedRun;
	private Integer failedRun;
	private Integer stoppedRun;
	private Integer running;
	private Integer noOfDatasheet;
	private Integer noOftestSuites;
	private Integer noOfTestcases;
	private Integer totalTestCases;
	List<TestSuitesData> projTestSuitesList= new ArrayList<TestSuitesData>();
	
	
	
	public List<TestSuitesData> getProjTestSuitesList() {
		return projTestSuitesList;
	}
	public void setProjTestSuitesList(List<TestSuitesData> projTestSuitesList) {
		this.projTestSuitesList = projTestSuitesList;
	}
	public Integer getTotalTestCases() {
		return totalTestCases;
	}
	public void setTotalTestCases(Integer totalTestCases) {
		this.totalTestCases = totalTestCases;
	}
	public Integer getRunning() {
		return running;
	}
	public void setRunning(Integer running) {
		this.running = running;
	}
	public Integer getNoOfDatasheet() {
		return noOfDatasheet;
	}
	public void setNoOfDatasheet(Integer noOfDatasheet) {
		this.noOfDatasheet = noOfDatasheet;
	}
	public Integer getNoOftestSuites() {
		return noOftestSuites;
	}
	public void setNoOftestSuites(Integer noOftestSuites) {
		this.noOftestSuites = noOftestSuites;
	}
	public Integer getNoOfTestcases() {
		return noOfTestcases;
	}
	public void setNoOfTestcases(Integer noOfTestcases) {
		this.noOfTestcases = noOfTestcases;
	}
	public String getProjectid() {
		return projectid;
	}
	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	public Integer getTotalLogCount() {
		return totalLogCount;
	}
	public void setTotalLogCount(Integer totalLogCount) {
		this.totalLogCount = totalLogCount;
	}
	
	public Integer getPassedLog() {
		return passedLog;
	}
	public void setPassedLog(Integer passedLog) {
		this.passedLog = passedLog;
	}
	public Integer getFailedLog() {
		return failedLog;
	}
	public void setFailedLog(Integer failedLog) {
		this.failedLog = failedLog;
	}
	public Integer getFreshRun() {
		return freshRun;
	}
	public void setFreshRun(Integer freshRun) {
		this.freshRun = freshRun;
	}
	public Integer getCompletedRun() {
		return completedRun;
	}
	public void setCompletedRun(Integer completedRun) {
		this.completedRun = completedRun;
	}
	public Integer getFailedRun() {
		return failedRun;
	}
	public void setFailedRun(Integer failedRun) {
		this.failedRun = failedRun;
	}
	public Integer getStoppedRun() {
		return stoppedRun;
	}
	public void setStoppedRun(Integer stoppedRun) {
		this.stoppedRun = stoppedRun;
	}
	public String getScenarioID() {
		return scenarioID;
	}
	public void setScenarioID(String scenarioID) {
		this.scenarioID = scenarioID;
	}
	
	
}
