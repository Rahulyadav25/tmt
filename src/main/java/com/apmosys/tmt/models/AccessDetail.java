package com.apmosys.tmt.models;

public class AccessDetail {
private Integer userid;
private Integer projectid;
private String projectName;
private String Accesstype;
public String getAccesstype() {
	return Accesstype;
}
public void setAccesstype(String accesstype) {
	Accesstype = accesstype;
}
public Integer getUserid() {
	return userid;
}
public void setUserid(Integer userid) {
	this.userid = userid;
}
public Integer getProjectid() {
	return projectid;
}
public void setProjectid(Integer projectid) {
	this.projectid = projectid;
}
public String getProjectName() {
	return projectName;
}
public void setProjectName(String projectName) {
	this.projectName = projectName;
}

}
