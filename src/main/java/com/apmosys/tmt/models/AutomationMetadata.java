package com.apmosys.tmt.models;


import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

//import javax.persistence.Cacheable;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.persistence.Transient;
@Cacheable(false)
@Entity
@Table(name="AutomationProjectMeta")
public class AutomationMetadata {
	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer id;
	@Column(name = "projectname")
	private String projectname;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}



	public String getProjectname() {
		return projectname;
	}

	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}

	public Integer getProjectid() {
		return projectid;
	}

	public void setProjectid(Integer projectid) {
		this.projectid = projectid;
	}

	public Integer getNoOfDatasheet() {
		return noOfDatasheet;
	}

	public void setNoOfDatasheet(Integer noOfDatasheet) {
		this.noOfDatasheet = noOfDatasheet;
	}

	public Integer getNoOftestSuites() {
		return noOftestSuites;
	}

	public void setNoOftestSuites(Integer noOftestSuites) {
		this.noOftestSuites = noOftestSuites;
	}

	public Integer getNoOfTestcases() {
		return noOfTestcases;
	}

	public void setNoOfTestcases(Integer noOfTestcases) {
		this.noOfTestcases = noOfTestcases;
	}

	public String getLocationofDatasheet() {
		return locationofDatasheet;
	}

	public void setLocationofDatasheet(String locationofDatasheet) {
		this.locationofDatasheet = locationofDatasheet;
	}

	@Column(name = "projectid")
	private Integer projectid;
	
	
	@Column(name = "noOfDatasheet")
	private Integer noOfDatasheet;
	
	@Column(name = "noOftestSuites")
	private Integer noOftestSuites;
	

	@Column(name = "noOfTestcases")
	private Integer noOfTestcases;
	
	@Column(name = "locationofDatasheet")
	private String locationofDatasheet;
	
}
