package com.apmosys.tmt.models;

public class RunWiseBusStatusForExport {
	private Integer runId;
	private String runName;
	private Integer closed=0;
	private Integer done=0;
	private Integer notAnIssue=0; 
	private Integer onHold=0;
	private Integer inPrgress=0;
	private Integer reOpened=0;
	private Integer toDo=0;
	private Integer duplicate=0;
	private Integer Total=0;

	@Override
	public String toString() {
		return "RunWiseBusStatusForExport [runId=" + runId + ", runName=" + runName + ", closed=" + closed + ", done="
				+ done + ", notAnIssue=" + notAnIssue + ", onHold=" + onHold + ", inPrgress=" + inPrgress
				+ ", reOpened=" + reOpened + ", toDo=" + toDo + ", duplicate=" + duplicate + ", Total=" + Total + "]";
	}
	public Integer getRunId() {
		return runId;
	}
	public void setRunId(Integer runId) {
		this.runId = runId;
	}
	public String getRunName() {
		return runName;
	}
	public void setRunName(String runName) {
		this.runName = runName;
	}
	public Integer getClosed() {
		return closed;
	}
	public void setClosed(Integer closed) {
		this.closed = closed;
	}
	public Integer getDone() {
		return done;
	}
	public void setDone(Integer done) {
		this.done = done;
	}
	public Integer getNotAnIssue() {
		return notAnIssue;
	}
	public void setNotAnIssue(Integer notAnIssue) {
		this.notAnIssue = notAnIssue;
	}
	public Integer getOnHold() {
		return onHold;
	}
	public void setOnHold(Integer onHold) {
		this.onHold = onHold;
	}
	public Integer getInPrgress() {
		return inPrgress;
	}
	public void setInPrgress(Integer inPrgress) {
		this.inPrgress = inPrgress;
	}
	public Integer getReOpened() {
		return reOpened;
	}
	public void setReOpened(Integer reOpened) {
		this.reOpened = reOpened;
	}
	public Integer getToDo() {
		return toDo;
	}
	public void setToDo(Integer toDo) {
		this.toDo = toDo;
	}
	public Integer getDuplicate() {
		return duplicate;
	}
	public void setDuplicate(Integer duplicate) {
		this.duplicate = duplicate;
	}
	public Integer getTotal() {
		return closed+ done +notAnIssue +onHold +inPrgress+ reOpened +toDo + duplicate ;
	}
	public void setTotal(Integer total) {
		Total = total;
	}

	
	
}
