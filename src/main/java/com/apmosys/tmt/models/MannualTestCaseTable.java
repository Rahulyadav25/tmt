package com.apmosys.tmt.models;

import java.util.Date;

//import javax.persistence.Cacheable;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@DynamicInsert
@DynamicUpdate
@Cacheable(false)
@Entity
@Table(name="Mannual_TestCase_Table")
public class MannualTestCaseTable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	private Integer SrID;
	@Transient
	private Integer testSuitesId;
	@Transient
	private String testSuitesName;
	@Transient
	private String testGroupName;
	private Integer testGroupId;
	private String srNo;
	private String module;
	private String subModule;
	private String scenarioID;
	private String scenarios;
	private String functionality;
	private String testID;
	private String testCaseType;
	private String testDescription;
	private String dateOfExec;
	private String browser;
	private String testData;
	private String steps;
	private String expectedResult;
	private String actualRsult;
	private String status;
	@Column(name = "action")
	private String action;
	@Column(name = "bug_ID")
	private String bugID;
	@Column(name = "bug_status")
	private String bugStatus;
	@Column(name = "bug_severity")
	private String bugSeverity;
	@Column(name = "bug_priority")
	private String bugPriority;
	private String executionTime;
	@Column(name = "preparation_time")
	private Long preparationTime;
	private String testerComment;
	private String testerName;
	private String jiraID;
	@Column(name = "prepared_by")
	private Integer tcPreparedBy;
	private Integer createdBy;
	private Date createdDate;
	private Integer updatedBy;
	private Date updatedDate;
	
	
	@Transient
	private Boolean selected=true;
	@Transient
	private String isActive;
	@Transient
	private String messageerror;

	public String getMessageerror() {
		return messageerror;
	}
	public void setMessageerror(String messageerror) {
		this.messageerror = messageerror;
	}
	@Transient
	private Integer absoluteTCID;
	@Column(name = "priority")
	private String priority;
	@Column(name = "complexity")
	private String complexity;
	
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getComplexity() {
		return complexity;
	}
	public void setComplexity(String complexity) {
		this.complexity = complexity;
	}
	public Integer getTestGroupId() {
		return testGroupId;
	}
	public void setTestGroupId(Integer testGroupId) {
		this.testGroupId = testGroupId;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	
	public Integer getTestSuitesId() {
		return testSuitesId;
	}
	public void setTestSuitesId(Integer testSuitesId) {
		this.testSuitesId = testSuitesId;
	}
	public String getTestSuitesName() {
		return testSuitesName;
	}
	public void setTestSuitesName(String testSuitesName) {
		this.testSuitesName = testSuitesName;
	}
	public String getTestGroupName() {
		return testGroupName;
	}
	public void setTestGroupName(String testGroupName) {
		this.testGroupName = testGroupName;
	}
	public String getSrNo() {
		return srNo;
	}
	public void setSrNo(String srNo) {
		this.srNo = srNo;
	}
	public String getTestID() {
		return testID;
	}
	public void setTestID(String testID) {
		this.testID = testID;
	}
	public String getDateOfExec() {
		return dateOfExec;
	}
	public void setDateOfExec(String dateOfExec) {
		this.dateOfExec = dateOfExec;
	}
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public Integer getSrID() {
		return SrID;
	}
	public void setSrID(Integer srID) {
		SrID = srID;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getSubModule() {
		return subModule;
	}
	public void setSubModule(String subModule) {
		this.subModule = subModule;
	}
	public String getScenarioID() {
		return scenarioID;
	}
	public void setScenarioID(String scenarioID) {
		this.scenarioID = scenarioID;
	}
	public String getScenarios() {
		return scenarios;
	}
	public void setScenarios(String scenarios) {
		this.scenarios = scenarios;
	}
	public String getFunctionality() {
		return functionality;
	}
	public void setFunctionality(String functionality) {
		this.functionality = functionality;
	}
	public String getExecutionTime() {
		return executionTime;
	}
	public void setExecutionTime(String executionTime) {
		this.executionTime = executionTime;
	}
	public String getTestCaseType() {
		return testCaseType;
	}
	public void setTestCaseType(String testCaseType) {
		this.testCaseType = testCaseType;
	}
	public String getTestDescription() {
		return testDescription;
	}
	public void setTestDescription(String testDescription) {
		this.testDescription = testDescription;
	}
	public String getTestData() {
		return testData;
	}
	public void setTestData(String testData) {
		this.testData = testData;
	}
	public String getSteps() {
		return steps;
	}
	public void setSteps(String steps) {
		this.steps = steps;
	}
	public String getExpectedResult() {
		return expectedResult;
	}
	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}
	public String getActualRsult() {
		return actualRsult;
	}
	public void setActualRsult(String actualRsult) {
		this.actualRsult = actualRsult;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTesterComment() {
		return testerComment;
	}
	public void setTesterComment(String testerComment) {
		this.testerComment = testerComment;
	}
	public String getTesterName() {
		return testerName;
	}
	public void setTesterName(String testerName) {
		this.testerName = testerName;
	}
	public String getJiraID() {
		return jiraID;
	}
	public void setJiraID(String jiraID) {
		this.jiraID = jiraID;
	}
	
	
	public Boolean getSelected() {
		return selected;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getAbsoluteTCID() {
		return absoluteTCID;
	}
	public void setAbsoluteTCID(Integer absoluteTCID) {
		this.absoluteTCID = absoluteTCID;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getBugID() {
		return bugID;
	}
	public void setBugID(String bugID) {
		this.bugID = bugID;
	}
	public String getBugStatus() {
		return bugStatus;
	}
	public void setBugStatus(String bugStatus) {
		this.bugStatus = bugStatus;
	}
	public String getBugSeverity() {
		return bugSeverity;
	}
	public void setBugSeverity(String bugSeverity) {
		this.bugSeverity = bugSeverity;
	}
	public String getBugPriority() {
		return bugPriority;
	}
	public void setBugPriority(String bugPriority) {
		this.bugPriority = bugPriority;
	}
	public Long getPreparationTime() {
		return preparationTime;
	}
	public void setPreparationTime(Long preparationTime) {
		this.preparationTime = preparationTime;
	}
	public Integer getTcPreparedBy() {
		return tcPreparedBy;
	}
	public void setTcPreparedBy(Integer tcPreparedBy) {
		this.tcPreparedBy = tcPreparedBy;
	}
	@Override
	public String toString() {
		return "MannualTestCaseTable [SrID=" + SrID + ", testSuitesId=" + testSuitesId + ", testSuitesName="
				+ testSuitesName + ", testGroupName=" + testGroupName + ", testGroupId=" + testGroupId + ", srNo="
				+ srNo + ", module=" + module + ", subModule=" + subModule + ", scenarioID=" + scenarioID
				+ ", scenarios=" + scenarios + ", functionality=" + functionality + ", testID=" + testID
				+ ", testCaseType=" + testCaseType + ", testDescription=" + testDescription + ", dateOfExec="
				+ dateOfExec + ", browser=" + browser + ", testData=" + testData + ", steps=" + steps
				+ ", expectedResult=" + expectedResult + ", actualRsult=" + actualRsult + ", status=" + status
				+ ", action=" + action + ", bugID=" + bugID + ", bugStatus=" + bugStatus + ", bugSeverity="
				+ bugSeverity + ", bugPriority=" + bugPriority + ", executionTime=" + executionTime
				+ ", preparationTime=" + preparationTime + ", testerComment=" + testerComment + ", testerName="
				+ testerName + ", jiraID=" + jiraID + ", tcPreparedBy=" + tcPreparedBy + ", createdBy=" + createdBy
				+ ", createdDate=" + createdDate + ", updatedBy=" + updatedBy + ", updatedDate=" + updatedDate
				+ ", selected=" + selected + ", isActive=" + isActive + ", absoluteTCID=" + absoluteTCID + "]";
	}



}
