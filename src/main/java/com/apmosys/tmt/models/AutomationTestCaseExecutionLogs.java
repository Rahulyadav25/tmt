package com.apmosys.tmt.models;

import java.util.Arrays;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.persistence.Transient;

@Entity
@Table(name = "autoBuglogs")
public class AutomationTestCaseExecutionLogs {
	@Id
	private Integer id;
	
//	@Column(name="RUN_ID")
//	private Integer runId;
	
	@Column(name="project_name")
	private String project_name;
	
	@Column(name="Application_Name")
	private String applicationName;
	
	@Column(name="Process")
	private String process;
	
	@Column(name="ScenarioID")
	private String scenarioID;
	
//	@Column(name="TestCaseID")
//	private String testCaseID;
	
//	@Column(name="Response_Time")
//	private String responseTime;
	
	@Column(name="Expected_Result")
	private String expectedResult;
	
	@Column(name="Actual_Result")
	private String actualResult;
	
	@Column(name="status")
	private String status;
	
	@Column(name="Timestamp")
	private String timestamp;
//	@Column(name="Description")
//	private String description;
	
//	@Column(name="BrowserType")
//	private String browserType;
	
	@Column(name="ScreenshotID")
	private String screenshotID;
	//pranik 
//	@Column(name="TestcaseType")
//	private String TestcaseType;
	
//	@Column(name="Step")
//	private String Step;
	
	@Transient
	private String screenshotName;
	
	@Transient
	private byte[] screenshotImg;
//changed
	@Column(name="IP_HOST_ZONE")
	private String ip_host_zone;
//	@Column(name="scenario_description")
//	private String scenario_description;
	
	//additional columns
	
	@Column(name="TestCaseID")
	private String testCaseSrNo; //testcase id
	
	@Column(name="Description")
	private String testDescription;
	
	@Transient
	private String testCaseAction;
//	@Column(name="status")
//	private String status;
	@Column(name="bug_id")
	private String bugId;
	
	@Column(name="bugPriority")
	private String bugPriority;//
	
	@Column(name="bugSeverity")
	private String bugSeverity;//
	
	@Column(name="Response_Time")
	private String executionTime;//responsetime
	
	@Column(name="bugType")
	private String bugType;//
	
	@Column(name="bugToolId")
	private String bugToolId;//
	
	@Column(name="BrowserType")
	private String browser;
	
	@Column(name="complexity")
	private String complexity;//
	
	@Column(name="functionality")
	private String functionality;//
	
	
//	@Column(name="ScenarioID")
//	private String scenarioID;//need to check
	@Column(name="scenario_description")
	private String scenarios;
	
	@Column(name="TestcaseType")
	private String testCaseType;
	
	@Column(name="testData")
	private String testData;//
	
	@Column(name="testerComment")
	private String testerComment;//
	
	@Column(name="RUN_ID")
	private String testID;//runid
	
	@Column(name="Step")
	private String steps;
	
	@Column(name="priority")
	private String priority;//
	
	@Column(name="bugStatus")
	private String bugStatus;//
	
	@Column(name="bug_summary")
	private String apiBugSummary;//
	
	@Transient
	private String toolName;
	
	@Column(name="projectId")
	private String projectId;
	
	@Transient
	private String projectName;
	
	@Transient
	private Date dateOfExec;
	//end


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getProject_name() {
		return project_name;
	}


	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}


	public String getApplicationName() {
		return applicationName;
	}


	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}


	public String getProcess() {
		return process;
	}


	public void setProcess(String process) {
		this.process = process;
	}


	public String getScenarioID() {
		return scenarioID;
	}


	public void setScenarioID(String scenarioID) {
		this.scenarioID = scenarioID;
	}


	public String getExpectedResult() {
		return expectedResult;
	}


	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}


	public String getActualResult() {
		return actualResult;
	}


	public void setActualResult(String actualResult) {
		this.actualResult = actualResult;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}


	public String getScreenshotID() {
		return screenshotID;
	}


	public void setScreenshotID(String screenshotID) {
		this.screenshotID = screenshotID;
	}


	public String getScreenshotName() {
		return screenshotName;
	}


	public void setScreenshotName(String screenshotName) {
		this.screenshotName = screenshotName;
	}


	public byte[] getScreenshotImg() {
		return screenshotImg;
	}


	public void setScreenshotImg(byte[] screenshotImg) {
		this.screenshotImg = screenshotImg;
	}


	public String getIp_host_zone() {
		return ip_host_zone;
	}


	public void setIp_host_zone(String ip_host_zone) {
		this.ip_host_zone = ip_host_zone;
	}


	public String getTestCaseSrNo() {
		return testCaseSrNo;
	}


	public void setTestCaseSrNo(String testCaseSrNo) {
		this.testCaseSrNo = testCaseSrNo;
	}


	public String getTestDescription() {
		return testDescription;
	}


	public void setTestDescription(String testDescription) {
		this.testDescription = testDescription;
	}


	public String getTestCaseAction() {
		return testCaseAction;
	}


	public void setTestCaseAction(String testCaseAction) {
		this.testCaseAction = testCaseAction;
	}


	public String getBugId() {
		return bugId;
	}


	public void setBugId(String bugId) {
		this.bugId = bugId;
	}


	public String getBugPriority() {
		return bugPriority;
	}


	public void setBugPriority(String bugPriority) {
		this.bugPriority = bugPriority;
	}


	public String getBugSeverity() {
		return bugSeverity;
	}


	public void setBugSeverity(String bugSeverity) {
		this.bugSeverity = bugSeverity;
	}


	public String getExecutionTime() {
		return executionTime;
	}


	public void setExecutionTime(String executionTime) {
		this.executionTime = executionTime;
	}


	public String getBugType() {
		return bugType;
	}


	public void setBugType(String bugType) {
		this.bugType = bugType;
	}


	public String getBugToolId() {
		return bugToolId;
	}


	public void setBugToolId(String bugToolId) {
		this.bugToolId = bugToolId;
	}


	public String getBrowser() {
		return browser;
	}


	public void setBrowser(String browser) {
		this.browser = browser;
	}


	public String getComplexity() {
		return complexity;
	}


	public void setComplexity(String complexity) {
		this.complexity = complexity;
	}


	public String getFunctionality() {
		return functionality;
	}


	public void setFunctionality(String functionality) {
		this.functionality = functionality;
	}


	public String getScenarios() {
		return scenarios;
	}


	public void setScenarios(String scenarios) {
		this.scenarios = scenarios;
	}


	public String getTestCaseType() {
		return testCaseType;
	}


	public void setTestCaseType(String testCaseType) {
		this.testCaseType = testCaseType;
	}


	public String getTestData() {
		return testData;
	}


	public void setTestData(String testData) {
		this.testData = testData;
	}


	public String getTesterComment() {
		return testerComment;
	}


	public void setTesterComment(String testerComment) {
		this.testerComment = testerComment;
	}


	public String getTestID() {
		return testID;
	}


	public void setTestID(String testID) {
		this.testID = testID;
	}


	public String getSteps() {
		return steps;
	}


	public void setSteps(String steps) {
		this.steps = steps;
	}


	public String getPriority() {
		return priority;
	}


	public void setPriority(String priority) {
		this.priority = priority;
	}


	public String getBugStatus() {
		return bugStatus;
	}


	public void setBugStatus(String bugStatus) {
		this.bugStatus = bugStatus;
	}


	public String getApiBugSummary() {
		return apiBugSummary;
	}


	public void setApiBugSummary(String apiBugSummary) {
		this.apiBugSummary = apiBugSummary;
	}


	public String getToolName() {
		return toolName;
	}


	public void setToolName(String toolName) {
		this.toolName = toolName;
	}


	public String getProjectId() {
		return projectId;
	}


	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}


	public String getProjectName() {
		return projectName;
	}


	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}


	public Date getDateOfExec() {
		return dateOfExec;
	}


	public void setDateOfExec(Date dateOfExec) {
		this.dateOfExec = dateOfExec;
	}


	@Override
	public String toString() {
		return "AutomationTestCaseExecutionLogs [id=" + id + ", project_name=" + project_name + ", applicationName="
				+ applicationName + ", process=" + process + ", scenarioID=" + scenarioID + ", expectedResult="
				+ expectedResult + ", actualResult=" + actualResult + ", status=" + status + ", timestamp=" + timestamp
				+ ", screenshotID=" + screenshotID + ", screenshotName=" + screenshotName + ", screenshotImg="
				+ Arrays.toString(screenshotImg) + ", ip_host_zone=" + ip_host_zone + ", testCaseSrNo=" + testCaseSrNo
				+ ", testDescription=" + testDescription + ", testCaseAction=" + testCaseAction + ", bugId=" + bugId
				+ ", bugPriority=" + bugPriority + ", bugSeverity=" + bugSeverity + ", executionTime=" + executionTime
				+ ", bugType=" + bugType + ", bugToolId=" + bugToolId + ", browser=" + browser + ", complexity="
				+ complexity + ", functionality=" + functionality + ", scenarios=" + scenarios + ", testCaseType="
				+ testCaseType + ", testData=" + testData + ", testerComment=" + testerComment + ", testID=" + testID
				+ ", steps=" + steps + ", priority=" + priority + ", bugStatus=" + bugStatus + ", apiBugSummary="
				+ apiBugSummary + ", toolName=" + toolName + ", projectId=" + projectId + ", projectName=" + projectName
				+ ", dateOfExec=" + dateOfExec + "]";
	}


	public AutomationTestCaseExecutionLogs(Integer id, String project_name, String applicationName, String process,
			String scenarioID, String expectedResult, String actualResult, String status, String timestamp,
			String screenshotID, String screenshotName, byte[] screenshotImg, String ip_host_zone, String testCaseSrNo,
			String testDescription, String testCaseAction, String bugId, String bugPriority, String bugSeverity,
			String executionTime, String bugType, String bugToolId, String browser, String complexity,
			String functionality, String scenarios, String testCaseType, String testData, String testerComment,
			String testID, String steps, String priority, String bugStatus, String apiBugSummary, String toolName,
			String projectId, String projectName, Date dateOfExec) {
		super();
		this.id = id;
		this.project_name = project_name;
		this.applicationName = applicationName;
		this.process = process;
		this.scenarioID = scenarioID;
		this.expectedResult = expectedResult;
		this.actualResult = actualResult;
		this.status = status;
		this.timestamp = timestamp;
		this.screenshotID = screenshotID;
		this.screenshotName = screenshotName;
		this.screenshotImg = screenshotImg;
		this.ip_host_zone = ip_host_zone;
		this.testCaseSrNo = testCaseSrNo;
		this.testDescription = testDescription;
		this.testCaseAction = testCaseAction;
		this.bugId = bugId;
		this.bugPriority = bugPriority;
		this.bugSeverity = bugSeverity;
		this.executionTime = executionTime;
		this.bugType = bugType;
		this.bugToolId = bugToolId;
		this.browser = browser;
		this.complexity = complexity;
		this.functionality = functionality;
		this.scenarios = scenarios;
		this.testCaseType = testCaseType;
		this.testData = testData;
		this.testerComment = testerComment;
		this.testID = testID;
		this.steps = steps;
		this.priority = priority;
		this.bugStatus = bugStatus;
		this.apiBugSummary = apiBugSummary;
		this.toolName = toolName;
		this.projectId = projectId;
		this.projectName = projectName;
		this.dateOfExec = dateOfExec;
	}


	public AutomationTestCaseExecutionLogs() {
		super();
	}
	
	//getter setter
	
	
}