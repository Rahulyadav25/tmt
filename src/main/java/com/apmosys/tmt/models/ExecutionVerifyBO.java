package com.apmosys.tmt.models;

/*import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;*/

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@DynamicInsert
@DynamicUpdate
@Cacheable(false)
@Entity
@Table(name = "executionsteps")
public class ExecutionVerifyBO {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "RUN_ID")
	private Integer runId;
	@Column(name = "project_name")
	private String projectName;
	@Column(name = "Application_Name")
	private String applicationName;
	@Column(name = "ScenarioID")
	private String scenarioId;
	@Column(name = "TestCaseID")
	private String testCaseId;
	@Column(name = "Response_Time")
	private String resTime;
	@Column(name = "Actual_Result")
	private String actualResult;
	@Column(name = "Expected_Result")
	private String expectedResult;
	@Column(name = "status")
	private String status;
	@Column(name = "Timestamp")
	private String timeStamp;
	@Column(name = "Description")
	private String description;
	@Column(name = "BrowserType")
	private String browserType;
	@Column(name = "IP_HOST_ZONE")
	private String ip;
	@Column(name = "ScreenshotID")
	private String screenShotId;
	@Transient
	private byte[] screenshot;
	@Transient
	private String start;
	@Transient
	private String end;
	@Transient
	private String testcasestatus;
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRunId() {
		return runId;
	}

	public void setRunId(Integer runId) {
		this.runId = runId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getScenarioId() {
		return scenarioId;
	}

	public void setScenarioId(String scenarioId) {
		this.scenarioId = scenarioId;
	}

	public String getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}

	public String getResTime() {
		return resTime;
	}

	public void setResTime(String resTime) {
		this.resTime = resTime;
	}

	public String getActualResult() {
		return actualResult;
	}

	public void setActualResult(String actualResult) {
		this.actualResult = actualResult;
	}

	public String getExpectedResult() {
		return expectedResult;
	}

	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBrowserType() {
		return browserType;
	}

	public void setBrowserType(String browserType) {
		this.browserType = browserType;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getScreenShotId() {
		return screenShotId;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getTestcasestatus() {
		return testcasestatus;
	}

	public void setTestcasestatus(String testcasestatus) {
		this.testcasestatus = testcasestatus;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public void setScreenShotId(String screenShotId) {
		this.screenShotId = screenShotId;
	}

	public byte[] getScreenshot() {
		return screenshot;
	}

	public void setScreenshot(byte[] screenshot) {
		this.screenshot = screenshot;
	}

	@Override
	public String toString() {
		return "ExecutionVerifyBO [id=" + id + ", runId=" + runId + ", projectName=" + projectName
				+ ", applicationName=" + applicationName + ", scenarioId=" + scenarioId + ", testCaseId=" + testCaseId
				+ ", resTime=" + resTime + ", actualResult=" + actualResult + ", expectedResult=" + expectedResult
				+ ", status=" + status + ", timeStamp=" + timeStamp + ", description=" + description + ", browserType="
				+ browserType + ", ip=" + ip + ", screenShotId=" + screenShotId + "]";
	}

}
