package com.apmosys.tmt.models;

import java.util.Date;

public class userRole {
	private String userid;
	private Integer id;
	private String username;
	private Integer createdRoleID;
	private String createdDate;
	private String userroleAdmin;	
	private String userCreatedBy;
    private String userrole;
    private String isactive;
    private String password;
    private String creatorid;
    private String roleType;
    private String projectName;
    private String AccessType;
    private String projectId;
    
    private Integer filedAttempts;
    
    
    
  
	public Integer getFiledAttempts() {
		return filedAttempts;
	}
	public void setFiledAttempts(Integer filedAttempts) {
		this.filedAttempts = filedAttempts;
	}
	public String getAccessDate() {
		return accessDate;
	}
	public void setAccessDate(String accessDate) {
		this.accessDate = accessDate;
	}
	private String accessGivenBy;
    private String accessDate;
    
    
    public String getAccessGivenBy() {
		return accessGivenBy;
	}
	public void setAccessGivenBy(String accessGivenBy) {
		this.accessGivenBy = accessGivenBy;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getAccessType() {
		return AccessType;
	}
	public void setAccessType(String accessType) {
		AccessType = accessType;
	}
	public String getRoleType() {
		return roleType;
	}
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}
	private Integer userroleid;
	
	public Integer getCreatedRoleID() {
		return createdRoleID;
	}
	public void setCreatedRoleID(Integer createdRoleID) {
		this.createdRoleID = createdRoleID;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
    
    
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getUserroleAdmin() {
		return userroleAdmin;
	}
	public void setUserroleAdmin(String userroleAdmin) {
		this.userroleAdmin = userroleAdmin;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public Integer getUserroleid() {
		return userroleid;
	}
	public void setUserroleid(Integer userroleid) {
		this.userroleid = userroleid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUserCreatedBy() {
		return userCreatedBy;
	}
	public void setUserCreatedBy(String userCreatedBy) {
		this.userCreatedBy = userCreatedBy;
	}
	public String getUserrole() {
		return userrole;
	}
	public void setUserrole(String userrole) {
		this.userrole = userrole;
	}
	public String getIsactive() {
		return isactive;
	}
	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreatorid() {
		return creatorid;
	}
	public void setCreatorid(String creatorid) {
		this.creatorid = creatorid;
	}
	

}
