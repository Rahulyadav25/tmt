package com.apmosys.tmt.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;

@Entity
@Table(name = "screenshots")
public class AutomationScreenshot 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer screenshotId;
	
	@Column(name="image_name")
	private String screenshotName;
	
	@Column(name="image_value")
	private byte[] screenshot;
	
	public AutomationScreenshot()
	{
		
	}

	public Integer getScreenshotId() {
		return screenshotId;
	}

	public void setScreenshotId(Integer screenshotId) {
		this.screenshotId = screenshotId;
	}

	public String getScreenshotName() {
		return screenshotName;
	}

	public void setScreenshotName(String screenshotName) {
		this.screenshotName = screenshotName;
	}

	public byte[] getScreenshot() {
		return screenshot;
	}

	public void setScreenshot(byte[] screenshot) {
		this.screenshot = screenshot;
	}
	
	
}
