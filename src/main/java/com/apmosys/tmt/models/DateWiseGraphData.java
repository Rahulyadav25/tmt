package com.apmosys.tmt.models;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateWiseGraphData 
{
private Date specificdate;
private Integer pass;
public Integer getTotalunexecuted() {
	return Totalunexecuted;
}
public void setTotalunexecuted(Integer totalunexecuted) {
	Totalunexecuted = totalunexecuted;
}
private Integer fail;
private Integer retest;
private Integer blocker;
private Integer totalcases;
private Integer Totalunexecuted;
public Date getSpecificdate() {
	return specificdate;
}
public void setSpecificdate(Date date) {
	/*Date date1 = null;
	try {
		date1 = new SimpleDateFormat("MM/dd/yyyy").parse(date);
	} catch (ParseException e) {
		e.printStackTrace();
	}
	java.sql.Date sqlDate = new java.sql.Date(date1.getTime());*/
	specificdate = date; 
}
public Integer getPass() {
	return pass;
}
public void setPass(Integer pass) {
	this.pass = pass;
}
public Integer getFail() {
	return fail;
}
public void setFail(Integer fail) {
	this.fail = fail;
}
public Integer getRetest() {
	return retest;
}
public void setRetest(Integer retest) {
	this.retest = retest;
}
public Integer getBlocker() {
	return blocker;
}
public void setBlocker(Integer blocker) {
	this.blocker = blocker;
}
public Integer getTotalcases() {
	return totalcases;
}
public void setTotalcases(Integer totalcases) {
	this.totalcases = totalcases;
}
	
}
