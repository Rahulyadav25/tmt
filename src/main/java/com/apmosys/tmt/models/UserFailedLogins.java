package com.apmosys.tmt.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;

@Table(name = "user_failed_logins")
@Entity
public class UserFailedLogins {
	@Id
	@Column(name = "user_id")
	private String userId;
	@Column(name = "failed_attempt")
	private Integer failedAttemptCount;
	
	
	//toString
	@Override
	public String toString() {
		return "UserFailedLogins [userId=" + userId + ", failedAttemptCount=" + failedAttemptCount + "]";
	}
	
	//setters and getters
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getFailedAttemptCount() {
		return failedAttemptCount;
	}
	public void setFailedAttemptCount(Integer failedAttemptCount) {
		this.failedAttemptCount = failedAttemptCount;
	}
	
}//class
