package com.apmosys.tmt.models;

import java.util.Date;

public class DateWiseRunData {
	private Integer  totalexecution;
	private Date  dateofexecution;
	private Integer  runid;
	private String runname;
	private String projectName;
	public Integer getTotalexecution() {
		return totalexecution;
	}
	public void setTotalexecution(Integer totalexecution) {
		this.totalexecution = totalexecution;
	}
	public Date getDateofexecution() {
		return dateofexecution;
	}
	public void setDateofexecution(Date dateofexecution) {
		this.dateofexecution = dateofexecution;
	}
	public Integer getRunid() {
		return runid;
	}
	public void setRunid(Integer runid) {
		this.runid = runid;
	}
	public String getRunname() {
		return runname;
	}
	public void setRunname(String runname) {
		this.runname = runname;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

}
