package com.apmosys.tmt;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Component;

import com.apmosys.tmt.models.UserDetailsEntity;
import com.apmosys.tmt.models.UserDetailsEntityDTO;

@Component
public class Codec {

	public String decrypt(String encrypted) throws Exception {
		String key1 = "IjfgGhpOiLjbrtJf";
		String key2 = "pOiJlKmmHgFvdStf";
		IvParameterSpec iv = new IvParameterSpec(key2.getBytes("UTF-8"));

		SecretKeySpec skeySpec = new SecretKeySpec(key1.getBytes("UTF-8"), "AES");

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
		byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted.getBytes()));

		return new String(original);
	}

	public String encrypt(String encrypted) throws Exception {
		String key1 = "IjfgGhpOiLjbrtJf";
		String key2 = "pOiJlKmmHgFvdStf";
		IvParameterSpec iv = new IvParameterSpec(key2.getBytes("UTF-8"));

		SecretKeySpec skeySpec = new SecretKeySpec(key1.getBytes("UTF-8"), "AES");

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
		byte[] ctbytes = cipher.doFinal(encrypted.getBytes());
		String original = new String(Base64.getEncoder().encode(ctbytes), "UTF-8");
		return original;
	}

	public UserDetailsEntityDTO encryptUserDetails(UserDetailsEntity user) throws Exception {

		System.out.println("Codec.encryptUserDetails()" + user);
		UserDetailsEntityDTO encryptedUser = new UserDetailsEntityDTO();

		String key1 = "IjfgGhpOiLjbrtJf";
		String key2 = "pOiJlKmmHgFvdStf";
		IvParameterSpec iv = new IvParameterSpec(key2.getBytes("UTF-8"));

		SecretKeySpec skeySpec = new SecretKeySpec(key1.getBytes("UTF-8"), "AES");

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

		// Encrypt username
		byte[] encryptedUserName = cipher.doFinal(user.getUsername().getBytes());
		String encryptedStringUserName = Base64.getEncoder().encodeToString(encryptedUserName);
		encryptedUser.setUsername(encryptedStringUserName);

		// Encrypt name
		byte[] encryptedName = cipher.doFinal(user.getName().getBytes());
		String encryptedStringName = Base64.getEncoder().encodeToString(encryptedName);
		encryptedUser.setName(encryptedStringName);
		
		// Encrypt Id
		byte[] encryptedId = cipher.doFinal(user.getId().toString().getBytes());
		String encryptedStringId = Base64.getEncoder().encodeToString(encryptedId);
		encryptedUser.setId(encryptedStringId);

		// Encrypt userID
		byte[] encryptedUserID = cipher.doFinal(user.getUserID().getBytes());
		String encryptedStringUserID = Base64.getEncoder().encodeToString(encryptedUserID);
		encryptedUser.setUserID(encryptedStringUserID);

//		// Encrypt password
//		byte[] encryptedPassword = cipher.doFinal(user.getPassword().getBytes());
//		String encryptedStringPassword = Base64.getEncoder().encodeToString(encryptedPassword);
//		encryptedUser.setPassword(encryptedStringPassword);

		// Encrypt userrole
		byte[] encryptedUserRole = cipher.doFinal(user.getUser_role().toString().getBytes());
		String encryptedStringUserRole = Base64.getEncoder().encodeToString(encryptedUserRole);
		encryptedUser.setUser_role(encryptedStringUserRole);
		
		// Encrypt managerID
		byte[] encryptedManagerID = cipher.doFinal(user.getManagerID().toString().getBytes());
		String encryptedStringManagerID = Base64.getEncoder().encodeToString(encryptedManagerID);
		encryptedUser.setManagerID(encryptedStringManagerID);

		// Encrypt token
		byte[] encryptedTokenID = cipher.doFinal(user.getTokenID().toString().getBytes());
		String encryptedStringTokenID = Base64.getEncoder().encodeToString(encryptedTokenID);
		encryptedUser.setTokenID(encryptedStringTokenID);

		// Encrypt isActive
		byte[] encryptedIsActive = cipher.doFinal(user.getIsActive().getBytes());
		String encryptedStringIsActive = Base64.getEncoder().encodeToString(encryptedIsActive);
		encryptedUser.setIsActive(encryptedStringIsActive);

		// Encrypt createdBy
		if (user.getCreatedBy() != null) {
		byte[] encryptedCreatedBy = cipher.doFinal(user.getCreatedBy().toString().getBytes());
		String encryptedStringCreatedBy = Base64.getEncoder().encodeToString(encryptedCreatedBy);
		encryptedUser.setCreatedBy(encryptedStringCreatedBy);
		}else {
			encryptedUser.setCreatedBy("");
		}

		System.err.println(encryptedUser);
		return encryptedUser;
	}

	public String getMacId() {
		try {
			String OSName = System.getProperty("os.name");
			if (OSName.contains("Windows")) {
				return (getMAC4Windows());
			} else {
				String mac = getMAC4Linux("eth0");
				if (mac == null) {
					mac = getMAC4Linux("eth1");
					if (mac == null) {
						mac = getMAC4Linux("eth2");
						if (mac == null) {
							mac = getMAC4Linux("wlan0");
						}
					}
				}
				return mac;
			}
		} catch (Exception E) {
			System.err.println("System Mac Exp : " + E.getMessage());
			return null;
		}
	}

	private String getMAC4Linux(String name) {
		try {
			NetworkInterface network = NetworkInterface.getByName(name);
			byte[] mac = network.getHardwareAddress();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
			}
			return (sb.toString());
		} catch (Exception E) {
			System.err.println("System Linux MAC Exp : " + E.getMessage());
			return null;
		}
	}

	private String getMAC4Windows() {
		try {
			InetAddress addr = InetAddress.getLocalHost();
			NetworkInterface network = NetworkInterface.getByInetAddress(addr);

			byte[] mac = network.getHardwareAddress();

			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
			}

			return sb.toString();
		} catch (Exception E) {
			E.printStackTrace();
			return null;
		}
	}

	public String IP_HOST() throws Exception {
		String ip = null;
		String hostname = "";
		String IP_HOST_ZONE = "";
		try {
			InetAddress address = InetAddress.getLocalHost();
//			ip = address.getHostAddress();
//			hostname = address.getHostName();
			// IP_HOST_ZONE = "10.255.100.21" + "TMT" + "apmosys_tmt_app";
			IP_HOST_ZONE = "10.0.11.249" + "TMT" + "MMFSS-LOAD-GN-5";
//aprientis
//			IP_HOST_ZONE = "10.0.11.249" + "TMT" + "MMFSS-LOAD-GN-5";//mahindra automation
//			IP_HOST_ZONE = "3.109.128.184" + "TMT" + "ip-172-31-44-134";
//			IP_HOST_ZONE = "150.242.14.145" + "TMT" + "ApMosysrabale2.rapidns.com";//150 server
//			IP_HOST_ZONE = "10.10.202.104" + "TMT" + "MF-UCP-UAT-APP-01";//mahindra private ip dont use
//			IP_HOST_ZONE = "10.10.127.31" + "TMT" + "MF-UCP-UAT-APP-01";//mahindra
//			IP_HOST_ZONE = "172.31.44.134" + "TMT" + "ip-172-31-44-134"; //real appreciate
//			Dj4DVTsdH6TFoV7fHp+XMjuEk0SrH4m7T8YSUoA2U9FAu5i8p4SInaWGt9wfSauB
//					IP_HOST_ZONE = "172.30.24.19" + "TMT" + "apmosys_tmt_app";
//					IP_HOST_ZONE = "150.242.14.145" + "TMT" + "ApMosysrabale2.rapidns.com";
			// IP_HOST_ZONE = "150.242.14.145" + "TMT" + "ApMosysrabale2.rapidns.com";
//			IP_HOST_ZONE = "10.184.12.38" + "TMT" + "utkmumptmtapp01.utkarsh.bank";//utaesh banc
//			IP_HOST_ZONE = ip + "TMT" + hostname;

		} catch (UnknownHostException e) {

			e.printStackTrace();
		}
		// TIME ZONE
		return IP_HOST_ZONE;
	}

	public static void main(String args[]) throws Exception {
		Codec codec = new Codec();
		String macId = codec.IP_HOST();
		System.out.println(macId);
		String separator = "TMT";
		String encrypted = "12/12/2023";
//		String encrypted = "10/08/2022";
		String key = macId + separator + encrypted;
		// ip+TMT+Host+TMT+Date
		String encrypt = codec.encrypt(key);
		System.out.println(encrypt);
//		System.out.println(codec.decrypt("1em9k/xy8hWXPI+dAudGLbRatc0VYgbiY4FPDmi6B3sW70oAfWmU88ll044S5zGa"));
		System.out.println(codec.decrypt(encrypt));
	}
}