package com.apmosys.tmt.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.map.HashedMap;
import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.BO.MannualTestGroupAndCase;
import com.apmosys.tmt.models.MannualTestCaseTable;
import com.apmosys.tmt.models.MannualTestGroup;
import com.apmosys.tmt.repository.MannualTestGroupRepo;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Repository
@Transactional
public class MannualTestGroupDAO {
	@PersistenceContext
    private EntityManager entityManager;

	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	MannualTestGroupRepo mannualTestGroupRepo;

	public MannualTestGroup createTestGroup(MannualTestGroup mannualTestGroup) {
		MannualTestGroup testGroup = null;	
		testGroup = mannualTestGroupRepo.save(mannualTestGroup);		
		return testGroup;
	}
	public MannualTestGroup checkAndCreateGroup(MannualTestGroup mannualTestGroup) {
		MannualTestGroup group=null;
		group=mannualTestGroupRepo.findTestGroupBySuitesAndGroupName(mannualTestGroup.getTestSuitesId(), mannualTestGroup.getGroupName());
		if(group==null) {
			group = mannualTestGroupRepo.save(mannualTestGroup);
		}
		return group;
		
	}

	public MannualTestGroup updateTestGroup(MannualTestGroup testGroup) {
		MannualTestGroup mannualTestGroup = mannualTestGroupRepo.findById(testGroup.getTestGroupId()).orElse(null);
		mannualTestGroup.setGroupName(testGroup.getGroupName());
		mannualTestGroup.setUpdatedBy(testGroup.getUpdatedBy());
		mannualTestGroup.setIsActive(testGroup.getIsActive());
		mannualTestGroup.setUpdatedDate(new Date());
		testGroup = mannualTestGroupRepo.save(mannualTestGroup);
		return testGroup;
	}

	public List<MannualTestGroup> getTestGroup(Integer testSuitesId) {
		List<MannualTestGroup> testGroup = mannualTestGroupRepo.findTestGroupByProjectId(testSuitesId);
		return testGroup;
	}

	public MannualTestGroup getTestGroupDetails(Integer groupId) {
		return mannualTestGroupRepo.findById(groupId).orElse(null);
	}

	public Map<String, MannualTestGroupAndCase> getTestGroupAndCases(Integer testSuitesId) {
		List<Object[]> list = null;
		List<MannualTestCaseTable> testSuitesDeatils = new ArrayList<MannualTestCaseTable>();
		Map<String, MannualTestGroupAndCase> maps = new HashedMap<String, MannualTestGroupAndCase>();
		String overViewProjectHQL = "SELECT mts.testSuitesId,mts.testSuitesName,mg.groupName,mt.SRID,"
				+ "mt.testGroupId,mt.srNo,mt.testID,mt.dateOfExec,\r\n"
				+ "mt.browser,mt.functionality,mt.type,mt.testCaseType,mt.testDescription,"
				+ "mt.testData,mt.steps,mt.expectedResult,mt.actualRsult,\r\n"
				+ "mt.status,mt.testerComment,mt.testerName,mt.jiraID,mt.module,mt.subModule,mt.scenarioID,"
				+ "mt.scenarios,mt.executionTime,mg.testGroupId as gpId,mg.isActive,\r\n"
				+ "mt.action, mt.bug_ID, mt.bug_status, mt.preparation_time FROM Mannual_TestSuites_Table mts\r\n"
				+ "LEFT JOIN Mannual_TestGroup_Table mg ON mts.testSuitesId=mg.testSuitesId\r\n"
				+ "LEFT JOIN Mannual_TestCase_Table mt ON mg.testGroupId =mt.testGroupId\r\n"
				+ "WHERE mts.testSuitesId=:testSuitesId order by mt.srID ASC";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("testSuitesId", testSuitesId);
			list = query.list();
			for (Object[] objects : list) {
				MannualTestCaseTable details = new MannualTestCaseTable();
				details.setTestSuitesId((Integer) objects[0]);
				details.setTestSuitesName((String) objects[1]);
				details.setTestGroupName((String) objects[2]);
				details.setTestGroupId((Integer) objects[26]);
				details.setIsActive((String) objects[27]);

				details.setSrID((Integer) objects[3]);
				if (objects[5] != null)
					details.setSrNo(((Integer) objects[5]).toString());
				details.setTestID((String) objects[6]);
				details.setTestDescription((String) objects[12]);
				if ((String) objects[8] == null) {
					details.setBrowser("");
				} else {
					details.setBrowser((String) objects[8]);
				}

				details.setFunctionality((String) objects[9]);
				if ((String) objects[11] == null) {
					details.setTestCaseType("");
				} else {
					details.setTestCaseType((String) objects[11]);
				}

				details.setExpectedResult((String) objects[15]);
				details.setTestData((String) objects[13]);
				details.setSteps((String) objects[14]);
				details.setScenarios((String) objects[24]);
				details.setScenarioID((String) objects[23]);
				details.setAction((objects[28]!=null)?(String) objects[28]:null);
				details.setBugID((objects[29]!=null)?objects[29].toString():null);
				details.setBugStatus((objects[30]!=null)?(String) objects[30]:null);
				details.setStatus((objects[17]!=null)?(String) objects[17]:null);
				details.setPreparationTime((objects[31]!=null)?((Integer) objects[31]).longValue():null);
				testSuitesDeatils.add(details);
				session.flush();
				// System.out.println(details);
			}
			for (MannualTestCaseTable details1 : testSuitesDeatils) {
				if (details1 != null) {
					MannualTestGroupAndCase groupAndCase = new MannualTestGroupAndCase();
					List<MannualTestCaseTable> values = testSuitesDeatils.stream()
							.filter(x -> x.getTestGroupName().equals(details1.getTestGroupName()))
							.collect(Collectors.toList());
					groupAndCase.setGroupId(details1.getTestGroupId());
					groupAndCase.setIsActive(details1.getIsActive());
					if (groupAndCase.getGroupId() != null) {
						groupAndCase.setTestCaseList(values);
					}
					maps.put(details1.getTestGroupName(), groupAndCase);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return maps;
	}

	public Map<String, MannualTestGroupAndCase> getGroupTestCasesBysuitesFiltered(Integer testSuitesId, String colName, String colValue) {
		List<Object[]> list = null;
		List<MannualTestCaseTable> testSuitesDeatils = new ArrayList<MannualTestCaseTable>();
		Map<String, MannualTestGroupAndCase> maps = new HashedMap<String, MannualTestGroupAndCase>();
		String condition="";
		if(colName.equals("status")) {
			condition="AND mt.status=\"Fail\"";
		}
		else if(colName.equals("bugStatus")) {
			condition="AND mt.bug_status=\"Resolved\"";
		}
		String overViewProjectHQL = "SELECT mts.testSuitesId,mts.testSuitesName,mg.groupName,mt.SRID,"
				+ "mt.testGroupId,mt.srNo,mt.testID,mt.dateOfExec,\r\n"
				+ "mt.browser,mt.functionality,mt.type,mt.testCaseType,mt.testDescription,"
				+ "mt.testData,mt.steps,mt.expectedResult,mt.actualRsult,\r\n"
				+ "mt.status,mt.testerComment,mt.testerName,mt.jiraID,mt.module,mt.subModule,mt.scenarioID,"
				+ "mt.scenarios,mt.executionTime,mg.testGroupId as gpId,mg.isActive,\r\n"
				+ "mt.action, mt.bug_ID, mt.bug_status FROM Mannual_TestSuites_Table mts\r\n"
				+ "LEFT JOIN Mannual_TestGroup_Table mg ON mts.testSuitesId=mg.testSuitesId\r\n"
				+ "LEFT JOIN Mannual_TestCase_Table mt ON mg.testGroupId =mt.testGroupId\r\n"
				+ "WHERE mts.testSuitesId=:testSuitesId "+condition+" order by mt.srID ASC";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("testSuitesId", testSuitesId);
			list = query.list();
			for (Object[] objects : list) {
				MannualTestCaseTable details = new MannualTestCaseTable();
				details.setTestSuitesId((Integer) objects[0]);
				details.setTestSuitesName((String) objects[1]);
				details.setTestGroupName((String) objects[2]);
				details.setTestGroupId((Integer) objects[26]);
				details.setIsActive((String) objects[27]);

				details.setSrID((Integer) objects[3]);
				if (objects[5] != null)
					details.setSrNo(((Integer) objects[5]).toString());
				details.setTestID((String) objects[6]);
				details.setTestDescription((String) objects[12]);
				if ((String) objects[8] == null) {
					details.setBrowser("");
				} else {
					details.setBrowser((String) objects[8]);
				}

				details.setFunctionality((String) objects[9]);
				if ((String) objects[11] == null) {
					details.setTestCaseType("");
				} else {
					details.setTestCaseType((String) objects[11]);
				}

				details.setExpectedResult((String) objects[15]);
				details.setTestData((String) objects[13]);
				details.setSteps((String) objects[14]);
				details.setScenarios((String) objects[24]);
				details.setScenarioID((String) objects[23]);
				details.setAction((objects[28]!=null)?(String) objects[28]:null);
				details.setBugID((objects[29]!=null)?objects[29].toString():null);
				details.setBugStatus((objects[30]!=null)?(String) objects[30]:null);
				details.setStatus((objects[17]!=null)?(String) objects[17]:null);
				testSuitesDeatils.add(details);
				session.flush();
				// System.out.println(details);
			}
			for (MannualTestCaseTable details1 : testSuitesDeatils) {
				if (details1 != null) {
					MannualTestGroupAndCase groupAndCase = new MannualTestGroupAndCase();
					List<MannualTestCaseTable> values = testSuitesDeatils.stream()
							.filter(x -> x.getTestGroupName().equals(details1.getTestGroupName()))
							.collect(Collectors.toList());
					groupAndCase.setGroupId(details1.getTestGroupId());
					groupAndCase.setIsActive(details1.getIsActive());
					if (groupAndCase.getGroupId() != null) {
						groupAndCase.setTestCaseList(values);
					}
					maps.put(details1.getTestGroupName(), groupAndCase);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return maps;
	}
	
	public List<MannualTestCaseTable> getAllTestCassesByTestRun(Integer reRunId) {
		List<Object[]> list = null;
		List<MannualTestCaseTable> testSuitesDeatils = new ArrayList<MannualTestCaseTable>();
		String overViewProjectHQL = "SELECT mts.testSuitesId,mts.testSuitesName,mg.groupName,"
				+ "			mt.id,mt.projectId,mt.reRunId,mt.testSuitesId as suitesId,mt.SRID,"
				+ "			mt.srNo,mt.module,mt.subModule,mt.scenarioID,mt.scenarios,"
				+ "			mt.functionality,mt.testID,mt.testCaseType,mt.testDescription,"
				+ "			mt.browser,mt.testData,mt.steps,mt.expectedResult,mt.STATUS,"
				+ "			mt.isActive, mg.testGroupId as gpId, mt.absoluteSrID, mt.bugId \r\n"
				+ "			FROM Mannual_TestSuites_Table mts\r\n"
				+ "			INNER JOIN Mannual_TestGroup_Table mg ON mts.testSuitesId=mg.testSuitesId\r\n"
				+ "			INNER JOIN MannualTestCaseExecutionLogs mt ON mg.testGroupId =mt.testGroupId\r\n"
				+ "			INNER JOIN MannualReRunTable mre ON mre.id=mt.reRunId\r\n"
				+ "	      WHERE mt.reRunId=:reRunId And mt.SRID IS NOT NULL and mt.absoluteSrID IS NOT NULL"
				+ "		  order by mt.srID ASC";
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("reRunId", reRunId);
			list = query.list();
			System.out.println("get list::::::::;---test run-----" + list);
			for (Object[] objects : list) {
				try
				{
				MannualTestCaseTable details = new MannualTestCaseTable();
				details.setTestSuitesId((Integer) objects[0]);
				details.setTestSuitesName((String) objects[1]);
				details.setTestGroupName((String) objects[2]);
				details.setTestGroupId((Integer) objects[23]);
				details.setSrID((Integer) objects[3]);
				if (objects[7] != null)
					details.setSrNo(((Integer) objects[7]).toString());
				details.setTestID((String) objects[14]);
				details.setTestDescription((String) objects[16]);
				details.setBrowser((String) objects[17]);
				details.setFunctionality((String) objects[13]);
				details.setTestCaseType((String) objects[15]);
				details.setExpectedResult((String) objects[20]);
				details.setTestData((String) objects[18]);
				details.setSteps((String) objects[19]);
				details.setScenarios((String) objects[12]);
				details.setScenarioID((String) objects[11]);
				details.setAbsoluteTCID((Integer) objects[24]);
				if((objects[25]==null)||(objects[25].equals(""))) {
					details.setBugID(null);
				}
				else {
					details.setBugID(objects[25].toString());
				}
				//details.setBugID((objects[25]!=null)?Integer.valueOf(objects[25].toString()):null);
					/*
					 * details.setTestSuitesId((Integer) objects[0]);
					 * details.setTestSuitesName((String) objects[1]);
					 * details.setTestGroupName((String) objects[2]);
					 * details.setTestGroupId((Integer) objects[26]); details.setSrID((Integer)
					 * objects[3]); if (objects[5] != null) details.setSrNo(((Integer)
					 * objects[5]).toString()); details.setTestID((String) objects[6]);
					 * details.setTestDescription((String) objects[12]); details.setBrowser((String)
					 * objects[8]); details.setFunctionality((String) objects[9]);
					 * details.setTestCaseType((String) objects[11]);
					 * details.setExpectedResult((String) objects[15]); details.setTestData((String)
					 * objects[13]); details.setSteps((String) objects[14]);
					 * details.setScenarios((String) objects[24]); details.setScenarioID((String)
					 * objects[23]);
					 */
				testSuitesDeatils.add(details);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
		} catch (Exception e) {
			e.printStackTrace();

		}
		return testSuitesDeatils;
	}

	public List<MannualTestCaseTable> getAllTestCassesBySuitesId(Integer testSuitesId) {
		List<Object[]> list = null;
		List<MannualTestCaseTable> testSuitesDeatils = new ArrayList<MannualTestCaseTable>();
		String overViewProjectHQL = "SELECT mts.testSuitesId,mts.testSuitesName,mg.groupName,mt.SRID,"
				+ "mt.testGroupId,mt.srNo,mt.testID,mt.dateOfExec,\r\n" + 
				"mt.browser,mt.functionality,mt.type,mt.testCaseType,mt.testDescription,mt.testData,"
				+ "mt.steps,mt.expectedResult,mt.actualRsult,\r\n" + 
				"mt.status,mt.testerComment,mt.testerName,mt.jiraID,mt.module,mt.subModule,mt.scenarioID,"
				+ "mt.scenarios,mt.executionTime,mg.testGroupId as gpId,\r\n"
				+ "mt.bug_ID, mt.bug_severity, mt.bug_priority FROM Mannual_TestSuites_Table mts\r\n"
				+ "LEFT JOIN Mannual_TestGroup_Table mg ON mts.testSuitesId=mg.testSuitesId\r\n"
				+ "LEFT JOIN Mannual_TestCase_Table mt ON mg.testGroupId =mt.testGroupId\r\n"
				+ "WHERE mts.testSuitesId=:testSuitesId AND mt.SRID IS NOT null order by mt.srID ASC";
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("testSuitesId", testSuitesId);
			list = query.list();
			System.out.println("get list::::::::;--------"+list);
			for (Object[] objects : list) {
				try
				{
				MannualTestCaseTable details = new MannualTestCaseTable();
				details.setTestSuitesId((Integer) objects[0]);
				details.setTestSuitesName((String) objects[1]);
				details.setTestGroupName((String) objects[2]);
				details.setTestGroupId((Integer) objects[26]);
				details.setSrID((Integer) objects[3]);
				if (objects[5] != null)
					details.setSrNo(((Integer) objects[5]).toString());
				details.setTestID((String) objects[6]);
				details.setTestDescription((String) objects[12]);
				details.setBrowser((String) objects[8]);
				details.setFunctionality((String) objects[9]);
				details.setTestCaseType((String) objects[11]);
				details.setExpectedResult((String) objects[15]);
				details.setTestData((String) objects[13]);
				details.setSteps((String) objects[14]);
				details.setScenarios((String) objects[24]);
				details.setScenarioID((String) objects[23]);
				details.setBugID( (String) objects[27]);
				details.setBugSeverity((String) objects[28]);
				details.setBugPriority((String) objects[29]);
				testSuitesDeatils.add(details);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
			}
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
		} catch (Exception e) {
			e.printStackTrace();

		}
		return testSuitesDeatils;
	}

	public List<MannualTestCaseTable> getFailTestCasesByReRunId(Integer reRunId, Integer runId) {
		List<Object[]> list = null;
		List<MannualTestCaseTable> testSuitesDeatils = new ArrayList<MannualTestCaseTable>();
		String overViewProjectHQL = "SELECT mts.testSuitesId,mts.testSuitesName,"
				+ "mg.groupName,mt.id,mt.projectId,mt.reRunId,"
				+ "mt.testSuitesId as suitesId,mt.SRID,mt.srNo,mt.module,mt.subModule,"
				+ "mt.scenarioID,mt.scenarios,mt.functionality,mt.testID,"
				+ "mt.testCaseType,mt.testDescription,mt.browser,mt.testData,"
				+ "mt.steps,mt.expectedResult,mt.STATUS,mt.isActive, "
				+ "mg.testGroupId as gpId, mt.absoluteSrID, mt.bugId \r\n"
				+ ", mt.bugSeverity, mt.bugPriority FROM Mannual_TestSuites_Table mts\r\n"
				+ "INNER JOIN Mannual_TestGroup_Table mg ON mts.testSuitesId=mg.testSuitesId\r\n"
				+ "INNER JOIN MannualTestCaseExecutionLogs mt ON mg.testGroupId =mt.testGroupId\r\n"
				+ "INNER JOIN MannualReRunTable mre ON mre.id=mt.reRunId\r\n"
				+ "WHERE mt.reRunId=:reRunId And mt.SRID IS NOT NULL AND "
				+ "(mt.STATUS=:status) AND mt.testCaseAction NOT IN (:action)"
				+ " and mt.absoluteSrID IS NOT NULL order by mt.srID ASC";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("reRunId", reRunId);
			query.setParameter("status", "Fail");
			query.setParameter("action", "Not Started");
			list = query.list();
			for (Object[] objects : list) {
				MannualTestCaseTable details = new MannualTestCaseTable();
				details.setTestSuitesId((Integer) objects[0]);
				details.setTestSuitesName((String) objects[1]);
				details.setTestGroupName((String) objects[2]);
				details.setTestGroupId((Integer) objects[23]);
				details.setSrID((Integer) objects[3]);
				if (objects[7] != null)
					details.setSrNo(((Integer) objects[7]).toString());
				details.setTestID((String) objects[14]);
				details.setTestDescription((String) objects[16]);
				details.setBrowser((String) objects[17]);
				details.setFunctionality((String) objects[13]);
				details.setTestCaseType((String) objects[15]);
				details.setExpectedResult((String) objects[20]);
				details.setTestData((String) objects[18]);
				details.setSteps((String) objects[19]);
				details.setScenarios((String) objects[12]);
				details.setScenarioID((String) objects[11]);
				details.setAbsoluteTCID((Integer) objects[24]);
				details.setBugID((objects[25]!=null)?objects[25].toString():null);
				details.setBugSeverity((objects[26]!=null)?objects[26].toString():null);
				details.setBugPriority((objects[27]!=null)?objects[26].toString():null);
				
				testSuitesDeatils.add(details);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return testSuitesDeatils;
	}
	
	public List<MannualTestCaseTable> getFailResolvedTestCasesByReRunId(Integer reRunId, Integer runId) {
		List<Object[]> list = null;
		List<MannualTestCaseTable> testSuitesDeatils = new ArrayList<MannualTestCaseTable>();
		String overViewProjectHQL = "SELECT mts.testSuitesId,mts.testSuitesName,mg.groupName,"
				+ "mt.id,mt.projectId,mt.reRunId,\r\n" + 
				"	mt.testSuitesId as suitesId,mt.SRID,mt.srNo,mt.module,mt.subModule,mt.scenarioID,mt.scenarios,\r\n" + 
				"	mt.functionality,mt.testID,mt.testCaseType,mt.testDescription,mt.browser,mt.testData,\r\n" + 
				"	mt.steps,mt.expectedResult,mt.STATUS,mt.isActive,mg.testGroupId as gpId, mt.absoluteSrID,\r\n" + 
				"	mt.bugId, mt.bugSeverity, mt.bugPriority FROM Mannual_TestSuites_Table mts INNER JOIN Mannual_TestGroup_Table mg ON mts.testSuitesId=mg.testSuitesId\r\n" + 
				"	INNER JOIN MannualTestCaseExecutionLogs mt ON mg.testGroupId =mt.testGroupId INNER JOIN \r\n" + 
				"	MannualReRunTable mre ON mre.id=mt.reRunId WHERE mt.reRunId=:reRunId And mt.SRID IS NOT NULL AND \r\n" + 
				"	(mt.STATUS=:status) AND mt.testCaseAction NOT IN (:action) and mt.absoluteSrID IS NOT NULL \r\n" + 
				"	and mt.bugId IS NOT NULL AND mt.bugStatus=:bugStatus order by mt.srID ASC;";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("reRunId", reRunId);
			query.setParameter("status", "Fail");
			query.setParameter("action", "Not Started");
			query.setParameter("bugStatus", "Resolved");
			list = query.list();
			for (Object[] objects : list) {
				MannualTestCaseTable details = new MannualTestCaseTable();
				details.setTestSuitesId((Integer) objects[0]);
				details.setTestSuitesName((String) objects[1]);
				details.setTestGroupName((String) objects[2]);
				details.setTestGroupId((Integer) objects[23]);
				details.setSrID((Integer) objects[3]);
				if (objects[7] != null)
					details.setSrNo(((Integer) objects[7]).toString());
				details.setTestID((String) objects[14]);
				details.setTestDescription((String) objects[16]);
				details.setBrowser((String) objects[17]);
				details.setFunctionality((String) objects[13]);
				details.setTestCaseType((String) objects[15]);
				details.setExpectedResult((String) objects[20]);
				details.setTestData((String) objects[18]);
				details.setSteps((String) objects[19]);
				details.setScenarios((String) objects[12]);
				details.setScenarioID((String) objects[11]);
				details.setAbsoluteTCID((Integer) objects[24]);
				details.setBugID((objects[25]!=null)?objects[25].toString():null);
				details.setBugSeverity((objects[26]!=null)?objects[26].toString():null);
				details.setBugPriority((objects[27]!=null)?objects[26].toString():null);
				testSuitesDeatils.add(details);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return testSuitesDeatils;
	}
}


