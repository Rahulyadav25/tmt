package com.apmosys.tmt.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.MannualTestSuites;
import com.apmosys.tmt.models.OverViewTestSuitesDeatils;
import com.apmosys.tmt.repository.MannualTestSuitesRepo;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
@Repository
@Transactional
public class MannualTestSuitesDAO {
	@PersistenceContext
    private EntityManager entityManager;
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	MannualTestSuitesRepo mannualTestSuitesDRepo;
	
	public MannualTestSuites createTestSuites(MannualTestSuites suites) 
	{
		return mannualTestSuitesDRepo.save(suites);
	}
	
	public MannualTestSuites checkAndCreateSuiteBySuiteName(MannualTestSuites mannualTestSuites) 
	{
		MannualTestSuites suite=null;
		suite=mannualTestSuitesDRepo.findByTestSuitesNameAndProjectIdAndIsDeleted(mannualTestSuites.getTestSuitesName(),mannualTestSuites.getProjectId(),"N");
		if(suite==null)
		{
			System.out.println("in dao to check suites saving data"); 
			suite = mannualTestSuitesDRepo.save(mannualTestSuites);
			suite.setCheckForTestSuite("newly Created");
		}
		else
		{
			System.out.println("mannualTestSuites.getTestSuitesName()..."+mannualTestSuites.getTestSuitesName());
			if(suite.getIsDeleted().equals("Y"))
			{
				System.out.println("in dao to check suites saving data"); 
				suite = mannualTestSuitesDRepo.save(mannualTestSuites);
				suite.setCheckForTestSuite("newly Created");
			}
			else
			{
				suite.setCheckForTestSuite("TestSuite already exists");
			}
			
		}
		return suite;
		
	}

	/*
	 * public String updateDeletedTestSuite(String updatedByEmail, String
	 * testsuitesname) { Session session=entityManager.unwrap(Session.class);
	 * Transaction tx=session.beginTransaction(); //ProjectDeatilsMeta
	 * accessEntity=new ProjectDeatilsMeta(); String msg="Fail"; try {
	 * session.setCacheMode(CacheMode.IGNORE); String sql =
	 * "UPDATE MannualTestSuites SET isDeleted='N', updatedDate=NOW(), updatedBy=:updatedByEmail WHERE testSuitesName=:suitesName"
	 * ; Query query = session.createNativeQuery(sql);
	 * query.setParameter("updatedByEmail", updatedByEmail);
	 * query.setParameter("suitesName",testsuitesname); query.executeUpdate();
	 * tx.commit(); msg="Success"; } catch(Exception e) { e.printStackTrace();
	 * msg="Fail"; } return msg; }
	 */
	
	public MannualTestSuites checkAndCreateSuites(MannualTestSuites mannualTestSuites) {
		MannualTestSuites suite=null;
		System.out.println("mannualTestSuites.getTestSuitesName()..."+mannualTestSuites.getTestSuitesName()+"..."+mannualTestSuites.getProjectId()); 
		suite=mannualTestSuitesDRepo.findTestSuiteBySuiteName(mannualTestSuites.getTestSuitesName(),mannualTestSuites.getProjectId());
		System.out.println("suite.."+suite);
		if(suite==null) 
		{
			System.out.println("In dao to check suiteso saving data"); 
			suite = mannualTestSuitesDRepo.save(mannualTestSuites);
		}
		return suite;
		
	}
	
	/*
	 * public MannualTestSuites checkAndCreateSuites1(MannualTestSuites suites) {
	 * MannualTestSuites suiteDummy=null; MannualTestSuites
	 * suitesObj=mannualTestSuitesDRepo.findTestSuiteBySuiteName(suites.
	 * getTestSuitesName());
	 * 
	 * System.out.println("in dao to check suiteso "+suitesObj.getTestSuitesId());
	 * if(suitesObj==null) {
	 * System.out.println("in dao to check suiteso saving data"); suiteDummy =
	 * mannualTestSuitesDRepo.save(suites); return suiteDummy; } return suitesObj;
	 * 
	 * }
	 */
	  
	public String updateTestSuites(MannualTestSuites suites) {
		MannualTestSuites testSuites=mannualTestSuitesDRepo.findById(suites.getTestSuitesId()).orElse(null);		
		String status=null;
		System.out.println("111............................");
		MannualTestSuites testsuitesname=mannualTestSuitesDRepo.findByTestSuitesNameAndProjectIdAndIsDeleted(suites.getTestSuitesName(),suites.getProjectId(),"N");
			
		if(testSuites.getTestSuitesName().equals(suites.getTestSuitesName()))
		{
			System.out.println("2222..11111.....");
			testSuites.setTestSuitesName(suites.getTestSuitesName());
			testSuites.setIsActive(suites.getIsActive());
			testSuites.setUpdatedDate(new Date());
			testSuites.setUpdatedBy(suites.getUpdatedBy());
			mannualTestSuitesDRepo.save(testSuites);
			status="TestSuite Updated Successfully";

		}
		else if(testsuitesname==null) {
		//MannualTestSuites testSuites=mannualTestSuitesDRepo.findOne(suites.getTestSuitesId());		
		testSuites.setTestSuitesName(suites.getTestSuitesName());
		testSuites.setIsActive(suites.getIsActive());
		testSuites.setUpdatedDate(new Date());
		testSuites.setUpdatedBy(suites.getUpdatedBy());
		System.out.println("2222222222222............................");

		mannualTestSuitesDRepo.save(testSuites);
		status="TestSuite Updated Successfully";
		}
		else {
			System.out.println("TestSuite already exists");
			status="TestSuite already exists";
		}
		return status;
	}
	
	/*
	 * public List<MannualTestSuites> findByTestSuitesName(String suitesname) {
	 * return mannualTestSuitesDRepo.findByTestSuitesName(suitesname);
	 * 
	 * }
	 */
	public MannualTestSuites getTestSuitsDetails(Integer testSuitesId) {
		return mannualTestSuitesDRepo.findById(testSuitesId).orElse(null);
	}

	
	public List<MannualTestSuites> getTestSuitsByPid(Integer projectID) {
		return mannualTestSuitesDRepo.findTestSuitesByPID(projectID);
	}
	
	public List<OverViewTestSuitesDeatils> getTestSuitesByProject(Integer projectID) {
		List<Object[]> list=null;
		List<OverViewTestSuitesDeatils> testSuitesDeatils=new ArrayList<OverViewTestSuitesDeatils>();
		String overViewProjectHQL="SELECT p.projectID,p.projectName, mts.testSuitesId,mts.testSuitesName,count(DISTINCT mg.testGroupId),count(DISTINCT mt.SRID)\r\n" + 
				",mts.isActive FROM Project_MetaTable p INNER JOIN  Mannual_TestSuites_Table mts ON p.projectID=mts.projectId LEFT join Mannual_TestGroup_Table mg ON mts.testSuitesId=mg.testSuitesId left join Mannual_TestCase_Table mt ON mg.testGroupId =mt.testGroupId\r\n" + 
				"WHERE p.projectID=:projectId AND mts.isDeleted='N' GROUP BY mts.testSuitesId  order by mts.createdDate desc";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query= session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectId", projectID);
			list=query.list();
			for (Object[] objects : list) {
				OverViewTestSuitesDeatils details=new OverViewTestSuitesDeatils();
				details.setProjectId((Integer) objects[0]);
				details.setProjectName((String) objects[1]);
				details.setTestSuitesId((Integer) objects[2]);
				details.setTestSuitesName((String) objects[3]);	
//				details.setTestGroupNo(Long.valueOf(((BigInteger)objects[4]).toString()));
//				details.setTestCaseNo(Long.valueOf(((BigInteger)objects[5]).toString()));
				
				details.setTestGroupNo(Long.valueOf((objects[4]).toString()));
				details.setTestCaseNo (Long.valueOf ((objects[5]).toString()));
				
				
				
				details.setIsActive((String) objects[6]);	
				testSuitesDeatils.add(details);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return testSuitesDeatils; 
	}
}
