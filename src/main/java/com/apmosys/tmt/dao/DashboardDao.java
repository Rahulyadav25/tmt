package com.apmosys.tmt.dao;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.TestSuitesData;
import com.apmosys.tmt.models.dashboardprojectdetailsAuto;
import com.apmosys.tmt.models.dashboardprojectwisedetail;
import com.apmosys.tmt.models.maindashboardproject;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Repository
@Transactional
public class DashboardDao {
	@PersistenceContext
    private EntityManager entityManager;
	@Autowired
	SessionFactory sessionFactory;
	
	private static DecimalFormat df2 = new DecimalFormat("#.##");

	public List<dashboardprojectwisedetail> projectviewdata(int projectid, String isActive) {
		List<Object[]> list =null;
		String delay="";
		List<dashboardprojectwisedetail> dashboardprojectwisedetail = new ArrayList<>();
		
		String overViewProjectHQL_03102020 ="SELECT count(*) as totalcases,:projectid, \n" + 
				"(select projectName from Project_MetaTable where projectID=:projectid) as projectname,\n" + 
				"(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN( \n" + 
				"					SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in(\n" + 
				"		SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=:projectid AND mt.isActive='Y' AND mt.isDeleted='N') \n" + 
				"		AND isactive='Y') AND mc.status='Fail') as Total_Fail, \n" + 
				"(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN( SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in(\n" + 
				"		SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=:projectid AND mt.isActive='Y' AND mt.isDeleted='N') \n" + 
				"		AND isactive='Y') AND mc.status='Pass') as Total_Pass,\n" + 
				"count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed, \n" + 
				"count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold', \n" + 
				"count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review', \n" + 
				"count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started',\n" + 
				"(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN( \n" + 
				"					SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in(\n" + 
				"		SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=:projectid AND mt.isActive='Y' AND mt.isDeleted='N') \n" + 
				"		AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_severity='Major' AND mc.status='Fail'\n" + 
				"		 AND (mc.bug_status='Open' or	mc.bug_status='Reopen')) AS Major,\n" + 
				"(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN( \n" + 
				"					SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in(\n" + 
				"		SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=:projectid AND mt.isActive='Y' AND mt.isDeleted='N') \n" + 
				"		AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_severity='Minor' AND mc.status='Fail'\n" + 
				"		 AND (mc.bug_status='Open' or	mc.bug_status='Reopen')) AS Minor,\n" + 
				"(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN( \n" + 
				"					SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in(\n" + 
				"		SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=:projectid AND mt.isActive='Y' AND mt.isDeleted='N') \n" + 
				"		AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_severity='Critical' AND mc.status='Fail'\n" + 
				"		 AND (mc.bug_status='Open' or	mc.bug_status='Reopen')) AS Critical,\n" + 
				"(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN( \n" + 
				"					SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in(\n" + 
				"		SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=:projectid AND mt.isActive='Y' AND mt.isDeleted='N') \n" + 
				"		AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_severity='Blocker' AND mc.status='Fail'\n" + 
				"		 AND (mc.bug_status='Open' or	mc.bug_status='Reopen')) AS Blocker,		 \n" + 
				"(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN( \n" + 
				"					SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in(\n" + 
				"		SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=:projectid AND mt.isActive='Y' AND mt.isDeleted='N') \n" + 
				"		AND isactive='Y') AND mc.status='Fail' AND mc.ACTION='Completed' AND (mc.bug_severity='Minor' \n" + 
				"		OR mc.bug_severity='Major' OR mc.bug_severity='Critical' OR mc.bug_severity='Blocker')) AS totalbugs,\n" + 
				"sum(mt.executionTime) as Total_secs,\n" + 
				"(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN( \n" + 
				"					SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in(\n" + 
				"		SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=:projectid AND mt.isActive='Y' AND mt.isDeleted='N') \n" + 
				"		AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_status='Open') AS openbug,\n" + 
				"(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN( \n" + 
				"					SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in(\n" + 
				"		SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=:projectid AND mt.isActive='Y' AND mt.isDeleted='N') \n" + 
				"		AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_status='Closed') AS closebug,\n" + 
				"(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN( \n" + 
				"					SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in(\n" + 
				"		SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=:projectid AND mt.isActive='Y' AND mt.isDeleted='N') \n" + 
				"		AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_status='Reopen') AS reopenbug,\n" + 
				"(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN( \n" + 
				"					SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in(\n" + 
				"		SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=:projectid AND mt.isActive='Y' AND mt.isDeleted='N') \n" + 
				"		AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_status='Resolved') AS ResolvedBug,\n" + 
				"count(IF(mt.testCaseAction = 'N/A', 1, NULL)) as 'NA' ,\n" + 
				"count(IF(mt.testCaseAction = 'incomplete', 1, NULL)) as 'Total_action_incomplete' \n" + 
				"FROM MannualRun_Table mr \n" + 
				"	LEFT JOIN MannualReRunTable mre ON mr.id=mre.runId\n" + 
				"	LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mre.id \n" + 
				"where mt.projectId=:projectid and mr.isActive='Y'";
				
			Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL_03102020);
			query.setParameter("projectid", projectid);
			list = query.list();
			for (Object[] objects : list) {
				dashboardprojectwisedetail test=new dashboardprojectwisedetail();
				if(objects[1].toString()==null)
				{
					test.setProjectid(objects[1].toString());
				}
				
				test.setTotaltestcases(Integer.valueOf(objects[0].toString()));
				test.setProjectname(objects[2].toString());
				test.setTotalpass(Integer.valueOf(objects[4].toString()));
				test.setTotalfail(Integer.valueOf(objects[3].toString()));
				test.setTotalnotstarted(Integer.valueOf(objects[8].toString()));
				test.setTotalcompleted(Integer.valueOf(objects[5].toString()));
				test.setTotalonhold(Integer.valueOf(objects[6].toString()));
				test.setTotalunderreview(Integer.valueOf(objects[7].toString()));
				test.setTotalcurrentrunmajor(Integer.valueOf(objects[9].toString()));
				test.setTotalcurrentrunminor(Integer.valueOf(objects[10].toString()));
				test.setTotalcurrentruncritical(Integer.valueOf(objects[11].toString()));
				test.setTotalcurrentrunblocker(Integer.valueOf(objects[12].toString()));
				test.setTotalbugs(Integer.valueOf(objects[13].toString()));
				test.setOpenBug(Integer.valueOf(objects[15].toString()));
				test.setCloseBug(Integer.valueOf(objects[16].toString()));
				test.setReOpenBug(Integer.valueOf(objects[17].toString()));
				test.setResolvedBug(Integer.valueOf(objects[18].toString()));
				test.setTotalNA(Integer.valueOf(objects[19].toString()));
				test.setTotalactionincomplete(Integer.valueOf(objects[20].toString()));
				if(objects[14]!=null)
				{
					try
					{
//						System.out.println();
//						System.out.println("dssss.."+objects[14].toString()+"   ...  projetname"+objects[2].toString()+"..projectis"+projectid);
					
					Double test2=(Double) objects[14];
					long testqq= (new Double(test2)).longValue();
					long sec = testqq % 60;
				    long minutes = testqq % 3600 / 60;
				    long hoursss = testqq % 28800 / 3600;
				    long daysss = testqq / 28800;
				    delay=daysss+"-days "+hoursss+"-hours "+minutes+"-Minute";
				    test.setDayspend((int) daysss);
				    test.setHourspend((int) hoursss);
				    test.setMinutespend((int) minutes);
					
						/*
						 * Double test2=(Double) objects[14]; long seconds= (new
						 * Double(test2)).longValue(); int day = (int)TimeUnit.SECONDS.toDays(seconds);
						 * long hours = TimeUnit.SECONDS.toHours(seconds) - (day *24); long minute =
						 * TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds)*
						 * 60); long second = TimeUnit.SECONDS.toSeconds(seconds) -
						 * (TimeUnit.SECONDS.toMinutes(seconds) *60);
						 * 
						 * delay=day+"-days "+hours+"-hours "+minute+"-Minute";
						 * 
						 * test.setDayspend((int) day); test.setHourspend((int) hours);
						 * test.setMinutespend((int) minute);
						 */
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
				else
				{
					delay=0+"-days "+0+"-hours "+0+"-Minute";
					 test.setDayspend(0);
					    test.setHourspend(0);
					    test.setMinutespend(0);
				}
				
				test.setSetTotalmanhours(delay);
				dashboardprojectwisedetail.add(test);
			 }
		}catch (Exception e) {
			e.printStackTrace();
		}
		return dashboardprojectwisedetail;
	}

	public List<maindashboardproject> projectholeview(int pageNo,int PageSize) {
		List<Object[]> list = null;
		List<maindashboardproject> executionLogs = new ArrayList<>();
		String delay="";
		
		
//		String overViewProjectHQL ="SELECT (SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN\n" + 
//				"( SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId IN\n" + 
//				"(SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isDeleted='N')) ) as totalcasesAct,\n" + 
//				"				                 p.projectId,\n" + 
//				"								 p.projectName, \n" + 
//				"								\n" + 
//				"         \n" + 
//				"                           (SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
//				"										SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
//				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
//				"										AND isactive='Y') AND mc.status='Fail') as Total_Fail,  \n" + 
//				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN( SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
//				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
//				"										AND isactive='Y') AND mc.status='Pass') as Total_Pass, \n" + 
//				"								\n" + 
//				"                                \n" + 
//				"                        count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,  \n" + 
//				"								count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',  \n" + 
//				"								count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',  \n" + 
//				"								count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started', \n" + 
//				"								\n" + 
//				"                                \n" + 
//				"                                (SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
//				"										SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
//				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
//				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_severity='Major' AND mc.status='Fail' \n" + 
//				"										 AND (mc.bug_status='Open' or	mc.bug_status='Reopen')) AS Major, \n" + 
//				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
//				"													SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
//				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
//				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_severity='Minor' AND mc.status='Fail' \n" + 
//				"										 AND (mc.bug_status='Open' or	mc.bug_status='Reopen')) AS Minor, \n" + 
//				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
//				"													SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
//				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
//				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_severity='Critical' AND mc.status='Fail' \n" + 
//				"										 AND (mc.bug_status='Open' or	mc.bug_status='Reopen')) AS Critical, \n" + 
//				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
//				"													SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
//				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
//				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_severity='Blocker' AND mc.status='Fail' \n" + 
//				"										 AND (mc.bug_status='Open' or	mc.bug_status='Reopen')) AS Blocker,		  \n" + 
//				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
//				"													SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
//				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
//				"										AND isactive='Y') AND mc.status='Fail' AND mc.ACTION='Completed' AND (mc.bug_severity='Minor'  \n" + 
//				"										OR mc.bug_severity='Major' OR mc.bug_severity='Critical' OR mc.bug_severity='Blocker')) AS totalbugs, \n" + 
//				"								       \n" + 
//				"                                       sum(mt.executionTime) as Total_secs, \n" + 
//				"								\n" + 
//				"                                \n" + 
//				"                        (SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
//				"													SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
//				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
//				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_status='Open') AS openbug, \n" + 
//				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
//				"													SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
//				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
//				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_status='Closed') AS closebug, \n" + 
//				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
//				"													SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
//				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
//				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_status='Reopen') AS reopenbug, \n" + 
//				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
//				"										SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
//				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
//				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_status='Resolved') AS ResolvedBug, \n" + 
//				"								\n" + 
//				"                                \n" + 
//				"                        count(IF(mt.testCaseAction = 'N/A', 1, NULL)) as 'NA' , \n" + 
//				"								count(IF(mt.testCaseAction = 'incomplete', 1, NULL)) as 'Total_action_incomplete',\n" + 
//				"                                \n" + 
//				"				             \n" + 
//				"                             (select count(p1.AccessType) from ProjectAccessTable p1 where p1.projectID=p.projectId ) as testers,  \n" + 
//				"				                \n" + 
//				"				             COUNT(DISTINCT mts.testSuitesId ) as totalsuite,\n" + 
//				"				             (SELECT COUNT(DISTINCT mgx.testGroupId) FROM Mannual_TestGroup_Table mgx WHERE testSuitesId in( \n" + 
//				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=p.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
//				"										)  AS totalGroup,\n" + 
//				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN( SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
//				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=p.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
//				"										)) as totaltestcases,\n" + 
//				"								\n" + 
//				"                                \n" + 
//				"                        COUNT(DISTINCT mre.runId) as totalActive,  \n" + 
//				"								(select count(*) from MannualReRunTable mr1 where mr1.isActive='Y' and mr1.status='Running' and mr1.projectId=p.projectID) as active,  \n" + 
//				"								(select count(*) from MannualReRunTable mr1 where mr1.isActive='N' and mr1.status='Running' and mr1.projectId=p.projectID) as inactive,  \n" + 
//				"								(select count(*) from MannualReRunTable mr1 where mr1.status='Completed' and mr1.projectId=p.projectID) as completed_run,      \n" + 
//				"								p.applicationName  \n" + 
//				"								\n" + 
//				"                                \n" + 
//				"                                FROM Project_MetaTable p \n" + 
//				"									left join MannualRun_Table mr  ON p.projectId =mr.projectID \n" + 
//				"									LEFT JOIN  MannualReRunTable mre ON mr.id=mre.runId \n" + 
//				"									LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mre.id\n" + 
//				"								   LEFT JOIN Mannual_TestSuites_Table mts ON mts.projectId=p.projectID AND mts.isDeleted='N'  \n" + 
//				"								where  p.isMannual='true' AND p.isActive='Y' \n" + 
//				"				                group by p.projectId   order by p.projectId DESC  LIMIT "+pageNo+", "+PageSize+" ";
		
	
		String overViewProjectHQL ="SELECT count(mr.id) as totalRunTestCases,p.projectId,\n" + 
				"								 p.projectName, \n" + 
				"								\n" + 
				"         \n" + 
				"                           (SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
				"										SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
				"										AND isactive='Y') AND mc.status='Fail') as Total_Fail,  \n" + 
				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN( SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
				"										AND isactive='Y') AND mc.status='Pass') as Total_Pass, \n" + 
				"								\n" + 
				"                                \n" + 
				"                        count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,  \n" + 
				"								count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',  \n" + 
				"								count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',  \n" + 
				"								count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started', \n" + 
				"								\n" + 
				"                                \n" + 
				"                                (SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
				"										SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_severity='Major' AND mc.status='Fail' \n" + 
				"										 AND (mc.bug_status='Open' or	mc.bug_status='Reopen')) AS Major, \n" + 
				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
				"													SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_severity='Minor' AND mc.status='Fail' \n" + 
				"										 AND (mc.bug_status='Open' or	mc.bug_status='Reopen')) AS Minor, \n" + 
				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
				"													SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_severity='Critical' AND mc.status='Fail' \n" + 
				"										 AND (mc.bug_status='Open' or	mc.bug_status='Reopen')) AS Critical, \n" + 
				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
				"													SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_severity='Blocker' AND mc.status='Fail' \n" + 
				"										 AND (mc.bug_status='Open' or	mc.bug_status='Reopen')) AS Blocker,		  \n" + 
				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
				"													SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
				"										AND isactive='Y') AND mc.status='Fail' AND mc.ACTION='Completed' AND (mc.bug_severity='Minor'  \n" + 
				"										OR mc.bug_severity='Major' OR mc.bug_severity='Critical' OR mc.bug_severity='Blocker')) AS totalbugs, \n" + 
				"								       \n" + 
				"                                       sum(mt.executionTime) as Total_secs, \n" + 
				"								\n" + 
				"                                \n" + 
				"                        (SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
				"													SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_status='Open') AS openbug, \n" + 
				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
				"													SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_status='Closed') AS closebug, \n" + 
				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
				"													SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_status='Reopen') AS reopenbug, \n" + 
				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN(  \n" + 
				"										SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=mr.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
				"										AND isactive='Y') AND mc.ACTION='Completed' AND mc.bug_status='Resolved') AS ResolvedBug, \n" + 
				"								\n" + 
				"                                \n" + 
				"                        count(IF(mt.testCaseAction = 'N/A', 1, NULL)) as 'NA' , \n" + 
				"								count(IF(mt.testCaseAction = 'incomplete', 1, NULL)) as 'Total_action_incomplete',\n" + 
				"                                \n" + 
				"				             \n" + 
				"                             (select count(p1.AccessType) from ProjectAccessTable p1 where p1.projectID=p.projectId ) as testers,  \n" + 
				"				                \n" + 
				"				             (select COUNT(DISTINCT mts.testSuitesId ) from Mannual_TestSuites_Table mts where mts.projectId=p.projectID AND mts.isDeleted='N') as totalsuite,\n" + 
				"				             (SELECT COUNT(DISTINCT mgx.testGroupId) FROM Mannual_TestGroup_Table mgx WHERE testSuitesId in( \n" + 
				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=p.projectId  AND mt.isDeleted='N')  \n" + 
				"										)  AS totalGroup,\n" + 
				"								(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId IN( SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in( \n" + 
				"										SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=p.projectId AND mt.isActive='Y' AND mt.isDeleted='N')  \n" + 
				"										)) as totaltestcases,\n" + 
				"								\n" + 
				"                                \n" + 
				"                        COUNT(DISTINCT mre.runId) as totalActive,  \n" + 
				"								(select count(*) from MannualReRunTable mr1 where mr1.isActive='Y' and mr1.status='Running' and mr1.projectId=p.projectID) as active,  \n" + 
				"								(select count(*) from MannualReRunTable mr1 where mr1.isActive='N' and mr1.status='Running' and mr1.projectId=p.projectID) as inactive,  \n" + 
				"								(select count(*) from MannualReRunTable mr1 where mr1.status='Completed' and mr1.projectId=p.projectID) as completed_run,      \n" + 
				"								p.applicationName  \n" + 
				"								\n" + 
				"                                \n" + 
				"                                FROM Project_MetaTable p \n" + 
				"									left join MannualRun_Table mr  ON p.projectId =mr.projectID \n" + 
				"									LEFT JOIN  MannualReRunTable mre ON mr.id=mre.runId \n" + 
				"									LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mre.id\n" + 
				"								    \n" + 
				"								where  p.isMannual='true' AND p.isActive='Y' \n" + 
				"				                group by p.projectId   order by p.projectId DESC  LIMIT "+pageNo+", "+PageSize+" ";
//		Session session=entityManager.unwrap(Session.class);
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
//			query.setInteger(1, pageNo);
//			query.setInteger(2, PageSize);
			list = query.list();
			for (Object[] objects : list) {
				maindashboardproject test = new maindashboardproject();
//				
			//pranik	
				
				test.setTotalRunTestcases(Integer.valueOf(objects[0].toString()));
				test.setProjectid(objects[1].toString());
//				int projectid = (int) objects[0];
				test.setProjectname(objects[2].toString());
//				System.out.print("applicationName:"+objects[2].toString());
		    	test.setTotalcurrentrunfail(Integer.valueOf(objects[3].toString()));
				test.setTotalcurrentrunpass(Integer.valueOf(objects[4].toString()));
				test.setTotalcompleted(Integer.valueOf(objects[5].toString()));
				test.setTotalonhold(Integer.valueOf(objects[6].toString()));
				test.setTotalunderreview(Integer.valueOf(objects[7].toString()));
				test.setTotalnotstarted(Integer.valueOf(objects[8].toString()));
				test.setTotalcurrentrunmajor(Integer.valueOf(objects[9].toString()));
				test.setTotalcurrentrunminor(Integer.valueOf(objects[10].toString()));
				test.setTotalcurrentruncritical(Integer.valueOf(objects[11].toString()));
				test.setTotalcurrentrunblocker(Integer.valueOf(objects[12].toString()));
				test.setTotalbugs(Integer.valueOf(objects[13].toString()));
				if(objects[14]!=null)
				{
					try
					{
			
					Double test2=(Double) objects[14];
					long testqq= (new Double(test2)).longValue();
					long sec = testqq % 60;
				    long minutes = testqq % 3600 / 60;
				    long hoursss = testqq % 28800 / 3600;
				    long daysss = testqq / 28800;
				    delay=daysss+"-days "+hoursss+"-hours "+minutes+"-Minute";
//				    test.setDayspend((int) daysss);
//				    test.setHourspend((int) hoursss);
//				    test.setMinutespend((int) minutes);
					
						
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
				else
				{
					delay=0+"-days "+0+"-hours "+0+"-Minute";
//					 test.setDayspend(0);
//					    test.setHourspend(0);
//					    test.setMinutespend(0);
				}
				
				test.setManhours(delay);
				test.setOpenBug(Integer.valueOf(objects[15].toString()));
				test.setCloseBug(Integer.valueOf(objects[16].toString()));
				test.setReOpenBug(Integer.valueOf(objects[17].toString()));
				test.setResolvedBug(Integer.valueOf(objects[18].toString()));
				test.setTotalNA(Integer.valueOf(objects[19].toString()));
				test.setTotalactionincomplete(Integer.valueOf(objects[20].toString()));
//			    int getAllocationtester=getTotalTester(projectid);
//			    test.setTotalallotedresource(getAllocationtester);
				test.setTotalallotedresource(Integer.valueOf(objects[21].toString()));

				test.setTotaltestsuites(Integer.valueOf(objects[22].toString()));
				test.setTotalgroup(Integer.valueOf(objects[23].toString()));
				test.setTotaltestcases(Integer.valueOf(objects[24].toString()));

				test.setTotalrun(Integer.valueOf(objects[25].toString()));
				test.setTotalActiverun(Integer.valueOf(objects[26].toString()));
				test.setTotalinActiverun(Integer.valueOf(objects[27].toString()));
				test.setTotalruncompleted(Integer.valueOf(objects[28].toString()));
				test.setApplicationName(objects[29].toString());
				
				//successrate
				float test1=Integer.valueOf(objects[4].toString());
				//float test2=test.getTotalcompleted();
		    	float test2=Integer.valueOf(objects[24].toString());
				if(test2!=0)
				test.setSuccessrate(df2.format((test1*100)/test2));

				executionLogs.add(test);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return executionLogs;
	}

	public int getTotalTester(Integer pid) {
		Object list = null;
		int i = 0;
		Session session=entityManager.unwrap(Session.class);
		Query qry = null;
		try {
			/*
			 * String
			 * sql="select count(IF(p.AccessType != 'Tester', 1, NULL)) from ProjectAccessTable p where p.projectID=:pId"
			 * ;
			 */

			String sql ="select count(p.AccessType) from ProjectAccessTable p \n" + 
					"				inner join UserDetailsEntity u on u.id=p.userDetailsId  where p.projectID=:pId"; /*
							 * "select count(IF(p.AccessType !='Readonly', 1, NULL)) from ProjectAccessTable p "
							 * +
							 * "inner join UserDetailsEntity u on u.id=p.userDetailsId  where p.projectID=:pId"
							 * ;
							 */
			qry = session.createNativeQuery(sql);
			qry.setParameter("pId", pid);
			Object arr[] = qry.list().toArray();

			i = Integer.valueOf(arr[0].toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}

	public List<maindashboardproject> projectholeviewaccess(int id) {
		List<Object[]> list = null;
		List<maindashboardproject> executionLogs = new ArrayList<>();

		/*
		 * String overViewProjectHQL =
		 * "SELECT p.projectID,p.projectName, COUNT(DISTINCT mts.testSuitesId) as totalsuite, \n"
		 * + "COUNT(DISTINCT mg.testGroupId)as totalGroup, \n" +
		 * "COUNT(DISTINCT mt.SRID)as totaltestcases,\n" +
		 * "COUNT(DISTINCT mre.runId) as totalActive,\n" +
		 * "(select count(*) from MannualReRunTable mr1 where mr1.isActive='Y' and mr1.status='Running' and mr1.projectId=p.projectID) as active,\n"
		 * +
		 * "(select count(*) from MannualReRunTable mr1 where mr1.isActive='N' and mr1.projectId=p.projectID) as inactive,\n"
		 * +
		 * "(select count(*) from MannualReRunTable mr1 where mr1.status='Completed' and mr1.projectId=p.projectID) as completed_run\n"
		 * +
		 * "From Project_MetaTable p inner join ProjectAccessTable pa on p.projectID =pa.projectID\n"
		 * + "LEFT JOIN MannualRun_Table mr ON mr.projectId =p.projectId\n" +
		 * "inner JOIN MannualReRunTable mre ON mr.id  =mre.runId\n" +
		 * "LEFT JOIN Mannual_TestSuites_Table mts ON p.projectID=mts.projectId \n" +
		 * "LEFT JOIN Mannual_TestGroup_Table mg ON mts.testSuitesId=mg.testSuitesId \n"
		 * +
		 * "LEFT JOIN Mannual_TestCase_Table mt ON mg.testGroupId =mt.testGroupId where pa.userDetailsId=:pId\n"
		 * + "GROUP BY p.projectID order by p.createdTime desc";
		 */
		// --------------rajat query------------------
		/*
		 * String overViewProjectHQL =
		 * "SELECT p.projectID,p.projectName, COUNT(DISTINCT mts.testSuitesId) as totalsuite, \n"
		 * + "COUNT(DISTINCT mg.testGroupId)as totalGroup, \n" +
		 * "COUNT(DISTINCT mt.SRID)as totaltestcases,\n" +
		 * "COUNT(DISTINCT mre.runId) as totalActive,\n" +
		 * "(select count(*) from MannualReRunTable mr1 where mr1.isActive='Y' and mr1.status='Running' and mr1.projectId=p.projectID) as active,\n"
		 * +
		 * "(select count(*) from MannualReRunTable mr1 where mr1.isActive='N' and mr1.projectId=p.projectID) as inactive,\n"
		 * +
		 * "(select count(*) from MannualReRunTable mr1 where mr1.status='Completed' and mr1.projectId=p.projectID) as completed_run\n"
		 * +
		 * "From Project_MetaTable p inner join ProjectAccessTable pa on p.projectID =pa.projectID\n"
		 * + "LEFT JOIN MannualRun_Table mr ON mr.projectId =p.projectId\n" +
		 * "inner JOIN MannualReRunTable mre ON mr.id  =mre.runId\n" +
		 * "LEFT JOIN Mannual_TestSuites_Table mts ON p.projectID=mts.projectId \n" +
		 * "LEFT JOIN Mannual_TestGroup_Table mg ON mts.testSuitesId=mg.testSuitesId \n"
		 * +
		 * "LEFT JOIN Mannual_TestCase_Table mt ON mg.testGroupId =mt.testGroupId where pa.userDetailsId=:pId\n"
		 * + "GROUP BY p.projectID order by p.createdTime desc";
		 */
		// ------after blockage--
		/*
		 * "SELECT p.projectID,p.projectName, COUNT(DISTINCT mts.testSuitesId) as totalsuite, \n"
		 * + "COUNT(DISTINCT mg.testGroupId)as totalGroup, \n" +
		 * "COUNT(DISTINCT mt.SRID)as totaltestcases,\n" +
		 * "COUNT(DISTINCT mre.runId) as totalActive,\n" +
		 * "(select count(*) from MannualReRunTable mr1 where mr1.isActive='Y' and mr1.status='Running' and mr1.projectId=p.projectID) as active,\n"
		 * +
		 * "(select count(*) from MannualReRunTable mr1 where mr1.isActive='N' and mr1.status='Running' and mr1.projectId=p.projectID) as inactive,\n"
		 * +
		 * "(select count(*) from MannualReRunTable mr1 where mr1.status='Completed' and mr1.projectId=p.projectID) as completed_run\n"
		 * +
		 * "From Project_MetaTable p inner join ProjectAccessTable pa on p.projectID =pa.projectID\n"
		 * + "LEFT JOIN MannualRun_Table mr ON mr.projectId =p.projectId\n" +
		 * "left JOIN MannualReRunTable mre ON mr.id  =mre.runId\n" +
		 * "LEFT JOIN Mannual_TestSuites_Table mts ON p.projectID=mts.projectId \n" +
		 * "LEFT JOIN Mannual_TestGroup_Table mg ON mts.testSuitesId=mg.testSuitesId \n"
		 * +
		 * "LEFT JOIN Mannual_TestCase_Table mt ON mg.testGroupId =mt.testGroupId where pa.userDetailsId=:pId AND p.isActive='Y'\n"
		 * + "GROUP BY p.projectID order by p.createdTime desc";
		 */
		String overViewProjectHQL = "SELECT p.projectID,p.projectName,p.applicationName ,COUNT(DISTINCT mts.testSuitesId ) as totalsuite, \n" + 
				"				COUNT(DISTINCT mg.testGroupId)as totalGroup,\n" + 
				"				COUNT(DISTINCT mt.SRID)as totaltestcases,\n" + 
				"				COUNT(DISTINCT mre.runId) as totalActive,\n" + 
				"				(select count(*) from MannualReRunTable mr1 where mr1.isActive='Y' and mr1.status='Running' \n" + 
				"					and mr1.projectId=p.projectID) as active,\n" + 
				"				(select count(*) from MannualReRunTable mr1 where mr1.isActive='N' and mr1.status='Running' \n" + 
				"					and mr1.projectId=p.projectID) as inactive,\n" + 
				"				(select count(*) from MannualReRunTable mr1 where mr1.status='Completed' and mr1.projectId=p.projectID) \n" + 
				"					as completed_run\n" + 
				"				From Project_MetaTable p inner join ProjectAccessTable pa on p.projectID =pa.projectID\n" + 
				"				LEFT JOIN MannualRun_Table mr ON mr.projectId =p.projectId\n" + 
				"				left JOIN MannualReRunTable mre ON mr.id  =mre.runId\n" + 
				"				LEFT JOIN Mannual_TestSuites_Table mts ON p.projectID=mts.projectId AND mts.isDeleted='N'\n" + 
				"				LEFT JOIN Mannual_TestGroup_Table mg ON mts.testSuitesId=mg.testSuitesId \n" + 
				"				LEFT JOIN Mannual_TestCase_Table mt ON mg.testGroupId =mt.testGroupId where p.isMannual='true' \n" + 
				"				 AND pa.userDetailsId=:pId AND p.isActive='Y'\n" + 
				"				GROUP BY p.projectID order by p.createdTime DESC ";
		Session session=entityManager.unwrap(Session.class);

		try {
//			Query query = session.createNativeQuery(overViewProjectHQL);
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("pId", id);
			list = query.list();
			for (Object[] objects : list) {
				maindashboardproject test = new maindashboardproject();
				test.setProjectname(objects[1].toString());
				test.setProjectid(objects[0].toString());
				test.setApplicationName(objects[2].toString());
				System.out.print("applicationName:"+objects[2].toString());
				test.setTotalgroup(Integer.valueOf(objects[4].toString()));
				test.setTotaltestsuites(Integer.valueOf(objects[3].toString()));
				test.setTotalrun(Integer.valueOf(objects[6].toString()));
				test.setTotaltestcases(Integer.valueOf(objects[5].toString()));
				test.setTotalActiverun(Integer.valueOf(objects[7].toString()));
				test.setTotalruncompleted(Integer.valueOf(objects[9].toString()));
				test.setTotalinActiverun(Integer.valueOf(objects[8].toString()));
				executionLogs.add(test);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return executionLogs;
	}

	public List<dashboardprojectdetailsAuto> projectviewdataAuto() {
		List<Object[]> list = null;
		String delay = "";
		List<dashboardprojectdetailsAuto> dashboardprojectdetailsAutos = new ArrayList<>();
		String overViewProjectHQL = " SELECT p.projectID,p.projectName,\n" + " (Select COUNT(l.status) FROM logs l\n"
				+ " WHERE l.project_name= p.projectName)AS 'log count',\n" + " (Select COUNT(l.status) FROM logs l\n"
				+ " WHERE l.project_name= p.projectName\n" + " AND l.status='PASS')AS 'PASS',\n"
				+ "(Select COUNT(l.status) FROM logs l\n" + " WHERE l.project_name= p.projectName\n"
				+ " AND l.status='FAIL')AS 'FAIL',\n" + "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName) AS 'total fresh run',\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='Success') AS 'completed run',\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='Fail') AS 'failed run',\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='Stopped') AS 'stopped run',\r\n"
				+ "(CASE WHEN a.NoOfDatasheet IS NULL THEN '0' ELSE a.NoOfDatasheet end) AS 'NoOfDatasheet', \r\n"
				+ "(CASE WHEN a.NoOftestSuites IS NULL THEN '0' ELSE a.NoOftestSuites end) AS 'NoOftestSuites',\r\n"
				+ "(CASE WHEN a.NoOfTestcases IS NULL THEN '0' ELSE a.NoOfTestcases end) AS 'NoOfTestcases',\r\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='RUNNING') AS 'running',"
				+ "(SELECT  COUNT(l.TestCaseID) from logs l LEFT JOIN Run_history r\n" + "ON l.RUN_ID = r.RUN_ID\n"
				+ "WHERE p.projectName = l.project_name) AS 'totalTestCase',\r\n" + "(SELECT \r\n"
				+ "SUM(( TIME_TO_SEC(cast(STR_TO_DATE(end_time,'%d-%m-%Y %H:%i:%s')as TIME))-TIME_TO_SEC(cast(STR_TO_DATE(start_time,'%d-%m-%Y %H:%i:%s')as TIME))))\r\n"
				+ "FROM Run_history r \r\n" + "WHERE r.project_name = p.projectName\r\n" + "AND r.start_time !=''\r\n"
				+ "AND r.end_time !='')AS 'avgTime'\r\n"
				+ " FROM Project_MetaTable p  LEFT join AutomationProjectMeta a ON p.projectID = a.Projectid \r\n"
				+ " WHERE p.isAutomation='TRUE'\r\n" + " GROUP BY p.projectName";

		System.out.println("query is " + overViewProjectHQL);
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);

			list = query.list();
			System.out.println("list is " + list.toString());
			for (Object[] objects : list) {
				dashboardprojectdetailsAuto test = new dashboardprojectdetailsAuto();
				test.setProjectid(objects[0].toString());
				test.setProjectname(objects[1].toString());
				int logcount = Integer.valueOf(objects[2].toString());
				int pass = Integer.valueOf(objects[3].toString());
				int fail = Integer.valueOf(objects[4].toString());
				double successrate = 0;
				if (pass != 0)
					successrate = (double) ((pass * 100.0f) / logcount);/* Math.round((pass/logcount)*100; */
				DecimalFormat df = new DecimalFormat("#.###");
		        df.setRoundingMode(RoundingMode.CEILING);

				int freshRun = Integer.valueOf(objects[5].toString());
				int completedRun = Integer.valueOf(objects[6].toString());
				double runrate = 0;
				if (completedRun != 0)
					runrate = (double) ((completedRun * 100.0f) / freshRun);/* Math.round((completedRun/freshRun)*100); */

				test.setTotalLogCount(logcount);
				test.setTotalPass(pass);
				test.setTotalFail(fail);
				test.setTotalSuccessRate(df.format(successrate));
				test.setTotalFreshRun(Integer.valueOf(objects[5].toString()));
				test.setTotalCompletedRun(Integer.valueOf(objects[6].toString()));
				test.setTotalFailedRun(Integer.valueOf(objects[7].toString()));
				test.setStoppedRun(Integer.valueOf(objects[8].toString()));
				test.setNoOfDatasheet(Integer.valueOf(objects[9].toString()));
				test.setNoOftestSuites(Integer.valueOf(objects[10].toString()));
				test.setNoOfTestcases(Integer.valueOf(objects[11].toString()));
				test.setRunning(Integer.valueOf(objects[12].toString()));
				test.setTotalTestCases(Integer.valueOf(objects[13].toString()));
				test.setProjTestSuitesList(getRunStatus(Integer.valueOf(objects[0].toString())));
				//
				System.out.println();
				System.out.println("objects[14].toString()..2222>>>>>>>>>>>." + objects[14].toString());
				if (objects[14] != null) {
					try {
						test.setAvgTime(Integer.valueOf(objects[14].toString()));

						Double test2 = Double.valueOf(objects[14].toString());
						long testqq = (new Double(test2)).longValue();
						long sec = testqq % 60;
						long minutes = testqq % 3600 / 60;
						long hoursss = testqq % 28800 / 3600;
						long daysss = testqq / 28800;
						delay = daysss + "-days " + hoursss + "-hours " + minutes + "-Minute";
						test.setDaySpent((int) daysss);
						test.setHourSpent((int) hoursss);
						test.setMinutespent((int) minutes);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					delay = 0 + "-days " + 0 + "-hours " + 0 + "-Minute";
					test.setDaySpent(0);
					test.setHourSpent(0);
					test.setMinutespent(0);
				}

				test.setRunRate(df.format(runrate));

				dashboardprojectdetailsAutos.add(test);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dashboardprojectdetailsAutos;
	}
/*
 * 
	public List<dashboardprojectdetailsAuto> projectviewdataAuto(String role, int userid) {
		List<Object[]> list = null;
		String delay = "";
		String joinQuery = "";
		String whrCond = "";
		if (!role.equals("Admin")) {
			joinQuery = " JOIN ProjectAccessTable pa ON p.projectID = pa.projectID  \r\n";
			whrCond = " AND pa.userDetailsId = " + userid + " ";
		} else {
			joinQuery = "";
			whrCond = "";
		}
		List<dashboardprojectdetailsAuto> dashboardprojectdetailsAutos = new ArrayList<>();
		String overViewProjectHQL = " SELECT p.projectID,p.projectName,\n" + " (Select COUNT(l.status) FROM logs l\n"
				+ " WHERE l.project_name= p.projectName)AS 'log count',\n" + " (Select COUNT(l.status) FROM logs l\n"
				+ " WHERE l.project_name= p.projectName\n" + " AND l.status='PASS')AS 'PASS',\n"
				+ "(Select COUNT(l.status) FROM logs l\n" + " WHERE l.project_name= p.projectName\n"
				+ " AND l.status='FAIL')AS 'FAIL',\n" + "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName) AS 'total fresh run',\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='Success') AS 'completed run',\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='Fail') AS 'failed run',\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='Stopped') AS 'stopped run',\r\n"
				+ "(CASE WHEN a.NoOfDatasheet IS NULL THEN '0' ELSE a.NoOfDatasheet end) AS 'NoOfDatasheet', \r\n"
				+ "(CASE WHEN a.NoOftestSuites IS NULL THEN '0' ELSE a.NoOftestSuites end) AS 'NoOftestSuites',\r\n"
				+ "(CASE WHEN a.NoOfTestcases IS NULL THEN '0' ELSE a.NoOfTestcases end) AS 'NoOfTestcases',\r\n"
				+ "(select COUNT(l.ScenarioID) FROM logs l WHERE l.project_name= p.projectName AND l.status='PASS') AS 'NoOfScenarioCompleted',\r\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='RUNNING') AS 'running',"
				+ "(SELECT  COUNT(l.TestCaseID) from logs l LEFT JOIN Run_history r\n" + "ON l.RUN_ID = r.RUN_ID\n"
				+ "WHERE p.projectName = l.project_name) AS 'totalTestCase',\r\n" + "(SELECT \r\n"
				+ "SUM(( TIME_TO_SEC(cast(STR_TO_DATE(end_time,'%d-%m-%Y %H:%i:%s')as TIME))-TIME_TO_SEC(cast(STR_TO_DATE(start_time,'%d-%m-%Y %H:%i:%s')as TIME))))\r\n"
				+ "FROM Run_history r \r\n" + "WHERE r.project_name = p.projectName\r\n" + "AND r.start_time !=''\r\n"
				+ "AND r.end_time !='')AS 'avgTime'\r\n"
				+ " FROM Project_MetaTable p  LEFT join AutomationProjectMeta a ON p.projectID = a.Projectid  \r\n"
				+ joinQuery + "WHERE p.isAutomation='TRUE' and p.isActive='Y' \r\n" + whrCond + " GROUP BY p.projectName order by p.createdTime desc";

		System.out.println("query is " + overViewProjectHQL);
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);

			list = query.list();
			System.out.println("list is " + list.toString());
			for (Object[] objects : list) {
				dashboardprojectdetailsAuto test = new dashboardprojectdetailsAuto();
				test.setProjectid(objects[0].toString());
				test.setProjectname(objects[1].toString());
				int logcount = Integer.valueOf(objects[2].toString());
				int pass = Integer.valueOf(objects[3].toString());
				int fail = Integer.valueOf(objects[4].toString());
				double successrate = 0;
				if (pass != 0)
					successrate = (double) ((pass * 100.0f) / logcount);
				DecimalFormat df = new DecimalFormat("#.###");
		        df.setRoundingMode(RoundingMode.CEILING);

		        

				int freshRun = Integer.valueOf(objects[5].toString());
				int completedRun = Integer.valueOf(objects[6].toString());
				double runrate = 0;
				if (completedRun != 0)
					runrate = (double) ((completedRun * 100.0f) / freshRun);

				test.setTotalLogCount(logcount);
				test.setTotalPass(pass);
				test.setTotalFail(fail);
				test.setTotalSuccessRate(df.format(successrate));
				test.setTotalFreshRun(Integer.valueOf(objects[5].toString()));
				test.setTotalCompletedRun(Integer.valueOf(objects[6].toString()));
				test.setTotalFailedRun(Integer.valueOf(objects[7].toString()));
				test.setStoppedRun(Integer.valueOf(objects[8].toString()));
				test.setNoOfDatasheet(Integer.valueOf(objects[9].toString()));
				test.setNoOftestSuites(Integer.valueOf(objects[10].toString()));
				test.setNoOfTestcases(Integer.valueOf(objects[11].toString()));
				test.setNoOfScenarioCompleted(Integer.valueOf(objects[12].toString()));
				test.setRunning(Integer.valueOf(objects[13].toString()));
				test.setTotalTestCases(Integer.valueOf(objects[14].toString()));
				test.setProjTestSuitesList(getRunStatus(Integer.valueOf(objects[0].toString())));
				//

				if (objects[14] != null) {
					try {
						test.setAvgTime(Integer.valueOf(objects[15].toString()));
						Double test2 = Double.valueOf(objects[15].toString());
						long testqq = (new Double(test2)).longValue();
						long sec = testqq % 60;
						long minutes = testqq % 3600 / 60;
						long hoursss = testqq % 28800 / 3600;
						long daysss = testqq / 28800;
						delay = daysss + "-days " + hoursss + "-hours " + minutes + "-Minute";
						test.setDaySpent((int) daysss);
						test.setHourSpent((int) hoursss);
						test.setMinutespent((int) minutes);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					delay = 0 + "-days " + 0 + "-hours " + 0 + "-Minute";
					test.setDaySpent(0);
					test.setHourSpent(0);
					test.setMinutespent(0);
				}

				test.setRunRate(df.format(runrate));

				dashboardprojectdetailsAutos.add(test);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dashboardprojectdetailsAutos;
	}
	*/

	

	public List<dashboardprojectdetailsAuto> projectviewdataAuto(String role, int userid) {
		List<Object[]> list = null;
		String delay = "";
		String joinQuery = "";
		String whrCond = "";
		if (!role.equals("Admin")) {
			joinQuery = " JOIN ProjectAccessTable pa ON p.projectID = pa.projectID  \r\n";
			whrCond = " AND pa.userDetailsId = " + userid + " ";
		} else {
			joinQuery = "";
			whrCond = "";
		}
		List<dashboardprojectdetailsAuto> dashboardprojectdetailsAutos = new ArrayList<>();
		String overViewProjectHQL = " SELECT p.projectID,p.projectName,p.applicationName ,\n" + " (Select COUNT(l.status) FROM logs l\n"
				+ " WHERE l.project_name= p.projectName)AS 'log count',\n" + " (Select COUNT(l.status) FROM logs l\n"
				+ " WHERE l.project_name= p.projectName\n" + " AND l.status='PASS')AS 'PASS',\n"
				+ "(Select COUNT(l.status) FROM logs l\n" + " WHERE l.project_name= p.projectName\n"
				+ " AND l.status='FAIL')AS 'FAIL',\n" + "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName) AS 'total fresh run',\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='Success') AS 'completed run',\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='Fail') AS 'failed run',\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='Stopped') AS 'stopped run',\r\n"
				+ "(CASE WHEN a.NoOfDatasheet IS NULL THEN '0' ELSE a.NoOfDatasheet end) AS 'NoOfDatasheet', \r\n"
				+ "(CASE WHEN a.NoOftestSuites IS NULL THEN '0' ELSE a.NoOftestSuites end) AS 'NoOftestSuites',\r\n"
				+ "(CASE WHEN a.NoOfTestcases IS NULL THEN '0' ELSE a.NoOfTestcases end) AS 'NoOfTestcases',\r\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='RUNNING') AS 'running',"
				+ "(SELECT  COUNT(l.TestCaseID) from logs l LEFT JOIN Run_history r\n" + "ON l.RUN_ID = r.RUN_ID\n"
				+ "WHERE p.projectName = l.project_name) AS 'totalTestCase',\r\n" + "(SELECT \r\n"
				+ "SUM(( TIME_TO_SEC(cast(STR_TO_DATE(end_time,'%d-%m-%Y %H:%i:%s')as TIME))-TIME_TO_SEC(cast(STR_TO_DATE(start_time,'%d-%m-%Y %H:%i:%s')as TIME))))\r\n"
				+ "FROM Run_history r \r\n" + "WHERE r.project_name = p.projectName\r\n" + "AND r.start_time !=''\r\n"
				+ "AND r.end_time !='')AS 'avgTime',"
				+ "(select COUNT(distinct l.ScenarioID) FROM logs l WHERE l.project_name= p.projectName) AS 'completedScenario'\r\n"
				+ " FROM Project_MetaTable p  LEFT join AutomationProjectMeta a ON p.projectID = a.Projectid  \r\n"
				+ joinQuery + "WHERE p.isAutomation='TRUE' and p.isActive='Y' \r\n" + whrCond + " GROUP BY p.projectName order by p.createdTime desc";

		System.out.println("query is " + overViewProjectHQL);
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);

			list = query.list();
			System.out.println("list is " + list.toString());
			for (Object[] objects : list) {
				dashboardprojectdetailsAuto test = new dashboardprojectdetailsAuto();
				test.setProjectid(objects[0].toString());
				test.setProjectname(objects[1].toString());
				test.setApplicationName(objects[2].toString());
				System.out.print("Application name auto:"+objects[2].toString());
				int logcount = Integer.valueOf(objects[3].toString());
				int pass = Integer.valueOf(objects[4].toString());
				int fail = Integer.valueOf(objects[5].toString());
				double successrate = 0;
				if (pass != 0)
					successrate = (double) ((pass * 100.0f) / logcount);/* Math.round((pass/logcount)*100; */
				DecimalFormat df = new DecimalFormat("#.###");
		        df.setRoundingMode(RoundingMode.CEILING);

		        

				int freshRun = Integer.valueOf(objects[5].toString());
				int completedRun = Integer.valueOf(objects[6].toString());
				double runrate = 0;
				if (completedRun != 0)
					runrate = (double) ((completedRun * 100.0f) / freshRun);/* Math.round((completedRun/freshRun)*100); */

				test.setTotalLogCount(logcount);
				test.setTotalPass(pass);
				test.setTotalFail(fail);
				test.setTotalSuccessRate(df.format(successrate));
				test.setTotalFreshRun(Integer.valueOf(objects[6].toString()));
				test.setTotalCompletedRun(Integer.valueOf(objects[7].toString()));
				test.setTotalFailedRun(Integer.valueOf(objects[8].toString()));
				test.setStoppedRun(Integer.valueOf(objects[9].toString()));
				test.setNoOfDatasheet(Integer.valueOf(objects[10].toString()));
				test.setNoOftestSuites(Integer.valueOf(objects[11].toString()));
				test.setNoOfTestcases(Integer.valueOf(objects[12].toString()));
				test.setRunning(Integer.valueOf(objects[13].toString()));
				test.setTotalTestCases(Integer.valueOf(objects[14].toString()));
				test.setProjTestSuitesList(getRunStatus(Integer.valueOf(objects[0].toString())));
				test.setToalCompletedScenario(Integer.valueOf(objects[16].toString()));
				//

				if (objects[15] != null) {
					try {
						test.setAvgTime(Integer.valueOf(objects[14].toString()));
						Double test2 = Double.valueOf(objects[14].toString());
						long testqq = (new Double(test2)).longValue();
						long sec = testqq % 60;
						long minutes = testqq % 3600 / 60;
						long hoursss = testqq % 28800 / 3600;
						long daysss = testqq / 28800;
						delay = daysss + "-days " + hoursss + "-hours " + minutes + "-Minute";
						test.setDaySpent((int) daysss);
						test.setHourSpent((int) hoursss);
						test.setMinutespent((int) minutes);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					delay = 0 + "-days " + 0 + "-hours " + 0 + "-Minute";
					test.setDaySpent(0);
					test.setHourSpent(0);
					test.setMinutespent(0);
				}

				test.setRunRate(df.format(runrate));

				dashboardprojectdetailsAutos.add(test);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dashboardprojectdetailsAutos;
	}

	
	public List<TestSuitesData> getRunStatus(int projectid) {
		System.out.println("in poa_dao");
		List<Object[]> list = null;
		String delay = "";
		List<TestSuitesData> projDetails = new ArrayList<>();
		String overViewProjectHQL = "SELECT p.projectID,p.projectName,l.ScenarioID,COUNT(l.ScenarioID)AS total,\r\n"
				+ "count(IF(l.status = 'Pass', 1, NULL) ) as  passed,\r\n"
				+ "COUNT(IF(l.`status`='Fail',1,NULL) ) AS failed,l.RUN_ID\r\n"
				+ "FROM Project_MetaTable p  LEFT JOIN  logs l\r\n" + "ON p.projectName =l.project_name  \r\n"
				+ "WHERE p.isAutomation='TRUE'\r\n" + "AND p.projectID = :projectid\r\n" + "GROUP BY l.RUN_ID ORDER BY l.RUN_ID desc LIMIT 3";
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectid", projectid);
			list = query.list();
			for (Object[] objects : list) {
				TestSuitesData test = new TestSuitesData();

				test.setProjectid(objects[0].toString());
				test.setProjectname(objects[1].toString());
				if (objects[2] == null) {
					System.out.println("scenario id is null");
				} else {
					test.setScenarioID(objects[2].toString());
				}
				test.setTotalTestSuites(Integer.valueOf(objects[3].toString()));
				test.setTotalPassed(Integer.valueOf(objects[4].toString()));
				test.setTotalFailed(Integer.valueOf(objects[5].toString()));
				if (objects[6] != null) {
					test.setRunId(Integer.valueOf(objects[6].toString()));
				}

				projDetails.add(test);
				System.out.println("data of automation overview  " + projDetails.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projDetails;
	}
	public List<dashboardprojectdetailsAuto> projectviewdataAuto(String role, int userid,int start) {
		List<Object[]> list = null;
		String delay = "";
		String joinQuery = "";
		String whrCond = "";
		if (!role.equals("Admin")) {
			joinQuery = " JOIN ProjectAccessTable pa ON p.projectID = pa.projectID  \r\n";
			whrCond = " AND pa.userDetailsId = " + userid + " ";
			
		} else {
			joinQuery = "";
			whrCond = "";
		}
		
		
		List<dashboardprojectdetailsAuto> dashboardprojectdetailsAutos = new ArrayList<>();
		String overViewProjectHQL = "SELECT p.projectID,p.projectName,p.applicationName ,\n" + " (Select COUNT(l.status) FROM logs l\n"
				+ " WHERE l.project_name= p.projectName)AS 'log count',\n" + " (Select COUNT(l.status) FROM logs l\n"
				+ " WHERE l.project_name= p.projectName\n" + " AND l.status='PASS')AS 'PASS',\n"
				+ "(Select COUNT(l.status) FROM logs l\n" + " WHERE l.project_name= p.projectName\n"
				+ " AND l.status='FAIL')AS 'FAIL',\n" + "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName) AS 'total fresh run',\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='Success') AS 'completed run',\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='Fail') AS 'failed run',\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='Stopped') AS 'stopped run',\r\n"
				+ "(CASE WHEN a.NoOfDatasheet IS NULL THEN '0' ELSE a.NoOfDatasheet end) AS 'NoOfDatasheet', \r\n"
				+ "(CASE WHEN a.NoOftestSuites IS NULL THEN '0' ELSE a.NoOftestSuites end) AS 'NoOftestSuites',\r\n"
				+ "(CASE WHEN a.NoOfTestcases IS NULL THEN '0' ELSE a.NoOfTestcases end) AS 'NoOfTestcases',\r\n"
				+ "(SELECT COUNT(r.status) FROM Run_history r\n"
				+ "WHERE r.project_name = p.projectName AND r.status='RUNNING') AS 'running',"
				+ "(SELECT  COUNT(l.TestCaseID) from logs l LEFT JOIN Run_history r\n" + "ON l.RUN_ID = r.RUN_ID\n"
				+ "WHERE p.projectName = l.project_name) AS 'totalTestCase',\r\n" + "(SELECT \r\n"
				+ "SUM(( TIME_TO_SEC(cast(STR_TO_DATE(end_time,'%d-%m-%Y %H:%i:%s')as TIME))-TIME_TO_SEC(cast(STR_TO_DATE(start_time,'%d-%m-%Y %H:%i:%s')as TIME))))\r\n"
				+ "FROM Run_history r \r\n" + "WHERE r.project_name = p.projectName\r\n" + "AND r.start_time !=''\r\n"
				+ "AND r.end_time !='')AS 'avgTime',"
				+ "(select COUNT(distinct l.ScenarioID) FROM logs l WHERE l.project_name= p.projectName) AS 'completedScenario'\r\n"
				+ " FROM Project_MetaTable p  LEFT join AutomationProjectMeta a ON p.projectID = a.Projectid  \r\n"
				+ joinQuery + "WHERE p.isAutomation='TRUE' and p.isActive='Y' \r\n" + whrCond + " GROUP BY p.projectName order by p.projectID desc LIMIT :start,:end";

		System.out.println("query is " + overViewProjectHQL);
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("start", start);
			query.setParameter("end", 20);
			//System.out.println("Rahul Modification "+sqlquery);
			list = query.list();
			System.out.println("list is " + list.toString());
			for (Object[] objects : list) {
				dashboardprojectdetailsAuto test = new dashboardprojectdetailsAuto();
				test.setProjectid(objects[0].toString());
				test.setProjectname(objects[1].toString());
				test.setApplicationName(objects[2].toString());
				System.out.print("Application name auto:"+objects[2].toString());
				int logcount = Integer.valueOf(objects[3].toString());
				int pass = Integer.valueOf(objects[4].toString());
				int fail = Integer.valueOf(objects[5].toString());
				double successrate = 0;
				if (pass != 0)
					successrate = (double) ((pass * 100.0f) / logcount);/* Math.round((pass/logcount)*100; */
				DecimalFormat df = new DecimalFormat("#.###");
		        df.setRoundingMode(RoundingMode.CEILING);

		        

				int freshRun = Integer.valueOf(objects[5].toString());
				int completedRun = Integer.valueOf(objects[6].toString());
				double runrate = 0;
				if (completedRun != 0)
					runrate = (double) ((completedRun * 100.0f) / freshRun);/* Math.round((completedRun/freshRun)*100); */

				test.setTotalLogCount(logcount);
				test.setTotalPass(pass);
				test.setTotalFail(fail);
				test.setTotalSuccessRate(df.format(successrate));
				test.setTotalFreshRun(Integer.valueOf(objects[6].toString()));
				test.setTotalCompletedRun(Integer.valueOf(objects[7].toString()));
				test.setTotalFailedRun(Integer.valueOf(objects[8].toString()));
				test.setStoppedRun(Integer.valueOf(objects[9].toString()));
				test.setNoOfDatasheet(Integer.valueOf(objects[10].toString()));
				test.setNoOftestSuites(Integer.valueOf(objects[11].toString()));
				test.setNoOfTestcases(Integer.valueOf(objects[12].toString()));
				test.setRunning(Integer.valueOf(objects[13].toString()));
				test.setTotalTestCases(Integer.valueOf(objects[14].toString()));
				test.setProjTestSuitesList(getRunStatus(Integer.valueOf(objects[0].toString())));
				test.setToalCompletedScenario(Integer.valueOf(objects[16].toString()));
				//

				if (objects[15] != null) {
					try {
						test.setAvgTime(Integer.valueOf(objects[14].toString()));
						Double test2 = Double.valueOf(objects[14].toString());
						long testqq = (new Double(test2)).longValue();
						long sec = testqq % 60;
						long minutes = testqq % 3600 / 60;
						long hoursss = testqq % 28800 / 3600;
						long daysss = testqq / 28800;
						delay = daysss + "-days " + hoursss + "-hours " + minutes + "-Minute";
						test.setDaySpent((int) daysss);
						test.setHourSpent((int) hoursss);
						test.setMinutespent((int) minutes);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					delay = 0 + "-days " + 0 + "-hours " + 0 + "-Minute";
					test.setDaySpent(0);
					test.setHourSpent(0);
					test.setMinutespent(0);
				}

				test.setRunRate(df.format(runrate));

				dashboardprojectdetailsAutos.add(test);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dashboardprojectdetailsAutos;
	}

}