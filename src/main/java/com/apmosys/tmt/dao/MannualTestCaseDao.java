package com.apmosys.tmt.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import javax.servlet.http.HttpSession;

import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.AutomationTestCaseExecutionLogs;
//import com.apmosys.tmt.models.AutomationTestCaseExecutionLogs;
import com.apmosys.tmt.models.MannualTestCaseExecutionLogs;
import com.apmosys.tmt.models.MannualTestCaseTable;
import com.apmosys.tmt.models.UserDetailsEntity;
import com.apmosys.tmt.repository.AutomationExecutionLogsRepo;
import com.apmosys.tmt.repository.MannualTestCaseRepository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.servlet.http.HttpSession;
@Repository
@Transactional
public class MannualTestCaseDao {
	@PersistenceContext
    private EntityManager entityManager;
	@Autowired
	private HttpSession session;
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	MannualTestCaseRepository mannualTestCaseRepository;
	
	@Autowired
	AutomationExecutionLogsRepo automationExecutionLogsRepo;
	
	
	
	public List<MannualTestCaseTable> getAllTestCasesByProjectID(Integer projectID) {
		List<Object[]> list = null;
		List<MannualTestCaseTable> testcases = new ArrayList<MannualTestCaseTable>();
		String overViewProjectHQL = "SELECT mt.SRID,mt.bug_ID,mt.bug_status from "
				+ "Mannual_TestCase_Table mt WHERE mt.testGroupId \r\n" + 
				" 	in(SELECT mg.testGroupId FROM Mannual_TestGroup_Table mg "
				+ "WHERE mg.testSuitesId \r\n" + 
				" 	IN(select m.testSuitesId FROM Mannual_TestSuites_Table m "
				+ "WHERE m.projectId=:projectID)) AND mt.bug_ID !='';";
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectID", projectID);
			list = query.list();
			for (Object[] objects : list) {
				try
				{
				MannualTestCaseTable details = new MannualTestCaseTable();
				details.setSrID((Integer) objects[0]);
				if (objects[1] != null)
					details.setBugID((String) objects[1]);
				details.setBugStatus((String) objects[2]); 
				testcases.add(details);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
			}
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
		} catch (Exception e) {
			e.printStackTrace();
	
		}
		return testcases;
	}
	
	public void updateTestCaseLatestStatus(MannualTestCaseExecutionLogs dbLogs) {
		
		try {
			UserDetailsEntity user = (UserDetailsEntity) session.getAttribute("user");
			MannualTestCaseTable entity=mannualTestCaseRepository.findById(dbLogs.getAbsoluteTCID()).orElse(null);
			entity.setStatus(dbLogs.getStatus());
			entity.setAction(dbLogs.getTestCaseAction());
			if((dbLogs.getBugId()==null) || (dbLogs.getBugId().equals(""))){
				entity.setBugID(null);
			}
			else {
				entity.setBugID(dbLogs.getBugId());
			}
			entity.setBugStatus(dbLogs.getBugStatus());
			entity.setBugSeverity(dbLogs.getBugSeverity());
			entity.setBugPriority(dbLogs.getBugPriority());
			entity.setUpdatedBy(user.getId());
			entity.setUpdatedDate(new Date());
			mannualTestCaseRepository.save(entity);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	//automation
public void updateTestCaseLatestStatus1(AutomationTestCaseExecutionLogs arLogs) {
		
		try {
			UserDetailsEntity user = (UserDetailsEntity) session.getAttribute("user");
			AutomationTestCaseExecutionLogs entity=automationExecutionLogsRepo.getBugDetails(arLogs.getBugId());
			entity.setStatus(arLogs.getStatus());
			entity.setTestCaseAction(arLogs.getTestCaseAction());
			if((arLogs.getBugId()==null) || (arLogs.getBugId().equals(""))){
				entity.setBugId(null);
			}
			else {
				entity.setBugId(arLogs.getBugId());
			}
			entity.setBugStatus(arLogs.getBugStatus());
			entity.setBugSeverity(arLogs.getBugSeverity());
			entity.setBugPriority(arLogs.getBugPriority());
//			entity.setUpdatedBy(user.getId());
//			entity.setUpdatedDate(new Date());
			automationExecutionLogsRepo.save(entity);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	public void deleteTestCase(Integer testcaseSID) {
		MannualTestCaseTable entity=mannualTestCaseRepository.findById(testcaseSID).orElse(null);		
		mannualTestCaseRepository.delete(entity);
	}
	
	public String deleteMulTestCase(Integer[] testcaseSID) 
	{
		Session session=entityManager.unwrap(Session.class);
		String mulDelQuery="DELETE FROM Mannual_TestCase_Table\r\n" + 
				"WHERE SrID IN(:testcaseSID)";
		System.out.println("  1    "+mulDelQuery);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(mulDelQuery);
			System.out.println("   2   "+query);
			query.setParameterList("testcaseSID", testcaseSID);
			System.out.println("   3   "+testcaseSID);
			int status=query.executeUpdate();
			if(status>=1) {
				return "deleted";
			}
			else 
			{
				return "not_deleted";
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "not_deleted";
		}
		
	}
	public void deleteTestCaseBygId(Integer groupId) {		
		mannualTestCaseRepository.deleteAllTestCasesByGId(groupId);
	}
	public MannualTestCaseTable addNewTestId(MannualTestCaseTable testcase) {
		MannualTestCaseTable s=null;
		Integer maxId=mannualTestCaseRepository.findMaxTestCasesId(testcase.getTestGroupId());
		Integer findtcid=mannualTestCaseRepository.findByTestIsByTestgrp(testcase.getTestID(),testcase.getTestGroupId());
		if(findtcid ==0)
		{
		if(maxId==null)
			maxId=0;
		Integer newCaseId=maxId+1;
		testcase.setSrNo(newCaseId.toString());	
		
		 s=mannualTestCaseRepository.save(testcase);
		}
		else {
			s.setMessageerror("Duplicate test case id"+testcase.getTestID());
		}
		return s;
	}
	
	public MannualTestCaseTable addNewTestIds(MannualTestCaseTable testcase) {
		MannualTestCaseTable s=new MannualTestCaseTable();
		List<MannualTestCaseTable> s1=null;
		try {
			Integer maxId=mannualTestCaseRepository.findMaxTestCasesId(testcase.getTestGroupId());
			Integer testcaseid=mannualTestCaseRepository.findByTestIsByTestgrp(testcase.getTestID(),testcase.getTestGroupId());
			if(testcaseid==1)
			{
				s.setMessageerror("Duplicate test case id");
				//String set=" "+s.getTestID();
				//s1.add(testcase.getTestID());
				//s.setSetvalue(s1);
			}
			else {
			if(maxId==null)
				maxId=0;
			Integer newCaseId=maxId+1;
			testcase.setSrNo(newCaseId.toString());	
			System.out.println("testcase ............>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+testcase.getSrID()+"tescsae test data.."+testcase.getTestData());
			s=mannualTestCaseRepository.save(testcase);
			s.setMessageerror("save data");
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return s;
	}
	
	public MannualTestCaseTable updateTestCase(MannualTestCaseTable testcase) {
		MannualTestCaseTable details=mannualTestCaseRepository.findById(testcase.getSrID()).orElse(null);
		details.setUpdatedDate(new Date());
		details.setUpdatedBy(testcase.getUpdatedBy());
		details.setScenarioID(testcase.getScenarioID());
		details.setBrowser(testcase.getBrowser());
		details.setScenarios(testcase.getScenarios());
		details.setTestCaseType(testcase.getTestCaseType());
		details.setSteps(testcase.getSteps());
		details.setTestID(testcase.getTestID());
		details.setTestData(testcase.getTestData());
		details.setTestDescription(testcase.getTestDescription());	
		details.setFunctionality(testcase.getFunctionality());
		details.setExpectedResult(testcase.getExpectedResult());
		details.setIsActive(testcase.getIsActive());
		MannualTestCaseTable details1=mannualTestCaseRepository.save(details);
		return details1;
	}
	public List<MannualTestCaseTable> getTestCases(Integer groupId) {
		List<MannualTestCaseTable> testCases= mannualTestCaseRepository.findTestCasesByTestGroupId(groupId);	
		return testCases;
		//return projectRepository.getAllProject();
	}
}
