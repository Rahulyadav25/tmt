package com.apmosys.tmt.dao;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

//import javax.transaction.Transactional;
//
//import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.apmosys.tmt.models.AutomationMetadata;
import com.apmosys.tmt.models.AutomationRunLogs;
import com.apmosys.tmt.models.AutomationRunTable;
import com.apmosys.tmt.models.ExecutionStepsBo;
import com.apmosys.tmt.repository.AutomationMetaRepository;
import com.apmosys.tmt.repository.AutomationRunRepository;
import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.QueueReference;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

@Repository
@Transactional
public class AutomationRunDao {
	@PersistenceContext
    private EntityManager entityManager;
	@Autowired
	private AutomationRunRepository automationRunRepository;
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	private AutomationMetaRepository automationmeta;

	public AutomationRunTable createRun(String pName) {
		AutomationRunTable runEntity = new AutomationRunTable();
		try {
			//SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			List<AutomationRunTable> findruningstatus=automationRunRepository.findByProject_name(pName);
			if(findruningstatus.isEmpty())
			{

			}
			else {
				for(AutomationRunTable running: findruningstatus)
				{
					if(running.getStatus().equals("RUNNING"))
					{

					}
					else {
						runEntity.setStatus("RUNNING");
						//runEntity.setStart_time(dateFormat.format(new Date()));
						runEntity.setProject_name(pName);
						runEntity = automationRunRepository.save(runEntity);
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return runEntity;
	}
	public Integer updateRunHistoryStatus(Integer runId, String status) {
		Integer count;
		Session session=entityManager.unwrap(Session.class);
		Transaction tx = session.beginTransaction();
		String projectmetaQuery = "Update AutomationRunTable set status=:status where runId=:runid";
		Query qry = session.createQuery(projectmetaQuery);
		qry.setParameter("status", status);
		qry.setParameter("runid", runId);
		count = qry.executeUpdate();
		tx.commit();
		return count;
	}
	public AutomationRunTable getCurrentRun(String project) {
		Session session=entityManager.unwrap(Session.class);
		List<Object[]> obj = null;
		AutomationRunTable runTable = null;
		try {
			String currentRunQuery = "SELECT runId,project_name,status,start_time,end_time,total_time from AutomationRunTable where runId=(select max(runId) from AutomationRunTable where project_name=:project)";
			Query qry = session.createQuery(currentRunQuery);
			qry.setParameter("project", project);
			obj = qry.list();
			if (obj.size() != 0) {
				Object[] rundetails = obj.get(0);
				runTable = new AutomationRunTable();
				runTable.setRunId((Integer) rundetails[0]);
				runTable.setProject_name((String) rundetails[1]);
				runTable.setStatus((String) rundetails[2]);
				runTable.setStart_time((String) rundetails[3]);
				runTable.setEnd_time((String) rundetails[4]);
				runTable.setTotal_time((String) rundetails[5]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return runTable;
	}

	public List<AutomationRunLogs> getCurrentRunLogs(String project) {
		List<AutomationRunLogs> list1 = new ArrayList<AutomationRunLogs>();
		AutomationRunLogs runLogs = null;
		Session session=entityManager.unwrap(Session.class);
		String projectmetaQuery = "SELECT l.id,l.runId,l.project_name,l.applicationName,l.scenarioID,"
				+ "l.testCaseID,l.responseTime,l.expectedResult,l.actualResult,l.status,l.timestamp,"
				+ "l.description,l.browserType, l.process, l.screenshotID, s.screenshotName, s.screenshot "
				+ "FROM AutomationRunLogs l, AutomationScreenshot s where "
				+ "l.runId=(select max(runId) from AutomationRunTable where project_name=:project) "
				+ "AND l.screenshotID=s.screenshotId order by l.id desc";
		Query qry = session.createQuery(projectmetaQuery);
		qry.setParameter("project", project);
		List<Object[]> list = qry.list();
		if (list.size() != 0) {
			for (Object[] logsDetails : list) {
				runLogs = new AutomationRunLogs();
				runLogs.setId((Integer) logsDetails[0]);
				runLogs.setRunId((Integer) logsDetails[1]);
				runLogs.setProject_name((String) logsDetails[2]);
				runLogs.setApplicationName((String) logsDetails[3]);
				runLogs.setScenarioID((String) logsDetails[4]);
				runLogs.setTestCaseID((String) logsDetails[5]);
				runLogs.setResponseTime((String) logsDetails[6]);
				runLogs.setExpectedResult((String) logsDetails[7]);
				runLogs.setActualResult((String) logsDetails[8]);
				runLogs.setStatus((String) logsDetails[9]);
				runLogs.setTimestamp((String) logsDetails[10]);
				runLogs.setDescription((String) logsDetails[11]);
				runLogs.setBrowserType((String) logsDetails[12]);
				runLogs.setProcess((String) logsDetails[13]);
				runLogs.setScreenshotID((String) logsDetails[14]);
				runLogs.setScreenshotName((String) logsDetails[15]);
				runLogs.setScreenshotImg((byte[]) logsDetails[16]);
				list1.add(runLogs);
			}
		}
		return list1;
	}

	public Integer stopJenkinsProject(String pName, String slaveMachine, Integer pid, JenkinsServer jenkins)
			throws IOException, InterruptedException {

		final String jobName = "STOP-job-" + UUID.randomUUID().toString();
		String cmd = "<hudson.tasks.Shell>" + " <command>cd\r"
				+ "ps aux | grep -i chrome | awk {'print $2'} | xargs kill -9\r" + "</command>"
				+ "</hudson.tasks.Shell>";

		String jobXml = "<?xml version='1.1' encoding='UTF-8'?>" + "<project>" + "<actions/>"
				+ "<description></description>" + "<displayName>" + jobName + "</displayName>"
				+ "<keepDependencies>false</keepDependencies>" + "<properties/>" + "<scm class='hudson.scm.NullSCM'/>"
				+ "<assignedNode>" + slaveMachine + "</assignedNode>" + "<canRoam>false</canRoam>"
				+ "<disabled>false</disabled>"
				+ "<blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>"
				+ "<blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>" + " <triggers/>"
				+ " <concurrentBuild>false</concurrentBuild>" + "<builders>" + cmd + "</builders>" + "<publishers/>"
				+ "<buildWrappers/>" + "</project>";
		try {
			jenkins.getJob(pName).details().getLastBuild().Stop(true);
			jenkins.createJob(jobName, jobXml, true);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("updating-------------");
			jenkins.updateJob(jobName, jobXml, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		QueueReference b = jenkins.getJobs().get(jobName).details().build(true);
		Thread.sleep(5000);
		// jenkins.deleteJob(jobName,true);
		return null;
	}

	public Integer updateStopStatus(Integer runId) {
		Integer count;
		Session session=entityManager.unwrap(Session.class);
		Transaction tx = session.beginTransaction();
		String projectmetaQuery = "Update AutomationRunTable set status=:status where runId=:runid";
		Query qry = session.createQuery(projectmetaQuery);
		qry.setParameter("status", "Stopped");
		qry.setParameter("runid", runId);
		count = qry.executeUpdate();
		tx.commit();
		return count;
	}

	public List<Object[]> getAutomationRunReport(String pName) {
		List<AutomationRunTable> runTable=new ArrayList<AutomationRunTable>();
		List<Object[]> list = new ArrayList<>();
		Session session=entityManager.unwrap(Session.class);
		try {
			String automationReport = "SELECT RUN_ID, applicationID, start_time, end_time, total_time, STATUS FROM Run_history WHERE project_name=:pName and (end_time!='' or end_time!=null) order by RUN_ID asc";
			Query qry = session.createNativeQuery(automationReport);
			qry.setParameter("pName", pName);
			list = qry.list();
			/*
			 * for (Object[] objects : list) { AutomationRunTable assiginRunBo = new
			 * AutomationRunTable(); if (objects[0] != null)
			 * assiginRunBo.setRunId(Integer.parseInt(objects[0].toString()));
			 * 
			 * assiginRunBo.setApplicationID((String) objects[1]); if (objects[2] != null)
			 * assiginRunBo.setStart_time((String) objects[2]);
			 * 
			 * 
			 * if (objects[3] != null) assiginRunBo.setEnd_time(objects[3].toString());
			 * if(objects[4] != null) assiginRunBo.setTotal_time(objects[4].toString());
			 * if(objects[5] != null) assiginRunBo.setStatus(objects[5].toString());
			 * runTable.add(assiginRunBo);
			 * 
			 * }
			 */
			//runTable.addAll(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;

	}
	public List<Object[]> getDefectReport(String pName) {
		List<AutomationRunTable> runTable=new ArrayList<AutomationRunTable>();
		List<Object[]> list = new ArrayList<>();
		Session session=entityManager.unwrap(Session.class);
		try {
		//	String automationReport = "SELECT RUN_ID, applicationID, start_time, end_time, total_time, STATUS FROM Run_history WHERE project_name=:pName and (end_time!='' or end_time!=null) order by RUN_ID asc";
		//	String automationReport = "	SELECT l.RUN_ID,rh.applicationID,rh.start_time, rh.end_time,  rh.`status`,l.bug_id FROM logs l,Run_history rh  WHERE l.RUN_ID=rh.RUN_ID AND rh.project_name=:pName and l.bug_id IS NOT NULL";
			String automationReport = "SELECT Distinct  l.RUN_ID,rh.applicationID,rh.start_time, rh.end_time,rh.total_time,  rh.`status` FROM logs l,Run_history rh  WHERE l.RUN_ID=rh.RUN_ID AND rh.project_name=:pName and l.bug_id IS NOT NULL";

//			int count=0;
//			try {
//			    ResultSet rs = DatabaseService.statementDataBase().executeQuery(query);
//			    while(rs.next())
//			        count=rs.getInt(1);
//			} catch (SQLException e) {
//			    e.printStackTrace();
//			} finally {
//			    //...
//			}
			Query qry = session.createNativeQuery(automationReport);
			qry.setParameter("pName", pName);
			list = qry.list();
			/*
			 * for (Object[] objects : list) { AutomationRunTable assiginRunBo = new
			 * AutomationRunTable(); if (objects[0] != null)
			 * assiginRunBo.setRunId(Integer.parseInt(objects[0].toString()));
			 * 
			 * assiginRunBo.setApplicationID((String) objects[1]); if (objects[2] != null)
			 * assiginRunBo.setStart_time((String) objects[2]);
			 * 
			 * 
			 * if (objects[3] != null) assiginRunBo.setEnd_time(objects[3].toString());
			 * if(objects[4] != null) assiginRunBo.setTotal_time(objects[4].toString());
			 * if(objects[5] != null) assiginRunBo.setStatus(objects[5].toString());
			 * runTable.add(assiginRunBo);
			 * 
			 * }
			 */
			//runTable.addAll(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;

	}
	public List<Object[]> getAllDefectReport(String runId,String projectName) {
		List<AutomationRunTable> runTable=new ArrayList<AutomationRunTable>();
		List<Object[]> list = new ArrayList<>();
		
		String Status="";
		
		Session session=entityManager.unwrap(Session.class);
		try {
		//	String automationReport = "SELECT RUN_ID, applicationID, start_time, end_time, total_time, STATUS FROM Run_history WHERE project_name=:pName and (end_time!='' or end_time!=null) order by RUN_ID asc";
		//	String automationReport = "	SELECT l.RUN_ID,rh.applicationID,rh.start_time, rh.end_time,  rh.`status`,l.bug_id FROM logs l,Run_history rh  WHERE l.RUN_ID=rh.RUN_ID AND rh.project_name=:pName and l.bug_id IS NOT NULL";
			String automationReport = "SELECT l.RUN_ID,l.ScenarioID,l.TestCaseID,l.Description,l.Response_Time,l.Expected_Result,l.Actual_Result,l.status,l.bug_id,l.BrowserType,l.TestcaseType,l.Step,l.scenario_description,l.id,p.isJiraEnable FROM autoBuglogs l,Project_MetaTable p WHERE l.RUN_ID=:runId and p.projectName=:projectName and l.bug_id IS NOT NULL";


			Query qry = session.createNativeQuery(automationReport);
			qry.setParameter("runId", runId);
			qry.setParameter("projectName", projectName);
			list = qry.list();
			
//			for(Object[] obj : list){
//				
//				 Status = String.valueOf(obj[7]); // houseId is at first place in your query
//				
//				
//			}
			
			System.out.print(list);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;

	}


	public List<ExecutionStepsBo> getPassoutinfomation(String runid) {
		List<ExecutionStepsBo> runTable=new ArrayList<ExecutionStepsBo>();
		List<Object[]> list = new ArrayList<>();
		Session session=entityManager.unwrap(Session.class);
		try {
			String automationReport =" SELECT * FROM `logs` l WHERE (l.RUN_ID=:runid AND l.TestCaseID !='')";
			Query qry = session.createNativeQuery(automationReport);
			qry.setParameter("runid", runid);
			runTable = qry.list();

			/* for (Object[] objects : list) {
			  AutomationRunTable assiginRunBo = new  AutomationRunTable(); 
			  if (objects[0] != null)
			  assiginRunBo.setPassousecount(Integer.parseInt(objects[0].toString()));
			  if (objects[1] != null)
				  assiginRunBo.setTotalcount(Integer.parseInt(objects[1].toString()));

			   runTable.add(assiginRunBo);

			  }*/

			//runTable.addAll(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return runTable;

	}


	@Transactional
	public int saveAutoSheetDetails(AutomationMetadata metadata) {
		int count1=0;
		if (metadata.getProjectid() != 0 && metadata.getProjectname() != null && metadata.getNoOftestSuites() != 0) {

			AutomationMetadata metadata1=automationmeta.findByProjectid(metadata.getProjectid());
			System.out.println("metatdata------------"+metadata1);
			if(metadata1!=null)
			{
				metadata1.setLocationofDatasheet(metadata.getLocationofDatasheet());
				metadata1.setNoOfDatasheet(metadata.getNoOfDatasheet());
				metadata1.setNoOfTestcases(metadata.getNoOfTestcases());
				metadata1.setNoOftestSuites(metadata.getNoOftestSuites());
				metadata1.setProjectid(metadata.getProjectid());
				metadata1.setProjectname(metadata.getProjectname());
				Session session=entityManager.unwrap(Session.class);
//				Transaction tx = session.beginTransaction();
				try {
//					session.update(metadata1);
					session.merge(metadata1);
//					tx.commit();
					count1=1;
				} catch (Exception e) {
//					tx.rollback();
					e.printStackTrace();
				}
			}
			else
			{
				Object count = 0;
				Session session=entityManager.unwrap(Session.class);
//				Transaction tx = session.beginTransaction();
				try {
//					count = session.save(metadata);
					count = session.merge(metadata);
//					tx.commit();
					count1=1;
				} catch (Exception e) {
//					tx.rollback();
					e.printStackTrace();
				}

			}

		}
		return count1;

	}

	/*public BugTrackorToolEntity getTooldetails(String id) {
		BugTrackorToolEntity list1 = new BugTrackorToolEntity();
		AutomationRunLogs runLogs = null;
		Session session=entityManager.unwrap(Session.class);
		String projectmetaQuery = "from BugTrackorToolEntity wher";
		Query qry = session.createNativeQuery(projectmetaQuery);
		qry.setParameter("id", id);
		List<Object[]> list = qry.list();

		return list1;
	}*/

	public List<AutomationRunLogs> getRunLogsByRunId(String runId) {
		List<AutomationRunLogs> list1 = new ArrayList<AutomationRunLogs>();
		AutomationRunLogs runLogs = null;
		Session session=entityManager.unwrap(Session.class);
		String projectmetaQuery =  "SELECT l.scenarioID,l.testCaseID,l.responseTime,l.expectedResult,l.actualResult,l.status,l.timestamp,"
				+ "l.description,l.browserType, l.process, l.screenshotID, l.ip_host_zone, l. "
				+ "FROM AutomationRunLogs l where "
				+ "l.runId= :runId ";
		String projectmetaQuery1 = "SELECT scenarioID, testCaseID, response_Time, expected_Result, actual_Result, status, timestamp, "
				+ "description, browserType, process, screenshotID, ip_host_zone, scenario_description,TestcaseType,Step "
				+ "FROM logs WHERE "
				+ "RUN_ID="+runId+";";
		Query qry = session.createQuery(projectmetaQuery1);
		//		qry.setParameter("runId", runId);
		List<Object[]> list = qry.list();
		if (list.size() != 0) {
			for (Object[] logsDetails : list) {
				runLogs = new AutomationRunLogs();
				runLogs.setScenarioID((String) logsDetails[0]);
				runLogs.setTestCaseID((String) logsDetails[1]);
				runLogs.setResponseTime((String) logsDetails[2]);
				runLogs.setExpectedResult((String) logsDetails[3]);
				runLogs.setActualResult((String) logsDetails[4]);
				runLogs.setStatus((String) logsDetails[5]);
				runLogs.setTimestamp((String) logsDetails[6]);
				runLogs.setDescription((String) logsDetails[7]);
				runLogs.setBrowserType((String) logsDetails[8]);
				runLogs.setProcess((String) logsDetails[9]);
				runLogs.setScreenshotID((String) logsDetails[10]);
				runLogs.setIp_host_zone((String) logsDetails[11]);
				runLogs.setScenario_description((String) logsDetails[12]);
				runLogs.setTestcaseType((String) logsDetails[13]);
				runLogs.setStep((String) logsDetails[14]);
				list1.add(runLogs);
			}
		}
		return list1;
	}

	public Map<String, Long> getScenarioTestCaseSteps(String runId) {

		Map<String, Long> res = new HashMap();

		Session session=entityManager.unwrap(Session.class);

		String query1 = " SELECT l.ScenarioID, (select count(*) from logs where RUN_ID=:runId and ScenarioID = l.ScenarioID ) as total "
				+ ", (select count(*) from logs where RUN_ID=:runId and ScenarioID = l.ScenarioID and status='FAIL' ) as failed FROM logs l where l.RUN_ID = :runId  group by l.ScenarioID ";

//		String query1 = " SELECT l.ScenarioID, (select count(*) from logs where RUN_ID=:runId ) as total "
//				+ ", (select count(*) from logs where RUN_ID=:runId and ScenarioID = l.ScenarioID and status='FAIL' ) as failed FROM logs l where l.RUN_ID = :runId  group by l.ScenarioID ";

		
		/*  SMAPLE OUTPUT :- 
		 *  scenario    totalTestcases     failedTestcases
		 *  ELSS_76	       13	            2
		 *  ELSS_SIP_78	   12	            1
		 */
		String query3 = "SELECT count(*) FROM executionsteps  WHERE RUN_ID=:runId ";//steps

		Query qry1 = session.createNativeQuery(query1);
		Query qry3 = session.createNativeQuery(query3);

		qry1.setParameter("runId", runId);
		qry3.setParameter("runId", runId);
        System.out.print("qry1****"+qry1);


		List<Object[]> list1 = qry1.list();
		List<Long> list3 = qry3.list();

		if (list1.size() != 0) {

			Long testCasesCount=0l;
			Long testcasesF=0l;
			Long scenarioF=0l;

			for(Object[] o : list1) {
				testCasesCount+=Long.valueOf( String.valueOf(o[1]));
				testcasesF+=Long.valueOf( String.valueOf(o[2]));
				if(!(String.valueOf( o[2]).equalsIgnoreCase("0"))) {
					scenarioF++;
				}
			}//for

			res.put("testCases", testCasesCount);
			res.put("scenarios", Long.valueOf(list1.size()));
			res.put("testcasesP", testCasesCount-testcasesF);
			res.put("scenarioF", scenarioF);

			
		}
		if (list3.size() != 0) 
			res.put("steps",Long.valueOf( String.valueOf(list3.get(0))));
		else
		res.put("steps", 0l);
		
		return res;
	}//

}//class