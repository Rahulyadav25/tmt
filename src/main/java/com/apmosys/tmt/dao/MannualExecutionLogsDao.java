package com.apmosys.tmt.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.BO.AssiginRunBo;
import com.apmosys.tmt.BO.DefectStatusBo;
import com.apmosys.tmt.BO.MannualRunDetailsData;
import com.apmosys.tmt.BO.OverallRunActionStatus;
import com.apmosys.tmt.BO.OverallRunStatus;
import com.apmosys.tmt.BO.ResourceRequestBo;
import com.apmosys.tmt.BO.ResourceResponseBo;
import com.apmosys.tmt.BO.resourceexecution;
import com.apmosys.tmt.models.AutomationTestCaseExecutionLogs;
//import com.apmosys.tmt.models.AutomationTestCaseExecutionLogs;
import com.apmosys.tmt.models.MannualTestCaseExecutionLogs;
import com.apmosys.tmt.models.Testrunwisedetails;
import com.apmosys.tmt.models.projectwisefailstatus;
import com.apmosys.tmt.models.runwisestatus;
import com.apmosys.tmt.repository.AutomationExecutionLogsRepo;
import com.apmosys.tmt.repository.MannualExecutionLogsRepo;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.servlet.http.HttpSession;

@Repository
@Transactional
public class MannualExecutionLogsDao {
	@PersistenceContext
    private EntityManager entityManager;
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	MannualExecutionLogsRepo mannualExecutionLogsRepo;
	@Autowired
	AutomationExecutionLogsRepo automationExecutionLogsRepo;
	
	@Autowired
	private HttpSession session;
	public List<MannualTestCaseExecutionLogs> getTCExecutionLogsByRerunID(Integer reRunId) {
		List<MannualTestCaseExecutionLogs> tc = null;
		// Integer absoluteTC=
		try {
			tc = mannualExecutionLogsRepo.findByReRunId(reRunId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tc;
	}

	public MannualTestCaseExecutionLogs getTCLatestStatusFromTimestamp(Integer absoluteTC) {
		MannualTestCaseExecutionLogs tc = null;
		// Integer absoluteTC=
		try {
			tc = mannualExecutionLogsRepo.getTCLatestStatusFromTimestamp(absoluteTC);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tc;
	}

	public List<MannualTestCaseExecutionLogs> getAllExecutedTestCasesByProjectID(Integer projectID) {
		List<Object[]> list = null;
		List<MannualTestCaseExecutionLogs> testcases = new ArrayList<MannualTestCaseExecutionLogs>();
		String overViewProjectHQL = "SELECT exe.id, exe.bugId, exe.bugStatus FROM\r\n"
				+ " MannualTestCaseExecutionLogs exe WHERE exe.projectId=:projectID AND exe.bugId !='';";
//		Session session = sessionFactory.getCurrentSession();
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectID", projectID);
			list = query.list();
			for (Object[] objects : list) {
				try {
					MannualTestCaseExecutionLogs details = new MannualTestCaseExecutionLogs();
					details.setId((Integer) objects[0]);
					if (objects[1] != null)
						details.setBugId((String) objects[1]);
					details.setBugStatus((String) objects[2]);
					testcases.add(details);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
		} catch (Exception e) {
			e.printStackTrace();

		}
		return testcases;
	}

	public void saveTestCaseForRun(List<MannualTestCaseExecutionLogs> logsList) {
		mannualExecutionLogsRepo.saveAll(logsList);

	}

	public void deleteActiveTestCasesForRun(Integer reRunId) {
		mannualExecutionLogsRepo.deleteActiveTestCasesForRun(reRunId);
	}

	public Map<String, MannualRunDetailsData> getTestGroupAndCasesByRun(Integer reRunId) {
		List<Object[]> list = null;
		List<MannualTestCaseExecutionLogs> executionLogs = new ArrayList<>();
		Map<String, MannualRunDetailsData> maps = new HashedMap();
		String overViewProjectHQL = " SELECT mg.groupName,mt.id,mt.projectId,mt.reRunId,mt.testSuitesId,mt.testGroupId,mt.SRID,mt.srNo,mt.module,mt.subModule\r\n"
				+ "	,mt.scenarioID,mt.scenarios,mt.functionality,mt.testID,mt.testCaseType,mt.testDescription,mt.dateOfExec,mt.browser,mt.testData,mt.steps,mt.expectedResult,mt.actualResult,mt.STATUS,mt.executionTime,mt.testerComment\r\n"
				+ "	,mt.testerId,mt.isActive,mt.issueIntegrationId,mt.bugSeverity,mt.bugPriority,mt.bugId,mt.testCaseAction\r\n"
				+ "	,mt.createdTimeStamp,mt.updatedTimeStamp,u.NAME AS assignto\r\n"
				+ "	,mt.absoluteSrID ,mt.priority,mt.complexity FROM MannualTestCaseExecutionLogs mt\r\n"
				+ "	LEFT JOIN Mannual_TestGroup_Table mg ON mg.testGroupId =mt.testGroupId \r\n"
				+ "	LEFT JOIN UserDetailsEntity u ON u.id=mt.testerId\r\n"
				+ "	WHERE mt.reRunId=:reRunId and mt.absoluteSrID IS NOT NULL order by mt.srID ASC";
		/*
		 * "SELECT mg.groupName,mt.id,mt.projectId,mt.reRunId,mt.testSuitesId,mt.testGroupId,mt.SRID,mt.srNo,mt.module,mt.subModule\r\n"
		 * +
		 * "	,mt.scenarioID,mt.scenarios,mt.functionality,mt.testID,mt.testCaseType,mt.testDescription,mt.dateOfExec,mt.browser,mt.testData,mt.steps,mt.expectedResult,mt.actualResult,mt.STATUS,mt.executionTime,mt.testerComment\r\n"
		 * +
		 * "	,mt.testerId,mt.isActive,mt.issueIntegrationId,mt.bugSeverity,mt.bugPriority,mt.bugId,mt.testCaseAction\r\n"
		 * + "	,mt.createdTimeStamp,mt.updatedTimeStamp,u.NAME AS assignto\r\n" +
		 * "	,mt.absoluteSrID FROM MannualTestCaseExecutionLogs mt\r\n" +
		 * "	LEFT JOIN Mannual_TestGroup_Table mg ON mg.testGroupId =mt.testGroupId \r\n"
		 * + "	LEFT JOIN UserDetailsEntity u ON u.id=mt.testerId\r\n" +
		 * "	WHERE mt.reRunId=:reRunId and mt.absoluteSrID IS NOT NULL " +
		 * "order by mt.srID ASC";
		 */
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("reRunId", reRunId);
			list = query.list();
			for (Object[] objects : list) {
				MannualTestCaseExecutionLogs logs = new MannualTestCaseExecutionLogs();
				logs.setGroupName((String) objects[0]);
				logs.setId((Integer) objects[1]);
				logs.setProjectId((Integer) objects[2]);
				logs.setReRunId((Integer) objects[3]);
				logs.setTestGroupId((Integer) objects[5]);
				logs.setTestCaseSrId((Integer) objects[6]);
				logs.setTestCaseSrNo((Integer) objects[7]);
				logs.setScenarioID((String) objects[10]);
				logs.setScenarios((String) objects[11]);
				logs.setFunctionality((String) objects[12]);
				logs.setTestCaseType((String) objects[14]);
				logs.setTestDescription((String) objects[15]);
				logs.setDateOfExec((Date) objects[16]);
				logs.setBrowser((String) objects[17]);
				logs.setSteps((String) objects[19]);
				logs.setTestData((String) objects[18]);
				logs.setExpectedResult((String) objects[20]);
				logs.setActualResult((String) objects[21]);
				logs.setStatus((String) objects[22]);
				if (objects[23] != null) {
					logs.setExecutionTime(Long.parseLong((String) objects[23]));
				}
				logs.setBugSeverity((String) objects[28]);
				logs.setBugPriority((String) objects[29]);
				logs.setTesterComment((String) objects[24]);
				logs.setTesterName((String) objects[34]);
				logs.setTestCaseAction((String) objects[31]);
				logs.setTesterId((String) objects[25]);
				logs.setTestID((String) objects[13]);
				logs.setAbsoluteTCID((Integer) objects[35]);
				logs.setPriority((String) objects[36]);
				logs.setComplexity((String) objects[37]);
				executionLogs.add(logs);
			}
			for (MannualTestCaseExecutionLogs details1 : executionLogs) {
				if (details1 != null) {
					MannualRunDetailsData mannualRunDetails = new MannualRunDetailsData();
					List<MannualTestCaseExecutionLogs> values = executionLogs.stream()
							.filter(x -> x.getGroupName().equals(details1.getGroupName())).collect(Collectors.toList());
					mannualRunDetails.setGroupId(details1.getTestGroupId().toString());
					if (mannualRunDetails.getGroupId() != null) {
						if (values != null) {
							mannualRunDetails.setTestCaseExecutionLogs(values);
							mannualRunDetails.setTestCaseCount(values.size());
						}
					}
					maps.put(details1.getGroupName(), mannualRunDetails);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return maps;
	}

	public List<MannualTestCaseExecutionLogs> getDetailsReportByRun(Integer reRunId) {
		List<Object[]> list = null;
		List<MannualTestCaseExecutionLogs> executionLogs = new ArrayList<>();
		String overViewProjectHQL = "SELECT mg.groupName,mt.id,mt.projectId,mt.reRunId,mt.testSuitesId,mt.testGroupId,mt.SRID,mt.srNo,mt.module,mt.subModule\r\n"
				+ "	,mt.scenarioID,mt.scenarios,mt.functionality,mt.testID,mt.testCaseType,mt.testDescription,mt.dateOfExec,mt.browser,mt.testData,mt.steps,mt.expectedResult,mt.actualResult,mt.STATUS,mt.executionTime,mt.testerComment\r\n"
				+ "	,mt.testerId,mt.isActive,mt.issueIntegrationId,mt.bugSeverity,mt.bugPriority,mt.bugId,mt.testCaseAction\r\n"
				+ "	,mt.createdTimeStamp,mt.updatedTimeStamp,u.NAME AS assignto,mt.bugStatus,mt.bugAge\r\n"
				+ "	,mt.priority,mt.complexity \r\n" + "	FROM MannualTestCaseExecutionLogs mt\r\n"
				+ "	LEFT JOIN Mannual_TestGroup_Table mg ON mg.testGroupId =mt.testGroupId \r\n"
				+ "	LEFT JOIN UserDetailsEntity u ON u.id=mt.testerId\r\n"
				+ "	WHERE mt.reRunId=:reRunId order by mt.srID ASC";

		/*
		 * "SELECT mg.groupName,mt.id,mt.projectId,mt.reRunId,mt.testSuitesId,mt.testGroupId,mt.SRID,mt.srNo,mt.module,mt.subModule\r\n"
		 * +
		 * "	,mt.scenarioID,mt.scenarios,mt.functionality,mt.testID,mt.testCaseType,mt.testDescription,mt.dateOfExec,mt.browser,mt.testData,mt.steps,mt.expectedResult,mt.actualResult,mt.STATUS,mt.executionTime,mt.testerComment\r\n"
		 * +
		 * "	,mt.testerId,mt.isActive,mt.issueIntegrationId,mt.bugSeverity,mt.bugPriority,mt.bugId,mt.testCaseAction\r\n"
		 * +
		 * "	,mt.createdTimeStamp,mt.updatedTimeStamp,u.NAME AS assignto,mt.bugStatus,mt.bugAge\r\n"
		 * + "	FROM MannualTestCaseExecutionLogs mt\r\n" +
		 * "	LEFT JOIN Mannual_TestGroup_Table mg ON mg.testGroupId =mt.testGroupId \r\n"
		 * + "	LEFT JOIN UserDetailsEntity u ON u.id=mt.testerId\r\n" +
		 * "	WHERE mt.reRunId=:reRunId order by mt.srID ASC";
		 */
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("reRunId", reRunId);
			list = query.list();
			for (Object[] objects : list) {
				MannualTestCaseExecutionLogs logs = new MannualTestCaseExecutionLogs();
				logs.setGroupName((String) objects[0]);
				logs.setId((Integer) objects[1]);
				logs.setProjectId((Integer) objects[2]);
				logs.setReRunId((Integer) objects[3]);
				logs.setTestGroupId((Integer) objects[5]);
				logs.setTestCaseSrId((Integer) objects[6]);
				logs.setTestCaseSrNo((Integer) objects[7]);
				logs.setScenarioID((String) objects[10]);
				logs.setScenarios((String) objects[11]);
				logs.setFunctionality((String) objects[12]);
				logs.setTestCaseType((String) objects[14]);
				logs.setTestDescription((String) objects[15]);
				logs.setDateOfExec((Date) objects[16]);
				logs.setBrowser((String) objects[17]);
				logs.setSteps((String) objects[19]);
				logs.setTestData((String) objects[18]);
				logs.setExpectedResult((String) objects[20]);
				logs.setActualResult((String) objects[21]);
				logs.setStatus((String) objects[22]);
				logs.setReportExcutionTime((String) objects[23] != null ? (String) objects[23] : null);
				logs.setTesterComment((String) objects[24]);
				logs.setTesterName((String) objects[34]);
				logs.setTestCaseAction((String) objects[31]);
				logs.setTesterId((String) objects[25]);
				if ((String) objects[27] != null) {
					logs.setIssueIntegrationId((String) objects[27]);
				}
				if ((String) objects[30] != null) {
					logs.setBugId((String) objects[30]);
				}
				if ((String) objects[29] != null) {
					logs.setBugPriority((String) objects[29]);
				}
				if ((String) objects[28] != null) {
					logs.setBugSeverity((String) objects[28]);
				}
				if ((String) objects[35] != null) {
					logs.setBugStatus((String) objects[35]);
				}
				if ((String) objects[30] != null) {
					logs.setBugType("Existing Bug");
				} else {
					logs.setBugType("New Bug");
				}
				if ((String) objects[36] != null) {
					logs.setBugAge((String) objects[36]);
				}

				logs.setPriority((String) objects[37]);
				logs.setComplexity((String) objects[38]);
				executionLogs.add(logs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return executionLogs;
	}

	// suchi changes--------
	public Map<String, List<resourceexecution>> getResourseEfforts1(ResourceRequestBo requestBo, String type) {
		List<Object[]> list = null;
		List<resourceexecution> effortsList = new ArrayList<>();
		Map<String, List<resourceexecution>> maps = new HashedMap<String, List<resourceexecution>>();
		String whereCond = "";
		String whereTCCond = "";
		String groupByCond = "";
		if ((requestBo.getFromDate() == null && requestBo.getToDate() == null)) {
			whereCond = whereCond + "AND DATE(m.dateOfExec) BETWEEN (NOW() - INTERVAL 7 DAY) AND NOW()";
			whereTCCond = whereTCCond + "AND DATE(mc.createdDate) BETWEEN (NOW() - INTERVAL 7 DAY) AND NOW()";
		}
		if (!(requestBo.getFromDate() == null && requestBo.getToDate() == null)) {
			whereCond = whereCond + "AND DATE(m.dateOfExec) BETWEEN '" + requestBo.getFromDate() + "' AND '"
					+ requestBo.getToDate() + "'  \r\n";
			whereTCCond = whereTCCond + "AND DATE(mc.createdDate) BETWEEN '" + requestBo.getFromDate() + "' AND '"
					+ requestBo.getToDate() + "'  \r\n";
		}

		String overviewhql = "SELECT u.NAME,mr.id,(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId \r\n"
				+ "IN(SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId \r\n"
				+ "in(SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=:project AND mt.isDeleted='N' \r\n"
				+ "	AND isactive='Y')) AND mc.prepared_by=m.testerId " + whereTCCond + ") AS preparation,\r\n"
				+ "COUNT(*) AS total, sum(IF(mr.ROUND=1, 1,0)) AS freshrun,(COUNT(*)-sum(IF(mr.ROUND=1, 1,0))) as Rerun\r\n"
				+ "FROM MannualTestCaseExecutionLogs m,UserDetailsEntity u,MannualReRunTable mr\r\n"
				+ "WHERE m.testerId=u.id AND m.reRunId=mr.id AND m.testCaseAction=:action AND \r\n"
				+ " m.testerId IN (:testerId) and mr.runid in (:reRunId)" + whereCond + " GROUP BY m.testerId";
		// 21/11/2020
		/*
		 * "SELECT u.NAME,mr.id,(select count(*) from MannualReRunTable mr ,MannualTestCaseExecutionLogs ml \r\n"
		 * +
		 * "where mr.createdBy=m.testerId and mr.projectId=:project and mr.id=ml.reRunId and mr.status=\"Running\") AS preparation,\r\n"
		 * +
		 * "COUNT(*) AS total, sum(IF(mr.ROUND=1, 1,0)) AS freshrun,(COUNT(*)-sum(IF(mr.ROUND=1, 1,0))) as Rerun\r\n"
		 * +
		 * "FROM MannualTestCaseExecutionLogs m,UserDetailsEntity u,MannualReRunTable mr\r\n"
		 * +
		 * "WHERE m.testerId=u.id AND m.reRunId=mr.id AND m.testCaseAction=:action AND \r\n"
		 * + " m.testerId IN (:testerId) and mr.runid in (:reRunId)"+whereCond+
		 * " GROUP BY m.testerId";
		 */

		/*
		 * "SELECT u.NAME,mr.id,(SELECT COUNT(*) FROM Mannual_TestCase_Table mt,\r\n" +
		 * "Mannual_TestGroup_Table mg,Mannual_TestSuites_Table ms\r\n" +
		 * "WHERE  ms.testSuitesId=mg.testSuitesId \r\n" +
		 * "AND mt.createdBy= m.testerId AND ms.projectId=:project) AS preparation,\r\n"
		 * +
		 * "COUNT(*) AS total, sum(IF(mr.ROUND=1, 1,0)) AS freshrun,(COUNT(*)-sum(IF(mr.ROUND=1, 1,0))) as Rerun\r\n"
		 * +
		 * "FROM MannualTestCaseExecutionLogs m,UserDetailsEntity u,MannualReRunTable mr\r\n"
		 * +
		 * "WHERE m.testerId=u.id AND m.reRunId=mr.id AND m.testCaseAction=:action AND \r\n"
		 * + " m.testerId IN (:testerId) and mr.runid in (:reRunId)"+whereCond+
		 * " GROUP BY m.testerId";
		 */

		/*
		 * "SELECT u.NAME,mr.id,(SELECT COUNT(*) FROM Mannual_TestCase_Table mt,\r\n" +
		 * "Mannual_TestGroup_Table mg,Mannual_TestSuites_Table ms\r\n" +
		 * "WHERE ms.testSuitesId=mg.testSuitesId \r\n" +
		 * "AND mt.createdBy= m.testerId AND ms.projectId=:project) AS preparation,\r\n"
		 * +
		 * "COUNT(*) AS total, sum(IF(mr.ROUND=1, 1,0)) AS freshrun,(COUNT(*)-sum(IF(mr.ROUND=1, 1,0))) as Rerun\r\n"
		 * +
		 * "FROM MannualTestCaseExecutionLogs m,UserDetailsEntity u,MannualReRunTable mr\r\n"
		 * +
		 * "WHERE m.testerId=u.id AND m.reRunId=mr.id AND m.testCaseAction=:action AND \r\n"
		 * + " m.testerId IN (:testerId) and mr.runid in (:reRunId)"+whereCond+
		 * " GROUP BY m.testerId";
		 */
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overviewhql);
			query.setParameter("action", "Completed");
			query.setParameter("project", requestBo.getProjectid());
			query.setParameterList("testerId", requestBo.getTesterList());
			query.setParameterList("reRunId", requestBo.getRunList());
			list = query.list();
			for (Object[] objects : list) {
				if (objects != null) {
					resourceexecution responseBo = new resourceexecution();
					responseBo.setResourcename(objects[0].toString());
					responseBo.setResourceid(objects[1].toString());
					responseBo.setPreparedexecution(objects[2].toString());
					responseBo.setFreshrunexecution(objects[4].toString());
					responseBo.setRerunexecution(objects[5].toString());
					responseBo.setTotalexecution(objects[3].toString());
					effortsList.add(responseBo);
				}
			}
			for (resourceexecution details1 : effortsList) {
				if (details1 != null) {
					List<resourceexecution> values = effortsList.stream()
							.filter(x -> x.getResourcename().equals(details1.getResourcename()))
							.collect(Collectors.toList());
					if (values != null) {
						maps.put(details1.getResourcename(), values);
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return maps;
	}

	public List<AssiginRunBo> getTodoRunList(String uId, String projectId) {
		List<Object[]> list = null;
		List<AssiginRunBo> assiginRun = new ArrayList<>();
		String assignRunQuery ="SELECT r.id,r.runName, COUNT(*),\r\n" + 
				"				(SELECT u.name FROM UserDetailsEntity u WHERE u.id=r.createdBy) as creadtedby,\r\n" + 
				"				r.targetEndDate ,\r\n" + 
				"				 count(IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Completed,\r\n" + 
				"				count(IF(l.testCaseAction = 'Not Started', 1, NULL)) as Total_Not_Started,\r\n" + 
				"                 count(IF(l.testCaseAction = 'On Hold', 1, NULL)) as Total_OnHold,\r\n" + 
				"                  count(IF(l.testCaseAction ='Under Review', 1, NULL)) as Total_underReview,\r\n" + 
				"                   count(IF(l.testCaseAction = 'N/A', 1, NULL)) as Total_NA,\r\n" + 
				"				 count(IF(l.status = 'Pass', 1, NULL) && IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Pass,\r\n" + 
				"				 count(IF(l.status = 'Fail', 1, NULL) && IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Fail,\r\n" + 
				"				 	count(IF(l.status = 'Blocked-Showstopper', 1, NULL)) as Total_Blocked_Showstopper,\r\n" + 
				"								count(IF(l.status = 'Blocked-Undelivered', 1, NULL) ) as Total_Blocked_Undelivered,\r\n" + 
				"								count(IF(l.status = 'NA', 1, NULL) ) as Totana,\r\n" + 
				"								count(IF(l.status = 'Blocked-Testdata', 1, NULL) ) as Total_Blocked_Test_data,\r\n" + 
				"								count(IF(l.complexity = 'Must to execute', 1, NULL) ) as Total_lE,\r\n" + 
				"								count(IF(l.complexity = 'Need to execute', 1, NULL) ) as Total_NTE,\r\n" + 
				"								count(IF(l.complexity = 'Nice to execute', 1, NULL) ) as Total_NITE,\r\n" + 
				"								count(IF(l.priority = 'High', 1, NULL) ) as Total_high,\r\n" + 
				"								count(IF(l.priority = 'Medium', 1, NULL) ) as Total_Medium,\r\n" + 
				"								count(IF(l.priority = 'Low', 1, NULL) ) as Total_low,\r\n" + 
				"								count(IF(l.status = 'Blocked-FSD not clear', 1, NULL) ) as Total_BlockedFSDnotclear,\r\n" + 
				"								count(IF(l.status = 'NE', 1, NULL) ) as Total_NE,\r\n" + 
				"								count(IF(l.status = 'Deferred', 1, NULL) ) as Total_Deferred,\r\n" + 
				"								count(IF(l.status = 'On Hold', 1, NULL) ) as OnHold,\r\n" + 
				"								count(IF(l.status = 'Duplicate', 1, NULL) ) as Total_Duplicate,\r\n" + 
				"								count(IF(l.status = 'Out of Scope', 1, NULL) ) as Total_OutofScope,\r\n" + 
				"								count(IF(l.status = 'Partially Pass', 1, NULL) ) as Total_PartiallyPass,\r\n" + 
				//				"                   count(IF(l.testCaseAction = 'Pass', 1, NULL)) as Total_Action_Pass\r\n" + 
				"                   count(IF(l.testCaseAction = 'Incomplete', 1, NULL)) as Total_Action_Incomplete\r\n" + 

				"				 FROM MannualTestCaseExecutionLogs l\r\n" + 
				"				 INNER JOIN MannualReRunTable r ON l.reRunId=r.id WHERE\r\n" + 
				"				 l.testerId=:uId and l.projectId=:projectId and r.status='Running'\r\n" + 
				"                 and r.isActive = 'Y'  GROUP BY r.id;\r\n" + 
				"";
		/*"SELECT r.id,r.runName, COUNT(*),\r\n" + 
				"				(SELECT u.name FROM UserDetailsEntity u WHERE u.id=r.createdBy) as creadtedby,\r\n" + 
				"				r.targetEndDate ,\r\n" + 
				"				 count(IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Completed,\r\n" + 
				"				count(IF(l.testCaseAction = 'Not Started', 1, NULL)) as Total_Not_Started,\r\n" + 
				"                 count(IF(l.testCaseAction = 'On Hold', 1, NULL)) as Total_OnHold,\r\n" + 
				"                  count(IF(l.testCaseAction ='Under Review', 1, NULL)) as Total_underReview,\r\n" + 
				"                   count(IF(l.testCaseAction = 'N/A', 1, NULL)) as Total_NA,\r\n" + 
				"				 count(IF(l.status = 'Pass', 1, NULL) && IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Pass,\r\n" + 
				"				 count(IF(l.status = 'Fail', 1, NULL) && IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Fail\r\n" + 
				"				 FROM MannualTestCaseExecutionLogs l\r\n" + 
				"				 INNER JOIN MannualReRunTable r ON l.reRunId=r.id WHERE\r\n" + 
				"				 l.testerId=:uId and l.projectId=:projectId and r.status='Running'\r\n" + 
				"                 and r.isActive = 'Y'  GROUP BY r.id;";
		 */	/*"SELECT r.id,r.runName, COUNT(*),\r\n" + 
				" (SELECT u.name FROM UserDetailsEntity u WHERE u.id=r.createdBy) as creadtedby,\r\n" + 
				" r.targetEndDate ,\r\n" + 
				" count(IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Completed,\r\n" + 
				" count(IF(l.testCaseAction = 'Not Started', 1, NULL)) as Total_Not_Started,\r\n" + 
				" count(IF(l.status = 'Pass', 1, NULL) && IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Pass,\r\n" + 
				" count(IF(l.status = 'Fail', 1, NULL) && IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Fail\r\n" + 
				" FROM MannualTestCaseExecutionLogs l\r\n" + 
				" INNER JOIN MannualReRunTable r ON l.reRunId=r.id WHERE\r\n" + 
				" l.testerId=:uId and l.projectId=:projectId and r.status='Running'\r\n" + 
				" and r.isActive = 'Y'  GROUP BY r.id;";*/
		/*				"SELECT r.id,r.runName, COUNT(*),(SELECT u.name FROM UserDetailsEntity u WHERE u.id=r.createdBy) as creadtedby,r.targetEndDate FROM MannualTestCaseExecutionLogs l INNER JOIN MannualReRunTable r ON l.reRunId=r.id WHERE l.testerId=:uId and l.projectId=:projectId and r.status='Running' and r.isActive = 'Y'  GROUP BY r.id";
		 */		Session session=entityManager.unwrap(Session.class);
		 try {
			 session.setCacheMode(CacheMode.IGNORE);
			 session.flush();
			 session.clear();
			 Query query = session.createNativeQuery(assignRunQuery);
			 query.setParameter("uId", uId);
			 query.setParameter("projectId", projectId);
			 list = query.list();
			 int i=1;
			 for (Object[] objects : list) {
				 /*
				  * AssiginRunBo assiginRunBo = new AssiginRunBo();
				  * System.out.println("0..."+(i+1)); if (objects[0] != null)
				  * assiginRunBo.setRunId(objects[0].toString());
				  * assiginRunBo.setRunName((String) objects[1]); if (objects[2] != null)
				  * assiginRunBo.setAssiginTestCaseCount(Integer.parseInt(objects[2].toString()))
				  * ; assiginRunBo.setCreatedBy((String) objects[3]); if (objects[4] != null)
				  * assiginRunBo.setTargetDate(objects[4].toString()); if(objects[5] != null)
				  * assiginRunBo.setTotalComplete(Integer.parseInt(objects[5].toString()));
				  * if(objects[6] != null)
				  * assiginRunBo.setTotalNotStated(Integer.parseInt(objects[6].toString()));
				  * if(objects[7] != null)
				  * assiginRunBo.setTotalOnHold(Integer.parseInt(objects[7].toString()));
				  * if(objects[8] != null)
				  * assiginRunBo.setTotalUnderReview(Integer.parseInt(objects[8].toString()));
				  * if(objects[9] != null)
				  * assiginRunBo.setTotalNA(Integer.parseInt(objects[9].toString()));
				  * if(objects[10] != null)
				  * assiginRunBo.setTotalPass(Integer.parseInt(objects[10].toString()));
				  * if(objects[11] != null)
				  * assiginRunBo.setTotalFail(Integer.parseInt(objects[11].toString()));
				  * assiginRun.add(assiginRunBo); if(objects[12] != null)
				  * assiginRunBo.setBlockshowtopper(Integer.parseInt(objects[12].toString()));
				  * assiginRun.add(assiginRunBo); if (objects[13] != null)
				  * assiginRunBo.setBlockedundelivered(Integer.parseInt(objects[13].toString()));
				  * assiginRun.add(assiginRunBo); if (objects[14] != null)
				  * assiginRunBo.setNa(Integer.parseInt(objects[14].toString()));
				  * assiginRun.add(assiginRunBo); if (objects[15] != null)
				  * assiginRunBo.setBlockedtestdata(Integer.parseInt(objects[15].toString()));
				  * assiginRun.add(assiginRunBo); if (objects[16] != null)
				  * assiginRunBo.setMustte(Integer.parseInt(objects[16].toString()));
				  * assiginRun.add(assiginRunBo); if (objects[17] != null)
				  * assiginRunBo.setNeedte(Integer.parseInt(objects[17].toString()));
				  * assiginRun.add(assiginRunBo); if (objects[18] != null)
				  * assiginRunBo.setNicete(Integer.parseInt(objects[18].toString()));
				  * assiginRun.add(assiginRunBo); if (objects[19] != null)
				  * assiginRunBo.setHigh(Integer.parseInt(objects[19].toString()));
				  * assiginRun.add(assiginRunBo); if (objects[20] != null)
				  * assiginRunBo.setMedium(Integer.parseInt(objects[20].toString()));
				  * assiginRun.add(assiginRunBo); if (objects[21] != null)
				  * assiginRunBo.setLow(Integer.parseInt(objects[21].toString()));
				  * assiginRun.add(assiginRunBo);
				  * 
				  */
				 AssiginRunBo assiginRunBo = new AssiginRunBo();
				 if (objects[0] != null)
					 assiginRunBo.setRunId(objects[0].toString());
				 assiginRunBo.setRunName((String) objects[1]);
				 if (objects[2] != null)
					 assiginRunBo.setAssiginTestCaseCount(Integer.parseInt(objects[2].toString()));
				 assiginRunBo.setCreatedBy((String) objects[3]);
				 if (objects[4] != null)
					 assiginRunBo.setTargetDate(objects[4].toString());
				 if(objects[5] != null)
					 assiginRunBo.setTotalComplete(Integer.parseInt(objects[5].toString()));
				 if(objects[6] != null)
					 assiginRunBo.setTotalNotStated(Integer.parseInt(objects[6].toString()));
				 if(objects[7] != null)
					 assiginRunBo.setTotalOnHold(Integer.parseInt(objects[7].toString()));
				 if(objects[8] != null)
					 assiginRunBo.setTotalUnderReview(Integer.parseInt(objects[8].toString()));
				 if(objects[9] != null)
					 assiginRunBo.setTotalNA(Integer.parseInt(objects[9].toString()));
				 if(objects[10] != null)
					 assiginRunBo.setTotalPass(Integer.parseInt(objects[10].toString()));
				 if(objects[11] != null)
					 assiginRunBo.setTotalFail(Integer.parseInt(objects[11].toString()));

				 if (objects[12] != null)
					 assiginRunBo.setBlockshowtopper(Integer.parseInt(objects[12].toString()));

				 if (objects[13] != null)
					 assiginRunBo.setBlockedundelivered(Integer.parseInt(objects[13].toString()));

				 if (objects[14] != null)
					 assiginRunBo.setNa(Integer.parseInt(objects[14].toString()));

				 if (objects[15] != null)
					 assiginRunBo.setBlockedtestdata(Integer.parseInt(objects[15].toString()));

				 if (objects[16] != null)
					 assiginRunBo.setMustte(Integer.parseInt(objects[16].toString()));

				 if (objects[17] != null)
					 assiginRunBo.setNeedte(Integer.parseInt(objects[17].toString()));

				 if (objects[18] != null)
					 assiginRunBo.setNicete(Integer.parseInt(objects[18].toString()));

				 if (objects[19] != null)
					 assiginRunBo.setHigh(Integer.parseInt(objects[19].toString()));

				 if (objects[20] != null)
					 assiginRunBo.setMedium(Integer.parseInt(objects[20].toString()));

				 if (objects[21] != null)
					 assiginRunBo.setLow(Integer.parseInt(objects[21].toString()));

				 if (objects[22] != null)
					 assiginRunBo.setBlockednotanclear(Integer.parseInt(objects[22].toString()));

				 if (objects[23] != null)
					 assiginRunBo.setNe(Integer.parseInt(objects[23].toString()));

				 if (objects[24] != null)
					 assiginRunBo.setDeferred(Integer.parseInt(objects[24].toString()));

				 if (objects[25] != null)
					 assiginRunBo.setOnhold(Integer.parseInt(objects[25].toString()));

				 if (objects[26] != null)
					 assiginRunBo.setDuplicate(Integer.parseInt(objects[26].toString()));

				 if (objects[27] != null)
					 assiginRunBo.setOutofScope(Integer.parseInt(objects[27].toString()));

				 if (objects[28] != null)
					 assiginRunBo.setPartiallyPass(Integer.parseInt(objects[28].toString()));

				 if (objects[29] != null)
					 //changes by 2-7-2021
					 //					assiginRunBo.setTotalactionpass(Integer.parseInt(objects[29].toString()));
					 assiginRunBo.setTotalactionincomplete(Integer.parseInt(objects[29].toString()));

				 assiginRun.add(assiginRunBo);

			 }

		 } catch (Exception e) {
			 e.printStackTrace();
		 }
		 /*
		  * finally { session.close(); }
		  */
		 System.out.println("assiginRun.."+assiginRun.toString());
		 return assiginRun;
	}

	/*public List<AssiginRunBo> getTodoRunList(String uId, String projectId) {
	 *
	 * List<Object[]> list = null; List<AssiginRunBo> assiginRun = new
	 * ArrayList<>(); String assignRunQuery ="SELECT r.id,r.runName, COUNT(*),\r\n"
	 * +
	 * "				(SELECT u.name FROM UserDetailsEntity u WHERE u.id=r.createdBy) as creadtedby,\r\n"
	 * + "				r.targetEndDate ,\r\n" +
	 * "				 count(IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Completed,\r\n"
	 * +
	 * "				count(IF(l.testCaseAction = 'Not Started', 1, NULL)) as Total_Not_Started,\r\n"
	 * +
	 * "                 count(IF(l.testCaseAction = 'On Hold', 1, NULL)) as Total_OnHold,\r\n"
	 * +
	 * "                  count(IF(l.testCaseAction ='Under Review', 1, NULL)) as Total_underReview,\r\n"
	 * +
	 * "                   count(IF(l.testCaseAction = 'N/A', 1, NULL)) as Total_NA,\r\n"
	 * +
	 * "				 count(IF(l.status = 'Pass', 1, NULL) && IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Pass,\r\n"
	 * +
	 * "				 count(IF(l.status = 'Fail', 1, NULL) && IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Fail,\r\n"
	 * +
	 * "				 	count(IF(l.status = 'Blocked-Showstopper', 1, NULL)) as Total_Blocked_Showstopper,\r\n"
	 * +
	 * "								count(IF(l.status = 'Blocked-Undelivered', 1, NULL) ) as Total_Blocked_Undelivered,\r\n"
	 * +
	 * "								count(IF(l.status = 'NA', 1, NULL) ) as Totana,\r\n"
	 * +
	 * "								count(IF(l.status = 'Blocked-Testdata', 1, NULL) ) as Total_Blocked_Test_data,\r\n"
	 * +
	 * "								count(IF(l.priority = 'Must to execute', 1, NULL) ) as Total_MTE,\r\n"
	 * +
	 * "								count(IF(l.priority = 'Need to execute', 1, NULL) ) as Total_NTE,\r\n"
	 * +
	 * "								count(IF(l.priority = 'Nice to execute', 1, NULL) ) as Total_NITE,\r\n"
	 * +
	 * "								count(IF(l.complexity = 'High', 1, NULL) ) as Total_high,\r\n"
	 * +
	 * "								count(IF(l.complexity = 'Medium', 1, NULL) ) as Total_Medium,\r\n"
	 * +
	 * "								count(IF(l.complexity = 'Low', 1, NULL) ) as Total_low\r\n"
	 * + "				 FROM MannualTestCaseExecutionLogs l\r\n" +
	 * "				 INNER JOIN MannualReRunTable r ON l.reRunId=r.id WHERE\r\n"
	 * +
	 * "				 l.testerId=:uId and l.projectId=:projectId and r.status='Running'\r\n"
	 * + "                 and r.isActive = 'Y'  GROUP BY r.id;";
	 * "SELECT r.id,r.runName, COUNT(*),\r\n" +
	 * "				(SELECT u.name FROM UserDetailsEntity u WHERE u.id=r.createdBy) as creadtedby,\r\n"
	 * + "				r.targetEndDate ,\r\n" +
	 * "				 count(IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Completed,\r\n"
	 * +
	 * "				count(IF(l.testCaseAction = 'Not Started', 1, NULL)) as Total_Not_Started,\r\n"
	 * +
	 * "                 count(IF(l.testCaseAction = 'On Hold', 1, NULL)) as Total_OnHold,\r\n"
	 * +
	 * "                  count(IF(l.testCaseAction ='Under Review', 1, NULL)) as Total_underReview,\r\n"
	 * +
	 * "                   count(IF(l.testCaseAction = 'N/A', 1, NULL)) as Total_NA,\r\n"
	 * +
	 * "				 count(IF(l.status = 'Pass', 1, NULL) && IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Pass,\r\n"
	 * +
	 * "				 count(IF(l.status = 'Fail', 1, NULL) && IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Fail\r\n"
	 * + "				 FROM MannualTestCaseExecutionLogs l\r\n" +
	 * "				 INNER JOIN MannualReRunTable r ON l.reRunId=r.id WHERE\r\n"
	 * +
	 * "				 l.testerId=:uId and l.projectId=:projectId and r.status='Running'\r\n"
	 * + "                 and r.isActive = 'Y'  GROUP BY r.id;";
	 * 
	 * "SELECT r.id,r.runName, COUNT(*),\r\n" +
	 * " (SELECT u.name FROM UserDetailsEntity u WHERE u.id=r.createdBy) as creadtedby,\r\n"
	 * + " r.targetEndDate ,\r\n" +
	 * " count(IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Completed,\r\n"
	 * +
	 * " count(IF(l.testCaseAction = 'Not Started', 1, NULL)) as Total_Not_Started,\r\n"
	 * +
	 * " count(IF(l.status = 'Pass', 1, NULL) && IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Pass,\r\n"
	 * +
	 * " count(IF(l.status = 'Fail', 1, NULL) && IF(l.testCaseAction = 'Completed', 1, NULL)) as Total_Fail\r\n"
	 * + " FROM MannualTestCaseExecutionLogs l\r\n" +
	 * " INNER JOIN MannualReRunTable r ON l.reRunId=r.id WHERE\r\n" +
	 * " l.testerId=:uId and l.projectId=:projectId and r.status='Running'\r\n" +
	 * " and r.isActive = 'Y'  GROUP BY r.id;";
	 * "SELECT r.id,r.runName, COUNT(*),(SELECT u.name FROM UserDetailsEntity u WHERE u.id=r.createdBy) as creadtedby,r.targetEndDate FROM MannualTestCaseExecutionLogs l INNER JOIN MannualReRunTable r ON l.reRunId=r.id WHERE l.testerId=:uId and l.projectId=:projectId and r.status='Running' and r.isActive = 'Y'  GROUP BY r.id"
	 * ; Session session = sessionFactory.getCurrentSession(); try {
	 * session.setCacheMode(CacheMode.IGNORE); session.flush(); session.clear();
	 * Query query = session.createNativeQuery(assignRunQuery);
	 * query.setParameter("uId", uId); query.setParameter("projectId", projectId);
	 * list = query.list(); for (Object[] objects : list) { AssiginRunBo
	 * assiginRunBo = new AssiginRunBo(); if (objects[0] != null)
	 * assiginRunBo.setRunId(objects[0].toString());
	 * assiginRunBo.setRunName((String) objects[1]); if (objects[2] != null)
	 * assiginRunBo.setAssiginTestCaseCount(Integer.parseInt(objects[2].toString()))
	 * ; assiginRunBo.setCreatedBy((String) objects[3]); if (objects[4] != null)
	 * assiginRunBo.setTargetDate(objects[4].toString()); if(objects[5] != null)
	 * assiginRunBo.setTotalComplete(Integer.parseInt(objects[5].toString()));
	 * if(objects[6] != null)
	 * assiginRunBo.setTotalNotStated(Integer.parseInt(objects[6].toString()));
	 * if(objects[7] != null)
	 * assiginRunBo.setTotalOnHold(Integer.parseInt(objects[7].toString()));
	 * if(objects[8] != null)
	 * assiginRunBo.setTotalUnderReview(Integer.parseInt(objects[8].toString()));
	 * if(objects[9] != null)
	 * assiginRunBo.setTotalNA(Integer.parseInt(objects[9].toString()));
	 * if(objects[10] != null)
	 * assiginRunBo.setTotalPass(Integer.parseInt(objects[10].toString()));
	 * if(objects[11] != null)
	 * assiginRunBo.setTotalFail(Integer.parseInt(objects[11].toString()));
	 * assiginRun.add(assiginRunBo); if(objects[12] != null)
	 * assiginRunBo.setBlockshowtopper(Integer.parseInt(objects[12].toString()));
	 * assiginRun.add(assiginRunBo); if(objects[13] != null)
	 * assiginRunBo.setBlockedundelivered(Integer.parseInt(objects[13].toString()));
	 * assiginRun.add(assiginRunBo); if(objects[14] != null)
	 * assiginRunBo.setNa(Integer.parseInt(objects[14].toString()));
	 * assiginRun.add(assiginRunBo); if(objects[15] != null)
	 * assiginRunBo.setBlockedtestdata(Integer.parseInt(objects[15].toString()));
	 * assiginRun.add(assiginRunBo); if(objects[16] != null)
	 * assiginRunBo.setMustte(Integer.parseInt(objects[16].toString()));
	 * assiginRun.add(assiginRunBo); if(objects[17] != null)
	 * assiginRunBo.setNeedte(Integer.parseInt(objects[17].toString()));
	 * assiginRun.add(assiginRunBo); if(objects[18] != null)
	 * assiginRunBo.setNicete(Integer.parseInt(objects[18].toString()));
	 * assiginRun.add(assiginRunBo); if(objects[19] != null)
	 * assiginRunBo.setHigh(Integer.parseInt(objects[19].toString()));
	 * assiginRun.add(assiginRunBo); if(objects[20] != null)
	 * assiginRunBo.setMedium(Integer.parseInt(objects[20].toString()));
	 * assiginRun.add(assiginRunBo); if(objects[21] != null)
	 * assiginRunBo.setLow(Integer.parseInt(objects[21].toString()));
	 * assiginRun.add(assiginRunBo); }
	 * 
	 * } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * finally { session.close(); }
	 * 
	 * return assiginRun;
	 *}
	 */

	public Map<String, MannualRunDetailsData> getTodoTestCaseList(String uId, Integer runId) {
		List<Object[]> list = null;
		List<MannualTestCaseExecutionLogs> todoTestCaseList = new ArrayList<>();
		Map<String, MannualRunDetailsData> maps = new HashedMap<String, MannualRunDetailsData>();
		String overViewProjectHQL = "SELECT mg.groupName,mt.id,mt.projectId,mt.reRunId,mt.testSuitesId,mt.testGroupId,mt.SRID,mt.srNo,mt.module,mt.subModule\r\n"
				+ " ,mt.scenarioID,mt.scenarios,mt.functionality,mt.testID,mt.testCaseType,mt.testDescription,mt.dateOfExec,mt.browser,mt.testData,mt.steps,mt.expectedResult,mt.actualResult,mt.STATUS,mt.executionTime,mt.testerComment\r\n"
				+ " ,mt.testerId,mt.isActive,mt.issueIntegrationId,mt.bugSeverity,mt.bugPriority,mt.bugId,mt.testCaseAction\r\n"
				+ " ,mt.createdTimeStamp,mt.updatedTimeStamp,u.NAME AS assignto\r\n"
				+ " ,p.isJiraEnable,mt.priority,mt.complexity  FROM MannualTestCaseExecutionLogs mt\r\n"
				+ " LEFT JOIN Mannual_TestGroup_Table mg ON mg.testGroupId =mt.testGroupId \r\n"
				+ " LEFT JOIN UserDetailsEntity u ON u.id=mt.testerId\r\n"
				+ " LEFT JOIN Project_MetaTable p ON p.projectID=mt.projectId "
				+ " WHERE mt.reRunId=:runId and u.id=:uId order by mt.srID ASC";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("runId", runId);
			query.setParameter("uId", uId);
			list = query.list();
			for (Object[] objects : list) {
				MannualTestCaseExecutionLogs logs = new MannualTestCaseExecutionLogs();
				logs.setGroupName((String) objects[0]);
				logs.setId((Integer) objects[1]);
				logs.setProjectId((Integer) objects[2]);
				logs.setReRunId((Integer) objects[3]);
				logs.setTestGroupId((Integer) objects[5]);
				logs.setTestCaseSrId((Integer) objects[6]);
				logs.setTestCaseSrNo((Integer) objects[7]);
				logs.setScenarioID((String) objects[10]);
				logs.setScenarios((String) objects[11]);
				logs.setFunctionality((String) objects[12]);
				logs.setTestID((String) objects[13]);
				logs.setTestCaseType((String) objects[14]);
				logs.setTestDescription((String) objects[15]);
				logs.setDateOfExec((Date) objects[16]);
				if ((String) objects[17] == null) {
					logs.setBrowser("");
				} else {
					logs.setBrowser((String) objects[17]);
				}
				logs.setSteps((String) objects[19]);
				logs.setTestData((String) objects[18]);
				logs.setExpectedResult((String) objects[20]);
				logs.setActualResult((String) objects[21]);
				if ((String) objects[22] == null) {
					logs.setStatus("");
				} else {
					logs.setStatus((String) objects[22]);
				}
				logs.setTesterComment((String) objects[24]);
				logs.setTesterName((String) objects[34]);
				logs.setTestCaseAction((String) objects[31]);
				logs.setPriority((String) objects[36]);
				logs.setComplexity((String) objects[37]);
				if (objects[23] != null)
					logs.setExecutionTime(Long.valueOf(objects[23].toString()));
				if ((String) objects[28] == null) {
					logs.setBugSeverity("");
				} else {
					logs.setBugSeverity((String) objects[28]);
				}
				if ((String) objects[29] == null) {
					logs.setBugPriority("");
				} else {
					logs.setBugPriority((String) objects[29]);
				}

				logs.setBugId((String) objects[30]);
				logs.setBugType("");
				if (objects[22].equals("Fail")) {
					if ((String) objects[30] != null) {
						logs.setBugType("1");
					} else {
						logs.setBugType("0");
					}
				}
				if (objects[22].equals("Pass")) {
					if ((String) objects[30] != null) {
						logs.setBugType("1");
					}

				}
				if ((objects[35].equals(null)) || (objects[35].equals(""))) {
					logs.setBugToolId(null);
				} else {
					logs.setBugToolId(Integer.valueOf(objects[35].toString()));
				}

				todoTestCaseList.add(logs);
			}
			for (MannualTestCaseExecutionLogs details1 : todoTestCaseList) {
				if (details1 != null) {
					MannualRunDetailsData mannualRunDetails = new MannualRunDetailsData();
					List<MannualTestCaseExecutionLogs> values = todoTestCaseList.stream()
							.filter(x -> x.getGroupName().equals(details1.getGroupName())).collect(Collectors.toList());
					mannualRunDetails.setGroupId(details1.getTestGroupId().toString());
					if (mannualRunDetails.getGroupId() != null) {
						if (values != null) {
							mannualRunDetails.setTestCaseExecutionLogs(values);
							mannualRunDetails.setTestCaseCount(values.size());
						}
					}
					maps.put(details1.getGroupName(), mannualRunDetails);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return maps;
	}

	public MannualTestCaseExecutionLogs saveTestExceution(MannualTestCaseExecutionLogs exLogs) {

		MannualTestCaseExecutionLogs details1 = null;
		if (exLogs.getReRunId() != null) {
			MannualTestCaseExecutionLogs details = mannualExecutionLogsRepo
					.findById(Integer.parseInt(exLogs.getId().toString())).orElse(null);
			/*
			 * String lastStatus=details.getStatus(); String
			 * lastBugStatus=details.getBugStatus();
			 */
			details.setBrowser(exLogs.getBrowser());
			details.setScenarios(exLogs.getScenarios());
			details.setTestCaseType(exLogs.getTestCaseType());
			details.setSteps(exLogs.getSteps());
			details.setTestData(exLogs.getTestData());
			details.setTestDescription(exLogs.getTestDescription());
			details.setFunctionality(exLogs.getFunctionality());
			details.setExpectedResult(exLogs.getExpectedResult());
			details.setActualResult(exLogs.getActualResult());
			details.setTesterComment(exLogs.getTesterComment());
			details.setStatus(exLogs.getStatus());
			details.setBugStatus(exLogs.getBugStatus());
			details.setBugSeverity(exLogs.getBugSeverity());
			details.setBugPriority(exLogs.getBugPriority());
			details.setBugId(exLogs.getBugId());
			details.setBugSeverity(exLogs.getBugSeverity());
			details.setBugPriority(exLogs.getBugPriority());
			details.setBugId(exLogs.getBugId());
			details.setComplexity(exLogs.getComplexity());
			details.setPriority(exLogs.getPriority());

			if (details.getStatus() != null) {
				if (details.getStatus().equalsIgnoreCase("Fail")) {
					if (StringUtils.isEmpty(exLogs.getBugStatus())
							|| ("Open").equalsIgnoreCase(exLogs.getBugStatus())) {
						details.setBugStatus("Open");
					}
					if (StringUtils.isNotBlank(exLogs.getBugSummary())) {
						details.setBugSummary(exLogs.getBugSummary());
					}

				}
				//pranik
				if (details.getStatus().equalsIgnoreCase("Pass")) {
					if (StringUtils.isEmpty(exLogs.getBugStatus())
							|| ("close").equalsIgnoreCase(exLogs.getBugStatus())) {
						details.setBugStatus("close");
					}
					if (StringUtils.isNotBlank(exLogs.getBugSummary())) {
						details.setBugSummary(exLogs.getBugSummary());
					}

				}
				
				
			}

			details.setTestCaseAction(exLogs.getTestCaseAction());
			/*
			 * if (exLogs.getStatus() != null) { if (exLogs.getStatus().equals("Fail")) {
			 * details.setBugSeverity(exLogs.getBugSeverity());
			 * details.setBugPriority(exLogs.getBugPriority());
			 * details.setBugId(exLogs.getBugId()); } else { details.setBugSeverity(null);
			 * details.setBugPriority(null); details.setBugId(null); } } else {
			 * details.setBugSeverity(null); details.setBugPriority(null);
			 * details.setBugId(null);
			 * 
			 * }
			 */
			details.setExecutionTime(exLogs.getExecutionTime());
			details.setDateOfExec(new Date());
			details.setUpdatedTimeStamp(new Date());
			details1 = mannualExecutionLogsRepo.save(details);
		}
		return details1;
	}
	
	
	
	
	//added later
	public AutomationTestCaseExecutionLogs saveTestExceution1(AutomationTestCaseExecutionLogs arLogs) {

		AutomationTestCaseExecutionLogs details1 = null;
		if (arLogs.getTestID()!= null) {
			AutomationTestCaseExecutionLogs details = automationExecutionLogsRepo
					.getBugDetails(arLogs.getBugId());
		
			/*
			 * String lastStatus=details.getStatus(); String
			 * lastBugStatus=details.getBugStatus();
			 */
			details.setBrowser(arLogs.getBrowser());
			details.setScenarios(arLogs.getScenarios());
			details.setTestCaseType(arLogs.getTestCaseType());
			details.setSteps(arLogs.getSteps());
			details.setTestData(arLogs.getTestData());
			details.setTestDescription(arLogs.getTestDescription());
			details.setFunctionality(arLogs.getFunctionality());
			details.setExpectedResult(arLogs.getExpectedResult());
			details.setActualResult(arLogs.getActualResult());
			details.setTesterComment(arLogs.getTesterComment());
			details.setStatus(arLogs.getStatus());
			details.setBugStatus(arLogs.getBugStatus());
			details.setBugSeverity(arLogs.getBugSeverity());
			details.setBugPriority(arLogs.getBugPriority());
			details.setBugId(arLogs.getBugId());
			details.setComplexity(arLogs.getComplexity());
			details.setApiBugSummary(arLogs.getApiBugSummary());
			details.setBugType(arLogs.getBugType());
			details.setBugToolId(arLogs.getBugToolId());
			details.setPriority(arLogs.getPriority());
			details.setProjectId(arLogs.getProjectId());
			details.setProjectName(arLogs.getProject_name());
			details.setToolName(arLogs.getToolName());


//			if (details.getStatus() != null) {
//				if (details.getStatus().equalsIgnoreCase("Fail")) {
//					if (StringUtils.isEmpty(arLogs.getBugStatus())
//							|| ("Open").equalsIgnoreCase(arLogs.getBugStatus()) 
//							|| ("To Do").equalsIgnoreCase(arLogs.getBugStatus())) {
//					//	details.setBugStatus("Open");
//						details.setBugStatus(arLogs.getBugStatus());
//						
//					}
//					if (StringUtils.isNotBlank(arLogs.getApiBugSummary())) {
//						details.setApiBugSummary(arLogs.getApiBugSummary());
//					}
//
//				}
//				//pranik
//				if (details.getStatus().equalsIgnoreCase("Pass")) {
//					if (StringUtils.isEmpty(arLogs.getBugStatus())
//							|| ("close").equalsIgnoreCase(arLogs.getBugStatus())
//							|| ("Done").equalsIgnoreCase(arLogs.getBugStatus())) {
//					//	details.setBugStatus("close");
//					details.setBugStatus(arLogs.getBugStatus());
//					}
//					if (StringUtils.isNotBlank(arLogs.getApiBugSummary())) {
//						details.setApiBugSummary(arLogs.getApiBugSummary());
//					}
//
//				}
			if (details.getStatus() != null) {
				if (details.getStatus().equalsIgnoreCase("Fail")) {
					if (StringUtils.isEmpty(arLogs.getBugStatus())
							|| ("Open").equalsIgnoreCase(arLogs.getBugStatus())) {
						details.setBugStatus("Open");
					}
					if (StringUtils.isNotBlank(arLogs.getApiBugSummary())) {
						details.setApiBugSummary(arLogs.getApiBugSummary());
					}

				}
				//pranik
				if (details.getStatus().equalsIgnoreCase("Pass")) {
					if (StringUtils.isEmpty(arLogs.getBugStatus())
							|| ("close").equalsIgnoreCase(arLogs.getBugStatus())) {
						details.setBugStatus("close");
					}
					if (StringUtils.isNotBlank(arLogs.getApiBugSummary())) {
						details.setApiBugSummary(arLogs.getApiBugSummary());
					}

				}
				
			}

			details.setTestCaseAction(arLogs.getTestCaseAction());
			/*
			 * if (exLogs.getStatus() != null) { if (exLogs.getStatus().equals("Fail")) {
			 * details.setBugSeverity(exLogs.getBugSeverity());
			 * details.setBugPriority(exLogs.getBugPriority());
			 * details.setBugId(exLogs.getBugId()); } else { details.setBugSeverity(null);
			 * details.setBugPriority(null); details.setBugId(null); } } else {
			 * details.setBugSeverity(null); details.setBugPriority(null);
			 * details.setBugId(null);
			 * 
			 * }
			 */
			details.setExecutionTime(arLogs.getExecutionTime());
			details.setDateOfExec(new Date());
		//	details.setUpdatedTimeStamp(new Date());
			details1 = automationExecutionLogsRepo.save(details);
		}
		return details1;
	}
	
	

	public MannualTestCaseExecutionLogs saveBugId(MannualTestCaseExecutionLogs logs) {
		MannualTestCaseExecutionLogs caseExecutionLogs = mannualExecutionLogsRepo.findById(logs.getId()).orElse(null);
		caseExecutionLogs.setBugId(logs.getBugId());
		return mannualExecutionLogsRepo.save(caseExecutionLogs);
	}
	public AutomationTestCaseExecutionLogs saveBugId1(AutomationTestCaseExecutionLogs arlogs) {
		AutomationTestCaseExecutionLogs caseExecutionLogs = automationExecutionLogsRepo.getBugDetails(arlogs.getBugId());
		caseExecutionLogs.setBugId(arlogs.getBugId());
		return automationExecutionLogsRepo.save(caseExecutionLogs);
	}
	public MannualTestCaseExecutionLogs changeAssiginTo(String newAssiginId, String logsId) {
		MannualTestCaseExecutionLogs caseExecutionLogs = mannualExecutionLogsRepo.findById(Integer.parseInt(logsId)).orElse(null);
		caseExecutionLogs.setTesterId(newAssiginId);
		return mannualExecutionLogsRepo.save(caseExecutionLogs);
	}

	public Integer changeMulAssiginTo(Integer logsid, String uid) {
		Integer i = mannualExecutionLogsRepo.updateAssiginingTo(uid, logsid);
		return i;
	}

	public OverallRunStatus getOverallRunStatus(Integer runId) {
		List<Object[]> runList = null;
		OverallRunStatus runStatus = new OverallRunStatus();
		String runDetailsHQL = "SELECT IF(mr.status IS NULL,'Untested',mr.status) as name , COUNT(*) as count FROM MannualTestCaseExecutionLogs mr WHERE mr.reRunId=:reRunId and mr.testCaseAction='Completed' GROUP BY mr.status";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(runDetailsHQL);
			query.setParameter("reRunId", runId);
			runList = query.list();
			if (runList != null) {
				for (Object[] objects : runList) {
					if (objects[0].equals("Fail")) {
//						runStatus.setFailCount(((BigInteger) objects[1]).intValue());
						runStatus.setFailCount((BigInteger.valueOf((long) objects[1]).intValue()));
					} else if (objects[0].equals("Pass")) {
						
						runStatus.setPassCount((BigInteger.valueOf((long) objects[1]).intValue()));

//						runStatus.setPassCount(((BigInteger) objects[1]).intValue());
					} else {
//						runStatus.setUntestedCount(runStatus.getUntestedCount() + ((BigInteger) objects[1]).intValue());
						runStatus.setUntestedCount(runStatus.getUntestedCount() + ((BigInteger.valueOf((long) objects[1]).intValue())));
					}

				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		/*
		 * finally { session.close(); }
		 */
		return runStatus;
	}

	public DefectStatusBo getOverallDefecttatus(Integer runId) {
		List<Object[]> actionList = null;
		DefectStatusBo defectStatusBo = new DefectStatusBo();
		String actionDetailsHQL = "SELECT mr.bugSeverity AS name , COUNT(*) FROM MannualTestCaseExecutionLogs mr \r\n"
				+ "WHERE mr.reRunId=:reRunId and mr.testCaseAction='Completed' and mr.status='Fail' GROUP BY mr.bugSeverity";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(actionDetailsHQL);
			query.setParameter("reRunId", runId);
			actionList = query.list();
			if (actionList != null) {
				for (Object[] objects : actionList) {
					if (objects[0] != null) {
						if (objects[0].equals("Critical")) {
//							defectStatusBo.setCritical(((BigInteger) objects[1]).intValue());
							defectStatusBo.setCritical((BigInteger.valueOf((long) objects[1]).intValue()));
						} else if (objects[0].equals("Major")) {
//							defectStatusBo.setMajor(((BigInteger) objects[1]).intValue());
							defectStatusBo.setMajor((BigInteger.valueOf((long) objects[1]).intValue()));
						} else if (objects[0].equals("Minor")) {
//							defectStatusBo.setMinor(((BigInteger) objects[1]).intValue());
							defectStatusBo.setMinor((BigInteger.valueOf((long) objects[1]).intValue()));
						} else {
//							defectStatusBo.setBlocker(defectStatusBo.getBlocker() + ((BigInteger) objects[1]).intValue());
							defectStatusBo.setBlocker(defectStatusBo.getBlocker() + ((BigInteger.valueOf((long) objects[1]).intValue())));
						}
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return defectStatusBo;
	}

	public OverallRunActionStatus getOverallRunActionStatus(Integer reRunId) {
		List<Object[]> actionList = null;
		OverallRunActionStatus runStatus = new OverallRunActionStatus();
		String actionDetailsHQL = "SELECT mr.testCaseAction AS name , COUNT(*) FROM MannualTestCaseExecutionLogs mr \r\n"
				+ "WHERE mr.reRunId=:reRunId GROUP BY mr.testCaseAction";
		Session session=entityManager.unwrap(Session.class);
		session.setCacheMode(CacheMode.IGNORE);
		session.flush();
		session.clear();
		try {
			Query query = session.createNativeQuery(actionDetailsHQL);
			query.setParameter("reRunId", reRunId);
			actionList = query.list();
			if (actionList != null) {
				for (Object[] objects : actionList) {
					try{
						if (objects[0].equals("Under Review")) {
							
//							runStatus.setUnderReview(((BigInteger) objects[1]).intValue());
							runStatus.setUnderReview((BigInteger.valueOf((long) objects[1]).intValue()));
						} else if (objects[0].equals("Completed")) {
//							runStatus.setCompleted(((BigInteger) objects[1]).intValue());
							runStatus.setCompleted((BigInteger.valueOf((long) objects[1]).intValue()));
						} else if (objects[0].equals("On Hold")) {
//							runStatus.setOnHold(((BigInteger) objects[1]).intValue());
							runStatus.setOnHold((BigInteger.valueOf((long) objects[1]).intValue()));

						} else if (objects[0].equals("N/A")) {
//							runStatus.setNa(((BigInteger) objects[1]).intValue());
							runStatus.setNa((BigInteger.valueOf((long) objects[1]).intValue()));


						} else if (objects[0].equals("Incomplete")) {
//							runStatus.setTotalactionincomplete(((BigInteger) objects[1]).intValue());
							runStatus.setTotalactionincomplete((BigInteger.valueOf((long) objects[1]).intValue()));


						}
						else {
//							runStatus.setUnTested(runStatus.getUnTested() + ((BigInteger) objects[1]).intValue());
							runStatus.setUnTested(runStatus.getUnTested() + ((BigInteger.valueOf((long) objects[1]).intValue())));
						}
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}//for
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		/*
		 * finally { session.close(); }
		 */
		return runStatus;
	}

	public List<Testrunwisedetails> testrunwisedetails(Integer projectid) {
		List<Object[]> list = null;
		List<Testrunwisedetails> executionLogs = new ArrayList<>();
		String overViewProjectHQL = " SELECT count(IF(mt.testCaseAction != 'null', 1, NULL)) as totalcases,mr.runId,mr.runName,\r\n" + 
				"				count(IF(mt.status = 'Fail', 1, NULL) && IF(mt.testCaseAction = 'Completed', 1, NULL)) as Total_Fail,\r\n" + 
				"				count(IF(mt.status = 'Pass', 1, NULL) && IF(mt.testCaseAction = 'Completed', 1, NULL)) as Total_Pass,\r\n" + 
				"				count(IF(mt.status is null, 1, NULL)) as Total_Unexecuted,\r\n" + 
				"				count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,\r\n" + 
				"				count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',\r\n" + 
				"				count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',\r\n" + 
				"				count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started',\r\n" + 
				"				count(IF(mt.testCaseAction = 'N/A', 1, NULL)) as 'N/A',\r\n" + 
				"				count(IF(mt.bugSeverity = 'major' AND mt.status = 'Fail', 1, NULL)) as Major,\r\n" + 
				"				count(IF(mt.bugSeverity = 'minor' AND mt.status = 'Fail', 1, NULL)) as Minor,\r\n" + 
				"				count(IF(mt.bugSeverity = 'critical' AND mt.status = 'Fail', 1, NULL)) as Critical,\r\n" + 
				"				count(IF(mt.bugSeverity = 'blocker' AND mt.status = 'Fail', 1, NULL)) as blocker,sum(mt.executionTime) as Total_secs,mr.round,\r\n" + 
				"				count(IF(mt.bugstatus = 'Open' AND mt.testCaseAction = 'Completed', 1, NULL)) as Open,\r\n" + 
				"				count(IF(mt.bugstatus = 'Closed' AND mt.testCaseAction = 'Completed', 1, NULL)) as Closed,\r\n" + 
				"				count(IF(mt.bugstatus = 'Reopen' AND mt.testCaseAction = 'Completed', 1, NULL)) as Reopen,mr.status,mr.id,mr.isActive\r\n" + 
				"				,count(IF(mt.bugstatus = 'Resolved' AND mt.testCaseAction = 'Completed', 1, NULL)) as Resolved ,\r\n" + 
				"				count(IF(mt.status = 'Blocked-Showstopper', 1, NULL)) as Total_Blocked_Showstopper,\r\n" + 
				"								count(IF(mt.status = 'Blocked-Undelivered', 1, NULL) ) as Total_Blocked_Undelivered,\r\n" + 
				"								count(IF(mt.status = 'NA', 1, NULL) ) as Total_NA,\r\n" + 
				"								count(IF(mt.status = 'Blocked-Testdata', 1, NULL) ) as Total_Blocked_Test_data,\r\n" + 
				"								count(IF(mt.status = 'Blocked-FSD not clear', 1, NULL) ) as Total_BlockedFSDnotclear,\r\n" + 
				"								count(IF(mt.status = 'NE', 1, NULL) ) as Total_NE,\r\n" + 
				"								count(IF(mt.status = 'Deferred', 1, NULL) ) as Total_Deferred,\r\n" + 
				"								count(IF(mt.status = 'On Hold', 1, NULL) ) as Total_OnHold,\r\n" + 
				"								count(IF(mt.status = 'Duplicate', 1, NULL) ) as Total_Duplicate,\r\n" + 
				"								count(IF(mt.status = 'Out of Scope', 1, NULL) ) as Total_OutofScope,\r\n" + 
				"								count(IF(mt.complexity = 'Must to execute', 1, NULL) ) as Total_MTE,\r\n" + 
				"								count(IF(mt.complexity = 'Need to execute', 1, NULL) ) as Total_NTE,\r\n" + 
				"								count(IF(mt.complexity = 'Nice to execute', 1, NULL) ) as Total_NITE,\r\n" + 
				"								count(IF(mt.priority = 'High', 1, NULL) ) as Total_high,\r\n" + 
				"								count(IF(mt.priority = 'Medium', 1, NULL) ) as Total_Medium,\r\n" + 
				"								count(IF(mt.priority = 'Low', 1, NULL) ) as Total_low,"+
				"				count(IF(mt.status = 'Partially Pass', 1, NULL)) as Total_Partially_Pass,\r\n" + 
				"				count(IF(mt.testCaseAction = 'incomplete', 1, NULL)) as 'Total_action_incomplete' \r\n" + 
				""+ " FROM MannualReRunTable mr\r\n" + 
				"				LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mr.id where mr.projectId=:projectid\r\n" + 
				"				group by  mr.id order by mr.id desc\r\n" + 
				"\r\n" + 
				"";
		/*
		 * "SELECT count(IF(mt.testCaseAction != '', 1, NULL)) as totalcases,mr.runId,mr.runName,\r\n"
		 * +
		 * "				count(IF(mt.status = 'Fail', 1, NULL) && IF(mt.testCaseAction = 'Completed', 1, NULL)) as Total_Fail,\r\n"
		 * +
		 * "				count(IF(mt.status = 'Pass', 1, NULL) && IF(mt.testCaseAction = 'Completed', 1, NULL)) as Total_Pass,\r\n"
		 * +
		 * "				count(IF(mt.status is null, 1, NULL)) as Total_Unexecuted,\r\n"
		 * +
		 * "				count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,\r\n"
		 * +
		 * "				count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',\r\n"
		 * +
		 * "				count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',\r\n"
		 * +
		 * "				count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started',\r\n"
		 * + "				count(IF(mt.testCaseAction = 'N/A', 1, NULL)) as 'N/A',\r\n"
		 * +
		 * "				count(IF(mt.bugSeverity = 'major' AND mt.status = 'Fail', 1, NULL)) as Major,\r\n"
		 * +
		 * "				count(IF(mt.bugSeverity = 'minor' AND mt.status = 'Fail', 1, NULL)) as Minor,\r\n"
		 * +
		 * "				count(IF(mt.bugSeverity = 'critical' AND mt.status = 'Fail', 1, NULL)) as Critical,\r\n"
		 * +
		 * "				count(IF(mt.bugSeverity = 'blocker' AND mt.status = 'Fail', 1, NULL)) as blocker,sum(mt.executionTime) as Total_secs,mr.round,\r\n"
		 * +
		 * "				count(IF(mt.bugstatus = 'Open' AND mt.testCaseAction = 'Completed', 1, NULL)) as Open,\r\n"
		 * +
		 * "				count(IF(mt.bugstatus = 'Closed' AND mt.testCaseAction = 'Completed', 1, NULL)) as Closed,\r\n"
		 * +
		 * "				count(IF(mt.bugstatus = 'Reopen' AND mt.testCaseAction = 'Completed', 1, NULL)) as Reopen,mr.status,mr.id,mr.isActive\r\n"
		 * +
		 * "				,count(IF(mt.bugstatus = 'Resolved' AND mt.testCaseAction = 'Completed', 1, NULL)) as Resolved FROM MannualReRunTable mr\r\n"
		 * +
		 * "				LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mr.id where mr.projectId=:projectid\r\n"
		 * + "				group by  mr.id order by mr.id desc";
		 */
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			query.setParameter("projectid", projectid);
			list = query.list();
			for (Object[] objects : list) {
				try {
					Testrunwisedetails test = new Testrunwisedetails();
					test.setTotalcases(Integer.valueOf(objects[0].toString()));
					try {
						test.setRunid(Integer.valueOf(objects[1].toString()));
					} catch (Exception e) {
						test.setRunid(Integer.valueOf(objects[11].toString()));
					}
					try {

						test.setRunname(objects[2].toString());
						test.setTotalfail(Integer.valueOf(objects[3].toString()));
						test.setTotalpass(Integer.valueOf(objects[4].toString()));
						//test.setTotalexecuted(Integer.valueOf(objects[5].toString()));
						test.setTotalunexecuted(Integer.valueOf(objects[5].toString()));
						test.setTotalcompleted(Integer.valueOf(objects[6].toString()));
						test.setTotalonhold(Integer.valueOf(objects[7].toString()));
						test.setTotalnotstarted(Integer.valueOf(objects[9].toString()));
						test.setTotalna(Integer.valueOf(objects[10].toString()));
						test.setTotalmajor(Integer.valueOf(objects[11].toString()));
						test.setTotalminor(Integer.valueOf(objects[12].toString()));
						test.setTotalcritical(Integer.valueOf(objects[13].toString()));
						test.setTotalblocker(Integer.valueOf(objects[14].toString()));
						if (objects[15] == null) {
							test.setTotalmanhours((float) 0.0);
						} else {
							test.setTotalmanhours(Float.valueOf(objects[15].toString()));
						}
						test.setRound(Integer.valueOf(objects[16].toString()));
						test.setOpen(Integer.valueOf(objects[17].toString()));
						test.setReopen(Integer.valueOf(objects[19].toString()));
						test.setClosed(Integer.valueOf(objects[18].toString()));
						test.setRunround(test.getRunname() + "_" + test.getRound());
						test.setStatus((String) objects[20]);
						test.setIsActive((String) objects[22]);
						test.setResolved(Integer.valueOf(objects[23].toString()));
						test.setBlockshowtopper(Integer.valueOf(objects[24].toString()));
						test.setBlockedundelivered(Integer.valueOf(objects[25].toString()));
						test.setBlockedtestdata(Integer.valueOf(objects[27].toString()));
						test.setNa(Integer.valueOf(objects[26].toString()));
						test.setBlockednotanclear(Integer.valueOf(objects[28].toString()));
						test.setNe(Integer.valueOf(objects[29].toString()));
						test.setDeferred(Integer.valueOf(objects[30].toString()));
						test.setOnhold(Integer.valueOf(objects[31].toString()));
						test.setDuplicate(Integer.valueOf(objects[32].toString()));
						test.setOutofScope(Integer.valueOf(objects[33].toString()));
						test.setNicete(Integer.valueOf(objects[36].toString()));
						test.setMustte(Integer.valueOf(objects[34].toString()));
						test.setNeedte(Integer.valueOf(objects[35].toString()));
						test.setHigh(Integer.valueOf(objects[37].toString()));
						test.setMedium(Integer.valueOf(objects[38].toString()));
						test.setLow(Integer.valueOf(objects[39].toString()));
						test.setPartiallypass(Integer.valueOf(objects[40].toString()));
						test.setTotalactionincomplete(Integer.valueOf(objects[41].toString()));
						executionLogs.add(test);
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}//for
				catch (Exception e) {
					e.printStackTrace();
				}
			}//for
			System.out.println(executionLogs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return executionLogs;
	}

	public List<runwisestatus> runwisestatus(Integer projectid) {
		List<Object[]> list = null;
		List<runwisestatus> executionLogs = new ArrayList<>();
		String overViewProjectHQL = "SELECT count(IF(mt.testCaseAction != '', 1, NULL)) as totalcases,mt.reRunId, mr.runName,\r\n"
				+ "				count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,\r\n"
				+ "				count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',\r\n"
				+ "				count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',\r\n"
				+ "				count(IF(mt.testCaseAction = 'On Hold', 1, NULL))+count(IF(mt.testCaseAction = 'Completed', 1, NULL))\r\n"
				+ "				+count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as totalExecuted,\r\n"
				+ "				count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started',\r\n"
				+ "                count(IF(mt.testCaseAction = 'N/A', 1, NULL)) as 'NA',\r\n"
				+ "				sum(mt.executionTime) as Total_secs,\r\n"
				+ "				count(*)-count(IF(mt.testCaseAction = 'On Hold', 1, NULL))\r\n"
				+ "				-count(IF(mt.testCaseAction = 'Under Review', 1, NULL))\r\n"
				+ "				-count(IF(mt.testCaseAction = 'Completed', 1, NULL))\r\n"
				+ "				-count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as NE,mr.round ,mr.runId,mr.id,\r\n"
				+ "				count(IF(mt.testCaseAction = 'incomplete', 1, NULL)) as 'Incomplete'\r\n"
				+ "				FROM MannualReRunTable mr  LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mr.id\r\n"
				+ "				WHERE mr.projectId=:projectid AND mr.isActive='Y' AND mr.STATUS='Running' order BY mr.id desc limit 5";
		Session session=entityManager.unwrap(Session.class);
		try {
			// test
			Query query = session.createNativeQuery(overViewProjectHQL);
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			query.setParameter("projectid", projectid);
			list = query.list();
			for (Object[] objects : list) {
				runwisestatus test = new runwisestatus();
				try {
					test.setTotalcases(objects[0]==null?null:Integer.valueOf(objects[0].toString()));
					try {
						test.setRunid(Integer.valueOf(objects[1].toString()));
					} catch (NullPointerException e) {
						e.printStackTrace();
						test.setRunid(Integer.valueOf(objects[13].toString()));
						// test.setRunid(Integer.valueOf(objects[1].toString()));

					} catch (Exception e) {
						e.printStackTrace();
						test.setRunid(Integer.valueOf(objects[11]==null?null:objects[11].toString()));
					}

					test.setRunname(objects[2]==null?null:objects[2].toString());
					test.setTotalcompleted(objects[3]==null?null:Integer.valueOf(objects[3].toString()));
					test.setTotalonhold(objects[4]==null?null:Integer.valueOf(objects[4].toString()));
					test.setTotalunderreview(objects[5]==null?null:Integer.valueOf(objects[5].toString()));
					test.setTotalexecuted(objects[6]==null?null:Integer.valueOf(objects[6].toString()));
					test.setTotalnotstarted(objects[7]==null?null:Integer.valueOf(objects[7].toString()));
					test.setTotalNA(objects[8]==null?null:Integer.valueOf(objects[8].toString()));
					if (objects[9] == null) {
						test.setTotalmanhours((float) 0.0);
					} else {
						test.setTotalmanhours(Float.valueOf(objects[9].toString()));
					}
					test.setTotalNe(objects[10]==null?null:Integer.valueOf(objects[10].toString()));
					test.setRound(objects[11]==null?null:Integer.valueOf(objects[11].toString()));
					test.setRoundrunname(test.getRunname() + "_" + test.getRound());
					test.setRerunId(objects[12]==null?null:Integer.valueOf(objects[12].toString()));
					test.setIncomplete(objects[14]==null?null:Integer.valueOf(objects[14].toString()));
					executionLogs.add(test);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}//for
			System.out.println(executionLogs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return executionLogs;
	}

	public List<projectwisefailstatus> projectwisefailstatus(Integer projectid) {
		List<Object[]> list = null;
		List<projectwisefailstatus> executionLogs = new ArrayList<>();
		String overViewProjectHQL = "select count(IF(mt.status = 'Fail', 1, NULL)) as totalFail,\r\n"
				+ "	count(IF((mt.bugSeverity = 'critical') AND (mt.bugstatus = 'Open'), 1, NULL)) as CriticalOpen,\r\n"
				+ "	count(IF((mt.bugSeverity = 'critical') AND (mt.bugstatus = 'Closed'), 1, NULL)) as CriticalClosed,\r\n"
				+ "	count(IF((mt.bugSeverity = 'critical') AND (mt.bugstatus = 'Reopen'), 1, NULL)) as CriticalReopen,\r\n"
				+ "	count(IF((mt.bugSeverity = 'critical') AND (mt.bugstatus = 'Resolved'), 1, NULL)) as CriticalResolved,\r\n"
				+ "	count(IF(mt.bugSeverity = 'blocker' and mt.bugstatus = 'Open', 1, NULL)) AS BlockerOpen,\r\n"
				+ "	count(IF((mt.bugSeverity = 'blocker') AND (mt.bugstatus = 'Closed'), 1, NULL)) as BlockerClosed,\r\n"
				+ "	count(IF((mt.bugSeverity = 'blocker') AND (mt.bugstatus = 'Reopen'), 1, NULL)) as BlockerReopen,\r\n"
				+ "	count(IF((mt.bugSeverity = 'blocker') AND (mt.bugstatus = 'Resolved'), 1, NULL)) as BlockerResolved,\r\n"
				+ "	count(IF(mt.bugSeverity = 'major' and mt.bugstatus = 'Open', 1, NULL)) as MajorOpen,\r\n"
				+ "	count(IF((mt.bugSeverity = 'major') AND (mt.bugstatus = 'Closed'), 1, NULL)) as MajorClosed,\r\n"
				+ "	count(IF((mt.bugSeverity = 'major') AND (mt.bugstatus = 'Reopen'), 1, NULL)) as MajorReopen,\r\n"
				+ "	count(IF((mt.bugSeverity = 'major') AND (mt.bugstatus = 'Resolved'), 1, NULL)) as MajorResolved,\r\n"
				+ "	count(IF((mt.bugSeverity = 'minor') and (mt.bugstatus = 'Open'), 1, NULL)) as MinorOpen,\r\n"
				+ "	count(IF((mt.bugSeverity = 'minor') AND (mt.bugstatus = 'Closed'), 1, NULL)) as MinorClosed,\r\n"
				+ "	count(IF((mt.bugSeverity = 'minor') AND (mt.bugstatus = 'Reopen'), 1, NULL)) as MinorReopen,\r\n"
				+ "	count(IF((mt.bugSeverity = 'minor') AND (mt.bugstatus = 'Resolved'), 1, NULL)) as MinorResolved,\r\n"
				+ "	mr.id,mr.runName,mr.round,mr.runId FROM MannualReRunTable mr LEFT JOIN \r\n"
				+ "	MannualTestCaseExecutionLogs mt on mr.id=mt.reRunId  where mr.projectId=:projectid\r\n"
				+ "	 and mr.isActive='Y' and mr.STATUS='Running' order by mt.reRunId desc LIMIT 5;";
		/*
		 * "select count(IF(mt.status = 'Fail', 1, NULL)) as totalFail,\r\n" +
		 * "count(IF(mt.bugSeverity = 'major', 1, NULL)) as Major,\r\n" +
		 * "count(IF(mt.bugSeverity = 'minor', 1, NULL)) as Minor,\r\n" +
		 * "count(IF(mt.bugSeverity = 'critical', 1, NULL)) as Critical,\r\n" +
		 * "count(IF(mt.bugSeverity = 'blocker', 1, NULL)) as blocker,mr.id,\r\n" +
		 * "mr.runName,mr.round," + "count(IF(mt.bugstatus = 'Open', 1, NULL)) as Open,"
		 * + "count(IF(mt.bugstatus = 'Closed', 1, NULL)) as Closed," +
		 * "count(IF(mt.bugstatus = 'Reopen', 1, NULL)) as Reopen,mr.runId\r\n" +
		 * " FROM MannualReRunTable mr LEFT JOIN \r\n" +
		 * "MannualTestCaseExecutionLogs mt on mr.id=mt.reRunId  where mr.projectId=:projectid and mr.isActive='Y' and mr.STATUS='Running' group by mt.reRunId desc limit 5\r\n"
		 * + "\r\n" + "";
		 */
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			query.setParameter("projectid", projectid);
			list = query.list();
			for (Object[] objects : list) {
				try {
					projectwisefailstatus test = new projectwisefailstatus();
					test.setTotalrunfail(objects[0]==null?null:Integer.valueOf(objects[0].toString()));
					test.setTotalcriticalopen(objects[1]==null?null:Integer.valueOf(objects[1].toString()));
					test.setTotalcriticalclosed(objects[2]==null?null:Integer.valueOf(objects[2].toString()));
					test.setTotalcriticalreopen(objects[3]==null?null:Integer.valueOf(objects[3].toString()));
					test.setTotalcriticalresolved(objects[4]==null?null:Integer.valueOf(objects[4].toString()));
					test.setTotalblockeropen(objects[5]==null?null:Integer.valueOf(objects[5].toString()));
					test.setTotalblockerclosed(objects[6]==null?null:Integer.valueOf(objects[6].toString()));
					test.setTotalblockerreopen(objects[7]==null?null:Integer.valueOf(objects[7].toString()));
					test.setTotalblockerresolved(objects[8]==null?null:Integer.valueOf(objects[8].toString()));
					test.setTotalmajoropen(objects[9]==null?null:Integer.valueOf(objects[9].toString()));
					test.setTotalmajorclosed(objects[10]==null?null:Integer.valueOf(objects[10].toString()));
					test.setTotalmajorreopen(objects[11]==null?null:Integer.valueOf(objects[11].toString()));
					test.setTotalmajorresolved(objects[12]==null?null:Integer.valueOf(objects[12].toString()));
					test.setTotalminoropen(objects[13]==null?null:Integer.valueOf(objects[13].toString()));
					test.setTotalminorclosed(objects[14]==null?null:Integer.valueOf(objects[14].toString()));
					test.setTotalminorreopen(objects[15]==null?null:Integer.valueOf(objects[15].toString()));
					test.setTotalminorresolved(objects[16]==null?null:Integer.valueOf(objects[16].toString()));
					test.setRunId((Integer)objects[17]);
					test.setRunname(objects[18]==null?null:objects[18].toString());
					test.setRound(objects[19]==null?null:Integer.valueOf(objects[19].toString()));
					/*
					 * test.setTotalmajor(Integer.valueOf(objects[1].toString()));
					 * test.setTotalminor(Integer.valueOf(objects[2].toString()));
					 * test.setTotalcritical(Integer.valueOf(objects[3].toString()));
					 * test.setTotalblocker(Integer.valueOf(objects[4].toString()));
					 * test.setRunid(Integer.valueOf(objects[5].toString()));
					 * test.setRunname(objects[6].toString());
					 * test.setRound(Integer.valueOf(objects[7].toString()));
					 * test.setTotalopen(Integer.valueOf(objects[8].toString()));
					 * test.setTotalclosed(Integer.valueOf(objects[9].toString()));
					 * test.setTotalreopen(Integer.valueOf(objects[10].toString()));
					 * test.setRunId(Integer.valueOf(objects[11].toString()));
					 */
					test.setRoundrunname(test.getRunname() + "_" + test.getRound());
					executionLogs.add(test);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			System.out.println(executionLogs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return executionLogs;
	}

	public List<com.apmosys.tmt.models.runwisestatus> runwisestatuscheck(String runlist) {
		List<Object[]> list = null;
		List<runwisestatus> executionLogs = new ArrayList<>();
		String overViewProjectHQL = "SELECT count(IF(mt.testCaseAction != '', 1, NULL)) as totalcases,mr.id,\r\n"
				+ "mr.runName," + "count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,\r\n"
				+ "count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',\r\n"
				+ "count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',\r\n"
				+ "count(IF(mt.testCaseAction = 'On Hold', 1, NULL))+count(IF(mt.testCaseAction = 'Completed', 1, NULL))\r\n"
				+ "+count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as totalExecuted,\r\n"
				+ "count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started',\r\n"
				+ "count(IF(mt.testCaseAction = 'N/A', 1, NULL)) AS 'N/A',\r\n"
				+ "sum(mt.executionTime) as Total_secs,\r\n"
				+ "count(*)-count(IF(mt.testCaseAction = 'On Hold', 1, NULL))\r\n"
				+ "-count(IF(mt.testCaseAction = 'Under Review', 1, NULL))\r\n"
				+ "-count(IF(mt.testCaseAction = 'Completed', 1, NULL))\r\n"
				+ "-count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as NE,mr.round \r\n"
				+ "FROM MannualReRunTable mr\r\n" + "LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mr.id\r\n"
				+ "WHERE mr.id in (\'" + runlist + "\') AND mr.isActive='Y' AND mr.STATUS='Running'\r\n"
				+ "group BY mr.id";
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			list = query.list();
			for (Object[] objects : list) {
				try {

					runwisestatus test = new runwisestatus();
					test.setTotalcases(objects[0]==null?null:Integer.valueOf(objects[0].toString()));
					test.setRunid(objects[1]==null?null:Integer.valueOf(objects[1].toString()));
					test.setRunname(objects[2]==null?null:objects[2].toString());
					test.setTotalcompleted(objects[3]==null?null:Integer.valueOf(objects[3].toString()));
					test.setTotalonhold(objects[4]==null?null:Integer.valueOf(objects[4].toString()));
					test.setTotalunderreview(objects[5]==null?null:Integer.valueOf(objects[5].toString()));
					test.setTotalexecuted(objects[6]==null?null:Integer.valueOf(objects[6].toString()));
					test.setTotalnotstarted(objects[7]==null?null:Integer.valueOf(objects[7].toString()));
					test.setTotalNA(objects[8]==null?null:Integer.valueOf(objects[8].toString()));
					if (objects[9] == null) {
						test.setTotalmanhours((float) 0.0);
					} else {
						test.setTotalmanhours(Float.valueOf(objects[9].toString()));
					}
					test.setTotalNe(objects[10]==null?null:Integer.valueOf(objects[10].toString()));
					test.setRound(objects[11]==null?null:Integer.valueOf(objects[11].toString()));
					test.setRoundrunname(test.getRunname() + "_" + test.getRound());
					executionLogs.add(test);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}//for
			System.out.println(executionLogs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return executionLogs;
	}

	public List<com.apmosys.tmt.models.projectwisefailstatus> runwisefailstatus(String runlist) {

		List<Object[]> list = null;
		List<projectwisefailstatus> executionLogs = new ArrayList<>();
		String overViewProjectHQL = "select count(IF(mt.status = 'Fail', 1, NULL)) as totalFail,\r\n"
				+ "count(IF(mt.bugSeverity = 'major', 1, NULL)) as Major,\r\n"
				+ "count(IF(mt.bugSeverity = 'minor', 1, NULL)) as Minor,\r\n"
				+ "count(IF(mt.bugSeverity = 'critical', 1, NULL)) as Critical,\r\n"
				+ "count(IF(mt.bugSeverity = 'blocker', 1, NULL)) as blocker,mr.id,\r\n" + "mr.runName,mr.round,"
				+ "count(IF(mt.bugstatus = 'Open', 1, NULL)) as Open,"
				+ "count(IF(mt.bugstatus = 'Closed', 1, NULL)) as Closed,"
				+ "count(IF(mt.bugstatus = 'Reopen', 1, NULL)) as Reopen\r\n"
				+ " FROM MannualReRunTable mr LEFT JOIN \r\n"
				+ "MannualTestCaseExecutionLogs mt on mr.id=mt.reRunId  where mr.id in (\'" + runlist
				+ "\') and mr.isActive='Y' and mr.STATUS='Running' group by mt.reRunId\r\n" + "\r\n" + "";
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			list = query.list();
			for (Object[] objects : list) {
				try {
					projectwisefailstatus test = new projectwisefailstatus();
					test.setTotalrunfail(objects[0]==null?null:Integer.valueOf(objects[0].toString()));
					test.setTotalmajor(objects[1]==null?null:Integer.valueOf(objects[1].toString()));
					test.setTotalminor(objects[2]==null?null:Integer.valueOf(objects[2].toString()));
					test.setTotalcritical(objects[3]==null?null:Integer.valueOf(objects[3].toString()));
					test.setTotalblocker(objects[4]==null?null:Integer.valueOf(objects[4].toString()));
					test.setRunid(objects[5]==null?null:Integer.valueOf(objects[5].toString()));
					test.setRunname(objects[6]==null?null:objects[6].toString());
					test.setRound(objects[7]==null?null:Integer.valueOf(objects[7].toString()));
					test.setTotalopen(objects[8]==null?null:Integer.valueOf(objects[8].toString()));
					test.setTotalclosed(objects[9]==null?null:Integer.valueOf(objects[9].toString()));
					test.setTotalreopen(objects[10]==null?null:Integer.valueOf(objects[10].toString()));
					test.setRoundrunname(test.getRunname() + "_" + test.getRound());
					executionLogs.add(test);
				}//try
				catch (Exception e) {
					// TODO: handle exception
				}
			}//for
			System.out.println(executionLogs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return executionLogs;

	}

	/*
	 * public Map<String, List<ResourceResponseBo>>
	 * getResourseEfforts(ResourceRequestBo requestBo) { List<Object[]> list = null;
	 * List<ResourceResponseBo> effortsList = new ArrayList<>(); Map<String,
	 * List<ResourceResponseBo>> maps = new HashedMap<String,
	 * List<ResourceResponseBo>>(); String whereCond="";
	 * if(!(requestBo.getFromDate()==null && requestBo.getToDate()==null)) {
	 * whereCond=whereCond +
	 * "AND DATE(m.dateOfExec) BETWEEN '"+requestBo.getFromDate()+"' AND '"
	 * +requestBo.getToDate()+"'  \r\n"; } String overViewProjectHQL =
	 * "SELECT u.name ,COUNT(*),DATE(m.dateOfExec)\r\n" +
	 * "FROM MannualTestCaseExecutionLogs m,UserDetailsEntity u,MannualReRunTable mr\r\n"
	 * +
	 * "WHERE m.testerId=u.id AND m.reRunId=mr.id AND  m.testerId IN (:testerId) AND mr.runId IN (:reRunId) AND m.testCaseAction=:action\r\n"
	 * + whereCond + "GROUP BY m.testerId,DATE(m.dateOfExec)";
	 * 
	 * Session session=entityManager.unwrap(Session.class); try {
	 * session.setCacheMode(CacheMode.IGNORE); session.flush(); session.clear();
	 * Query query = session.createNativeQuery(overViewProjectHQL);
	 * query.setParameter("action", "Completed"); query.setParameterList("testerId",
	 * requestBo.getTesterList()); query.setParameterList ("reRunId",
	 * requestBo.getRunList()); list = query.list(); for (Object[] objects : list) {
	 * if(objects[2]!=null) { ResourceResponseBo responseBo=new
	 * ResourceResponseBo(); responseBo.setExeCount(((BigInteger)
	 * objects[1]).toString()); responseBo.setExeDate(((Date)
	 * objects[2]).toString()); responseBo.setTesterName ((String) objects[0]);
	 * System.out.println(responseBo); effortsList.add(responseBo); } } for
	 * (ResourceResponseBo details1 : effortsList) { if (details1 != null) {
	 * List<ResourceResponseBo> values = effortsList.stream() .filter(x ->
	 * x.getTesterName().equals(details1.getTesterName())).collect(Collectors.toList
	 * ()); if (values != null) { maps.put(details1.getTesterName(), values); }
	 * 
	 * }
	 * 
	 * } } catch (Exception e) { e.printStackTrace(); } return maps; }
	 */
	public Map<String, List<ResourceResponseBo>> getResourseEfforts(ResourceRequestBo requestBo, String type) {
		List<Object[]> list = null;
		List<ResourceResponseBo> effortsList = new ArrayList<>();
		Map<String, List<ResourceResponseBo>> maps = new HashedMap<String, List<ResourceResponseBo>>();
		String whereCond = "";
		String groupByCond = "";
		/*
		 * if(type=="fresh_run") { whereCond=whereCond + "AND mr.round = '1'	\r\n"; }
		 */
		if (type == "executed") {
			groupByCond = ",DATE(m.dateOfExec)";
		}
		if ((requestBo.getFromDate() == null && requestBo.getToDate() == null)) {
			whereCond = whereCond + "AND DATE(m.dateOfExec) BETWEEN (NOW() - INTERVAL 7 DAY) AND NOW()";
		}
		if (!(requestBo.getFromDate() == null && requestBo.getToDate() == null)) {
			whereCond = whereCond + "AND DATE(m.dateOfExec) BETWEEN '" + requestBo.getFromDate() + "' AND '"
					+ requestBo.getToDate() + "'  \r\n";
		}

		String overViewProjectHQL = "SELECT u.name ,COUNT(*),DATE(m.dateOfExec)\r\n"
				+ "FROM MannualTestCaseExecutionLogs m,UserDetailsEntity u,MannualReRunTable mr\r\n"
				+ "WHERE m.testerId=u.id AND m.reRunId=mr.id AND  m.testerId IN (:testerId) AND mr.runId IN (:reRunId) AND m.testCaseAction=:action\r\n"
				+ whereCond + "GROUP BY m.testerId" + groupByCond;

		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("action", "Completed");
			query.setParameterList("testerId", requestBo.getTesterList());
			query.setParameterList("reRunId", requestBo.getRunList());
			list = query.list();
			for (Object[] objects : list) {
				try {
					
				if (objects[2] != null) {
					ResourceResponseBo responseBo = new ResourceResponseBo();
//					responseBo.setExeCount(((BigInteger) objects[1]).toString());
					responseBo.setExeCount((BigInteger.valueOf((long) objects[1]).toString()));					
					responseBo.setExeDate(((Date) objects[2]).toString());
					responseBo.setTesterName((String) objects[0]);
					System.out.println("hello lipsa           " + responseBo);
					effortsList.add(responseBo);
				}
				}
				catch (Exception e) {
e.printStackTrace();
				}
			}
			for (ResourceResponseBo details1 : effortsList) {
				try {
					
				if (details1 != null) {
					List<ResourceResponseBo> values = effortsList.stream()
							.filter(x -> x.getTesterName().equals(details1.getTesterName()))
							.collect(Collectors.toList());
					if (values != null) {
						maps.put(details1.getTesterName(), values);
					}
				}
				}catch (Exception e) {
e.printStackTrace();
				}

			}//for
		} catch (Exception e) {
			e.printStackTrace();
		}
		return maps;
	}

	public List<ResourceResponseBo> tableResourceEfforts(ResourceRequestBo requestBo) {
		List<Object[]> list = null;
		List<ResourceResponseBo> effortsList = new ArrayList<>();
		String whereCond = "\r\n";
		String whereTCCond = "\r\n";
		if (!(requestBo.getFromDate() == null && requestBo.getToDate() == null)) {
			whereCond = whereCond + "AND DATE(m.dateOfExec) BETWEEN '" + requestBo.getFromDate() + "' AND '"
					+ requestBo.getToDate() + "'  \r\n";
			whereTCCond = whereTCCond + "AND DATE(mc.createdDate) BETWEEN '" + requestBo.getFromDate() + "' AND '"
					+ requestBo.getToDate() + "'  \r\n";
		}
		String tableData = "SELECT tmp.name, sum(tmp.total) as exeCount, sum(tmp.executionTime), sum(tmp.pass) AS pass\r\n"
				+ ", sum(tmp.fail) as fail, (SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId \r\n"
				+ "		IN(SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId \r\n"
				+ "		in(SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=:pID\r\n"
				+ "		 AND mt.isDeleted='N')) AND mc.prepared_by=tmp.testerId \r\n" + whereTCCond
				+ "		 ) AS prepCount\r\n"
				+ ", (SELECT SUM(mc.preparation_time) from Mannual_TestCase_Table mc WHERE mc.testGroupId \r\n"
				+ "		IN(SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId \r\n"
				+ "		in(SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId=:pID AND mt.isDeleted='N' \r\n"
				+ "		)) AND mc.prepared_by=tmp.testerId " + whereTCCond + ")\r\n" + "		AS prepTime FROM\r\n"
				+ "	(SELECT u.name ,COUNT(*) AS total,sum(m.executionTime) AS executionTime\r\n"
				+ "	, if(m.STATUS='Pass',count(m.STATUS),0) AS pass, if(m.STATUS='Fail',count(m.STATUS),0) AS fail, m.testerId\r\n"
				+ "FROM MannualTestCaseExecutionLogs m, UserDetailsEntity u,MannualReRunTable mr  \r\n"
				+ "WHERE m.testerId=u.id AND m.reRunId=mr.id AND  m.testerId IN (:testerId) AND mr.runId IN (:reRunId) AND m.testCaseAction=:action "
				+ whereCond + " \r\n" + " GROUP BY m.testerId,m.STATUS)tmp\r\n" + "  GROUP BY tmp.name";
		// 21/11/2020
		/*
		 * "SELECT tmp.name, sum(tmp.total) as exeCount, ROUND(sum(tmp.executionTime/60),2), sum(tmp.pass) AS pass, sum(tmp.fail) as fail from\r\n"
		 * +
		 * " (SELECT u.name ,COUNT(*) AS total,sum(m.executionTime) AS executionTime, if(m.STATUS='Pass',count(m.STATUS),0) AS pass, if(m.STATUS='Fail',count(m.STATUS),0) AS fail\r\n"
		 * +
		 * " FROM MannualTestCaseExecutionLogs m,UserDetailsEntity u,MannualReRunTable mr\r\n"
		 * +
		 * " WHERE m.testerId=u.id AND m.reRunId=mr.id AND  m.testerId IN (:testerId) AND mr.runId IN (:reRunId) AND m.testCaseAction=:action\r\n"
		 * + whereCond+ " GROUP BY m.testerId,m.STATUS)tmp GROUP BY tmp.name";
		 */

		System.out.println(tableData);
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(tableData);
			query.setParameter("action", "Completed");
			query.setParameterList("testerId", requestBo.getTesterList());
			query.setParameterList("reRunId", requestBo.getRunList());
			query.setParameter("pID", requestBo.getProjectid());
			list = query.list();

			for (Object[] objects : list) {
				try {
					ResourceResponseBo responseBo = new ResourceResponseBo();
					responseBo.setTesterName((String) objects[0]);
					responseBo.setExeCount(objects[1] != null ? objects[1].toString() : null);

					if (objects[2] != null) {
						Double test2 = (Double) objects[2];
						long testqq = (new Double(test2)).longValue();
						long sec = testqq % 60;
						long minutes = testqq % 3600 / 60;
						long hoursss = testqq % 28800 / 3600;
						long daysss = testqq / 28800;
						responseBo.setExeTime(daysss + "-Day(s) " + hoursss + "-Hour(s) " + minutes + "-Minute(s)");
					} else {
						responseBo.setExeTime(0 + "-Day(s) " + 0 + "-Hour(s) " + 0 + "-Minute(s)");
					}

					responseBo.setExePass(objects[3] != null ? (objects[3]).toString() : null);
					responseBo.setExeFail(objects[4] != null ? (objects[4]).toString() : null);
					responseBo.setPrepCount(objects[5] != null ? (objects[5]).toString() : null);

					if (objects[6] != null) {
						Double test2 = ((BigDecimal) objects[6]).doubleValue();
						long testqq = (new Double(test2)).longValue();
						long sec = testqq % 60;
						long minutes = testqq % 3600 / 60;
						long hoursss = testqq % 28800 / 3600;
						long daysss = testqq / 28800;
						responseBo.setPrepTime(daysss + "-Day(s) " + hoursss + "-Hour(s) " + minutes + "-Minute(s)");
					} else {
						responseBo.setPrepTime(0 + "-Day(s) " + 0 + "-Hour(s) " + 0 + "-Minute(s)");
					}

					effortsList.add(responseBo);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return effortsList;
	}

	public Map<String, MannualRunDetailsData> getTestGroupAndCasesByRunAssignstatus(Integer reRunId, String status) {
		List<Object[]> list = null;
		List<MannualTestCaseExecutionLogs> executionLogs = new ArrayList<>();
		Map<String, MannualRunDetailsData> maps = new HashedMap();
		String overViewProjectHQL = "SELECT mg.groupName,mt.id,mt.projectId,mt.reRunId,mt.testSuitesId,mt.testGroupId,mt.SRID,mt.srNo,mt.module,mt.subModule\r\n"
				+ "	,mt.scenarioID,mt.scenarios,mt.functionality,mt.testID,mt.testCaseType,mt.testDescription,mt.dateOfExec,mt.browser,mt.testData,mt.steps,mt.expectedResult,mt.actualResult,mt.STATUS,mt.executionTime,mt.testerComment\r\n"
				+ "	,mt.testerId,mt.isActive,mt.issueIntegrationId,mt.bugSeverity,mt.bugPriority,mt.bugId,mt.testCaseAction\r\n"
				+ "	,mt.createdTimeStamp,mt.updatedTimeStamp,u.NAME AS assignto\r\n"
				+ "	,mt.absoluteSrID FROM MannualTestCaseExecutionLogs mt\r\n"
				+ "	LEFT JOIN Mannual_TestGroup_Table mg ON mg.testGroupId =mt.testGroupId \r\n"
				+ "	LEFT JOIN UserDetailsEntity u ON u.id=mt.testerId\r\n"
				+ "	WHERE mt.reRunId=:reRunId and mt.absoluteSrID IS NOT NULL " + "order by mt.srID ASC";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("reRunId", reRunId);
			list = query.list();
			for (Object[] objects : list) {
				MannualTestCaseExecutionLogs logs = new MannualTestCaseExecutionLogs();
				logs.setGroupName((String) objects[0]);
				logs.setId((Integer) objects[1]);
				logs.setProjectId((Integer) objects[2]);
				logs.setReRunId((Integer) objects[3]);
				logs.setTestGroupId((Integer) objects[5]);
				logs.setTestCaseSrId((Integer) objects[6]);
				logs.setTestCaseSrNo((Integer) objects[7]);
				logs.setScenarioID((String) objects[10]);
				logs.setScenarios((String) objects[11]);
				logs.setFunctionality((String) objects[12]);
				logs.setTestCaseType((String) objects[14]);
				logs.setTestDescription((String) objects[15]);
				logs.setDateOfExec((Date) objects[16]);
				logs.setBrowser((String) objects[17]);
				logs.setSteps((String) objects[19]);
				logs.setTestData((String) objects[18]);
				logs.setExpectedResult((String) objects[20]);
				logs.setActualResult((String) objects[21]);
				logs.setStatus((String) objects[22]);
				logs.setTesterComment((String) objects[24]);
				logs.setTesterName((String) objects[34]);
				if ((String) objects[34] != null) {
					logs.setIsassign("1");
				} else {
					logs.setIsassign("0");
				}
				logs.setTestCaseAction((String) objects[31]);
				logs.setTesterId((String) objects[25]);
				logs.setTestID((String) objects[13]);
				logs.setAbsoluteTCID((Integer) objects[35]);
				if (status.equals(logs.getIsassign())) {
					executionLogs.add(logs);
				} else if (status.equals("")) {
					executionLogs.add(logs);
				}

			}
			for (MannualTestCaseExecutionLogs details1 : executionLogs) {
				if (details1 != null) {
					MannualRunDetailsData mannualRunDetails = new MannualRunDetailsData();
					List<MannualTestCaseExecutionLogs> values = executionLogs.stream()
							.filter(x -> x.getGroupName().equals(details1.getGroupName())).collect(Collectors.toList());
					mannualRunDetails.setGroupId(details1.getTestGroupId().toString());
					if (mannualRunDetails.getGroupId() != null) {
						if (values != null) {
							mannualRunDetails.setTestCaseExecutionLogs(values);
							mannualRunDetails.setTestCaseCount(values.size());
						}
					}
					maps.put(details1.getGroupName(), mannualRunDetails);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return maps;
	}

	public List<Object[]> testrunwisedetailsOverview(Integer projectid) {
		List<Object[]> list = null;
		String overViewProjectHQL="SELECT count(IF(mt.testCaseAction != 'null', 1, NULL)) as totalcases,mr.runId,mr.runName,\r\n" + 
				"						count(IF(mt.status = 'Fail', 1, NULL) && IF(mt.testCaseAction = 'Completed', 1, NULL)) as Total_Fail,\r\n" + 
				"						count(IF(mt.status = 'Pass', 1, NULL) && IF(mt.testCaseAction = 'Completed', 1, NULL)) as Total_Pass,\r\n" + 
				"						count(IF(mt.status is null, 1, NULL)) as Total_Unexecuted,\r\n" + 
				"						count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,\r\n" + 
				"						count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',\r\n" + 
				"                        count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',\r\n" + 
				"                        count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started',\r\n" + 
				"						count(IF(mt.testCaseAction = 'N/A', 1, NULL)) as 'N/A',\r\n" + 
				"                        count(IF(mt.bugSeverity = 'major' AND mt.status = 'Fail', 1, NULL)) as Major,\r\n" + 
				"						count(IF(mt.bugSeverity = 'minor' AND mt.status = 'Fail', 1, NULL)) as Minor,\r\n" + 
				"						count(IF(mt.bugSeverity = 'critical' AND mt.status = 'Fail', 1, NULL)) as Critical,\r\n" + 
				"						count(IF(mt.bugSeverity = 'blocker' AND mt.status = 'Fail', 1, NULL)) as blocker,sum(mt.executionTime) as Total_secs,mr.round,\r\n" + 
				"						count(IF(mt.bugstatus = 'Open' AND mt.testCaseAction = 'Completed', 1, NULL)) as Open,\r\n" + 
				"						count(IF(mt.bugstatus = 'Closed' AND mt.testCaseAction = 'Completed', 1, NULL)) as Closed,\r\n" + 
				"						count(IF(mt.bugstatus = 'Reopen' AND mt.testCaseAction = 'Completed', 1, NULL)) as Reopen\r\n" + 
				"						, count(IF(mt.bugstatus = 'Resolved' AND mt.testCaseAction = 'Completed', 1, NULL)) as Resolved, mr.status,mr.id,\r\n" + 
				"							count(IF(mt.status = 'Blocked-Showstopper', 1, NULL)) as Total_Blocked_Showstopper,\r\n" + 
				"								count(IF(mt.status = 'Blocked-Undelivered', 1, NULL) ) as Total_Blocked_Undelivered,\r\n" + 
				"								count(IF(mt.status = 'NA', 1, NULL) ) as Total_NA,\r\n" + 
				"								count(IF(mt.status = 'Blocked-Testdata', 1, NULL) ) as Total_Blocked_Test_data,\r\n" + 
				"								count(IF(mt.complexity = 'Must to execute', 1, NULL) ) as Total_MTE,\r\n" + 
				"								count(IF(mt.complexity = 'Need to execute', 1, NULL) ) as Total_NTE,\r\n" + 
				"								count(IF(mt.complexity = 'Nice to execute', 1, NULL) ) as Total_NITE,\r\n" + 
				"								count(IF(mt.priority = 'High', 1, NULL) ) as Total_high,\r\n" + 
				"								count(IF(mt.priority = 'Medium', 1, NULL) ) as Total_Medium,\r\n" + 
				"								count(IF(mt.priority = 'Low', 1, NULL) ) as Total_low,\r\n" + 
				"								count(IF(mt.status = 'Blocked-FSD not clear', 1, NULL) ) as Total_BlockedFSDnotclear,\r\n" + 
				"								count(IF(mt.status = 'NE', 1, NULL) ) as Total_NE,\r\n" + 
				"								count(IF(mt.status = 'Deferred', 1, NULL) ) as Total_Deferred,\r\n" + 
				"								count(IF(mt.status = 'On Hold', 1, NULL) ) as Hold,\r\n" + 
				"								count(IF(mt.status = 'Duplicate', 1, NULL) ) as Total_Duplicate,\r\n" + 
				"								count(IF(mt.status = 'Out of Scope', 1, NULL) ) as Total_OutofScope,\r\n" + 
				"								count(IF(mt.status = 'Partially Pass', 1, NULL) ) as Total_PartiallyPass,\r\n" + 
				"						count(IF(mt.testCaseAction = 'Incomplete', 1, NULL)) as 'total_action_incomplete'\r\n" + 
				" FROM 					MannualReRunTable mr\r\n" + 

				"						LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId = mr.id where mr.projectId=:projectid and mr.isActive='Y'\r\n" + 
				"						 group by  mr.id order by mr.id desc\r\n" + 
				"\r\n" + 
				"";
		/*"SELECT count(IF(mt.testCaseAction != 'null', 1, NULL)) as totalcases,mr.runId,mr.runName,\r\n"
				+ "						count(IF(mt.status = 'Fail', 1, NULL) && IF(mt.testCaseAction = 'Completed', 1, NULL)) as Total_Fail,\r\n"
				+ "						count(IF(mt.status = 'Pass', 1, NULL) && IF(mt.testCaseAction = 'Completed', 1, NULL)) as Total_Pass,\r\n"
				+ "						count(IF(mt.status is null, 1, NULL)) as Total_Unexecuted,\r\n"
				+ "						count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,\r\n"
				+ "						count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',\r\n"
				+ "                        count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',\r\n"
				+ "                        count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started',\r\n"
				+ "						count(IF(mt.testCaseAction = 'N/A', 1, NULL)) as 'N/A',\r\n"
				+ "                        count(IF(mt.bugSeverity = 'major' AND mt.status = 'Fail', 1, NULL)) as Major,\r\n"
				+ "						count(IF(mt.bugSeverity = 'minor' AND mt.status = 'Fail', 1, NULL)) as Minor,\r\n"
				+ "						count(IF(mt.bugSeverity = 'critical' AND mt.status = 'Fail', 1, NULL)) as Critical,\r\n"
				+ "						count(IF(mt.bugSeverity = 'blocker' AND mt.status = 'Fail', 1, NULL)) as blocker,sum(mt.executionTime) as Total_secs,mr.round,\r\n"
				+ "						count(IF(mt.bugstatus = 'Open' AND mt.testCaseAction = 'Completed', 1, NULL)) as Open,\r\n"
				+ "						count(IF(mt.bugstatus = 'Closed' AND mt.testCaseAction = 'Completed', 1, NULL)) as Closed,\r\n"
				+ "						count(IF(mt.bugstatus = 'Reopen' AND mt.testCaseAction = 'Completed', 1, NULL)) as Reopen\r\n"
				+ "						, count(IF(mt.bugstatus = 'Resolved' AND mt.testCaseAction = 'Completed', 1, NULL)) as Resolved, mr.status,mr.id,\r\n"
				+ "							count(IF(mt.status = 'Blocked-Showstopper', 1, NULL)) as Total_Blocked_Showstopper,\r\n"
				+ "								count(IF(mt.status = 'Blocked-Undelivered', 1, NULL) ) as Total_Blocked_Undelivered,\r\n"
				+ "								count(IF(mt.status = 'NA', 1, NULL) ) as Total_NA,\r\n"
				+ "								count(IF(mt.status = 'Blocked-Testdata', 1, NULL) ) as Total_Blocked_Test_data,\r\n"
				+ "								count(IF(mt.complexity = 'Must to execute', 1, NULL) ) as Total_MTE,\r\n"
				+ "								count(IF(mt.complexity = 'Need to execute', 1, NULL) ) as Total_NTE,\r\n"
				+ "								count(IF(mt.complexity = 'Nice to execute', 1, NULL) ) as Total_NITE,\r\n"
				+ "								count(IF(mt.priority = 'High', 1, NULL) ) as Total_high,\r\n"
				+ "								count(IF(mt.priority = 'Medium', 1, NULL) ) as Total_Medium,\r\n"
				+ "								count(IF(mt.priority = 'Low', 1, NULL) ) as Total_low\r\n"
				+ "									 FROM MannualReRunTable mr\r\n"
				+ "						LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mr.id where mr.projectId=:projectid and mr.isActive='Y'\r\n"
				+ "						 group by  mr.id order by mr.id desc\r\n" + "";*/

		/* "SELECT count(IF(mt.testCaseAction != '', 1, NULL)) as totalcases,mr.runId,mr.runName,\r\n"
		 * +
		 * "						count(IF(mt.status = 'Fail', 1, NULL) && IF(mt.testCaseAction = 'Completed', 1, NULL)) as Total_Fail,\r\n"
		 * +
		 * "						count(IF(mt.status = 'Pass', 1, NULL) && IF(mt.testCaseAction = 'Completed', 1, NULL)) as Total_Pass,\r\n"
		 * +
		 * "						count(IF(mt.status is null, 1, NULL)) as Total_Unexecuted,\r\n"
		 * +
		 * "						count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,\r\n"
		 * +
		 * "						count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',\r\n"
		 * +
		 * "                        count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',\r\n"
		 * +
		 * "                        count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started',\r\n"
		 * +
		 * "						count(IF(mt.testCaseAction = 'N/A', 1, NULL)) as 'N/A',\r\n"
		 * +
		 * "                        count(IF(mt.bugSeverity = 'major' AND mt.status = 'Fail', 1, NULL)) as Major,\r\n"
		 * +
		 * "						count(IF(mt.bugSeverity = 'minor' AND mt.status = 'Fail', 1, NULL)) as Minor,\r\n"
		 * +
		 * "						count(IF(mt.bugSeverity = 'critical' AND mt.status = 'Fail', 1, NULL)) as Critical,\r\n"
		 * +
		 * "						count(IF(mt.bugSeverity = 'blocker' AND mt.status = 'Fail', 1, NULL)) as blocker,sum(mt.executionTime) as Total_secs,mr.round,\r\n"
		 * +
		 * "						count(IF(mt.bugstatus = 'Open' AND mt.testCaseAction = 'Completed', 1, NULL)) as Open,\r\n"
		 * +
		 * "						count(IF(mt.bugstatus = 'Closed' AND mt.testCaseAction = 'Completed', 1, NULL)) as Closed,\r\n"
		 * +
		 * "						count(IF(mt.bugstatus = 'Reopen' AND mt.testCaseAction = 'Completed', 1, NULL)) as Reopen\r\n"
		 * +
		 * "						, count(IF(mt.bugstatus = 'Resolved' AND mt.testCaseAction = 'Completed', 1, NULL)) as Resolved, mr.status,mr.id FROM MannualReRunTable mr\r\n"
		 * +
		 * "						LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mr.id where mr.projectId=:projectid and mr.isActive='Y'\r\n"
		 * + "						 group by  mr.id order by mr.id desc\r\n" + "\r\n" +
		 * "	";
		 */

		/*
		 * " SELECT count(IF(mt.testCaseAction != '', 1, NULL)) as totalcases,mr.runId,\r\n"
		 * + "mr.runName,\r\n" +
		 * "count(IF(mt.status = 'Fail', 1, NULL) && IF(mt.testCaseAction = 'Completed', 1, NULL)) as Total_Fail,\r\n"
		 * +
		 * "count(IF(mt.status = 'Pass', 1, NULL) && IF(mt.testCaseAction = 'Completed', 1, NULL)) as Total_Pass,\r\n"
		 * + "count(IF(mt.status is null, 1, NULL)) as Total_Unexecuted,\r\n" +
		 * "count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,\r\n" +
		 * "count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',\r\n" +
		 * "count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',\r\n"
		 * +
		 * "count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started',\r\n"
		 * + "count(IF(mt.bugSeverity = 'major', 1, NULL)) as Major,\r\n" +
		 * "count(IF(mt.bugSeverity = 'minor', 1, NULL)) as Minor,\r\n" +
		 * "count(IF(mt.bugSeverity = 'critical', 1, NULL)) as Critical,\r\n" +
		 * "count(IF(mt.bugSeverity = 'blocker', 1, NULL)) as blocker,sum(mt.executionTime) as Total_secs,mr.round,"
		 * + "count(IF(mt.bugstatus = 'Open', 1, NULL)) as Open," +
		 * "count(IF(mt.bugstatus = 'Closed', 1, NULL)) as Closed," +
		 * "count(IF(mt.bugstatus = 'Reopen', 1, NULL)) as Reopen,mr.status,mr.id\r\n" +
		 * "FROM MannualReRunTable mr\r\n" +
		 * "LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mr.id where mr.projectId=:projectid and mr.isActive='Y'\r\n"
		 * + " group by  mr.id order by mr.id desc";
		 */
		Session session=entityManager.unwrap(Session.class);
		session.setCacheMode(CacheMode.IGNORE);
		session.flush();
		session.clear();
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectid", projectid);
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}