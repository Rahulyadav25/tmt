package com.apmosys.tmt.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.ProjectDetailsAuto;
import com.apmosys.tmt.models.TestSuiteDefectTracker;
import com.apmosys.tmt.models.TestSuitesData;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
@Repository
@Transactional
public class ProjectOverviewAutoDao {
	@PersistenceContext
    private EntityManager entityManager;
	@Autowired
	SessionFactory sessionFactory;
	public List<ProjectDetailsAuto> getoverviewdetails(int projectid) {
		System.out.println("in poa_dao");
		List<Object[]> list =null;
		String delay="";
		List<ProjectDetailsAuto> projDetails = new ArrayList<>();
		String overViewProjectHQL = 
				" SELECT p.projectID,p.projectName,\r\n" + 
						" (Select COUNT(l.status) FROM logs l\r\n" + 
						" WHERE l.project_name= p.projectName)AS 'log count',\r\n" + 
						" (Select COUNT(l.status) FROM logs l\r\n" + 
						" WHERE l.project_name= p.projectName\r\n" + 
						" AND l.status='PASS')AS 'PASS',\r\n" + 
						"(Select COUNT(l.status) FROM logs l\r\n" + 
						" WHERE l.project_name= p.projectName\r\n" + 
						" AND l.status='FAIL')AS 'FAIL',\r\n" + 
						"(SELECT COUNT(r.status) FROM Run_history r\r\n" + 
						"WHERE r.project_name = p.projectName) AS 'total fresh run',\r\n" + 
						"(SELECT COUNT(r.status) FROM Run_history r\r\n" + 
						"WHERE r.project_name = p.projectName AND r.status='Success') AS 'completed run',\r\n" + 
						"(SELECT COUNT(r.status) FROM Run_history r\r\n" + 
						"WHERE r.project_name = p.projectName AND r.status='Fail') AS 'failed run',\r\n" + 
						"(SELECT COUNT(r.status) FROM Run_history r\r\n" + 
						"WHERE r.project_name = p.projectName AND r.status='Stopped') AS 'stopped run',\r\n"+
						"(CASE WHEN a.NoOfDatasheet IS NULL THEN '0' ELSE a.NoOfDatasheet end) AS 'NoOfDatasheet', \r\n" + 
						"(CASE WHEN a.NoOftestSuites IS NULL THEN '0' ELSE a.NoOftestSuites end) AS 'NoOftestSuites',\r\n" + 
						"(CASE WHEN a.NoOfTestcases IS NULL THEN '0' ELSE a.NoOfTestcases end) AS 'NoOfTestcases',\r\n"
						+ "(SELECT COUNT(r.status) FROM Run_history r\r\n" + 
						"WHERE r.project_name = p.projectName AND r.status='RUNNING') AS 'running',"
						+ "(SELECT  COUNT(l.TestCaseID) from logs l LEFT JOIN Run_history r\r\n" + 
						"ON l.RUN_ID = r.RUN_ID\r\n" + 
						"WHERE p.projectName = l.project_name) AS 'totalTestCase'" + 
						"FROM Project_MetaTable p  LEFT join AutomationProjectMeta a ON p.projectID = a.Projectid \r\n" + 
						"WHERE p.isAutomation='TRUE'\r\n" +
						"AND p.projectID = :projectid\r\n"+
						" GROUP BY p.projectName";
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectid", projectid);
			list = query.list();
			for (Object[] objects : list) {
				ProjectDetailsAuto test=new ProjectDetailsAuto();
	
				
				test.setProjectid(objects[0].toString());
				test.setProjectname(objects[1].toString());
				test.setTotalLogCount(Integer.valueOf(objects[2].toString()));
				test.setPassedLog(Integer.valueOf(objects[3].toString()));
				test.setFailedLog(Integer.valueOf(objects[4].toString()));
				test.setFreshRun(Integer.valueOf(objects[5].toString()));
				test.setCompletedRun(Integer.valueOf(objects[6].toString()));
				test.setFailedRun(Integer.valueOf(objects[7].toString()));
				test.setStoppedRun(Integer.valueOf(objects[8].toString()));
				test.setNoOfDatasheet(Integer.valueOf(objects[9].toString()));
				test.setNoOftestSuites(Integer.valueOf(objects[10].toString()));
				test.setNoOfTestcases(Integer.valueOf(objects[11].toString()));
				test.setRunning(Integer.valueOf(objects[12].toString()));
				test.setTotalTestCases(Integer.valueOf(objects[13].toString()));
				test.setProjTestSuitesList(getallActiveRun(Integer.valueOf(objects[0].toString())));
				projDetails.add(test);
				System.out.println("data of automation overview  "+projDetails.toString());
				
				
			 }
		}catch (Exception e) {
			e.printStackTrace();
		}
		return projDetails;
	}
	public List<TestSuitesData> getLogDetails(int projectid,String fromDate, String toDate) {
		System.out.println("in poa_dao");
		List<Object[]> list =null;
		String delay="";
		List<TestSuitesData> projDetails = new ArrayList<>();
		
		
		String overViewProjectHQL = null;
		if( fromDate.equals("false")||toDate.equals("false")) {
		overViewProjectHQL= "SELECT p.projectID,p.projectName,l.ScenarioID,COUNT(l.ScenarioID)AS total,\r\n" + 
				"count(IF(l.status = 'Pass', 1, NULL) ) as  passed,\r\n" + 
				"COUNT(IF(l.`status`='Fail',1,NULL) ) AS failed,l.RUN_ID,count(l.TestCaseID)as'testCase'\r\n" + 
				"FROM Project_MetaTable p  LEFT JOIN  logs l\r\n" + 
				"ON p.projectName =l.project_name  \r\n" + 
				"WHERE p.projectID = :projectid\r\n" +
				"GROUP BY l.RUN_ID ORDER BY l.RUN_ID desc";
		}
		if(fromDate!=null&&
				toDate!=null || fromDate!=""&&
				toDate!="") {
			fromDate = getFormattedDate(fromDate);
			toDate = getFormattedDate(toDate);
			overViewProjectHQL= "SELECT p.projectID,p.projectName,l.ScenarioID,COUNT(l.ScenarioID)AS total,\r\n" + 
					"count(IF(l.status = 'Pass', 1, NULL) ) as  passed,\r\n" + 
					"COUNT(IF(l.`status`='Fail',1,NULL) ) AS failed,l.RUN_ID,count(l.TestCaseID)as'testCase'\r\n" + 
					"FROM logs l   LEFT JOIN  Project_MetaTable p\r\n" + 
					"ON p.projectName =l.project_name   JOIN Run_history r\r\n" + 
					"ON  l.RUN_ID =r.RUN_ID \r\n" + 
					"WHERE p.isAutomation='TRUE' \r\n" + 
					"AND p.projectID = :projectid\r\n" + 
					"AND   STR_TO_DATE(r.start_time,'%d-%m-%Y') >='"+fromDate+"'\r\n" + 
					"AND   STR_TO_DATE(r.end_time,'%d-%m-%Y') <='"+toDate+"'\r\n" + 
					"GROUP BY l.RUN_ID  ORDER BY l.RUN_ID desc;";
		}
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectid", projectid);
			list = query.list();
			for (Object[] objects : list) {
				TestSuitesData test=new TestSuitesData();
	
				
				test.setProjectid(objects[0].toString());
				test.setProjectname(objects[1].toString());
				test.setScenarioID(objects[2].toString());
				test.setTotalTestSuites(Integer.valueOf(objects[3].toString()));
				test.setTotalPassed(Integer.valueOf(objects[4].toString()));
				test.setTotalFailed(Integer.valueOf(objects[5].toString()));
				if(objects[6]!=null)
				{test.setRunId(Integer.valueOf(objects[6].toString()));
				test.setDefectTrackers(getTrackerList(objects[0].toString(), objects[6].toString()));}
				if(objects[7]!=null)
				{
					test.setTotalTestCases(Integer.valueOf(objects[7].toString()));
					
				}
				
	
				projDetails.add(test);
				System.out.println("data of automation overview  "+projDetails.toString());
			 }
		}catch (Exception e) {
			e.printStackTrace();
		}
		return projDetails;
	}
	public List<TestSuitesData> getSummaryDetails(int projectid) {
		System.out.println("in poa_dao");
		List<Object[]> list =null;
		String delay="";
		List<TestSuitesData> projDetails = new ArrayList<>();
		
		
		String overViewProjectHQL = null;
		
		overViewProjectHQL= "SELECT p.projectID,p.projectName,l.ScenarioID,COUNT(l.ScenarioID)AS total,\r\n" + 
				"count(IF(l.status = 'Pass', 1, NULL) ) as  passed,\r\n" + 
				"COUNT(IF(l.`status`='Fail',1,NULL) ) AS failed,l.RUN_ID,count(l.TestCaseID)as'testCase'\r\n" + 
				"FROM Project_MetaTable p  LEFT JOIN  logs l\r\n" + 
				"ON p.projectName =l.project_name  \r\n" + 
				"WHERE p.projectID = :projectid\r\n" +
				"GROUP BY l.RUN_ID ORDER BY l.RUN_ID desc";
		
		
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectid", projectid);
			list = query.list();
			for (Object[] objects : list) {
				TestSuitesData test=new TestSuitesData();
				if(objects[2] == null) {
					Integer i =new Integer(0);
					objects[2]=i;
					
				}
				
				test.setProjectid(objects[0].toString());
				test.setProjectname(objects[1].toString());
				test.setScenarioID(objects[2].toString());
				test.setTotalTestSuites(Integer.valueOf(objects[3].toString()));
				test.setTotalPassed(Integer.valueOf(objects[4].toString()));
				test.setTotalFailed(Integer.valueOf(objects[5].toString()));
				if(objects[6]!=null)
				{test.setRunId(Integer.valueOf(objects[6].toString()));
				test.setDefectTrackers(getTrackerList(objects[0].toString(), objects[6].toString()));}
				if(objects[7]!=null)
				{
					test.setTotalTestCases(Integer.valueOf(objects[7].toString()));
					
				}
				
	
				projDetails.add(test);
				System.out.println("data of automation overview  "+projDetails.toString());
			 }
		}catch (Exception e) {
			e.printStackTrace();
		}
		return projDetails;
	}
	
	
	public List<TestSuitesData> getallActiveRun(int projectid) {
		System.out.println("in poa_dao");
		List<Object[]> list =null;
		String delay="";
		List<TestSuitesData> projDetails = new ArrayList<>();
		
		
		String 
		overViewProjectHQL= "SELECT p.projectID,p.projectName,l.ScenarioID,COUNT(l.ScenarioID)AS total,\r\n" + 
				"count(IF(l.status = 'Pass', 1, NULL) ) as  passed,\r\n" + 
				"COUNT(IF(l.`status`='Fail',1,NULL) ) AS failed,l.RUN_ID,count(DISTINCT l.TestCaseID)as'testCase'\r\n" + 
				"FROM Project_MetaTable p  LEFT JOIN  logs l\r\n" + 
				"ON p.projectName =l.project_name  \r\n" + 
				"WHERE p.isAutomation='TRUE'\r\n" + 
				"AND p.projectID = :projectid\r\n" +
				"GROUP BY l.RUN_ID ORDER BY l.RUN_ID desc";
		
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectid", projectid);
			list = query.list();
			for (Object[] objects : list) {
				TestSuitesData test=new TestSuitesData();
	
				
				test.setProjectid(objects[0].toString());
				test.setProjectname(objects[1].toString());
				//test.setScenarioID(objects[2].toString());
				test.setScenarioID(objects[2]!=null?objects[2].toString():null);
				test.setTotalTestSuites(Integer.valueOf(objects[3].toString()));
				test.setTotalPassed(Integer.valueOf(objects[4].toString()));
				test.setTotalFailed(Integer.valueOf(objects[5].toString()));
				if(objects[6]!=null)
				{test.setRunId(Integer.valueOf(objects[6].toString()));
				test.setDefectTrackers(getTrackerList(objects[0].toString(), objects[6].toString()));}
				if(objects[7]!=null)
				{
					test.setTotalTestCases(Integer.valueOf(objects[7].toString()));
					
				}
				
	
				projDetails.add(test);
				System.out.println("data of automation overview  "+projDetails.toString());
			 }
		}catch (Exception e) {
			e.printStackTrace();
		}
		return projDetails;
	}
	
	private String getFormattedDate(String date) {
		
	String sDate="";
		try {
			
			Date newDate = new SimpleDateFormat("MM/dd/yyyy").parse(date);
			sDate = new SimpleDateFormat("yyyy-MM-dd").format(newDate);
		}catch(Exception e) {
			
		}
		return sDate;
	}
	public List<TestSuitesData> runwiseAutodetails(int projectid,String runlist) {
		System.out.println("in poa_dao");
		List<Object[]> list =null;
		String delay="";
		List<TestSuitesData> projDetails = new ArrayList<>();
		
		
		String  
			overViewProjectHQL= " SELECT p.projectID,p.projectName,l.ScenarioID,COUNT(l.ScenarioID)AS total,\r\n" + 
					"count(IF(l.status = 'Pass', 1, NULL) ) as  passed,\r\n" + 
					"COUNT(IF(l.`status`='Fail',1,NULL) ) AS failed,l.RUN_ID,count(l.TestCaseID)as'testCase'\r\n" + 
					"FROM logs l   LEFT JOIN  Project_MetaTable p\r\n" + 
					"ON p.projectName =l.project_name   \r\n" + 
					"WHERE p.isAutomation='TRUE' \r\n" + 
					"AND p.projectID =:projectid\r\n" + 
					"AND  l.RUN_ID IN ("+runlist+")\r\n" + 
					"GROUP BY l.RUN_ID  ORDER BY l.RUN_ID desc";
		
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectid", projectid);
			list = query.list();
			for (Object[] objects : list) {
				TestSuitesData test=new TestSuitesData();
	
				
				test.setProjectid(objects[0].toString());
				test.setProjectname(objects[1].toString());
				test.setScenarioID(objects[2].toString());
				test.setTotalTestSuites(Integer.valueOf(objects[3].toString()));
				test.setTotalPassed(Integer.valueOf(objects[4].toString()));
				test.setTotalFailed(Integer.valueOf(objects[5].toString()));
				if(objects[6]!=null)
				{test.setRunId(Integer.valueOf(objects[6].toString()));
				test.setDefectTrackers(getTrackerList(objects[0].toString(), objects[6].toString()));}
				if(objects[7]!=null)
				{
					test.setTotalTestCases(Integer.valueOf(objects[7].toString()));
				}
				
	
				projDetails.add(test);
				System.out.println("data of automation overview  "+projDetails.toString());
			 }
		}catch (Exception e) {
			e.printStackTrace();
		}
		return projDetails;
	}
	
	public TestSuiteDefectTracker getTrackerList(String projectid,String runId) {
		
		System.out.println("in poa_dao");
		List<Object[]> list =null;
		String delay="";
		TestSuiteDefectTracker projDetails = new TestSuiteDefectTracker();
		
		
		String  
			overViewProjectHQL= "SELECT projectid, runid, testcaseid, defectid, Severity, STATUS,\r\n" + 
					"count(IF(Status = 'Open', 1, NULL) ) AS  'Open',\r\n" + 
					"count(IF(Status = 'Reopen', 1, NULL) ) AS  'Reopen',\r\n" + 
					"count(IF(Status = 'Closed', 1, NULL) ) AS  'Closed',\r\n" + 
					"count(IF(Status = 'Blocker', 1, NULL) ) AS  'Blocker',\r\n" + 
					"count(IF(Severity = 'Minor', 1, NULL) ) AS  'Minor',\r\n" + 
					"count(IF(Severity = 'Major', 1, NULL) ) AS  'Major',\r\n" + 
					"count(IF(Severity = 'Critical', 1, NULL) ) AS  'Critical'\r\n" + 
					"FROM DefectTrackerData\r\n" + 
					"WHERE projectid =:projectid\r\n" + 
					"AND runid = '"+runId+"'";
		
		Session session=entityManager.unwrap(Session.class);
		try {
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectid", projectid);
			list = query.list();
			for (Object[] objects : list) {
				TestSuiteDefectTracker test=new TestSuiteDefectTracker();
	
				if(objects[0]!=null)
				{test.setProjectId(objects[0].toString());}
				if(objects[1]!=null)
				{test.setRunId(objects[1].toString());}
				if(objects[2]!=null) {
				test.setTestcaseId(objects[2].toString());}
				if(objects[3]!=null) {
				test.setDefectId(objects[3].toString());}
				if(objects[4]!=null) {
				test.setSeverity(objects[4].toString());}
				if(objects[5]!=null) {
				test.setStatus(objects[5].toString());}
				
				if(objects[6]!=null) {
				test.setTotalOpen(Integer.valueOf(objects[6].toString()));}
				if(objects[7]!=null) {
				test.setTotalReOpen(Integer.valueOf(objects[7].toString()));}
				if(objects[8]!=null) {
				test.setTotalClosed(Integer.valueOf(objects[8].toString()));}
				if(objects[9]!=null) {
				test.setTotalBlocker(Integer.valueOf(objects[9].toString()));}
				if(objects[10]!=null) {
				test.setTotalMinor(Integer.valueOf(objects[10].toString()));}
				if(objects[11]!=null) {
				test.setTotalMajor(Integer.valueOf(objects[11].toString()));}
				if(objects[12]!=null) {
				test.setTotalCritical(Integer.valueOf(objects[12].toString()));}
				
				
	
				projDetails=test;
				System.out.println("data of automation overview  "+projDetails.toString());
			 }
		}catch (Exception e) {
			e.printStackTrace();
		}
		return projDetails;
	}
}