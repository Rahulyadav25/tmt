package com.apmosys.tmt.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.apmosys.tmt.models.AttachmentFetch;
import com.apmosys.tmt.utils.ServiceResponse;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

@Transactional
@Repository
public class AttachmentDao {
	@PersistenceContext
    private EntityManager entityManager;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	public ServiceResponse saveAttachment(String attachmentName, byte[] fileData, long fileSize, String uploadedBy, Integer testCaseID) 
	{
		Session session=entityManager.unwrap(Session.class);
		ServiceResponse serviceResponse=new ServiceResponse();
		String sqlQuery="insert into Attachment(testCaseID,attachment_name,size,byte_content,uploadedBy,uploadedDate"
				+ ") values (:testCaseID,:attachmentName,:fileSize,:fileData,:uploadedBy,now())";
		System.out.println(sqlQuery);
		Query query = session.createNativeQuery(sqlQuery);
		query.setParameter("testCaseID", testCaseID);
		query.setParameter("attachmentName", attachmentName);
		query.setParameter("fileSize", fileSize);
		query.setParameter("fileData", fileData);
		query.setParameter("uploadedBy", uploadedBy);
		System.out.println("queryExe:"+query.toString());
		int lastinsertedIdStatus=query.executeUpdate();
		if(lastinsertedIdStatus==1) {
			serviceResponse.setServiceResponse("Attachment Uploaded Successfully");
			}else {
				serviceResponse.setServiceError("Attachment Upload Failed");
			}
		return serviceResponse;
	}
	
	
	public List<AttachmentFetch> attchmentFetch(Integer testCaseID) 
	{
		List<AttachmentFetch> attachment=new ArrayList<>();
		List<Object[]> list = new ArrayList<>();
		Session session=entityManager.unwrap(Session.class);
		ServiceResponse serviceResponse=new ServiceResponse();
		String sqlQuery="select attachment_name,size,byte_content from Attachment where testCaseID=:testCaseID";
		System.out.println(sqlQuery);
		Query query = session.createNativeQuery(sqlQuery);
		try {
		query.setParameter("testCaseID", testCaseID);
		System.out.println("queryExe:"+query.toString());
		list = query.list();
	}
	catch(Exception e)
	{
		e.printStackTrace();;
	}
	for(Object[] object :list) 
	  {
		AttachmentFetch details=new AttachmentFetch();
		details.setAttachmentName(object[0].toString());
		details.setSize(object[1].toString());
		details.setAttachment((byte[]) object[2]);
		
		attachment.add(details);
	  }
	return attachment;
	}

	public List<AttachmentFetch> attchmentFetchdown(Integer testCaseID, String attachname) {
		List<AttachmentFetch> attachment=new ArrayList<>();
		List<Object[]> list = new ArrayList<>();
		Session session=entityManager.unwrap(Session.class);
		ServiceResponse serviceResponse=new ServiceResponse();
		String sqlQuery="select attachment_name,size,byte_content from Attachment where testCaseID=:testCaseID and attachment_name=:attachname";
		System.out.println(sqlQuery);
		Query query = session.createNativeQuery(sqlQuery);
		try {
		query.setParameter("testCaseID", testCaseID);
		query.setParameter("attachname", attachname);
		System.out.println("queryExe:"+query.toString());
		list = query.list();
	}
	catch(Exception e)
	{
		e.printStackTrace();;
	}
	for(Object[] object :list) 
	  {
		AttachmentFetch details=new AttachmentFetch();
		details.setAttachmentName(object[0].toString());
		details.setSize(object[1].toString());
		details.setAttachment((byte[]) object[2]);
		
		attachment.add(details);
	  }
	return attachment;
	}
	
	
	//added by sakti
	public List<AttachmentFetch> attchmentFetch1(Integer testCaseID,String bugId) 
	{
		List<AttachmentFetch> attachment=new ArrayList<>();
		List<Object[]> list = new ArrayList<>();
		Session session=entityManager.unwrap(Session.class);
		ServiceResponse serviceResponse=new ServiceResponse();
//		String sqlQuery="select attachment_name,size,byte_content from Attachment where testCaseID=:testCaseID";
		String sqlQuery="SELECT s.image_name,l.ScreenshotID,s.image_value FROM screenshots s inner join logs l on l.ScreenshotID=s.id where bug_id=:bugId"; 

		System.out.println(sqlQuery);
		Query query = session.createNativeQuery(sqlQuery);
		try {
//		query.setParameter("testCaseID", testCaseID);
		query.setParameter("bugId", bugId);
		System.out.println("queryExe:"+query.toString());
		list = query.list();
	}
	catch(Exception e)
	{
		e.printStackTrace();;
	}
	for(Object[] object :list) 
	  {
		AttachmentFetch details=new AttachmentFetch();
		details.setAttachmentName(object[0].toString());
		details.setSize(object[1].toString());
		details.setAttachment((byte[]) object[2]);
		
		attachment.add(details);
	  }
	return attachment;
	}

	public List<AttachmentFetch> deleteScreenshot(Integer testcaseID, String attachname) {

		List<AttachmentFetch> attachment=new ArrayList<>();
		List<Object[]> list = new ArrayList<>();
		Session session=entityManager.unwrap(Session.class);
		ServiceResponse serviceResponse=new ServiceResponse();
		String sqlQuery="delete from Attachment where testCaseID=:testCaseID and attachment_name=:attachname";
		System.out.println(sqlQuery);
		Query query = session.createNativeQuery(sqlQuery);
		try {
		query.setParameter("testCaseID", testcaseID);
		query.setParameter("attachname", attachname);
		System.out.println("queryExe:"+query.toString());
		query.executeUpdate();
		
		Session session1 = sessionFactory.getCurrentSession();
		String sqlQuery1="select attachment_name,size,byte_content from Attachment where testCaseID=:testCaseID";
		Query query1 = session.createNativeQuery(sqlQuery1);
		query1.setParameter("testCaseID", testcaseID);
	
		list = query1.list();
	}
	catch(Exception e)
	{
		e.printStackTrace();;
	}
	for(Object[] object :list) 
	  {
		AttachmentFetch details=new AttachmentFetch();
		details.setAttachmentName(object[0].toString());
		details.setSize(object[1].toString());
		details.setAttachment((byte[]) object[2]);
		
		attachment.add(details);
	  }
	return attachment;
	
	}

}
