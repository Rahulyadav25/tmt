package com.apmosys.tmt.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.map.HashedMap;
import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.BO.ResourceRequestBo;
import com.apmosys.tmt.BO.ResourceResponseBo;
import com.apmosys.tmt.models.ProjectDeatilsMeta;
import com.apmosys.tmt.models.ResouecesData;
import com.apmosys.tmt.models.userRole;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;


@Repository
@Transactional
public class ResourceDao {
	@PersistenceContext
    private EntityManager entityManager;
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	private DefectDao defectDao;

	public List<ProjectDeatilsMeta> getAllProjectName() {
		List<Object[]> list=null;
		List<ProjectDeatilsMeta> activeRunList = new ArrayList<ProjectDeatilsMeta>();
		String overViewProjectHQL = "SELECT p.projectID,p.projectName FROM Project_MetaTable p WHERE p.isActive=:isActive";
		Session session=entityManager.unwrap(Session.class);
		try {
		session.setCacheMode(CacheMode.IGNORE);
		session.flush();
		session.clear();
		Query query = session.createNativeQuery(overViewProjectHQL);
		//query.setParameter("projectId", pId);
		query.setParameter("isActive", "Y");
		//query.setParameter("status", "Running");
		list = query.list();
		for (Object[] objects : list) {
		ProjectDeatilsMeta pjInfo=new ProjectDeatilsMeta();
		pjInfo.setProjectID((Integer) objects[0]);
		pjInfo.setProjectName((String)objects[1]);

		activeRunList.add(pjInfo);

		}
		} catch (Exception e) {
		e.printStackTrace();
		}

		return activeRunList;

		}

	public List<userRole> getUserListOfResouce() {
		List<Object[]> list = null;
		List<userRole> userlist = new ArrayList<userRole>();
		String overViewProjectHQL = "SELECT ud.id, ud.name FROM UserDetailsEntity ud order by ud.name asc ";
		/*
		 * "SELECT ud.id, ud.name FROM UserDetailsEntity ud\r\n" +
		 * " left JOIN ProjectAccessTable pa  ON  ud.id =pa.userDetailsId \r\n" +
		 * " where pa.AccessType != 'Readonly' GROUP BY pa.userDetailsId";
		 */
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);

			list = query.list();
			for (Object[] objects : list) {
				userRole userData = new userRole();
				userData.setId((Integer) objects[0]);
				userData.setName((String) objects[1]);

				userlist.add(userData);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return userlist;

	}

	
	public List<userRole> getMangetCreatedUserList(Integer userid) {
		List<Object[]> list=null;
		List<userRole> userlist = new ArrayList<userRole>();
		String overViewProjectHQL = "SELECT ud.id, ud.name FROM UserDetailsEntity ud\r\n" + 
				"				WHERE ud.id=:userid \r\n" + 
				"				union\r\n" + 
				"SELECT ud.id, ud.name FROM UserDetailsEntity ud\r\n" + 
				"				where  ud.managerID =:userid";
		Session session=entityManager.unwrap(Session.class);
		try {
		session.setCacheMode(CacheMode.IGNORE);
		session.flush();
		session.clear();
		Query query = session.createNativeQuery(overViewProjectHQL);
		
		query.setParameter("userid", userid);
		list = query.list();
		for (Object[] objects : list) {
			userRole userData=new userRole();
			userData.setId((Integer) objects[0]);
			userData.setName((String) objects[1]);

			userlist.add(userData);

		}
		} catch (Exception e) {
		e.printStackTrace();
		}

		return userlist;

		}
	
	public List<Object[]> tableResourceEffortsAdmin(ResourceRequestBo requestBo) 
	{
		List<Object[]> list = null;
		List<ResourceResponseBo> effortsList = new ArrayList<>();
		String whereCond="\r\n";
		String whereBugCond="\r\n";
		String whereTCCond="\r\n";
		if(!(requestBo.getFromDate()==null && requestBo.getToDate()==null))
		{
			whereCond=whereCond + "AND DATE(m.updatedTimeStamp) BETWEEN '"+requestBo.getFromDate()+"' AND '"+requestBo.getToDate()+"'  \r\n";
			whereBugCond=whereBugCond+"AND DATE(b.created_on) BETWEEN '"+requestBo.getFromDate()+"' AND '"+requestBo.getToDate()+"'  \r\n";
			whereTCCond=whereTCCond+"AND DATE(mc.createdDate) BETWEEN '"+requestBo.getFromDate()+"' AND '"+requestBo.getToDate()+"'  \r\n";
		}
		String tableData="SELECT tmp.name, sum(tmp.total) as exeCount,  sum(tmp.pass) AS pass, sum(tmp.fail) as fail\r\n" + 
				", (select count(b.bug_id) FROM BugDetails b WHERE b.project_id IN (:projectId)\r\n" + 
				"AND b.created_by=tmp.testerId "+ whereBugCond + " ) AS bugCount\r\n" + 
				", sum(tmp.executionTime), (SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId \r\n" + 
				"	IN(SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in(SELECT testSuitesId FROM \r\n" + 
				"	Mannual_TestSuites_Table mt WHERE mt.projectId IN (:projectId) \r\n" + 
				"		AND mt.isDeleted='N')) AND mc.prepared_by=tmp.testerId  \r\n" + 
				whereTCCond + " ) AS prepCount \r\n" + 
				"		, (SELECT SUM(mc.preparation_time) from Mannual_TestCase_Table mc WHERE mc.testGroupId  \r\n" + 
				"			IN(SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId  \r\n" + 
				"			in(SELECT testSuitesId FROM Mannual_TestSuites_Table mt WHERE mt.projectId IN (:projectId) \r\n" + 
				"			AND mt.isDeleted='N' )) AND mc.prepared_by=tmp.testerId "+whereTCCond+" )\r\n" + 
				"				AS prepTime from\r\n" + 
				"		(SELECT u.name, m.testerId ,COUNT(*) AS total,sum(m.executionTime) AS executionTime\r\n" + 
				"			, if(m.STATUS='Pass',count(m.STATUS),0) AS pass, if(m.STATUS='Fail',count(m.STATUS),0) AS fail\r\n" + 
				"FROM MannualTestCaseExecutionLogs m,UserDetailsEntity u\r\n" + 
				"WHERE m.testerId=u.id AND m.testerId IN (:testerId) AND m.projectId IN (:projectId) \r\n" + 
				"AND m.testCaseAction=:action \r\n" + whereCond + 
				"			GROUP BY m.testerId,m.STATUS)tmp GROUP BY tmp.name;";
		
				/*"SELECT tmp.name, sum(tmp.total) as exeCount,  sum(tmp.pass) AS pass, sum(tmp.fail) as fail, tmp.testerId, sum(tmp.executionTime) from\r\n" + 
				"				 (SELECT u.name, m.testerId ,COUNT(*) AS total,sum(m.executionTime) AS executionTime, if(m.STATUS='Pass',count(m.STATUS),0) AS pass, if(m.STATUS='Fail',count(m.STATUS),0) AS fail\r\n" + 
				"				 FROM MannualTestCaseExecutionLogs m,UserDetailsEntity u\r\n" + 
				"				 WHERE m.testerId=u.id AND m.testerId IN (:testerId) AND m.projectId IN (:projectId) AND m.testCaseAction=:action \r\n"+
						whereCond+
				"			GROUP BY m.testerId,m.STATUS)tmp GROUP BY tmp.name;";*/
				
				
				/*"SELECT tmp.NAME, sum(tmp.total) as exeCount,  sum(tmp.pass) AS pass\r\n" + 
				", sum(tmp.fail) as fail, tmp.testerId, sum(tmp.executionTime) from\r\n" + 
				"				 (SELECT u.NAME, m.testerId ,COUNT(*) AS total,sum(m.executionTime) AS executionTime\r\n" + 
				"				 , if(m.STATUS='Pass',count(m.STATUS),0) AS pass, if(m.STATUS='Fail',count(m.STATUS),0) AS fail\r\n" + 
				"				 FROM MannualTestCaseExecutionLogs m,UserDetailsEntity u\r\n" + 
				"				 WHERE m.testerId=u.id AND m.testerId IN (:testerId) AND m.projectId IN (:projectId) \r\n" + 
				"				 AND m.testCaseAction='Completed' "+whereCond+" group by m.testerId,m.STATUS)tmp GROUP BY tmp.NAME";*/
				/*;*/
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(tableData);
			System.out.println();
			query.setParameter("action", "Completed");
			query.setParameterList("testerId", requestBo.getTesterList());
			query.setParameterList ("projectId", requestBo.getProjectList());
			list = query.list();
			for(int i=0; i<list.size(); i++) {
				Object[] obj=list.get(i);
				/*list.get(i)[4]=defectDao.getBugCountByTesterID(Integer.valueOf((String) obj[4]), requestBo.getProjectList(), whereBugCond);
				System.out.println("--------Bug count of----"+obj[4]+"--"+obj[0]);*/
				
				//BigDecimal bd=(BigDecimal) obj[5];
				if( obj[5]!=null) {
					Double test2=((Double) obj[5]);
					long testqq= (new Double(test2)).longValue();
					long sec = testqq % 60;
				    long minutes = testqq % 3600 / 60;
				    long hoursss = testqq % 28800 / 3600;
				    long daysss = testqq / 28800;
				    list.get(i)[5]=daysss+"-Day(s) "+hoursss+"-Hour(s) "+minutes+"-Minute(s)";
				}
				else {
					list.get(i)[5]=0+"-Day(s) "+0+"-Hour(s) "+0+"-Minute(s)";
				}
				
			    //System.out.println(obj[1]+"--"+obj[2]+"--"+obj[3]+"--"+obj[5]);
				if(obj[7]!=null) {
					Double test3=((BigDecimal) obj[7]).doubleValue();
					long testqq3= (new Double(test3)).longValue();
					long sec3 = testqq3 % 60;
				    long minutes3 = testqq3 % 3600 / 60;
				    long hoursss3 = testqq3 % 28800 / 3600;
				    long daysss3 = testqq3 / 28800;
				    list.get(i)[7]=daysss3+"-Day(s) "+hoursss3+"-Hour(s) "+minutes3+"-Minute(s)";
				}
				else {
				    list.get(i)[7]=0+"-Day(s) "+0+"-Hour(s) "+0+"-Minute(s)";
				}
			    
			}
			/*
			 * for (Object[] objects : list) { ResourceResponseBo responseBo=new
			 * ResourceResponseBo(); responseBo.setTesterName ((String) objects[0]);
			 * responseBo.setExeCount(objects[1]!=null?objects[1].toString():null);
			 * responseBo.setExeTime(objects[2]!=null?objects[2].toString():null);
			 * responseBo.setExePass(objects[3]!=null?(objects[3]).toString():null);
			 * responseBo.setExeFail(objects[4]!=null?(objects[4]).toString():null);
			 * effortsList.add(responseBo); }
			 */
			
		}
		catch(Exception e)
		{		
			e.printStackTrace();
		}
		return list;
	}
	
	public List<Object[]> tableResourceEffortsForManager(ResourceRequestBo requestBo) 
	{
		List<Object[]> list = null;
		List<ResourceResponseBo> effortsList = new ArrayList<>();
		String whereCond="\r\n";
		String whereBugCond="\r\n";
		if(!(requestBo.getFromDate()==null && requestBo.getToDate()==null))
		{
			whereCond=whereCond + "AND DATE(m.dateOfExec) BETWEEN '"+requestBo.getFromDate()+"' AND '"+requestBo.getToDate()+"'  \r\n";
			whereBugCond=whereBugCond+"AND DATE(b.created_on) BETWEEN '"+requestBo.getFromDate()+"' AND '"+requestBo.getToDate()+"'  \r\n";
		}
		String tableData="SELECT tmp.name, sum(tmp.total) as exeCount, sum(tmp.Pass) AS pass, sum(tmp.Fail) as fail, tmp.testerId, sum(tmp.executionTime) from\r\n" + 
				"				 (SELECT u.name, m.testerId, COUNT(*) AS total,SUM(m.executionTime) AS executionTime, if(m.STATUS='Pass',count(m.STATUS),0) AS pass, if(m.STATUS='Fail',count(m.STATUS),0) AS fail\r\n" + 
				"				 FROM MannualTestCaseExecutionLogs m,UserDetailsEntity u\r\n" + 
				"				 WHERE m.testerId=u.id AND m.testerId IN (:testerId) AND m.projectId IN (:projectId) AND m.testCaseAction=:action  \r\n" + 
				whereCond+
				"			GROUP BY m.testerId,m.STATUS)tmp GROUP BY tmp.name;";
				
				
		System.out.println(tableData);
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(tableData);
			System.out.println();
			query.setParameter("action", "Completed");
			query.setParameterList("testerId", requestBo.getTesterList());
			query.setParameterList ("projectId", requestBo.getProjectList());
			list = query.list();
			for(int i=0; i<list.size(); i++) {
				Object[] obj=list.get(i);
				list.get(i)[4]=defectDao.getBugCountByTesterID(Integer.valueOf((String) obj[4]), requestBo.getProjectList(), whereBugCond);
				System.out.println("--------Bug count of----"+obj[4]+"--"+obj[0]);
				//BigDecimal bd=(BigDecimal) obj[5];
				Double test2=(Double) obj[5];
				long testqq= (new Double(test2)).longValue();
				long sec = testqq % 60;
			    long minutes = testqq % 3600 / 60;
			    long hoursss = testqq % 28800 / 3600;
			    long daysss = testqq / 28800;
			    list.get(i)[5]=daysss+"-Day(s) "+hoursss+"-Hour(s) "+minutes+"-Minute(s)";
			    System.out.println(obj[1]+"--"+obj[2]+"--"+obj[3]+"--"+obj[5]);
			}
			
			/*
			 * for (Object[] objects : list) { ResourceResponseBo responseBo=new
			 * ResourceResponseBo(); responseBo.setTesterName ((String) objects[0]);
			 * responseBo.setExeCount(objects[1]!=null?objects[1].toString():null);
			 * responseBo.setExeTime(objects[2]!=null?objects[2].toString():null);
			 * responseBo.setExePass(objects[3]!=null?(objects[3]).toString():null);
			 * responseBo.setExeFail(objects[4]!=null?(objects[4]).toString():null);
			 * effortsList.add(responseBo); }
			 */
			
		}
		catch(Exception e)
		{		
			e.printStackTrace();
		}
		return list;
	}

	public Map<String, List<ResouecesData>> getResorcesEffort(ResourceRequestBo requestBo) {
		List<Object[]> list = null;
		List<ResouecesData> effortsList = new ArrayList<>();
		Map<String, List<ResouecesData>> maps = new HashedMap<String, List<ResouecesData>>();
		String whereCond="";
		String groupByCond="";
		String whereTCCond="";
	System.out.println("projectnam si not nulll..................."+requestBo.getProjectList()+"........testet bne.."+requestBo.getTesterList());
		if ((requestBo.getFromDate() == null && requestBo.getToDate() == null)) {
			whereCond = whereCond + "AND DATE(mtl.updatedTimeStamp) BETWEEN (NOW() - INTERVAL 7 DAY) AND NOW()";
			whereTCCond=whereTCCond+"AND DATE(mc.createdDate) BETWEEN (NOW() - INTERVAL 7 DAY) AND NOW()";
		}
		if (!(requestBo.getFromDate() == null && requestBo.getToDate() == null)) {
			whereCond = whereCond + "AND DATE(mtl.updatedTimeStamp) BETWEEN '" + requestBo.getFromDate() + "' AND '"
					+ requestBo.getToDate() + "'  \r\n";
			whereTCCond=whereTCCond+"AND DATE(mc.createdDate) BETWEEN '"+requestBo.getFromDate()+"' AND '"+requestBo.getToDate()+"'  \r\n";
		}

		String overviewhql="SELECT (SELECT ud.name FROM UserDetailsEntity ud WHERE ud.id=mtl.testerId) AS userame, COUNT(*) AS TotalCount,\r\n" +
				"(SELECT COUNT(DISTINCT mc.SRID) from Mannual_TestCase_Table mc WHERE mc.testGroupId \r\n" + 
				"	IN(SELECT testGroupId FROM Mannual_TestGroup_Table WHERE testSuitesId in(SELECT testSuitesId FROM \r\n" + 
				"	Mannual_TestSuites_Table mt WHERE mt.projectId IN (:ProjectId) \r\n" + 
				"		AND mt.isDeleted='N')) AND mc.prepared_by= mtl.testerId  \r\n" + 
				"AND DATE(mc.createdDate) BETWEEN '2020-11-16' AND '2020-11-21'  \r\n" + 
				" ) AS prepCount, " +
				" COUNT(if(mtl.testCaseAction='Not Started',1,NULL) )AS notstarted ,\r\n" + 
				"COUNT(if(mtl.testCaseAction='Completed',1,NULL) )AS completed,\r\n" + 
				"COUNT(if(mtl.testCaseAction='Under Review',1,NULL) )AS UnderReview,\r\n" + 
				"COUNT(if(mtl.testCaseAction='N/A',1,NULL) )AS NA,\r\n" + 
				"COUNT(if(mtl.testCaseAction='On Hold',1,NULL) )AS OnHold\r\n" + 
				" FROM MannualTestCaseExecutionLogs mtl WHERE mtl.testerId IN (:testerId) " +
				"and mtl.ProjectId IN (:ProjectId)"+whereCond+" GROUP BY mtl.testerId  ";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
//			session.flush();
//			session.clear();
			Query query = session.createNativeQuery(overviewhql);
//			Query query = session.createQuery(overviewhql);
			
			query.setParameterList("testerId", requestBo.getTesterList());
			query.setParameterList("ProjectId", requestBo.getProjectList());
			list = query.list();
			for (Object[] objects : list) {
				if(objects!=null) {
					ResouecesData responseBo=new ResouecesData();
				responseBo.setUserName(objects[0].toString());
				responseBo.setTotalCount(Integer.parseInt(objects[1].toString()));
				responseBo.setTotalCountPrepared(Integer.parseInt(objects[2].toString()));
				responseBo.setNotStarted(Integer.parseInt(objects[3].toString()));
				responseBo.setCompleted(Integer.parseInt(objects[4].toString()));
				responseBo.setUnderReview(Integer.parseInt(objects[5].toString()));
				responseBo.setNa(Integer.parseInt(objects[6].toString()));
				responseBo.setOnhold(Integer.parseInt(objects[7].toString()));
				effortsList.add(responseBo);
				}
			}
			for (ResouecesData details1 : effortsList) {
				if (details1 != null) {
					List<ResouecesData> values = effortsList.stream()
							.filter(x -> x.getUserName().equals(details1.getUserName())).collect(Collectors.toList());
						if (values != null) {	
							maps.put(details1.getUserName(), values);
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return maps;
	}
	
	public Map<String, List<ResouecesData>> getDatewiseUserForResouces(ResourceRequestBo requestBo, String type) {
		List<Object[]> list = null;
		List<ResouecesData> effortsList = new ArrayList<>();
		Map<String, List<ResouecesData>> maps = new HashedMap<String, List<ResouecesData>>();
		String whereCond="";
		String groupByCond="";
		
		if((requestBo.getFromDate()==null && requestBo.getToDate()==null))
		{
			whereCond=whereCond + "AND DATE(m.updatedTimeStamp) BETWEEN (NOW() - INTERVAL 7 DAY) AND NOW()";
		}
		if(!(requestBo.getFromDate()==null && requestBo.getToDate()==null))
		{
			whereCond=whereCond + "AND DATE(m.updatedTimeStamp) BETWEEN '"+requestBo.getFromDate()+"' AND '"+requestBo.getToDate()+"'  \r\n";
		}
	
		String overViewProjectHQL = "SELECT ue.name,COUNT(if(m.testCaseAction='Completed',1,NULL))AS completed,DATE(m.dateOfExec)\r\n" + 
				"				FROM MannualTestCaseExecutionLogs m inner JOIN UserDetailsEntity ue ON ue.id=m.testerId\r\n" + 
				"				WHERE   m.testerId IN (:testerId)  and m.projectId IN (:ProjectId)\r\n" + 
				
				whereCond +
				"GROUP BY m.testerId"
				+ groupByCond;
		
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			
			query.setParameterList("testerId", requestBo.getTesterList());
			query.setParameterList("ProjectId", requestBo.getProjectList());
		
			list = query.list();
			for (Object[] objects : list) {
				if(objects[2]!=null) {
					ResouecesData responseBo=new ResouecesData();
				responseBo.setCompleted(Integer.parseInt(objects[1].toString()));
				responseBo.setExcutionDate((Date) objects[2]);
				responseBo.setUserName((String) objects[0]);
				System.out.println("hello Monali           "+responseBo);
				effortsList.add(responseBo);
				}
			}
			for (ResouecesData details1 : effortsList) {
				if (details1 != null) {
					List<ResouecesData> values = effortsList.stream()
							.filter(x -> x.getUserName().equals(details1.getUserName())).collect(Collectors.toList());
						if (values != null) {						
							maps.put(details1.getUserName(), values);
					}
					
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return maps;
	}
	
	public List<userRole> getUserListOfResouceByProjectIDs(List<Integer> pIDs) {
		List<Object[]> list = null;
		List<userRole> userlist = new ArrayList<userRole>();
		String overViewProjectHQL = "SELECT id,name FROM UserDetailsEntity where userRole='100'\r\n" + 
				"		union SELECT up.id,up.name FROM Project_MetaTable pm\r\n" + 
				"		inner JOIN ProjectAccessTable p ON pm.projectID=p.projectID \r\n" + 
				"		right JOIN UserDetailsEntity up ON up.id=p.userDetailsId WHERE \r\n" + 
				"		p.projectID IN(:pIDs) GROUP BY p.userDetailsId";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameterList("pIDs", pIDs);
			list = query.list();
			for (Object[] objects : list) {
				userRole userData = new userRole();
				userData.setId((Integer) objects[0]);
				userData.setName((String) objects[1]);

				userlist.add(userData);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return userlist;

	}
	
	public List<userRole> getUserListOfResouceByProjectIDandManagerID(List<Integer> pIDs, Integer managerID) {
		List<Object[]> list = null;
		List<userRole> userlist = new ArrayList<userRole>();
		String overViewProjectHQL = "SELECT up.id,up.name FROM Project_MetaTable pm\r\n" + 
				"	inner JOIN ProjectAccessTable p ON pm.projectID=p.projectID \r\n" + 
				"	right JOIN UserDetailsEntity up ON up.id=p.userDetailsId WHERE \r\n" + 
				"	up.managerID=:managerID and p.projectID IN(:pIDs) GROUP BY p.userDetailsId;";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameterList("pIDs", pIDs);
			query.setParameter("managerID", managerID);
			list = query.list();
			for (Object[] objects : list) {
				userRole userData = new userRole();
				userData.setId((Integer) objects[0]);
				userData.setName((String) objects[1]);

				userlist.add(userData);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return userlist;

	}
}
