package com.apmosys.tmt.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//import javax.transaction.Transactional;

import org.apache.commons.collections4.map.HashedMap;
//import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.apmosys.tmt.BO.ExecutionReportAutomation;
import com.apmosys.tmt.models.ExecutionStepsBo;
import com.apmosys.tmt.models.ExecutionVerifyBO;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

@Repository
@Transactional
public class AutomationLogsDao {
	@PersistenceContext
    private EntityManager entityManager;
	@Autowired
	SessionFactory sessionFactory;
	/*public Map<String, ExecutionReportAutomation> getExecutionLogsByRunid(Integer runId) {

		Session session=entityManager.unwrap(Session.class);
		List<Object[]> obj = null;
		Map<String, ExecutionReportAutomation> verifylogs=new HashedMap<String, ExecutionReportAutomation>();
		try {
			String currentRunQuery = "SELECT l.id,l.RUN_ID,l.project_name,l.Application_Name,l.ScenarioID,l.TestCaseID,l.Response_Time,l.Expected_Result,l.Actual_Result,l.status,l.TIMESTAMP,l.Description,l.BrowserType\r\n" + 
					",l.ScreenshotID FROM logs l\r\n" + 
					"WHERE l.RUN_ID=:runId";
			String currentRunQuery = "SELECT l.id,l.RUN_ID,l.project_name,l.Application_Name,l.ScenarioID,l.TestCaseID,l.Response_Time,l.Expected_Result,l.Actual_Result,l.STATUS,\r\n" + 
					"			l.TIMESTAMP,l.Description,l.BrowserType,l.ScreenshotID,s.image_value  FROM logs l left JOIN screenshots s ON l.ScreenshotID=s.id WHERE l.RUN_ID=:runId";
			
			
			String currentRunQuery =" SELECT l.id,l.RUN_ID,l.project_name,l.Application_Name,l.ScenarioID,l.TestCaseID,l.Response_Time,\r\n" + 
					"l.Expected_Result,l.Actual_Result,l.STATUS,\r\n" + 
					"			l.TIMESTAMP,l.Description,l.BrowserType,l.ScreenshotID,s.image_value\r\n" + 
					",p.status,p.description,p.browser,p.summary,p.bugSummary,p.expectedResult,p.severity,p.priority\r\n" + 
					" FROM logs l left JOIN screenshots s ON l.ScreenshotID=s.id \r\n" + 
					"left join logsautomation p on l.RUN_ID=p.runid and l.TestCaseID=p.testcaseid  where l.RUN_ID=:runId";
			
			Query qry = session.createNativeQuery(currentRunQuery);
			qry.setParameter("runId", runId);
			obj = qry.list();
			for (Object[] object : obj) {
			
				ExecutionVerifyBO bo=new ExecutionVerifyBO();
				bo.setId((Integer)object[0]);
				bo.setRunId((Integer)object[1]);
				bo.setProjectName((String)object[2]);
				bo.setApplicationName((String)object[3]);
				bo.setScenarioId((String)object[4]);
				bo.setTestCaseId((String)object[5]);
				bo.setResTime((String)object[6]);
				bo.setExpectedResult((String)object[7]);
				bo.setActualResult((String)object[8]);
				bo.setStatus((String)object[9]);
				bo.setTimeStamp((String)object[10]);
				bo.setDescription((String)object[11]);
				bo.setBrowserType((String)object[12]);
				bo.setScreenShotId((String)object[13]);
				bo.setScreenshot((byte[]) object[14]);
				bo.setTestcasestatus((String)object[15]);
				bo.setTestcasestatus((String)object[15]);
				bo.setTestcasestatus((String)object[15]);
				verifylogs.put((String)object[13], bo);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return verifylogs;
	}*/
	
	public Map<String, ExecutionReportAutomation> getExecutionLogsByRunid(Integer runId,String scenario) {

		Session session=entityManager.unwrap(Session.class);
		List<Object[]> obj = null;
		Map<String, ExecutionReportAutomation> verifylogs = new HashedMap<String, ExecutionReportAutomation>();
		try {
			String currentRunQuery=null;
			if(scenario.equalsIgnoreCase("all")) {
				
			 currentRunQuery =" SELECT l.id,l.RUN_ID,l.project_name,l.Application_Name,l.ScenarioID,l.TestCaseID,l.Response_Time,\r\n" + 
					"l.Expected_Result,l.Actual_Result,l.STATUS,\r\n" + 
					"l.TIMESTAMP,l.Description,l.BrowserType,l.ScreenshotID,s.image_value\r\n" + 
					",p.casestatus,p.description,p.browser,p.summary,p.bugSummary,p.expectedResult,p.severity,p.priority,p.actualresult,l.TestcaseType,l.Step\r\n" + 
					" FROM logs l left JOIN screenshots s ON l.ScreenshotID=s.id \r\n" + 
					"left join logsautomation p on l.RUN_ID=p.runid and l.TestCaseID=p.testcaseid  where l.RUN_ID=:runId ";
			}else {
				
			
			 currentRunQuery =" SELECT l.id,l.RUN_ID,l.project_name,l.Application_Name,l.ScenarioID,l.TestCaseID,l.Response_Time,\r\n" + //6
					"l.Expected_Result,l.Actual_Result,l.STATUS,\r\n" + //9
					"l.TIMESTAMP,l.Description,l.BrowserType,l.ScreenshotID,s.image_value\r\n" + //14
					",p.casestatus,p.description,p.browser,p.summary,p.bugSummary,p.expectedResult,p.severity,p.priority,p.actualresult,l.TestcaseType,l.Step\r\n" + 
					" FROM logs l left JOIN screenshots s ON l.ScreenshotID=s.id \r\n" + 
					"left join logsautomation p on l.RUN_ID=p.runid and l.TestCaseID=p.testcaseid  where l.RUN_ID=:runId and l.ScenarioID=:scenario";
			
			}
			Query qry = session.createNativeQuery(currentRunQuery);
			qry.setParameter("runId", runId);

			if(!scenario.equalsIgnoreCase("all")) 
			qry.setParameter("scenario", scenario);
			
			System.out.print("qry for senario:"+qry);
			
			obj = qry.list();
			for (Object[] object : obj) {
			
				ExecutionReportAutomation bo=new ExecutionReportAutomation();
				bo.setId((Integer)object[0]);
				bo.setRunId((Integer)object[1]);
				bo.setProjectName((String)object[2]);
				bo.setApplicationName((String)object[3]);
				bo.setScenarioId((String)object[4]);
				bo.setTestCaseId((String)object[5]);
				bo.setResTime((String)object[6]);
				bo.setExpectedResult((String)object[7]);
				bo.setActualResult((String)object[8]);
				bo.setStatus((String)object[9]);
				bo.setTimeStamp((String)object[10]);
				bo.setDescription((String)object[11]);
				bo.setBrowserType((String)object[12]);
				bo.setScreenShotId((String)object[13]);
				bo.setScreenshot((byte[]) object[14]);
				bo.setTstatus((String)object[15]);
				bo.setTdescription((String)object[16]);
				bo.setTbrowser((String)object[17]);
				bo.setTcasesid((String)object[5]);
				bo.setTsummary((String)object[18]);
				bo.setTbugsummary((String)object[19]);
				bo.setTexpectedResult((String)object[20]);
				bo.setTseverity((String)object[21]);
				bo.setTpriority((String)object[22]);
				bo.setTestcaseType((String)object[24]);
				bo.setStep((String)object[25]);
				verifylogs.put((String)object[13], bo);
			//	System.out.print("BO:"+bo);
				
				
			}
		//	System.out.print("verifylogs:"+verifylogs);
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return verifylogs;
	}
	
	public List<ExecutionStepsBo> getExecutionStepsByRunid(Integer runId) {

		Session session=entityManager.unwrap(Session.class);
		List<Object[]> obj = null;
		List<ExecutionStepsBo> verifylogs=new ArrayList<ExecutionStepsBo>();
		try {
			String currentRunQuery = "SELECT e.id,e.RUN_ID,e.StepsNo,e.ScenarioID,e.TestCaseID,e.module, "
					+ "e.datafield,e.testdata,e.TIMESTAMP,e.Control,e.logs_id FROM executionsteps e "
					+ " WHERE e.RUN_ID=:runId ORDER BY e.TestCaseID ASC";
			Query qry = session.createNativeQuery(currentRunQuery);
			qry.setParameter("runId", runId);
			obj = qry.list();
			for (Object[] object : obj) {
			
				ExecutionStepsBo bo=new ExecutionStepsBo();
				bo.setId((Integer)object[0]);
				bo.setRunId(object[1].toString());
				bo.setStepNo(object[2].toString());
				bo.setScenarioId((String)object[3]);
				bo.setTestCaseId((String)object[4]);
				bo.setModule((String)object[5]);
				bo.setDataField((String)object[6]);
				bo.setTestData((String)object[7]);
				bo.setTimeStamp((String)object[8]);
				bo.setControl((String)object[9]);
				bo.setLogsId((String)object[10]);
				verifylogs.add(bo);
			}//for (Object[] object : obj) 
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return verifylogs;
	}
	public Object getEnvironmentByRunid(Integer runId) {

		Session session=entityManager.unwrap(Session.class);
		Object obj = null;
		ExecutionVerifyBO environment = new ExecutionVerifyBO();
		try {
			String currentRunQuery = "SELECT \r\n" + 
					"(SELECT l.IP_HOST_ZONE FROM logs l WHERE l.RUN_ID=:runId GROUP BY l.IP_HOST_ZONE LIMIT 1) AS environment,\r\n" + 
					"(SELECT l.Timestamp FROM  logs l WHERE l.RUN_ID=:runId ORDER BY l.Timestamp LIMIT 1) as 'start',\r\n" + 
					"(SELECT l.Timestamp FROM logs l WHERE l.RUN_ID=:runId ORDER BY l.Timestamp DESC LIMIT 1) as 'end'";
			Query qry = session.createNativeQuery(currentRunQuery);
			qry.setParameter("runId", runId);
			obj = qry.list();
			//environment.setIp((String)obj[0]);
			/*for (Object[] object : obj) {
				
			}
			*/
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return obj;
	}
	
	
	public List<String> getScenarioRunwise(Integer runId) {

		Session session=entityManager.unwrap(Session.class);
		List<String> obj = null;
		try {
			String currentRunQuery = "select distinct ScenarioID from logs where RUN_ID=:runId";
			Query qry = session.createNativeQuery(currentRunQuery);
			qry.setParameter("runId", runId);
			obj = qry.list();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return obj;
	}
	public List<String> getTotalScenariosTestCasesSteps(Integer runId) {
		
//		Session session = sessionFactory.getCurrentSession();
		Session session=entityManager.unwrap(Session.class);
		List<String> obj = null;
		try {
			String currentRunQuery = "";
			Query qry = session.createNativeQuery(currentRunQuery);
			qry.setParameter("runId", runId);
			obj = qry.list();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return obj;
	}
	
	
}
