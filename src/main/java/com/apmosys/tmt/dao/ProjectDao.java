package com.apmosys.tmt.dao;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.AutomationMetadata;
import com.apmosys.tmt.models.AutomationRunLogs;
import com.apmosys.tmt.models.AutomationRunTable;
import com.apmosys.tmt.models.OverviewProjectDetails;
import com.apmosys.tmt.models.ProjectAccessEntity;
import com.apmosys.tmt.models.ProjectDeatilsMeta;
import com.apmosys.tmt.models.ProjectEntitlement;
import com.apmosys.tmt.repository.AutomationLogsRepository;
import com.apmosys.tmt.repository.AutomationMetaRepository;
import com.apmosys.tmt.repository.AutomationRunRepository;
import com.apmosys.tmt.repository.ProjectRepository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.PersistenceContext;
import jakarta.servlet.http.HttpSession;
@Repository
@Transactional
public class ProjectDao {
	@PersistenceContext
    private EntityManager entityManager;
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	private HttpSession httpSession; 
	@Autowired
	AutomationMetaRepository automationMetaRepository;
	@Autowired
	private AutomationLogsRepository automationlogry;
	@Autowired
	private AutomationRunRepository automationRunRepository;

	
	@Transactional
	public Object saveProjectMetaDetails(ProjectDeatilsMeta uploadData) {
		Object count = 0;
		Session session=entityManager.unwrap(Session.class);
//		Transaction tx = session.beginTransaction();
		try {					
			count = session.save(uploadData);
//			tx.commit();
		} catch (Exception e) {
//			tx.rollback();
			e.printStackTrace();
		} 
		return count;
	}
	
	public ProjectDeatilsMeta getProjectDetails(Integer pid) {
		ProjectDeatilsMeta projectDetails = new ProjectDeatilsMeta();
		Query qry=null;
		Session session=entityManager.unwrap(Session.class);
		session.setCacheMode(CacheMode.IGNORE);
		session.flush();
		session.clear();
		String query="SELECT p.projectID ,p.projectName,b.bugTrackingToolName,b.toolid,b.Userid,b.password,b.IP, b.apiToken FROM ProjectDeatilsMeta p , BugTrackorToolEntity b  WHERE p.isJiraEnable= b.toolid  and p.projectID=:pid and p.isJiraEnable IS NOT null";
		qry = session.createQuery(query);
		qry.setParameter("pid", pid);
		List<Object[]> list=qry.list();
		for (Object[] objects : list) {
			System.out.println(objects);
			projectDetails.setProjectID((Integer) objects[0]);
			projectDetails.setProjectName((String) objects[1]);
			projectDetails.setToolId(((Integer)objects[3]).toString());
			projectDetails.setToolName((String) objects[2]);
			projectDetails.setBugTool((String) objects[4]);
			projectDetails.setBugToolPassword((String) objects[5]);
			projectDetails.setBugToolURL((String) objects[6]);
			projectDetails.setBugToolApiToken((String) objects[7]);
		}
		return projectDetails;
	}
	
	//new added
	public ProjectDeatilsMeta getProjectDetailsauto1(String projectName) {
		ProjectDeatilsMeta projectDetails = new ProjectDeatilsMeta();
		Query qry=null;
		Session session=entityManager.unwrap(Session.class);
		session.setCacheMode(CacheMode.IGNORE);
		session.flush();
		session.clear();
		String query="SELECT p.projectID ,p.projectName,b.bugTrackingToolName,b.toolid,b.Userid,b.password,b.IP, b.apiToken FROM ProjectDeatilsMeta p , BugTrackorToolEntity b  WHERE p.isJiraEnable= b.toolid  and p.projectName=:pid and p.isJiraEnable IS NOT null";
		qry = session.createQuery(query);
		qry.setParameter("pid", projectName);
		List<Object[]> list=qry.list();
		for (Object[] objects : list) {
			System.out.println(objects);
			projectDetails.setProjectID((Integer) objects[0]);
			projectDetails.setProjectName((String) objects[1]);
			projectDetails.setToolId(((Integer)objects[3]).toString());
			projectDetails.setToolName((String) objects[2]);
			projectDetails.setBugTool((String) objects[4]);
			projectDetails.setBugToolPassword((String) objects[5]);
			projectDetails.setBugToolURL((String) objects[6]);
			projectDetails.setBugToolApiToken((String) objects[7]);
		}
		return projectDetails;
	}

	public ProjectDeatilsMeta findByMinutesDate(Integer projectId) {
		ProjectDeatilsMeta projectDetails = new ProjectDeatilsMeta();
		List projectDetail=new ArrayList<String>();
		String one=null;
		Query qry=null;
		int hours;
		int days; 
		int restMinutes;
		int onedayMinutes = 480;
		String delay="";
		Session session=entityManager.unwrap(Session.class);
		session.setCacheMode(CacheMode.IGNORE);
		session.flush();
		session.clear();
		String query="select TIMESTAMPDIFF(MINUTE,mr.projectEndDate,NOW()) AS minute from Project_MetaTable mr WHERE  mr.projectID=:projectId";
		qry = session.createNativeQuery(query);
		qry.setParameter("projectId", projectId);
		projectDetail =qry.list();
		if(projectDetail.isEmpty()) {
			
		}
		else {
		projectDetails.setDelayTimeProject(projectDetail.get(0).toString());
			int minutes=Integer.parseInt(projectDetails.getDelayTimeProject());
			if(minutes<0)
			{
				delay="in time";
			}
			else
			{
			if(minutes<60)
			{
				/*delay=" "+minutes+"-minutes";
				runDetails.setDelayby(delay);*/
				delay="in time";
			}
			else if(minutes > 60 && minutes < onedayMinutes)
			 {
				
		        /*hours = (int) Math.floor(minutes/60);
		        restMinutes = minutes%60;
		        delay=" "+hours+"-hours "+restMinutes+"-minutes";*/
				delay="in time";
		      }
			else{	
		        days = (int) Math.floor((minutes/60)/24);
		        if(days==0)
		        {
		        	delay="in time";
		        }
		        else
		        {
		        restMinutes = minutes % onedayMinutes;
		        hours = (int) Math.floor(restMinutes/60); 
		        restMinutes = restMinutes % 60;
		        delay= " "+days+"-days ";
		        }
		       
		    }
			}
		}

	projectDetails.setDelayTimeProject(delay);
	
			
		return projectDetails;
	}


	
	
/*	public ProjectDeatilsMeta getProjectDetails(Integer pid) {
		ProjectDeatilsMeta projectDetails = new ProjectDeatilsMeta();
		Query qry=null;
		Session session=entityManager.unwrap(Session.class);
		session.setCacheMode(CacheMode.IGNORE);
		session.flush();
		session.clear();
		String query="SELECT p.projectID ,p.projectName,b.bugTrackingToolName,b.Toolid  FROM ProjectDeatilsMeta p , BugTrackorToolEntity b  WHERE p.isJiraEnable= b.Toolid  and p.projectID=:pid and p.isJiraEnable!=null";
		qry = session.createNativeQuery(query);
		qry.setParameter("pid", pid);
		List<Object[]> list=qry.list();
		for (Object[] objects : list) {
			System.out.println(objects);
			projectDetails.setProjectID((Integer) objects[0]);
			projectDetails.setProjectName((String) objects[1]);
			projectDetails.setToolId(((Integer)objects[3]).toString());
			projectDetails.setToolName((String) objects[2]);
		}
		return projectDetails;
	}*/
	public List<ProjectDeatilsMeta> getAllProject() {
		return projectRepository.getAllProject();
	}
	public List<OverviewProjectDetails> overViewProjectDetails(int id) {
		List<Object[]> list=null;
		List<OverviewProjectDetails> projectDetails=new ArrayList<OverviewProjectDetails>();
		Session session=entityManager.unwrap(Session.class);
		session.setCacheMode(CacheMode.IGNORE);
		session.flush();
		session.clear();
		Query qry=null;
		System.out.println("----------overViewProjectDetails-------"+id);
		String overViewProjectHQL="SELECT p.projectID,p.projectName, COUNT(DISTINCT mts.testSuitesId), COUNT(DISTINCT mg.testGroupId), COUNT(DISTINCT mt.SRID)"+
				"FROM ProjectAccessTable a "
				+"INNER JOIN Project_MetaTable p ON a.projectID=p.projectID "+
				"LEFT JOIN Mannual_TestSuites_Table mts ON p.projectID=mts.projectId "+
				"LEFT JOIN Mannual_TestGroup_Table mg ON mts.testSuitesId=mg.testSuitesId "+
				"LEFT JOIN Mannual_TestCase_Table mt ON mg.testGroupId =mt.testGroupId "+
				"WHERE a.userDetailsId=:userid "+
				"GROUP BY p.projectID order by p.createdTime desc";
		qry = session.createNativeQuery(overViewProjectHQL);
		qry.setParameter("userid",id);
	
		try {
			 list=qry.list();
			for (Object[] objects : list) {
				OverviewProjectDetails details=new OverviewProjectDetails();
				details.setProjectId((Integer) objects[0]);
				details.setProjectName((String) objects[1]);
				details.setTestSuitesNo(Long.valueOf(((BigInteger)objects[2]).toString()));
				details.setTestGroupNo(Long.valueOf(((BigInteger)objects[3]).toString()));
				details.setTestCaseNo(Long.valueOf(((BigInteger)objects[4]).toString()));
				projectDetails.add(details);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return projectDetails; 
	}	
	public List<OverviewProjectDetails> overViewProjectDetailsAdmin() {
		List<Object[]> list=null;
		List<OverviewProjectDetails> projectDetails=new ArrayList<OverviewProjectDetails>();
		Session session=entityManager.unwrap(Session.class);
		session.setCacheMode(CacheMode.IGNORE);
		session.flush();
		session.clear();
		Query qry=null;
		String overViewProjectHQL="SELECT p.projectID,p.projectName, COUNT(DISTINCT mts.testSuitesId), COUNT(DISTINCT mg.testGroupId), COUNT(DISTINCT mt.SRID)"+
				"From Project_MetaTable p "+
				"LEFT JOIN Mannual_TestSuites_Table mts ON p.projectID=mts.projectId "+
				"LEFT JOIN Mannual_TestGroup_Table mg ON mts.testSuitesId=mg.testSuitesId "+
				"LEFT JOIN Mannual_TestCase_Table mt ON mg.testGroupId =mt.testGroupId "+
				"GROUP BY p.projectID order by p.createdTime desc";
		qry = session.createNativeQuery(overViewProjectHQL);
		
		try {
			Query query= session.createNativeQuery(overViewProjectHQL);
			list=query.list();
			for (Object[] objects : list) {
				OverviewProjectDetails details=new OverviewProjectDetails();
				details.setProjectId((Integer) objects[0]);
				details.setProjectName((String) objects[1]);
				details.setTestSuitesNo(Long.valueOf(((BigInteger)objects[2]).toString()));
				details.setTestGroupNo(Long.valueOf(((BigInteger)objects[3]).toString()));
				details.setTestCaseNo(Long.valueOf(((BigInteger)objects[4]).toString()));
				projectDetails.add(details);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return projectDetails; 
	}
public boolean getProjectDetailbyid(int projectid) {
		// TODO Auto-generated method stub
		return false;
	}
@Transactional
	public ProjectDeatilsMeta deleteprojectbyid(int projectid) {
		Session session=entityManager.unwrap(Session.class);
//		EntityTransaction tx=entityManager.getTransaction();
//	if(!tx.isActive()) {
//		tx.begin();
//	}
//		Transaction tx=session.beginTransaction();
		ProjectDeatilsMeta accessEntity=new ProjectDeatilsMeta();
		accessEntity.setProjectID(projectid);
		try
		{
			session.setCacheMode(CacheMode.IGNORE);
		String sql = "delete from ProjectDeatilsMeta where projectID=:pID";
		Query query = session.createQuery(sql);
		query.setParameter("pID",projectid);
		query.executeUpdate();
		session.flush();
		session.clear();
//		tx.commit();
		}
		catch(Exception e)
		{
//			tx.rollback();
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return accessEntity;
	}
	public String updateProject(ProjectDeatilsMeta projectDeatilsMeta) {
		{

			Session session=entityManager.unwrap(Session.class);
			String msg="Success";
			try
//			(Session session=entityManager.unwrap(Session.class))
			{	
			System.out.println("oldprojectname........oldprojectname......."+projectDeatilsMeta.getOldprojectname());
			ProjectDeatilsMeta projectDeatilsMeta2=projectRepository.findByProjectID(projectDeatilsMeta.getProjectID());
			List<AutomationRunTable> autoruntable=automationRunRepository.findByProject_name(projectDeatilsMeta.getOldprojectname());
			
			int projectid=projectDeatilsMeta.getProjectID();
//			AutomationMetadata aprjmeta=automationMetaRepository.findByProjectid(projectDeatilsMeta.getProjectID());
			AutomationMetadata aprjmeta=automationMetaRepository.findByProjectid(projectid);
			
			List<AutomationRunLogs> arl=null;
			 arl=automationlogry.findByProject_name(projectDeatilsMeta.getOldprojectname());
			//System.out.println("Project id drom project dao..."+projectDeatilsMeta.getProjectID()+"....arl..."+arl.get(0).getProject_name());
			
			System.out.println(projectDeatilsMeta.getProjectStartDate()+"....pa71TO..");	
				System.out.println(projectDeatilsMeta.getProjectEndDate()+"...projectDeatilsMeta2"+projectDeatilsMeta.getCreatedBy());
				if(aprjmeta==null)
				{
					if(projectDeatilsMeta.getIsAutomation().equals("true"))
					{
						AutomationMetadata amd=new AutomationMetadata();
						amd.setProjectid(projectDeatilsMeta2.getProjectID());
						amd.setProjectname(projectDeatilsMeta2.getProjectName());
//						//session.save(amd);
						 if(arl.isEmpty())
						    {
						   
						    }
						 else {
							// AutomationRunLogs autolog=new AutomationRunLogs();
							 //arl.get(0).setProject_name(projectDeatilsMeta2.getProjectName());
							  automationlogry.updateLogProjectname(projectDeatilsMeta.getProjectName(),projectDeatilsMeta.getOldprojectname());
							 //session.update(arl.get(0));
						 }
						 if(autoruntable.isEmpty())
						 {
							 
						 }else {
							 automationRunRepository.updateautoruntable(projectDeatilsMeta.getProjectName(), projectDeatilsMeta.getOldprojectname());;
						 }
					}
					projectDeatilsMeta.setCreatedBy(projectDeatilsMeta2.getCreatedBy());
					projectDeatilsMeta.setCreatedTime(projectDeatilsMeta2.getCreatedTime());
					System.out.println(projectDeatilsMeta.getCreatedBy()+"..null auto meta .."+projectDeatilsMeta.getCreatedTime());
//				    session.update(projectDeatilsMeta);
				    session.merge(projectDeatilsMeta);
				}
				else {
					if(projectDeatilsMeta.getIsAutomation().equals("false"))
					{
				projectDeatilsMeta.setCreatedBy(projectDeatilsMeta2.getCreatedBy());
				projectDeatilsMeta.setCreatedTime(projectDeatilsMeta2.getCreatedTime());
				//aprjmeta.setProjectname(projectDeatilsMeta.getProjectName());
				System.out.println(projectDeatilsMeta.getCreatedBy()+"..."+projectDeatilsMeta.getCreatedTime());
//			    session.update(projectDeatilsMeta);
			    session.merge(projectDeatilsMeta);
			    //session.update(aprjmeta);
			   
				}
					else{
						System.out.println("test cases..."+projectDeatilsMeta2.getCreatedBy());
						projectDeatilsMeta.setCreatedBy(projectDeatilsMeta2.getCreatedBy());
						projectDeatilsMeta.setCreatedTime(projectDeatilsMeta2.getCreatedTime());
						aprjmeta.setLocationofDatasheet(aprjmeta.getLocationofDatasheet());
						aprjmeta.setNoOfDatasheet(aprjmeta.getNoOfDatasheet());
						aprjmeta.setNoOfTestcases(aprjmeta.getNoOfTestcases());
						aprjmeta.setNoOftestSuites(aprjmeta.getNoOftestSuites());
						aprjmeta.setProjectname(projectDeatilsMeta.getProjectName());
						System.out.println(projectDeatilsMeta.getCreatedBy()+"..."+projectDeatilsMeta.getCreatedTime());
//					    session.update(projectDeatilsMeta);
					    session.merge(projectDeatilsMeta);
					   // session.update(aprjmeta);
					    if(arl.isEmpty())
					    {
					   
					    }
					 else {
						  automationlogry.updateLogProjectname(projectDeatilsMeta.getProjectName(),projectDeatilsMeta.getOldprojectname());

					 }
					    if(autoruntable.isEmpty())
						 {
							 
						 }else {
							 automationRunRepository.updateautoruntable(projectDeatilsMeta.getProjectName(), projectDeatilsMeta.getOldprojectname());;
						 }
					}
				}
			}
			
			
			catch(Exception e)
			{
				msg="Unable to update";
				e.printStackTrace();
				
			}
			finally {
				session.close();
			}
			
			return msg;
		}

	}
	public String deleteTestSuites(Integer testsuitesid) {
		Session session=entityManager.unwrap(Session.class);
		Transaction tx=session.beginTransaction();
		ProjectDeatilsMeta accessEntity=new ProjectDeatilsMeta();
		String msg="Fail";
		try
		{
			session.setCacheMode(CacheMode.IGNORE);
		String sql = "delete from MannualTestSuites where testSuitesId=:suitesid";
		Query query = session.createNativeQuery(sql);
		query.setParameter("suitesid",testsuitesid);
		query.executeUpdate();
		tx.commit();
		msg="Success";
		}
		catch(Exception e)
		{
			e.printStackTrace();
			msg="Fail";
		}
		return msg;
	}
	
	@Transactional
	public String updateTestSuitesAsDeleted(String updatedByEmail, Integer testsuitesid, String testsuitesname) {
		Session session=entityManager.unwrap(Session.class);
//		Transaction tx=session.beginTransaction();
		//ProjectDeatilsMeta accessEntity=new ProjectDeatilsMeta();
		String msg="Fail";
		try
		{
			session.setCacheMode(CacheMode.IGNORE);
			String sql = "UPDATE MannualTestSuites SET isDeleted='Y', updatedDate=NOW(), updatedBy=:updatedByEmail WHERE testSuitesId=:suitesid AND testSuitesName=:suitesName";
			Query query = session.createQuery(sql);
			query.setParameter("updatedByEmail", updatedByEmail);
			query.setParameter("suitesid",testsuitesid);
			query.setParameter("suitesName",testsuitesname);
			query.executeUpdate();
//			tx.commit();
			msg="Success";
		}
		catch(Exception e)
		{
			e.printStackTrace();
			msg="Fail";
		}
		return msg;
	}
	public Long getProjecttestsuites(int projectid) {
		Session session=entityManager.unwrap(Session.class);
		Long testsuite = null;
		List testsuites=new ArrayList<String>();
		String sql = "select count(*) as totaltestsuites from Mannual_TestSuites_Table WHERE projectId=:projectid AND isDeleted='N'";
		try {
			Query query = session.createNativeQuery(sql);
			query.setParameter("projectid", projectid);
			testsuites = query.list();
			testsuite=Long.valueOf(testsuites.get(0).toString());
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return testsuite;
	}
	public Long getProjecttestcases(int projectid) {
		Session session=entityManager.unwrap(Session.class);
		Long testsuite = null;
		List testsuites=new ArrayList<String>();
		String sql = "select count(*) from Mannual_TestCase_Table e inner join Mannual_TestGroup_Table f on \r\n" + 
				"				f.testGroupId=e.testGroupId inner join Mannual_TestSuites_Table l \r\n" + 
				"				on f.testSuitesId=l.testSuitesId  and isDeleted='N' where l.projectId=:projectid";
		try {
			Query query = session.createNativeQuery(sql);
			query.setParameter("projectid", projectid);
			testsuites = query.list();
			testsuite=Long.valueOf(testsuites.get(0).toString());
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return testsuite;
	}
	
	public List<ProjectDeatilsMeta> getAllProjectForManager(String userid) {
		List<Object[]> list=null;
		List<ProjectDeatilsMeta> projectDetails=new ArrayList<ProjectDeatilsMeta>();
		Session session=entityManager.unwrap(Session.class);
		session.setCacheMode(CacheMode.IGNORE);
		session.flush();
		session.clear();
		Query qry=null;
		System.out.println("----------overViewProjectDetails-------"+userid);
		String overViewProjectHQL="select pm.projectID,pm.createdBy,pm.createdTime,pm.isActive,pm.projectName,pm.projectStartDate,pm.projectEndDate\r\n" + 
				"from UserDetailsEntity uf inner join ProjectAccessTable ps\r\n" + 
				"on uf.id=ps.userDetailsId inner join\r\n" + 
				"Project_MetaTable pm on pm.projectID=ps.projectID\r\n" + 
				"WHERE uf.id=:userid\r\n" + 
				"AND ps.AccessType = 'Manager'  \r\n" + 
				"order by ps.projectID";
		qry = session.createNativeQuery(overViewProjectHQL);
		qry.setParameter("userid",userid);
	
		try {
			 list=qry.list();
			for (Object[] objects : list) {
				ProjectDeatilsMeta details=new ProjectDeatilsMeta();
				details.setProjectID((Integer) objects[0]);
				details.setCreatedBy((String) objects[1]);
				details.setCreatedTime((Date) objects[2]);
				details.setIsActive((String) objects[3]);
				details.setProjectName((String) objects[4]);
				details.setProjectStartDate((Date) objects[5]);	
				details.setProjectEndDate((Date) objects[6]);
				
				projectDetails.add(details);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	/*
		 * finally { session.close(); }
		 */
		return projectDetails; 
		
	}
	
	public String[] getProjectrun(int projectid) {
		Session session=entityManager.unwrap(Session.class);
		Long testsuite = null;
		String[]  Array= new String[3];
		List<Object[]> list=null;
		String sql = "select count(*) as totalRun ,t.projectName from MannualReRunTable r inner join Project_MetaTable t \r\n" + 
				"on r.projectId=t.projectID where r.projectId=:projectid";
		try {
			Query query = session.createNativeQuery(sql);
			query.setParameter("projectid", projectid);
			list = query.list();
			for (Object[] objects : list) {
				try {
				Array[0]=objects[0].toString();
				Array[1]=objects[1]==null?null:objects[1].toString();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}//for
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return Array;
	}
	public Long getProjectcurrentrun(int projectid) {
		Session session=entityManager.unwrap(Session.class);
		Long testsuite = null;
		List testsuites=new ArrayList<String>();
		String sql = "select count(*) as totalRun from MannualReRunTable where projectId=:projectid and isActive='Y' and status='Running'";
		try {
			Query query = session.createNativeQuery(sql);
			query.setParameter("projectid", projectid);
			testsuites = query.list();
			testsuite=Long.valueOf(testsuites.get(0).toString());
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return testsuite;
	}
	public Long getProjecttotalexecuted(int projectid) {
		Session session=entityManager.unwrap(Session.class);
		Long testsuite = null;
		List testsuites=new ArrayList<String>();
		String sql = "select count(*) as totalexecuted from MannualTestCaseExecutionLogs mt where mt.projectId=:projectid and \r\n" + 
				"mt.testCaseAction='Completed'";
		try {
			Query query = session.createNativeQuery(sql);
			query.setParameter("projectid", projectid);
			testsuites = query.list();
			testsuite=Long.valueOf(testsuites.get(0).toString());
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return testsuite;
	}
	public Long getprojecttotalpass(int projectid) {
		Session session=entityManager.unwrap(Session.class);
		Long testsuite = null;
		List testsuites=new ArrayList<String>();
		String sql = "select count(*) as totalpass from MannualTestCaseExecutionLogs mt where mt.projectId=:projectid and \r\n" + 
				"mt.testCaseAction='Completed' and mt.status='Pass'";
		try {
			Query query = session.createNativeQuery(sql);
			query.setParameter("projectid", projectid);
			testsuites = query.list();
			testsuite=Long.valueOf(testsuites.get(0).toString());
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return testsuite;
	}
	public String deletegroup(Integer groupId) {
		Session session=entityManager.unwrap(Session.class);
		Transaction tx=session.beginTransaction();
		String msg="Fail";
		try
		{
			session.setCacheMode(CacheMode.IGNORE);
		String sql = "delete from MannualTestGroup where testGroupId=:groupId";
		Query query = session.createNativeQuery(sql);
		query.setParameter("groupId",groupId);
		query.executeUpdate();
		tx.commit();
		msg="Success";
		}
		catch(Exception e)
		{
			e.printStackTrace();
			msg="Fail";
		}
		return msg;
	}
	public String deletetestruncaseid(Integer testcaseid) {
		Session session=entityManager.unwrap(Session.class);
		Transaction tx=session.beginTransaction();
		String msg="Fail";
		try
		{
		session.setCacheMode(CacheMode.IGNORE);
		String sql = "delete from MannualTestCaseExecutionLogs where id=:testcaseid";
		Query query = session.createNativeQuery(sql);
		query.setParameter("testcaseid",testcaseid);
		query.executeUpdate();
		tx.commit();
		msg="Success";
		}
		catch(Exception e)
		{
			e.printStackTrace();
			msg="Fail";
		}
		return msg;
	}
	public List<ProjectAccessEntity> getShowProjectDetails(String projectid) {
		Session session=entityManager.unwrap(Session.class);
		List<ProjectAccessEntity> ue=new ArrayList<>();
		List<Object[]> list=new ArrayList<>();
		try
		{
		String sql ="select uf.userName,uf.name, ps.AccessType,ps.Access_Given_By,ps.Access_Date from\r\n" + 
				" UserDetailsEntity uf inner join ProjectAccessTable ps\r\n" + 
				"on uf.id=ps.userDetailsId inner join \r\n" + 
				"Project_MetaTable pm on pm.projectID=ps.projectID where pm.projectID=:projectid order by ps.projectID;"; 
				
				/*"select ude.username,ude.name,pat.AccessType,pat.Access_Given_By,pat.Access_Date from UserDetailsEntity ude, ProjectAccessTable pat,Project_MetaTable pmt\r\n" + 
				"						where pat.userDetailsId=ude.id and pmt.projectID=pat.projectID  and \r\n" + 
				"								ude.id In (select pat.userDetailsId from ProjectAccessTable pat, Project_MetaTable pmt where pmt.projectID=pat.projectID  and pmt.projectID=:projectid) \r\n" + 
				"                                group by ude.username ORDER BY AccessType DESC;\r\n" + 
				"";*/
		Query query = session.createNativeQuery(sql);
		query.setParameter("projectid",projectid);
		

		ue=query.list();
		
		/*for (Object[] objects : list)
		{	userRole user=new userRole();
			user.setName(objects[1].toString());
			user.setUsername(objects[0].toString());
			user.setUserid(objects[2].toString());
			ue.add(user);
		}
		*/

		
		}
		catch(Exception e)
		{
			e.printStackTrace();
			
		}
		return ue;
	}
	
	
	public List<ProjectEntitlement> getAllUserNames(String projectid) {
		Session session=entityManager.unwrap(Session.class);
		List<ProjectEntitlement> ue=new ArrayList<>();
		List<Object[]> list=new ArrayList<>();
		try
		{
		String sql = "select ude.username,ude.name,pat.AccessType,pat.userDetailsId from UserDetailsEntity ude, ProjectAccessTable pat\r\n" + 
				",Project_MetaTable pmt\r\n" + 
				"where pat.userDetailsId=ude.id and pmt.projectID=pat.projectID  and\r\n" + 
				"ude.id In (select pat.userDetailsId from ProjectAccessTable pat, Project_MetaTable pmt\r\n" + 
				"where pmt.projectID=pat.projectID  and pmt.projectID=:projectid) group by ude.username;";
		Query query = session.createNativeQuery(sql);
		query.setParameter("projectid",projectid);
		
		list=query.list();
		for (Object[] objects : list)
		{	ProjectEntitlement user=new ProjectEntitlement();
			user.setName(objects[1].toString());
			user.setUsername(objects[0].toString());
			user.setUserid(objects[2].toString());
			ue.add(user);
		}
		
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
			
		}
		return ue;
	}
	
}

