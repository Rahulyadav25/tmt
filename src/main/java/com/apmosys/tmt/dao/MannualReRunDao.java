package com.apmosys.tmt.dao;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.map.HashedMap;
import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.BO.MannualReRunDetailsByRnd;
import com.apmosys.tmt.BO.ResourceDateWiseExecution;
import com.apmosys.tmt.models.MannualReRunTable;
import com.apmosys.tmt.models.MannualRunTable;
import com.apmosys.tmt.repository.MannualReRunRepository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Repository
@Transactional
public class MannualReRunDao {
	@PersistenceContext
    private EntityManager entityManager;
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	MannualReRunRepository reRunRepository;

	public Integer getCurrentRoundCount(Integer rerunID) {
		Integer roundCount=null;

		try {
			MannualReRunTable runDetails=reRunRepository.findById(rerunID).orElse(null);
			roundCount=runDetails.getRound();
		}
		catch(Exception e) {
			e.printStackTrace();
		}

		return roundCount;
	}

	public MannualReRunTable createRun(MannualReRunTable runDetails) {
		return reRunRepository.save(runDetails);
	}
	public MannualReRunTable stopActiveRun(MannualReRunTable run) {
		MannualReRunTable runDetails=reRunRepository.findById(run.getId()).orElse(null);
		runDetails.setStatus(run.getStatus());
		runDetails.setUpdatedDate(new Date());
		runDetails.setActualEndDate(new Date());
		runDetails.setIsActive(run.getIsActive());
		System.out.println("is active dao   "+runDetails.getIsActive());
		return reRunRepository.save(runDetails);
	}
	public Integer deleteActiveRun(Integer runId) {
		Integer i=0;
		try {
			reRunRepository.deleteRun(runId);
			i=1;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}
	public List<MannualReRunTable> getRundelaystatus(Integer pId) {
		List<Object[]> list=null;
		int hours;
		int days; 
		int restMinutes;
		int onedayMinutes = 480;
		String delay="";
		String date=null;
		System.out.println("11.....");  
		List<MannualReRunTable> completedRunList = new ArrayList<MannualReRunTable>();
		String overViewProjectHQL ="SELECT mr.id,mr.projectId,mr.testSuitesId,mr.runName ,mr.startDate,mr.targetEndDate ,mr.actualEndDate,mr.STATUS, mr.isActive,mr.createdBy \r\n" + 
				"				,mr.updatedBy,mr.createdDate ,mr.updatedDate,mr.description,(\r\n" + 
				"				SELECT u.name\r\n" + 
				"				FROM UserDetailsEntity u\r\n" + 
				"				WHERE u.id=mr.createdBy) AS createName ,count(IF(mt.testCaseAction != 'null', 1, NULL)) as totalcases,\r\n" + 
				"				count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,\r\n" + 
				"				count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',\r\n" + 
				"				count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',\r\n" + 
				"				count(IF(mt.testCaseAction = 'On Hold', 1, NULL))+count(IF(mt.testCaseAction = 'Completed', 1, NULL))\r\n" + 
				"				+count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as totalExecuted,\r\n" + 
				"				count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started',\r\n" + 
				"                count(IF(mt.testCaseAction = 'N/A', 1, NULL)) as 'NA',\r\n" + 
				"				count(IF(mt.status = 'Pass' && mt.testCaseAction = 'Completed', 1, NULL)) as 'Pass',\r\n" + 
				"				 count(IF(mt.status = 'Fail' && mt.testCaseAction = 'Completed', 1, NULL)) as 'Fail',\r\n" + 
				"				sum(mt.executionTime) as Total_secs,TIMESTAMPDIFF(MINUTE,mr.targetEndDate,NOW()) AS minute,mr.ROUND,mr.runId,\r\n" +
				"               count(IF(mt.testCaseAction = 'Incomplete', 1, NULL)) as 'totalactionincomplete'\r\n" + 
				"				FROM MannualReRunTable mr LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mr.id \r\n" + 
				"				WHERE mr.projectId=:projectId AND  mr.isActive=:isActive and mr.status=:status group BY mr.id";
		/*"SELECT mr.id,mr.projectId,mr.testSuitesId,mr.runName ,mr.startDate,mr.targetEndDate ,mr.actualEndDate,mr.STATUS, mr.isActive,mr.createdBy \r\n" + 
				",mr.updatedBy,mr.createdDate ,mr.updatedDate,mr.description,(\r\n" + 
				"SELECT u.name\r\n" + 
				"FROM UserDetailsEntity u\r\n" + 
				"WHERE u.id=mr.createdBy) AS createName ,count(IF(mt.testCaseAction != '', 1, NULL)) as totalcases,\r\n" + 
				"count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,\r\n" + 
				"count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',\r\n" + 
				"count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',\r\n" + 
				"count(IF(mt.testCaseAction = 'On Hold', 1, NULL))+count(IF(mt.testCaseAction = 'Completed', 1, NULL))\r\n" + 
				"+count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as totalExecuted,\r\n" + 
				"count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started',\r\n" +
				"count(IF(mt.status = 'Pass' && mt.testCaseAction = 'Completed', 1, NULL)) as 'Pass',\r\n" + 
				" count(IF(mt.status = 'Fail' && mt.testCaseAction = 'Completed', 1, NULL)) as 'Fail',\r\n" + 
				"sum(mt.executionTime) as Total_secs,TIMESTAMPDIFF(MINUTE,mr.targetEndDate,NOW()) AS minute,mr.ROUND,mr.runId\r\n" + 
				" FROM MannualReRunTable mr LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mr.id "+
				"WHERE mr.projectId=:projectId AND  mr.isActive=:isActive and mr.status=:status group BY mr.id";*/

		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectId", pId);
			query.setParameter("isActive", "Y");
			query.setParameter("status", "Running");
			list = query.list();
			System.out.println("new doa");
			for (Object[] objects : list) {
				MannualReRunTable runDetails=new MannualReRunTable();
				runDetails.setId((Integer) objects[0]);
				runDetails.setTestSuitesId( (Integer) objects[2]);
				runDetails.setRunName((String) objects[3]);
				runDetails.setStartDate((String) objects[4]);
				runDetails.setRound((Integer) objects[26]);
				runDetails.setRunId((Integer) objects[27]);
//				runDetails.setTotalactionpass((BigInteger) objects[28]);
				runDetails.setTotalactionpass( (BigInteger.valueOf((long) objects[28])) );
				if(objects[5]!=null)
					runDetails.setTargetEndDate(objects[5].toString().substring(0,19));	
				runDetails.setStatus((String) objects[7]);
				runDetails.setIsActive((String) objects[8]);
				if(objects[11]!=null)
					runDetails.setCreatedDate((Date)objects[11]);
				runDetails.setUpdatedDate((Date) objects[12]);			
				runDetails.setDescription((String) objects[13]);
				runDetails.setCreatedBy((String) objects[14]);
//				if(objects[15]!=null)
//					runDetails.setTestCaseNo((BigInteger) objects[15]);
//				if(objects[16]!=null)
//					runDetails.setCompleted((BigInteger) objects[16]);
//				if(objects[17]!=null)
//					runDetails.setOnHold((BigInteger) objects[17]);
//				if(objects[18]!=null)
//					runDetails.setUnderReview((BigInteger) objects[18]);
//				if(objects[19]!=null)
//					runDetails.setTotalExecuted((BigInteger) objects[19]);
//				if(objects[20]!=null)
//					runDetails.setNotStarted((BigInteger) objects[20]);
//				if(objects[21]!=null)
//					runDetails.setNA((BigInteger) objects[21]);
//				runDetails.setPass((BigInteger) objects[22]);
//				runDetails.setFail((BigInteger) objects[23]);
				
				//new added
				if(objects[15]!=null)
					runDetails.setTestCaseNo((BigInteger.valueOf((long) objects[15])));
				if(objects[16]!=null)
					runDetails.setCompleted((BigInteger.valueOf((long) objects[16])));
				if(objects[17]!=null)
					runDetails.setOnHold((BigInteger.valueOf((long) objects[17])));
				if(objects[18]!=null)
					runDetails.setUnderReview((BigInteger.valueOf((long) objects[18])));
				if(objects[19]!=null)
					runDetails.setTotalExecuted((BigInteger.valueOf((long) objects[19])));
				if(objects[20]!=null)
					runDetails.setNotStarted((BigInteger.valueOf((long) objects[20])));
				if(objects[21]!=null)
					runDetails.setNA((BigInteger.valueOf((long) objects[21])));
				runDetails.setPass((BigInteger.valueOf((long) objects[22])));
				runDetails.setFail((BigInteger.valueOf((long) objects[23])));
				if(objects[22]!=null)
					runDetails.setTimeTake((Double) objects[24]);
				if(objects[25]!=null)
				{
					int minutes=Integer.valueOf(objects[25].toString());
					if(minutes<0)
					{
						delay="in time";
					}
					else
					{
						if(minutes<60)
						{
							/*delay=" "+minutes+"-minutes";
						runDetails.setDelayby(delay);*/
							delay="in time";
						}
						else if(minutes > 60 && minutes < onedayMinutes)
						{

							/*hours = (int) Math.floor(minutes/60);
				        restMinutes = minutes%60;
				        delay=" "+hours+"-hours "+restMinutes+"-minutes";*/
							delay="in time";
						}
						else{	
							days = (int) Math.floor((minutes/60)/24);
							if(days==0)
							{
								delay="in time";
							}
							else
							{
								restMinutes = minutes % onedayMinutes;
								hours = (int) Math.floor(restMinutes/60); 
								restMinutes = restMinutes % 60;
								delay= " "+days+"-days ";
							}

						}
					}
				}
				runDetails.setDelayby(delay);
				/*	runDetails.setDescription((String) objects[14]);
				runDetails.setCreatedBy((String) objects[14]);*/
				completedRunList.add(runDetails);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return completedRunList;
	}
	public List<MannualReRunTable> getInactiveRun(Integer pId) {
		List<Object[]> list=null;
		int hours;
		int days; 
		int restMinutes;
		int onedayMinutes = 480;
		String delay="";
		List<MannualReRunTable> completedRunList = new ArrayList<MannualReRunTable>();
		String overViewProjectHQL = "SELECT mr.id,mr.projectId,mr.testSuitesId,mr.runName ,mr.startDate,mr.targetEndDate ,mr.actualEndDate,mr.STATUS, mr.isActive,mr.createdBy \r\n" + 
				",mr.updatedBy,mr.createdDate ,mr.updatedDate,mr.description,(\r\n" + 
				"SELECT u.name\r\n" + 
				"FROM UserDetailsEntity u\r\n" + 
				"WHERE u.id=mr.createdBy) AS createName ,\r\n" + 
				"COUNT(IF(mt.testCaseAction != '', 1, NULL)) as totalcases,\r\n" + 
				"count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,\r\n" + 
				"count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',\r\n" + 
				"count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',\r\n" + 
				"count(IF(mt.testCaseAction = 'On Hold', 1, NULL))+count(IF(mt.testCaseAction = 'Completed', 1, NULL))\r\n" + 
				"+count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as totalExecuted,\r\n" + 
				"count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started',\r\n" + 
				"count(IF(mt.testCaseAction = 'N/A', 1, NULL)) as 'NA',\r\n" + 
				"count(IF(mt.status = 'Pass' && mt.testCaseAction = 'Completed', 1, NULL)) as 'Pass',\r\n" + 
				" count(IF(mt.status = 'Fail' && mt.testCaseAction = 'Completed', 1, NULL)) as 'Fail',\r\n" + 
				" sum(mt.executionTime) as Total_secs,TIMESTAMPDIFF(MINUTE,mr.targetEndDate,NOW()) AS minute,mr.ROUND,mr.runId,"
				+ "COUNT(IF(mt.testCaseAction = 'Incomplete', 1, NULL)) AS totalincomplte\r\n" + 
				" FROM MannualReRunTable mr LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mr.id \r\n" + 
				" WHERE mr.projectId=:projectId AND  mr.isActive=:isActive group BY mr.id";
		/*"SELECT mr.id,mr.projectId,mr.testSuitesId,mr.runName ,mr.startDate,mr.targetEndDate ,mr.actualEndDate,mr.STATUS, mr.isActive,mr.createdBy \r\n" + 
				",mr.updatedBy,mr.createdDate ,mr.updatedDate,mr.description,(\r\n" + 
				"SELECT u.name\r\n" + 
				"FROM UserDetailsEntity u\r\n" + 
				"WHERE u.id=mr.createdBy) AS createName ,COUNT(IF(mt.testCaseAction != '', 1, NULL)) as totalcases,\r\n" + 
				"count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,\r\n" + 
				"count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',\r\n" + 
				"count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',\r\n" + 
				"count(IF(mt.testCaseAction = 'On Hold', 1, NULL))+count(IF(mt.testCaseAction = 'Completed', 1, NULL))\r\n" + 
				"+count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as totalExecuted,\r\n" + 
				"count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started',\r\n" + 
				"count(IF(mt.status = 'Pass' && mt.testCaseAction = 'Completed', 1, NULL)) as 'Pass',\r\n" + 
				" count(IF(mt.status = 'Fail' && mt.testCaseAction = 'Completed', 1, NULL)) as 'Fail',\r\n" + 
				"sum(mt.executionTime) as Total_secs,TIMESTAMPDIFF(MINUTE,mr.targetEndDate,NOW()) AS minute,mr.ROUND,mr.runId\r\n" + 
				" FROM MannualReRunTable mr LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mr.id "+
				"WHERE mr.projectId=:projectId AND  mr.isActive=:isActive group BY mr.id";*/

		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectId", pId);
			query.setParameter("isActive", "N");
			/*	query.setParameter("status", "Running");*/
			list = query.list();
			for (Object[] objects : list) {
				MannualReRunTable runDetails=new MannualReRunTable();
				runDetails.setId((Integer) objects[0]);
				runDetails.setTestSuitesId( (Integer) objects[2]);
				runDetails.setRunName((String) objects[3]);
				runDetails.setStartDate((String) objects[4]);
				runDetails.setRound((Integer) objects[26]);

				runDetails.setRunId((Integer) objects[27]);
				if(objects[5]!=null)
					runDetails.setTargetEndDate(objects[5].toString());	
				runDetails.setStatus((String) objects[7]);
				runDetails.setIsActive((String) objects[8]);
				if(objects[11]!=null)
					runDetails.setCreatedDate((Date)objects[11]);
				runDetails.setUpdatedDate((Date) objects[12]);			
				runDetails.setDescription((String) objects[13]);
				runDetails.setCreatedBy((String) objects[14]);
				if(objects[15]!=null)
//					runDetails.setTestCaseNo((BigInteger) objects[15]);
				runDetails.setTestCaseNo((BigInteger.valueOf((long) objects[15])));
				if(objects[16]!=null)
					runDetails.setCompleted((BigInteger.valueOf((long) objects[16])));
				if(objects[17]!=null)
					runDetails.setOnHold((BigInteger.valueOf((long) objects[17])));
				if(objects[18]!=null)
					runDetails.setUnderReview((BigInteger.valueOf((long) objects[18])));
				if(objects[19]!=null)
					runDetails.setTotalExecuted((BigInteger.valueOf((long) objects[19])));
				if(objects[20]!=null)
					runDetails.setNotStarted((BigInteger.valueOf((long) objects[20])));
				if(objects[21]!=null)
					runDetails.setNA((BigInteger.valueOf((long) objects[21])));
				runDetails.setPass((BigInteger.valueOf((long) objects[22])));
				runDetails.setFail((BigInteger.valueOf((long) objects[23])));
				if(objects[22]!=null)
					runDetails.setTimeTake((Double) objects[24]);

				if(objects[25]!=null)
				{
					int minutes=Integer.valueOf(objects[25].toString());
					if(minutes<0)
					{
						delay="in time";
					}
					else
					{
						if(minutes<60)
						{
							/*delay=" "+minutes+"-minutes";
						runDetails.setDelayby(delay);*/
							delay="in time";
						}
						else if(minutes > 60 && minutes < onedayMinutes)
						{

							/*hours = (int) Math.floor(minutes/60);
				        restMinutes = minutes%60;
				        delay=" "+hours+"-hours "+restMinutes+"-minutes";*/
							delay="in time";
						}
						else{	
							days = (int) Math.floor((minutes/60)/24);
							if(days==0)
							{
								delay="in time";
							}
							else
							{
								restMinutes = minutes % onedayMinutes;
								hours = (int) Math.floor(restMinutes/60); 
								restMinutes = restMinutes % 60;
								delay= " "+days+"-days ";
							}

						}
					}
				}
				if(objects[28]!=null)
//					runDetails.setTotalimcomete((BigInteger) objects[28]);
				runDetails.setTotalimcomete((BigInteger.valueOf((long) objects[28])));
				runDetails.setDelayby(delay);
				/*	runDetails.setDescription((String) objects[14]);
				runDetails.setCreatedBy((String) objects[14]);*/
				completedRunList.add(runDetails);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return completedRunList;
	}
	public List<MannualReRunTable> getActiveRun(Integer pId) {
		List<Object[]> list=null;
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		List<MannualReRunTable> activeRunList = new ArrayList<MannualReRunTable>();
		String overViewProjectHQL ="SELECT mre.id,mr.projectId,mr.testSuitesId,mre.runName,mre.startDate,mre.targetEndDate,mre.actualEndDate,mre.STATUS, mre.isActive,mre.createdBy \r\n" + 
				",mre.updatedBy,mre.createdDate,mre.updatedDate,mre.description,(\r\n" + 
				"SELECT u.name\r\n" + 
				"FROM UserDetailsEntity u\r\n" + 
				"WHERE u.id=mr.createdBy) AS createName, COUNT(IF(mt.testCaseAction != '', 1, NULL)) AS totalcases, COUNT(IF(mt.testCaseAction = 'Completed', 1, NULL)) AS Completed, COUNT(IF(mt.testCaseAction = 'On Hold', 1, NULL)) AS 'On Hold', COUNT(IF(mt.testCaseAction = 'Under Review', 1, NULL)) AS 'Under Review', COUNT(IF(mt.testCaseAction = 'On Hold', 1, NULL))+ COUNT(IF(mt.testCaseAction = 'Completed', 1, NULL))\r\n" + 
				"+ COUNT(IF(mt.testCaseAction = 'Under Review', 1, NULL)) AS totalExecuted, COUNT(IF(mt.testCaseAction = 'Not Started', 1, NULL)) AS 'Not Started', COUNT(IF(mt.status = 'Pass' && mt.testCaseAction = 'Completed', 1, NULL)) AS 'Pass', COUNT(IF(mt.status = 'Fail' && mt.testCaseAction = 'Completed', 1, NULL)) AS 'Fail', SUM(mt.executionTime) AS Total_secs\r\n" + 
				",mre.round,mre.runId,COUNT(IF(mt.testCaseAction = 'Pass', 1, NULL)) AS totalactionpass FROM MannualRun_Table mr LEFT JOIN MannualReRunTable mre ON mr.id=mre.runId\r\n" + 
				"LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mre.id\r\n" + 
				"WHERE mr.projectId=:projectId AND mr.isActive=:isActive AND mre.STATUS=:status\r\n" + 
				"GROUP BY mre.id order by mre.id desc";
		Session session=entityManager.unwrap(Session.class);
		try {
			System.out.println("new dao");
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectId", pId);
			query.setParameter("isActive", "Y");
			query.setParameter("status", "Running");
			list = query.list();
			for (Object[] objects : list) {
				MannualReRunTable runDetails=new MannualReRunTable();
				runDetails.setId((Integer) objects[0]);
				runDetails.setTestSuitesId( (Integer) objects[2]);
				runDetails.setRunName((String) objects[3]);
				runDetails.setStartDate((String) objects[4]);
				if(objects[5]!=null)
					runDetails.setTargetEndDate(objects[5].toString());	
				if(objects[11]!=null)
					runDetails.setCreatedDate((Date) objects[11]);
				runDetails.setUpdatedDate( (Date) objects[12]);				
				runDetails.setDescription((String) objects[13]);
				runDetails.setCreatedBy((String) objects[14]);
				if(objects[15]!=null)
//					runDetails.setTestCaseNo((BigInteger) objects[15]);
				runDetails.setTestCaseNo((BigInteger.valueOf((long) objects[15])));
				if(objects[16]!=null)
//					runDetails.setCompleted((BigInteger) objects[16]);
				runDetails.setCompleted((BigInteger.valueOf((long) objects[16])));
				if(objects[17]!=null)
//					runDetails.setOnHold((BigInteger) objects[17]);
				runDetails.setOnHold((BigInteger.valueOf((long) objects[17])));
				if(objects[18]!=null)
//					runDetails.setUnderReview((BigInteger) objects[18]);
				runDetails.setUnderReview((BigInteger.valueOf((long) objects[18])));

				if(objects[19]!=null)
//					runDetails.setTotalExecuted((BigInteger) objects[19]);
				runDetails.setTotalExecuted((BigInteger.valueOf((long) objects[19])));
				if(objects[20]!=null)
//					runDetails.setNotStarted((BigInteger) objects[20]);
				runDetails.setNotStarted((BigInteger.valueOf((long) objects[20])));
				runDetails.setPass((BigInteger.valueOf((long) objects[21])));
				runDetails.setFail((BigInteger.valueOf((long) objects[22])));
				if(objects[21]!=null)
					runDetails.setTimeTake((Double) objects[23]);
				runDetails.setStatus((String) objects[7]);
				runDetails.setIsActive((String) objects[8]);
				runDetails.setRound((Integer) objects[24]);
				runDetails.setRunId((Integer) objects[25]);
//				runDetails.setTotalactionpass((BigInteger) objects[26]);
				runDetails.setTotalactionpass((BigInteger.valueOf((long) objects[26])));
				activeRunList.add(runDetails);
				//System.out.println(runDetails);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return activeRunList;

	}


	public Map<String, MannualReRunDetailsByRnd> getCompletedRunNew(Integer pId) {
		List<Object[]> list=null;
		String date=null;
		List<MannualReRunTable> completedRunList = new ArrayList<MannualReRunTable>();
		Map<String, MannualReRunDetailsByRnd> maps = new HashedMap<String, MannualReRunDetailsByRnd>();

		long mStrippedValue=0;
		long mStrippedValues =0;
		long mStrippedValuess =0;

		String overViewProjectHQL = "SELECT mre.id,mr.projectId,mre.testSuitesId,mr.runName,mre.startDate,mre.targetEndDate,mre.actualEndDate,mre.STATUS, mre.isActive,mre.createdBy \r\n" + 
				",mre.updatedBy,mre.createdDate,mre.updatedDate,mre.description,(\r\n" + 
				"SELECT u.name\r\n" + 
				"FROM UserDetailsEntity u\r\n" + 
				"WHERE u.id=mr.createdBy) AS createName, COUNT(IF(mt.testCaseAction != '', 1, NULL)) AS totalcases, COUNT(IF(mt.testCaseAction = 'Completed', 1, NULL)) AS Completed, COUNT(IF(mt.testCaseAction = 'On Hold', 1, NULL)) AS 'On Hold', COUNT(IF(mt.testCaseAction = 'Under Review', 1, NULL)) AS 'Under Review', COUNT(IF(mt.testCaseAction = 'On Hold', 1, NULL))+ COUNT(IF(mt.testCaseAction = 'Completed', 1, NULL))\r\n" + 
				"+ COUNT(IF(mt.testCaseAction = 'Under Review', 1, NULL)) AS totalExecuted, COUNT(IF(mt.testCaseAction = 'Not Started', 1, NULL)) AS 'Not Started',COUNT(IF(mt.testCaseAction = 'N/A', 1, NULL)) AS 'NA', COUNT(IF(mt.status = 'Pass' && mt.testCaseAction = 'Completed', 1, NULL)) AS 'Pass', COUNT(IF(mt.status = 'Fail' && mt.testCaseAction = 'Completed', 1, NULL)) AS 'Fail', SUM(mt.executionTime) AS Total_secs\r\n" + 
				",mre.round,mre.runId,COUNT(IF(mt.testCaseAction = 'Pass', 1, NULL)) AS totalactionpass FROM MannualRun_Table mr LEFT JOIN MannualReRunTable mre ON mr.id=mre.runId\r\n" + 
				"LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mre.id\r\n" + 
				"WHERE mr.projectId=:projectId AND mr.isActive=:isActive AND mre.STATUS=:status\r\n" + 
				"GROUP BY mre.id order by mre.id desc";
		/*"SELECT mre.id,mr.projectId,mre.testSuitesId,mr.runName,mre.startDate,mre.targetEndDate,mre.actualEndDate,mre.STATUS, mre.isActive,mre.createdBy \r\n" + 
				",mre.updatedBy,mre.createdDate,mre.updatedDate,mre.description,(\r\n" + 
				"SELECT u.name\r\n" + 
				"FROM UserDetailsEntity u\r\n" + 
				"WHERE u.id=mr.createdBy) AS createName, COUNT(IF(mt.testCaseAction != '', 1, NULL)) AS totalcases, COUNT(IF(mt.testCaseAction = 'Completed', 1, NULL)) AS Completed, COUNT(IF(mt.testCaseAction = 'On Hold', 1, NULL)) AS 'On Hold', COUNT(IF(mt.testCaseAction = 'Under Review', 1, NULL)) AS 'Under Review', COUNT(IF(mt.testCaseAction = 'On Hold', 1, NULL))+ COUNT(IF(mt.testCaseAction = 'Completed', 1, NULL))\r\n" + 
				"+ COUNT(IF(mt.testCaseAction = 'Under Review', 1, NULL)) AS totalExecuted, COUNT(IF(mt.testCaseAction = 'Not Started', 1, NULL)) AS 'Not Started', COUNT(IF(mt.status = 'Pass' && mt.testCaseAction = 'Completed', 1, NULL)) AS 'Pass', COUNT(IF(mt.status = 'Fail' && mt.testCaseAction = 'Completed', 1, NULL)) AS 'Fail', SUM(mt.executionTime) AS Total_secs\r\n" + 
				",mre.round,mre.runId FROM MannualRun_Table mr LEFT JOIN MannualReRunTable mre ON mr.id=mre.runId\r\n" + 
				"LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mre.id\r\n" + 
				"WHERE mr.projectId=:projectId AND mr.isActive=:isActive AND mre.STATUS=:status\r\n" + 
				"GROUP BY mre.id order by mre.id desc";*/
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectId", pId);
			query.setParameter("isActive", "Y");
			query.setParameter("status", "Completed");
			list = query.list();
			for (Object[] objects : list) {
				MannualReRunTable runDetails=new MannualReRunTable();
				runDetails.setId((Integer) objects[0]);
				runDetails.setProjectId((Integer) objects[1]);
				runDetails.setTestSuitesId( (Integer) objects[2]);
				runDetails.setRunName((String) objects[3]);
				runDetails.setStartDate((String) objects[4]);
				if(objects[5]!=null)
					runDetails.setTargetEndDate(objects[5].toString().substring(0,19));
				runDetails.setActualEndDate((Date) objects[6]);
				if(objects[11]!=null)
					runDetails.setCreatedDate((Date) objects[11]);
				runDetails.setUpdatedDate((Date) objects[12]);				
				runDetails.setDescription((String) objects[13]);
				runDetails.setCreatedBy((String) objects[14]);
				if(objects[15]!=null)
					runDetails.setTestCaseNo((BigInteger.valueOf((long) objects[15])));
				if(objects[16]!=null)
					runDetails.setCompleted((BigInteger.valueOf((long) objects[16])));
				if(objects[17]!=null)
					runDetails.setOnHold((BigInteger.valueOf((long) objects[17])));
				if(objects[18]!=null)
					runDetails.setUnderReview((BigInteger.valueOf((long) objects[18])));
				if(objects[19]!=null)
					runDetails.setTotalExecuted((BigInteger.valueOf((long) objects[19])));
				if(objects[20]!=null)
					runDetails.setNotStarted((BigInteger.valueOf((long) objects[20])));
				if(objects[21]!=null)
					runDetails.setNA((BigInteger.valueOf((long) objects[21])));
				runDetails.setPass((BigInteger.valueOf((long) objects[22])));
				runDetails.setFail((BigInteger.valueOf((long) objects[23])));
				if(objects[22]!=null)
					runDetails.setTimeTake((Double) objects[24]);
				runDetails.setStatus((String) objects[7]);
				runDetails.setIsActive((String) objects[8]);
				runDetails.setRound((Integer) objects[25]);
				runDetails.setRunId((Integer) objects[26]);
				runDetails.setTotalactionpass((BigInteger.valueOf((long) objects[27])));
				completedRunList.add(runDetails);
				//System.out.println(runDetails);
			}

			for (MannualReRunTable completeRun : completedRunList) {
				if (completeRun != null) {
					MannualReRunDetailsByRnd reRunList=new MannualReRunDetailsByRnd();
					List<MannualReRunTable> values = completedRunList.stream()
							.filter(x -> x.getRunId().equals(completeRun.getRunId()))
							.collect(Collectors.toList());	
					reRunList.setRunName(completeRun.getRunName());
					if(values!=null) {
						reRunList.setTotalReRunCount(values.size());
						reRunList.setReRunList(values);
					}

					if(completeRun.getRunId()!=null)
						maps.put(completeRun.getRunId().toString(), reRunList);


					Double exexutiontime=completeRun.getTimeTake();
					if(exexutiontime!=null) {
						Double day = exexutiontime / (24 * 3600); 

						exexutiontime = exexutiontime % (24 * 3600); 
						Double hour = exexutiontime / 3600; 

						exexutiontime %= 3600; 
						Double minutes = exexutiontime / 60 ;

						System.out.println("day"+day+".....hour..."+hour+"...miniutes.."+minutes);
						if(day!=null)
						{
							//String dayscount=day.toString();
							mStrippedValue = new Double(day).longValue();
							//days=Integer.parseInt(dayscount);

						}
						if(hour !=null)
						{
							// String hourcount=hour.toString();
							mStrippedValues = new Double(hour).longValue();
							// hours=Integer.parseInt(hourcount);
						}
						if(minutes!=null)
						{
							//  String minutescount=minutes.toString();
							mStrippedValuess = new Double(minutes).longValue();
							//  minutess=Integer.parseInt(minutescount);
						}
					}//if(exexutiontime!=null) 
					
					
					String strd=Long.toString(mStrippedValue);
					String strh=Long.toString(mStrippedValues);
					String strm=Long.toString(mStrippedValuess);
					completeRun.setTimetakendhm(strd+"day: "+strh+"hrs: "+strm+"min");
					//				   System.out.println("day......"+mStrippedValue+"hor...."+mStrippedValues+"...minutes.remove."+mStrippedValuess);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		 * finally { session.close(); }
		 */
		return maps;
	}

	public Map<String, MannualReRunDetailsByRnd> getCompletedRunNew1(Integer pId) {
		List<Object[]> list=null;
		String date=null;
		List<MannualReRunTable> completedRunList = new ArrayList<MannualReRunTable>();
		Map<String, MannualReRunDetailsByRnd> maps = new HashedMap<String, MannualReRunDetailsByRnd>();
		String overViewProjectHQL ="SELECT mre.id,mr.projectId,mre.testSuitesId,mr.runName,mre.startDate,mre.targetEndDate,mre.actualEndDate,mre.STATUS, mre.isActive,mre.createdBy  \r\n" + 
				"				,mre.updatedBy,mre.createdDate,mre.updatedDate,mre.description,(\r\n" + 
				"				SELECT u.name\r\n" + 
				"				FROM UserDetailsEntity u\r\n" + 
				"				WHERE u.id=mr.createdBy) AS createName, COUNT(IF(mt.testCaseAction != '', 1, NULL)) AS totalcases, COUNT(IF(mt.testCaseAction = 'Completed', 1, NULL)) AS Completed, COUNT(IF(mt.testCaseAction = 'On Hold', 1, NULL)) AS 'On Hold', COUNT(IF(mt.testCaseAction = 'Under Review', 1, NULL)) AS 'Under Review', COUNT(IF(mt.testCaseAction = 'On Hold', 1, NULL))+ COUNT(IF(mt.testCaseAction = 'Completed', 1, NULL))\r\n" + 
				"				 ,COUNT(IF(mt.testCaseAction = 'Under Review', 1, NULL)) AS totalExecuted, COUNT(IF(mt.testCaseAction = 'Not Started', 1, NULL)) AS 'Not Started',COUNT(IF(mt.testCaseAction = 'N/A', 1, NULL)) AS 'NA', COUNT(IF(mt.status = 'Pass' && mt.testCaseAction = 'Completed', 1, NULL)) AS 'Pass', COUNT(IF(mt.status = 'Fail' && mt.testCaseAction = 'Completed', 1, NULL)) AS 'Fail', SUM(mt.executionTime) AS Total_secs \r\n" + 
				"				,mre.round,mre.runId,COUNT(IF(mt.testCaseAction = 'Pass', 1, NULL)) AS totalactionpass FROM MannualRun_Table mr LEFT JOIN MannualReRunTable mre ON mr.id=mre.runId\r\n" + 
				"				LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mre.id\r\n" + 
				"WHERE mr.projectId=:projectId AND mr.isActive=:isActive AND mre.STATUS=:status\r\n" + 
				"				GROUP BY mre.id order by mre.id desc;";
		/* "SELECT mre.id,mr.projectId,mre.testSuitesId,mr.runName,mre.startDate,mre.targetEndDate,mre.actualEndDate,mre.STATUS, mre.isActive,mre.createdBy \r\n" + 
				",mre.updatedBy,mre.createdDate,mre.updatedDate,mre.description,(\r\n" + 
				"SELECT u.name\r\n" + 
				"FROM UserDetailsEntity u\r\n" + 
				"WHERE u.id=mr.createdBy) AS createName, COUNT(IF(mt.testCaseAction != '', 1, NULL)) AS totalcases, COUNT(IF(mt.testCaseAction = 'Completed', 1, NULL)) AS Completed, COUNT(IF(mt.testCaseAction = 'On Hold', 1, NULL)) AS 'On Hold', COUNT(IF(mt.testCaseAction = 'Under Review', 1, NULL)) AS 'Under Review', COUNT(IF(mt.testCaseAction = 'On Hold', 1, NULL))+ COUNT(IF(mt.testCaseAction = 'Completed', 1, NULL))\r\n" + 
				"+ COUNT(IF(mt.testCaseAction = 'Under Review', 1, NULL)) AS totalExecuted, COUNT(IF(mt.testCaseAction = 'Not Started', 1, NULL)) AS 'Not Started',COUNT(IF(mt.testCaseAction = 'N/A', 1, NULL)) AS 'NA', COUNT(IF(mt.status = 'Pass' && mt.testCaseAction = 'Completed', 1, NULL)) AS 'Pass', COUNT(IF(mt.status = 'Fail' && mt.testCaseAction = 'Completed', 1, NULL)) AS 'Fail', SUM(mt.executionTime) AS Total_secs\r\n" + 
				",mre.round,mre.runId,COUNT(IF(mt.testCaseAction = 'Pass', 1, NULL)) AS totalactionpass FROM MannualRun_Table mr LEFT JOIN MannualReRunTable mre ON mr.id=mre.runId\r\n" + 
				"LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mre.id\r\n" + 
				"WHERE mr.projectId=:projectId AND mr.isActive=:isActive AND mre.STATUS=:status\r\n" + 
				"GROUP BY mre.id order by mre.id desc";*/
		/*"SELECT mre.id,mr.projectId,mre.testSuitesId,mr.runName,mre.startDate,mre.targetEndDate,mre.actualEndDate,mre.STATUS, mre.isActive,mre.createdBy \r\n" + 
				",mre.updatedBy,mre.createdDate,mre.updatedDate,mre.description,(\r\n" + 
				"SELECT u.name\r\n" + 
				"FROM UserDetailsEntity u\r\n" + 
				"WHERE u.id=mr.createdBy) AS createName, COUNT(IF(mt.testCaseAction != '', 1, NULL)) AS totalcases, COUNT(IF(mt.testCaseAction = 'Completed', 1, NULL)) AS Completed, COUNT(IF(mt.testCaseAction = 'On Hold', 1, NULL)) AS 'On Hold', COUNT(IF(mt.testCaseAction = 'Under Review', 1, NULL)) AS 'Under Review', COUNT(IF(mt.testCaseAction = 'On Hold', 1, NULL))+ COUNT(IF(mt.testCaseAction = 'Completed', 1, NULL))\r\n" + 
				"+ COUNT(IF(mt.testCaseAction = 'Under Review', 1, NULL)) AS totalExecuted, COUNT(IF(mt.testCaseAction = 'Not Started', 1, NULL)) AS 'Not Started', COUNT(IF(mt.status = 'Pass' && mt.testCaseAction = 'Completed', 1, NULL)) AS 'Pass', COUNT(IF(mt.status = 'Fail' && mt.testCaseAction = 'Completed', 1, NULL)) AS 'Fail', SUM(mt.executionTime) AS Total_secs\r\n" + 
				",mre.round,mre.runId FROM MannualRun_Table mr LEFT JOIN MannualReRunTable mre ON mr.id=mre.runId\r\n" + 
				"LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mre.id\r\n" + 
				"WHERE mr.projectId=:projectId AND mr.isActive=:isActive AND mre.STATUS=:status\r\n" + 
				"GROUP BY mre.id order by mre.id desc";*/
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectId", pId);
			query.setParameter("isActive", "Y");
			query.setParameter("status", "Completed");
			list = query.list();
			for (Object[] objects : list) {
				MannualReRunTable runDetails=new MannualReRunTable();
				runDetails.setId((Integer) objects[0]);
				runDetails.setProjectId((Integer) objects[1]);
				runDetails.setTestSuitesId( (Integer) objects[2]);
				runDetails.setRunName((String) objects[3]);
				runDetails.setStartDate((String) objects[4]);
				if(objects[5]!=null)
					runDetails.setTargetEndDate(objects[5].toString().substring(0,19));
				runDetails.setActualEndDate((Date) objects[6]);
				if(objects[11]!=null)
					runDetails.setCreatedDate((Date) objects[11]);
				runDetails.setUpdatedDate((Date) objects[12]);				
				runDetails.setDescription((String) objects[13]);
				runDetails.setCreatedBy((String) objects[14]);
				if(objects[15]!=null)
//					runDetails.setTestCaseNo((BigInteger) objects[15]);
				runDetails.setTestCaseNo((BigInteger.valueOf((long) objects[15])));
				if(objects[16]!=null)
//					runDetails.setCompleted((BigInteger) objects[16]);
				runDetails.setCompleted((BigInteger.valueOf((long) objects[16])));
				if(objects[17]!=null)
					runDetails.setOnHold((BigInteger.valueOf((long) objects[17])));
				if(objects[18]!=null)
					runDetails.setUnderReview((BigInteger.valueOf((long) objects[18])));
				if(objects[19]!=null)
					runDetails.setTotalExecuted((BigInteger.valueOf((long) objects[19])));
				if(objects[20]!=null)
					runDetails.setNotStarted((BigInteger.valueOf((long) objects[20])));
				if(objects[21]!=null)
					runDetails.setNA((BigInteger.valueOf((long) objects[21])));
				runDetails.setPass((BigInteger.valueOf((long) objects[22])));
				runDetails.setFail((BigInteger.valueOf((long) objects[23])));
				if(objects[24]!=null)
					runDetails.setTimeTake((Double) objects[24]);
				runDetails.setStatus((String) objects[7]);
				runDetails.setIsActive((String) objects[8]);
				runDetails.setRound((Integer) objects[25]);
				runDetails.setRunId((Integer) objects[26]);
				runDetails.setTotalactionpass((BigInteger.valueOf((long) objects[27])));
				completedRunList.add(runDetails);
				//System.out.println(runDetails);
			}

			for (MannualReRunTable completeRun : completedRunList) {
				if (completeRun != null) {
					MannualReRunDetailsByRnd reRunList=new MannualReRunDetailsByRnd();
					List<MannualReRunTable> values = completedRunList.stream()
							.filter(x -> x.getRunId().equals(completeRun.getRunId()))
							.collect(Collectors.toList());	
					reRunList.setRunName(completeRun.getRunName());
					if(values!=null) {
						reRunList.setTotalReRunCount(values.size());
						reRunList.setReRunList(values);
					}

					if(completeRun.getRunId()!=null)
						maps.put(completeRun.getRunId().toString(), reRunList);

					Double exexutiontime=completeRun.getTimeTake();
					Double day = exexutiontime / (24 * 3600); 

					exexutiontime = exexutiontime % (24 * 3600); 
					Double hour = exexutiontime / 3600; 

					exexutiontime %= 3600; 
					Double minutes = exexutiontime / 60 ;

					long mStrippedValue=0;
					long mStrippedValues =0;
					long mStrippedValuess =0;


					System.out.println("day"+day+".....hour..."+hour+"...miniutes.."+minutes);
					if(day!=null)
					{
						//String dayscount=day.toString();
						mStrippedValue = new Double(day).longValue();
						//days=Integer.parseInt(dayscount);

					}
					if(hour !=null)
					{
						// String hourcount=hour.toString();
						mStrippedValues = new Double(hour).longValue();
						// hours=Integer.parseInt(hourcount);
					}
					if(minutes!=null)
					{
						//  String minutescount=minutes.toString();
						mStrippedValuess = new Double(minutes).longValue();
						//  minutess=Integer.parseInt(minutescount);
					}
					String strd=Long.toString(mStrippedValue);
					String strh=Long.toString(mStrippedValues);
					String strm=Long.toString(mStrippedValuess);
					completeRun.setTimetakendhm(strd+"day: "+strh+"hrs: "+strm+"min");
					System.out.println("day......"+mStrippedValue+"hor...."+mStrippedValues+"...minutes.remove."+mStrippedValuess);


				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		 * finally { session.close(); }
		 */
		return maps;
	}
	public List<MannualReRunTable> getCompletedRun(Integer pId) {
		List<Object[]> list=null;
		List<MannualReRunTable> completedRunList = new ArrayList<MannualReRunTable>();
		String overViewProjectHQL = "SELECT mre.id,mr.projectId,mre.testSuitesId,mr.runName,mre.startDate,mre.targetEndDate,mre.actualEndDate,mre.STATUS, mre.isActive,mre.createdBy \r\n" + 
				",mre.updatedBy,mre.createdDate,mre.updatedDate,mre.description,(\r\n" + 
				"SELECT u.name\r\n" + 
				"FROM UserDetailsEntity u\r\n" + 
				"WHERE u.id=mr.createdBy) AS createName, COUNT(IF(mt.testCaseAction != '', 1, NULL)) AS totalcases, COUNT(IF(mt.testCaseAction = 'Completed', 1, NULL)) AS Completed, COUNT(IF(mt.testCaseAction = 'On Hold', 1, NULL)) AS 'On Hold', COUNT(IF(mt.testCaseAction = 'Under Review', 1, NULL)) AS 'Under Review', COUNT(IF(mt.testCaseAction = 'On Hold', 1, NULL))+ COUNT(IF(mt.testCaseAction = 'Completed', 1, NULL))\r\n" + 
				"+ COUNT(IF(mt.testCaseAction = 'Under Review', 1, NULL)) AS totalExecuted, COUNT(IF(mt.testCaseAction = 'Not Started', 1, NULL)) AS 'Not Started', COUNT(IF(mt.status = 'Pass' && mt.testCaseAction = 'Completed', 1, NULL)) AS 'Pass', COUNT(IF(mt.status = 'Fail' && mt.testCaseAction = 'Completed', 1, NULL)) AS 'Fail', SUM(mt.executionTime) AS Total_secs\r\n" + 
				",mre.round,mre.runId,COUNT(IF(mt.testCaseAction = 'Pass', 1, NULL)) AS totalactionpass FROM MannualRun_Table mr LEFT JOIN MannualReRunTable mre ON mr.id=mre.runId\r\n" + 
				"LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mre.id\r\n" + 
				"WHERE mr.projectId=:projectId AND mr.isActive=:isActive AND mre.STATUS=:status\r\n" + 
				"GROUP BY mre.id order by mre.id desc";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectId", pId);
			query.setParameter("isActive", "Y");
			query.setParameter("status", "Completed");
			list = query.list();
			for (Object[] objects : list) {
				MannualReRunTable runDetails=new MannualReRunTable();
				runDetails.setId((Integer) objects[0]);
				runDetails.setProjectId((Integer) objects[1]);
				runDetails.setTestSuitesId( (Integer) objects[2]);
				runDetails.setRunName((String) objects[3]);
				runDetails.setStartDate((String) objects[4]);
				if(objects[5]!=null)
					runDetails.setTargetEndDate(objects[5].toString());	
				if(objects[11]!=null)
					runDetails.setCreatedDate((Date) objects[11]);
				runDetails.setUpdatedDate((Date) objects[12]);				
				runDetails.setDescription((String) objects[13]);
				runDetails.setCreatedBy((String) objects[14]);
				if(objects[15]!=null)
//					runDetails.setTestCaseNo((BigInteger) objects[15]);
				runDetails.setTestCaseNo((BigInteger.valueOf((long) objects[15])));
				if(objects[16]!=null)
//					runDetails.setCompleted((BigInteger) objects[16]);
				runDetails.setCompleted((BigInteger.valueOf((long) objects[16])));
				if(objects[17]!=null)
//					runDetails.setOnHold((BigInteger) objects[17]);
				runDetails.setOnHold((BigInteger.valueOf((long) objects[17])));
				if(objects[18]!=null)
//					runDetails.setUnderReview((BigInteger) objects[18]);
				runDetails.setUnderReview((BigInteger.valueOf((long) objects[18])));
				if(objects[19]!=null)
//					runDetails.setTotalExecuted((BigInteger) objects[19]);
				runDetails.setTotalExecuted((BigInteger.valueOf((long) objects[19])));
				if(objects[20]!=null)
//					runDetails.setNotStarted((BigInteger) objects[20]);
				runDetails.setNotStarted((BigInteger.valueOf((long) objects[20])));
				runDetails.setPass((BigInteger.valueOf((long) objects[21])));
				runDetails.setFail((BigInteger.valueOf((long) objects[22])));
				if(objects[21]!=null)
					runDetails.setTimeTake((Double) objects[23]);
				runDetails.setStatus((String) objects[7]);
				runDetails.setIsActive((String) objects[8]);
				runDetails.setRound((Integer) objects[24]);
				runDetails.setRunId((Integer) objects[25]);
//				runDetails.setTotalactionpass((BigInteger) objects[25]);
				runDetails.setTotalactionpass((BigInteger.valueOf((long) objects[25])));
				completedRunList.add(runDetails);
				//System.out.println(runDetails);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return completedRunList;
	}
	public List<MannualReRunTable> getAllRun(Integer pId) {
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		List<MannualReRunTable> activeRunList = new ArrayList<MannualReRunTable>();
		activeRunList=reRunRepository.getAllRun(pId);
		return activeRunList;

	}
	public List<MannualReRunTable> getrerunnameByrun(Integer runid) {
		List<MannualReRunTable> activeRunList = new ArrayList<MannualReRunTable>();
		activeRunList=reRunRepository.getrerunnameByrun(runid);
		for (MannualReRunTable obj : activeRunList)
		{
			obj.setRerunname(obj.getRunName()+"_"+obj.getRound());

		}
		return activeRunList;
	}
	public MannualReRunTable getLastRerunnameByrun(Integer runid) {
		MannualReRunTable lastRun = null;
		lastRun=reRunRepository.findByRunIdOrderByRoundDesc(runid).get(0);

		return lastRun;
	}

	public List<MannualReRunTable> getAllCompleteRunByRun(Integer runid) {
		List<MannualReRunTable> allRun = null;
		allRun=reRunRepository.findByRunIdOrderByRoundDesc(runid);

		return allRun;
	}

	public List<MannualRunTable> getActiveRunRerun(Integer pId) {
		List<Object[]> list=null;
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		List<MannualRunTable> activeRunList = new ArrayList<MannualRunTable>();
		String overViewProjectHQL = "select mr.id,mr.runName,mr.projectId from MannualRun_Table mr inner join MannualReRunTable mre on mr.id=mre.runId where mr.isActive=:isActive and"
				+ " mr.projectId=:projectId GROUP BY mr.id order by mr.id desc";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectId", pId);
			query.setParameter("isActive", "Y");
			//query.setParameter("status", "Running");
			list = query.list();
			for (Object[] objects : list) {
				MannualRunTable runDetails=new MannualRunTable();
				runDetails.setId((Integer) objects[0]);
				runDetails.setRunName((String)objects[1]);

				activeRunList.add(runDetails);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return activeRunList;

	}
	public List<MannualReRunTable> getActiveRunrunning(Integer pId) {
		List<Object[]> list=null;
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		List<MannualReRunTable> activeRunList = new ArrayList<MannualReRunTable>();
		String overViewProjectHQL = "select id,runName,round,projectId from MannualReRunTable where isActive=:isActive and"
				+ " projectId=:projectId and status=:status";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("projectId", pId);
			query.setParameter("isActive", "Y");
			query.setParameter("status", "Running");
			list = query.list();
			for (Object[] objects : list) {
				MannualReRunTable runDetails=new MannualReRunTable();
				runDetails.setId((Integer) objects[0]);
				runDetails.setRunName((String)objects[1]);
				runDetails.setRound(Integer.valueOf(objects[2].toString()));
				runDetails.setRerunname(runDetails.getRunName()+"_"+runDetails.getRound());
				activeRunList.add(runDetails);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return activeRunList;

	}
	public List<ResourceDateWiseExecution> testerExecution(Integer runid) {
		List<Object[]> list=null;
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		List<ResourceDateWiseExecution> executiondata = new ArrayList<ResourceDateWiseExecution>();
		String overViewProjectHQL ="SELECT COUNT(*) AS noofexecution,a.updatedTimeStamp AS DATE,a.testerId,(SELECT y.name FROM \r\n" + 
				"UserDetailsEntity y WHERE y.id=a.testerId) AS TesterName\r\n" + 
				"FROM MannualTestCaseExecutionLogs a WHERE \r\n" + 
				"a.reRunId=:runid GROUP BY a.testerId,date(a.updatedTimeStamp)";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(overViewProjectHQL);
			query.setParameter("runid", runid);
			list = query.list();
			for (Object[] objects : list) {
				ResourceDateWiseExecution runDetails=new ResourceDateWiseExecution();
				runDetails.setNoofexecution(Integer.valueOf(objects[0].toString()));
				runDetails.setDateofexecution((Date)objects[1]);
				runDetails.setTesterid(Integer.valueOf(objects[2].toString()));
				runDetails.setTestername((String)objects[3]);
				executiondata.add(runDetails);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return executiondata;

	}
}
