package com.apmosys.tmt.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.apmosys.tmt.models.MannualRunTable;
import com.apmosys.tmt.repository.MannualReRunRepository;
import com.apmosys.tmt.repository.MannualRunRepository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Repository
@Transactional
public class MannualRunDao {
	@PersistenceContext
    private EntityManager entityManager;
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	MannualRunRepository runRepository;
	
	public MannualRunTable createRun(MannualRunTable runTable) {
		MannualRunTable existingRun=null;
		if(runTable.getId()!=null) {
			MannualRunTable existingRun1=runRepository.findById(runTable.getId()).orElse(null);
			existingRun1.setIsActive(runTable.getIsActive());
			System.out.println("is active in dao if not null "+existingRun1.getIsActive());
		if(existingRun1!=null)
				existingRun1.setRunFlag(existingRun1.getRunFlag()+1);
				existingRun= runRepository.save(existingRun1);
		}
		else {
			runTable.setRunFlag(1);
			existingRun= runRepository.save(runTable);
		}
		return existingRun;
	}
	public MannualRunTable createRunTable(MannualRunTable runtable) {
		return runRepository.save(runtable);
	}
	public Integer deleteRoundOneRun(Integer runId) {
		Integer i=0;
		try {
			System.out.println("calling delete mannualruntable dao, id is  "+runId);
			runRepository.deleteRun(runId);
		 i=1;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}

	
}
