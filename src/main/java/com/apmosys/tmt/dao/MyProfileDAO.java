package com.apmosys.tmt.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.BO.MyProfileDataBO;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Repository
@Transactional
public class MyProfileDAO {
	@PersistenceContext
    private EntityManager entityManager;

	@Autowired
	SessionFactory sessionFactory;

	public List<MyProfileDataBO> getMyProjectsDataAdmin(Integer userid) {
		List<Object[]> list = null;
		List<MyProfileDataBO> data = new ArrayList<MyProfileDataBO>();
		String mpq = "SELECT p.projectID, p.isActive, p.projectName,p.applicationName, p.projectStartDate, p.projectEndDate\r\n" + 
				", (SELECT COUNT(m.id) FROM MannualTestCaseExecutionLogs m \r\n" + 
				"	WHERE m.testerId=:userid AND m.projectId=p.projectID) AS tcassigned\r\n" + 
				", (SELECT COUNT(DISTINCT(m.reRunId)) FROM MannualTestCaseExecutionLogs m \r\n" + 
				"	WHERE m.testerId=:userid AND m.projectId=p.projectID) AS runassigned FROM Project_MetaTable p";
				//+ "ORDER BY p.createdTime desc;";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(mpq);
			System.out.print("query"+query);
			query.setParameter("userid", userid);
			list = query.list();
			for (Object[] objects : list) {
				MyProfileDataBO mp = new MyProfileDataBO();
				mp.setProjectID((Integer) objects[0]);
				mp.setProjectIsActive((String) objects[1]);
				mp.setProjectName((String) objects[2]);
				mp.setApplicationName((String) objects[3]);
				mp.setProjectStartDate((Date) objects[4]);
				mp.setProjectEndDate((Date) objects[5]);
				mp.setAssignedTCCount((BigInteger.valueOf((long) objects[6]).intValue()));
				mp.setAssignedRerunCount((BigInteger.valueOf((long) objects[7]).intValue()));
//				mp.setAssignedTCCount(((BigInteger) objects[6]).intValue());
//				mp.setAssignedRerunCount(((BigInteger) objects[7]).intValue());
				mp.setProjectAccess("Admin");

				data.add(mp);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;

	}
	
	public List<MyProfileDataBO> getMyProjectsDataNonAdmin(Integer userid) {
		List<Object[]> list = null;
		List<MyProfileDataBO> data = new ArrayList<MyProfileDataBO>();
		String mpq = "SELECT p.projectID, p.isActive, p.projectName,p.applicationName, p.projectStartDate, p.projectEndDate\r\n" + 
				", (SELECT COUNT(m.id) FROM MannualTestCaseExecutionLogs m \r\n" + 
				"	WHERE m.testerId=:userid AND m.projectId=p.projectID) AS tcassigned\r\n" + 
				", (SELECT COUNT(DISTINCT(m.reRunId)) FROM MannualTestCaseExecutionLogs m \r\n" + 
				"	WHERE m.testerId=:userid AND m.projectId=p.projectID) AS runassigned\r\n" + 
				"	,(select a.AccessType FROM ProjectAccessTable a WHERE a.userDetailsId=:userid\r\n" + 
				"		AND a.projectID=p.projectID) AS roleassigned FROM Project_MetaTable p\r\n" + 
				"	where p.projectID IN(SELECT a.projectID FROM ProjectAccessTable a WHERE a.userDetailsId=:userid)"
				+ "ORDER BY p.createdTime desc;";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createNativeQuery(mpq);
			query.setParameter("userid", userid);
			list = query.list();
			for (Object[] objects : list) {
				MyProfileDataBO mp = new MyProfileDataBO();
				mp.setProjectID((Integer) objects[0]);
				mp.setProjectIsActive((String) objects[1]);
				mp.setProjectName((String) objects[2]);
				mp.setApplicationName((String) objects[3]);
				mp.setProjectStartDate((Date) objects[4]);
				mp.setProjectEndDate((Date) objects[5]);
				mp.setAssignedTCCount((BigInteger.valueOf((long) objects[6]).intValue()));
				mp.setAssignedRerunCount((BigInteger.valueOf((long) objects[7]).intValue()));
//				mp.setAssignedTCCount(((BigInteger) objects[6]).intValue());
//				mp.setAssignedRerunCount(((BigInteger) objects[7]).intValue());
				mp.setProjectAccess((String) objects[8]);

				data.add(mp);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;

	}

}
