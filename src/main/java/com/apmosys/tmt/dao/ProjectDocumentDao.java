package com.apmosys.tmt.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.ProjectDocument;
import com.apmosys.tmt.repository.ProjectDocumentRepo;
//import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

//import ch.qos.logback.core.net.SyslogOutputStream;

@Repository
@Transactional
public class ProjectDocumentDao {
	@PersistenceContext
    private EntityManager entityManager;
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	ProjectDocumentRepo documentRepo;

	@Transactional
	public Integer saveDocumentDetails(ProjectDocument projectDocument) throws SQLException {
		Integer i = null;
		Session session=entityManager.unwrap(Session.class);
//		Transaction transaction = session.beginTransaction();
		try {
			session.setCacheMode(CacheMode.IGNORE);
//			session.flush();
//			session.clear();
			// projectDocument.setCreatedTimeStamp(new Date());
			i = (Integer) session.save(projectDocument);
//			transaction.commit();
		}
//		catch(MySQLIntegrityConstraintViolationException e) {}
		
		catch (ConstraintViolationException e) {
			e.printStackTrace();
			
//			transaction.rollback();
			i=0;
			if(e.getSQLState().equals("23000")) {
	            
	                i = 1796;
	                
//	                transaction.rollback();
	                throw new SQLException(i.toString());
	            
	        }
		}
		catch (Exception e) {
			i=0;
			System.out.println("%%%%%<<<<<<<<"+i+">>>>>>>");
			e.printStackTrace();
//			transaction.rollback();
		}
		
		return  i;
	}

	public List<ProjectDocument> getPublicDocument() {
		Session session = null;
		List<ProjectDocument> publicDocument = new ArrayList<ProjectDocument>();
		String publicDocumentHQL = "SELECT p.id,p.subProjectId,m.projectName,u.name,p.documentsType,p.documentName,p.filePath,p.isPublic,p.createdTimeStamp,p.id FROM ProjectDocument p ,ProjectDeatilsMeta m,UserDetailsEntity u WHERE p.projectId=m.projectID AND p.uploadBy=u.id AND p.isPublic='Y' order by m.id";
		try {
			 session=entityManager.unwrap(Session.class);
//			session = sessionFactory.getCurrentSession();
			session.setCacheMode(CacheMode.IGNORE);
			System.out.println("<<<<<<<<<In public document dao>>>>>>>>>>");
			session.flush();
			session.clear();
			Query query=session.createQuery(publicDocumentHQL);
			List<Object[]> list=query.list();
			for (Object[] objects : list) {
				ProjectDocument document=new ProjectDocument();
				document.setId((Integer) objects[9]);
				document.setProjectId((Integer) objects[0]);
				document.setSubProjectId((Integer) objects[1]);
				document.setProjectName((String) objects[2]);
				document.setUploadByName((String) objects[3]);
				document.setDocumentName((String) objects[5]);
				document.setDocumentsType((String) objects[4]);
				document.setFilePath((String) objects[6]);
				document.setIsPublic((String) objects[7]);
				document.setCreatedTimeStamp((Date) objects[8]);
				publicDocument.add(document);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return publicDocument;
	}
	public List<ProjectDocument> getProjectDocument(Integer pId) {
		Session session = null;
		List<ProjectDocument> publicDocument = new ArrayList<ProjectDocument>();
		String publicDocumentHQL = " SELECT p.id,p.subProjectId,m.projectName,u.name,p.documentsType,p.documentName,p.filePath,p.isPublic,p.createdTimeStamp,p.id FROM ProjectDocument p ,ProjectDeatilsMeta m,UserDetailsEntity u  WHERE p.projectId=m.projectID AND p.uploadBy=u.id AND m.projectID=:projectID and p.isPublic='N' order by p.createdTimeStamp desc";
/*				"SELECT p.id,p.subProjectId,m.projectName,u.name,p.documentsType,p.documentName,p.filePath,p.isPublic,p.createdTimeStamp,p.id FROM ProjectDocument p ,ProjectDeatilsMeta m,UserDetailsEntity u WHERE p.projectId=m.projectID AND p.uploadBy=u.id AND m.id=:projectID order by p.createdTimeStamp desc";
*/		try {
			session=entityManager.unwrap(Session.class);
			session.setCacheMode(CacheMode.IGNORE);
			System.out.println("<<<<<<<<<In project document dao>>>>>>>>>>");
			session.flush();
			session.clear();
			Query query=session.createQuery(publicDocumentHQL);
			query.setParameter("projectID", pId);
			List<Object[]> list=query.list();
			for (Object[] objects : list) {
				ProjectDocument document=new ProjectDocument();
				document.setId((Integer) objects[9]);
				document.setProjectId((Integer) objects[0]);
				document.setSubProjectId((Integer) objects[1]);
				document.setProjectName((String) objects[2]);
				document.setUploadByName((String) objects[3]);
				document.setDocumentName((String) objects[5]);
				document.setDocumentsType((String) objects[4]);
				document.setFilePath((String) objects[6]);
				document.setIsPublic((String) objects[7]);
				document.setCreatedTimeStamp((Date) objects[8]);
				publicDocument.add(document);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return publicDocument;
	}
	public ProjectDocument projectDocumentDao(Integer pId) {
	
		return documentRepo.findById(pId).orElse(null);
	}

	public List<String> getDocumentTypes() {
		List<String> list=null;
		
		List<String> documentList = new ArrayList<String>();
		String hQL = "select d.name from DocumentType d";
		Session session=entityManager.unwrap(Session.class);
		try {
		session.setCacheMode(CacheMode.IGNORE);
		session.flush();
		session.clear();
		Query query = session.createNativeQuery(hQL);
		
		list = query.list();
		
		for (String documentTypes : list) {
/*//			String documentType=new DocumentType();
//			
//			documentType.setName((String)documentTypes);
*/			
			documentList.add(documentTypes);

		}
		} catch (Exception e) {
		e.printStackTrace();
		}
		
		return documentList;

		}

	public List<ProjectDocument> getProjectDocsByPidAndDocType(String pId, String documentsName) {
		// TODO Auto-generated method stub
		List<ProjectDocument[]> list=null;
		
		List<ProjectDocument> projectDocList = new ArrayList<ProjectDocument>();
		String hQL = "SELECT p.projectId,p.documentsType FROM ProjectDocument p WHERE p.projectId=:pId AND p.documentsType=:documentsName ";
		Session session=entityManager.unwrap(Session.class);
		try {
		session.setCacheMode(CacheMode.IGNORE);
		session.flush();
		session.clear();
		Query query = session.createQuery(hQL);
		query.setParameter("pId", Integer.parseInt(pId));
		query.setParameter("documentsName", documentsName);
		
		list = query.list();
		
		for (Object[] projectDocument : list) {
			ProjectDocument projectDocuments=new ProjectDocument();
			projectDocuments.setProjectId((Integer)projectDocument[0]);
			projectDocuments.setDocumentsType((String)projectDocument[1]);
			
			projectDocList.add(projectDocuments);

		}
		} catch (Exception e) {
		e.printStackTrace();
		}
		
		return projectDocList;

		
		
	}

	public List<ProjectDocument> getProjectDocsByPidAndDocTypeIsPublicY(String pId, String documentsName,
			String createdBy, String isPublic) {
		// TODO Auto-generated method stub
		List<ProjectDocument[]> list = null;

		List<ProjectDocument> projectDocList = new ArrayList<ProjectDocument>();
		if (isPublic.equals("Y")) {

			String hQL = "SELECT p.projectId,p.documentsType FROM ProjectDocument p WHERE p.projectId=:pId AND p.documentsType=:documentsName AND p.uploadBy=:createdBy AND p.isPublic='Y'";
			Session session=entityManager.unwrap(Session.class);
			try {
				session.setCacheMode(CacheMode.IGNORE);
				session.flush();
				session.clear();
				Query query = session.createQuery(hQL);
				query.setParameter("pId", Integer.parseInt(pId));
				query.setParameter("documentsName", documentsName);
				query.setParameter("createdBy", createdBy);

				list = query.list();

				for (Object[] projectDocument : list) {
					ProjectDocument projectDocuments = new ProjectDocument();
					projectDocuments.setProjectId((Integer) projectDocument[0]);
					projectDocuments.setDocumentsType((String) projectDocument[1]);

					projectDocList.add(projectDocuments);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return projectDocList;

		}

		return projectDocList;

	}

	public List<ProjectDocument> getProjectDocsByPidAndDocTypeIsPublicN(String pId, String documentsName,
			String createdBy, String isPublic) {
		// TODO Auto-generated method stub
		List<ProjectDocument[]> list = null;

		List<ProjectDocument> projectDocList = new ArrayList<ProjectDocument>();
		if (isPublic.equals("N")) {

			String hQL = "SELECT p.projectId,p.documentsType FROM ProjectDocument p WHERE p.projectId=:pId AND p.documentsType=:documentsName AND p.uploadBy=:createdBy AND p.isPublic='N'";
			Session session=entityManager.unwrap(Session.class);
			try {
				session.setCacheMode(CacheMode.IGNORE);
				session.flush();
				session.clear();
				Query query = session.createQuery(hQL);
				query.setParameter("pId", Integer.parseInt(pId));
				query.setParameter("documentsName", documentsName);
				query.setParameter("createdBy", createdBy);

				list = query.list();

				for (Object[] projectDocument : list) {
					ProjectDocument projectDocuments = new ProjectDocument();
					projectDocuments.setProjectId((Integer) projectDocument[0]);
					projectDocuments.setDocumentsType((String) projectDocument[1]);

					projectDocList.add(projectDocuments);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return projectDocList;

		}

		return projectDocList;

	}
	public void deleteDocument(int id) {
		// TODO Auto-generated method stub
		
	}

	public Integer deleteDocumentById(int id) {
		// TODO Auto-generated method stub
		Integer i=null;
		String hql="DELETE FROM ProjectDocument WHERE id =:id ";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createQuery(hql);
			query.setParameter("id", id);
			i=query.executeUpdate();

			
		}catch(Exception e){
			e.printStackTrace();
			i=0;
		}
		return i;
	}

	public List<ProjectDocument> checkForExistingEntryByPrimary(int projectId, String documentsType, String documentName) {
		// TODO Auto-generated method stub
		List<ProjectDocument[]> list=null;

		List<ProjectDocument> projectDocList = new ArrayList<ProjectDocument>();
		String hQL = "SELECT p.projectId,p.documentsType FROM ProjectDocument p WHERE p.projectId=:projectId AND p.documentsType=:documentsType AND p.documentName=:documentName";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query = session.createQuery(hQL);
			query.setParameter("projectId",projectId);
			query.setParameter("documentName", documentName);
			query.setParameter("documentsType", documentsType);
			

			list = query.list();

			for (Object[] projectDocument : list) {
				ProjectDocument projectDocuments=new ProjectDocument();
				projectDocuments.setProjectId((Integer)projectDocument[0]);
				projectDocuments.setDocumentsType((String)projectDocument[1]);

				projectDocList.add(projectDocuments);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return projectDocList;

		
		
		
	
	}
}
