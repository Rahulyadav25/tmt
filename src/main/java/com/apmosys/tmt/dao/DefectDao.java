package com.apmosys.tmt.dao;

import java.math.BigInteger;
/*import java.sql.Date;*/
import java.util.ArrayList;
import java.util.List;

import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.DefectsDeatils;
import com.apmosys.tmt.models.StatusDefectGraph;
import com.apmosys.tmt.models.defectseverity;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Repository
@Transactional
public class DefectDao {
	@PersistenceContext
    private EntityManager entityManager;
	
	@Autowired
	SessionFactory sessionFactory;
	public List<DefectsDeatils> getDefectProjectDetails(Integer projectID) {
		List<Object[]> list=new ArrayList<>();
		List<DefectsDeatils> defectsDeatils=new ArrayList<DefectsDeatils>();
		
		String defectHQL="SELECT bd.bug_id, mut.testSuitesName, u.NAME, bd.bug_summary, bd.bug_status\n" + 
				"		, bd.created_on,(DATEDIFF(SYSDATE(),bd.created_on)) as creatddate\n" + 
				"		 FROM BugDetails bd, Mannual_TestCase_Table tct INNER join\n" + 
				"		 Mannual_TestGroup_Table tg ON tg.testGroupId =tct.testGroupId left JOIN\n" + 
				"		Mannual_TestSuites_Table mut  ON mut.testSuitesId = tg.testSuitesId JOIN\n" + 
				"		UserDetailsEntity u WHERE tct.SrID = bd.tc_sr_id AND bd.project_id=:projectId \n" + 
				"		AND bd.created_by=u.id;";
				/*"SELECT bd.bug_id,bd.bug_summary,bd.created_on,bd.bug_status,(DATEDIFF(SYSDATE(),bd.created_on)) as creatddate,mut.testSuitesName\n" + 
				" FROM BugDetails bd, Mannual_TestCase_Table tct INNER join\n" + 
				" Mannual_TestGroup_Table tg ON tg.testGroupId =tct.testGroupId left JOIN\n" + 
				"Mannual_TestSuites_Table mut  ON mut.testSuitesId = tg.testSuitesId \n" + 
				" WHERE   tct.SrID = bd.tc_sr_id AND  bd.project_id=:projectId\n" + 
				"";
				/*"SELECT mtl.bug_id,mtl.bug_summary,mtl.created_on,mtl.bug_status\n" +
						"WHERE mtl.project_id=:projectId ";*/
				/*" from BugDetails mtl\n" + 				"INNER JOIN MannualReRunTable mre \n" + */
				
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query= session.createNativeQuery(defectHQL);
			query.setParameter("projectId", projectID);
			defectsDeatils=query.list();
			/*for (Object[] objects : list) {
				DefectsDeatils details=new DefectsDeatils();
				details.setRunName((String) objects[0]);
				details.setCreatedDate((Date) objects[1]);
				details.setBugId((String) objects[2]);	
				details.setBugStatus((String) objects[3]);
				details.setBugSeverity((String) objects[4]);
				defectsDeatils.add(details);
			}*/
			//defectsDeatils.add(list);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return defectsDeatils; 
	}
	
	
	public List<defectseverity> getdefectseveritygraph(Integer projectID) {
		List<Object[]> list=new ArrayList<>();
		List<defectseverity> defectsDeatils=new ArrayList<defectseverity>();
		
		String defectHQL="SELECT count(if(b.bug_severity='Critical',1,NULL)) AS critical ,\n" + 
				"count(if(b.bug_severity='Blocker',1,NULL)) AS blocker ,\n" + 
				"count(if(b.bug_severity='Major',1,NULL)) AS major ,\n" + 
				"count(if(b.bug_severity='Minor',1,NULL)) AS minor\n" + 
				" FROM BugDetails b  WHERE b.project_id =:projectID"
				+ " ;";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query= session.createNativeQuery(defectHQL);
			query.setParameter("projectID", projectID);
			defectsDeatils=query.list();
			for (Object[] objects : list) {
				defectseverity details=new defectseverity();
				details.setCritical(Integer.parseInt(objects[0].toString()));
				details.setMajor(Integer.parseInt(objects[2].toString()));
				details.setMinor(Integer.parseInt(objects[3].toString()));
				defectsDeatils.add(details);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return defectsDeatils; 
	}
	
	public List<StatusDefectGraph> getstuatudefectsgraph(Integer projectID) {
		List<Object[]> list=new ArrayList<>();
		List<StatusDefectGraph> defectsDeatils=new ArrayList<StatusDefectGraph>();
		
		String defectHQL="SELECT monthname(b.created_on) AS MONTH,\n" + 
				"COUNT(if(b.bug_status='Open',1,NULL) )AS OPEN ,\n" + 
				"COUNT(if(b.bug_status='Closed',1,NULL) )AS closed ,\n" + 
				"COUNT(if(b.bug_status='Resolved',1,NULL) )AS Reolved ,\n" + 
				"COUNT(if(b.bug_status='Reopen',1,NULL) )AS Reopen   \n" + 
				"FROM BugDetails b\n" + 
				"WHERE  b.project_id=:projectID\n" + 
				"GROUP BY MONTH(b.created_on)\n" + 
				"ORDER BY MONTH;";
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query= session.createNativeQuery(defectHQL);
			query.setParameter("projectID", projectID);
			list=query.list();
			for (Object[] objects : list) {
				StatusDefectGraph details=new StatusDefectGraph();
				details.setMonthname(objects[0].toString());
				details.setOpen(Integer.parseInt(objects[1].toString()));
				details.setClosed(Integer.parseInt(objects[2].toString()));
				details.setResolved(Integer.parseInt(objects[3].toString()));
				details.setReopen(Integer.parseInt(objects[4].toString()));
				defectsDeatils.add(details);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return defectsDeatils; 
	}
	
	public Integer getBugCountByTesterID(Integer testerID, List<Integer> pID, String whereBugCond) {
		Integer bugCount=0;
		
		String defectHQL="select count(b.bug_id) FROM BugDetails b WHERE b.project_id IN(:pID) \n" + 
				"				 AND b.created_by IN(:testerID) "+whereBugCond;
		Session session=entityManager.unwrap(Session.class);
		try {
			session.setCacheMode(CacheMode.IGNORE);
			session.flush();
			session.clear();
			Query query= session.createNativeQuery(defectHQL);
			query.setParameter("testerID", testerID);
			query.setParameterList("pID", pID);
			List r=query.list();
			bugCount=((BigInteger) r.get(0)).intValue();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * finally { session.close(); }
		 */
		return bugCount; 
	}
}
