package com.apmosys.tmt;

import java.text.SimpleDateFormat;
import java.util.Date;

//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
//import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.apmosys.tmt.models.UserDetailsEntity;
import com.apmosys.tmt.models.UserSessionDetails;
import com.apmosys.tmt.repository.UserSessionsRepo;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@SpringBootApplication
@Controller
public class FinaclePortal extends SpringBootServletInitializer {

	@Value("${app.license.key}")
	private String licenseKey;
	@Autowired
	private HttpSession session;
	@Autowired
	private Codec codec;



	@RequestMapping("/home")
	public String getUserManagement() {

		//		System.out.println(session.getAttribute("user"));

		try {
			if (licenseKey != null) {
				String decode = codec.decrypt(licenseKey);
				System.out.println(decode);
				String[] splitVal = decode.split("TMT");
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

				String macId = codec.IP_HOST();
				System.out.println("system license alert.."+dateFormat.parse(splitVal[2]));

				if ((splitVal[0] + "TMT" + splitVal[1]).equals(macId)) {
					if (dateFormat.parse(splitVal[2]).after(new Date())) {
						if (session.isNew()) {
							return "redirect: login.html";
						} else {
							// System.out.println("vsdv"+session.getCreationTime());
							return "tmt/index.html";
						}
					} else
						return "tmt/Lisense.html";
				} else {
					return "tmt/Lisense.html";
				}
			} else {
				return "tmt/Lisense.html";
			}
		} catch (Exception e) {
			return "tmt/Lisense.html";
		}

	}

	@RequestMapping("/logout")
	public String logoutUser(HttpServletRequest request) {
		//		System.out.print("apmosys log out");
//		session.invalidate();
		return "redirect: login.html";
	}


	@RequestMapping("/")//  "/"
	public String loginUser(HttpServletResponse response) throws Exception {
		try { 
			if (licenseKey != null) {
				// if license key is 
				String decode = codec.decrypt(licenseKey);
				//				System.out.println(decode);
				String[] splitVal = decode.split("TMT");
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				String macId = codec.IP_HOST();
				System.out.println("system license alert.."+
						((splitVal[0] + "TMT" + splitVal[1]).equals(macId)));

				if ((splitVal[0] + "TMT" + splitVal[1]).equals(macId)) {
					if (dateFormat.parse(splitVal[2]).after(new Date())) {
						if (session.getAttribute("user") == null) {
							System.out.println("no session");
							response.addHeader("Content-Security-Policy", "script-src 'self' 'unsafe-inline'");
							return "redirect: login.html";
						} else {
							System.out.println("valid session: " + session.getCreationTime());
							return "tmt/index.html";
						}
					} else
						return "tmt/Lisense.html";
				} else {
					return "tmt/Lisense.html";
				}
			} else {
				return "tmt/Lisense.html";
			}
		} catch (Exception e) {
			return "tmt/Lisense.html";
		}

	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		// TODO Auto-generated method stub
		return builder.sources(FinaclePortal.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(FinaclePortal.class, args);

	}

}