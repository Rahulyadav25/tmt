package com.apmosys.tmt.utils;

import java.util.Map;

//import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.apmosys.tmt.models.UserDetailsEntity;

import jakarta.servlet.http.HttpSession;

/*
 * custom logger mostly gonna be called from controllers having request and response body
 * 
 */
@Service
public class CustomLogger {

	 Logger LOGGER=LoggerFactory.getLogger(CustomLogger.class);
	
	public  void logMyInfo(String methodName,Map<String, Object> serviceRequestInputs,HttpSession session, ServiceResponse res) {
		
	try {
		System.out.println("Inside logger service");
		UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
		if(detailsEntity !=null)
		LOGGER.info("--------------------Enter with UserID : "+detailsEntity.getUserID()+" and Name : "+detailsEntity.getUsername()+"--------------------");
		LOGGER.info("with method Name: "+methodName);
		if(serviceRequestInputs!=null)
		LOGGER.info("method params(inputs): "+serviceRequestInputs.toString());
		if(res!=null && res.getServiceResponse()!=null) {
		LOGGER.info("Response Status : "+res.getServiceStatus());
//		ArrayList a=(ArrayList)res.getServiceResponse(); 
//		a.stream().forEach(eachData-> LOGGER.info(eachData.toString()));
//		 LOGGER.info(res.getServiceResponse().toString());
//		 LOGGER.info();
		}
		if(res!=null && res.getServiceError()!=null && res.getServiceError()!="") 
			LOGGER.error("error : "+res.getServiceError().toString()+" with service code: "+res.getServiceStatus());
		LOGGER.info("--------------------LOG END------------------------------------------------------------------------------------------");
		
	}//try
	catch (Exception e) {

	}//catch
	}//method
}//class