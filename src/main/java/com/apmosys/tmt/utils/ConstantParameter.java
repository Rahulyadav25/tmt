package com.apmosys.tmt.utils;

import java.util.List;

public class ConstantParameter {
	public static final String[] MannualExcelHeaderList = { "Sr.No", "Module", "Sub-Module", "Scenario ID", "Scenarios", "Functionality","Test Case ID",
			"Test Cases","Test case type(Positive/negative)","Test Data/Inputs","Steps to Reproduce","Expected Result","Browser/OS", "Test Case Prepared By"
			, "Test Case Preparation Time(hh:mm:ss)"};
	
	public static final String[] MannualRunExcelHeaderList = {"Sr.No", "Module", "Sub-Module", "Scenario ID", "Scenarios", "Functionality","Test Case ID",
			"Test Cases","Test case type(Positive/negative)","Test Data/Inputs","Steps to Reproduce","Expected Result",	"Browser/OS", "Test Case Prepared By"
			, "Test Case Preparation Time(hh:mm:ss)", "Date Of Execution",	"Actual Result"	,"Execution Time", "Tester Comment", "Tester Name", "Is Active"
			, "Bug Severity", "Bug Priority", "Bug Status", "Bug Id","Sr ID", "Status", "Test Case Action", "Complexity", "Priority"
		};
	
	public static final String[] AutoMainController = {"Sr_No" , "RunStatus" , "ApplicationID" , "Description" , "Remark" , "Process1"};
	public static final String[] AutoDataSheet = {"Sr_No" , "Process" , "TestFlow_Path" , "DefaultWait_Time" , "MaxResponseTime" , "Availability_alert" ,
			"ResponseTime_alert" , "DB_Connection" , "Multiple_Testcases"};
	public static final String[] AutoDBConnection = {"SR_NO" , "Process" , "localdriver" , "localurl" , "localUserName" , "localpassword" , "Run_Status"};

}
