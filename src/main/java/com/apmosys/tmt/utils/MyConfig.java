package com.apmosys.tmt.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class MyConfig implements WebMvcConfigurer {
	
	@Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
        		.allowCredentials(true)
        		.allowedOrigins("http://localhost:4200")
        		.allowedMethods("POST","GET");
    }
	
//	@Bean
//	public MultipartConfigElement multipartConfigElement() {
//	    MultipartConfigFactory factory = new MultipartConfigFactory();
////	    factory.setMaxFileSize(DataSize.of(10, DataUnit.MEGABYTES));
////	    factory.setMaxRequestSize(DataSize.of(10, DataUnit.MEGABYTES));
//	    return factory.createMultipartConfig();
//	}
	
	@Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
            http.
            csrf().disable().
            headers().frameOptions().deny();
            return http.build();
    }

}
