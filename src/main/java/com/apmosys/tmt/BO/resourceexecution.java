package com.apmosys.tmt.BO;

public class resourceexecution {
	private String resourcename;
	private String resourceid;
	private String preparedexecution;
	private String freshrunexecution;
	private String rerunexecution;
	public String getTotalexecution() {
		return totalexecution;
	}
	public void setTotalexecution(String totalexecution) {
		this.totalexecution = totalexecution;
	}
	private String totalexecution;
	public String getResourcename() {
		return resourcename;
	}
	public void setResourcename(String resourcename) {
		this.resourcename = resourcename;
	}
	public String getResourceid() {
		return resourceid;
	}
	public void setResourceid(String resourceid) {
		this.resourceid = resourceid;
	}
	public String getPreparedexecution() {
		return preparedexecution;
	}
	public void setPreparedexecution(String preparedexecution) {
		this.preparedexecution = preparedexecution;
	}
	public String getFreshrunexecution() {
		return freshrunexecution;
	}
	public void setFreshrunexecution(String freshrunexecution) {
		this.freshrunexecution = freshrunexecution;
	}
	public String getRerunexecution() {
		return rerunexecution;
	}
	public void setRerunexecution(String rerunexecution) {
		this.rerunexecution = rerunexecution;
	}

}
