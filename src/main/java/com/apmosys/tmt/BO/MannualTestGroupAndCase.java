package com.apmosys.tmt.BO;

import java.util.List;

//import javax.persistence.Cacheable;

import com.apmosys.tmt.models.MannualTestCaseTable;

import jakarta.persistence.Cacheable;
@Cacheable(false)
public class MannualTestGroupAndCase {
private Integer groupId;
private String isActive;

private List<MannualTestCaseTable> testCaseList;
public Integer getGroupId() {
	return groupId;
}
public void setGroupId(Integer groupId) {
	this.groupId = groupId;
}
public List<MannualTestCaseTable> getTestCaseList() {
	return testCaseList;
}
public void setTestCaseList(List<MannualTestCaseTable> testCaseList) {
	this.testCaseList = testCaseList;
}
@Override
public String toString() {
	return "MannualTestGroupAndCase [groupId=" + groupId + ", testCaseList=" + testCaseList + "]";
}

public String getIsActive() {
	return isActive;
}
public void setIsActive(String isActive) {
	this.isActive = isActive;
}
}