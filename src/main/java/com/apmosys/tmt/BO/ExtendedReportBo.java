package com.apmosys.tmt.BO;

import java.util.List;

import com.apmosys.tmt.models.ExecutionStepsBo;

public class ExtendedReportBo {
	private String projectname;
	private String status;
	private String description;
	private String browser;
	private String summary;
	private String bugsummary;
	private String expectedresult;
	private String actualresult;
	private String severity;
	private String priority;
	private String bugtool;
	private List<ExecutionStepsBo> steps;
	private ExecutionReportAutomation verify;
	
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	public String getBugtool() {
		return bugtool;
	}
	public void setBugtool(String bugtool) {
		this.bugtool = bugtool;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getBugsummary() {
		return bugsummary;
	}
	public void setBugsummary(String bugsummary) {
		this.bugsummary = bugsummary;
	}
	public String getExpectedresult() {
		return expectedresult;
	}
	public void setExpectedresult(String expectedresult) {
		this.expectedresult = expectedresult;
	}
	public String getActualresult() {
		return actualresult;
	}
	public void setActualresult(String actualresult) {
		this.actualresult = actualresult;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}

public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
public List<ExecutionStepsBo> getSteps() {
	return steps;
}
public void setSteps(List<ExecutionStepsBo> steps) {
	this.steps = steps;
}
public ExecutionReportAutomation getVerify() {
	return verify;
}
public void setVerify(ExecutionReportAutomation executionReportAutomation) {
	this.verify = executionReportAutomation;
}
@Override
public String toString() {
	return "ExtendedReportBo [steps=" + steps + ", verify=" + verify + "]";
}


}
