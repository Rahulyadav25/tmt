package com.apmosys.tmt.BO;

public class OverallRunActionStatus {

	
	private Integer completed=0;;
	private Integer unTested=0;
	private Integer onHold=0;
	private Integer underReview=0;
	private Integer na=0;
	private Integer totalactionincomplete=0;
	
	public Integer getTotalactionincomplete() {
		return totalactionincomplete;
	}
	public void setTotalactionincomplete(Integer totalactionincomplete) {
		this.totalactionincomplete = totalactionincomplete;
	}
	public Integer getCompleted() {
		return completed;
	}
	public void setCompleted(Integer completed) {
		this.completed = completed;
	}
	public Integer getUnTested() {
		return unTested;
	}
	public Integer getNa() {
		return na;
	}
	public void setNa(Integer na) {
		this.na = na;
	}
	public void setUnTested(Integer unTested) {
		this.unTested = unTested;
	}
	public Integer getOnHold() {
		return onHold;
	}
	public void setOnHold(Integer onHold) {
		this.onHold = onHold;
	}
	public Integer getUnderReview() {
		return underReview;
	}
	public void setUnderReview(Integer underReview) {
		this.underReview = underReview;
	}

	
	


}
