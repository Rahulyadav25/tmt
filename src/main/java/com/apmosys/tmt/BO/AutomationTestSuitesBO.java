package com.apmosys.tmt.BO;

public class AutomationTestSuitesBO {
	private String testSuitesName;
	private String isRun;
	private boolean isFilePresent;
	
	public AutomationTestSuitesBO(){
		
	}
	
	public String getTestSuitesName() {
		return testSuitesName;
	}
	public void setTestSuitesName(String testSuitesName) {
		this.testSuitesName = testSuitesName;
	}
	public String getIsRun() {
		return isRun;
	}
	public void setIsRun(String isRun) {
		this.isRun = isRun;
	}
	public boolean getIsFilePresent() {
		return isFilePresent;
	}
	public void setIsFilePresent(boolean isFilePresent) {
		this.isFilePresent=isFilePresent;
	}
	
}
