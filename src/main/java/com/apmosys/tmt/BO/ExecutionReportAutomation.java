package com.apmosys.tmt.BO;

import java.util.Arrays;

public class ExecutionReportAutomation {
	private Integer id;
	private Integer runId;
	private String projectName;
	private String applicationName;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	private String scenarioId;
	private String testCaseId;
	private String resTime;
	private String actualResult;
	private String expectedResult;
	private String status;
	private String timeStamp;
	private String description;
	private String browserType;
	private String ip;
	private String screenShotId;
	private byte[] screenshot;
	
	private String TestcaseType;
	private String Step;
	
	
	private String start;
	private String end;
	private String tstatus;
	private String texpectedResult;
	private String tseverity;
	private String tpriority;
	private String tbugsummary;
	private String tsummary;
	private String tbrowser;
	private String tbugtool;
	private String passedoutvalue;

	@Override
	public String toString() {
		return "ExecutionReportAutomation [id=" + id + ", runId=" + runId + ", projectName=" + projectName
				+ ", applicationName=" + applicationName + ", scenarioId=" + scenarioId + ", testCaseId=" + testCaseId
				+ ", resTime=" + resTime + ", actualResult=" + actualResult + ", expectedResult=" + expectedResult
				+ ", status=" + status + ", timeStamp=" + timeStamp + ", description=" + description + ", browserType="
				+ browserType + ", ip=" + ip + ", screenShotId=" + screenShotId + ", screenshot="
				+ Arrays.toString(screenshot) + ", TestcaseType=" + TestcaseType + ", Step=" + Step + ", start=" + start
				+ ", end=" + end + ", tstatus=" + tstatus + ", texpectedResult=" + texpectedResult + ", tseverity="
				+ tseverity + ", tpriority=" + tpriority + ", tbugsummary=" + tbugsummary + ", tsummary=" + tsummary
				+ ", tbrowser=" + tbrowser + ", tbugtool=" + tbugtool + ", passedoutvalue=" + passedoutvalue
				+ ", tdescription=" + tdescription + ", trunid=" + trunid + ", tcasesid=" + tcasesid + "]";
	}
	public String getPassedoutvalue() {
		return passedoutvalue;
	}
	public void setPassedoutvalue(String passedoutvalue) {
		this.passedoutvalue = passedoutvalue;
	}
	public String getTstatus() {
		return tstatus;
	}
	public void setTstatus(String tstatus) {
		this.tstatus = tstatus;
	}
	public String getTbugtool() {
		return tbugtool;
	}
	public void setTbugtool(String tbugtool) {
		this.tbugtool = tbugtool;
	}
	public String getTexpectedResult() {
		return texpectedResult;
	}
	public void setTexpectedResult(String texpectedResult) {
		this.texpectedResult = texpectedResult;
	}
	public String getTseverity() {
		return tseverity;
	}
	public void setTseverity(String tseverity) {
		this.tseverity = tseverity;
	}
	public String getTpriority() {
		return tpriority;
	}
	public void setTpriority(String tpriority) {
		this.tpriority = tpriority;
	}
	public String getTbugsummary() {
		return tbugsummary;
	}
	public void setTbugsummary(String tbugsummary) {
		this.tbugsummary = tbugsummary;
	}
	public String getTsummary() {
		return tsummary;
	}
	public void setTsummary(String tsummary) {
		this.tsummary = tsummary;
	}
	public String getTbrowser() {
		return tbrowser;
	}
	public void setTbrowser(String tbrowser) {
		this.tbrowser = tbrowser;
	}
	public String getTdescription() {
		return tdescription;
	}
	public void setTdescription(String tdescription) {
		this.tdescription = tdescription;
	}
	public String getTrunid() {
		return trunid;
	}
	public void setTrunid(String trunid) {
		this.trunid = trunid;
	}
	public String getTcasesid() {
		return tcasesid;
	}
	public void setTcasesid(String tcasesid) {
		this.tcasesid = tcasesid;
	}
	private String tdescription;
	private String trunid;
	private String tcasesid;
	public Integer getRunId() {
		return runId;
	}
	public void setRunId(Integer runId) {
		this.runId = runId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getScenarioId() {
		return scenarioId;
	}
	public void setScenarioId(String scenarioId) {
		this.scenarioId = scenarioId;
	}
	public String getTestCaseId() {
		return testCaseId;
	}
	public void setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}
	public String getResTime() {
		return resTime;
	}
	public void setResTime(String resTime) {
		this.resTime = resTime;
	}
	public String getActualResult() {
		return actualResult;
	}
	public void setActualResult(String actualResult) {
		this.actualResult = actualResult;
	}
	public String getExpectedResult() {
		return expectedResult;
	}
	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBrowserType() {
		return browserType;
	}
	public void setBrowserType(String browserType) {
		this.browserType = browserType;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getScreenShotId() {
		return screenShotId;
	}
	public void setScreenShotId(String screenShotId) {
		this.screenShotId = screenShotId;
	}
	public byte[] getScreenshot() {
		return screenshot;
	}
	public void setScreenshot(byte[] screenshot) {
		this.screenshot = screenshot;
	}
	
	public String getTestcaseType() {
		return TestcaseType;
	}
	public void setTestcaseType(String testcaseType) {
		TestcaseType = testcaseType;
	}
	public String getStep() {
		return Step;
	}
	public void setStep(String step) {
		Step = step;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	
	
}

