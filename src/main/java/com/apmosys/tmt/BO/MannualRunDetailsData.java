package com.apmosys.tmt.BO;

import java.util.List;

import com.apmosys.tmt.models.MannualTestCaseExecutionLogs;

public class MannualRunDetailsData {
private String groupId;
private Integer testCaseCount;
private List<MannualTestCaseExecutionLogs> testCaseExecutionLogs;
public String getGroupId() {
	return groupId;
}
public void setGroupId(String groupId) {
	this.groupId = groupId;
}
public List<MannualTestCaseExecutionLogs> getTestCaseExecutionLogs() {
	return testCaseExecutionLogs;
}
public void setTestCaseExecutionLogs(List<MannualTestCaseExecutionLogs> testCaseExecutionLogs) {
	this.testCaseExecutionLogs = testCaseExecutionLogs;
}
public Integer getTestCaseCount() {
	return testCaseCount;
}
public void setTestCaseCount(Integer testCaseCount) {
	this.testCaseCount = testCaseCount;
}


}
