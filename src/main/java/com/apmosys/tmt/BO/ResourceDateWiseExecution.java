package com.apmosys.tmt.BO;

import java.util.Date;

public class ResourceDateWiseExecution {
	private int noofexecution;
	private Date dateofexecution;
	private String testername;
	private int testerid;
	public int getNoofexecution() {
		return noofexecution;
	}
	public void setNoofexecution(int noofexecution) {
		this.noofexecution = noofexecution;
	}
	public Date getDateofexecution() {
		return dateofexecution;
	}
	public void setDateofexecution(Date objects) {
		//Date dt=new Date(objects);
		this.dateofexecution = objects;
	}
	public String getTestername() {
		return testername;
	}
	public void setTestername(String testername) {
		this.testername = testername;
	}
	public int getTesterid() {
		return testerid;
	}
	public void setTesterid(int testerid) {
		this.testerid = testerid;
	}
	

}
