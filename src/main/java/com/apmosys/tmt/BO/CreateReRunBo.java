package com.apmosys.tmt.BO;

import java.util.List;

import com.apmosys.tmt.models.MannualTestCaseTable;

public class CreateReRunBo {
	private Integer reRunId;
	private Integer runId;
	private Integer projectId;
	private Integer testSuitesId;
	private String runName;
	private String startDate;
	private String targetEndDate;
	private String actualEndDate;
	private String status; 
	private String isActive;  
	private String createdBy;
	private String createdDate;
	private String description;
	private String assignTo;
	private Integer round;
	private List<MannualTestCaseTable> testCaseList;
	public Integer getReRunId() {
		return reRunId;
	}
	public void setReRunId(Integer reRunId) {
		this.reRunId = reRunId;
	}
	public Integer getRunId() {
		return runId;
	}
	public void setRunId(Integer runId) {
		this.runId = runId;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public Integer getTestSuitesId() {
		return testSuitesId;
	}
	public void setTestSuitesId(Integer testSuitesId) {
		this.testSuitesId = testSuitesId;
	}
	public String getRunName() {
		return runName;
	}
	public void setRunName(String runName) {
		this.runName = runName;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getTargetEndDate() {
		return targetEndDate;
	}
	public void setTargetEndDate(String targetEndDate) {
		this.targetEndDate = targetEndDate;
	}
	public String getActualEndDate() {
		return actualEndDate;
	}
	public void setActualEndDate(String actualEndDate) {
		this.actualEndDate = actualEndDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAssignTo() {
		return assignTo;
	}
	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}
	public Integer getRound() {
		return round;
	}
	public void setRound(Integer round) {
		this.round = round;
	}
	public List<MannualTestCaseTable> getTestCaseList() {
		return testCaseList;
	}
	public void setTestCaseList(List<MannualTestCaseTable> testCaseList) {
		this.testCaseList = testCaseList;
	}
	
}
