package com.apmosys.tmt.BO;

public class JenkinsProjectDetails {
private Integer pId;
private String pName;
private String slaveMachineName;
private String osType;
public Integer getpId() {
	return pId;
}
public void setpId(Integer pId) {
	this.pId = pId;
}
public String getpName() {
	return pName;
}
public void setpName(String pName) {
	this.pName = pName;
}
public String getSlaveMachineName() {
	return slaveMachineName;
}
public void setSlaveMachineName(String slaveMachineName) {
	this.slaveMachineName = slaveMachineName;
}
public String getOsType() {
	return osType;
}
public void setOsType(String osType) {
	this.osType = osType;
}

}
