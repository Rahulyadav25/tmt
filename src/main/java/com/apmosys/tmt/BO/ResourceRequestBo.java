package com.apmosys.tmt.BO;

import java.util.List;

public class ResourceRequestBo {
	private String fromDate;
	private String toDate;
	private int projectid;
	private List<Integer> runList;
	private List<Integer> testerList;
	private List<Integer> projectList;

	public List<Integer> getProjectList() {
		return projectList;
	}

	public void setProjectList(List<Integer> projectList) {
		this.projectList = projectList;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public int getProjectid() {
		return projectid;
	}

	public void setProjectid(int projectid) {
		this.projectid = projectid;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public List<Integer> getRunList() {
		return runList;
	}

	public void setRunList(List<Integer> runList) {
		this.runList = runList;
	}

	public List<Integer> getTesterList() {
		return testerList;
	}

	public void setTesterList(List<Integer> testerList) {
		this.testerList = testerList;
	}

	@Override
	public String toString() {
		return "ResourceRequestBo [fromDate=" + fromDate + ", toDtae=" + toDate + ", runList=" + runList
				+ ", testerList=" + testerList + "]";
	}

}
