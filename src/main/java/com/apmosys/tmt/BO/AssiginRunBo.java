package com.apmosys.tmt.BO;

public class AssiginRunBo {
private String runName;
private String runId;
private String createdBy;
private String targetDate;
private Integer assiginTestCaseCount;
private Integer totalComplete;
private Integer totalNotStated;
private Integer totalOnHold;
private Integer totalUnderReview;
private Integer totalNA;
private Integer totalFail;
private Integer totalPass;
private String colordate;
private int blockshowtopper;
private int blockedundelivered;
private int na;
private int blockedtestdata;
private int mustte;
private int needte;
private int nicete;
private int high;
private int medium;
private int low;
private int blockednotanclear;
private int ne;
private int deferred;
private int onhold;
private int duplicate;
private int OutofScope;
private int PartiallyPass;
//private int totalactionpass;
//modified to pass->> incomplete
private int totalactionincomplete;

public int getTotalactionincomplete() {
	return totalactionincomplete;
}
public void setTotalactionincomplete(int totalactionincomplete) {
	this.totalactionincomplete = totalactionincomplete;
}
public int getPartiallyPass() {
	return PartiallyPass;
}
public void setPartiallyPass(int partiallyPass) {
	PartiallyPass = partiallyPass;
}
public int getBlockednotanclear() {
	return blockednotanclear;
}
public void setBlockednotanclear(int blockednotanclear) {
	this.blockednotanclear = blockednotanclear;
}
public int getNe() {
	return ne;
}
public void setNe(int ne) {
	this.ne = ne;
}
public int getDeferred() {
	return deferred;
}
public void setDeferred(int deferred) {
	this.deferred = deferred;
}
public int getOnhold() {
	return onhold;
}
public void setOnhold(int onhold) {
	this.onhold = onhold;
}
public int getDuplicate() {
	return duplicate;
}
public void setDuplicate(int duplicate) {
	this.duplicate = duplicate;
}
public int getOutofScope() {
	return OutofScope;
}
public void setOutofScope(int outofScope) {
	OutofScope = outofScope;
}
public int getBlockshowtopper() {
	return blockshowtopper;
}
public void setBlockshowtopper(int blockshowtopper) {
	this.blockshowtopper = blockshowtopper;
}
public int getBlockedundelivered() {
	return blockedundelivered;
}
public void setBlockedundelivered(int blockedundelivered) {
	this.blockedundelivered = blockedundelivered;
}
public int getNa() {
	return na;
}
public void setNa(int na) {
	this.na = na;
}
public int getBlockedtestdata() {
	return blockedtestdata;
}
public void setBlockedtestdata(int blockedtestdata) {
	this.blockedtestdata = blockedtestdata;
}
public int getMustte() {
	return mustte;
}
public void setMustte(int mustte) {
	this.mustte = mustte;
}
public int getNeedte() {
	return needte;
}
public void setNeedte(int needte) {
	this.needte = needte;
}
public int getNicete() {
	return nicete;
}
public void setNicete(int nicete) {
	this.nicete = nicete;
}
public int getHigh() {
	return high;
}
public void setHigh(int high) {
	this.high = high;
}
public int getMedium() {
	return medium;
}
public void setMedium(int medium) {
	this.medium = medium;
}
public int getLow() {
	return low;
}
public void setLow(int low) {
	this.low = low;
}
public String getColordate() {
	return colordate;
}
public void setColordate(String colordate) {
	this.colordate = colordate;
}
public Integer getTotalOnHold() {
	return totalOnHold;
}
public void setTotalOnHold(Integer totalOnHold) {
	this.totalOnHold = totalOnHold;
}
public Integer getTotalUnderReview() {
	return totalUnderReview;
}
public void setTotalUnderReview(Integer totalUnderReview) {
	this.totalUnderReview = totalUnderReview;
}
public Integer getTotalNA() {
	return totalNA;
}
public void setTotalNA(Integer totalNA) {
	this.totalNA = totalNA;
}



public String getRunName() {
	return runName;
}
public Integer getTotalComplete() {
	return totalComplete;
}
public void setTotalComplete(Integer totalComplete) {
	this.totalComplete = totalComplete;
}
public Integer getTotalNotStated() {
	return totalNotStated;
}
public void setTotalNotStated(Integer totalNotStated) {
	this.totalNotStated = totalNotStated;
}
public Integer getTotalPass() {
	return totalPass;
}
public void setTotalPass(Integer totalPass) {
	this.totalPass = totalPass;
}
public Integer getTotalFail() {
	return totalFail;
}
public void setTotalFail(Integer totalFail) {
	this.totalFail = totalFail;
}
public void setRunName(String runName) {
	this.runName = runName;
}
public String getRunId() {
	return runId;
}
public void setRunId(String runId) {
	this.runId = runId;
}
public Integer getAssiginTestCaseCount() {
	return assiginTestCaseCount;
}
public void setAssiginTestCaseCount(Integer assiginTestCaseCount) {
	this.assiginTestCaseCount = assiginTestCaseCount;
}
public String getCreatedBy() {
	return createdBy;
}
public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
}
public String getTargetDate() {
	return targetDate;
}
public void setTargetDate(String targetDate) {
	this.targetDate = targetDate;
}

}
