package com.apmosys.tmt.BO;

import java.util.List;

import com.apmosys.tmt.models.MannualTestCaseTable;

public class UploadTestCaseBo {
private String testGroupName;
private Integer testGroupId;
private List<MannualTestCaseTable> testCases;
public String getTestGroupName() {
	return testGroupName;
}
public void setTestGroupName(String testGroupName) {
	this.testGroupName = testGroupName;
}
public List<MannualTestCaseTable> getTestCases() {
	return testCases;
}
public void setTestCases(List<MannualTestCaseTable> testCases) {
	this.testCases = testCases;
}
public Integer getTestGroupId() {
	return testGroupId;
}
public void setTestGroupId(Integer testGroupId) {
	this.testGroupId = testGroupId;
}



}
