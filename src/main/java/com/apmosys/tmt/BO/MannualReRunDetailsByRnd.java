package com.apmosys.tmt.BO;

import java.util.List;

import com.apmosys.tmt.models.MannualReRunTable;

public class MannualReRunDetailsByRnd {
private String runName;
private Integer totalReRunCount;
private Integer latestRound;
private List<MannualReRunTable> reRunList;
public String getRunName() {
	return runName;
}
public void setRunName(String runName) {
	this.runName = runName;
}
public List<MannualReRunTable> getReRunList() {
	return reRunList;
}
public void setReRunList(List<MannualReRunTable> reRunList) {
	this.reRunList = reRunList;
}
public Integer getTotalReRunCount() {
	return totalReRunCount;
}
public void setTotalReRunCount(Integer totalReRunCount) {
	this.totalReRunCount = totalReRunCount;
}
public Integer getLatestRound() {
	return latestRound;
}
public void setLatestRound(Integer latestRound) {
	this.latestRound = latestRound;
}

}
