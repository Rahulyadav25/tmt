package com.apmosys.tmt.BO;

import java.util.Date;

public class MyProfileDataBO {
	private Integer projectID;
	private String projectName;
	private String applicationName;
	private String projectIsActive;
	private Date projectStartDate;
	private Date projectEndDate;
	private Integer assignedTCCount;
	private Integer assignedRerunCount;
	private String projectAccess;
	
	@Override
	public String toString() {
		return "MyProfileDataBO [projectID=" + projectID + ", projectName=" + projectName + ", applicationName="
				+ applicationName + ", projectIsActive=" + projectIsActive + ", projectStartDate=" + projectStartDate
				+ ", projectEndDate=" + projectEndDate + ", assignedTCCount=" + assignedTCCount
				+ ", assignedRerunCount=" + assignedRerunCount + ", projectAccess=" + projectAccess + "]";
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public MyProfileDataBO(){
		
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectIsActive() {
		return projectIsActive;
	}

	public void setProjectIsActive(String projectIsActive) {
		this.projectIsActive = projectIsActive;
	}

	public Date getProjectStartDate() {
		return projectStartDate;
	}

	public void setProjectStartDate(Date projectStartDate) {
		this.projectStartDate = projectStartDate;
	}

	public Date getProjectEndDate() {
		return projectEndDate;
	}

	public void setProjectEndDate(Date projectEndDate) {
		this.projectEndDate = projectEndDate;
	}

	public Integer getAssignedTCCount() {
		return assignedTCCount;
	}

	public void setAssignedTCCount(Integer assignedTCCount) {
		this.assignedTCCount = assignedTCCount;
	}

	public Integer getAssignedRerunCount() {
		return assignedRerunCount;
	}

	public void setAssignedRerunCount(Integer assignedRerunCount) {
		this.assignedRerunCount = assignedRerunCount;
	}

	public String getProjectAccess() {
		return projectAccess;
	}

	public void setProjectAccess(String projectAccess) {
		this.projectAccess = projectAccess;
	}
	
	
}
