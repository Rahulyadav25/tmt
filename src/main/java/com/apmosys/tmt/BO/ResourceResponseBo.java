package com.apmosys.tmt.BO;

public class ResourceResponseBo {
	private String testerName;
	private String exeCount;
	private String exeDate;
	private String exeTime;
	private String exePass;
	private String exeFail;
	private String prepCount;
	private String prepTime;
	private String toolName;
	
	public String getTesterName() {
		return testerName;
	}
	public void setTesterName(String testerName) {
		this.testerName = testerName;
	}
	public String getExeCount() {
		return exeCount;
	}
	public void setExeCount(String exeCount) {
		this.exeCount = exeCount;
	}
	public String getExeDate() {
		return exeDate;
	}
	public void setExeDate(String exeDate) {
		this.exeDate = exeDate;
	}
	public String getExeTime() {
		return exeTime;
	}
	public void setExeTime(String exeTime) {
		this.exeTime = exeTime;
	}
	public String getExePass() {
		return exePass;
	}
	public void setExePass(String exePass) {
		this.exePass = exePass;
	}
	public String getExeFail() {
		return exeFail;
	}
	public void setExeFail(String exeFail) {
		this.exeFail = exeFail;
	}
	
	public String getPrepCount() {
		return prepCount;
	}
	public void setPrepCount(String prepCount) {
		this.prepCount = prepCount;
	}
	public String getPrepTime() {
		return prepTime;
	}
	public void setPrepTime(String prepTime) {
		this.prepTime = prepTime;
	}
	public String getToolName() {
		return toolName;
	}
	public void setToolName(String toolName) {
		this.toolName = toolName;
	}
	@Override
	public String toString() {
		return "ResourceResponseBo [testerName=" + testerName + ", exeCount=" + exeCount + ", exeDate=" + exeDate
				+ ", exeTime=" + exeTime + ", exePass=" + exePass + ", exeFail=" + exeFail + ", prepCount=" + prepCount
				+ ", prepTime=" + prepTime + ", toolName=" + toolName + "]";
	}
	
	
	

}
