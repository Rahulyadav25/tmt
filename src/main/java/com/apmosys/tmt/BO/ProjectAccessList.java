package com.apmosys.tmt.BO;

import java.util.List;

public class ProjectAccessList {
 private List<String> manager;
 private List<String> tester;
 private List<String> readonly;
 private String userid;
public List<String> getManager() {
	return manager;
}
public void setManager(List<String> manager) {
	this.manager = manager;
}
public List<String> getTester() {
	return tester;
}
public void setTester(List<String> tester) {
	this.tester = tester;
}
public List<String> getReadonly() {
	return readonly;
}
public void setReadonly(List<String> readonly) {
	this.readonly = readonly;
}
public String getUserid() {
	return userid;
}
public void setUserid(String userid) {
	this.userid = userid;
}
@Override
public String toString() {
	return "ProjectAccessList [manager=" + manager + ", tester=" + tester + ", readonly=" + readonly + ", userid="
			+ userid + "]";
}
 
 
}
