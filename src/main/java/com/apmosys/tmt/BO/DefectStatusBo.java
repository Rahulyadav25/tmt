package com.apmosys.tmt.BO;

public class DefectStatusBo {
private Integer critical=0;
private Integer major=0;
private Integer minor=0;
private Integer blocker=0;
public Integer getCritical() {
	return critical;
}
public void setCritical(Integer critical) {
	this.critical = critical;
}
public Integer getMajor() {
	return major;
}
public void setMajor(Integer major) {
	this.major = major;
}
public Integer getMinor() {
	return minor;
}
public void setMinor(Integer minor) {
	this.minor = minor;
}
public Integer getBlocker() {
	return blocker;
}
public void setBlocker(Integer blocker) {
	this.blocker = blocker;
}

}
