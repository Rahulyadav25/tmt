package com.apmosys.tmt.BO;



public class RedmineDTO {

	private Integer projectId;
	private Integer trackerId;
	private String subject;
	private String description;
	private Integer statusId;
	private Integer priorityId;
	private String startDate;
	private Integer assignedToId;
	private Integer toolid;
	private Integer tmtProjectID;
	private String identifier;
	private String statusName;
	
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public Integer getTrackerId() {
		return trackerId;
	}
	public void setTrackerId(Integer trackerId) {
		this.trackerId = trackerId;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getStatusId() {
		return statusId;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public Integer getPriorityId() {
		return priorityId;
	}
	public void setPriorityId(Integer priorityId) {
		this.priorityId = priorityId;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public Integer getAssignedToId() {
		return assignedToId;
	}
	public void setAssignedToId(Integer assignedToId) {
		this.assignedToId = assignedToId;
	}
	public Integer getToolid() {
		return toolid;
	}
	public void setToolid(Integer toolid) {
		this.toolid = toolid;
	}
	public Integer getTmtProjectID() {
		return tmtProjectID;
	}
	public void setTmtProjectID(Integer tmtProjectID) {
		this.tmtProjectID = tmtProjectID;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	@Override
	public String toString() {
		return "RedmineDTO [projectId=" + projectId + ", trackerId=" + trackerId + ", subject=" + subject
				+ ", description=" + description + ", statusId=" + statusId + ", priorityId=" + priorityId
				+ ", startDate=" + startDate + ", assignedToId=" + assignedToId + ", toolid=" + toolid
				+ ", tmtProjectID=" + tmtProjectID + ", identifier=" + identifier + ", statusName=" + statusName + "]";
	}
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	
}
