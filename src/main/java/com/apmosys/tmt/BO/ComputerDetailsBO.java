package com.apmosys.tmt.BO;

public class ComputerDetailsBO {
private String computerName;
private Boolean isOffline;
private String computerOS;
public String getComputerName() {
	return computerName;
}
public void setComputerName(String computerName) {
	this.computerName = computerName;
}
public Boolean getIsOffline() {
	return isOffline;
}
public void setIsOffline(Boolean isOffline) {
	this.isOffline = isOffline;
}
public String getComputerOS() {
	return computerOS;
}
public void setComputerOS(String l) {
	this.computerOS = l;
}

}
