package com.apmosys.tmt.BO;

public class OverallRunStatus {
	
	private Integer passCount=0;;
	private Integer failCount=0;
	private Integer untestedCount=0;
	private Integer Retest=0;
	private Integer Blocker=0;
	public Integer getPassCount() {
		return passCount;
	}
	public void setPassCount(Integer passCount) {
		this.passCount = passCount;
	}
	public Integer getFailCount() {
		return failCount;
	}
	public void setFailCount(Integer failCount) {
		this.failCount = failCount;
	}
	public Integer getUntestedCount() {
		return untestedCount;
	}
	public void setUntestedCount(Integer untestedCount) {
		this.untestedCount = untestedCount;
	}
	public Integer getRetest() {
		return Retest;
	}
	public void setRetest(Integer retest) {
		Retest = retest;
	}
	public Integer getBlocker() {
		return Blocker;
	}
	public void setBlocker(Integer blocker) {
		Blocker = blocker;
	}
	
	
}
