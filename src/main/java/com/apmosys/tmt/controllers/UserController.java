package com.apmosys.tmt.controllers;
/*package com.apmosys.pheonix.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.apmosys.pheonix.models.User;
import com.apmosys.pheonix.services.UserService;
import com.apmosys.pheonix.utils.ServiceResponse;


@RestController
@RequestMapping(path="/api")
public class UserController {

	
	@Autowired
	private UserService userService;
	
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public ServiceResponse loginUser(@RequestBody User user,HttpServletRequest request) throws Exception {
		String remoteAddr=null;
		if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            System.out.println(request.getLocalAddr()+"  "+request.getScheme()+" "+request.getRemoteAddr());
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
                
            }
        }
		System.out.println("welcome to portal");
		ServiceResponse response=new ServiceResponse();
		response= userService.userLogin(user,remoteAddr);
		
		return response;

	}

	
	@RequestMapping(value="/getAllUsers", method=RequestMethod.POST)
	public ServiceResponse getAllUsers(){
		
		ServiceResponse serviceResponse=new ServiceResponse();
		serviceResponse=userService.getAllUsers();
		return serviceResponse;
	}
	
	@RequestMapping(value="/getAllUsers", method=RequestMethod.POST)
	public ServiceResponse getAllUsers(){
		
		ServiceResponse serviceResponse=new ServiceResponse();
		serviceResponse=userService.getAllUsers();
		return serviceResponse;
	}
	
	
	@RequestMapping(value="/saveUser", method=RequestMethod.POST)
	public ServiceResponse saveUser(@RequestBody User user){
		
		ServiceResponse serviceResponse=new ServiceResponse();
		serviceResponse=userService.saveUser(user);
		return serviceResponse;
	}
	
	
	
	@RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
	public ServiceResponse deleteUser(@RequestBody User user) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = userService.deleteUser(user);
		return serviceResponse;
	}
	
	
	@RequestMapping(value="/getADUser", method=RequestMethod.POST)
	public ServiceResponse getADUser(@RequestParam("userId") String userId){
		
		ServiceResponse serviceResponse=new ServiceResponse();
		serviceResponse=userService.getADUser(userId);
		return serviceResponse;
	}
	
	
	
}
*/