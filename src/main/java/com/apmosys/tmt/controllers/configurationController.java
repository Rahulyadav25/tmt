package com.apmosys.tmt.controllers;

import java.text.ParseException;
import java.util.HashMap;

//import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.apmosys.tmt.Codec;
import com.apmosys.tmt.BO.ProjectAccessList;
import com.apmosys.tmt.models.BugTrackorToolEntity;
import com.apmosys.tmt.models.ProjectAccessEntity;
import com.apmosys.tmt.models.UserDetailsEntity;
/*import com.apmosys.tmt.models.UserBo;*/
import com.apmosys.tmt.models.userRole;
/*import com.apmosys.tmt.repository.UserDetailsEntity;*/
import com.apmosys.tmt.services.configurationService;
import com.apmosys.tmt.utils.CustomLogger;
import com.apmosys.tmt.utils.ServiceResponse;

import jakarta.servlet.http.HttpSession;

@RestController
@RequestMapping(path = "/api")
public class configurationController {
//	Logger log = LoggerFactory.getLogger(configurationController.class.getName());

	@Autowired
	private Codec codec;
	@Autowired
	private HttpSession session;
	@Autowired
	private configurationService configService;
	@Autowired
	CustomLogger CustomLogger;
	
	private HashMap<String, Object> serviceRequestInputs;

	@RequestMapping(value = "/getAllUser", method = RequestMethod.POST)
	public ServiceResponse getManualChart(int id, int role) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.getAllUser(id, role);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("id",id);
		serviceRequestInputs.put("role",role);
		CustomLogger.logMyInfo("configurationController.getManualChart() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/getUserbyid", method = RequestMethod.POST)
	public ServiceResponse getUserbyid(String userid) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.getUserDetailsbyid(userid);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("userid",userid);
		CustomLogger.logMyInfo("configurationController.getUserbyid() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/createUser", method = RequestMethod.POST)
	public ServiceResponse createUser(@RequestHeader("Authorization") String token, String mailid, String name,
			String usertype, String creator, String userid) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				String creatorid = configService.getUseridbyname(creator);
				ServiceResponse serviceResponse = new ServiceResponse();
				userRole user = new userRole();
				user.setUsername(mailid);
				user.setName(name);
				user.setUserrole(usertype);
				/* user.setPassword("Apmosys@123"); */
				user.setCreatorid(creatorid);
				user.setUserCreatedBy(creator);
				user.setUserid(userid);
				serviceResponse = configService.createuser(user);
				//return response;
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("mailid",mailid);
				serviceRequestInputs.put("name",name);
				serviceRequestInputs.put("creator",creator);
				serviceRequestInputs.put("usertype",usertype);
				serviceRequestInputs.put("userid",userid);
				CustomLogger.logMyInfo("configurationController.createUser() ",serviceRequestInputs, session , serviceResponse );
				return serviceResponse;
			} else {
				return null;
			}
		} catch (Exception e) {
			System.out.print(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	public ServiceResponse updateUser(@RequestHeader("Authorization") String token, String userid, String username,
			String name, String usertype, String activation) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse serviceResponse = new ServiceResponse();
				userRole user = new userRole();
				user.setUsername(username);
				user.setName(name);
				user.setUserid(userid);
				user.setUserrole(usertype);
				user.setIsactive(activation);
				serviceResponse = configService.updateUser(user);
				//return response;
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("userid",userid);
				serviceRequestInputs.put("name",name);
				serviceRequestInputs.put("username",username);
				serviceRequestInputs.put("usertype",usertype);
				serviceRequestInputs.put("userid",userid);
				serviceRequestInputs.put("activation",activation);
				CustomLogger.logMyInfo("configurationController.updateUser() ",serviceRequestInputs, session , serviceResponse );
				return serviceResponse;
			} else {
				return null;
			}
		} catch (Exception e) {
			System.out.print(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
	public ServiceResponse deleteUser(@RequestHeader("Authorization") String token, int userid) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {

				ServiceResponse serviceResponse = new ServiceResponse();
				/*
				 * userRole user = new userRole(); user.setId(Integer.valueOf(userid));
				 */
				serviceResponse = configService.deleteUser(userid);
				//return response;
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("userid",userid);
				CustomLogger.logMyInfo("configurationController.deleteUser() ",serviceRequestInputs, session , serviceResponse );
				return serviceResponse;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/getUserAccessList", method = RequestMethod.POST)
	public ServiceResponse getUserAccessList(int userid, int roleid) {
		//System.out.println("+++++++getUserAccessList+++++++++++++++++++++userid" + userid);
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.getAccessList(userid, roleid);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("userid",userid);
		serviceRequestInputs.put("roleid",roleid);
		CustomLogger.logMyInfo("configurationController.getUserbyid() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/saveaccess", method = RequestMethod.POST)
	public ServiceResponse saveaccess(@RequestBody ProjectAccessList accessList) {
		ServiceResponse serviceResponse = new ServiceResponse();
		//System.out.println(accessList);
		serviceResponse = configService.saveaccess(accessList);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("accessList",accessList);
		CustomLogger.logMyInfo("configurationController.getUserAccessList() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/saveroleuser", method = RequestMethod.POST)
	public ServiceResponse saveroleuser(String role) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.saverole(role);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("role",role);
		CustomLogger.logMyInfo("configurationController.saveroleuser() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/getAllrole", method = RequestMethod.POST)
	public ServiceResponse getAllrole() {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.getAllrole();
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("configurationController.getAllrole() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/deleterole", method = RequestMethod.POST)
	public ServiceResponse deleterole(int userid) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.deleterole(userid);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("userid",userid);
		CustomLogger.logMyInfo("configurationController.deleterole() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/removeAccess", method = RequestMethod.POST)
	public ServiceResponse removeAccess(String projectid, int userid) {
		//System.out.println("Project ids----------------------------->" + projectid);
		//System.out.println("user ids----------------------------->" + userid);
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.removeAccess(projectid, userid);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("userid",userid);
		serviceRequestInputs.put("projectid",projectid);
		CustomLogger.logMyInfo("configurationController.removeAccess() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/getRoletype", method = RequestMethod.POST)
	public ServiceResponse getRoletype(int role) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.getRoletype(role);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("role",role);
		CustomLogger.logMyInfo("configurationController.getRoletype() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/getallmanager", method = RequestMethod.POST)
	public ServiceResponse getallmanager() {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.getAllmanager();
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("configurationController.getallmanager() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/edituser", method = RequestMethod.POST)
	public ServiceResponse edituser(@RequestHeader("Authorization") String token, int id, String username,
			String isactive, String manager, String name, String userrole) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse serviceResponse = new ServiceResponse();
				userRole user = new userRole();
				user.setUsername(username);
				user.setIsactive(isactive);
				user.setName(name);
				user.setUserrole(userrole);
				user.setCreatorid(manager);
				user.setId(id);
				serviceResponse = configService.EditUser(user);
				//return response;
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("id",id);
				serviceRequestInputs.put("username",username);
				serviceRequestInputs.put("name",name);
				serviceRequestInputs.put("manager",manager);
				serviceRequestInputs.put("isactive",isactive);
				serviceRequestInputs.put("userrole",userrole);
				CustomLogger.logMyInfo("configurationController.edituser() ",serviceRequestInputs, session , serviceResponse );
				return serviceResponse;
			} else {
				return null;
			}
		} catch (Exception e) {
			System.out.print(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/getdetailsbyuserid", method = RequestMethod.POST)
	public ServiceResponse getdetailsbyuserid(int userid) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.getAccessListbyidonly(userid);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("userid",userid);
		CustomLogger.logMyInfo("configurationController.getdetailsbyuserid() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/createbugTrackor", method = RequestMethod.POST)
	public ServiceResponse createbugTrackor(@RequestHeader("Authorization") String token,
			String ip, String name,
			String active, String userid, String password, String udName) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse serviceResponse = new ServiceResponse();
				BugTrackorToolEntity bg = new BugTrackorToolEntity();
				bg.setBugTrackingToolName(name);
				bg.setIP(ip);
				//System.out.println(active);
				bg.setIsactive(active);
				bg.setPassword(password);
				bg.setUserid(userid);
				bg.setUserDefinedToolName(udName);
				serviceResponse = configService.createbugTrackorSer(bg);
				//return response;
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("userid",userid);
				serviceRequestInputs.put("ip",ip);
				serviceRequestInputs.put("active",active);
				serviceRequestInputs.put("name",name);
				serviceRequestInputs.put("password",password);
				serviceRequestInputs.put("udName",udName);
				CustomLogger.logMyInfo("configurationController.createbugTrackor() ",serviceRequestInputs, session , serviceResponse );
				return serviceResponse;
			} else {
				return null;
			}
		} catch (Exception e) {
			System.out.print(e.toString());
		}
		return null;
	}

	@GetMapping(value = "/getIntegratedBugTool")
	public ServiceResponse getIntegratedBugTool() {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.getIntegratedBugTool();
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("configurationController.getIntegratedBugTool()",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/ShowbugTrackor", method = RequestMethod.POST)
	public ServiceResponse ShowbugTrackor() {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.ShowBugTrackor();
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("configurationController.ShowbugTrackor() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/ShowbugTrackorbyID", method = RequestMethod.POST)
	public ServiceResponse ShowbugTrackorbyID(int id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.ShowBugTrackornyid(id);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("id",id);
		CustomLogger.logMyInfo("configurationController.ShowbugTrackorbyID() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/EditbugTrackor", method = RequestMethod.POST)
	public ServiceResponse EditbugTrackor(@RequestHeader("Authorization") String token,
			int id, String ip, String name,
			String active, String userid, String password, String udName) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {

				ServiceResponse serviceResponse = new ServiceResponse();
				BugTrackorToolEntity bg = new BugTrackorToolEntity();
				bg.setBugTrackingToolName(name);
				bg.setIP(ip);
				bg.setIsactive(active);
				bg.setToolid(id);
				bg.setPassword(password);
				bg.setUserid(userid);
				bg.setUserDefinedToolName(udName);
				serviceResponse = configService.EditbugTrackor(bg);
				//return response;
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("userid",userid);
				serviceRequestInputs.put("ip",ip);
				serviceRequestInputs.put("active",active);
				serviceRequestInputs.put("name",name);
				serviceRequestInputs.put("password",password);
				serviceRequestInputs.put("udName",udName);
				CustomLogger.logMyInfo("configurationController.EditbugTrackor() ",serviceRequestInputs, session , serviceResponse );
				return serviceResponse;
			} else {
				return null;
			}
		} catch (Exception e) {
			System.out.print(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/ShowbugTrackorActive", method = RequestMethod.POST)
	public ServiceResponse ShowbugTrackorActive() {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.showbugTrackorActive();
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("configurationController.ShowbugTrackorActive() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/deleteBugTool", method = RequestMethod.POST)
	public ServiceResponse deleteBugTool(int id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.deleteBugTool(id);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("id",id);
		CustomLogger.logMyInfo("configurationController.deleteBugTool() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/OverallGraph", method = RequestMethod.POST)
	public ServiceResponse ProjectWiseGraph() {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.OverallGraph();
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("configurationController.ProjectWiseGraph() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/ProjectWiseGraph", method = RequestMethod.POST)
	public ServiceResponse ProjectWiseGraph(int id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.ProjectWiseGraph(id);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("id",id);
		CustomLogger.logMyInfo("configurationController.ProjectWiseGraph() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/RunwiseGraph", method = RequestMethod.POST)
	public ServiceResponse RunwiseGraph(int id) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.RunwiseGraph(id);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("id",id);
		CustomLogger.logMyInfo("configurationController.RunwiseGraph() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/getUserByProjectId", method = RequestMethod.POST)
	public ServiceResponse getUserByProjectId(Integer pid) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.getUserByProjectId(pid);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("pid",pid);
		CustomLogger.logMyInfo("configurationController.getUserByProjectId() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/getadminViewEntitlement", method = RequestMethod.POST)
	public ServiceResponse getadminViewEntitlement(Integer userId, Integer userRole, String projectwiserole,
			String username) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.getadminViewEntitlement(userId, userRole, projectwiserole, username);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("userId",userId);
		serviceRequestInputs.put("userRole",userRole);
		serviceRequestInputs.put("projectwiserole",projectwiserole);
		serviceRequestInputs.put("username",username);
		CustomLogger.logMyInfo("configurationController.getadminViewEntitlement() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/deleteGiveAccessUserEntitlement", method = RequestMethod.POST)
	public ServiceResponse deleteGiveAccessUserEntitlement(Integer projectid, Integer userid, String projectwiserole,
			Integer userrole) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = configService.deleteGiveAccessUserEntitlement(projectid, userid, projectwiserole, userrole);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("userid",userid);
		serviceRequestInputs.put("projectwiserole", projectwiserole);
		serviceRequestInputs.put("userrole",userrole);
		CustomLogger.logMyInfo("configurationController.deleteGiveAccessUserEntitlement() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/giveAccessUserEntitlement", method = RequestMethod.POST)
	public ServiceResponse giveAccessUserEntitlement(Integer projectid, Integer userid, String projectwiserole,
			int projectaccessid, Integer userrole) throws ParseException {
		ServiceResponse serviceResponse = new ServiceResponse();
		ProjectAccessEntity pae = new ProjectAccessEntity();
		pae.setAccesstype(projectwiserole);
		pae.setProjectID(projectid);
		pae.setUserID(userid);
		pae.setSrNo(projectaccessid);
		pae.setUserrole(userrole);
		serviceResponse = configService.giveAccessUserEntitlement(pae);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("userid",userid);
		serviceRequestInputs.put("projectwiserole",projectwiserole);
		serviceRequestInputs.put("projectid",projectid);
		serviceRequestInputs.put("projectaccessid",projectaccessid);
		serviceRequestInputs.put("userrole",userrole);
		CustomLogger.logMyInfo("configurationController.giveAccessUserEntitlement() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

	@RequestMapping(value = "/changeProjectStatus", method = RequestMethod.POST)
	public ServiceResponse changeProjectStatus(Integer projectID, String isActive, Integer user_role)
			throws ParseException {
		ServiceResponse serviceResponse = new ServiceResponse();

		serviceResponse = configService.changeProjectStatus(projectID, isActive, user_role);
		//System.out.println(serviceResponse);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectID",projectID);
		serviceRequestInputs.put("isActive",isActive);
		serviceRequestInputs.put("user_role",user_role);
		CustomLogger.logMyInfo("configurationController.changeProjectStatus() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}
	
	@RequestMapping(value = "/getMyProjectsData", method = RequestMethod.POST)
	public ServiceResponse getMyProjectsData(Integer userid, Integer userRoleID)
			throws ParseException {
		ServiceResponse serviceResponse = new ServiceResponse();

		serviceResponse = configService.getMyProjectsData(userid, userRoleID);
		//System.out.println(serviceResponse);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("userid",userid);
		serviceRequestInputs.put("userRoleID",userRoleID);
		CustomLogger.logMyInfo("configurationController.getMyProjectsData() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}

}
