package com.apmosys.tmt.controllers;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.apmosys.tmt.Codec;
import com.apmosys.tmt.BO.JenkinsProjectDetails;
import com.apmosys.tmt.models.AutomationRunLogs;
import com.apmosys.tmt.models.AutomationScreenshot;
import com.apmosys.tmt.models.UserDetailsEntity;
import com.apmosys.tmt.services.AutomationScriptService;
import com.apmosys.tmt.services.AutomationService;
import com.apmosys.tmt.utils.CustomLogger;
import com.apmosys.tmt.utils.ServiceResponse;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@RestController
@RequestMapping(path = "/api")
public class AutomationController {
	@Autowired
	AutomationScriptService automationScriptService;
	@Autowired
	AutomationService automationService;
	@Autowired
	private Codec codec;
	@Autowired
	private HttpSession session;
	@Value("${automationPath}")
	private String automationPath;
	@Autowired 
	private ServletContext servletContext;
	@Value("${TMT_url}")
	private String jiraIP;
	@Autowired
	CustomLogger CustomLogger;

	HashMap<String, Object> serviceRequestInputs;

	@RequestMapping(value = "/checkControllerFile", method = RequestMethod.POST)
	public ServiceResponse checkControllerFile(@RequestParam("projectName") String projectName,
			@RequestParam("sheetData") String sheetData) {
		ServiceResponse serviceResponse = new ServiceResponse();
		//System.out.println("test...testupload.");
		if(sheetData.equals("test"))
		{
			serviceResponse = automationService.checkControllerFile(projectName);

		}
		else {
			//System.out.println("check data sheet...");
			serviceResponse = automationService.checkDataSheetFile(projectName,sheetData);

		}
		//return response;
		serviceRequestInputs = new HashMap<String,Object>();
		serviceRequestInputs.put("projectName",projectName);
		serviceRequestInputs.put("sheetData",sheetData);
		CustomLogger.logMyInfo("AutomationController.checkControllerFile() ",serviceRequestInputs, session , serviceResponse );

		return serviceResponse;
	}

	@RequestMapping(value = "/uploadControllerFile", method = RequestMethod.POST)
	public ServiceResponse uploadController(@RequestHeader("Authorization") String token,
			@RequestParam("file") MultipartFile file, @RequestParam("projectName") String projectname,
			@RequestParam("projectid") int projectid) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = automationService.uploadControllerAuto(file, projectname, projectid);
		//return response;
		serviceRequestInputs = new HashMap<String,Object>();
		serviceRequestInputs.put("file",file);
		serviceRequestInputs.put("projectname",projectname);
		serviceRequestInputs.put("projectid", projectid);
		CustomLogger.logMyInfo("AutomationController.checkControllerFile() ",serviceRequestInputs, session , serviceResponse );


		return serviceResponse;
	}

	@RequestMapping(value = "/uploadDataSheets", method = RequestMethod.POST)
	public ServiceResponse uploadDataSheets(@RequestParam("datasheetFile") MultipartFile file,
			@RequestParam("projectName") String projectname, @RequestParam("projectid") int projectid) {

		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = automationService.uploadDataSheets(file, projectname, projectid);
		return serviceResponse;
	}

	@RequestMapping(value = "/getProjectAutoFiles", method = RequestMethod.POST)
	public ServiceResponse getProjectAutoFiles(@RequestParam("projectName") String projectName) {

		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = automationService.getProjectAutoFiles(projectName);
		return serviceResponse;
	}

	@RequestMapping(value = "/startscript", method = RequestMethod.POST)
	public ServiceResponse startRun(@RequestHeader("Authorization") String token, @RequestParam Integer pId,
			@RequestParam String pName, @RequestParam String datasheetsList,
			@RequestBody JenkinsProjectDetails projectDetails) {
		ServiceResponse response = null;
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				automationScriptService.updateMainController(datasheetsList, projectDetails);
				response = automationScriptService.startScript(pId, pName);
			} else {

			}
		} catch (Exception e) {
			//			System.out.print(e.toString());
			e.printStackTrace();	
		}
		return response;

	}

	@RequestMapping(value = "/stopscript", method = RequestMethod.POST)
	public ServiceResponse stopRun(@RequestHeader("Authorization") String token, @RequestParam Integer pId,
			@RequestParam String pName) {
		ServiceResponse response = null;
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				response = automationScriptService.stopScript(pId, pName);
			} else {

			}
		} catch (Exception e) {
			System.out.print(e.toString());
		}
		return response;

	}

	@RequestMapping(value = "/getslavemachine", method = RequestMethod.POST)
	public ServiceResponse getSlaveMachine(@RequestHeader("Authorization") String token, @RequestParam Integer pId,
			@RequestParam String pName) {
		ServiceResponse response = null;
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				response = automationScriptService.getSlaveMechine();
			} else {

			}
		} catch (Exception e) {
			System.out.print(e.toString());
		}
		return response;

	}

	@RequestMapping(value = "/gettestSuitesname", method = RequestMethod.POST)
	public ServiceResponse getTestSuitesName(@RequestHeader("Authorization") String token, @RequestParam Integer pId,
			@RequestParam String pName) {
		ServiceResponse response = null;
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				response = automationScriptService.getTestSuitesName(pName);
			} else {

			}
		} catch (Exception e) {
			System.out.print(e.toString());
		}
		return response;

	}

	@RequestMapping(value = "/updateJenkinsProject", method = RequestMethod.POST)
	public ServiceResponse getSlaveMachine(@RequestHeader("Authorization") String token,
			@RequestBody JenkinsProjectDetails projectDetails) {
		ServiceResponse response = null;
		try {
		//	response = automationScriptService.createJenkinsProject(projectDetails);
		} catch (Exception e) {
			System.out.print(e.toString());
		}
		return response;
	}

	@RequestMapping(value = "/getCurrentRun", method = RequestMethod.POST)
	public ServiceResponse getCurrentRun(@RequestHeader("Authorization") String token, @RequestParam Integer pId,
			@RequestParam String pName) {
		ServiceResponse response = null;
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				response = automationScriptService.getCurrentRun(pName);
			} else {

			}
		} catch (Exception e) {
			System.out.print(e.toString());
		}
		return response;

	}

	@RequestMapping(value = "/currentRunDetails", method = RequestMethod.POST)
	public ServiceResponse currentRunDetails(@RequestHeader("Authorization") String token, @RequestParam Integer pId,
			@RequestParam String pName) {
		ServiceResponse response = null;
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				response = automationScriptService.currentRunDetails(pName);
			} else {

			}
		} catch (Exception e) {
			System.out.print(e.toString());
		}
		return response;
	}

	@GetMapping(value = "/downloadAutoFile")
	public ServiceResponse downloadAutoFile(HttpServletResponse servletResponse, @RequestParam String type,
			@RequestParam String fileName, @RequestParam String projectName) {
		ServiceResponse response = null;
		Resource resource = null;
		MediaType mediaType = null;
		byte[] buffer = new byte[1024];
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			/*
			 * String tokenID = codec.decrypt(token); if (tokenID.equals(sestoken)) {
			 */
			resource = automationService.downloadAutoFile(type, fileName, projectName);
			File file = resource.getFile();
			// servletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			String mimeType = servletContext.getMimeType(resource.getFile().getName());
			mediaType = MediaType.parseMediaType(mimeType);
			if (mediaType == null)
				mediaType = MediaType.APPLICATION_OCTET_STREAM;
			servletResponse.setContentType(mediaType.getType());
			servletResponse.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
			servletResponse.setContentLength((int) file.length());
			BufferedInputStream inStream = new BufferedInputStream(new FileInputStream(file));
			BufferedOutputStream outStream = new BufferedOutputStream(servletResponse.getOutputStream());

			int nRead = 0;
			while ((nRead = inStream.read(buffer)) != -1) {
				// servletResponse.getWriter().write(nRead);
				outStream.write(buffer, 0, nRead);
			}
			outStream.flush();
			inStream.close();
			// }

		} catch (Exception e) {
			System.out.print(e.toString());
		}
		return response;
	}

	@GetMapping(value = "/deleteAutomationFile")
	public ServiceResponse deleteAutomationFile(@RequestHeader("Authorization") String token, @RequestParam String type,
			@RequestParam String fileName, @RequestParam String projectName) {

		ServiceResponse response = new ServiceResponse();
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				response = automationService.deleteAutomationFile(type, fileName, projectName);
			} else {

			}
		} catch (Exception e) {
			System.out.print(e.toString());
		}
		return response;

	}

	@GetMapping(value = "/getAutomationRunReport")
	public ServiceResponse getAutomationRunReport(@RequestParam String pName) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = automationService.getAutomationRunReport(pName);
		return serviceResponse;
	}
	//pranik
	@GetMapping(value = "/getDefectReport")
	public ServiceResponse getDefectReport(@RequestParam String pName) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = automationService.getDefectReport(pName);
		return serviceResponse;
	}
	@GetMapping(value = "/getAllDefectReport")
	public ServiceResponse getAllDefectReport(@RequestParam String runID,@RequestParam String projectName) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = automationService.getAllDefectReport(runID,projectName);
		return serviceResponse;
	}
	@RequestMapping(value = "/getExtendedReport", method = RequestMethod.POST)
	public ServiceResponse getExtendedReport(@RequestParam String runId,@RequestParam String scenario) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = automationService.getExtendedReport(runId,scenario);
		return serviceResponse;
	}

	@GetMapping(value = "/getExtentReportScreenshot")
	public ServiceResponse getExtentReportScreenshot(@RequestParam Integer screenshotId) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = automationService.getExtentReportScreenshot(screenshotId);
		return serviceResponse;
	}

	@RequestMapping(value = "/getEnvironmentAnalysis", method = RequestMethod.POST)
	public ServiceResponse getEnvironmentAnalysis(@RequestParam String runId) {
		ServiceResponse serviceResponse = new ServiceResponse();
		serviceResponse = automationService.getEnvironmentAnalysis(runId);
		return serviceResponse;
	}
	@RequestMapping(value = "/exportTestCaseToExcel", method = RequestMethod.POST)
	public void exportTestCaseToExcel(@RequestParam(value = "runId") String runId,
			HttpServletRequest request, 
			HttpServletResponse response) {
		try {
			List<AutomationRunLogs> logs= automationService.getRunLogsByRunID(runId);
			//  Map<String, Object> excelMap = new HashMap<String, Object>();
			//  return new ModelAndView("ExcelReport", UIErrorMessages.DATA, excelMap);
			String fileName = "RandomFile";
			HttpHeaders header = new HttpHeaders();
			header.setContentType(new MediaType("application", "vnd.ms-excel"));
			//		      response.setHeader("Content-disposition", "attachment; filename=" + fileName);
			//method to create Workbook

			Workbook workbook=new XSSFWorkbook() ;//created excel fle
			//set name 
			response.addHeader("Content-Disposition","attachment;filename=DoesntMatter.xlsx");
			//create sheet
			Sheet sheet=workbook.createSheet("TestCases");
			//create 1st row as headings
			Row r1=sheet.createRow(0);//1st row (Header)
			//			HSSFCellStyle s=(HSSFCellStyle) workbook.createCellStyle();
			//			HSSFFont f=(HSSFFont) workbook.createFont();
			//			f.setBold(true);
			//			s.setFont(f);
			//			r1.setRowStyle();

			r1.createCell(0).setCellValue("TestCaseID");
			r1.createCell(1).setCellValue("Description");
			r1.createCell(2).setCellValue("Scenario ID");
			r1.createCell(3).setCellValue("Response Time");
			r1.createCell(4).setCellValue("Expected Result");
			r1.createCell(5).setCellValue("Actual Result");
			r1.createCell(6).setCellValue("Status");
			r1.createCell(7).setCellValue("Timestamp");
			r1.createCell(8).setCellValue("Screenshot Link");
			r1.createCell(9).setCellValue("Browser");
			r1.createCell(10).setCellValue("IP_HOST_ZONE");
			r1.createCell(11).setCellValue("Scenario Description");
			r1.createCell(12).setCellValue("Testcase type");
			r1.createCell(13).setCellValue("Step");

			//CREATE data rows
			int rowVal=1;

			for(AutomationRunLogs l : logs) {

				Row r=sheet.createRow(rowVal);
				r.createCell(0).setCellValue(l.getTestCaseID());
				r.createCell(1).setCellValue(l.getDescription());
				r.createCell(2).setCellValue(l.getScenarioID());
				r.createCell(3).setCellValue(l.getResponseTime());
				r.createCell(4).setCellValue(l.getExpectedResult());
				r.createCell(5).setCellValue(l.getActualResult());
				r.createCell(6).setCellValue(l.getStatus());
				r.createCell(7).setCellValue(l.getTimestamp());
				//http://localhost:9776/TMT/api/getss?id=210699
				r.createCell(8).setCellValue(jiraIP+"api/getss?id="+l.getScreenshotID());
				r.createCell(9).setCellValue(l.getBrowserType());
				r.createCell(10).setCellValue(l.getIp_host_zone());
				r.createCell(11).setCellValue(l.getScenario_description());
				r.createCell(12).setCellValue(l.getTestcaseType());
				r.createCell(13).setCellValue(l.getStep());
				rowVal++;
				//				List<String> omAcc=om.getOrderMAccept();// 	ACCEPT   RETURN
				//				String omAccAsString="";//   ""  "-ACCEPT"  "-ACCEPT-RETURN"
				//				for(String a : omAcc) {
				//					omAccAsString=omAccAsString+ "-"+ a;
			}//for
			try {
				workbook.write(response.getOutputStream());
				workbook.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@GetMapping(value = "/getss")
	public void downloadFile(HttpServletResponse response,
			HttpServletRequest request, @RequestParam Integer id ) throws IOException
	{
		AutomationScreenshot pdf = automationService.getss(id);;

		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", "attachment; filename=screenshot_"+id+".jpg");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.getOutputStream().write(pdf.getScreenshot());
	}



	@RequestMapping(value = "/testConnectionRun", method = RequestMethod.POST)
	public ServiceResponse TestConnectionRun(@RequestHeader("Authorization") String token,
			@RequestParam String pName, @RequestParam String slave, @RequestParam String applicationName ) {
		ServiceResponse response = null;
		try {
			//			automationScriptService.
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				//	automationScriptService.updateMainController(datasheetsList, projectDetails);
				response = automationScriptService.testConnection( pName,slave,applicationName);
			} else {

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;

	}
	@RequestMapping(value = "/getScenarioRunwise", method = RequestMethod.POST)
	public List<String>  getScenarioRunwise(
			@RequestParam String runID) {
		List<String> list = null;
		try {
			//automationScriptService.
				list = automationService.getScenarioRunwise(runID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;

	}
	@RequestMapping(value = "/getTotalScenariosTestCasesSteps1", method = RequestMethod.POST)
	public Map<String, Long>  getTotalScenariosTestCasesStepsRunwise(
			@RequestParam String runID) {
		Map<String, Long> list = null;
		try {
			//automationScriptService.
			list = automationService.getTotalScenariosTestCasesStepsCountsLogwise(runID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
		
	}
	/*
	@RequestMapping(value = "/getTotalScenariosTestCasesSteps", method = RequestMethod.POST)
	public Map<String,Integer>  getTotalScenariosTestCasesStepsRunwise1(
			@RequestParam String runID) {
		Map<String,Integer> list = null;
		try {
			
			//automationScriptService.g 
			list = automationService.getTotalScenariosTestCasesStepsCounts1(runID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
		
	}
	 */

}
