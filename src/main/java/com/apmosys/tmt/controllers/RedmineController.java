package com.apmosys.tmt.controllers;

import java.util.HashMap;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.apmosys.tmt.BO.RedmineDTO;
import com.apmosys.tmt.services.RedmineService;
import com.apmosys.tmt.utils.CustomLogger;
import com.apmosys.tmt.utils.ServiceResponse;

import jakarta.servlet.http.HttpSession;



@RestController
@RequestMapping(path = "/api")
public class RedmineController {
	
	@Autowired
	private RedmineService redmineService;
	
	private HashMap<String, Object> serviceRequestInputs;
	
	@Autowired
	CustomLogger CustomLogger;
	
	@Autowired
	private HttpSession session;

	@RequestMapping(path = "/createIssueByProjectId", method = RequestMethod.POST)
	public ServiceResponse createIssueByProjectId(@RequestBody RedmineDTO redmineDTO) {
		
		System.out.println(redmineDTO);
		
		ServiceResponse response = redmineService.createIssueByProjectId(redmineDTO);
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("RedmineController.createIssueByProjectId() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(path = "/getAllRedmineProjects", method = RequestMethod.POST)
	public ServiceResponse getAllRedmineProjects(@RequestBody RedmineDTO redmineDTO) {
		
		
		ServiceResponse response = redmineService.getAllRedmineProjects(redmineDTO);
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("RedmineController.getAllRedmineProjects() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(path = "/getAllRedmineTrackers", method = RequestMethod.POST)
	public ServiceResponse getAllRedmineTrackers(@RequestBody RedmineDTO redmineDTO) {
		
		
		ServiceResponse response = redmineService.getAllRedmineTrackers(redmineDTO);
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("RedmineController.getAllRedmineTrackers() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(path = "/getAllRedmineStatuses", method = RequestMethod.POST)
	public ServiceResponse getAllRedmineStatuses(@RequestBody RedmineDTO redmineDTO) {
		
		
		ServiceResponse response = redmineService.getAllRedmineStatuses(redmineDTO);
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("RedmineController.getAllRedmineStatuses() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(path = "/getAllRedmineIssuePriorities", method = RequestMethod.POST)
	public ServiceResponse getAllRedmineIssuePriorities(@RequestBody RedmineDTO redmineDTO) {
		
		
		ServiceResponse response = redmineService.getAllRedmineIssuePriorities(redmineDTO);
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("RedmineController.getAllRedmineIssuePriorities() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(path = "/getAllRedmineUsers", method = RequestMethod.POST)
	public ServiceResponse getAllRedmineUsers(@RequestBody RedmineDTO redmineDTO) {
		
		
		ServiceResponse response = redmineService.getAllRedmineUsers(redmineDTO);
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("RedmineController.getAllRedmineUsers() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(path = "/getRedmineProjectByIdentifier", method = RequestMethod.POST)
	public ServiceResponse getRedmineProjectByIdentifier(@RequestBody RedmineDTO redmineDTO) {
		
		
		ServiceResponse response = redmineService.getRedmineProjectByIdentifier(redmineDTO);
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("RedmineController.getRedmineProjectByIdentifier() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(path = "/getProjectIssues", method = RequestMethod.POST)
	public ServiceResponse getProjectIssues(@RequestBody RedmineDTO redmineDTO) {
		
		ServiceResponse response = redmineService.getProjectIssues(redmineDTO);
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("RedmineController.getProjectIssues() ",serviceRequestInputs, session , response );
		return response;
	}
	
	@RequestMapping(path = "/getProjectIssuesStatus", method = RequestMethod.POST)
	public ServiceResponse getProjectIssuesStatus(@RequestBody RedmineDTO redmineDTO) {
		
		ServiceResponse response = redmineService.getProjectIssuesStatus(redmineDTO);
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("RedmineController.getProjectIssuesStatus() ",serviceRequestInputs, session , response );
		return response;
	}
	
	
}
