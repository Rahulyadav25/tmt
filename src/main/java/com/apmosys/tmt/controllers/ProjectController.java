package com.apmosys.tmt.controllers;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

//import javax.servlet.ServletContext;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.apmosys.tmt.Codec;
import com.apmosys.tmt.BO.CreateRunBo;
import com.apmosys.tmt.BO.ExecutionReportAutomation;
import com.apmosys.tmt.BO.ResourceRequestBo;
import com.apmosys.tmt.models.AttachmentFetch;
import com.apmosys.tmt.models.AutomationTestCaseExecutionLogs;
import com.apmosys.tmt.models.MannualReRunTable;
import com.apmosys.tmt.models.MannualRunTable;
import com.apmosys.tmt.models.MannualTestCaseExecutionLogs;
import com.apmosys.tmt.models.MannualTestCaseTable;
import com.apmosys.tmt.models.MannualTestGroup;
import com.apmosys.tmt.models.MannualTestSuites;
import com.apmosys.tmt.models.ProjectDeatilsMeta;
import com.apmosys.tmt.models.ProjectDocument;
import com.apmosys.tmt.models.RobosoftJiraAutomationModel;
import com.apmosys.tmt.models.RunWiseBusStatusForExport;
import com.apmosys.tmt.models.UserDetailsEntity;
import com.apmosys.tmt.repository.BugTrackerRepository;
import com.apmosys.tmt.repository.ProjectRepository;
import com.apmosys.tmt.services.BugZillaService;
import com.apmosys.tmt.services.JiraService;
import com.apmosys.tmt.services.ProjectService;
import com.apmosys.tmt.utils.CustomLogger;
import com.apmosys.tmt.utils.ServiceResponse;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@RestController
@RequestMapping(path = "/api")
public class ProjectController {
	@Autowired
	private Codec codec;
	@Autowired
	private HttpSession session;
	@Autowired
	ProjectService projectService;
	@Autowired
	private ServletContext servletContext;
	@Autowired
	BugZillaService buzilaService;
	private HashMap<String, Object> serviceRequestInputs;
	
	@Autowired
	private JiraService jiraService;
	
	@Autowired
	CustomLogger CustomLogger;
	
	@Autowired
	ProjectRepository projectRepository;
	
	@Autowired
	private BugTrackerRepository bugRepo;



	@RequestMapping(value = "/updateBugDetailsAPI", method = RequestMethod.POST)
    public ServiceResponse updateBugDetailsAPI(@RequestHeader("Authorization") String token,
			@RequestParam String projectName, @RequestParam("projectID") Integer projectID) {
//projectName =[proj] ;  projectID =[1234];
		ServiceResponse response = new ServiceResponse();
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				response=projectService.updateBugDetailsAPI(projectName, projectID);
				//return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
			response.setServiceError("failed");
		}
		return response;
	}
	
		@RequestMapping(value = "/getProjectBugStatus", method = RequestMethod.POST)
	public ServiceResponse getProjectBugStatus(@RequestHeader("Authorization") String token,
			@RequestParam String projectName, @RequestParam("projectID") Integer projectID) {
		ServiceResponse response = new ServiceResponse();
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				////System.out.printlnln("Set value");
				response=projectService.getProjectBugStatus(projectName, projectID);
				////System.out.printlnln("Set value 1.....");
				//return response;
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("projectName",projectName);
				serviceRequestInputs.put("projectID",projectID);
				CustomLogger.logMyInfo("ProjectController.getProjectBugStatus() ", serviceRequestInputs, session , response );
			} else {
				//return null;
			}
		} catch (Exception e) {
//			//System.out.println(e.toString());
			response.setServiceError("failed");
		}
		return response;
	}
		
	@RequestMapping(value = "/getBugzillaBugsProductWise", method = RequestMethod.POST)
	public ServiceResponse getBugzillaBugsProductWise(@RequestHeader("Authorization") String token,
			@RequestParam String projectName, @RequestParam("projectID") Integer projectID) {
		ServiceResponse response = new ServiceResponse();
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				response=projectService.getBugzillaBugsProductWise(projectName, projectID);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("projectName",projectName);
				serviceRequestInputs.put("projectID",projectID);
				CustomLogger.logMyInfo("ProjectController.getBugzillaBugsProductWise() ",serviceRequestInputs, session , response );
			} else {
				//return null;
			}
		} catch (Exception e) {
			System.out.println(e.toString());
			response.setServiceError("failed");
		}
		//return response;
		return response;
	}
	//for testing only
	@RequestMapping(value = "/getBugzillaBugsProductWise1", method = RequestMethod.POST)
	public ServiceResponse getBugzillaBugsProductWise1(
			@RequestParam String projectName, @RequestParam("projectID") Integer projectID) {
		ServiceResponse response = new ServiceResponse();
		try {
				response=projectService.getBugzillaBugsProductWise(projectName, projectID);
		} catch (Exception e) {
			//System.out.println(e.toString());
			response.setServiceError("failed");
		}
		//return response;
		return response;
	}
	
	@RequestMapping(value = "/createproject", method = RequestMethod.POST)
	public ServiceResponse createproject(@RequestHeader("Authorization") String token,
			@RequestBody ProjectDeatilsMeta projectDeatilsMeta, int userid) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				////System.out.printlnln();
				////System.out.printlnln("projectDeatilsMeta...create controller.."+projectDeatilsMeta.getProjectStartDate()+"projectDeatilsMeta.."+projectDeatilsMeta.getProjectEndDate());
				ServiceResponse response = projectService.saveProject(projectDeatilsMeta, userid);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("projectDeatilsMeta",projectDeatilsMeta);
				CustomLogger.logMyInfo("ProjectController.createproject() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}
	
	@RequestMapping(value = "/uploadPreview", method = RequestMethod.POST)
	public ServiceResponse uploadPreview(@RequestHeader("Authorization") String token,
	@RequestParam("file") MultipartFile file, @RequestParam String suiteName,@RequestParam String runName ,
	@RequestParam String projectId,@RequestParam String sameTestSuite) {
	try {
	UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
	String sestoken = detailsEntity.getTokenID();
	String tokenID = codec.decrypt(token);
	if (tokenID.equals(sestoken)) {
	ServiceResponse response = null;
	MannualRunTable testRun = new MannualRunTable();
	if (projectId != null) {
	testRun.setSuiteName(suiteName);
	testRun.setProjectId(Integer.parseInt(projectId));
	testRun.setRunName(runName);
	response = projectService.uploadPreview(file, testRun,sameTestSuite);
	serviceRequestInputs=new HashMap<String,Object>();
	serviceRequestInputs.put("file",file);
	serviceRequestInputs.put("suiteName",suiteName);
	serviceRequestInputs.put("runName",runName);
	serviceRequestInputs.put("projectId",projectId);
	serviceRequestInputs.put("sameTestSuite",sameTestSuite);
	CustomLogger.logMyInfo("ProjectController.uploadPreview() ",serviceRequestInputs, session , response );
	}


	// ServiceResponse response=null;/*projectService.createTestSuites(suites);*/
	return response;
	} else {
	return null;
	}
	} catch (Exception e) {
	//System.out.println(e.toString());
	}
	return null;
	}
	
	@RequestMapping(value = "/uploadTestExecution", method = RequestMethod.POST)
	public ServiceResponse uploadTestExecution(@RequestHeader("Authorization") String token,
	@RequestParam("file") MultipartFile file, @RequestParam String suiteName, @RequestParam String runName ,
	@RequestParam String projectId,
	@RequestParam String createdBy, @RequestParam String  testSuitesId,
	@RequestParam String  testCaseList,
	@RequestParam String round,@RequestParam String  targetEndDate,
	@RequestParam String isActive,
	@RequestParam String description, @RequestParam String sameTestSuite) {
	try {
	UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
	String sestoken = detailsEntity.getTokenID();
	String tokenID = codec.decrypt(token);
	if (tokenID.equals(sestoken)) {
	ServiceResponse response = null;
	MannualRunTable testRun = new MannualRunTable();
	if (projectId != null) {

	testRun.setProjectId(Integer.parseInt(projectId));
	testRun.setIsActive(isActive);
	testRun.setCreatedBy(createdBy);
	testRun.setTestSuitesId(Integer.parseInt(testSuitesId));
	testRun.setRunName(runName);
	testRun.setSuiteName(suiteName);
	response = projectService.uploadTestRunExecution(file, testRun, round,targetEndDate,description,sameTestSuite);
	serviceRequestInputs=new HashMap<String,Object>();
	serviceRequestInputs.put("file",file);
	serviceRequestInputs.put("suiteName",suiteName);
	serviceRequestInputs.put("runName",runName);
	serviceRequestInputs.put("projectId",projectId);
	serviceRequestInputs.put("createdBy",createdBy);
	serviceRequestInputs.put("testSuitesId",testSuitesId);
	serviceRequestInputs.put("testCaseList",testCaseList);
	serviceRequestInputs.put("targetEndDate",targetEndDate);
	serviceRequestInputs.put("isActive",isActive);
	serviceRequestInputs.put("description",description);
	serviceRequestInputs.put("sameTestSuite",sameTestSuite);
	CustomLogger.logMyInfo("ProjectController.uploadTestExecution() ",serviceRequestInputs, session , response );
	}
	// ServiceResponse response=null;/*projectService.createTestSuites(suites);*/
	return response;
	} else {
	return null;
	}
	} catch (Exception e) {
	//System.out.println(e.toString());
	}
	return null;
	}
	
	@RequestMapping(value = "/getAllProjectName", method = RequestMethod.POST)
	public ServiceResponse getAllProjectName() {
		ServiceResponse response = projectService.getAllProjectName();
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("ProjectController.getAllProjectName() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getUserListOfResouce", method = RequestMethod.POST)
	public ServiceResponse getUserListOfResouce() {
		ServiceResponse response = projectService.getUserListOfResouce();
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("ProjectController.getUserListOfResouce() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/getUserListOfResouceByProjectIDs", method = RequestMethod.POST)
	public ServiceResponse getUserListOfResouceByProjectIDs(@RequestParam List<Integer> pIDs
			,@RequestParam Integer userRole, @RequestParam Integer userID) {
		ServiceResponse response = projectService.getUserListOfResouceByProjectIDs(pIDs, userRole, userID);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("pIDs",pIDs);
		CustomLogger.logMyInfo("ProjectController.getUserListOfResouceByProjectIDs() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/getDatewiseUserForResouces", method = RequestMethod.POST)
	public ServiceResponse getDatewiseUserForResouces(@RequestBody ResourceRequestBo resourceRequestBo) {
		//////System.out.printlnln(resourceRequestBo);
		ServiceResponse response = projectService.getDatewiseUserForResouces(resourceRequestBo);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("resourceRequestBo ",resourceRequestBo);
		CustomLogger.logMyInfo("ProjectController.getDatewiseUserForResouces() ",serviceRequestInputs, session , response );
		return response;

	}
	
//	ResourceRequestBo={
//		fromDate:12,
//		runList : [13,12,32,4,4],
//		c:'abc'
//	};
	@RequestMapping(value = "/getResorcesEffort", method = RequestMethod.POST)
	public ServiceResponse getResorcesEffort(@RequestBody ResourceRequestBo resourceRequestBo) {
		ServiceResponse response = projectService.getResorcesEffort(resourceRequestBo);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("resourceRequestBo ",resourceRequestBo);
		CustomLogger.logMyInfo("ProjectController.getResorcesEffort() ",serviceRequestInputs, session , response );
		return response;

	}




	
	
	@RequestMapping(value = "/uploadTestExecutions", method = RequestMethod.POST)
	public ServiceResponse uploadTestExecutions(@RequestHeader("Authorization") String token,
	@RequestBody List<MannualTestCaseExecutionLogs> mannualTestCaseExecutionLogs, @RequestParam String testSuitesId ,
	@RequestParam String userId, @RequestParam String projectId,@RequestParam String round,
	@RequestParam String runName ,@RequestParam String testSuitesName,@RequestParam String  targetEndDate,@RequestParam String description,
	@RequestParam String isActive) {
		ServiceResponse response = null;
		try {
	UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
	String sestoken = detailsEntity.getTokenID();
	String tokenID = codec.decrypt(token);
	if (tokenID.equals(sestoken)) {

	MannualRunTable testRun = new MannualRunTable();
	////System.out.printlnln("the store the data in project controller");
	////System.out.printlnln("Target Date is ... project controller..."+targetEndDate);
	
	if (projectId != null) {
	testRun.setProjectId(Integer.parseInt(projectId));
	testRun.setIsActive(isActive);
	testRun.setCreatedBy(userId);
	testRun.setTestSuitesId(Integer.parseInt(testSuitesId));
	testRun.setRunName(runName);
	testRun.setSuiteName(testSuitesName);
	response = projectService.uploadTestRunExecutions(mannualTestCaseExecutionLogs, testRun, round,targetEndDate,description,isActive);
	}
	// ServiceResponse response=null;/*projectService.createTestSuites(suites);*/
	serviceRequestInputs=new HashMap<String,Object>();
	serviceRequestInputs.put("mannualTestCaseExecutionLogs ",mannualTestCaseExecutionLogs);
	serviceRequestInputs.put("testSuitesId ",testSuitesId);
	serviceRequestInputs.put("userId ",userId);
	serviceRequestInputs.put("projectId ",projectId);
	serviceRequestInputs.put("round ",round);
	serviceRequestInputs.put("runName ",runName);
	serviceRequestInputs.put("testSuitesName ",testSuitesName);
	serviceRequestInputs.put("runName ",runName);
	serviceRequestInputs.put("targetEndDate ",targetEndDate);
	serviceRequestInputs.put("description ",description);
	serviceRequestInputs.put("isActive ",isActive);
	CustomLogger.logMyInfo("ProjectController.uploadTestExecutions() ",serviceRequestInputs, session , response );
	return response;
	} else {
	return null;
	}
	} catch (Exception e) {
	//System.out.println(e.toString());
	}	
	return response;
	}
	
	@RequestMapping(value = "/createtestsuites", method = RequestMethod.POST)
	public ServiceResponse creaTetestSuites(@RequestHeader("Authorization") String token,
			@RequestBody MannualTestSuites suites) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.createTestSuites(suites);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("suites ",suites);
				CustomLogger.logMyInfo("ProjectController.creaTetestSuites() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}
	
	@RequestMapping(value = "/retrieveSameTestSuite", method = RequestMethod.POST)
	public ServiceResponse retrieveTestSuite(@RequestHeader("Authorization") String token,
			@RequestBody MannualTestSuites suites, @RequestParam String decision) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) 
			{
				ServiceResponse response = projectService.retrieveTestSuite(suites, decision);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("suites ",suites);
				serviceRequestInputs.put("decision ",decision);
				CustomLogger.logMyInfo("ProjectController.retrieveTestSuite() ",serviceRequestInputs, session , response );
				return response;
			} 
			else 
			{
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}
	

@RequestMapping(value = "/createtestsuitesandcases", method = RequestMethod.POST)
	public ServiceResponse creaTetestSuites(
//			@RequestHeader("Authorization") String token,
			@RequestParam("file") MultipartFile file, @RequestParam String projectId,
			@RequestParam String testSuitesName, @RequestParam String createdBy, @RequestParam String isActive) {
		try {
			int a = 10;
//			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
//			String sestoken = detailsEntity.getTokenID();
//			String tokenID = codec.decrypt(token);
//			System.out.println("ProjectService.uploadTestSuites()...........uploading 1001");
//			if (tokenID.equals(sestoken)) {
			if (a==10) {
				ServiceResponse response = null;
				MannualTestSuites testSuites = new MannualTestSuites();
				if (projectId != null) {
//					System.out.println("ProjectService.uploadTestSuites()...........uploading 1002");
					testSuites.setProjectId(Integer.parseInt(projectId));
					testSuites.setIsActive(isActive);
					testSuites.setCreatedBy(createdBy);
					testSuites.setTestSuitesName(testSuitesName);
					response = projectService.uploadTestSuites(file, testSuites);
//					System.out.println("ProjectService.uploadTestSuites()...........uploading 1003");
				}
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("file ",file);
				serviceRequestInputs.put("projectId ",projectId);
				serviceRequestInputs.put("testSuitesName ",testSuitesName);
				serviceRequestInputs.put("createdBy ",createdBy);
				serviceRequestInputs.put("isActive ",isActive);
				CustomLogger.logMyInfo("ProjectController.creaTetestSuites() ",serviceRequestInputs, session , response );
//				System.out.println("ProjectService.uploadTestSuites()...........uploading 1004 response "+response);
				// ServiceResponse response=null;/*projectService.createTestSuites(suites);*/
				return response;
			}
		else {
				return null;
			}
		} catch (Exception e) {
			System.out.println("ProjectService.uploadTestSuites()...........uploading 100");

			e.printStackTrace();
		}
		return null;
	}
	
@RequestMapping(value = "/importTestCases", method = RequestMethod.POST)
public ServiceResponse importTestCases(@RequestHeader("Authorization") String token,
		@RequestParam("file") MultipartFile file, @RequestParam String groupId, @RequestParam String uploadType,
		@RequestParam String suiteNameFS) {
	try {
		UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
		String sestoken = detailsEntity.getTokenID();
		String tokenID = codec.decrypt(token);
		if (tokenID.equals(sestoken)) {
			ServiceResponse response = null;
			if (groupId != null) {
				if (uploadType.equals("append")) {
					response = projectService.importTestCases(file, groupId, uploadType,suiteNameFS);
				} else if (uploadType.equals("replace")) {
					response = projectService.importTestCases(file, groupId, uploadType,suiteNameFS);
				} else {

				}
			}

			// ServiceResponse response=null;/*projectService.createTestSuites(suites);*/
			serviceRequestInputs=new HashMap<String,Object>();
			serviceRequestInputs.put("file ",file);
			serviceRequestInputs.put("groupId ",groupId);
			serviceRequestInputs.put("uploadType ",uploadType);
			serviceRequestInputs.put("suiteNameFS ",suiteNameFS);
			CustomLogger.logMyInfo("ProjectController.importTestCases() ",serviceRequestInputs, session , response );
			return response;
		} else {
			return null;
		}
	} catch (Exception e) {
		//System.out.println(e.toString());
	}
	return null;
}


	/*
	 * @RequestMapping(value = "/importTestCases", method = RequestMethod.POST)
	 * public ServiceResponse importTestCases(@RequestHeader("Authorization") String
	 * token,
	 * 
	 * @RequestParam("file") MultipartFile file, @RequestParam String
	 * groupId, @RequestParam String uploadType) { try { UserDetailsEntity
	 * detailsEntity = (UserDetailsEntity) session.getAttribute("user"); String
	 * sestoken = detailsEntity.getTokenID(); String tokenID = codec.decrypt(token);
	 * if (tokenID.equals(sestoken)) { ServiceResponse response = null; if (groupId
	 * != null) { if (uploadType.equals("append")) { response =
	 * projectService.importTestCases(file, groupId, uploadType); } else if
	 * (uploadType.equals("replace")) { response =
	 * projectService.importTestCases(file, groupId, uploadType); } else {
	 * 
	 * } }
	 * 
	 * // ServiceResponse response=null;/*projectService.createTestSuites(suites);
	 * return response; } else { return null; } } catch (Exception e) {
	 * //System.out.println(e.toString()); } return null; }
	 * 
	 */	
@RequestMapping(value = "/importtestsuites", method = RequestMethod.POST)
public ServiceResponse importTestSuites(@RequestHeader("Authorization") String token,
		@RequestParam("file") MultipartFile file, @RequestParam String testSuitesId,
		@RequestParam String uploadType,@RequestParam String testSuiteName) {
	try {
		UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
		String sestoken = detailsEntity.getTokenID();
		String tokenID = codec.decrypt(token);
		if (tokenID.equals(sestoken)) {
			ServiceResponse response = null;
			if (testSuitesId != null) {
				if (uploadType.equals("append")) {
					response = projectService.importTestSuites(file, testSuitesId, uploadType,testSuiteName);
				} else if (uploadType.equals("replace")) {
					response = projectService.importTestSuitess(file, testSuitesId, uploadType,testSuiteName);
				} 
				
			}
			serviceRequestInputs=new HashMap<String,Object>();
			serviceRequestInputs.put("file",file);
			serviceRequestInputs.put("testSuitesId",testSuitesId);
			serviceRequestInputs.put("uploadType",uploadType);
			serviceRequestInputs.put("testSuiteName",testSuiteName);
			CustomLogger.logMyInfo("ProjectController.importTestSuites() ",serviceRequestInputs, session , response );
			return response;
		} else {
			return null;
		}
	} catch (Exception e) {
		//System.out.println(e.toString());
	}
	return null;
}


	/*
	 * @RequestMapping(value = "/importtestsuites", method = RequestMethod.POST)
	 * public ServiceResponse importTestSuites(@RequestHeader("Authorization")
	 * String token,
	 * 
	 * @RequestParam("file") MultipartFile file, @RequestParam String testSuitesId,
	 * 
	 * @RequestParam String uploadType) { try { UserDetailsEntity detailsEntity =
	 * (UserDetailsEntity) session.getAttribute("user"); String sestoken =
	 * detailsEntity.getTokenID(); String tokenID = codec.decrypt(token); if
	 * (tokenID.equals(sestoken)) { ServiceResponse response = null; if
	 * (testSuitesId != null) { if (uploadType.equals("append")) { response =
	 * projectService.importTestSuites(file, testSuitesId, uploadType); } else if
	 * (uploadType.equals("replace")) { response =
	 * projectService.importTestSuites(file, testSuitesId, uploadType); } else {
	 * 
	 * } } return response; } else { return null; } } catch (Exception e) {
	 * //System.out.println(e.toString()); } return null; }
	 */
	@RequestMapping(value = "/updatetestsuites", method = RequestMethod.POST)
	public ServiceResponse updateTestSuites(@RequestHeader("Authorization") String token,
			@RequestBody MannualTestSuites suites,String projectwiserole) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.updateTestSuites(suites,projectwiserole);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("suites ",suites);
				serviceRequestInputs.put("projectwiserole",projectwiserole);
				CustomLogger.logMyInfo("ProjectController.updateTestSuites() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/deleteTestCase", method = RequestMethod.POST)
	public ServiceResponse deleteTestCase(@RequestHeader("Authorization") String token,
			@RequestParam Integer testcaseSID) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.deleteTestCase(testcaseSID);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("testcaseSID ",testcaseSID);
				CustomLogger.logMyInfo("ProjectController.deleteTestCase() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}
	
	
	@RequestMapping(value = "/deleteMulTestCase", method = RequestMethod.POST)
	public ServiceResponse deleteMulTestCase(@RequestHeader("Authorization") String token,
			@RequestParam Integer[] testcaseSID) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.deleteMulTestCase(testcaseSID);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("testcaseSID ",Arrays.toString(testcaseSID));
				CustomLogger.logMyInfo("ProjectController.deleteTestCase() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/addNewTestId", method = RequestMethod.POST)
	public ServiceResponse addNewTestId(@RequestHeader("Authorization") String token,
			@RequestBody MannualTestCaseTable testCase) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.addNewTestId(testCase);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("testCase ",testCase);
				CustomLogger.logMyInfo("ProjectController.deleteTestCase() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/updateTestCase", method = RequestMethod.POST)
	public ServiceResponse updateTestCase(@RequestHeader("Authorization") String token,
			@RequestBody MannualTestCaseTable testCase) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.updateTestCase(testCase);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("testCase ",testCase);
				CustomLogger.logMyInfo("ProjectController.updateTestCase() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/getOverViewproject", method = RequestMethod.POST)
	public ServiceResponse getOverViewproject(int userid, int roleid) {
		ServiceResponse response = projectService.getOverViewProjectDetails(roleid, userid);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("userid ",userid);
		serviceRequestInputs.put("roleid ",roleid);
		CustomLogger.logMyInfo("ProjectController.getOverViewproject() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getAllproject", method = RequestMethod.POST)
	public ServiceResponse getAllproject() {
		ServiceResponse response = projectService.getAllProject();
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("ProjectController.getAllproject() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getTestSuits", method = RequestMethod.POST)
	public ServiceResponse getTestSuits(@RequestParam Integer projectID) {
		ServiceResponse response = projectService.getTestSuits(projectID);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectID ",projectID);
		CustomLogger.logMyInfo("ProjectController.getTestSuits() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/getResourseEffortstake", method = RequestMethod.POST)
	public ServiceResponse getResourseEffortstake(@RequestBody ResourceRequestBo resourceRequestBo) {
		ServiceResponse response = projectService.getResourseEffortsstake(resourceRequestBo);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("resourceRequestBo ",resourceRequestBo);
		CustomLogger.logMyInfo("ProjectController.getResourseEffortstake() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getTestSuitsDetails", method = RequestMethod.POST)
	public ServiceResponse getTestSuitsDetails(@RequestParam Integer testSuitesId) {
		ServiceResponse response = projectService.getTestSuitsDetails(testSuitesId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("testSuitesId ",testSuitesId);
		CustomLogger.logMyInfo("ProjectController.getTestSuitsDetails() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getTestGroupAndCases", method = RequestMethod.POST)
	public ServiceResponse getTestGroupAndCases(@RequestParam Integer testSuitesId) {
		ServiceResponse response = projectService.getTestGroupAndCases(testSuitesId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("testSuitesId ",testSuitesId);
		CustomLogger.logMyInfo("ProjectController.getTestGroupAndCases() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/getGroupTestCasesBysuitesFiltered", method = RequestMethod.POST)
	public ServiceResponse getGroupTestCasesBysuitesFiltered(@RequestParam Integer testSuitesId, String filterParam) {
		ServiceResponse response = projectService.getGroupTestCasesBysuitesFiltered(testSuitesId, filterParam);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("testSuitesId ",testSuitesId);
		serviceRequestInputs.put("filterParam ",filterParam);
		CustomLogger.logMyInfo("ProjectController.getGroupTestCasesBysuitesFiltered() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getAllTestCassesBySuitesId", method = RequestMethod.POST)
	public ServiceResponse getAllTestCassesBySuitesId(@RequestParam Integer testSuitesId) {
		ServiceResponse response = projectService.getAllTestCassesBySuitesId(testSuitesId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("testSuitesId ",testSuitesId);
		CustomLogger.logMyInfo("ProjectController.getAllTestCassesBySuitesId() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getAllTestCassesByTestRun", method = RequestMethod.POST)
	public ServiceResponse getAllTestCassesByTestRun(@RequestParam Integer rerunId) {
		ServiceResponse response = projectService.getAllTestCassesByTestRun(rerunId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("rerunId ",rerunId);
		CustomLogger.logMyInfo("ProjectController.getAllTestCassesByTestRun() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getFailTestCasesByReRunId", method = RequestMethod.POST)
	public ServiceResponse getFailTestCasesByReRunId(@RequestParam Integer reRunId, @RequestParam Integer runId) {
		ServiceResponse response = projectService.getFailTestCasesByReRunId(reRunId, runId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("reRunId ",reRunId);
		serviceRequestInputs.put("runId ",runId);
		CustomLogger.logMyInfo("ProjectController.getFailTestCasesByReRunId() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/getFailResolvedTestCasesByReRunId", method = RequestMethod.POST)
	public ServiceResponse getFailResolvedTestCasesByReRunId(@RequestParam Integer reRunId, @RequestParam Integer runId) {
		ServiceResponse response = projectService.getFailResolvedTestCasesByReRunId(reRunId, runId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("reRunId ",reRunId);
		serviceRequestInputs.put("runId ",runId);
		CustomLogger.logMyInfo("ProjectController.getFailTestCasesByReRunId() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/createTestgroup", method = RequestMethod.POST)
	public ServiceResponse createTestGroup(@RequestHeader("Authorization") String token,
			@RequestBody MannualTestGroup mannualTestGroup) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {

				ServiceResponse response = projectService.createTestGroup(mannualTestGroup);
			
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("mannualTestGroup ",mannualTestGroup);
				CustomLogger.logMyInfo("ProjectController.createTestGroup() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/updateTestGroup", method = RequestMethod.POST)
	public ServiceResponse updateTestGroup(@RequestHeader("Authorization") String token,
			@RequestBody MannualTestGroup testGroup) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.updateTestGroup(testGroup);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("testGroup ",testGroup);
				CustomLogger.logMyInfo("ProjectController.updateTestGroup() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/getTestGroupDetails", method = RequestMethod.POST)
	public ServiceResponse getTestGroupDetails(@RequestParam Integer testGroupId) {
		ServiceResponse response = projectService.getTestGroupDetails(testGroupId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("testGroupId ",testGroupId);
		CustomLogger.logMyInfo("ProjectController.getTestGroupDetails() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getTestGroup", method = RequestMethod.POST)
	public ServiceResponse getTestGroup(@RequestParam Integer testSuitesId) {
		ServiceResponse response = projectService.getTestGroup(testSuitesId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("testSuitesId ",testSuitesId);
		CustomLogger.logMyInfo("ProjectController.getTestGroup() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getTestCases", method = RequestMethod.POST)
	public ServiceResponse getTestCases(@RequestParam Integer gropuID) {
		ServiceResponse response = projectService.getTestCases(gropuID);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("gropuID ",gropuID);
		CustomLogger.logMyInfo("ProjectController.getTestCases() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getTestSuitsByPid", method = RequestMethod.POST)
	public ServiceResponse getTestSuitsByPid(@RequestParam Integer projectID) {
		ServiceResponse response = projectService.getTestSuitsByPid(projectID);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectID ",projectID);
		CustomLogger.logMyInfo("ProjectController.getTestSuitsByPid() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/createRun", method = RequestMethod.POST)
	public ServiceResponse createRun(@RequestHeader("Authorization") String token,
			@RequestBody CreateRunBo testRunObj) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
////System.out.printlnln("isactive in controller "+testRunObj.getIsActive());
				ServiceResponse response = projectService.createRun(testRunObj);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("testRunObj ",testRunObj);
				CustomLogger.logMyInfo("ProjectController.createRun() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/getActiveRun", method = RequestMethod.POST)
	public ServiceResponse getActiveRun(@RequestParam Integer projectID) {
		ServiceResponse response = projectService.getActiveRun(projectID);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectID ",projectID);
		CustomLogger.logMyInfo("ProjectController.getActiveRun() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getCompletedRun", method = RequestMethod.POST)
	public ServiceResponse getCompletedRun(@RequestParam Integer projectID) {
		ServiceResponse response = projectService.getCompletedRun(projectID);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectID ",projectID);
		CustomLogger.logMyInfo("ProjectController.getCompletedRun() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/deleteActiveRun", method = RequestMethod.POST)
	public ServiceResponse deleteActiveRun(@RequestHeader("Authorization") String token, @RequestParam Integer runId) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.deleteActiveRun(runId);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("runId ",runId);
				CustomLogger.logMyInfo("ProjectController.deleteActiveRun() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/deleteRoundOneRun", method = RequestMethod.POST)
	public ServiceResponse deleteRoundOneRun(@RequestHeader("Authorization") String token, @RequestParam Integer id) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				////System.out.printlnln("calling delete mannualruntable controller, id is  "+id);
				ServiceResponse response = projectService.deleteRoundOneRun(id);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("id ",id);
				CustomLogger.logMyInfo("ProjectController.deleteRoundOneRun() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/getdealayedRun", method = RequestMethod.POST)
	public ServiceResponse getdealayedRun(@RequestParam Integer projectID) {
		ServiceResponse response = projectService.getdealayedRun(projectID);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectID ",projectID);
		CustomLogger.logMyInfo("ProjectController.getdealayedRun() ",serviceRequestInputs, session , response );
		return response;

	}
	@RequestMapping(value = "/getInactiveRun", method = RequestMethod.POST)
	public ServiceResponse getInactiveRun(@RequestParam Integer projectID) {
		ServiceResponse response = projectService.getInactiveRun(projectID);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectID ",projectID);
		CustomLogger.logMyInfo("ProjectController.getInactiveRun() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/checkTSuites", method = RequestMethod.POST)
	public ServiceResponse checkTSuites(@RequestParam Integer testsuiteid) {
		ServiceResponse response = projectService.checkTSuites(testsuiteid);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("testsuiteid ",testsuiteid);
		CustomLogger.logMyInfo("ProjectController.getInactiveRun() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/getTestGroupAndCasesByRun", method = RequestMethod.POST)
	public ServiceResponse getTestGroupAndCasesByRun(@RequestParam Integer runId) {
		ServiceResponse response = projectService.getTestGroupAndCasesByRun(runId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("runId ",runId);
		CustomLogger.logMyInfo("ProjectController.getTestGroupAndCasesByRun() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getTodoRunList", method = RequestMethod.POST)
	public ServiceResponse getTodoRunList(@RequestParam String userId, @RequestParam String projectId) {
		ServiceResponse response = projectService.getTodoRunList(userId, projectId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectId ",projectId);
		CustomLogger.logMyInfo("ProjectController.getTestGroupAndCasesByRun() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getTodoTestCaseList", method = RequestMethod.POST)
	public ServiceResponse getTodoTestCaseList(@RequestParam String runId, @RequestParam String userId) {
		ServiceResponse response = projectService.getTodoTestCaseList(userId, runId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("userId ",userId);
		CustomLogger.logMyInfo("ProjectController.getTodoTestCaseList() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/saveTestExceution", method = RequestMethod.POST)
	public ServiceResponse saveTestExceution(@RequestHeader("Authorization") String token,
			@RequestBody MannualTestCaseExecutionLogs exLogs) {
     System.out.println("token:"+token);
     
     System.out.println(exLogs);
     
    
     
     
     
		ServiceResponse response=new ServiceResponse();
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if ( tokenID.equals(sestoken) ) {
				 response = projectService.saveTestExceution(exLogs);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("exLogs ",exLogs.toString());
				CustomLogger.logMyInfo("ProjectController.saveTestExceution() ",serviceRequestInputs, session , response );
			}
			else {
				return response;
			}
		} 
		   catch (Exception e) {
e.printStackTrace();
		   }
		return response;
	}
	@SuppressWarnings("null")
	@RequestMapping(value = "/saveTestExceution1", method = RequestMethod.POST)
	public ServiceResponse saveTestExceution1(
//			@RequestHeader("Authorization") String token,
			@RequestParam String actualResult, @RequestParam String expectedResult,
			@RequestParam String testCaseSrNo, @RequestParam String testDescription,
			@RequestParam String testCaseAction, @RequestParam String status,
			@RequestParam String bugId, @RequestParam String bugPriority,
			@RequestParam String bugSeverity, @RequestParam String executionTime,
			@RequestParam String bugType, 
			@RequestParam String bugToolId, @RequestParam String browser,
			@RequestParam String complexity, @RequestParam String functionality,
			@RequestParam String groupName, @RequestParam String scenarioID,
			@RequestParam String scenarios,
			@RequestParam String testCaseType, @RequestParam String testData,
			@RequestParam String testerComment, @RequestParam String testID,
			@RequestParam String steps, @RequestParam String priority , @RequestParam String bugStatus,
			@RequestParam String apiBugSummary ,  @RequestParam String bugToolName,
			@RequestParam String projectId, @RequestParam String projectName  
			) {
	
		
//     System.out.print("token:"+token);
		ServiceResponse response=new ServiceResponse();
		AutomationTestCaseExecutionLogs arLogs=new AutomationTestCaseExecutionLogs();
		try {
			int a = 10;
			//AutomationTestCaseExecutionLogs arLogs = null;
//			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
//			String sestoken = detailsEntity.getTokenID();
//			String tokenID = codec.decrypt(token);
//			if ( tokenID.equals(sestoken) ) {
				if ( a == 10) {
				
				
				 arLogs.setActualResult(actualResult);
				 arLogs.setBrowser(browser);
				 arLogs.setBugId(bugId);
				 arLogs.setBugPriority(bugPriority);
				 arLogs.setBugSeverity(bugSeverity);
				 arLogs.setBugToolId(bugToolId);
				 arLogs.setBugType(bugType);
				 arLogs.setComplexity(complexity);
				 arLogs.setExecutionTime(executionTime);
				 arLogs.setExpectedResult(expectedResult);
				 arLogs.setFunctionality(functionality);
				 arLogs.setPriority(priority);
				 arLogs.setScenarioID(scenarioID);
				 arLogs.setScenarios(scenarios);
				 arLogs.setStatus(status);
				 arLogs.setSteps(steps);
				 arLogs.setTestCaseAction(testCaseAction);
				 arLogs.setTestCaseSrNo(testCaseSrNo);
				 arLogs.setTestCaseType(testCaseType);
				 arLogs.setTestData(testData);
				 arLogs.setTestDescription(testDescription);
				 arLogs.setTesterComment(testerComment);
				 arLogs.setTestID(testID);
				 arLogs.setBugStatus(bugStatus);
				 arLogs.setApiBugSummary(apiBugSummary);
				 arLogs.setToolName(bugToolName);
				 arLogs.setProjectId(projectId);
				 arLogs.setProjectName(projectName);
				 
				 
				
				 response = projectService.saveTestExceution1(arLogs);
				 
		
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("exLogs ",arLogs.toString());
				CustomLogger.logMyInfo("ProjectController.saveTestExceution() ",serviceRequestInputs, session , response );
			}
			else {
				return response;
			}
		} 
		   catch (Exception e) {
e.printStackTrace();
		   }
		return response;
	}
	@RequestMapping(value = "/roboBugRaiseAuto", method = RequestMethod.POST)
	public ServiceResponse roboBugRaiseAutoroboBugRaiseAuto(@RequestHeader("Authorization") String token,
			@RequestBody RobosoftJiraAutomationModel Robo) {

		ServiceResponse response=new ServiceResponse();
		try {
			 
//			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
//			System.out.print("detailsEntity:"+detailsEntity);
//			String sestoken = detailsEntity.getTokenID();
//			System.out.print("detailsEntity:"+detailsEntity);
//			String tokenID = codec.decrypt(token);
			if ( Robo.getRunID()!= null ) {
				 response = projectService.saveTestExceutionRobo(Robo);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("exLogs ",Robo.toString());
				CustomLogger.logMyInfo("ProjectController.saveTestExceution() ",serviceRequestInputs, session , response );
			}
			else {
				return response;
			}
		} 
		   catch (Exception e) {
e.printStackTrace();
		   }
		return response;
	}
	@RequestMapping(value = "/saveAutomationbug", method = RequestMethod.POST)
	public ServiceResponse saveAutomationbug(@RequestParam("tstatus") String tstatus,@RequestParam("texpectedResult") String texpectedResult,
			@RequestParam("tseverity") String tseverity,@RequestParam("tpriority") String tpriority,@RequestParam("tbugsummary") String tbugsummary,
			@RequestParam("tbrowser") String tbrowser,@RequestParam("tbugtool") String bugtool,@RequestParam("tdescription") String tdescription
			,@RequestParam("project") String projectname) {
		ServiceResponse response = new ServiceResponse();
		try {
			ExecutionReportAutomation exLogs=new ExecutionReportAutomation();
			exLogs.setTbrowser(tbrowser);
			exLogs.setTbugsummary(tbugsummary);
			exLogs.setTdescription(tdescription);
			exLogs.setTexpectedResult(texpectedResult);
			exLogs.setTpriority(tpriority);
			exLogs.setTseverity(tseverity);
			exLogs.setTbrowser(tbrowser);
			exLogs.setTbugtool(bugtool);
			exLogs.setProjectName(projectname);
			
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			//String tokenID = codec.decrypt(token);
			//if (tokenID.equals(sestoken)) {
				//response = projectService.generatebugAutomation(exLogs);
			serviceRequestInputs=new HashMap<String,Object>();
			serviceRequestInputs.put("tstatus ",tstatus);
			serviceRequestInputs.put("texpectedResult ",texpectedResult);
			serviceRequestInputs.put("tseverity ",tseverity);
			serviceRequestInputs.put("tpriority ",tpriority);
			serviceRequestInputs.put("tbrowser ",tbrowser);
			serviceRequestInputs.put("tbugsummary ",tbugsummary);
			serviceRequestInputs.put("tdescription ",tdescription);
			serviceRequestInputs.put("bugtool ",bugtool);
			serviceRequestInputs.put("projectname ",projectname);
			CustomLogger.logMyInfo("ProjectController.saveAutomationbug() ",serviceRequestInputs, session , response );

			//} else {
			//	return null;
			//}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return response;
	}
	/*
	@RequestMapping(value="/saveAttachment", method=RequestMethod.POST)
	public ServiceResponse saveAttachment(@RequestParam("file") MultipartFile file,@RequestParam("testCaseID") Integer testCaseID,
			@RequestParam("uploadedBy") String uploadedBy) throws IOException{
		
		ServiceResponse serviceResponse=new ServiceResponse();
		serviceResponse=projectService.saveAttachment(file,testCaseID,uploadedBy);
		return serviceResponse;
	}*/
	@RequestMapping(value="/saveAttachment", method=RequestMethod.POST)
	public ServiceResponse saveAttachment(@RequestParam("file") MultipartFile file[],
			@RequestParam("testCaseID") Integer testCaseID,@RequestParam("uploadedBy") String uploadedBy,
			@RequestParam("projectId") Integer projectId) throws IOException{
		
		ServiceResponse serviceResponse=new ServiceResponse();
		serviceResponse=projectService.saveAttachment(file,testCaseID,uploadedBy,projectId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("uploadedBy ",uploadedBy);
		serviceRequestInputs.put("file ",Arrays.toString(file));
		serviceRequestInputs.put("testCaseID ",testCaseID);
		serviceRequestInputs.put("projectId ",projectId);
		CustomLogger.logMyInfo("ProjectController.saveTestExceution() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}
	@RequestMapping(value="/saveAttachmentAuto", method=RequestMethod.POST)
	public ServiceResponse saveAttachmentAuto(@RequestParam("file") MultipartFile file[],
			@RequestParam("bugId") String bugId,@RequestParam("projectName") String projectName) throws IOException{
		
		ServiceResponse serviceResponse=new ServiceResponse();
		ProjectDeatilsMeta p = projectRepository.findByprojectName(projectName);
		ProjectDeatilsMeta bugTrackorTool = jiraService.getBugToolDetails(p.getProjectID());
		serviceResponse=jiraService.addAttachmentToIssueAuto(file,bugTrackorTool,bugId);
//		serviceRequestInputs=new HashMap<String,Object>();
//		serviceRequestInputs.put("uploadedBy ",uploadedBy);
//		serviceRequestInputs.put("file ",Arrays.toString(file));
//		serviceRequestInputs.put("testCaseID ",testCaseID);
//		serviceRequestInputs.put("projectId ",projectId);
		CustomLogger.logMyInfo("ProjectController.saveAttachmentAuto() ",serviceRequestInputs, session , serviceResponse );
		return serviceResponse;
	}
	@RequestMapping(value = "/getTestGroupAndCasesByRunstutus", method = RequestMethod.POST)
	public ServiceResponse getfiletrOptionData(@RequestHeader("Authorization") String token,@RequestParam Integer runId,String status) {
		ServiceResponse response =projectService.getfiletrOptionData(runId,status);
		return response;

	}

	@RequestMapping(value = "/changeAssiginTo", method = RequestMethod.POST)
	public ServiceResponse changeAssiginTo(@RequestHeader("Authorization") String token,
			@RequestParam String newAssiginId, @RequestParam String logsId) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.changeAssiginTo(newAssiginId, logsId);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("newAssiginId ",newAssiginId);
				serviceRequestInputs.put("logsId ",logsId);
				CustomLogger.logMyInfo("ProjectController.changeAssiginTo() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/changeMulAssiginTo", method = RequestMethod.POST)
	public ServiceResponse changeMulAssiginTo(@RequestHeader("Authorization") String token,
			@RequestBody List<Integer> logsId, String mulAssiginName) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.changeMulAssiginTo(logsId, mulAssiginName);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("logsId ",logsId);
				serviceRequestInputs.put("mulAssiginName ",mulAssiginName);
				CustomLogger.logMyInfo("ProjectController.changeMulAssiginTo() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/getOverallRunStatus", method = RequestMethod.POST)
	public ServiceResponse getOverallRunStatus(@RequestParam Integer runId) {
		ServiceResponse response = projectService.getOverallRunStatus(runId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("runId ",runId);
		CustomLogger.logMyInfo("ProjectController.getOverallRunStatus() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getallActiveRun", method = RequestMethod.POST)
	public ServiceResponse getallActiveRun(@RequestParam Integer projectid) {
		ServiceResponse response = projectService.getallActiveRun(projectid);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid ",projectid);
		CustomLogger.logMyInfo("ProjectController.getallActiveRun() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getOverallRunActionStatus", method = RequestMethod.POST)
	public ServiceResponse getOverallRunActionStatus(@RequestParam Integer runId) {
		ServiceResponse response = projectService.getOverallRunActionStatus(runId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("runId ",runId);
		CustomLogger.logMyInfo("ProjectController.getOverallRunActionStatus() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getOverallDefectStatus", method = RequestMethod.POST)
	public ServiceResponse getOverallDefectStatus(@RequestParam Integer runId) {
		ServiceResponse response = projectService.getOverallDefecttatus(runId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("runId ",runId);
		CustomLogger.logMyInfo("ProjectController.getOverallDefectStatus() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getAllRun", method = RequestMethod.POST)
	public ServiceResponse getAllRun(@RequestParam Integer projectID) {
		ServiceResponse response = projectService.getAllRun(projectID);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectID ",projectID);
		CustomLogger.logMyInfo("ProjectController.getAllRun() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getDetailsReportByRun", method = RequestMethod.POST)
	public ServiceResponse getDetailsReportByRun(@RequestParam Integer runId) {
		ServiceResponse response = projectService.getDetailsReportByRun(runId);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("runId ",runId);
		CustomLogger.logMyInfo("ProjectController.getDetailsReportByRun() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getprojectDetails", method = RequestMethod.POST)
	public ServiceResponse getprojectDetails(@RequestParam String projectname, @RequestParam int projectid) {
		ServiceResponse response = projectService.getProject(projectname, projectid);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid ",projectid);
		CustomLogger.logMyInfo("ProjectController.getprojectDetails() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/editprojectDetails", method = RequestMethod.POST)
	public ServiceResponse editprojectDetails(@RequestHeader("Authorization") String token,
			@RequestBody ProjectDeatilsMeta projectDeatilsMeta) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.editsaveProject(projectDeatilsMeta);
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("projectDeatilsMeta ",projectDeatilsMeta);
				CustomLogger.logMyInfo("ProjectController.editprojectDetails() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/deleteProjectbyid", method = RequestMethod.POST)
	public ServiceResponse deleteProjectbyid(@RequestHeader("Authorization") String token,
			@RequestParam int projectid, @RequestParam String isPtype) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.deleteProject(projectid,isPtype);
			
				//for logging log4j
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("projectid ",projectid);
				serviceRequestInputs.put("isPtype ",isPtype);
				CustomLogger.logMyInfo("ProjectController.deleteProjectbyid() ",serviceRequestInputs, session , response );
				
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/editprojectsave", method = RequestMethod.POST)
	public ServiceResponse editprojectsave(@RequestHeader("Authorization") String token,
			@RequestBody ProjectDeatilsMeta projectDeatilsMeta) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				////System.out.printlnln("projectDeatilsMeta..controller..."+projectDeatilsMeta.getProjectStartDate()+"projectDeatilsMeta..controller..."+projectDeatilsMeta.getProjectEndDate());
				ServiceResponse response = projectService.saveEditProject(projectDeatilsMeta);
				//for logging log4j
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("projectDeatilsMeta ",projectDeatilsMeta);
				CustomLogger.logMyInfo("ProjectController.editprojectsave() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/deleteTestSuites", method = RequestMethod.POST)
	public ServiceResponse deleteTestSuites(@RequestHeader("Authorization") String token, @RequestParam String updatedByEmail,
			@RequestParam Integer testsuiteid, @RequestParam String testsuitesname) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) 
			{
				ServiceResponse response = projectService.deleteTestSuites(updatedByEmail,testsuiteid, testsuitesname);
				//for logging log4j
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("updatedByEmail ",updatedByEmail);
				serviceRequestInputs.put("testsuitesname ",testsuitesname);
				serviceRequestInputs.put("testsuiteid ",testsuiteid);
				CustomLogger.logMyInfo("ProjectController.deleteTestSuites() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/testrunwisedetails", method = RequestMethod.POST)
	public ServiceResponse testrunwisedetails(@RequestParam Integer projectid) {
		ServiceResponse response = projectService.testrunwisedetails(projectid);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid ",projectid);
		CustomLogger.logMyInfo("ProjectController.testrunwisedetails() ",serviceRequestInputs, session , response );
		return response;

	}
	@RequestMapping(value = "/testrunwisedetailsoverview", method = RequestMethod.POST)
	public ServiceResponse testrunwisedetailsoverview(@RequestParam Integer projectid) {
		ServiceResponse response = projectService.testrunwisedetailsoverview(projectid);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid ",projectid);
		CustomLogger.logMyInfo("ProjectController.testrunwisedetailsoverview() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/projectrunwisedetails", method = RequestMethod.POST)
	public ServiceResponse projectrunwisedetails(@RequestParam Integer projectid) {
		ServiceResponse response = projectService.projectrunwisedetails(projectid);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid ",projectid);
		CustomLogger.logMyInfo("ProjectController.projectrunwisedetails() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/runwisedetails", method = RequestMethod.POST)
	public ServiceResponse runwisedetails(@RequestParam String runlist) {
		//System.out.printlnln(runlist);
		ServiceResponse response = projectService.runwisedetails(runlist);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("runlist ",runlist);
		CustomLogger.logMyInfo("ProjectController.runwisedetails() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getSingleproject", method = RequestMethod.POST)
	public ServiceResponse getSingleproject(@RequestParam int projectid) {
		ServiceResponse response = projectService.getProjectSelf(projectid);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid ",projectid);
		CustomLogger.logMyInfo("ProjectController.getSingleproject() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/projectwisefailstatus", method = RequestMethod.POST)
	public ServiceResponse projectwisefailstatus(@RequestParam int projectid) {
		ServiceResponse response = projectService.projectwisefailstatus(projectid);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid ",projectid);
		CustomLogger.logMyInfo("ProjectController.projectwisefailstatus() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getProjectDetails", method = RequestMethod.POST)
	public ServiceResponse getProjectDetails(@RequestParam int projectid) {
		ServiceResponse response = projectService.getProjectDetails(projectid);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid ",projectid);
		CustomLogger.logMyInfo("ProjectController.getProjectDetails() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/deletegroup", method = RequestMethod.POST)
	public ServiceResponse deletegroup(@RequestHeader("Authorization") String token, @RequestParam Integer groupId) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.deletegroup(groupId);
				//for logging log4j
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("groupId ",groupId);
				CustomLogger.logMyInfo("ProjectController.deletegroup() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/deletetestruncaseid", method = RequestMethod.POST)
	public ServiceResponse deletetestruncaseid(@RequestHeader("Authorization") String token,
			@RequestBody MannualTestCaseExecutionLogs logs) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.deletetestruncaseid(logs);
				//for logging log4j
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("MannualTestCaseExecutionLogs  ",logs);
				CustomLogger.logMyInfo("ProjectController.deletetestruncaseid() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/uploadDocument", method = RequestMethod.POST)
	public ServiceResponse uploadDocuments(@RequestHeader("Authorization") String token,
			@RequestParam("file") MultipartFile file, @RequestParam String pId, @RequestParam String pName,
			@RequestParam String documentsName, @RequestParam String isPublic, @RequestParam String createdBy) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.uploadDocuments(file, pId, pName, documentsName, isPublic,
						createdBy);
				//for logging log4j
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("file  ",file);
				serviceRequestInputs.put("pId  ",pId);
				serviceRequestInputs.put("pName  ",pName);
				serviceRequestInputs.put("documentsName  ",documentsName);
				serviceRequestInputs.put("isPublic  ",isPublic);
				serviceRequestInputs.put("createdBy  ",createdBy);
				CustomLogger.logMyInfo("ProjectController.uploadDocuments() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/getPublicDocument", method = RequestMethod.GET)
	public ServiceResponse getPublicDocument() {
		ServiceResponse response = projectService.getPublicDocument();
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("ProjectController.getPublicDocument() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getProjectDocument", method = RequestMethod.POST)
	public ServiceResponse getProjectDocument(@RequestHeader("Authorization") String token,
			@RequestParam("pId") Integer pId) {
		UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");

		String sestoken = detailsEntity.getTokenID();
		//System.out.println("apmosys token is " + sestoken);
		ServiceResponse response = projectService.getProjectDocument(pId);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("pId  ",pId);
		CustomLogger.logMyInfo("ProjectController.getProjectDocument() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/stopActiveRun", method = RequestMethod.POST)
	public ServiceResponse stopActiveRun(@RequestBody MannualReRunTable mannualReRunTable) {
	////System.out.printlnln("is active in controller is "+mannualReRunTable.getIsActive());
		ServiceResponse response = projectService.stopActiveRun(mannualReRunTable);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("mannualReRunTable  ",mannualReRunTable);
		CustomLogger.logMyInfo("ProjectController.stopActiveRun() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getrerunnameByrun", method = RequestMethod.POST)
	public ServiceResponse getrerunnameByrun(@RequestParam Integer runid) {
		ServiceResponse response = projectService.getrerunnameByrun(runid);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("runid  ",runid);
		CustomLogger.logMyInfo("ProjectController.getrerunnameByrun() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getLastRerunnameByrun", method = RequestMethod.POST)
	public ServiceResponse getLastRerunnameByrun(@RequestParam Integer runid, @RequestParam Integer suiteId) {
		ServiceResponse response = projectService.getLastRerunnameByrun(runid, suiteId);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("runid  ",runid);
		serviceRequestInputs.put("suiteId  ",suiteId);
		CustomLogger.logMyInfo("ProjectController.getLastRerunnameByrun() ",serviceRequestInputs, session , response );
		return response;
	}

	@RequestMapping(value = "/runwisefailstatus", method = RequestMethod.POST)
	public ServiceResponse runwisefailstatus(@RequestParam String runlist) {
		ServiceResponse response = projectService.runwisefailstatus(runlist);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("runlist  ",runlist);
 		CustomLogger.logMyInfo("ProjectController.runwisefailstatus() ",serviceRequestInputs, session , response );
		return response;

	}

	/*
	 * @RequestMapping(value="/downloadDocument", method=RequestMethod.POST) public
	 * ResponseEntity<byte[]> downloadDocument(@RequestBody ProjectDocument
	 * document,HttpServletRequest request,HttpServletResponse response) {
	 * ////System.out.printlnln(document); Resource resource=null; MediaType
	 * mediaType=null; try { resource = projectService.loadFileAsResource(document);
	 * FileSystemResource fileResource = new FileSystemResource(resource.getFile());
	 * 
	 * byte[] base64Bytes =
	 * Base64.encodeBase64(IOUtils.toByteArray(fileResource.getInputStream()));
	 * String mineType = servletContext.getMimeType(resource.getFile().getName());
	 * try { mediaType = MediaType.parseMediaType(mineType); if(mediaType==null)
	 * mediaType= MediaType.APPLICATION_OCTET_STREAM; } catch (Exception e) {
	 * mediaType= MediaType.APPLICATION_OCTET_STREAM; }
	 * 
	 * HttpHeaders headers = new HttpHeaders(); headers.add("filename",
	 * fileResource.getFilename());
	 * 
	 * return
	 * ResponseEntity.ok().headers(headers).contentType(mediaType).contentLength(
	 * base64Bytes.length).body(base64Bytes); } catch (Exception ex) {
	 * ex.printStackTrace(); } return null;
	 * 
	 * }
	 */

	@RequestMapping(value = "/downloadDocument")
	public ResponseEntity<byte[]> downloadDocument(@RequestParam Integer documentId, HttpServletRequest request,
			HttpServletResponse resonse) {
		Resource resource = null;
		MediaType mediaType = null;
		try {
			ProjectDocument document = projectService.projectDocumentDao(documentId);
			////System.out.printlnln(document);
			resource = projectService.loadFileAsResource(document);
			File file = resource.getFile();
			String mineType = servletContext.getMimeType(resource.getFile().getName());
			try {
				mediaType = MediaType.parseMediaType(mineType);
				if (mediaType == null)
					mediaType = MediaType.APPLICATION_OCTET_STREAM;
			} catch (Exception e) {
				mediaType = MediaType.APPLICATION_OCTET_STREAM;
			}

			resonse.setContentType(mediaType.getType());

			// Content-Disposition
			resonse.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName());

			// Content-Length
			resonse.setContentLength((int) file.length());

			BufferedInputStream inStream = new BufferedInputStream(new FileInputStream(file));
			BufferedOutputStream outStream = new BufferedOutputStream(resonse.getOutputStream());

			//for logging log4j
			serviceRequestInputs=new HashMap<String,Object>();
			serviceRequestInputs.put("documentId  ",documentId);
			serviceRequestInputs.put("request  ",request);
			serviceRequestInputs.put("resonse  ",resonse);
			CustomLogger.logMyInfo("ProjectController.downloadDocument() ",serviceRequestInputs, session , null );
			byte[] buffer = new byte[1024];
			int bytesRead = 0;
			while ((bytesRead = inStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
			outStream.flush();
			inStream.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/getAllDocumentTypes", method = RequestMethod.GET)
	public ServiceResponse getAllDocumentTypes() {
		ServiceResponse response = projectService.getDocumentTypes();
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		CustomLogger.logMyInfo("ProjectController.getAllDocumentTypes() ",serviceRequestInputs, session , response );
		return response;

	}
	@RequestMapping(value = "/configProjects", method = RequestMethod.POST)
	public ServiceResponse configProjects(@RequestParam Integer user_role,@RequestParam String userid) {
		ServiceResponse response = projectService.configProjects(user_role,userid);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("user_role  ",user_role);
		serviceRequestInputs.put("userid  ",userid);
		CustomLogger.logMyInfo("ProjectController.configProjects() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/deleteDocument", method = RequestMethod.POST)
	public ServiceResponse deleteDocument(@RequestHeader("Authorization") String token, @RequestParam("id") Integer id,
			@RequestParam("filePath") String filePath, @RequestParam("documentName") String documentName) {
		try {
			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
			String sestoken = detailsEntity.getTokenID();
			String tokenID = codec.decrypt(token);
			if (tokenID.equals(sestoken)) {
				ServiceResponse response = projectService.deleteDocument(id, filePath, documentName);
				//for logging log4j
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("id  ",id);
				serviceRequestInputs.put("filePath  ",filePath);
				serviceRequestInputs.put("documentName  ",documentName);
				CustomLogger.logMyInfo("ProjectController.deleteDocument() ",serviceRequestInputs, session , response );
				return response;
			} else {
				return null;
			}
		} catch (Exception e) {
			//System.out.println(e.toString());
		}
		return null;
	}

	@RequestMapping(value = "/getresourcewiseexecution", method = RequestMethod.POST)
	public ServiceResponse getresourcewiseexecution(@RequestParam("id") Integer runid) {
		ServiceResponse response = projectService.getResourcewiseexecution(runid);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("runid  ",runid);
		CustomLogger.logMyInfo("ProjectController.getresourcewiseexecution() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getRoletypebyproject", method = RequestMethod.POST)
	public ServiceResponse getRoletypebyproject(@RequestParam("projectid") Integer projectid,
			@RequestParam("id") Integer id) {
		ServiceResponse response = projectService.getRoletypebyproject(projectid, id);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid  ",projectid);
		serviceRequestInputs.put("id  ",id);
		CustomLogger.logMyInfo("ProjectController.getRoletypebyproject() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/getresourseefforts", method = RequestMethod.POST)
	public ServiceResponse getResourseEfforts(@RequestBody ResourceRequestBo resourceRequestBo) {
		//System.out.printlnln(resourceRequestBo);
		ServiceResponse response = projectService.getResourseEfforts(resourceRequestBo);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("resourceRequestBo  ",resourceRequestBo);
		CustomLogger.logMyInfo("ProjectController.getResourseEfforts() ",serviceRequestInputs, session , response );
		return response;

	}

	// Lipsa--17_09
	@RequestMapping(value = "/getResourseEffortsFreshRun", method = RequestMethod.POST)
	public ServiceResponse getResourseEffortsFreshRun(@RequestBody ResourceRequestBo resourceRequestBo) {
		ServiceResponse response = projectService.getResourseEffortsFreshRun(resourceRequestBo);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("resourceRequestBo  ",resourceRequestBo);
		CustomLogger.logMyInfo("ProjectController.getResourseEffortsFreshRun() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/tableResourceEfforts", method = RequestMethod.POST)
	public ServiceResponse tableResourceEfforts(@RequestBody ResourceRequestBo resourceRequestBo) {
		//System.out.printlnln(resourceRequestBo);
		ServiceResponse response = projectService.tableResourceEfforts(resourceRequestBo);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("resourceRequestBo  ",resourceRequestBo);
		CustomLogger.logMyInfo("ProjectController.tableResourceEfforts() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/tableResourceEffortsAdmin", method = RequestMethod.POST)
	public ServiceResponse tableResourceEffortsAdmin(@RequestBody ResourceRequestBo resourceRequestBo) {
		////System.out.printlnln(resourceRequestBo);
		ServiceResponse response = projectService.tableResourceEffortsAdmin(resourceRequestBo);
		return response;

	}
	
	@RequestMapping(value = "/tableResourceEffortsForManager", method = RequestMethod.POST)
	public ServiceResponse tableResourceEffortsForManager(@RequestBody ResourceRequestBo resourceRequestBo) {
		////System.out.printlnln(resourceRequestBo);
		ServiceResponse response = projectService.tableResourceEffortsForManager(resourceRequestBo);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("resourceRequestBo  ",resourceRequestBo);
		CustomLogger.logMyInfo("ProjectController.tableResourceEffortsForManager() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/getAllProjectNameForMAnager", method = RequestMethod.POST)
	public ServiceResponse getAllProjectNameForMAnager(@RequestParam("userid") Integer userid) 
	{
		ServiceResponse response = projectService.getAllProjectNameForMAnager(userid);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("userid  ",userid);
		CustomLogger.logMyInfo("ProjectController.getAllProjectNameForMAnager() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/getMangetCreatedUserList", method = RequestMethod.POST)
	public ServiceResponse getMangetCreatedUserList(@RequestParam("userid") Integer userid) 
	{
		ServiceResponse response = projectService.getMangetCreatedUserList(userid);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("userid  ",userid);
		CustomLogger.logMyInfo("ProjectController.getMangetCreatedUserList() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/updateTestGroupID", method = RequestMethod.POST)
	public ServiceResponse updateTestGroupID(@RequestParam("caseId") Integer caseId,
			@RequestParam("groupId") Integer groupId) {
		ServiceResponse response = projectService.updateTestGroupID(caseId, groupId);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("caseId  ",caseId);
		serviceRequestInputs.put("groupId  ",groupId);
		CustomLogger.logMyInfo("ProjectController.updateTestGroupID() ",serviceRequestInputs, session , response );
		return response;

	}
	@RequestMapping(value = "/getComponetDetails", method = RequestMethod.POST)
	public ServiceResponse getComponetDetails(@RequestParam("projectName") String projectName,
			@RequestParam("projectId") Integer projectId) {
		ServiceResponse response = projectService.getComponetDetails(projectName, projectId);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectName  ",projectName);
		serviceRequestInputs.put("projectId  ",projectId);
		CustomLogger.logMyInfo("ProjectController.getComponetDetails() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/fetchBugComment", method = RequestMethod.POST)
	public ServiceResponse fetchBugComment(@RequestParam("projectID") Integer projectID, @RequestParam("bugID") Integer bugID) {
		ServiceResponse response = projectService.fetchBugComment(projectID, bugID);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("bugID  ",bugID);
		serviceRequestInputs.put("projectID  ",projectID);
		CustomLogger.logMyInfo("ProjectController.fetchBugComment() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/reopenBugzillaIssue", method = RequestMethod.POST)
	public ServiceResponse reopenBugzillaIssue(@RequestParam("projectName") String projectName, @RequestParam("projectId") Integer projectId,
			@RequestParam("bugID") Integer bugID) {
		ServiceResponse response = projectService.reopenBugzillaIssue(projectName, projectId, bugID);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectName  ",projectName);
		serviceRequestInputs.put("bugID  ",bugID);
		serviceRequestInputs.put("projectId  ",projectId);
		CustomLogger.logMyInfo("ProjectController.reopenBugzillaIssue() ",serviceRequestInputs, session , response );
		return response;

	}
	
	/*
	 * @RequestMapping(value = "/test", method = RequestMethod.GET) public Integer
	 * updateTestGroupID() {
	 * 
	 * Integer i = buzilaService.createProject(); buzilaService.createComponent();
	 * 
	 * //buzilaService.createBug(); return null;
	 * 
	 * }
	 */
	
	@RequestMapping(value = "/downloadTemplate")
	public ResponseEntity<byte[]> downloadTemplate(HttpServletRequest request,
			HttpServletResponse resonse) {
		Resource resource = null;
		MediaType mediaType = null;
		try {
			
			resource = projectService.getTemplateFile();
			//for logging log4j
			serviceRequestInputs=new HashMap<String,Object>();
			serviceRequestInputs.put("request  ",request);
			serviceRequestInputs.put("resonse  ",resonse);
			CustomLogger.logMyInfo("ProjectController.downloadTemplate() ",serviceRequestInputs, session , null );
			////System.out.printlnln(resource);
			
			File file = resource.getFile();
			String mineType = servletContext.getMimeType(resource.getFile().getName());
			try {
				mediaType = MediaType.parseMediaType(mineType);
				if (mediaType == null)
					mediaType = MediaType.APPLICATION_OCTET_STREAM;
			} catch (Exception e) {
				mediaType = MediaType.APPLICATION_OCTET_STREAM;
			}

			resonse.setContentType(mediaType.getType());

			// Content-Disposition
			resonse.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + "Attachment"+file.getName());

			// Content-Length
			resonse.setContentLength((int) file.length());

			BufferedInputStream inStream = new BufferedInputStream(new FileInputStream(file));
			BufferedOutputStream outStream = new BufferedOutputStream(resonse.getOutputStream());

			byte[] buffer = new byte[1024];
			int bytesRead = 0;
			while ((bytesRead = inStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
			outStream.flush();
			inStream.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;

	}

	
	@RequestMapping(value = "/attchmentFetch", method = RequestMethod.POST)
	public ServiceResponse attchmentFetch(@RequestParam("testcaseID") Integer testcaseID) {
		
		ServiceResponse response = projectService.attchmentFetch(testcaseID);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("testcaseID  ",testcaseID);
		CustomLogger.logMyInfo("ProjectController.attchmentFetch() ",serviceRequestInputs, session , response );
		return response;

	}
	
	//added by sakti
	@RequestMapping(value = "/attchmentFetch1", method = RequestMethod.POST)
	public ServiceResponse attchmentFetch1(@RequestParam("testcaseID") Integer testcaseID,@RequestParam("bugId") String bugId) {
		
		ServiceResponse response = projectService.attchmentFetch1(testcaseID,bugId);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("testcaseID  ",testcaseID);
		CustomLogger.logMyInfo("ProjectController.attchmentFetch() ",serviceRequestInputs, session , response );
		return response;

	}

	@RequestMapping(value = "/deleteImage", method = RequestMethod.POST)
	public ServiceResponse deleteImage(@RequestParam("testcaseID") Integer testcaseID,@RequestParam("attachname") String attachname) {
		
		ServiceResponse response = projectService.deleteScreenshot(testcaseID,attachname);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("testcaseID  ",testcaseID);
		serviceRequestInputs.put("attachname  ",attachname);
		CustomLogger.logMyInfo("ProjectController.deleteImage() ",serviceRequestInputs, session , response );
		 
		return response;

	}
	@RequestMapping(value = "/downloadImage")
	public ResponseEntity<byte[]> downloadTemplate(HttpServletRequest request,
			HttpServletResponse resonse,@RequestParam("testcaseID") Integer testcaseID,@RequestParam("attachname") String attachname) {
		MediaType mediaType = null;
		try {
		List<AttachmentFetch> response1 = projectService.getImage(testcaseID,attachname);
		
		String mineType = servletContext.getMimeType(response1.get(0).getAttachmentName());
		try {
		mediaType = MediaType.parseMediaType(mineType);
		if (mediaType == null)
		mediaType = MediaType.APPLICATION_OCTET_STREAM;
		} catch (Exception e) {
		mediaType = MediaType.APPLICATION_OCTET_STREAM;
		}

		resonse.setContentType(mediaType.getType());

		// Content-Disposition
		resonse.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + response1.get(0).getAttachmentName());

		// Content-Length
		resonse.setContentLength(Integer.parseInt(response1.get(0).getSize()));

		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("testcaseID  ",testcaseID);
		serviceRequestInputs.put("attachname  ",attachname);
		serviceRequestInputs.put("(extra)resonse  ",resonse);
		CustomLogger.logMyInfo("ProjectController.downloadTemplate() ",serviceRequestInputs, session , null );
		
		BufferedOutputStream outStream = new BufferedOutputStream(resonse.getOutputStream());
		////System.out.printlnln(response1.get(0).getAttachment().length+"===");
		if ((response1.get(0).getAttachment().length) != -1) {
		outStream.write(response1.get(0).getAttachment());
		}
		outStream.flush();
		outStream.close();
		} catch (Exception ex) {
		ex.printStackTrace();
		}
		return null;

	}
	
	@RequestMapping(value = "/getShowProjectDetails", method = RequestMethod.POST)
	public ServiceResponse getShowProjectDetails(@RequestParam("projectid") String projectid) {
		ServiceResponse response = projectService.getShowProjectDetails(projectid);
	          	//for logging log4j
				serviceRequestInputs=new HashMap<String,Object>();
				serviceRequestInputs.put("projectid  ",projectid);
				CustomLogger.logMyInfo("ProjectController.getShowProjectDetails() ",serviceRequestInputs, session , response );
				 
		return response;

	}
	@RequestMapping(value = "/datewiserun", method = RequestMethod.POST)
	public ServiceResponse datewiserun(@RequestParam("startdate") String startdate,@RequestParam("enddate") String enddate,@RequestParam("projectid") int projectid) throws ParseException {
	
		ServiceResponse response = projectService.datewiserun(startdate,enddate,projectid);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid  ",projectid);
		serviceRequestInputs.put("startdate  ",startdate);
		serviceRequestInputs.put("enddate  ",enddate);
		CustomLogger.logMyInfo("ProjectController.datewiserun() ",serviceRequestInputs, session , response );
		
		return response;

	}
	@RequestMapping(value = "/datewiserunfailstatus", method = RequestMethod.POST)
	public ServiceResponse datewiserunfailstatus(@RequestParam("startdate") String startdate,@RequestParam("enddate") String enddate,@RequestParam("projectid") int projectid) throws ParseException {
		ServiceResponse response = projectService.datewiserunfailstatus(startdate,enddate,projectid);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("testcaseID  ",startdate);
		serviceRequestInputs.put("attachname  ",enddate);
		serviceRequestInputs.put("projectid  ",projectid);
		CustomLogger.logMyInfo("ProjectController.datewiserunfailstatus() ",serviceRequestInputs, session , response );
		
		return response;

	}
	
	@RequestMapping(value = "/getDefectProjectDetails", method = RequestMethod.POST)
	public ServiceResponse getDefectProjectDetails(@RequestParam int projectid) {
		//System.out.printlnln("Controller ........servicess......");
		ServiceResponse response = projectService.getDefectProjectDetails(projectid);
		//System.out.printlnln("defect");
		
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid  ",projectid);
		CustomLogger.logMyInfo("ProjectController.getDefectProjectDetails() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/findDuplicate", method = RequestMethod.POST)
	public ServiceResponse findDuplicate(@RequestHeader("Authorization") String token,
	@RequestParam String runName ) {
	try {
	UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
	String sestoken = detailsEntity.getTokenID();
	String tokenID = codec.decrypt(token);
	if (tokenID.equals(sestoken)) {
	ServiceResponse response = null;

	response = projectService.findDuplicate(runName);

	//for logging log4j
	serviceRequestInputs=new HashMap<String,Object>();
	serviceRequestInputs.put("runName  ",runName);
	CustomLogger.logMyInfo("ProjectController.findDuplicate() ",serviceRequestInputs, session , response );


	// ServiceResponse response=null;/*projectService.createTestSuites(suites);*/
	return response;
	} else {
	return null;
	}
	} catch (Exception e) {
	//System.out.println(e.toString());
	}
	return null;
	}
	@RequestMapping(value = "/uploadDiffPreview", method = RequestMethod.POST)
	public ServiceResponse uploadDiffPreview(@RequestHeader("Authorization") String token,
	@RequestParam("file") MultipartFile file, @RequestParam String suiteName,@RequestParam String runName ,
	@RequestParam String projectId,@RequestParam String sameTestSuite) {
	try {
	UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
	String sestoken = detailsEntity.getTokenID();
	String tokenID = codec.decrypt(token);
	if (tokenID.equals(sestoken)) {
		
	ServiceResponse response = null;
	MannualRunTable testRun = new MannualRunTable();
	if (projectId != null) {
	testRun.setSuiteName(suiteName);
	testRun.setProjectId(Integer.parseInt(projectId));
	testRun.setRunName(runName);
	response = projectService.uploadPreview(file, testRun,sameTestSuite);
	}

	//for logging log4j
	serviceRequestInputs=new HashMap<String,Object>();
	serviceRequestInputs.put("file  ",file);
	serviceRequestInputs.put("suiteName  ",suiteName);
	serviceRequestInputs.put("runName  ",runName);
	serviceRequestInputs.put("projectId  ",projectId);
	serviceRequestInputs.put("sameTestSuite  ",sameTestSuite);
	CustomLogger.logMyInfo("ProjectController.uploadDiffPreview() ",serviceRequestInputs, session , response );

	// ServiceResponse response=null;/*projectService.createTestSuites(suites);*/
	return response;
	} 
	
	else {
	return null;
	}
	} catch (Exception e) {
	//System.out.printlnln(e.toString());
	}
	return null;
	}
	
	@RequestMapping(value = "/getdefectseveritygraph", method = RequestMethod.POST)
	public ServiceResponse getdefectseveritygraph(@RequestParam int projectid) {
		////System.out.printlnln("Controller ........servicess......");
		ServiceResponse response = projectService.getdefectseveritygraph(projectid);
		////System.out.printlnln("defect");
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid  ",projectid);
		CustomLogger.logMyInfo("ProjectController.getdefectseveritygraph() ",serviceRequestInputs, session , response );
		return response;

	}
	
	@RequestMapping(value = "/getstuatudefectsgraph", method = RequestMethod.POST)
	public ServiceResponse getstuatudefectsgraph(@RequestParam int projectid) {
		////System.out.printlnln("Controller ........servicess......");
		ServiceResponse response = projectService.getstuatudefectsgraph(projectid);
		////System.out.printlnln("defect");
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid  ",projectid);
		CustomLogger.logMyInfo("ProjectController.getstuatudefectsgraph() ",serviceRequestInputs, session , response );
		return response;

	}
	@RequestMapping(value = "/unLockUser", method = RequestMethod.POST)
	public ServiceResponse unLockUser(@RequestParam String userId) {
		////System.out.printlnln("Controller ........servicess......");
		ServiceResponse response = projectService.unLockUser(userId);
		//for logging log4j
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("userId  ",userId);
		CustomLogger.logMyInfo("ProjectController.getstuatudefectsgraph() ",serviceRequestInputs, session , response );
		return response;
		
	}
	
	@RequestMapping(value = "/exportTestCaseBugsToExcel", method = RequestMethod.POST)
	public void exportTestCaseToExcel(@RequestParam(value = "pID") String pID,
			HttpServletRequest request, 
			HttpServletResponse response) {
		try {
			List<RunWiseBusStatusForExport> runs= jiraService.getAllBugsWithstatus(pID);
			String fileName = "RandomFile";
			HttpHeaders header = new HttpHeaders();
			header.setContentType(new MediaType("application", "vnd.ms-excel"));
			//method to create Workbook
			Workbook workbook=new XSSFWorkbook() ;//created excel file
			//set name 
			response.addHeader("Content-Disposition","attachment;filename=DoesntMatter.xlsx");
			//create sheet
			Sheet sheet=workbook.createSheet("TestCases");
			//create 1st row as headings
			Row r1=sheet.createRow(0);//1st row (Header)

			r1.createCell(0).setCellValue("Test Run Id");
			r1.createCell(1).setCellValue("Test Run Name");
			r1.createCell(2).setCellValue("Closed");
			r1.createCell(3).setCellValue("Done");
			r1.createCell(4).setCellValue("Duplicate");
			r1.createCell(5).setCellValue("Not An Issue");
			r1.createCell(6).setCellValue("On Hold");
			r1.createCell(7).setCellValue("In Progress");
			r1.createCell(8).setCellValue("Reopened");
			r1.createCell(9).setCellValue("To Do");
			r1.createCell(10).setCellValue("Grand Total");

			//CREATE data rows
			int rowVal=1;

			for(RunWiseBusStatusForExport l : runs) {
				
				Row r=sheet.createRow(rowVal);
				
				r.createCell(0).setCellValue(l.getRunId());
				r.createCell(1).setCellValue(l.getRunName());
				r.createCell(2).setCellValue(l.getClosed());
				r.createCell(3).setCellValue(l.getDone());
				r.createCell(4).setCellValue(l.getDuplicate());
				r.createCell(5).setCellValue(l.getNotAnIssue());
				r.createCell(6).setCellValue(l.getOnHold());
				r.createCell(7).setCellValue(l.getInPrgress());
				r.createCell(8).setCellValue(l.getReOpened());
				r.createCell(9).setCellValue(l.getToDo());
				r.createCell(10).setCellValue(l.getTotal());
				rowVal++;
			}//for
			try {
				workbook.write(response.getOutputStream());
				workbook.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}}
	
	@RequestMapping(value = "/getProjectToolDetails", method = RequestMethod.POST)
	public ServiceResponse getProjectToolDetails(@RequestParam int projectid) {
		ServiceResponse response = projectService.getProjectToolDetails(projectid);
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid  ",projectid);
		CustomLogger.logMyInfo("ProjectController.getProjectToolDetails() ",serviceRequestInputs, session , response );
		return response;

	}
	
}
