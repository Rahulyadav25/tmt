package com.apmosys.tmt.controllers;

import java.util.HashMap;

//import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.apmosys.tmt.services.ProjectOverviewAutoService;
import com.apmosys.tmt.utils.CustomLogger;
import com.apmosys.tmt.utils.ServiceResponse;

import jakarta.servlet.http.HttpSession;
@RestController
@RequestMapping(path="/api")

public class ProjectOverviewAutoController {
	@Autowired
	ProjectOverviewAutoService projecOAuto;
	@Autowired
	HttpSession session;
	@Autowired
	CustomLogger CustomLogger;
	
	private HashMap<String, Object> serviceRequestInputs;

	@RequestMapping(value="/getoverviewdetails", method=RequestMethod.POST)
	public ServiceResponse getoverviewdetails(@RequestParam int projectid,@RequestParam  int id,@RequestParam  int user_role) {	
		ServiceResponse response=projecOAuto.getoverviewdetails(projectid, id, user_role);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid",projectid);
		serviceRequestInputs.put("id",id);
		serviceRequestInputs.put("user_role",user_role);
		CustomLogger.logMyInfo("ProjectOverviewAutoController.getoverviewdetails() ",serviceRequestInputs, session , response );
		return response;
	}
	@RequestMapping(value="/getLogDetails", method=RequestMethod.GET)
	public ServiceResponse getLogDetails(@RequestParam int projectid,@RequestParam int id,
			@RequestParam  int user_role,@RequestParam String fromDate,@RequestParam  String toDate) {	
		ServiceResponse response=projecOAuto.getLogDetails(projectid, id, user_role, fromDate,  toDate);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid",projectid);
		serviceRequestInputs.put("id",id);
		serviceRequestInputs.put("user_role",user_role);
		serviceRequestInputs.put("fromDate",fromDate);
		serviceRequestInputs.put("toDate",toDate);
		CustomLogger.logMyInfo("ProjectOverviewAutoController.getLogDetails() ",serviceRequestInputs, session , response );
		 return response;	
	}
	@RequestMapping(value="/getSummaryDetails", method=RequestMethod.GET)
	public ServiceResponse getSummaryDetails(@RequestParam int projectid,@RequestParam int id,@RequestParam  int user_role) {	
		ServiceResponse response=projecOAuto.getSummaryDetails(projectid, id,user_role);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid",projectid);
		serviceRequestInputs.put("id",id);
		serviceRequestInputs.put("user_role",user_role);
		CustomLogger.logMyInfo("ProjectOverviewAutoController.getSummaryDetails() ",serviceRequestInputs, session , response );
		 return response;
	}
	@RequestMapping(value="/getallActiveRun", method=RequestMethod.GET)
	public ServiceResponse getallActiveRun(@RequestParam int projectid,@RequestParam int id,
			@RequestParam int user_role) {	
		ServiceResponse response=projecOAuto.getallActiveRun(projectid, id, user_role);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid",projectid);
		serviceRequestInputs.put("id",id);
		serviceRequestInputs.put("user_role",user_role);
		CustomLogger.logMyInfo("ProjectOverviewAutoController.getallActiveRun() ",serviceRequestInputs, session , response );
		 return response;
	}
	@RequestMapping(value = "/runwiseAutodetails", method = RequestMethod.GET)
	public ServiceResponse runwiseAutodetails(@RequestParam String runlist,
			@RequestParam int projectid,@RequestParam int id, @RequestParam int user_role) {
		//System.out.println(runlist);
		ServiceResponse response = projecOAuto.runwiseAutodetails(runlist, projectid, id,  user_role);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid",projectid);
		serviceRequestInputs.put("id",id);
		serviceRequestInputs.put("user_role",user_role);
		serviceRequestInputs.put("runlist",runlist);
		CustomLogger.logMyInfo("ProjectOverviewAutoController.runwiseAutodetails() ",serviceRequestInputs, session , response );
		return response;

	}
	@RequestMapping(value = "/getSingleproject", method = RequestMethod.GET)
	public ServiceResponse getSingleproject(@RequestParam int projectid) {
		ServiceResponse response = projecOAuto.getProjectSelf(projectid);
		//return response;
		serviceRequestInputs=new HashMap<String,Object>();
		serviceRequestInputs.put("projectid",projectid);
		CustomLogger.logMyInfo("ProjectOverviewAutoController.getSingleproject() ",serviceRequestInputs, session , response );
		return response;

	}
}
