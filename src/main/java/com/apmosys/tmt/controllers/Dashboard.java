package com.apmosys.tmt.controllers;
import java.util.Map;

//import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.apmosys.tmt.Codec;
import com.apmosys.tmt.models.UserDetailsEntity;
import com.apmosys.tmt.services.DashboardService;
import com.apmosys.tmt.utils.CustomLogger;
import com.apmosys.tmt.utils.ServiceResponse;

import jakarta.servlet.http.HttpSession;
@RestController
@RequestMapping(path="/api" ) 
public class Dashboard 
{
	//	private static final Logger LOG=LoggerFactory.getLogger(AA.class);

	@Autowired
	private Codec codec;

	@Autowired
	private HttpSession session;

	@Autowired
	DashboardService dashboardService;
	
	@Autowired
	CustomLogger CustomLogger;

	private Map<String,Object> serviceRequestInputs;
	/*
	 * getting called on dashboard statup to get projects details relating with user
	 * based on userId and its role 
	 */
	@RequestMapping(value="/projectholeview", method=RequestMethod.POST)
	public ServiceResponse projectholeview(
//			@RequestHeader("Authorization") String token,
			int userid, int roleid , 
			String isActive,
			int pageNo,
			int PageSize) {	
		ServiceResponse response=null;
		try {
			//detailsEntity.getusername will give userName
//			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
//			String sestoken = detailsEntity.getTokenID();
//			String tokenID = codec.decrypt(token);
//			if (tokenID.equals(sestoken)) 
			{
				response=dashboardService.projectholeview(userid,roleid,isActive,pageNo,PageSize);
				System.out.print("Dashboard controller response:"+response);
				/*
				 * setting inputs
				 */
				serviceRequestInputs=new HashedMap<String,Object>();
				serviceRequestInputs.put("userid",userid);
				serviceRequestInputs.put("roleid",roleid);
				serviceRequestInputs.put("isActive",isActive);
				/*
				 * adding projects Name to session Object of user
				 */
			/*
			 	if(response.getServiceResponse() instanceof maindashboardproject);
			 	ArrayList<maindashboardproject> p=(ArrayList<maindashboardproject>)response.getServiceResponse();
				List<String> projs=new ArrayList<String>();
				p.stream().forEach(x->projs.add(x.getProjectname()));
				session.setAttribute("projName", projs);		
			 */
				CustomLogger.logMyInfo("DashBoard.projectholeview() ",serviceRequestInputs, session , response );
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return response;

	}
	@RequestMapping(value="/projectviewdata", method=RequestMethod.POST)
	public ServiceResponse projectviewdata(int userid,int roleid,String isActive) {	
		ServiceResponse response=dashboardService.projectviewdata(userid,roleid, isActive);
		serviceRequestInputs=new HashedMap<String,Object>();
		serviceRequestInputs.put("UserId",userid);
		serviceRequestInputs.put("RoleId",roleid);
		serviceRequestInputs.put("IsActive",isActive);
		CustomLogger.logMyInfo("DashBoard.projectviewdata() ",serviceRequestInputs, session , response );
		return response;
	}


	@RequestMapping(value="/projectholeviewAuto", method=RequestMethod.POST)
	public ServiceResponse projectholeviewAuto(int userid,int roleid,int start) {	
		ServiceResponse response=dashboardService.projectholeviewAuto(userid,roleid,start);
		serviceRequestInputs=new HashedMap<String,Object>();
		serviceRequestInputs.put("userid",userid);
		serviceRequestInputs.put("roleid",roleid);
		CustomLogger.logMyInfo("DashBoard.projectholeviewAuto() ",serviceRequestInputs, session , response );
		return response;

	}
	@RequestMapping(value="/getRunStatus", method=RequestMethod.POST)
	public ServiceResponse getRunStatus(int projectid,int id, int user_role) {	
		ServiceResponse response=dashboardService.getRunStatus(projectid, id, user_role);
		serviceRequestInputs=new HashedMap<String,Object>();
		serviceRequestInputs.put("projectid ",projectid);
		serviceRequestInputs.put("id ",id);
		serviceRequestInputs.put("user_role ",user_role);
		CustomLogger.logMyInfo("DashBoard.getRunStatus() ",serviceRequestInputs, session , response );
		return response;
	}
}
