package com.apmosys.tmt.controllers;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;

//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.apmosys.tmt.Codec;
import com.apmosys.tmt.models.Admin;
import com.apmosys.tmt.models.UserDetailsEntity;
import com.apmosys.tmt.models.UserSessionDetails;
import com.apmosys.tmt.repository.UserSessionsRepo;
import com.apmosys.tmt.services.AdminService;
import com.apmosys.tmt.utils.CustomLogger;
import com.apmosys.tmt.utils.ServiceResponse;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

@RestController
@RequestMapping(path="/api")
public class LoginController {
	
	@Value("${app.license.key}")
	private String licenseKey;
	@Autowired
	private Codec codec;
	@Autowired
	private HttpSession session;
	@Autowired
	private UserSessionsRepo userSessionRepo;
	
	HashMap<String, Object> serviceRequestInputs ;
		
	@Autowired
	private AdminService adminService;
		
	@Autowired
	CustomLogger CustomLogger;
		
		@RequestMapping(value="/login", method=RequestMethod.POST)
		public ServiceResponse loginUser(@RequestBody Admin user,HttpServletRequest request) throws Exception {
			String remoteAddr=null;
			if (request != null) {
	            remoteAddr = request.getHeader("X-FORWARDED-FOR");
	            //System.out.println(request.getLocalAddr()+"  "+request.getScheme()+" "+request.getRemoteAddr());
	            if (remoteAddr == null || "".equals(remoteAddr)) {
	                remoteAddr = request.getRemoteAddr();
	            }
	        }
			//System.out.println("welcome to portal");
			ServiceResponse response=new ServiceResponse();
			response= adminService.userLogin(user,remoteAddr);
			
			//return response;
		    serviceRequestInputs = new HashMap<String,Object>();
			serviceRequestInputs.put("user",user);
			CustomLogger.logMyInfo("LoginController.getUserbyid() ",serviceRequestInputs, session , response );
			
			return response;
			

		}
		
		@RequestMapping(value = "/checkDBpassword", method = RequestMethod.POST)
		public ServiceResponse checkDBpassword(@RequestHeader("Authorization") String token,
				@RequestParam String currentPass) {
			ServiceResponse response = new ServiceResponse();
			try {
				UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
				String sestoken = detailsEntity.getTokenID();
				String tokenID = codec.decrypt(token);
				if (tokenID.equals(sestoken)) {
					response = adminService.checkDBpassword(currentPass, detailsEntity.getUsername());
				}
			} catch (Exception e) {
				System.out.print(e.toString());
			}
			//return response;
			serviceRequestInputs = new HashMap<String,Object>();
			serviceRequestInputs.put("currentPass ",currentPass);
			CustomLogger.logMyInfo("LoginController.checkDBpassword() ",serviceRequestInputs, session , response );
			return response;
		}
		@RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
		public ServiceResponse updatePassword(@RequestHeader("Authorization") String token,
				@RequestParam String currentPass, @RequestParam String newPass) {
			ServiceResponse response = new ServiceResponse();
			try {
				UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
				String sestoken = detailsEntity.getTokenID();
				String tokenID = codec.decrypt(token);
				if (tokenID.equals(sestoken)) {
					response = adminService.updatePassword(currentPass, newPass, detailsEntity.getUsername());

				}
			} catch (Exception e) {
				System.out.print(e.toString());
			}
			//return response;
			serviceRequestInputs = new HashMap<String,Object>();
			serviceRequestInputs.put("currentPass ",currentPass);
			serviceRequestInputs.put("newPass ",newPass);
			CustomLogger.logMyInfo("LoginController.updatePassword() ",serviceRequestInputs, session , response );
			return response;
		}
		
		
		@RequestMapping(value = "/getAccountDetails", method = RequestMethod.POST)
		public ServiceResponse getAccountDetails(@RequestHeader("Authorization") String token,
				@RequestParam String usename, @RequestParam String userrole,@RequestParam String managerId) {
			System.out.println("LoginController.getAccountDetails()");
			System.out.println("Authorization"+ token);
			System.out.println("usename"+ usename);
			System.out.println("userrole"+ userrole);
			ServiceResponse response = new ServiceResponse();
			try {
				UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
				String sestoken = detailsEntity.getTokenID();
				String tokenID = codec.decrypt(token);
				if (tokenID.equals(sestoken)) {
					response = adminService.getAccountDetails(usename, userrole,managerId, detailsEntity.getUsername());
					//System.out.println("ss"+detailsEntity.getPassword());
					
				}
			} catch (Exception e) {
				System.out.print(e.toString());
			}
			//return response;
			serviceRequestInputs = new HashMap<String,Object>();
			serviceRequestInputs.put("usename ",usename);
			serviceRequestInputs.put("userrole ",userrole);
			serviceRequestInputs.put("managerId ",managerId);
			CustomLogger.logMyInfo("LoginController.getAccountDetails() ",serviceRequestInputs, session , response );
			return response;
		}
		
		@RequestMapping(value = "/getMyEntitlementFnct", method = RequestMethod.POST)
		public ServiceResponse getMyEntitlementFnct(@RequestHeader("Authorization") String token,
				@RequestParam String userId,@RequestParam String role) {
			ServiceResponse response = new ServiceResponse();
			try {
				UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
				String sestoken = detailsEntity.getTokenID();
				String tokenID = codec.decrypt(token);
				if (tokenID.equals(sestoken)) {
					response = adminService.getMyEntitlementFnct(userId,role);
					//System.out.println("ss"+detailsEntity.getPassword());
				}
			} catch (Exception e) {
				System.out.print(e.toString());
			}
			//return response;
			serviceRequestInputs = new HashMap<String,Object>();
			serviceRequestInputs.put("userId ",userId);
			serviceRequestInputs.put("role ",role);
			CustomLogger.logMyInfo("LoginController.getMyEntitlementFnct() ",serviceRequestInputs, session , response );
			return response;
		}
		
		

		
	/*
	 * @RequestMapping(value="/getAllUsers", method=RequestMethod.POST) public
	 * ServiceResponse getAllUsers(){
	 * 
	 * ServiceResponse serviceResponse=new ServiceResponse();
	 * serviceResponse=adminService.getAllAdmins(); return serviceResponse; }
	 * 
	 * @RequestMapping(value="/createUser", method=RequestMethod.POST) public
	 * ServiceResponse createUser(@RequestBody User user){
	 * 
	 * ServiceResponse serviceResponse=new ServiceResponse();
	 * serviceResponse=adminService.createUser(user); return serviceResponse; }
	 */
		@RequestMapping(value="/sessionKills", method=RequestMethod.POST)
        public ServiceResponse sessionKills(){
         
         ServiceResponse serviceResponse=new ServiceResponse();
         UserSessionDetails usd=null;
// 		session=request.getSession();
 		if(session!=null) {
 			UserDetailsEntity detailsEntity = (UserDetailsEntity) session.getAttribute("user");
 			if(detailsEntity !=null) {
 				usd = userSessionRepo.findById(detailsEntity.getUserID()).orElse(null);
 				usd.setLast_check(new Date());
 				int count= userSessionRepo.updateSession(usd.getUserId(),usd.getSession_key(), "N" , new Date());
 				System.out.println("Invalidated session.. count "+count); 
 			}//if
 		}//if
 		else
 			System.out.println("Unale to Invalidate session Last check.. count "); 
         
         serviceResponse=adminService.sessionKills();
        //return response;
        
        serviceRequestInputs = new HashMap<String,Object>();
        CustomLogger.logMyInfo("LoginController.sessionKills() ",serviceRequestInputs, session , serviceResponse );
        return serviceResponse;
        }
		
		@RequestMapping(value = "/checkSession", method = RequestMethod.POST)
		public ServiceResponse sessionAvailable()
		{
		//System.out.println("logout checkSession controller");
		ServiceResponse response=new ServiceResponse();
		response=adminService.sessionAvailable();

		//return response;
		serviceRequestInputs = new HashMap<String,Object>();
		CustomLogger.logMyInfo("LoginController.sessionAvailable() ",serviceRequestInputs, session , response );
		return response;

		}

	
		@RequestMapping(value = "/checklicense", method = RequestMethod.POST)
		public String checklicense()
		{

			try {
				if (licenseKey != null) {
					String decode = codec.decrypt(licenseKey);
					//System.out.println(decode);
					String[] splitVal = decode.split("TMT");
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					String macId = codec.IP_HOST();
					//System.out.println("ststem loginixce ..dateFormat."+(dateFormat.parse(splitVal[2])));
					if ((splitVal[0] + "TMT" + splitVal[1]).equals(macId)) {
						if (dateFormat.parse(splitVal[2]).after(new Date())) {
						
								return "true";
							
						} else
							return "false";
					} else {
						return "false";
					}
				} else {
					return "false";
				}
			} catch (Exception e) {
				return "false";
			}
		}


		@RequestMapping(value = "/checklicenseexpred", method = RequestMethod.POST)
		public ServiceResponse checklicenseexpred()
		{ String str=null;
        ServiceResponse serviceResponse=new ServiceResponse();


			try {
				if (licenseKey != null) {
					String decode = codec.decrypt(licenseKey);
					//System.out.println(decode);
					String[] splitVal = decode.split("TMT");
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					String macId = codec.IP_HOST();
					
					
					  
				
				  SimpleDateFormat formDate = new SimpleDateFormat("yyyy-MM-dd");
				  
				  // String strDate = formDate.format(System.currentTimeMillis()); // option 1
				  String strDate =formDate.format(new Date()); // option 2
				 
				 String licedate=formDate.format(dateFormat.parse(splitVal[2]));
				 //System.out.println("strDate"+strDate+"..licedate.."+licedate+"..strDate.compareTo(licedate)"+strDate.compareTo(licedate));
				 //String diffdate=licedate.-strDate;
				 LocalDate dateBefore = LocalDate.parse(strDate);
					LocalDate dateAfter = LocalDate.parse(licedate);
						
					//calculating number of days in between
					long noOfDaysBetween = ChronoUnit.DAYS.between(dateBefore, dateAfter);
					//System.out.println(noOfDaysBetween+"...noOfDaysBetween..");	
					  if(noOfDaysBetween<15){
						  
						  if(strDate.compareTo(licedate)==0)
						  {
							  //System.out.println("set time"+noOfDaysBetween);
							   // str=noOfDaysBetween;
							    serviceResponse.setServiceStatus("200");
							    serviceResponse.setServiceResponse(noOfDaysBetween);
							   return serviceResponse;
						  }
						  else {
					  // //System.out.println("set time"+noOfDaysBetween);
					   // str="Less than "+noOfDaysBetween+"days";
					    serviceResponse.setServiceStatus("200");
					    serviceResponse.setServiceResponse(noOfDaysBetween);
					   return serviceResponse;
						  }
					  }
					  else {
						  serviceResponse.setServiceStatus("400");
						    serviceResponse.setServiceResponse("Liecene not expried");
					  }
					
				} else {
					serviceResponse.setServiceStatus("400");
					return serviceResponse;
				}
			} catch (Exception e) {
				serviceResponse.setServiceResponse("Error");
				return serviceResponse;
			}
			//return response;
			serviceRequestInputs = new HashMap<String,Object>();
			CustomLogger.logMyInfo("LoginController.checklicenseexpred() ",serviceRequestInputs, session , serviceResponse );
			return serviceResponse;
		}

		
		
		@GetMapping("/updateSession")
		public String updateSession() {
		//	System.out.println("done its came");
			adminService.updateSessionLastCheck();
			return null;
		}//updateSession
		
		
		
		
		
		
		
		
		
		
}
