package com.apmosys.tmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.apmosys.tmt.models.ProjectDocument;

public interface ProjectDocumentRepo extends JpaRepository<ProjectDocument, Integer>{
	@Query("From ProjectDocument where isPublic='Y'")
	public List<ProjectDocument> getPublicDocument();

//	public ProjectDocument findOne(Integer pId);

}
