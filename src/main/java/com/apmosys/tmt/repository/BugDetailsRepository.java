package com.apmosys.tmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apmosys.tmt.models.BugDetailsEntity;

public interface BugDetailsRepository extends JpaRepository<BugDetailsEntity, Integer> {

	 public BugDetailsEntity findByBugIDAndBugtoolIDAndProjectID(String bugID, Integer bugtoolID, Integer projectID);
	
	 public List<BugDetailsEntity> findByProjectID(Integer projectID);
	 
	 @Query(value="SELECT bugStatus FROM BugDetailsEntity where bugID=:bugID")
	 public List<String> getBugStatusByBugId(@Param("bugID") String bugID);

	 
}
