package com.apmosys.tmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.AutomationScreenshot;

@Repository
@Transactional
public interface AutomationScreenshotRepository extends JpaRepository<AutomationScreenshot, Integer> {

	AutomationScreenshot findByScreenshotId(Integer screenshotId);
	
	@Query(value="select * from screenshots s left JOIN logs l ON l.ScreenshotID=s.id where s.id=? ", nativeQuery=true)
	   public AutomationScreenshot getss(Integer id);


}
