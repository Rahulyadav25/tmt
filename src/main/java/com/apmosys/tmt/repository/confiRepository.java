package com.apmosys.tmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apmosys.tmt.models.UserDetailsEntity;

public interface confiRepository extends JpaRepository<UserDetailsEntity, Integer>{

	//@Query("From UserDetailsEntity UserDetails Where UserDetails.userID=:userid and UserDetails.username = :mailId and UserDetails.password =:password")
	
/*	List<UserDetailsEntity> findByUserIDAndUsernameAndNameAndUser_role(String userid, String username, String name,
			String username2);*/

	//<UserDetailsEntity> findByUserIDAndUsernameOrNameOrUserrole(String userid,String username,String name,int role);
	List<UserDetailsEntity> findByUserID(String userid);

	List<UserDetailsEntity> findByUserIDOrUsernameOrNameOrIsActive(String userid, String username, String name,
			String isactive);

/*	List<UserDetailsEntity> findByUsernameAndNameAndUser_role(String userid, String username, String name,
			String username2);*/

}