package com.apmosys.tmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apmosys.tmt.models.BugTrackorToolEntity;

public interface BugTrackerRepository extends JpaRepository<BugTrackorToolEntity, Integer>{
	 public List<BugTrackorToolEntity> findByBugTrackingToolName(String bugTrackingToolName);
	 
	 @Query(value="SELECT BuGTrackingToolName FROM BugIntegrationTool WHERE Toolid=:id", nativeQuery=true)
	 public String findByBugToolId(@Param("id") Integer id);
	 
	 public BugTrackorToolEntity findByToolid(Integer integer);
	 
	 public BugTrackorToolEntity findByBugTrackingToolNameAndUserDefinedToolName(String bugTrackingToolName, 
			 String userDefinedToolName);
}
