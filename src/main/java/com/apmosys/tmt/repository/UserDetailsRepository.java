package com.apmosys.tmt.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.apmosys.tmt.models.UserDetailsEntity;

public interface UserDetailsRepository extends JpaRepository<UserDetailsEntity, Integer>{
	@Query("From UserDetailsEntity")
	public List<UserDetailsEntity> getAllProject();
	public UserDetailsEntity findByUserID(String userID);

	
	public UserDetailsEntity findByUserIDAndManagerID(Integer valueOf, String userid);
	public Optional<UserDetailsEntity> findById(Integer valueOf);
	
	public UserDetailsEntity findByName(String name);
	
	@Query("From UserDetailsEntity rt where rt.managerID =:userid ")
	public List<UserDetailsEntity> findByUserList(Integer userid);
	
}
