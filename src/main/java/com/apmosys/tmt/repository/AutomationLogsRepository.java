package com.apmosys.tmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.AutomationRunLogs;

public interface AutomationLogsRepository extends JpaRepository<AutomationRunLogs, Integer>{

//	public AutomationRunLogs findByProject_name(String oldprojectname);
	
	// @Query(value="FROM logs WHERE project_name = :projectname;",nativeQuery=true)
	@Query("FROM AutomationRunLogs m where m.project_name = :project_name")
	public List<AutomationRunLogs> findByProject_name(@Param("project_name") String project_name);
	//public List<AutomationRunLogs> findByProject_name();
	
	@Transactional
	@Modifying
	@Query("UPDATE AutomationRunLogs l SET l.project_name = :projectname where l.project_name=:oldprojectname")
	public void updateLogProjectname(@Param("projectname") String projectname,@Param("oldprojectname") String oldprojectname);
	
}
