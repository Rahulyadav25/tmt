package com.apmosys.tmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.AutomationMetadata;

public interface AutomationMetaRepository extends JpaRepository<AutomationMetadata, Integer> {

	AutomationMetadata findByProjectid(int projectid);

	AutomationMetadata findByProjectname(String projectName);

	@Transactional
	@Modifying
	@Query("UPDATE AutomationMetadata l SET l.noOftestSuites = 0 where l.projectid=:projectid")
	 void updateData(@Param("projectid")int projectid);

	@Transactional
	@Modifying
	@Query("UPDATE AutomationMetadata l SET l.noOfTestcases =:NoTC where l.projectid=:projectid")
	 void updateDataone(@Param("projectid")int projectid,@Param("NoTC") int NoTC);
	
	
	@Transactional
	@Modifying
	@Query("delete from AutomationMetadata where projectid=:projectid")
	public Integer deleteByAutoProjectID(@Param("projectid") Integer projectid);
}
