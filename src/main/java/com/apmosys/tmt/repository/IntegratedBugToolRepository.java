package com.apmosys.tmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.apmosys.tmt.models.IntegratedBugToolEntity;

public interface IntegratedBugToolRepository extends JpaRepository<IntegratedBugToolEntity, Integer>{
	
	public IntegratedBugToolEntity findByBugToolName(String bugToolName);
}
