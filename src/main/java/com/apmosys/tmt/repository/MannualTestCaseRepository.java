package com.apmosys.tmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.MannualTestCaseTable;
@Repository
@Transactional
public interface MannualTestCaseRepository extends JpaRepository<MannualTestCaseTable, Integer> {
	  @Query("FROM MannualTestCaseTable m where m.testGroupId = :testGroupId") 
	   public List<MannualTestCaseTable> findTestCasesByTestGroupId(@Param("testGroupId") Integer groupId);
	  
	  @Query("Select Max(srNo) FROM MannualTestCaseTable m where m.testGroupId = :testGroupId") 
	  public Integer findMaxTestCasesId(@Param("testGroupId") Integer groupId);
	  
	  	@Transactional
		@Modifying
		@Query("DELETE FROM MannualTestCaseTable WHERE testGroupId=:testGroupId")
		public void deleteAllTestCasesByGId(@Param("testGroupId") Integer testGroupId);

	  	@Query(value="Select m.* FROM Mannual_TestCase_Table m where m.SrID = :caseId",nativeQuery=true) 
		public MannualTestCaseTable findTestCaseBySrID(@Param("caseId") Integer caseId);

		@Query(value="Select * FROM Mannual_TestCase_Table order by SrID desc limit 1",nativeQuery=true) 
		public Integer findLastRowTS();
		
		@Query(value="Select m.* FROM Mannual_TestCase_Table m where m.SrID > :findlatindex",nativeQuery=true)
		public List<MannualTestCaseTable> findNewAddID(@Param("findlatindex") Integer findlatindex);

		@Transactional
		@Modifying
		@Query("update MannualTestCaseTable mc set mc.bugStatus=:s WHERE mc.SrID=:srid")
		public int updateBugStatus(@Param("s") String s, @Param("srid") Integer srid);
		
		@Transactional
		@Modifying
		@Query("update MannualTestCaseTable mc set mc.bugStatus=:s"
				+ ", mc.bugSeverity=:se, mc.bugPriority=:p WHERE mc.SrID=:srid")
		public int updateBugDetails(@Param("s") String s, @Param("se") String se
				, @Param("p") String p, @Param("srid") Integer srid);

		@Query(value="Select count(testID) FROM Mannual_TestCase_Table where testGroupId=:testGroupId and testID=:testID",nativeQuery=true) 
		public Integer findByTestIsByTestgrp(@Param("testID") String testID,@Param("testGroupId")  Integer testGroupId);

//		public MannualTestCaseTable findOne(Integer absoluteTCID);

		//public Integer FindByTestID(Integer testGroupId, String testID);
		
	}
