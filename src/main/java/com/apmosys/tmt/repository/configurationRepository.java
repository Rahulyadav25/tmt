package com.apmosys.tmt.repository;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.map.HashedMap;
//import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.apmosys.tmt.BO.BugTrackor;
import com.apmosys.tmt.models.AccessDetail;
import com.apmosys.tmt.models.BugTrackorToolEntity;
import com.apmosys.tmt.models.DateWiseGraphData;
import com.apmosys.tmt.models.DateWiseRunData;
import com.apmosys.tmt.models.ProjectAccessEntity;
import com.apmosys.tmt.models.ProjectDeatilsMeta;
import com.apmosys.tmt.models.ProjectEntitlement;
import com.apmosys.tmt.models.RoleEntity;
import com.apmosys.tmt.models.RunWiseGraphData;
import com.apmosys.tmt.models.UserDetailsEntity;
import com.apmosys.tmt.models.userRole;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
@Repository
@Transactional

public class configurationRepository {
	@PersistenceContext
    private EntityManager entityManager;
	@Value("${password}")
	String password;
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<userRole> getAllUseradmin() {
		List<Object[]> list=null;
		List<userRole> test=new ArrayList<userRole>();
//		Session session = sessionFactory.getCurrentSession();
		Session session=entityManager.unwrap(Session.class);
		Query qry=null;
		try
		{
		String sql = "select u.userID,u.name,(select u1.name from UserDetailsEntity u1 where u1.userID=u.managerID) as managerName,(select r.roleType from RoleEntity r where r.roleId=u.userrole) as userRole,"
				+"u.isActive,u.username,u.managerID,u.id,u.userrole,(select u1.failedAttemptCount from UserFailedLogins u1 where u1.userId=u.userID) as failedLogins from  UserDetailsEntity u order by u.name asc";
		qry = session.createQuery(sql);
		 list = qry.list();
	System.out.println("================"+list);
		for (Object[] objects : list)
		{	userRole user=new userRole();
			user.setName(objects[1].toString());
		    user.setUserid((String) objects[0] !=null?(String)objects[0]:null);
		    user.setUsername(objects[5].toString());
		    if(objects[3]!=null)
		    {
			user.setUserrole(objects[3].toString());
		    }
			user.setIsactive(objects[4].toString());
			//user.setPassword(objects[5].toString());
			user.setCreatorid(objects[6].toString());
			user.setId(Integer.valueOf(objects[7].toString()));
			user.setUserroleid(Integer.valueOf(objects[8].toString()));
			if(objects[9]!=null)
			{
				user.setFiledAttempts(Integer.valueOf(objects[9].toString()));
			} else user.setFiledAttempts(0);
			test.add(user);
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return test;
		
	}
	public List<RoleEntity> getRoles() {
//		Session session=entityManager.unwrap(Session.class);
		Session session=entityManager.unwrap(Session.class);
		String sql = "from  RoleEntity";
		Query query = session.createQuery(sql);
		List<RoleEntity> roles = query.list();
		return roles;
	}
	
	@Transactional
	public String createUser(userRole user) {
		Object count=0;
		Session session=null;
		String msg="Success";
		try
		{
		UserDetailsEntity entity=new UserDetailsEntity();
		entity.setIsActive("Y");
		entity.setPassword(password);
		entity.setUsername(user.getUsername());
		entity.setUser_role(Integer.valueOf(user.getUserrole().toString()));
		entity.setName(user.getName());
	    entity.setManagerID(Integer.valueOf(user.getCreatorid()));
	    entity.setUserID(user.getUserid());
	    entity.setCreatedBy(Integer.valueOf(user.getCreatorid()));
	    /*entity.setM(Integer.valueOf(user.getCreatorid()));*/
//	    session = sessionFactory.getCurrentSession();
	   session=entityManager.unwrap(Session.class);
//		Transaction tx=session.beginTransaction();
		count= session.save(entity);
		
//		tx.commit();
		}
		catch(MethodArgumentTypeMismatchException e)
		{
			msg="Failed to created";
			session.clear();
		}
		catch(ConstraintViolationException e)
		{
			msg="already exist";
			session.clear();
		}
		catch(Exception e)
		{
			msg="Failed to created";
			e.printStackTrace();
		}
		return msg;
	}
	public List<userRole> getUserDetailsbyMID(Integer mID) {
//		Session session=entityManager.unwrap(Session.class);
		Session session=entityManager.unwrap(Session.class);
		List<userRole> test=new ArrayList<userRole>();
		String sql = "select u.userID,u.name,(select u1.name from UserDetailsEntity u1 where u1.userID=u.managerID) as managerName,(select r.roleType from RoleEntity r where r.roleId=u.userrole) as userRole,"
				+"u.isActive,u.username,u.managerID,u.id,u.userrole, (select u1.failedAttemptCount from UserFailedLogins u1 where u1.userId=u.userID) as failedLogins from  UserDetailsEntity u where u.managerID=:managerId or u.createdBy=:managerId ";
		try
		{
		Query query = session.createQuery(sql);
		query.setParameter("managerId", mID);
		List<Object[]> list = query.list();
		for (Object[] objects : list)
		{	userRole user=new userRole();
			user.setUserid((objects[0].toString()));
			user.setName(objects[1].toString());
			if(objects[2]==null)
			{
				user.setUserCreatedBy("");	
			}
			else
			{
				user.setUserCreatedBy(objects[2].toString());
			}
		
			user.setUserrole(objects[3].toString());
			user.setIsactive(objects[4].toString());
			user.setUsername(objects[5].toString());
			user.setCreatorid(objects[6].toString());
			user.setId(Integer.valueOf(objects[7].toString()));
			user.setUserroleid(Integer.valueOf(objects[8].toString()));
			if(objects[9]!=null)
			{
				user.setFiledAttempts(Integer.valueOf(objects[9].toString()));
			} else user.setFiledAttempts(0);
			test.add(user);
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return test;
	}
	public String userRole(Integer roleId) {
		RoleEntity role=new RoleEntity();
		List<RoleEntity> list=null;
		String roletype=null;
		try
		{
//		Session session=entityManager.unwrap(Session.class);
		Session session=entityManager.unwrap(Session.class);
		String sql = "from  RoleEntity where roleId=:roleid";
		Query query = session.createQuery(sql);
		query.setParameter("roleid", roleId);
		 list = query.list();
		for (int i = 0; i < list.size(); i++)
		{
			role = list.get(i);

		}
		roletype= role.getRoleType();
		}
		catch(Exception e)
		{
			
			e.printStackTrace();
			return null;
		}
		return roletype;
	}
	public Integer userRoleID(String roleName) {
//		Session session=entityManager.unwrap(Session.class);
		Session session=entityManager.unwrap(Session.class);
		String sql = "from  RoleEntity where roleType=:roletype";
		Query query = session.createQuery(sql);
		query.setParameter("roletype", roleName);
		RoleEntity role = (RoleEntity) query.list();
		System.out.println("===="+role.getRoleType());
		return role.getRoleId();
	}
	public List<Object[]> getAllUserDetails() {
//		Session session=entityManager.unwrap(Session.class);
		Session session=entityManager.unwrap(Session.class);
		String sql = "select u.userID,u.name,(select u1.name from UserDetailsEntity u1 where u1.userID=u.managerID) as managerName,(select r.roleType from RoleEntity r where r.roleId=u.userRole) as userRole,"
				+"u.isActive,u.userName,u.managerID from  UserDetailsEntity u ";
		Query query = session.createQuery(sql);
		List<Object[]> list = query.list();
		return list;
	}
	public Serializable updateUser(UserDetailsEntity details,String roleID) {
		Serializable count=0;
		try
		{
		
//		Session session=entityManager.unwrap(Session.class);
		Session session=entityManager.unwrap(Session.class);
		String sql = "update UserDetailsEntity u1 set u1.username=:updateuser,u1.name=:username,u1.userrole=:usertype,u1.isActive=:active where u1.userID=:userid";
		System.out.println(sql);
		Query query = session.createQuery(sql);
		System.out.println("updateUser----DAO"+details.getUsername());
		query.setParameter("updateuser", details.getUsername());
		query.setParameter("username", details.getName());
		query.setParameter("usertype", details.getUser_role());
		query.setParameter("active", details.getIsActive());
		query.setParameter("userid", Integer.valueOf(details.getUserID()));
		int result = query.executeUpdate();
		System.out.println(result);
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return count;
	}
	public List<UserDetailsEntity> getUserUnderManager(Integer uId,Integer pID) {
		// TODO Auto-generated method stub
//		Session session=entityManager.unwrap(Session.class);
		Session session=entityManager.unwrap(Session.class);
		Query query=null;
		String sql=null;
		if(uId==1000)
		{
			sql = "SELECT u.userID,u.name,p.srNo,(select r.roleType from RoleTable r where r.roleId=u.userRole) from UserDetailsEntity u LEFT JOIN ProjectAccessTable p ON p.userID=u.userID and p.projectID=:pid";
			query = session.createNativeQuery(sql);
			query.setParameter("pid", pID);		
		}
		else
		{
		sql = "SELECT u.userID,u.name,p.srNo,(select r.roleType from RoleTable r where r.roleId=u.userRole) from UserDetailsEntity u LEFT JOIN ProjectAccessTable p ON p.userID=u.userID and p.projectID=:pid where u.managerID=:managerId";
		query = session.createNativeQuery(sql);
		query.setParameter("managerId", uId);
		query.setParameter("pid", pID);
		}
		
		List<Object[]> list = query.list();
		List<UserDetailsEntity> userList=new ArrayList<UserDetailsEntity>();
		for (Object[] objlist : list) {
			UserDetailsEntity entity=new UserDetailsEntity();
			entity.setUserID((String)objlist[0]);
			entity.setName((String)objlist[1]);
			entity.setUser_role(Integer.valueOf((String)objlist[3]));
			if(objlist[2]==null){
			entity.setHasAcccess("N");
			}
			else {
				entity.setHasAcccess("Y");
			}
			userList.add(entity);
		}
		return userList;
	
	}
	public List<AccessDetail> GetDetailsAccess(int userid) {
		List<Object[]> list=null;
		List<AccessDetail> test=new ArrayList<AccessDetail>();
		try
		{
//		Session session=entityManager.unwrap(Session.class);
			Session session=entityManager.unwrap(Session.class);
		Query qry=null;
	
		String sql = "select u.userDetailsId,u.projectID,p.projectName,u.AccessType from ProjectAccessTable u inner join Project_MetaTable p on u.projectID=p.projectID where u.userDetailsId=:userid ";
		qry = session.createNativeQuery(sql);
		qry.setParameter("userid",userid);
		list=qry.list();
		for (Object[] objects : list)
		{	AccessDetail user=new AccessDetail();
			user.setUserid(Integer.valueOf(objects[0].toString()));
			user.setProjectid(Integer.valueOf(objects[1].toString()));
			user.setProjectName(objects[2].toString());
			if(objects[3]!=null)
			{
			user.setAccesstype(objects[3].toString());
			}
			test.add(user);
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		
		}
		return test;
	}
	
	@Transactional
	public Object saveRole(String role) {
		Object count=0;
		
		try
		{
			System.out.println("project id------------"+role);		
			RoleEntity accessEntity=new RoleEntity();
		accessEntity.setRoleType(role);
//		Session session=entityManager.unwrap(Session.class);
		Session session=entityManager.unwrap(Session.class);
//		Transaction tx=session.beginTransaction();
		count= session.save(accessEntity);
//		tx.commit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			count=0;
		}
		return count;

		
	}

	
	@Transactional
	public String giveProjectAccess(List<ProjectAccessEntity> accesEntities, Integer uId) {
		Object count = 0;
//		Session session=entityManager.unwrap(Session.class);
		Session session=entityManager.unwrap(Session.class);
		String check = "success";
		try {
			Query qry = null;
//			Transaction tx1 = session.beginTransaction();
			String sql = "delete from ProjectAccessEntity u where u.userDetailsId=:userid";
			qry = session.createQuery(sql);
			// qry.setParameter("projectid",Integer.valueOf(arr[i]));
			qry.setParameter("userid", uId);
			qry.executeUpdate();
//			tx1.commit();
			if (!accesEntities.isEmpty()) {
				for (ProjectAccessEntity projectAccessEntity : accesEntities) {
					try {
//					Transaction tx6 = session.beginTransaction();
					count = session.save(projectAccessEntity);
//					tx6.commit();
					}
					catch (Exception e) {
						e.fillInStackTrace();
					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			check = "issue";
			session.clear();
		}

		System.out.println(check);
		return check;

	}
	
	public List<userRole> getAccountDetails(String userame, String userrole,String managerId) {
//		Session session=entityManager.unwrap(Session.class);
		Session session=entityManager.unwrap(Session.class);
		List<userRole> ue=new ArrayList<>();
		List<Object[]> list=new ArrayList<>();
		try
		{
		String sql = "select usen.name,usen.userName,usen.createdDate,usen.id,usen.userID,usen.isActive ,rt.roleType,\n" + 
				"if(tmp.usrrole='100' OR tmp.totalCount=0 ,'Admin','Manager') as userroleAdmin\n" + 
				"from UserDetailsEntity usen,RoleTable rt ,(select se.userRole as usrrole ,\n" + 
				"id,COUNT(*) as totalCount  from UserDetailsEntity se where se.id=:managerId)tmp\n" + 
				"              where\n" + 
				"usen.userRole = rt.roleId and usen.userName=:userame";
		Query query = session.createNativeQuery(sql);
		System.out.print("query for account details"+query);
		query.setParameter("userame",userame);
		query.setParameter("managerId", managerId);
		
		list=query.list();
		for (Object[] objects : list)
		{	userRole user=new userRole();
			user.setName(objects[0].toString());
			user.setUsername(objects[1].toString());
			user.setCreatedDate(objects[2]!=null?objects[2].toString():null);
			user.setUserid(objects[4].toString());
			user.setIsactive(objects[5].toString());
			user.setRoleType(objects[6].toString());
			user.setUserroleAdmin(objects[7].toString());
			ue.add(user);
		}
		System.out.print("ue account details"+ue);

		
		}
		catch(Exception e)
		{
			e.printStackTrace();
			
		}
		return ue;
	}
	
	public List<userRole> getMyEntitlementFnct(String userId) {
//		Session session=entityManager.unwrap(Session.class);
		Session session=entityManager.unwrap(Session.class);
		List<userRole> ue=new ArrayList<>();
		List<Object[]> list=new ArrayList<>();
		try
		{
		String sql = "select pmt.projectName ,pat.AccessType from ProjectAccessTable pat, Project_MetaTable pmt\n" + 
				"where pmt.projectID=pat.projectID and pat.userDetailsId=:userId ";
		Query query = session.createNativeQuery(sql);
		query.setParameter("userId",userId);
		list=query.list();
		for (Object[] objects : list)
		{	userRole user=new userRole();
			user.setProjectName(objects[0].toString());
			user.setAccessType(objects[1].toString());
			ue.add(user);
		}
		
		
		}
		catch(Exception e)	
		{
			e.printStackTrace();
			System.out.println("esmmmmmmmmmmmmmmmmmmmmm");
		}
		return ue;
	}
	
	
	@Transactional
	public void deleteProjectAccess(String pId, String uId) {
		Session session=entityManager.unwrap(Session.class);
		
		ProjectAccessEntity accessEntity=new ProjectAccessEntity();
		accessEntity.setProjectID(Integer.parseInt(pId));
		accessEntity.setUserID(Integer.parseInt(uId));
		String arr[]=pId.split(",");
		System.out.println(arr);
		System.out.println("++++++++++++giveProjectAccess+++");
		try
		{
//			Transaction tx=session.beginTransaction();
			System.out.println("project id------------"+pId);		
		System.out.println("user id------------"+uId);	
		for(int i=0;i<arr.length;i++)
		{
		String sql = "delete from ProjectAccessEntity where projectID=:pID and userDetailsId=:uID";
		Query query = session.createQuery(sql);
		query.setParameter("pID",arr[i]);
		query.setParameter("uID", Integer.parseInt(uId));
		query.executeUpdate();
//		tx.commit();
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public Integer getuserCount() {
		Integer i=0;
		Session session=entityManager.unwrap(Session.class);			
		ProjectAccessEntity accessEntity=new ProjectAccessEntity();
		String sql = "select count(*) from UserDetailsEntity";
		Query query = session.createQuery(sql);
		 i=query.getFirstResult();
		 System.out.println(i+"==");
		return i;
	}
	public List<userRole> getUserbyID(int userid) {
		List<Object[]> list=null;
		List<userRole> test=new ArrayList<userRole>();
		Session session=entityManager.unwrap(Session.class);
		System.out.println("-------------------->>>>>>>>>heeeeeeeeeeee");
		Query qry=null;
		try
		{
		String sql = "select u.userID,u.name,(select u1.name from UserDetailsEntity u1 where u1.userID=u.managerID) as managerName,(select r.roleType from RoleEntity r where r.roleId=u.userrole) as userRole,"
				+"u.isActive,u.username,u.managerID,u.id from  UserDetailsEntity u where u.id=:userid ";
		qry = session.createQuery(sql);
		qry.setParameter("userid",userid);
		list=qry.list();
	      //System.out.println(list);
		
		for (Object[] objects : list)
		{	userRole user=new userRole();
			user.setUserid(objects[0].toString());
			user.setName(objects[1].toString());
			user.setUsername(objects[5].toString());
			if(objects[2]==null)
			{
				user.setUserCreatedBy("");	
			}
			else
			{
				user.setUserCreatedBy(objects[2].toString());
			}
		if(objects[3]!=null)
		{
			user.setUserrole(objects[3].toString());
		}
			/*user.setIsactive(objects[4].toString());
			user.setPassword(objects[5].toString());*/
			user.setCreatorid(objects[6].toString());
			user.setId(Integer.valueOf(objects[7].toString()));
			test.add(user);
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return test;
	}
	public String getUserByMailid(String creator) {
		Session session=entityManager.unwrap(Session.class);
		Query qry=null;
		String userid=null;
		List<Object[]> list=null;
		try
		{
		String sql = "select u.id,u.name from UserDetailsEntity u where u.username=:name ";
		qry = session.createQuery(sql);
		qry.setParameter("name",creator);
		list=qry.list();
		for (Object[] objects : list)
		{	userid=objects[0].toString();
			
		
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return userid;
	}
/*	public Serializable updateUser(com.apmosys.tmt.models.userRole user) {
		// TODO Auto-generated method stub
		return null;
	}*/
	@Transactional
	public String DeleteUser(int id) {
		Session session=entityManager.unwrap(Session.class);
		Query qry=null;
		List<Object[]> list=null;
	
		String msg="success";
		try
		{
//			Transaction tx=session.beginTransaction();
		String sql = "delete from UserDetailsEntity u where u.id=:userid ";
		qry = session.createQuery(sql);
		qry.setParameter("userid",id);
		qry.executeUpdate();
//		tx.commit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			msg="Fail due to dependency in project assign table";
		}
		return msg;
	}
	
	@Transactional
	public Serializable deleterole(int roleid) {
		Session session=entityManager.unwrap(Session.class);
		Query qry=null;
		List<Object[]> list=null;
//		Transaction tx=session.beginTransaction();
		try
		{
		String sql = "delete from RoleEntity u where u.roleId=:userid ";
		qry = session.createQuery(sql);
		qry.setParameter("userid",roleid);
		qry.executeUpdate();
//		tx.commit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return roleid;
	}
	
	@Transactional
	public String removeAccess(String projectid, int userid) {
		Session session=entityManager.unwrap(Session.class);
		String status="fail";
		Query qry=null;
		List<Object[]> list=null;
	
		System.out.println("removeAccess"+projectid);
		System.out.println("removeAccess"+userid);
		try
		{
//			Transaction tx=session.beginTransaction();
		String sql = "delete from ProjectAccessEntity u where u.userDetailsId=:userid and u.projectID IN (\'"+projectid+"\')";
		qry = session.createQuery(sql);
		//qry.setParameter("projectid",projectid);
		qry.setParameter("userid",userid);
	
		qry.executeUpdate();
//		tx.commit();
		status="Success";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	//	return roleid;
		return status;
	}
	
	@Transactional
	public List<ProjectDeatilsMeta> getAllProjectunderuser(int userid) {
		Session session=entityManager.unwrap(Session.class);
		Query qry=null;
		List<Object[]> list=null;
		List<ProjectDeatilsMeta> test=new ArrayList<ProjectDeatilsMeta>();
		
		try
		{
//			Transaction tx=session.beginTransaction();
		String sql = "select t.* from  ProjectAccessTable r right join Project_MetaTable t " + 
				"on r.projectID=t.projectID where r.userDetailsId=:userid";
		qry = session.createNativeQuery(sql);
		qry.setParameter("userid",userid);
		list=qry.list();
			for (Object[] objects : list)
			{	ProjectDeatilsMeta user=new ProjectDeatilsMeta();
				user.setProjectID(Integer.valueOf(objects[0].toString()));
				user.setIsActive(objects[3].toString());
				user.setProjectName(objects[6].toString());
				test.add(user);
			}
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return test;
	}
	public List<com.apmosys.tmt.models.userRole> getAllmanager() {
		List<userRole> test=new ArrayList<userRole>();
		Session session=entityManager.unwrap(Session.class);
		Query qry=null;
		try
		{
		String sql = "select u.userID,u.name,(select u1.name from UserDetailsEntity u1 where u1.userID=u.managerID) as managerName,(select r.roleType from RoleEntity r where r.roleId=u.userrole) as userRole,"
				+"u.isActive,u.username,u.managerID,u.id from  UserDetailsEntity u where u.userrole=101";
		qry = session.createQuery(sql);
		List<Object[]> list = qry.list();
	System.out.println("================"+list);
		for (Object[] objects : list)
		{	userRole user=new userRole();
			user.setUsername(objects[1].toString());
		    user.setUserid(objects[0]!=null ? objects[0].toString() :null);
		    user.setUsername(objects[1].toString());
			user.setUserrole(objects[3].toString());
			user.setIsactive(objects[4].toString());
			user.setPassword(objects[5].toString());
			user.setCreatorid(objects[6].toString());
			user.setId(Integer.valueOf(objects[7].toString()));
			test.add(user);
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return test;
	}
	
	@Transactional
	public String EditUser(com.apmosys.tmt.models.userRole user) {
		Serializable count=0;
		String msg="Success";
		Session session=entityManager.unwrap(Session.class);
		Query qry=null;
		List<Object[]> list=null;
//		Transaction tx=session.beginTransaction();
		try
		{
		if(user.getCreatorid()==null)
		{
			String sql = "update UserDetailsEntity a set a.username=:username,a.name=:name,a.isActive=:isactive,a.userrole=:userrole where a.id=:id";
			qry = session.createQuery(sql);
			qry.setParameter("username",user.getUsername());
			qry.setParameter("userrole",Integer.valueOf(user.getUserrole()));
			qry.setParameter("name",user.getName());
			qry.setParameter("isactive",user.getIsactive());
			qry.setParameter("id",user.getId());
			qry.executeUpdate();
//			tx.commit();
		}
		else
		{
		String sql = "update UserDetailsEntity a set a.username=:username,a.name=:name,a.isActive=:isactive,a.userrole=:userrole,a.managerID=:managerid where a.id=:id";
		qry = session.createQuery(sql);
		qry.setParameter("username",user.getUsername());
		qry.setParameter("name",user.getName());
		qry.setParameter("isactive",user.getIsactive());
		qry.setParameter("userrole",Integer.valueOf(user.getUserrole()));
		qry.setParameter("managerid",Integer.valueOf(user.getCreatorid()));
		qry.setParameter("id",user.getId());
		qry.executeUpdate();
//		tx.commit();
		}
		}
		catch(MethodArgumentTypeMismatchException e)
		{
			msg="Failed to created";
			session.clear();
		}
		catch(ConstraintViolationException e)
		{
			msg="already exist";
			session.clear();
		}
		catch(Exception e)
		{
			msg="Failed to created";
			e.printStackTrace();
		}
		return msg;
	}
	public List<AccessDetail> AdminDetailsAccess() {
		
			List<Object[]> list=null;
			List<AccessDetail> test=new ArrayList<AccessDetail>();
			try
			{
			Session session=entityManager.unwrap(Session.class);
			Query qry=null;
		
			String sql = "select projectID,projectName from Project_MetaTable ";
			qry = session.createNativeQuery(sql);
		
			list=qry.list();
			for (Object[] objects : list)
			{	AccessDetail user=new AccessDetail();
				/*user.setUserid(100);*/
				user.setProjectid(Integer.valueOf(objects[0].toString()));
				user.setProjectName(objects[1].toString());
				test.add(user);
			}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			
			}
			return test;
	}
	
	@Transactional
	public String createbugTrackor(BugTrackorToolEntity dataTool) {
		Object count=0;
		Session session=null;
		String msg="Success";
		try
		{
	    session = sessionFactory.getCurrentSession();
//		Transaction tx=session.beginTransaction();
		count= session.save(dataTool);
//		tx.commit();
		}
		catch(MethodArgumentTypeMismatchException e)
		{
			msg="Failed to created";
			session.clear();
		}
		catch(ConstraintViolationException e)
		{
			msg="already exist";
			session.clear();
		}
		catch(Exception e)
		{
			msg="Failed to created";
			e.printStackTrace();
		}
		System.out.println("-----------------------------------------create user dao"+msg);
		return msg;
	}
	public List<BugTrackorToolEntity> bugTrackorAll() {
		List<BugTrackorToolEntity> roles =null;
		try
		{
			Session session=entityManager.unwrap(Session.class);
			String sql = "from  BugTrackorToolEntity";
			Query query = session.createQuery(sql);
			 roles = query.list();
			
	}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return roles;
	}
	
	public BugTrackorToolEntity bugTrackorbytoolid(int id) {
		Session session=entityManager.unwrap(Session.class);
		Query qry=null;
		String userid=null;
		BugTrackor btr=new BugTrackor();
		List<Object[]> list=null;
		List<BugTrackorToolEntity> roles =null;
		BugTrackorToolEntity user=null;
		try
		{
		String sql = "select u.bugTrackingToolName,u.isactive,u.IP,u.Toolid from BugTrackorToolEntity u where u.Toolid=:id ";
		qry = session.createQuery(sql);
		qry.setParameter("id",id);
		list=qry.list();
		for (Object[] objects : list)
		{ 
			user=new BugTrackorToolEntity();
		user.setBugTrackingToolName(objects[0].toString());
		user.setIsactive(objects[1].toString());
		user.setIP(objects[2].toString());	
		user.setToolid(Integer.valueOf(objects[3].toString()));
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return user;
	}
	
	@Transactional
	public String bugTrackoredit(BugTrackorToolEntity bg) {
		Session session=entityManager.unwrap(Session.class);
		BugTrackor btr=new BugTrackor();
	
		String msg="Success";
		System.out.println("==========bugTrackoredit====="+bg.getUserid());
		try
		{
//			Transaction tx=session.beginTransaction();
		Query qry=null;
		String sql = "update BugTrackorToolEntity a set a.bugTrackingToolName=:name,a.IP=:ip,"
				+ "a.isactive=:isactive,a.Userid=:Userid,a.password=:password, a.userDefinedToolName=:uName"
				+ " where a.toolid=:id";
		qry = session.createQuery(sql);
		qry.setParameter("name",bg.getBugTrackingToolName());
		qry.setParameter("isactive",bg.getIsactive());
		qry.setParameter("ip",bg.getIP());
		qry.setParameter("id",bg.getToolid());
		qry.setParameter("Userid",bg.getUserid());
		qry.setParameter("password",bg.getPassword());
		qry.setParameter("uName",bg.getUserDefinedToolName());
		qry.executeUpdate();
//		tx.commit();
		}
		catch(Exception e)
		{
			msg="Unable to update";
			e.printStackTrace();
			
		}
		return msg;
	}
	public List<BugTrackorToolEntity> bugTrackorAllActive() {
			List<BugTrackorToolEntity> roles =null;
			try
			{
				Session session=entityManager.unwrap(Session.class);
				String sql = "from  BugTrackorToolEntity where isactive='Y'";
				Query query = session.createQuery(sql);
				 roles = query.list();
				
		}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return roles;
		}
	
	@Transactional
	public String removeBugTool(int id) {
		Session session=entityManager.unwrap(Session.class);
		String status="fail";
		Query qry=null;
		List<Object[]> list=null;
//		Transaction tx=session.beginTransaction();
		try
		{
		String sql = "delete from BugTrackorToolEntity u where u.toolid=:userid";
		qry = session.createQuery(sql);
		qry.setParameter("userid",id);
		qry.executeUpdate();
//		tx.commit();
		status="Success";
		}
		catch(Exception e)
		{
			status="Fail";
			e.printStackTrace();
		}
		return status;
	}
	
	
	@Transactional
	public List<DateWiseGraphData> ProjectWiseGraph(int id) {
		Session session=entityManager.unwrap(Session.class);
		String status="fail";
		Query qry=null;
		List<DateWiseGraphData> datewisegraph=new ArrayList<DateWiseGraphData>(); 
		DateWiseGraphData datewise=null;
		List<Object[]> list=null;
		
		try
		{
//			Transaction tx=session.beginTransaction();
		String sql = "select count(IF(status = 'Pass', 1, NULL)) as Total_Pass,\n" + 
				"count(IF(status = 'Fail', 1, NULL)) as Total_Fail,\n" + 
				"count(IF(status = 'Retest', 1, NULL)) as Total_Restest,\n" + 
				"count(IF(status = 'Blocker', 1, NULL)) as Total_Blocker,\n" + 
				"count(IF(status is null, 1, NULL)) as Total_unexecuted,\n" + 
				"date(dateOfExec) " + 
				"from MannualTestCaseExecutionLogs\n" + 
				"where date(dateOfExec) is not null and projectId=:id GROUP BY date(dateOfExec)";
		qry = session.createNativeQuery(sql);
		qry.setParameter("id",id);
		list=qry.list();
		for (Object[] objects : list)
		{ 
			datewise=new DateWiseGraphData();
			datewise.setPass(Integer.valueOf(objects[0].toString()));
			datewise.setFail(Integer.valueOf(objects[1].toString()));
			datewise.setRetest(Integer.valueOf(objects[2].toString()));
			datewise.setBlocker(Integer.valueOf(objects[3].toString()));
			datewise.setTotalunexecuted(Integer.valueOf(objects[4].toString()));
			datewise.setSpecificdate((Date) objects[5]);
			datewisegraph.add(datewise);
		}
		System.out.println(list);
		status="Success";
		}
		catch(Exception e)
		{
			status="Fail";
			e.printStackTrace();
		}
		return datewisegraph;
	}
	@Transactional
	public List<RunWiseGraphData> RunwiseGraph(int id) {
		Session session=entityManager.unwrap(Session.class);
		String status="fail";
		List<RunWiseGraphData> datewisegraph=new ArrayList<RunWiseGraphData>(); 
		RunWiseGraphData runtest=null;
		Query qry=null;
		List<Object[]> list=null;
//		Transaction tx=session.beginTransaction();
		try
		{
		String sql = "select count(IF(status = 'Pass', 1, NULL)) as Total_Pass,\n" + 
				"count(IF(status = 'Fail', 1, NULL)) as Total_Fail,\n" + 
				"count(IF(status is null, 1, NULL)) as Total_unexecuted,runId,(select runName from MannualRunTable where id=runId) as Name\n" + 
				"from MannualTestCaseExecutionLogs where projectId=:id group by runId";
		qry = session.createNativeQuery(sql);
		qry.setParameter("id",id);
		list=qry.list();
		for (Object[] objects : list)
		{ 
			runtest=new RunWiseGraphData();
			runtest.setPass(Integer.valueOf(objects[0].toString()));
			runtest.setFail(Integer.valueOf(objects[1].toString()));
			runtest.setUntested(Integer.valueOf(objects[2].toString()));	
			runtest.setRunid(Integer.valueOf(objects[3].toString()));
			if(objects[4]!=null)
			runtest.setRunname(objects[4].toString());
			datewisegraph.add(runtest);
		}
		System.out.println(list);
		status="Success";
		}
		catch(Exception e)
		{
			status="Fail";
			e.printStackTrace();
		}
		return datewisegraph;
	}
	
	@Transactional
	public List<DateWiseGraphData> OverallGraph() {
		Session session=entityManager.unwrap(Session.class);
		String status="fail";
		Query qry=null;
		List<DateWiseGraphData> datewisegraph=new ArrayList<DateWiseGraphData>(); 
		DateWiseGraphData datewise=null;
		List<Object[]> list=null;
//		Transaction tx=session.beginTransaction();
		try
		{
		String sql = "select count(IF(status = 'Pass', 1, NULL)) as Total_Pass,\n" + 
				"count(IF(status = 'Fail', 1, NULL)) as Total_Fail,\n" + 
				"count(IF(status = 'Retest', 1, NULL)) as Total_Restest,\n" + 
				"count(IF(status = 'Blocker', 1, NULL)) as Total_Blocker,\n" + 
				"count(IF(status is null, 1, NULL)) as Total_unexecuted,\n" + 
				"date(dateOfExec) " + 
				"from MannualTestCaseExecutionLogs\n" + 
				"where date(dateOfExec) is not null GROUP BY date(dateOfExec)";
		qry = session.createNativeQuery(sql);
		
		list=qry.list();
		for (Object[] objects : list)
		{ 
			datewise=new DateWiseGraphData();
			datewise.setPass(Integer.valueOf(objects[0].toString()));
			datewise.setFail(Integer.valueOf(objects[1].toString()));
			datewise.setRetest(Integer.valueOf(objects[2].toString()));
			datewise.setBlocker(Integer.valueOf(objects[3].toString()));
			datewise.setTotalunexecuted(Integer.valueOf(objects[4].toString()));
			datewise.setSpecificdate((Date) objects[5]);
			datewisegraph.add(datewise);
		}
		System.out.println(list);
		status="Success";
		}
		catch(Exception e)
		{
			status="Fail";
			e.printStackTrace();
		}
		return datewisegraph;
	}
	public List<AccessDetail> getAccesDetailsAdmin(int userid) {
		List<Object[]> list=null;
		List<AccessDetail> test=new ArrayList<AccessDetail>();
		try
		{
		Session session=entityManager.unwrap(Session.class);
		Query qry=null;
	
		String sql = "select p.projectID,p.projectName from  Project_MetaTable p";
		qry = session.createNativeQuery(sql);
		list=qry.list();
		for (Object[] objects : list)
		{	AccessDetail user=new AccessDetail();
			user.setUserid(userid);
			user.setProjectid(Integer.valueOf(objects[0].toString()));
			user.setProjectName(objects[1].toString());
			test.add(user);
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		
		}
		return test;
	}
	public String userRolebyid(int userid) {
		RoleEntity role=new RoleEntity();
		List<Object[]> list1=null;
		List<RoleEntity> list=new ArrayList<RoleEntity>();
		String roletype=null;
		System.out.println("============roletype=========="+userid);
		try
		{
		Session session=entityManager.unwrap(Session.class);
		//String sql = "select r.roleId,r.roleType from RoleTable r where r.roleId=(select a.userrole from UserDetailsEntity a where a.id=:userid";
		String sql = "select * from RoleTable where roleId=(select userRole from UserDetailsEntity  where id=:userid)";
		Query query = session.createNativeQuery(sql);
		query.setParameter("userid", userid);
		list1 = query.list();
		 for (Object[] objects : list1)
			{	RoleEntity user=new RoleEntity();
				user.setRoleId(Integer.valueOf(objects[0].toString()));
				user.setRoleType(objects[1].toString());
				roletype=objects[1].toString();
				list.add(user);
			}
		
		System.out.println("roletype=========="+roletype);
		}
		catch(Exception e)
		{
			
			e.printStackTrace();
			return null;
		}
		return roletype;
	}
	
	@Transactional
	public List<DateWiseGraphData> overallGraphidwise(int id) {
		Session session=entityManager.unwrap(Session.class);
		String status="fail";
		Query qry=null;
		List<DateWiseGraphData> datewisegraph=new ArrayList<DateWiseGraphData>(); 
		DateWiseGraphData datewise=null;
		List<Object[]> list=null;
//		Transaction tx=session.beginTransaction();
		try
		{
			String sql="select count(IF(a.status = 'Pass', 1, NULL)) as Total_Pass,\n" + 
					"count(IF(a.status = 'Fail', 1, NULL)) as Total_Fail,\n" + 
					"count(IF(a.status = 'Retest', 1, NULL)) as Total_Restest,\n" + 
					"count(IF(a.status = 'Blocker', 1, NULL)) as Total_Blocker,\n" + 
					"count(IF(a.status is null, 1, NULL)) as Total_unexecuted,\n" + 
					"date(dateOfExec) from MannualTestCaseExecutionLogs a inner join ProjectAccessTable p on p.projectID=a.projectId\n" + 
					"where p.userDetailsId=:userid and date(a.dateOfExec) is not null GROUP BY date(a.dateOfExec)";
	/*	String sql = "select count(IF(status = 'Pass', 1, NULL)) as Total_Pass,\n" + 
				"count(IF(status = 'Fail', 1, NULL)) as Total_Fail,\n" + 
				"count(IF(status = 'Retest', 1, NULL)) as Total_Restest,\n" + 
				"count(IF(status = 'Blocker', 1, NULL)) as Total_Blocker,\n" + 
				"count(IF(status is null, 1, NULL)) as Total_unexecuted,\n" + 
				"date(dateOfExec) " + 
				"from MannualTestCaseExecutionLogs\n" + 
				"where date(dateOfExec) is not null GROUP BY date(dateOfExec)";*/
		qry = session.createNativeQuery(sql);
		qry.setParameter("userid", id);
		list=qry.list();
		for (Object[] objects : list)
		{ 
			datewise=new DateWiseGraphData();
			datewise.setPass(Integer.valueOf(objects[0].toString()));
			datewise.setFail(Integer.valueOf(objects[1].toString()));
			datewise.setRetest(Integer.valueOf(objects[2].toString()));
			datewise.setBlocker(Integer.valueOf(objects[3].toString()));
			datewise.setTotalunexecuted(Integer.valueOf(objects[4].toString()));
			datewise.setSpecificdate((Date) objects[5]);
			datewisegraph.add(datewise);
		}
		System.out.println(list);
		status="Success";
		}
		catch(Exception e)
		{
			status="Fail";
			e.printStackTrace();
		}
		return datewisegraph;
	}
	@Transactional
	public List<DateWiseRunData> datewiseexecutionoforallrun(int id) {
		Session session=entityManager.unwrap(Session.class);
		String status="fail";
		Query qry=null;
		List<DateWiseRunData> datewisegraph=new ArrayList<DateWiseRunData>(); 
		DateWiseRunData datewise=null;
		List<Object[]> list=null;
//		Transaction tx=session.beginTransaction();
		try
		{
			String sql="select count(IF(mt.testCaseAction = 'On Hold', 1, NULL))+count(IF(mt.testCaseAction = 'Completed', 1, NULL))\n" + 
					"+count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as totalExecuted,mt.dateOfExec,mt.runId \n" + 
					  "(select runName from MannualRunTable where id=runId) as Name\n "+
					"from MannualTestCaseExecutionLogs mt where mt.runId=21 and mt.projectId=7 and\n" + 
					"date(dateOfExec) between CAST('2019-07-06' AS DATE) and CAST('2019-08-05' AS DATE)\n" + 
					"group by mt.runId,date(mt.dateOfExec)";
	
		qry = session.createNativeQuery(sql);
		qry.setParameter("userid", id);
		list=qry.list();
		for (Object[] objects : list)
		{ 
			datewise=new DateWiseRunData();
			datewise.setDateofexecution((Date) objects[1]);
			datewise.setTotalexecution(Integer.valueOf(objects[0].toString()));
			datewise.setRunname((objects[2].toString()));
			datewise.setProjectName(objects[3].toString());
			datewise.setRunid(Integer.valueOf(objects[4].toString()));
			datewisegraph.add(datewise);
		}
		System.out.println(list);
		status="Success";
		}
		catch(Exception e)
		{
			status="Fail";
			e.printStackTrace();
		}
		return datewisegraph;
	}
	public List<userRole> getUserByProjectId(Integer pid) {
		List<Object[]> list=null;
		List<userRole> test=new ArrayList<userRole>();
		Session session=entityManager.unwrap(Session.class);
		Query qry=null;
		try
		{
			String sql ="SELECT id,name FROM UserDetailsEntity where userRole='100'\n" + 
					"union\n" + 
					"SELECT up.id,up.name FROM Project_MetaTable pm\n" + 
					"inner JOIN ProjectAccessTable p ON pm.projectID=p.projectID \n" + 
					"right JOIN UserDetailsEntity up ON up.id=p.userDetailsId WHERE \n" + 
					"p.projectID=:pId and p.AccessType!='Readonly' GROUP BY p.userDetailsId"; 	
					/*
							 * "SELECT p.userDetailsId,u.name FROM Project_MetaTable pm inner JOIN ProjectAccessTable p ON pm.projectID=p.projectID inner JOIN UserDetailsEntity u ON u.id=p.userDetailsId WHERE p.projectID=:pId and p.AccessType!='Readonly' GROUP BY p.userDetailsId"
							 * ;
							 */
		qry = session.createNativeQuery(sql);
		qry.setParameter("pId", pid);
		 list = qry.list();
		for (Object[] objects : list)
		{	userRole user=new userRole();
			user.setName((String)objects[1]);
		    user.setId((Integer)objects[0]);
			test.add(user);
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return test;
		
	}
	/*
	 * public Map<String, ProjectEntitlement> getProjectDetailsFormanager() {
	 * List<Object[]> list =null; Map<String,ProjectEntitlement> map=new
	 * HashedMap<String, ProjectEntitlement>(); List<ProjectEntitlement> test=new
	 * ArrayList<ProjectEntitlement>(); Session session =
	 * sessionFactory.getCurrentSession(); Query qry=null; try { String sql
	 * ="select uf.id,uf.name,uf.userRole,uf.managerID,pm.projectName,ps.projectID,ps.AccessType,ps.srNo from UserDetailsEntity uf inner join ProjectAccessTable ps\n"
	 * +
	 * "on uf.id=ps.userDetailsId inner join Project_MetaTable pm on pm.projectID=ps.projectID order by ps.projectID"
	 * ; qry = session.createNativeQuery(sql); list=qry.list(); for (Object[] objects :
	 * list) {
	 * 
	 * ProjectEntitlement test1=new ProjectEntitlement();
	 * test1.setUserid(objects[0].toString());
	 * test1.setUsername(objects[1].toString());
	 * test1.setUserrole(objects[2].toString());
	 * test1.setProjectManager(objects[3]!=null?objects[3].toString():null);
	 * test1.setProjectName(objects[4]!=null?objects[4].toString():null);
	 * test1.setProjectId(objects[5]!=null?objects[5].toString():null);
	 * test1.setAccessProject(objects[6].toString());
	 * test1.setProjectaccessid(objects[7] !=null ? objects[7].toString() :null);
	 * if(test1.getUserrole().equals("100")) {
	 * 
	 * } else { test.add(test1); } } for (ProjectEntitlement details1 : test) { if
	 * (details1 != null) { ProjectEntitlement groupAndCase = new
	 * ProjectEntitlement(); List<ProjectEntitlement> values = test.stream()
	 * .filter(x -> x.getProjectName().equals(details1.getProjectName()))
	 * .collect(Collectors.toList());
	 * groupAndCase.setProjectId(details1.getProjectId()); if
	 * (groupAndCase.getProjectId() != null) { groupAndCase.setUserlist(values); }
	 * map.put(details1.getProjectName(), groupAndCase); }
	 * 
	 * } } catch(Exception e) { e.printStackTrace(); } return map;
	 * 
	 * }
	 */

	public Map<String, ProjectEntitlement> getProjectDetailsFormanager(String userId,String username) {
		List<Object[]> list = null;
		Map<String, ProjectEntitlement> map = new HashedMap<String, ProjectEntitlement>();
		List<ProjectEntitlement> test = new ArrayList<ProjectEntitlement>();
		Session session=entityManager.unwrap(Session.class);
		Query qry = null;
		try {
			String sql ="select uf.id,uf.name,uf.userRole,uf.managerID,pm.projectName,ps.projectID,ps.AccessType,ps.srNo from Project_MetaTable pm \n" + 
					"				LEFT JOIN ProjectAccessTable ps ON pm.projectID=ps.projectID and ps.Access_Given_By=:username JOIN UserDetailsEntity uf \n" + 
					"					ON uf.id=ps.userDetailsId order by pm.projectName asc";
					/*
							 * "select uf.id,uf.name,uf.userRole,uf.managerID,pm.projectName,ps.projectID,ps.AccessType,ps.srNo from UserDetailsEntity uf inner join ProjectAccessTable ps\n"
							 * +
							 * "on uf.id=ps.userDetailsId inner join Project_MetaTable pm on pm.projectID=ps.projectID   WHERE uf.id='"
							 * + userId + "' and ps.AccessType = 'Manager'  order by pm.projectName asc";
							 */
			qry = session.createNativeQuery(sql);
			qry.setParameter("username", username);

			list = qry.list();
			for (Object[] objects : list) {

				ProjectEntitlement test1 = new ProjectEntitlement();
				test1.setUserid(objects[0].toString());
				test1.setUsername(objects[1].toString());
				test1.setUserrole(objects[2].toString());
				test1.setProjectManager(objects[3] != null ? objects[3].toString() : null);
				test1.setProjectName(objects[4] != null ? objects[4].toString() : null);
				test1.setProjectId(objects[5] != null ? objects[5].toString() : null);
				test1.setAccessProject(objects[6].toString());

				test1.setProjectaccessid(objects[7].toString());

				test1.setProjectaccessid(objects[7] != null ? objects[7].toString() : null);

				if (test1.getUserrole().equals("100")) {

				} else {
					test.add(test1);
				}
			}
			for (ProjectEntitlement details1 : test) {
				if (details1 != null) {
					ProjectEntitlement groupAndCase = new ProjectEntitlement();
					List<ProjectEntitlement> values = test.stream()
							.filter(x -> x.getProjectName().equals(details1.getProjectName()))
							.collect(Collectors.toList());
					groupAndCase.setProjectId(details1.getProjectId());
					if (groupAndCase.getProjectId() != null) {
						groupAndCase.setUserlist(values);
					}
					map.put(details1.getProjectName(), groupAndCase);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;

	}

	/*
	 * public Map<String, ProjectEntitlement> getProjectDetailsForAdmin() {
	 * List<Object[]> list =null; Map<String,ProjectEntitlement> map=new
	 * HashedMap<String, ProjectEntitlement>(); List<ProjectEntitlement> test=new
	 * ArrayList<ProjectEntitlement>(); Session session =
	 * sessionFactory.getCurrentSession(); Query qry=null; try { String sql
	 * ="select uf.id,uf.name,uf.userRole,uf.managerID,pm.projectName,ps.projectID,ps.AccessType,ps.srNo from UserDetailsEntity uf inner join ProjectAccessTable ps\n"
	 * +
	 * "on uf.id=ps.userDetailsId inner join Project_MetaTable pm on pm.projectID=ps.projectID order by ps.projectID"
	 * ; qry = session.createNativeQuery(sql); list=qry.list(); for (Object[] objects :
	 * list) { ProjectEntitlement test1=new ProjectEntitlement();
	 * test1.setUserid(objects[0].toString());
	 * test1.setUsername(objects[1].toString());
	 * test1.setUserrole(objects[2].toString());
	 * test1.setProjectManager(objects[3]!=null?objects[3].toString():null);
	 * test1.setProjectName(objects[4]!=null?objects[4].toString():null);
	 * test1.setProjectId(objects[5]!=null?objects[5].toString():null);
	 * test1.setAccessProject(objects[6].toString());
	 * test1.setProjectaccessid(Integer.valueOf(objects[7].toString()));
	 * test.add(test1); } for (ProjectEntitlement details1 : test) { if (details1 !=
	 * null) { ProjectEntitlement groupAndCase = new ProjectEntitlement();
	 * List<ProjectEntitlement> values = test.stream() .filter(x ->
	 * x.getProjectName().equals(details1.getProjectName()))
	 * .collect(Collectors.toList());
	 * groupAndCase.setProjectId(details1.getProjectId()); if
	 * (groupAndCase.getProjectId() != null) { groupAndCase.setUserlist(values); }
	 * map.put(details1.getProjectName(), groupAndCase); }
	 * 
	 * } } catch(Exception e) { e.printStackTrace(); } return map;
	 * 
	 * }
	 */
	public Map<String, ProjectEntitlement> getProjectDetailsForAdmin() {
		List<Object[]> list = null;
		Map<String, ProjectEntitlement> map = new HashedMap<String, ProjectEntitlement>();
		List<ProjectEntitlement> test = new ArrayList<ProjectEntitlement>();
		Session session=entityManager.unwrap(Session.class);
		Query qry = null;
		try {
			/*
			 * String sql
			 * ="select uf.id,uf.name,uf.userRole,uf.managerID,pm.projectName,ps.projectID,ps.AccessType,ps.srNo from UserDetailsEntity uf inner join ProjectAccessTable ps\n"
			 * +
			 * "on uf.id=ps.userDetailsId inner join Project_MetaTable pm on pm.projectID=ps.projectID order by ps.projectID"
			 * ;
			 */
			String sql = "select uf.id,uf.name,uf.userRole,uf.managerID,pm.projectName,ps.projectID,ps.AccessType,ps.srNo from Project_MetaTable pm \n"
					+ "LEFT JOIN ProjectAccessTable ps ON pm.projectID=ps.projectID LEFT JOIN UserDetailsEntity uf \n"
					+ "on uf.id=ps.userDetailsId  order by pm.projectName asc";
			qry = session.createNativeQuery(sql);
			list = qry.list();
			for (Object[] objects : list) {
				ProjectEntitlement test1 = new ProjectEntitlement();
				test1.setUserid(objects[0] != null ? objects[0].toString() : "");
				test1.setUsername(objects[1] != null ? objects[1].toString() : "");
				test1.setUserrole(objects[2] != null ? objects[2].toString() : "");
				test1.setProjectManager(objects[3] != null ? objects[3].toString() : null);
				test1.setProjectName(objects[4] != null ? objects[4].toString() : null);
				test1.setProjectId(objects[5] != null ? objects[5].toString() : null);
				test1.setAccessProject(objects[6] != null ? objects[6].toString() : "");
				test1.setProjectaccessid(objects[7] !=null ?  objects[7].toString() :null );
				if (test1.getUserid() != "") {
					test.add(test1);
				}

			}
			for (ProjectEntitlement details1 : test) {
				if (details1 != null) {
					ProjectEntitlement groupAndCase = new ProjectEntitlement();
					List<ProjectEntitlement> values = test.stream()
							.filter(x -> x.getProjectName().equals(details1.getProjectName()))
							.collect(Collectors.toList());
					groupAndCase.setProjectId(details1.getProjectId());
					if (groupAndCase.getProjectId() != null) {
						groupAndCase.setUserlist(values);
					}
					map.put(details1.getProjectName(), groupAndCase);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;

	}

	
	@Transactional
	public String giveProjectAccessCreate(int pId, int uId,String createBy) {
		Object count=0;
//		 Session session= sessionFactory.getCurrentSession();
		Session session=entityManager.unwrap(Session.class);
		String check="success";
		try
		{
		Query qry=null;
			/*
			 * System.out.println(uId); System.out.println(pId);
			 */
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String date=sdf.format(new Date());
		
				ProjectAccessEntity accessEntity=new ProjectAccessEntity();
				accessEntity.setUserID(uId);
			    accessEntity.setProjectID(pId);
			    accessEntity.setAccesstype("Manager");
			    accessEntity.setAccessDate(date);
			    accessEntity.setAccessGivenBy(createBy);
//			    Transaction tx=session.beginTransaction();
			    
				count= session.save(accessEntity);
//				tx.commit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			check="issue";
			session.clear();
		}
		System.out.println(check);
		return check;

		
	}
	public String getRoletypebyproject(Integer projectid,int id) {
		ProjectAccessEntity role=new ProjectAccessEntity();
		List<ProjectAccessEntity> list=null;
		String roletype=null;
		try
		{
		Session session=entityManager.unwrap(Session.class);
		String sql = "from  ProjectAccessEntity where projectID=:projectid and userDetailsId=:id";
		Query query = session.createQuery(sql);
		query.setParameter("projectid", projectid);
		query.setParameter("id", id);
		list =query.list();
		for (ProjectAccessEntity p : list)
		{ 
			roletype=p.getAccesstype();
	
		}
		}
		catch(Exception e)
		{
			
			e.printStackTrace();
			return null;
		}
		return roletype;
	}
	public List<UserDetailsEntity> findByCreator(String userCreatedBy) {
	    
        List<UserDetailsEntity> list=new ArrayList<UserDetailsEntity>();
        try
        {
        Session session=entityManager.unwrap(Session.class);
        String sql = "from UserDetailsEntity where username=:userCreatedBy ";
        Query query = session.createQuery(sql);
        query.setParameter("userCreatedBy", userCreatedBy);
        list =query.list();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }
	public String getMaxid() {
		List<Object> list=null;
		String userid="";
	        try
	        {
	        Session session=entityManager.unwrap(Session.class);
	        String sql = "select max(userID) from UserDetailsEntity";
	        Query query = session.createQuery(sql);
	        list =query.list();
	        for (int i = 0; i < list.size(); i++)
			{
	        	userid = (String) list.get(i);

			}
	        }
	        catch(Exception e)
	        {
	            e.printStackTrace();
	        }
	        return userid;
	}
	@Transactional
	public String deleteGiveAccessUserEntitlement(Integer projectid, Integer userid, String projectwiserole) {

		Session session=entityManager.unwrap(Session.class);
		String status="fail";
		Query qry=null;
		List<Object[]> list=null;
//		Transaction tx=session.beginTransaction();
		try
		{
		String sql = "delete from ProjectAccessTable  where AccessType=:projectwiserole and projectID=:projectid and userDetailsId=:userid";
		qry = session.createNativeQuery(sql);
		qry.setParameter("projectwiserole",projectwiserole);
		qry.setParameter("projectid",projectid);
		qry.setParameter("userid",userid);
		qry.executeUpdate();
//		tx.commit();
		status="Success";
		}
		catch(Exception e)
		{
			status="Fail";
			e.printStackTrace();
		}
		return status;
	
	}
	
	@Transactional
	public ProjectAccessEntity giveAccessUserEntitlement(ProjectAccessEntity pae) {
		Serializable count=0;
		ProjectAccessEntity pcs=new ProjectAccessEntity();
		Session session=null;
		try
		{
//	    session = sessionFactory.getCurrentSession();
	     session=entityManager.unwrap(Session.class);
//		Transaction tx=session.beginTransaction();
		session.update(pae);
		pcs=pae;
//		tx.commit();
		}
		catch(MethodArgumentTypeMismatchException e)
		{
			session.clear();
		}
		catch(ConstraintViolationException e)
		{
			session.clear();
		}
		catch(Exception e)
		{
		
			e.printStackTrace();
		}
		return pcs;
	}
	
}
