package com.apmosys.tmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apmosys.tmt.models.ProjectDeatilsMeta;

public interface RoleRepository  extends JpaRepository<ProjectDeatilsMeta, Integer>{

}
