package com.apmosys.tmt.repository;

import java.util.List;

//import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apmosys.tmt.models.MannualRunTable;

import jakarta.transaction.Transactional;

public interface MannualRunRepository extends JpaRepository<MannualRunTable, Integer> {

	  @Transactional	  
	  @Modifying	 
	@Query("DELETE FROM MannualRunTable WHERE id=:id") 
	public void deleteRun(@Param("id") Integer id);
	 
	  public List<MannualRunTable> findByRunName(String runnames);

//	public MannualRunTable findOne(Integer id);
}
