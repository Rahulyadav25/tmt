package com.apmosys.tmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.AutomationRunLogs;
import com.apmosys.tmt.models.AutomationRunTable;

public interface AutomationRunRepository extends JpaRepository<AutomationRunTable, Integer> {

	//List<AutomationRunTable> findByProject_name(String oldprojectname);

	@Query("FROM AutomationRunTable m where m.project_name = :project_name")
	public List<AutomationRunTable> findByProject_name(@Param("project_name") String project_name);
	
	@Transactional
	@Modifying
	@Query("UPDATE AutomationRunTable l SET l.project_name = :projectname where l.project_name=:oldprojectname")
	public void updateautoruntable(@Param("projectname") String projectname,@Param("oldprojectname") String oldprojectname);
	
}
