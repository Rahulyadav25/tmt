package com.apmosys.tmt.repository;

import java.util.List;
import java.util.Optional;

//import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apmosys.tmt.models.MannualTestCaseExecutionLogs;

import jakarta.transaction.Transactional;

public interface MannualExecutionLogsRepo extends JpaRepository<MannualTestCaseExecutionLogs, Integer> {

	@Transactional
	@Modifying
	@Query("DELETE FROM MannualTestCaseExecutionLogs WHERE reRunId=:reRunId")
	public void deleteActiveTestCasesForRun(@Param("reRunId") Integer reRunId);

	@Transactional
	@Modifying
	@Query("UPDATE MannualTestCaseExecutionLogs l SET l.testerId = :testerId where l.id=:id")
	public Integer updateAssiginingTo(@Param("testerId") String testerId, @Param("id") Integer id);
	/*
	 * @Query("SELECT IF(mr.status IS NULL,'Untested',mr.status) , COUNT(*) FROM MannualTestCaseExecutionLogs mr WHERE mr.runId=:runId GROUP BY mr.status"
	 * ) List<Object[]> getOverallRunStatus(@Param("runId") Integer runId);
	 */

	public Optional<MannualTestCaseExecutionLogs> findById(Integer testCaseID);

	public List<MannualTestCaseExecutionLogs> findByTesterId(String id);
	
	@Query(value="SELECT ml.* FROM MannualTestCaseExecutionLogs ml join\r\n" + 
			"(SELECT MAX(m.updatedTimeStamp) AS tm, m.reRunId AS rrID FROM MannualTestCaseExecutionLogs m\r\n" + 
			" WHERE m.absoluteSrID=:srID and (SELECT mr.runId FROM MannualReRunTable mr WHERE mr.id=m.reRunId)\r\n" + 
			"  IS NOT NULL) as random WHERE ml.absoluteSrID=:srID AND ml.updatedTimeStamp=random.tm;",nativeQuery=true)
	public MannualTestCaseExecutionLogs getTCLatestStatusFromTimestamp(@Param("srID") Integer srID);

	public List<MannualTestCaseExecutionLogs> findByReRunId(Integer reRunId);
	
	@Query(value="SELECT bugId FROM MannualTestCaseExecutionLogs where reRunId=:reRunId and bugId IS NOT NULL")
	public List<String> getBugsByRunId(@Param("reRunId") Integer reRunId);

//	public MannualTestCaseExecutionLogs findOne(int parseInt);
}
