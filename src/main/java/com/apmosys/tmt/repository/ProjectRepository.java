package com.apmosys.tmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.ProjectDeatilsMeta;
import com.apmosys.tmt.models.User;

public interface ProjectRepository extends JpaRepository<ProjectDeatilsMeta, Integer>{
	@Query("From ProjectDeatilsMeta")
	public List<ProjectDeatilsMeta> getAllProject();
	public ProjectDeatilsMeta findByprojectID(int projectID);
	public ProjectDeatilsMeta findByprojectName(String projectName);
	public ProjectDeatilsMeta findByProjectID(Integer projectID);
	public List<ProjectDeatilsMeta> findByCreatedBy(String name);
	
	@Transactional
	@Modifying
	@Query("UPDATE ProjectDeatilsMeta l SET l.isMannual = :falsestae  where l.projectID=:projectid")
	public void updateAutodata(@Param("falsestae") String falsestae,@Param("projectid") Integer projectid);
	
	@Transactional
	@Modifying
	@Query("UPDATE ProjectDeatilsMeta l SET l.isAutomation = :falsestae  where l.projectID=:projectid")
	public void updateAutodataone(@Param("falsestae") String falsestae,@Param("projectid") Integer projectid);
	//public ProjectDeatilsMeta findByprojectID(String projectId);
	
	@Query(value="SELECT meta.projectID, tool.BuGTrackingToolName FROM Project_MetaTable meta\r\n"
			+ "INNER JOIN BugIntegrationTool tool ON tool.Toolid = meta.isJiraEnable\r\n"
			+ "where projectID = :projectid",nativeQuery = true)
	public List<Object[]> getProjectToolDetails(@Param("projectid")int projectid);
	
	
	//public ProjectDeatilsMeta updateAutodata(String string);
	
}


