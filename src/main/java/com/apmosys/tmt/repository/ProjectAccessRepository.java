package com.apmosys.tmt.repository;

//import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apmosys.tmt.models.ProjectAccessEntity;

import jakarta.transaction.Transactional;

public interface ProjectAccessRepository extends JpaRepository<ProjectAccessEntity, Integer>{

	@Transactional
	@Modifying
	@Query("delete from ProjectAccessEntity where projectID=:projectid")
	public Integer deleteByProjectID(@Param("projectid") Integer projectid);
	
}

