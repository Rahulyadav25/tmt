package com.apmosys.tmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apmosys.tmt.models.MannualTestGroup;
import com.apmosys.tmt.models.MannualTestSuites;

public interface MannualTestSuitesRepo extends JpaRepository<MannualTestSuites, Integer> {
	@Query("FROM MannualTestSuites m where m.projectId = :projectId") 
	public List<MannualTestSuites> findTestSuitesByPID(@Param("projectId") Integer projectId);
	
	@Query("FROM MannualTestSuites m where  m.testSuitesName = :testSuitesName and m.projectId =:projectId and m.isDeleted='N'") 
	public MannualTestSuites findTestSuiteBySuiteName(@Param("testSuitesName") String testSuitesName,@Param("projectId") Integer projectId);

	public MannualTestSuites findByTestSuitesNameAndProjectIdAndIsDeleted(String testSuitesName, Integer projectId,String isdeletd);

	public List<MannualTestSuites> findByTestSuitesNameAndIsDeleted(String suitesname,String isdeleted);

//	public MannualTestSuites findOne(Integer testSuitesId);


}
