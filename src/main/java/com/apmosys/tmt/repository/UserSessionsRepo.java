package com.apmosys.tmt.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.UserSessionDetails;
public interface UserSessionsRepo  extends JpaRepository<UserSessionDetails, String>{

	@Modifying
	@Transactional
	@Query("update UserSessionDetails set session_key=:session_key, active_status=:active_status, last_check=:last_check where userId=:userId ")
	public Integer updateSession(@Param(value = "userId") String userId,@Param(value = "session_key") String session_key,@Param(value = "active_status") String active_status,@Param(value = "last_check") Date last_check); 
}
