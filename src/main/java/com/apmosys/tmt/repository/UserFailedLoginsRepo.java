package com.apmosys.tmt.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.UserFailedLogins;

@Repository
public interface UserFailedLoginsRepo  extends JpaRepository<UserFailedLogins, String>{

	@Modifying
	@Transactional
	@Query("update UserFailedLogins set failedAttemptCount=:failedAttemptCount where userId=:userId ")
	public Integer updateFailedLoginCount(@Param(value = "userId") String userId,@Param(value = "failedAttemptCount") Integer failedAttemptCount); 
}
