package com.apmosys.tmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apmosys.tmt.models.EndUserSystemDetails;

public interface EndUserSystemDetailsRepository extends JpaRepository<EndUserSystemDetails, Integer> {
	
	@Query(value="SELECT id FROM EndUserSystemDetails WHERE name=:name", nativeQuery=true)
	public Integer findEUSDByName(@Param("name") String compList);

	//public EndUserSystemDetails findByName(String systemName);
}
