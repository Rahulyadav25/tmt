package com.apmosys.tmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.UpdateLogs;

public interface UpdateLogsRepository extends JpaRepository<UpdateLogs, Integer>{
	public List<UpdateLogs> findByProjectId(Integer projectID);

	public UpdateLogs findByProjectIdAndEndusersystemId(Integer projectID, Integer endUserSystemID);
	
	@Transactional
	@Modifying
	@Query("delete from UpdateLogs where projectId=:projectid")
	public Integer deleteByAutoUpdateLogProjectID(@Param("projectid") Integer projectid);
}
