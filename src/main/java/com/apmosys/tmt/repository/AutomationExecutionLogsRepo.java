package com.apmosys.tmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Modifying;

import com.apmosys.tmt.models.AutomationTestCaseExecutionLogs;

public interface AutomationExecutionLogsRepo extends JpaRepository<AutomationTestCaseExecutionLogs, Integer> {

//	@Query(value="SELECT * FROM autoBuglogs WHERE bug_id=:bug_id ;",nativeQuery=true)
//	public AutomationTestCaseExecutionLogs findByBugId(@Param("bug_id") String bug_id);
//	//public AutomationTestCaseExecutionLogs findBy(String bugId);
//	@Query("SELECT u FROM autoBuglogs u WHERE u.bug_id=:bug_id")
//	public AutomationTestCaseExecutionLogs getBugDetails(@Param("bug_id") String bug_id);
	
//	@Query("SELECT * FROM autoBuglogs u WHERE u.bug_id = ?1 ")
//	public AutomationTestCaseExecutionLogs getBugDetails(String bug_id);
	
//	@Query(value = "SELECT * FROM autoBuglogs u WHERE u.bug_id = :bugId ", 
//			  nativeQuery = true)
//	AutomationTestCaseExecutionLogs getBugDetails(@Param("bugId") String bugId);
	
	@Query(value="SELECT * FROM autoBuglogs where bug_id=:bug_id ", 
			  nativeQuery = true)
	public AutomationTestCaseExecutionLogs getBugDetails(@Param("bug_id") String bug_id);

//	public AutomationTestCaseExecutionLogs findByBugId(String bugId);

	
}
