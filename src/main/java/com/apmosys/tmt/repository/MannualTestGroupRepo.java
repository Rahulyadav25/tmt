package com.apmosys.tmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.apmosys.tmt.models.MannualTestCaseTable;
import com.apmosys.tmt.models.MannualTestGroup;
@Repository
@Transactional
public interface MannualTestGroupRepo extends JpaRepository<MannualTestGroup, Integer>  {
	  @Query("FROM MannualTestGroup m where m.testSuitesId = :testSuitesId") 
	  public List<MannualTestGroup> findTestGroupByProjectId(@Param("testSuitesId") Integer testGroupId);
	  @Query("FROM MannualTestGroup m where m.testSuitesId = :testSuitesId and m.groupName = :groupName") 
	  public MannualTestGroup findTestGroupBySuitesAndGroupName(@Param("testSuitesId") Integer testSuitesId,@Param("groupName") String groupName);
	public MannualTestGroup findByGroupName(String groupName);
	public MannualTestGroup findBytestSuitesIdAndGroupName(Integer testSuitesId, String groupName);
//	public MannualTestGroup findOne(Integer testGroupId);

}
