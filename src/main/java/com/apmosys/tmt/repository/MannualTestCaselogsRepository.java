package com.apmosys.tmt.repository;
import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.apmosys.tmt.models.MannualTestCaseExecutionLogs;
import com.apmosys.tmt.models.MannualReRunTable;
@Repository
@Transactional
public interface MannualTestCaselogsRepository extends JpaRepository<MannualTestCaseExecutionLogs,Integer> {
//	@Transactional
//	@Modifying 
//	@Query(value ="SELECT count(IF(mt.testCaseAction != '', 1, NULL)) as totalcases,mt.reRunId,\n" + 
//			"mr.runName,count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,\n" + 
//			"count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',\n" + 
//			"count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',\n" + 
//			"count(IF(mt.testCaseAction = 'On Hold', 1, NULL))+count(IF(mt.testCaseAction = 'Completed', 1, NULL))\n" + 
//			"+count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as totalExecuted,\n" + 
//			"count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started',\n" + 
//			"sum(mt.executionTime) as Total_secs,\n" + 
//			"count(*)-count(IF(mt.testCaseAction = 'On Hold', 1, NULL))\n" + 
//			"-count(IF(mt.testCaseAction = 'Under Review', 1, NULL))\n" + 
//			"-count(IF(mt.testCaseAction = 'Completed', 1, NULL))\n" + 
//			"-count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as NE,count(IF(mt.testCaseAction = 'incomplete', 1, NULL)) as Incomplete,"
//			+ "mr.round ,mr.id\n" + 
//			"FROM MannualReRunTable mr\n" + 
//			"LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mr.id\n" + 
//			"WHERE mr.projectId=:projectid AND mr.isActive='Y' AND mr.STATUS='Running'\n" + 
//			" and mr.createdDate between :startdate and :enddate\n" + 
//			"group BY mr.id desc ",nativeQuery=true)
	
	@Transactional
	@Modifying 
	@Query(value ="SELECT count(IF(mt.testCaseAction != '', 1, NULL)) as totalcases,mt.reRunId,\n" + 
			"mr.runName,count(IF(mt.testCaseAction = 'Completed', 1, NULL)) as Completed,\n" + 
			"count(IF(mt.testCaseAction = 'On Hold', 1, NULL)) as 'On Hold',\n" + 
			"count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as 'Under Review',\n" + 
			"count(IF(mt.testCaseAction = 'On Hold', 1, NULL))+count(IF(mt.testCaseAction = 'Completed', 1, NULL))\n" + 
			"+count(IF(mt.testCaseAction = 'Under Review', 1, NULL)) as totalExecuted,\n" + 
			"count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as 'Not Started',\n" + 
			"sum(mt.executionTime) as Total_secs,\n" + 
			"count(*)-count(IF(mt.testCaseAction = 'On Hold', 1, NULL))\n" + 
			"-count(IF(mt.testCaseAction = 'Under Review', 1, NULL))\n" + 
			"-count(IF(mt.testCaseAction = 'Completed', 1, NULL))\n" + 
			"-count(IF(mt.testCaseAction = 'Not Started', 1, NULL)) as NE,count(IF(mt.testCaseAction = 'incomplete', 1, NULL)) as Incomplete,"
			+ "mr.round ,mr.id\n" + 
			"FROM MannualReRunTable mr\n" + 
			"LEFT JOIN MannualTestCaseExecutionLogs mt ON mt.reRunId=mr.id\n" + 
			"WHERE mr.projectId=:projectid AND mr.isActive='Y' AND mr.STATUS='Running'\n" + 
			"and mr.createdDate between :startdate and :enddate\n" + 
			"group BY mr.id",nativeQuery=true)
	List<Object[]> datewiserundetails(@Param("startdate")Date startdate,@Param("enddate") Date enddate,@Param("projectid") int projectid);
	
	/*
	 * select count(IF(mt.status = 'Fail', 1, NULL)) as totalFail,\r\n" +
	 * "count(IF(mt.bugSeverity = 'major', 1, NULL)) as Major,\r\n" +
	 * "count(IF(mt.bugSeverity = 'minor', 1, NULL)) as Minor,\r\n" +
	 * "count(IF(mt.bugSeverity = 'critical', 1, NULL)) as Critical,\r\n" +
	 * "count(IF(mt.bugSeverity = 'blocker', 1, NULL)) as blocker,mr.id,\r\n" +
	 * "mr.runName,mr.round," + "count(IF(mt.bugstatus = 'Open', 1, NULL)) as Open,"
	 * + "count(IF(mt.bugstatus = 'Closed', 1, NULL)) as Closed," +
	 * "count(IF(mt.bugstatus = 'Reopen', 1, NULL)) as Reopen\r\n" +
	 * " FROM MannualReRunTable mr LEFT JOIN \r\n" +
	 * "MannualTestCaseExecutionLogs mt on mr.id=mt.reRunId  where mr.projectId=:projectid and mr.isActive='Y' and mr.STATUS='Running'  and mr.createdDate "
	 * + "between :startdate and :enddate group by mt.reRunId desc \r\n" + "\r\n" +
	 * "
	 */	
//	@Transactional
//	@Modifying 
//	@Query(value ="select count(IF(mt.status = 'Fail', 1, NULL)) as totalFail,\n" + 
//			"	count(IF((mt.bugSeverity = 'critical') AND (mt.bugstatus = 'Open'), 1, NULL)) as CriticalOpen,\n" + 
//			"	count(IF((mt.bugSeverity = 'critical') AND (mt.bugstatus = 'Closed'), 1, NULL)) as CriticalClosed,\n" + 
//			"	count(IF((mt.bugSeverity = 'critical') AND (mt.bugstatus = 'Reopen'), 1, NULL)) as CriticalReopen,\n" + 
//			"	count(IF((mt.bugSeverity = 'critical') AND (mt.bugstatus = 'Resolved'), 1, NULL)) as CriticalResolved,\n" + 
//			"	count(IF(mt.bugSeverity = 'blocker' and mt.bugstatus = 'Open', 1, NULL)) AS BlockerOpen,\n" + 
//			"	count(IF((mt.bugSeverity = 'blocker') AND (mt.bugstatus = 'Closed'), 1, NULL)) as BlockerClosed,\n" + 
//			"	count(IF((mt.bugSeverity = 'blocker') AND (mt.bugstatus = 'Reopen'), 1, NULL)) as BlockerReopen,\n" + 
//			"	count(IF((mt.bugSeverity = 'blocker') AND (mt.bugstatus = 'Resolved'), 1, NULL)) as BlockerResolved,\n" + 
//			"	count(IF(mt.bugSeverity = 'major' and mt.bugstatus = 'Open', 1, NULL)) as MajorOpen,\n" + 
//			"	count(IF((mt.bugSeverity = 'major') AND (mt.bugstatus = 'Closed'), 1, NULL)) as MajorClosed,\n" + 
//			"	count(IF((mt.bugSeverity = 'major') AND (mt.bugstatus = 'Reopen'), 1, NULL)) as MajorReopen,\n" + 
//			"	count(IF((mt.bugSeverity = 'major') AND (mt.bugstatus = 'Resolved'), 1, NULL)) as MajorResolved,\n" + 
//			"	count(IF((mt.bugSeverity = 'minor') and (mt.bugstatus = 'Open'), 1, NULL)) as MinorOpen,\n" + 
//			"	count(IF((mt.bugSeverity = 'minor') AND (mt.bugstatus = 'Closed'), 1, NULL)) as MinorClosed,\n" + 
//			"	count(IF((mt.bugSeverity = 'minor') AND (mt.bugstatus = 'Reopen'), 1, NULL)) as MinorReopen,\n" + 
//			"	count(IF((mt.bugSeverity = 'minor') AND (mt.bugstatus = 'Resolved'), 1, NULL)) as MinorResolved,\n" + 
//			"	mr.id,mr.runName,mr.round,mr.runId FROM MannualReRunTable mr LEFT JOIN \n" + 
//			"	MannualTestCaseExecutionLogs mt on mr.id=mt.reRunId  where mr.projectId=:projectid\n" + 
//			"	 and mr.createdDate BETWEEN :startdate and :enddate\n" + 
//			"	 and mr.isActive='Y' and mr.STATUS='Running' group by mt.reRunId desc LIMIT 5;",nativeQuery=true)
	
	@Transactional
	@Modifying 
	@Query(value ="select count(IF(mt.status = 'Fail', 1, NULL)) as totalFail,\n" + 
			"	count(IF((mt.bugSeverity = 'critical') AND (mt.bugstatus = 'Open'), 1, NULL)) as CriticalOpen,\n" + 
			"	count(IF((mt.bugSeverity = 'critical') AND (mt.bugstatus = 'Closed'), 1, NULL)) as CriticalClosed,\n" + 
			"	count(IF((mt.bugSeverity = 'critical') AND (mt.bugstatus = 'Reopen'), 1, NULL)) as CriticalReopen,\n" + 
			"	count(IF((mt.bugSeverity = 'critical') AND (mt.bugstatus = 'Resolved'), 1, NULL)) as CriticalResolved,\n" + 
			"	count(IF(mt.bugSeverity = 'blocker' and mt.bugstatus = 'Open', 1, NULL)) AS BlockerOpen,\n" + 
			"	count(IF((mt.bugSeverity = 'blocker') AND (mt.bugstatus = 'Closed'), 1, NULL)) as BlockerClosed,\n" + 
			"	count(IF((mt.bugSeverity = 'blocker') AND (mt.bugstatus = 'Reopen'), 1, NULL)) as BlockerReopen,\n" + 
			"	count(IF((mt.bugSeverity = 'blocker') AND (mt.bugstatus = 'Resolved'), 1, NULL)) as BlockerResolved,\n" + 
			"	count(IF(mt.bugSeverity = 'major' and mt.bugstatus = 'Open', 1, NULL)) as MajorOpen,\n" + 
			"	count(IF((mt.bugSeverity = 'major') AND (mt.bugstatus = 'Closed'), 1, NULL)) as MajorClosed,\n" + 
			"	count(IF((mt.bugSeverity = 'major') AND (mt.bugstatus = 'Reopen'), 1, NULL)) as MajorReopen,\n" + 
			"	count(IF((mt.bugSeverity = 'major') AND (mt.bugstatus = 'Resolved'), 1, NULL)) as MajorResolved,\n" + 
			"	count(IF((mt.bugSeverity = 'minor') and (mt.bugstatus = 'Open'), 1, NULL)) as MinorOpen,\n" + 
			"	count(IF((mt.bugSeverity = 'minor') AND (mt.bugstatus = 'Closed'), 1, NULL)) as MinorClosed,\n" + 
			"	count(IF((mt.bugSeverity = 'minor') AND (mt.bugstatus = 'Reopen'), 1, NULL)) as MinorReopen,\n" + 
			"	count(IF((mt.bugSeverity = 'minor') AND (mt.bugstatus = 'Resolved'), 1, NULL)) as MinorResolved,\n" + 
			"	mr.id,mr.runName,mr.round,mr.runId FROM MannualReRunTable mr LEFT JOIN \n" + 
			"	MannualTestCaseExecutionLogs mt on mr.id=mt.reRunId  where mr.projectId=:projectid\n" + 
			"	AND mr.createdDate BETWEEN :startdate and :enddate\n" + 
			"	 and mr.isActive='Y' and mr.STATUS='Running' group by mt.reRunId;",nativeQuery=true)
	List<Object[]> datewiserunfailstatus(@Param("startdate")Date startdate,@Param("enddate") Date enddate,@Param("projectid") int projectid);

	
	@Transactional
	@Modifying
	@Query("update MannualTestCaseExecutionLogs mc set mc.bugStatus=:s WHERE mc.id=:id")
	public int updateBugStatus(@Param("s") String s, @Param("id") Integer id);
	
	@Transactional
	@Modifying
	@Query("update MannualTestCaseExecutionLogs mc set mc.bugStatus=:s"
			+ ", mc.bugSeverity=:se, mc.bugPriority=:p WHERE mc.id=:id")
	public int updateBugDetails(@Param("s") String s, @Param("se") String se
			, @Param("p") String p, @Param("id") Integer id);
}
