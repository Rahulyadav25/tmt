package com.apmosys.tmt.repository;

import java.util.List;

//import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.apmosys.tmt.models.MannualReRunTable;

import jakarta.transaction.Transactional;

public interface MannualReRunRepository extends JpaRepository<MannualReRunTable, Integer> {
	@Query("FROM MannualReRunTable m where m.projectId = :projectId and m.isActive='Y' and m.status='Running'") 
	public List<MannualReRunTable> getActiveRunByPid(@Param("projectId") Integer projectId);
	@Query("FROM MannualReRunTable m where m.projectId = :projectId and m.isActive='Y' and m.status='Completed'") 
	public List<MannualReRunTable> getCompletedRunByPid(@Param("projectId") Integer projectId);
	@Query("FROM MannualReRunTable m where m.projectId = :projectId") 
	public List<MannualReRunTable> getAllRun(@Param("projectId") Integer projectId);
	@Query("FROM MannualReRunTable m where m.runId = :runid") 
	public List<MannualReRunTable> getrerunnameByrun(@Param("runid") Integer runid);
	@Query("SELECT id , runName FROM MannualReRunTable where projectId=:pid") 
	public List<Object[]> getidAndRunName(@Param("pid") Integer pid);
	
	public List<MannualReRunTable> findByRunIdOrderByRoundDesc(Integer runid);
	
	  @Transactional	  
	  @Modifying	 
	@Query("DELETE FROM MannualReRunTable WHERE id=:reRunId") 
	public void deleteRun(@Param("reRunId") Integer reRunId);
//	public MannualReRunTable findOne(Integer rerunID);
}
