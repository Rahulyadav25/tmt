package com.apmosys.tmt.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.apmosys.tmt.models.User;
import com.apmosys.tmt.models.UserDetails;
import com.apmosys.tmt.models.UserDetailsEntity;

public interface AdminRepository extends JpaRepository<UserDetails, Integer>{

	@Query("From UserDetailsEntity UserDetails Where UserDetails.username = :mailId and UserDetails.password =:password and UserDetails.isActive='Y'")
	public UserDetailsEntity findUserIgnoreCase(@Param("mailId") String mailId,@Param("password") String password);

	@Query("From UserDetailsEntity UserDetails Where UserDetails.username = :mailId and UserDetails.password =:password and UserDetails.isActive='N'")
	public UserDetailsEntity findUserIgnoreCase2(@Param("mailId") String mailId,@Param("password") String password);

	@Query("From UserDetailsEntity UserDetails Where UserDetails.username = :mailId ")
	public UserDetailsEntity findUserByOnlyMail(@Param("mailId") String mailId);
	/*@Query("From Admin admin Where admin.mailId = :mailId ")
public User getUser(@Param("mailId") String mailId);*/

	public Optional<UserDetails> findById(Integer id);

	public UserDetails findByUsername(String userName);

	//public UserDetailsEntity save(UserDetailsEntity user);

	/*public UserDetails findByUserIDAndUsernameAndNameAndUser_role(String userid, String username,
String name, String username2);*/


}