	var formToValidate= $("#loginForm").validate({
			rules: {
				loginUsername :{
					required: true,
					minlength:3
				},
				loginPassword :{
					required: true,
					minlength:3
				}
			},
			messages: {
				loginUsername :{
					required: '<span style="color:red">Please enter your username<span>',
					minlength: '<span style="color:red">Please enter a valid username<span>',
				},
				loginPassword :{
					required: '<span style="color:red">Please enter your password<span>',
					minlength: '<span style="color:red">Please enter a valid password<span>'
				}
			}
		});
		
		  function doMag() {
			  
		    var e = CryptoJS.enc.Utf8.parse("IjfgGhpOiLjbrtJf"),
		        o = CryptoJS.enc.Utf8.parse("pOiJlKmmHgFvdStf");
		    var n, a = (n = $("#loginPassword").val(), CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(n), e, {
		        keySize: 16,
		        iv: o,
		        mode: CryptoJS.mode.CBC,
		        padding: CryptoJS.pad.Pkcs7
		    }));
		    $("#loginPassword").val(a)
		}  
		
	
		$(function(){
			$("#loginForm")
			.submit(function(event){
				$("#loginMessage").html("");
				if(formToValidate.valid()){
					
				var $this = $("#loginButton");
				$this.button('loading');
				doMag();
				event.preventDefault();
				var username = $("#loginUsername").val();
				var password = $("#loginPassword").val();
				
				//console.log(username);
				//console.log(password);
				
				var data = {
					"empid": username,
					"password": password
				}

				
				$.ajax({
					url: '/ARMS/api/login',
					type: 'POST',
					data: JSON.stringify(data),
					headers: {
						'content-type':'application/json'
					},
					success: function (data) {
							
						var currentDate=moment(new Date()).format("DD-MM-YYYY HH:mm:ss");
						 if(data.serviceResponse!=null && data.serviceStatus=="Success"){
							localStorage.setItem('userName', data.serviceResponse.name);
							localStorage.setItem('userTPID', data.serviceResponse.empid);
							localStorage.setItem('lastLogin', currentDate);
							localStorage.setItem('role',data.serviceResponse.role);
							document.cookie="access-token=" + data.serviceResponse+"; Path=/";
							window.location.href="/ARMS/arms";	
							
						}else if(data.serviceStatus=="Fail" && data.serviceError=="Username or Password Invalid"){
							$("#loginMessage").html("<div class='alert alert-danger'>Login failed ! Invalid UserName or Password...!</div>");
							$this.button('reset');
						}
						else{
							$("#loginMessage").html("<div class='alert alert-danger'>Login failed ! User UnAuthorised...!</div>");
							$this.button('reset');
							
						} 
					
					},
					error: function(xhr, error){
					
						$("#loginMessage").html("<div class='alert alert-danger'>Login failed !</div>");
						$this.button('reset');
					}
				});
				}
			});
		});

