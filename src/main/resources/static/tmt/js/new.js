/**
 * 
 */

function showfield(name){
	var statusToDo=$("#statusToDo").val(); 
    if(name=='Completed'){
    	if(statusToDo!=="Pass" && statusToDo!=="Fail"){
 
        		$("#statusToDo").focus();
        		alert("Select at least one Status");
        	    e.stopImmediatePropagation();
        		return false;
        		
        	}else
        		return true;
        	
        	
        }
    else{
    	if(statusToDo=="Pass" && statusToDo=="Fail")
    	{
    		$("#statusToDo").focus();
    		e.stopImmediatePropagation();
    		return false;
    		
    	}else
    		return true;
    	
    }
    	
    	
    	  
  }

$("#submit").click(function (e){
	
	var steps=$("#steps").val();
	var testData=$("#testData").val();
	var testDescription=$("#testDescription").val();
	var expectedResult=$("#expectedResult").val();
	var result=$("#result").val();
	var testerComment=$("#testerComment").val();
	
	var statusToDo=$("#statusToDo").val();
	var testID=$("#testID").val();
	var testCaseAction=$("#testCaseAction").val();
	
	var browserNameToDo=$("#browserNameToDo").val();
	var priority=$("#priority").val();
	var complexity=$("#compexity").val();
	
	var file=$("#file").val();
	//var exetime=$("#exetime").val();
	
	var reg = /^[0-9]+$/;
//	var fileType=['jpeg','jpg','png',''];
//	var extension=$('#file').val().split(".").pop().toLowerCase();
//	if(fileType.indexOf(extension)== -1)
//	{
//	alert('Please Select A valid File Type With Extension');
//	document.getElementById("file").value = "";
//	return false;
//	}
	
	if(file==null){
		alert("Atleast one File should be selected");
		e.stopImmediatePropagation();
		return false;
	}
	
	if(browserNameToDo==null || browserNameToDo=="? string: ?"){
		$("#browserNameToDo").focus();
		alert("Atleast one browser should be selected");
		e.stopImmediatePropagation();
		return false;
	}
	
	
	
	if(steps==null || steps=="" || steps.trim() =="")
		{
		$("#steps").focus();
		alert("Steps should not be empty");
		e.stopImmediatePropagation();
		return false;
		}else if(testData==null || testData=="" || testData.trim() =="")
		{
			$("#testData").focus();
			alert("Test Data should not be empty");
			e.stopImmediatePropagation();
			return false;
			}else if(testDescription==null || testDescription=="" || testDescription.trim() =="")
			{
				$("#testDescription").focus();
				alert("Test Description should not be empty");
				e.stopImmediatePropagation();
				return false;
				}else if(expectedResult==null || expectedResult==" " || expectedResult.trim()=="")
				{
					$("#expectedResult").focus();
					alert("Expected Result should not be empty");
					e.stopImmediatePropagation();
					return false;
					}else if(result==null || result=="" || result.trim() =="")
					{
						$("#result").focus();
						alert("Actual Result should not be empty");
						e.stopImmediatePropagation();
						return false;
						}else if(testerComment==null || testerComment=="  " || testerComment == undefined || testerComment.trim()=="")
						{
							$("#testerComment").focus();
							alert("Tester Comment should not be empty");
							e.stopImmediatePropagation();
							return false;
							} else if(exetime==null || exetime=="")
								{
								$("#exetime").focus();
								alert("Execution Time should not be empty");
								e.stopImmediatePropagation();
								return false;
								} else if((testCaseAction=="Completed")&&(statusToDo!=="Pass" && statusToDo!=="Fail"))
									{
										$("#statusToDo").focus();
										alert("Status should not be empty");
										e.stopImmediatePropagation();
										return false;
									}
								else if(statusToDo =="? string:  ?" || statusToDo=="? object:null ?")
								{
									$("#statusToDo").focus();
									alert("Status should not be empty");
									e.stopImmediatePropagation();
									return false;
								}
	
								else if(testID==null || testID=="")
								{
									$("#testID").focus();
									alert("TestCase ID should not be empty");
									e.stopImmediatePropagation();
									return false;
									}
								else if(priority==null || priority=="")
									{
									$("#priority").focus();
									alert("Priority should not be empty");
									e.stopImmediatePropagation();
									return false;
									}
								else if(complexity==null || complexity=="")
									{
									$("#complexity").focus();
									alert("Complexity should not be empty");
									e.stopImmediatePropagation();
									return false;
									}
	
	
	/*if(exetime.match(reg))
	{
	return true;
	}else{
		alert("Execution should be in numbers");
		$("#exetime").focus();
		e.stopImmediatePropagation();
		return false; 
	}*/

});


$("#issues").click(function (e){ 
	
	var isNewIssue=$("#isNewIssue").val();
	var bugSeverity=$("#bugSeverity").val();
	var bugPriority=$("#bugPriority").val();
	
	 if(isNewIssue==null || isNewIssue==""){
			alert("New Issue should not be empty ");
			e.stopImmediatePropagation();
			return false;
		}else if(bugSeverity==null || bugSeverity=="" ){
			alert("Bug severity should not be empty");
			e.stopImmediatePropagation();
			return false;
		}else if(bugPriority==null && bugPriority!==" " ){
			alert("Bug bugPriority should not be empty");
			e.stopImmediatePropagation();
			return false;
		}
	 
});

$("#submitStrtR").click(function (e){ 
	var testSuitesName=$("#testSuitesName").val();
	var runname=$("#runname").val();
	var browserName=$("#browserName").val();
	var allTestCases=$("#allTestCases").val();
	var selectOneByOneTestCase=$("#selectOneByOneTestCase").val();
	var datepicker=$("[name='pEndAte']").val();
	 if(testSuitesName==null || testSuitesName==""){
			alert("Test Suite name should not be empty");
			e.stopImmediatePropagation();
			return false;
		}else if(runname==null || runname=="" ){
			alert("Test run name should not be empty");
			e.stopImmediatePropagation();
			return false;
		}else if(browserName==null && browserName!=="" ){
			//alert("Assign name should not be empty");
			alert("Assignee Name should not be empty");
			e.stopImmediatePropagation();
			return false;
		}
		else if(datepicker==null || datepicker.trim()==" " || datepicker==""){
			alert("Date should not be empty");
			e.stopImmediatePropagation();
			return false;
		}else if(allTestCases==null || allTestCases=="" ){
			alert("Select Test Case");
			e.stopImmediatePropagation();
			return false;
		}else if(selectOneByOneTestCase==null || selectOneByOneTestCase=="" ){
			alert("TestSelect Test Case");
			e.stopImmediatePropagation();
			return false;
		}
	
});


$("#submitNew").click(function (e){ 
 	var browserNameTest=$("#browserNameTest").val();
 	var regexp ="[a-zA-Z0-9]*$";
 	
	var scenariosId=$("#scenariosId").val();
	var scenarioName=$("#scenarioName").val();
	var testCaseType=$("#testCaseType").val();
	var stepsTest=$("#stepsTest").val();
	var testID=$("#testID").val();
	var funcitionality=$("#funcitionality").val();
	var testData=$("#testData").val();
	var expectedResult=$("#expectedResult").val();
	var testDescription=$("#testDescription").val();
	
	 if(testID==null || testID=="")
	{
		$("#testID").focus();
		alert("TestCase ID should not be empty");
		e.stopImmediatePropagation();
		return false;
		}
	 else if(browserNameTest==null || browserNameTest=="" ||browserNameTest=="? undefined:undefined ?" || browserNameTest=="? object:null ?"){
			alert("Atleast one Browser should be selected");
			e.stopImmediatePropagation();
			return false; 
		}
	 else if(scenariosId==null || scenariosId==""){
		alert("Scenario ID should not be Empty or AlphaNumeric");
		e.stopImmediatePropagation();
		return false;
	}else if(scenarioName==null || scenarioName=="" ){
		alert("Scenario should not be Empty");
		e.stopImmediatePropagation();
		return false;
	}else if(testCaseType!=="Positive" && testCaseType!=="Negative" ){
		alert("Atleast one Test Case Type should be selected");
		e.stopImmediatePropagation();
		return false;
	}else if(stepsTest==null || stepsTest=="" ){
		alert("Steps should not be Empty");
		e.stopImmediatePropagation();
		return false;
	}else if(funcitionality==null || funcitionality=="" ){
		alert("Funcitionality should not be Empty");
		e.stopImmediatePropagation();
		return false;
	}else if(testData==null || testData=="" ){
		alert("Test Data should not be Empty");
		e.stopImmediatePropagation();
		return false;
	}else if(expectedResult==null || expectedResult=="" ){
		alert("Expected Result should not be Empty");
		e.stopImmediatePropagation();
		return false;
	}else if(testDescription==null || testDescription=="" ){
		alert("Test Description should not be Empty");
		e.stopImmediatePropagation();
		return false;
	} 
	
	 if (!/[A-Z]/.test(testID)) {
			alert("Test Case Id must have A-Z,a-z,0-9 and special character");
			e.stopImmediatePropagation();
	         return false;
	     } else if (!/[a-z]/.test(testID)) {
	    		alert("Test Case Id must have A-Z,a-z,0-9 and special character");
	    		e.stopImmediatePropagation();
	         return false;
	     } else if (!/_[0-9]/.test(testID)) {
	    		alert("Test Case Id must have A-Z,a-z,0-9 and special character");
	    		e.stopImmediatePropagation();
	         return false;
	     }
	 
	if (!/[A-Z]/.test(scenariosId)) {
		alert("Scenario Id must have A-Z,a-z,0-9 and special character");
		e.stopImmediatePropagation();
         return false;
     } else if (!/[a-z]/.test(scenariosId)) {
    		alert("Scenario Id must have A-Z,a-z,0-9 and special character");
    		e.stopImmediatePropagation();
         return false;
     } else if (!/_[0-9]/.test(scenariosId)) {
    		alert("Scenario Id must have A-Z,a-z,0-9 and special character");
    		e.stopImmediatePropagation();
         return false;
     }

	
});  

$("#scenarioName").click(function() {
	var scenariosIds=$("#scenariosId").val();
	//alert("Hiii "+scenariosIds);
	if (!/[A-Z]/.test(scenariosIds)) {
		alert("Scenario Id must have A-Z,a-z,0-9 and special character");
		e.stopImmediatePropagation();
         return false;
     } else if (!/[a-z]/.test(scenariosIds)) {
    		alert("Scenario Id must have A-Z,a-z,0-9 and special character");
    		e.stopImmediatePropagation();
         return false;
     } else if (!/_[0-9]/.test(scenariosIds)) {
    		alert("Scenario Id must have A-Z,a-z,0-9 and special character");
    		e.stopImmediatePropagation();
         return false;
     }
     else{
    	 
     }
	 return true;

	});

$("#submity").click(function (e){ 
var browserNameTest=$("#browserNameTest").val();
 	
 	var regexp ="[a-zA-Z0-9]*$";
	var scenariosId=$("#scenariosId").val();
	var scenarioName=$("#scenarioName").val();
	var testCaseType=$("#testCaseType").val();
	var stepsTest=$("#stepsTest").val();
	var testID=$("#testID").val();
	var funcitionality=$("#funcitionality").val();
	var testData=$("#testData").val();
	var expectedResult=$("#expectedResult").val();
	var testDescription=$("#testDescription").val();
	

	 if(testID==null || testID=="")
	{
		$("#testID").focus();
		alert("TestCase ID should not be empty");
		e.stopImmediatePropagation();
		return false;
		}
	 else if(browserNameTest==null || browserNameTest=="" || browserNameTest=="? object:null ?" ){
			alert("Atleast one Browser should be selected");
			e.stopImmediatePropagation();
			return false; 
		}
	 else if(scenariosId==null || scenariosId==""){
		alert("Scenario ID should not be Empty or AlphaNumeric");
		e.stopImmediatePropagation();
		return false;
	}else if(scenarioName==null || scenarioName=="" ){
		alert("Scenario should not be Empty");
		e.stopImmediatePropagation();
		return false;
	}else if(testCaseType!=="Positive" && testCaseType!=="Negative" ){
		alert("Atleast one Test Case Type should be selected");
		e.stopImmediatePropagation();
		return false;
	}else if(stepsTest==null || stepsTest=="" ){
		alert("Steps should not be Empty");
		e.stopImmediatePropagation();
		return false;
	}else if(funcitionality==null || funcitionality=="" ){
		alert("Funcitionality should not be Empty");
		e.stopImmediatePropagation();
		return false;
	}else if(testData==null || testData=="" ){
		alert("Test Data should not be Empty");
		e.stopImmediatePropagation();
		return false;
	}else if(expectedResult==null || expectedResult=="" ){
		alert("Expected Result should not be Empty");
		e.stopImmediatePropagation();
		return false;
	}else if(testDescription==null || testDescription=="" ){
		alert("Test Description should not be Empty");
		e.stopImmediatePropagation();
		return false;
	} 
	
	 if (!/[A-Z]/.test(testID)) {
			alert("Test Case Id must have A-Z,a-z,0-9 and special character");
			e.stopImmediatePropagation();
	         return false;
	     } else if (!/[a-z]/.test(testID)) {
	    		alert("Test Case Id must have A-Z,a-z,0-9 and special character");
	    		e.stopImmediatePropagation();
	         return false;
	     } else if (!/_[0-9]/.test(testID)) {
	    		alert("Test Case Id must have A-Z,a-z,0-9 and special character");
	    		e.stopImmediatePropagation();
	         return false;
	     }
	 
	if (!/[A-Z]/.test(scenariosId)) {
		alert("Scenario Id must have A-Z,a-z,0-9 and special character");
		e.stopImmediatePropagation();
         return false;
     } else if (!/[a-z]/.test(scenariosId)) {
    		alert("Scenario Id must have A-Z,a-z,0-9 and special character");
    		e.stopImmediatePropagation();
         return false;
     } else if (!/_[0-9]/.test(scenariosId)) {
    		alert("Scenario Id must have A-Z,a-z,0-9 and special character");
    		e.stopImmediatePropagation();
         return false;
     }

	

});