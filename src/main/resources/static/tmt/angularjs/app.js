var demoApp = angular.module('demo', ['demo.controllers',
    'demo.services','ui.router', 'ngRoute', 'angularUtils.directives.dirPagination'
]);

demoApp.run(function($rootScope, $state , $stateParams, $anchorScroll) {
	$rootScope.$on('$stateChangeStart', function () {
        $anchorScroll();
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
});	

demoApp.config(function($stateProvider, $httpProvider, $urlRouterProvider, $locationProvider, $routeProvider) {
	
	$stateProvider	
	.state("dashboard", {
	    	url: "/dashboard",
	    	templateUrl: "dashboardnew.html",
	    	controller: "dashboardController",
			
	    })
	    
	    .state("admin.addProject", {
	    	url: "/addProjects",
	    	templateUrl: "addprojects.html",
	    	controller: "dashboardController",
			
	    })
	    
	  .state("projectDetails", {
	    	url: "/projectDetails",
	    	templateUrl: "projectDetails.html",
	    	controller: "projectDetailsController",
			
	    })
	   .state("projectDetails.testSuites", {
	    	url: "/testSuites",
	    	templateUrl: "testsuites.html",
	    	controller: "testSuitesController"
			
	    })
	   .state("projectDetails.overview", {
	    	url: "/overview",
	    	templateUrl: "projectOverview.html",
	    	controller: "projectOverviewController"
			
	    })
	     .state("projectDetails.testRun", {
	    	url: "/testRun",
	    	templateUrl: "testrun.html",
	    	controller: "testRunController"
			
	    })
	     .state("projectDetails.addtestrun", {
	    	url: "/addtestrun",
	    	templateUrl: "addtestrun.html",
	    	controller: "testRunController"
			
	    })
	     .state("projectDetails.todoList", {
	    	url: "/todoList",
	    	templateUrl: "todo.html",
	    	controller: "todoController"
			
	    })
	     .state("projectDetailsAuto.defectList", {
	    	url: "/defectList",
	    	templateUrl: "defectAuto.html",
	    	controller: "defectAutoController"
			
	    })
	    .state("projectDetails.projectreport", {
	    	url: "/projectreport",
	    	templateUrl: "projectReport.html",
	    	controller: "reportController"
			
	    })
	    .state("projectDetails.document", {
	    	url: "/documents",
	    	templateUrl: "documents.html",
	    	controller: "documentController"
			
	    })
	    .state("projectDetails.resource", {
	    	url: "/resourceinfo",
	    	templateUrl: "resource.html",
	    	controller: "resourceController"
			
	    })
	    
	    .state("admin.resource", {
	    	url: "/configresourceinfo",
	    	templateUrl: "configureResorces.html",
	    	controller: "configResoucesController"
			
	    })
	    
	     .state("projectDetails.myEntitlement", {
	    	url: "/myEntitlement",
	    	templateUrl: "myEntitlement.html",
	    	controller: "entitlementController"
			
	    })
	    
	    .state("projectDetails.defects", {
	    	url: "/defects",
	    	templateUrl: "defects.html",
	    	controller: "defectsController"
			
	    })
	    
	    .state("admin.adminEntitlement", {
	    	url: "/adminEntitlement",
	    	templateUrl: "adminEntitlement.html",
	    	controller: "entitlementController"
			
	    })
	    .state("admin.viewProjects", {
	    	url: "/viewProjects",
	    	templateUrl: "viewProjects.html",
	    	controller: "viewProjectsController"
			
	    })
	    
	   .state("admin", {
	    	url: "/admin",
	    	templateUrl: "admin.html",
	    	controller: "administratorController",
	    	resolve:{
	    		init:function(){
	    			console.log("working");
	    		}
	    	}
			
	    })
	    
	        .state("admin.adminAddUser", {
	    	url: "/addUser",
	    	templateUrl: "AddUserConfig.html",
	    	controller: "AddUserController"
	    	//service:"configurationservice"
			
	    })
	    
	     .state("admin.administrator", {
	    	url: "/administrator",
	    	templateUrl: "administrator.html",
	    	controller: "administratorController",
	    	resolve:{
	    		init:function(){
	    			console.log("working");
	    		}
	    	}
			
	    })
	     .state("admin.Integration", {
	    	url: "/Integration",
	    	templateUrl: "Integration.html",
	    	controller: "configurationController",
	    	resolve:{
	    		init:function(){
	    			console.log("working");
	    		}
	    	}
			
	    }).state("error", {
            url: "/error",
            templateUrl: "errorPage.html",
            controller: "administratorController",
            resolve:{
                init:function(){
                    console.log("working");
                }
            }
            
        })
	      .state("admin.usermanagement", {
	    	url: "/usermanagement",
	    	templateUrl: "UserManagement.html",
	    	controller: "configurationController",
	    	resolve:{
	    		init:function(){
	    			console.log("working");
	    		}
	    	}
			
	    })
	    .state("projectDetailsAuto", {
	    	url: "/projectDetailsAuto",
	    	templateUrl: "projectDetailsAuto.html",
	    	controller: "autopdetailsController",
	    	resolve:{
	    		init:function(){
	    			console.log("working");
	    		}
	    	}
	    })
	     .state("projectDetailsAuto.autoOverview", {
	    	url: "/autoOverview",
	    	templateUrl: "projectOverviewAuto.html",
	    	controller: "projectOverviewAutoController",
	    		resolve:{
		    		init:function(){
		    			console.log("autoOverview	 working");
		    		}
		    	}
			
	    })
	    .state("projectDetailsAuto.autorun", {
	    	url: "/autorun",
	    	templateUrl: "automationRun.html",
	    	controller: "automationRunController",
	    	resolve:{
	    		init:function(){
	    			console.log("working");
	    		}
	    	}	
	    })
	     .state("projectDetailsAuto.autoreport", {
	    	url: "/autoreport",
	    	templateUrl: "automationReport.html",
	    	controller: "autoReportController",
	    	resolve:{
	    		init:function(){
	    		}
	    	}	
	    })
	      .state("projectDetailsAuto.scriptconfgfile", {
	    	url: "/scriptconfgfile",
	    	templateUrl: "scriptConfig.html",
	    	controller: "scriptConfigController",
	    	resolve:{
	    		init:function(){
	    			console.log("working");
	    		}
	    	}
			
	    })
	    .state("projectDetailsAuto.exereport", {
	    	url: "/exereport",
	    	templateUrl: "extendedReport.html",
	    	controller: "extendedReportController",
	    	resolve:{
	    		init:function(){
	    			console.log("working");
	    		}
	    	}
			
	    })
	      .state("projectDetailsAuto.exereport.testDetails", {
	    	url: "/testDetails",
	    	templateUrl: "extentReports_testDetails.html",
	    	controller: "extendedReportController",
	    	resolve:{
	    		init:function(){
	    			console.log("working");
	    		}
	    	}
			
	    })
	      .state("projectDetailsAuto.exereport.analysis", {
	    	url: "/analysis",
	    	templateUrl: "extentReports_analysis.html",
	    	controller: "extendedReportController",
	    	resolve:{
	    		init:function(){
	    			console.log("working");
	    		}
	    	}
			
	    })
	    
	    .state("admin.myprofile", {
	    	url: "/My_Profile",
	    	templateUrl: "MyProfile.html",
	    	controller: "myProfileController",
	    })
	    
	    .state("admin.myprofile.overview", {
	    	url: "/Overview",
	    	templateUrl: "Overview.html",
	    	controller: "myProfileController",
	    })
	    
	    .state("admin.myprofile.myprojects", {
	    	url: "/My_Projects",
	    	templateUrl: "MyProjects.html",
	    	controller: "myProfileController",
	    })
	    
	    .state("admin.myprofile.mytodo", {
	    	url: "/My_ToDo",
	    	templateUrl: "MyTodo.html",
	    	controller: "myProfileController",
	    })
	    
	    .state("admin.myprofile.myreports", {
	    	url: "/My_Reports",
	    	templateUrl: "MyReports.html",
	    	controller: "myProfileController",
	    })
	    
	    .state("admin.myprofile.mydefects", {
	    	url: "/My_Defects",
	    	templateUrl: "MyDefects.html",
	    	controller: "myProfileController",
	    })
	    
	    $routeProvider   
	    .when('/scriptconfgfile', {
		    templateUrl: 'scriptConfig.html',
		    controller: 'scriptConfigController',
		    resolve: function() {
		        $('.se-pre-con').show();
		        alert("inside loader");
		      }
	    })

/*	 $urlRouterProvider.when("/projectDetails","/projectDetails/overview");*/
	 $urlRouterProvider.otherwise("/dashboard");
	 $urlRouterProvider.when("/projectDetailsAuto/exereport","/projectDetailsAuto/exereport/testDetails");
	
	
	
});