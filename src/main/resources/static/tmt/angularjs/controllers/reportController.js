demoApp.controller("reportController", function($scope, $interval, $state,
		$stateParams, $location, $anchorScroll, $rootScope, reportService) {
	$(document).ready(function() {
		checkcookie();
		var token = localStorage.getItem('tokenID');
		if (token == null || token == "") {
			window.location.href = "../login.html";
		}
	});

	/*
	 * $location.hash('top'); $anchorScroll();
	 */

	$rootScope.checkHome();
	$scope.projectName = localStorage.getItem('projectName');
	$scope.projectName = localStorage.getItem('projectName');
	$scope.projectId = localStorage.getItem('projectId');
	$scope.roleId = localStorage.getItem('userRole');
	if ($scope.roleId != undefined) {
		if ($scope.roleId === "100") {
			$("#testRun").css("display", "unset");
			$("#todo").css("display", "unset");
			$("#testSuites").css("display", "unset");
			$("#Automation").css("display", "unset");
		}
		if ($scope.projectwiserole == "Manager"
				|| $scope.projectwiserole === "Tester") {
			$("#todo").css("display", "unset");
			$("#testSuites").css("display", "unset");
			$("#Automation").css("display", "unset");
			if ($scope.projectwiserole === "Manager") {
				$("#testRun").css("display", "unset");

			} else if ($scope.projectwiserole === "Tester") {

				if ($scope.url == '/testRun') {
					$state.transitionTo('error');
				}

			} else if ($scope.projectwiserole === "Readonly") {

			}

		}
	}

	else {
		$state.transitionTo('error');
	}
	$("#iconList .nav-link").each(function() {
		$(this).removeClass("active");
	});
	$("#Reports a.nav-link").addClass("active");
	$scope.projectwiserole = localStorage.getItem('projectwiserole');

	$scope.showRunDetails = false;
	$scope.tableHeadingList = {
		"TestGroup Name" : "groupName",
		"TestCase Id" : "testCaseSrNo",
		"TestDescription" : "testDescription",
		"Execution Status" : "status",
		"Action" : "testCaseAction"
	}
	var columnList = {
			"TestGroup Name" : "groupName",
			"TestCase Id" : "testCaseSrNo",
			"Scenario ID" : "scenarioID",
			"Scenarios" : "scenarios",
			"Functionality" : "functionality",
			"TestCase Type" : "testCaseType",
			"Test Description" : "testDescription",
			"Date And Time Of Execution" : "dateOfExec",
			"Browser" : "browser",
			"Test Data" : "testData",
			"Steps" : "steps",
			"Expected Result" : "expectedResult",
			"Actual Result" : "actualResult",
			"Execution Status" : "status",
			"Execution Time" : "reportExcutionTime",
			"Tester Comment" : "testerComment",
			"Tester Name" : "testerName",
			"Action" : "testCaseAction",
			"Issue Integration ID" : "issueIntegrationId",
			"Bug Id" : "bugId",
			"Bug Severity" : "bugSeverity",
			"Bug Priority" : "bugPriority",
			"Bug Status" : "bugStatus",
			"Bug Age" : "bugAge",
			"Priority": "priority",
			"Complexity": "complexity",

		}
	var order = [ 'TestGroup Name', 'TestCase Id', 'Scenario ID', 'Scenarios',
		'Functionality', 'TestCase Type', 'Test Description',
		'Date And Time Of Execution', 'Browser', 'Test Data', 'Steps',
		'Expected Result', 'Actual Result', 'Execution Status',
		'Tester Comment', 'Action', 'Execution Time', 'Tester Name',
		'Issue Integration ID', 'Bug Id', 'Bug Severity', 'Bug Priority',
		'Bug Status', 'Bug Age', 'Priority', 'Complexity' ];

	function setOrder(obj) {
		var out = [];
		order.forEach(function(key) {
			if (key in obj)
				out.push({
					title : key,
					value : obj[key]
				});
		});
		return out;
	}

	$scope.items = setOrder(columnList);
	var countOfColumnList = $scope.items.length;
	$("select").on("select2:select", function(evt) {
		var element = evt.params.data.element;
		var $element = $(element);

		$element.detach();
		$(this).append($element);
		$(this).trigger("change");
		var count = $(this).select2('data').length
		if (count == countOfColumnList) {
			$("#selectCheckBox").prop('checked', true);
		} else {
			$("#selectCheckBox").prop('checked', false);
		}

	});
	$("select").on("select2:unselect", function(evt) {

		$("#selectCheckBox").prop('checked', false);

	});
	$('#selectCheckBox').change(
			function() {
				if (this.checked) {

					$(".reportColumnList > option")
							.prop('selected', 'selected').trigger('change');

				} else {
					$('.reportColumnList').val($scope.allRunList).trigger(
							'change');

				}
			});

	$('select').on('select2:close', function(evt) {
		var uldiv = $(this).siblings('span.select2').find('ul')
		var count = $(this).select2('data').length
		if (count == 0) {
			uldiv.html("<li>" + count + " item selected</li>")
		}
		else if (count == 1) {
			uldiv.html("<li>" + count + " item selected</li>")
		} 
		else {
			uldiv.html("<li>" + count + " items selected</li>")
		}

	});

	$scope.columnList = columnList;
	var selMul = false;
	$scope.selectMultiple = function() {
		if ($scope.selMul == true) {
			$('.reportColumnList').val($scope.allRunList).trigger('change');
		} else {
			$(".reportColumnList > option").prop('selected', 'selected')
					.trigger('change');
		}
	}

	$scope.showMul1 = function() {
		$(".reportColumnList").select2({
			closeOnSelect : false,
			placeholder : "Columns",
			allowHtml : true,
			allowClear : true,
			tags : true,

		});

	};
	$scope.getAllRunName = function() {
		reportService.getAllRunName($scope.projectId).then(
				function Success(response) {
					$(".se-pre-con").hide();
					$scope.allRunList = response.data.serviceResponse;

				});
	}
	$scope.changeRunName = function(runListSelected) {
		$scope.allRunListReport = null;
		console.log(runListSelected);

		reportService.getReRunName(runListSelected.id).then(
				function Success(response) {
					$(".se-pre-con").hide();
					$scope.rerunname = response.data.serviceResponse;

				});
		$scope.testCaseAction = "";
		$scope.status = "";
	}

	var applist = [];
	$scope.getRunReport = function() {
		$scope.items1 = "";
		var applist = $('#columnlist').val(); 
		
		if($scope.runListSelected ==null || $scope.runListSelected=="" || $scope.runListSelected ==undefined)
		{
			alert("Kindly select test run");
			return false;
		}
		else if($scope.rerunListSelected ==null || $scope.rerunListSelected=="" || $scope.rerunListSelected ==undefined)
		{
			alert("Kindly select test rerun");
			return false;
		}
		else if (applist.length <= 0) {
			$scope.tableHeadingList = $scope.applist;
			console.log($scope.tableHeadingList);
			alert("Kindly select column");
			return false;
		}
		else {
			$scope.tableHeadingList = {};
			console.log(applist);
			for (i = 0; i < applist.length; i++) {
				// console.log(applist[0]);
				$scope.tableHeadingList[applist[i]] = columnList[applist[i]];
			}
			console.log($scope.tableHeadingList);
			$scope.items1 = setOrder($scope.tableHeadingList);
			console.log($scope.items1);
			// console.log($scope.tableHeadingList);
		}
		// $('.reportColumnList').val($scope.tableHeadingList).trigger('change');
		console.log($scope.rerunListSelected.id);
		reportService.getRunReport($scope.rerunListSelected.id).then(
				function Success(response) {
					$(".se-pre-con").hide();
					$scope.showRunDetails = true;
					$scope.allRunListReport = response.data.serviceResponse;
				});
	}
	$scope.getRowVal = function(testcase, heading) {
		return testcase[heading];
	}
	$scope.selectRespectiveData = function() {
		$scope.columnlist = $('#columnlist').val();
		console.log(columnlist)
	}
	$scope.showMul1();
	$scope.downloadReport = function(id, fileName) {
		var currentdate = new Date();
		var datetime = " " + currentdate.getDate() + "/"
				+ (currentdate.getMonth() + 1) + "/"
				+ currentdate.getFullYear() + "- " + currentdate.getHours()
				+ ":" + currentdate.getMinutes() + ":"
				+ currentdate.getSeconds();
		$("#" + id).tableHTMLExport({
			type : 'csv',
			filename : fileName + '_' + datetime + '.csv'
		});
	}
	$scope.downloadPDFReport = function(id, fileName) {
		var currentdate = new Date();
		var datetime = " " + currentdate.getDate() + "/"
				+ (currentdate.getMonth() + 1) + "/"
				+ currentdate.getFullYear() + "- " + currentdate.getHours()
				+ ":" + currentdate.getMinutes() + ":"
				+ currentdate.getSeconds();
		$("#" + id).tableHTMLExport({
			type : 'pdf',
			filename : fileName + '_' + datetime
		});
	}
	$scope.downloadReport1 = function(id, fileName) {
		console.log(id);
		var data = $('#' + id)[0].outerHTML;
		var a = document.createElement("a");
		var URL = window.location.href;
		var downloadUrl = 'data:application/vnd.ms-excel,'
				+ encodeURIComponent(data);
		var currentdate = new Date();
		var datetime = " " + currentdate.getDate() + "/"
				+ (currentdate.getMonth() + 1) + "/"
				+ currentdate.getFullYear() + "- " + currentdate.getHours()
				+ ":" + currentdate.getMinutes() + ":"
				+ currentdate.getSeconds();
		// geting date and time
		a.href = downloadUrl;
		a.download = fileName + '_' + datetime + '.xls';
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
	}

	$scope.notcompleted = true; // pass-fail
	$scope.disableOpt = false; // action-completed
	$scope.disblestatus=false;
	$scope.checkAction = function() {
		if ($scope.testCaseAction.testCaseAction == "") {
			$scope.status = "";
			$scope.disableOpt = false;
		} else if (!$scope.testCaseAction.testCaseAction == "Completed") {
			$scope.notcompleted = false;
		}
		if ($scope.testCaseAction.testCaseAction == "NULL") {
			$scope.status = "";
			
			$scope.notcompleted = true; // pass-fail
			$scope.disableOpt = false; // action-completed
			
			if($scope.status=="Blocked-Showstopper" || $scope.status=="Blocked-Showstopper" || $scope.status=="Blocked-Showstopper" || $scope.status=="Blocked-Showstopper")
				{
				$scope.disblestatus=true;
				}
			
		}
	}
	$scope.switchdiv = function() {

		/*if($scope.testCaseAction==""){
		 $scope.status="";
		 }*/
		if ($scope.status == "Fail") {

			$scope.testCaseAction.testCaseAction = "Completed";
			$scope.disableOpt = true;

		} else if ($scope.status == "Pass") {
			$scope.testCaseAction.testCaseAction = "Completed";
			$scope.disableOpt = true;
		} else if( $scope.testCaseAction.status=="Blocked-Showstopper" ||$scope.testCaseAction.status=="Blocked-Undelivered" || 
				$scope.testCaseAction.status=="Blocked-Testdata" || $scope.testCaseAction.status=="Blocked-FSD not clear" || $scope.testCaseAction.status=="On Hold"
		)
	{
	
	$('#testcaseAction').val("On Hold");
	
	jQuery("select#testcaseAction option[value='On Hold']").show();
	$("#testcaseAction option[value='On Hold']").attr("selected", "selected");

	$scope.testCaseAction.testCaseAction='On Hold';	
	}
else if($scope.testCaseAction.status=="NE" )
	{
	
	$('#testcaseAction').val("Not Started");
	
	jQuery("select#testcaseAction option[value='Not Started']").show();
	$("#testcaseAction option[value='Not Started']").attr("selected", "selected");

	$scope.testCaseAction.testCaseAction='Not Started';	
		
	}
else if($scope.testCaseAction.status=="Deferred" || $scope.testCaseAction.status=="NA"
	 || $scope.testCaseAction.status=="Duplicate" || $scope.testCaseAction.status=="Out of Scope" )
	{
	
	$('#testCaseAction').val("N/A");
	
	jQuery("select#testcaseAction option[value='N/A']").show();
	$("#testcaseAction option[value='N/A']").attr("selected", "selected");

	$$scope.testCaseAction.testCaseAction='N/A';
	}


		else {
			
			$scope.notcompleted = true;
			$scope.disblestatus=false;
			$scope.disableOpt = false;
		}
	}

});