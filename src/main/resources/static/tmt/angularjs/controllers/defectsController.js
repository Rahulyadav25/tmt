	demoApp.controller("defectsController", function($scope,$interval,$filter,$state,$stateParams,$location,$anchorScroll,$rootScope,defectsService,reportService) {
		
		
		
		$(document).ready(function(){
			checkcookie();
			var token=localStorage.getItem('tokenID');
			if (token==null|| token=="")
			{
			window.location.href = "../login.html";
			}
			});
		var d=new Date();
		var month=d.getMonth()+1;
		var year=d.getFullYear(); 
		var date=d.getDate();
		var countOfColumnListNew1;

		if(month<10)
			{
			frm="0"+month+"/"+date+"/"+year;
			//frm=year+"/0"+month+"/"+date;
			}
		else
			{
			frm=month+"/"+date+"/"+year;
			//frm=year+"/"+month+"/"+date;
			}
		
		 var d1 = new Date();
		 d1.setDate(d1.getDate()-5);
		 var month1=d1.getMonth()+1;
			var year1=d1.getFullYear(); 
			var date1=d1.getDate();
			if(month<10)
			{
			frm1="0"+month1+"/"+date1+"/"+year1;
			//frm1=year1+"/0"+month1+"/"+date1;
			}
		else
			{
			frm1=month1+"/"+date1+"/"+year1;
			//frm1=year1+"/"+month1+"/"+date1;
			}
		
		$scope.toDate=frm;
		$scope.fromDate=frm1;
		$rootScope.checkHome();
		$scope.projectName=localStorage.getItem('projectName');
		var pName=$scope.projectName
		$scope.projectId = localStorage.getItem('projectId');
		$scope.uId= localStorage.getItem('userid');
		$scope.user_role= localStorage.getItem('userRole');
		$scope.roletype="";
		$scope.buttonvisible=false;
		$scope.documentstatus="";
		$scope.isPublic="N";
		$("#iconList .nav-link").each(function(){
			$(this).removeClass("active");
		});
		$("#defects a.nav-link").addClass("active");
		$scope.projectwiserole=localStorage.getItem('projectwiserole');
		 if($scope.user_role!=undefined)
			 {
			 if($scope.user_role==="100")
				{
				$("#testRun").css("display","unset");
				$("#todo").css("display","unset");
				$("#testSuites").css("display","unset");
				$("#Automation").css("display","unset");
				}
			if($scope.projectwiserole=="Manager" || $scope.projectwiserole==="Tester")
			{
				$("#todo").css("display","unset");
				$("#testSuites").css("display","unset");
				$("#Automation").css("display","unset");
				if($scope.projectwiserole==="Manager")
				{
					$("#testRun").css("display","unset");
			
				}
				else if($scope.projectwiserole==="Tester")
				{
			
					if($scope.url=='/testRun')
					{
						$state.transitionTo('error');
					}
				
				}
				else if($scope.projectwiserole==="Readonly")
				{
			
				}

			}
			 }
			
		else
		{
			$state.transitionTo('error');
		}
		
		
	

	/*----------------------defect refresh------------------------------------*/
		 
		/* 
		 $(document).ready(function(){
		       $(".defectrfrsh").click(function(){
		        $.ajax({
		            url:"",
		            success:refreshSuccess,
		            error:refreshFail
		        });
		       });

		        function refreshSuccess(result){
		            var obj = jQuery.parseJSON(eval(result));
		            $("").html(obj.name);
		            $("#div2").html(obj.place);
		        }

		        function refreshFail(result){
		            $("#div1").html("Refresh failed !!!");
		        }

		    });
		 */
		/* $.ajax({

             type: 'POST',
             url: '/updaterow.php',  
             data: parameters,  
             success: function(response)
             { 
                 $('#table').html(response); 
             },

             error: function(response){
                 alert(response); 

             },

           });*/

		 
		 /*-------------------------------------------*/
		 
			$scope.defectInit=function()
			{
				//alert("hii defect")
			defectsService.getDefectProjectDetails($scope.projectId).then(function Success(response) {
			 	$scope.projectDefectsData=response.data.serviceResponse;
			 	$(".se-pre-con").fadeOut('slow');
				console.log($scope.projectDefectsData);
				let toolName = localStorage.getItem('toolName');	
				var dfctArray=[];
				for(i=0;i<$scope.projectDefectsData.length;i++)
				{
					$scope.projectID=$scope.projectDefectsData[i][5];
					var d=new Date($scope.projectID);
					var t=("00" + (d.getMonth() + 1)).slice(-2)+"/"+ ("00" + d.getDate()).slice(-2)+"/"+d.getFullYear();
					if($scope.projectID !=null)
					{
						$scope.projectDefectsData[i][5]=t;
					}
					$scope.projectDefectsData[i].splice(0,0,i+1);
				}

				
				let defectData = $scope.projectDefectsData
				if(toolName === 'Redmine')
				{
					defectsService.getProjectIssuesStatus($scope.projectId).then(function Success(response) {
						$(".se-pre-con").fadeOut('slow');
						let redmineBugsList =  	response.data.serviceResponse;
						
						console.log(redmineBugsList);
						defectData.forEach((project)=>{
							
							let redmineBug = redmineBugsList.find((bug)=>{
								return bug.projectId == project[1]
							});
							project[5] = redmineBug.statusName
							
						})
						dfctArray = defectData;
						console.log(dfctArray);
						var table = $('#example').DataTable();
						table.destroy();
						$("#example").DataTable({
							/*data:$scope.projectDefectsData,*/
							data:dfctArray,
							
							dom: 'Bfrtip',
							select: true,
						   
							buttons: [
								{
							   extend: 'collection',
							   text: 'Export',
								buttons: [
								   {
									   extend: "excelHtml5",
									   fileName:  "CustomFileName" + ".xlsx",
										 title:pName,
									 
								   },
								   {
									   extend: "csvHtml5",
									   fileName:  "CustomFileName" + ".csv",
										 title:pName ,
									   
								   },
								   {
									   extend: "pdfHtml5",
									   fileName:  "CustomFileName" + ".pdf",
										 title:pName ,
									   orientation : 'landscape',
										pageSize : 'LEGAL'
								   }
								  ]
								}
							],
						
						});
				
					});
				}
				else{
					dfctArray=$scope.projectDefectsData;
					var table = $('#example').DataTable();
						table.destroy();
						$("#example").DataTable({
							/*data:$scope.projectDefectsData,*/
							data:dfctArray,
							
							dom: 'Bfrtip',
							select: true,
						   
							buttons: [
								{
							   extend: 'collection',
							   text: 'Export',
								buttons: [
								   {
									   extend: "excelHtml5",
									   fileName:  "CustomFileName" + ".xlsx",
										 title:pName,
									 
								   },
								   {
									   extend: "csvHtml5",
									   fileName:  "CustomFileName" + ".csv",
										 title:pName ,
									   
								   },
								   {
									   extend: "pdfHtml5",
									   fileName:  "CustomFileName" + ".pdf",
										 title:pName ,
									   orientation : 'landscape',
										pageSize : 'LEGAL'
								   }
								  ]
								}
							],
						
						});
				}
				
			 
			 	
				
				
	
			});
			}
			
			$scope.projectStatusdedect=[];
			 defectsService.getstuatudefectsgraph($scope.projectId).then(function Success(response) {
				 $scope.projectStatusdedect=response.data.serviceResponse;
				
				var listmonth=[];
				var listreopen=[];
				var listclosed=[];
				var listreolsved=[];
				var listopen=[];
				var stet=[];
				for(var i=0;i<$scope.projectStatusdedect.length;i++)
					{
					 listmonth.unshift($scope.projectStatusdedect[i].monthname);
					 listopen.unshift($scope.projectStatusdedect[i].open);
					 listclosed.unshift($scope.projectStatusdedect[i].closed);
					 listreolsved.unshift($scope.projectStatusdedect[i].resolved);
					 listreopen.unshift($scope.projectStatusdedect[i].reopen);
					
				 $('#chartZoneDisplay').highcharts({
				        xAxis: {
				            categories: listmonth
				            	},
				        yAxis: {
				            plotLines: [{
				                value: 0,
				                width: 1,
				                color: '#808080'
				            }]
				        },
				        legend: {
				            layout: 'vertical',
				            align: 'right',
				            verticalAlign: 'middle',
				            borderWidth: 0
				        },
				        series: [{
				            name: 'Open',
				            data: listopen
				        }, {
				            name: 'Closed',
				            data: listclosed
				        }, {
				            name: 'Reopen',
				            data: listreopen
				        }, {
				            name: 'Resolved',
				            data: listreolsved
				        }]
				    });
					}
				 
			 });
			 
			$scope.projectserviritystatus=[];
			 defectsService.getdefectseveritygraph($scope.projectId).then(function Success(response) {
				 $scope.projectserviritystatus=response.data.serviceResponse;
				// alert($scope.projectserviritystatus[0][1]);
				 //var critialtemp=
				 
				 var chart = AmCharts.makeChart("chartdiv", {
					  "type": "serial",
					  "startDuration": 2,
					  "dataProvider": [{
					    "country": "Blocker",
					    "visits": $scope.projectserviritystatus[0][1],
					    "color": "#FF0F00"
					  }, {
					    "country": "Critical",
					    "visits": $scope.projectserviritystatus[0][0],
					    "color": "#FF6600"
					  },{
					    "country": "Major",
					    "visits":$scope.projectserviritystatus[0][2],
					    "color": "#B0DE09"
					  }, {
					    "country": "Minor",
					    "visits":$scope.projectserviritystatus[0][3],
					    "color": "#04D215"
					  },  ],
					  "valueAxes": [{
					    "position": "left",
					    "axisAlpha": 0,
					    "gridAlpha": 0
					  }],
					  "graphs": [{
					    "balloonText": "[[category]]: <b>[[value]]</b>",
					    "colorField": "color",
					    "fillAlphas": 0.85,
					    "lineAlpha": 0.1,
					    "type": "column",
					    "topRadius": 1,
					    "valueField": "visits"
					  }],
					  "depth3D": 40,
					  "angle": 30,
					  "chartCursor": {
					    "categoryBalloonEnabled": false,
					    "cursorAlpha": 0,
					    "zoomable": false
					  },
					  "categoryField": "country",
					  "categoryAxis": {
					    "gridPosition": "start",
					    "axisAlpha": 0,
					    "gridAlpha": 0

					  },
					  "exportConfig": {
					    "menuTop": "20px",
					    "menuRight": "20px",
					    "menuItems": [{
					      "icon": '/lib/3/images/export.png',
					      "format": 'png'
					    }]
					  }
					}, 0);

					
				
			 });
			/*-------------------------------------*/
			 $scope.defectRfrsh=function()
				{
					defectsService.updateBugDetailsAPI($scope.projectName,$scope.projectId).then(
							function Success(response) {
								$scope.defectInit();
								$(".se-pre-con").fadeOut('slow');
								
							});
				}
				
			 $scope.exportRunWise=function()
				{
					defectsService.exportRunWise($scope.projectId).then(
						function Success(response) {
								const url = window.URL.createObjectURL(response.data);
  			 					 const a = document.createElement('a');
  	 								 a.style.display = 'none';
   									 a.href = url;
   						 // the filename you want
    								a.download = 'RunWiseReoprtJiraBugs.xlsx';
  									  document.body.appendChild(a);
  									  a.click();
   									 window.URL.revokeObjectURL(url);
							});
				}
			/*
			var binaryData = [];
					binaryData.push(response.data);
					const url =window.URL.createObjectURL(new Blob(binaryData, {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}))			
//								const url = window.URL.createObjectURL(response.data);
					 	 		const a = document.createElement('a');
  	 								 a.style.display = 'none';
   									 a.href = url;
   						 // the filename you want
    								a.download = 'RunWiseReoprtJiraBugs.xlsx';
  									  document.body.appendChild(a); 
  									  a.click();
   									 window.URL.revokeObjectURL(url);
							});
			*/
	});
	
	