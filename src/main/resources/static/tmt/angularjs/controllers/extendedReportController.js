demoApp.controller("extendedReportController", function($scope,$interval,$state,$stateParams,$location,$anchorScroll, $rootScope, autoReportService) {
	$scope.url = $location.url();
	$(document).ready(function(){
		checkcookie();
		var token=localStorage.getItem('tokenID');
		if (token==null|| token=="")
		{
		window.location.href = "../login.html";
		}
		if($scope.url =='/projectDetailsAuto/exereport')
			$state.go('projectDetailsAuto.exereport.testDetails');
		
		});
	
	$rootScope.checkHome();
	$scope.projectName = localStorage.getItem('projectName');
	$scope.projectId = localStorage.getItem('projectId');
	
	$scope.testcaseIDwiseData=[];
	$scope.randomData=[];
	$scope.createBug=[];
	$scope.RaiseBugAutomationraise=function()
	{
		
	/*	$scope.createBug.testCaseId=value[0].userid;
*/		$scope.createBug.tstatus=$scope.raiseValue[0].status;
		$scope.createBug.texpectedResult=$scope.raiseValue[0].expectedresult;
		$scope.createBug.tseverity=$scope.raiseValue[0].severity;
		$scope.createBug.tpriority=$scope.raiseValue[0].priority;
		$scope.createBug.tbugsummary=$scope.raiseValue[0].summary;
		$scope.createBug.tbrowser=$scope.raiseValue[0].browser;
		$scope.createBug.description=$scope.raiseValue[0].description;
		$scope.createBug.bugtool=$scope.raiseValue[0].bugtool;
		$scope.createBug.projectname=$scope.raiseValue[0].projectname;
		console.log($scope.createBug);
		autoReportService.saveAutomationbug($scope.createBug).then(function Success(response)
		{
			var result="";
			$scope.raiseissue=response.data.serviceResponse;
			console.log($scope.raiseissue);
			if($scope.raiseissue===null||$scope.raiseissue==="" || $scope.raiseissue===undefined ){
				result = "Sorry! Could not raise the issue";
				$("#response").css("color", "red");
			}
			if($scope.raiseissue!==null||$scope.raiseissue!=="" || $scope.raiseissue!==undefined ){
				result = "Issue has been raised Successfully. Issue id is "+$scope.raiseissue;
				$("#response").css("color", "green");
			}
			 $('#response').text(result); 
		
			$("#getSevirityPriorityModal").modal('hide');
			$(".se-pre-con").hide();
			$("#showResponse").modal('show');
			
		});
	}
	
	$scope.userid===null||$scope.userid===""||$scope.mailid==="" || $scope.userid===undefined 
	
	
	$scope.raiseKey=null;
	$scope.raiseValue=[];
	$scope.confirmRaisedIssue=function(key, value)
	{
		$scope.raiseKey=key;
		$scope.raiseValue=value;
		$("#confirmation-dialog-Mul-Del").modal('show');
	}
	$scope.getSevirityPriority=function()
	{
		
	$("#getSevirityPriorityModal").modal('show');
		$("#confirmation-dialog-Mul-Del").modal('hide');
	}
	
	
	$scope.getScenarioRunwise=function()
	{
		var runId=$rootScope.autoReportData[0];
		$scope.scenarios = [];
		
		autoReportService.getScenarioRunwise(runId).then(function Success(response)
		{
//			$(".se-pre-con").fadeOut('slow');
			$scope.scenarios = 	response;
			$scope.scenariosList = 	response.data;
			$scope.getExtendedReport();
		});
	}//getScenarioRunwise
	
//	$scope.getScenarioRunwise();
	
	$scope.updateval=function()
	{
		alert($scope.selScenario);
		$scope.getExtendedReport();
	}//updateval()
	
	
	$scope.getExtendedReport=function()
	 {
		var runId=$rootScope.autoReportData[0];
		//if not being selected then do default 1st one
		if($scope.selScenario ==undefined){
		var scenario=$scope.scenariosList[0];    
		}
		else{
		var scenario=$scope.selScenario;    
		}
		
		$scope.urnIdCurrent=runId;
		
		//alert(runId);
		autoReportService.getExtendedReport(runId,scenario).then(function Success(response)
		{
			$(".se-pre-con").fadeOut('slow');
			$scope.tcFilteredData=[];
			$scope.reportData=response.data.serviceResponse[0];
			for ( const [key,value] of Object.entries($scope.reportData ) ) {
				 let obj={
						 tcasesId:key,
						 scenarioID:key,
						 status:value[0].verify.status
				 }
				 $scope.tcFilteredData.push(obj);
			
			}
			/*
			for ( const [key,value] of Object.entries($scope.reportData ) ) {
				for(var i=0; i<value.length; i++)
					{
				 let obj={
						 tcasesId:key,
						 scenarioID:key,
						 status:value[i].verify.status,
						 ver:value[i].verify
				 	}
				 $scope.tcFilteredData.push(obj);
			   }//innser for
			}//outer for
			*/
			$rootScope.testDetailsData=$scope.reportData;
			$rootScope.scenarioData=response.data.serviceResponse[1];
			if($scope.reportData)
			{
				$scope.reportTestcaseID=Object.keys($scope.reportData);
				$rootScope.dataTestCaseID=$scope.reportTestcaseID;
				
				if(($rootScope.testCaseID == '')||($rootScope.testCaseID == undefined)){
					$scope.testID=$scope.reportTestcaseID[0];
					//$scope.reportManupulation();
				}
				else{
					$scope.testID=$rootScope.testCaseID;
				}
				$scope.reportManupulation();
				//$scope.extentReportAnalysis();
			}
			
		});
	}

	
//	$scope.getExtendedReport(); //data-ng-init='getExtendedReport()' its automatically called on load from html
	
	$scope.analysis={
			testCount:0,
			testPassCount:0,
			testFailCount:0,
			stepsCount:0,
			stepsPassCount:0,
			stepsFailCount:0,
			verifyCount:0,
			verifyPassCount:0,
			passPercentage:0,
			startTime:null,
			endTime:null,
			ip:null,
			user:null,
			location:null,
			scenarioCount:0,
			scenarioPassCount:0,
			scenarioFailCount:0
	}
	$scope.analysisTestArray=[];
	$scope.analysisStepsArray=[];
	$scope.analysisScenarioArray=[];

		
	$scope.extentReportAnalysis=function()
	{
		var runId=$rootScope.autoReportData[0];
		//alert(runId);
		autoReportService.getTotalScenariosTestCasesSteps(runId).then(function Success(response)
		{
//			$(".se-pre-con").fadeOut('slow');
			$scope.environment = response.data;
			$scope.analysis.scenarioCount1= response.data.scenarios;
			$scope.analysis.testCount2=response.data.testCases;
			$scope.analysis.stepsCount3=response.data.steps;
			$scope.analysis.testcasesP=response.data.testcasesP;
			$scope.analysis.scenarioF=response.data.scenarioF;
		
		autoReportService.getEnvironmentAnalysis(runId).then(function Success(response)
		{
			$(".se-pre-con").fadeOut('slow');
			$scope.environment=response.data.serviceResponse;
			$scope.analysis.ip=$scope.environment[0][0].split(',')[0];
			$scope.analysis.user=$scope.environment[0][0].split(',')[1];
			$scope.analysis.location=$scope.environment[0][0].split(',')[2];
			$scope.analysis.startTime=$scope.environment[0][1]
			$scope.analysis.endTime=$scope.environment[0][2]
		});
		
		var reportData=$rootScope.testDetailsData;
		var reportTestcaseID=$rootScope.dataTestCaseID;
		$scope.verifyCount=0;
		$scope.stepsCount=0;
		let stepsCount=0;
		
		$scope.analysis.testCount=reportTestcaseID.length;
		for(var i=0; i<reportTestcaseID.length; i++)
		{
			var reportDataTestCase=reportData[reportTestcaseID[i]];
			var checkVerifyCount=0;
			for(var j=0; j<reportDataTestCase.length; j++)
			{
				var reportDataVerifySteps=reportDataTestCase[j]
				if(reportDataVerifySteps.verify)
				{
					$scope.verifyCount=$scope.verifyCount+1;
					if(reportDataVerifySteps.verify.status=='PASS')
					{
						checkVerifyCount=checkVerifyCount+1;
					}
					
				}
				for(var k=0; k<reportDataVerifySteps.steps.length; k++)
				{
					if(reportDataVerifySteps.steps[k])
					{
						$scope.stepsCount=$scope.stepsCount+1;
					}
				}
				
			}
			
			
			var v=0;
			var c=0;
			v=$scope.verifyCount;
			c=checkVerifyCount;
			stepsCount=stepsCount+c;
			if((reportDataTestCase.length==c)&&(v!=0))
			{
				$scope.analysis.testPassCount=$scope.analysis.testPassCount+1;
			}
		}
		
		$scope.analysis.stepsCount=$scope.stepsCount;
		$scope.analysis.testFailCount=$scope.analysis.testCount-$scope.analysis.testPassCount;
		$scope.analysis.stepsPassCount=stepsCount;
		$scope.analysis.stepsFailCount=$scope.verifyCount-$scope.analysis.stepsPassCount;
			
		$scope.analysisTestArray=[$scope.analysis.testcasesP,($scope.analysis.testCount2-$scope.analysis.testcasesP)];
		$scope.analysisStepsArray=[$scope.analysis.stepsPassCount,$scope.analysis.stepsFailCount];
		
		$scope.analysis.passPercentage=($scope.analysis.testPassCount*100)/$scope.analysis.testCount;
		
		
		
		//graph
		var ctx = document.getElementById('test-analysis').getContext('2d');
		var myChart = new Chart(ctx, {
		    type: 'doughnut',
		    data: {
		        labels: ['Pass', 'Fail'],
		        datasets: [{
		            //label: '# of Votes',
		            data: $scope.analysisTestArray,
		            backgroundColor: [
		            	'rgba(75, 192, 192)',
		                'rgba(255, 99, 132)'		                
		            ],
		            borderColor: [
		            	'rgba(75, 192, 192, 1)',
		                'rgba(255, 99, 132, 1)'
		                
		            ],
		            borderWidth: 1
		        }]
		    },
		});
		
		var ctx2 = document.getElementById('step-analysis').getContext('2d');
		var myChart = new Chart(ctx2, {
		    type: 'doughnut',
		    data: {
		        labels: ['Pass', 'Fail'],
		        datasets: [{
		            //label: '# of Votes',
		            data: $scope.analysisStepsArray,
		            backgroundColor: [
		            	'rgba(75, 192, 192)',
		                'rgba(255, 99, 132)'		                
		            ],
		            borderColor: [
		            	'rgba(75, 192, 192, 1)',
		                'rgba(255, 99, 132, 1)'
		                
		            ],
		            borderWidth: 1
		        }]
		    },
		});
		
		let scenarioAnalysisData=$rootScope.scenarioData;
		$scope.reportScenarioID=Object.keys(scenarioAnalysisData);
		$scope.analysis.scenarioCount=$scope.reportScenarioID.length;
		
		
		
		for(let as=0; as<$scope.analysis.scenarioCount; as++){
			let asData=scenarioAnalysisData[$scope.reportScenarioID[as]];
			let asDataLength=asData.length;
			let passCountScenario=0;
			for(let asj=0; asj<asDataLength; asj++){
				let verifyData=asData[asj].verify;
				if(verifyData){
					if(verifyData.status=='PASS'){
						passCountScenario++;
					}
				}
			}
			if(asDataLength==passCountScenario){
				$scope.analysis.scenarioPassCount++;
			}
		}
		$scope.analysis.scenarioFailCount=$scope.analysis.scenarioCount-$scope.analysis.scenarioPassCount;
		$scope.analysisScenarioArray=[($scope.analysis.scenarioCount1-$scope.analysis.scenarioF), $scope.analysis.scenarioF];
		
		var ctx3 = document.getElementById('scenario-analysis').getContext('2d');
		var myChart = new Chart(ctx3, {
		    type: 'doughnut',
		    data: {
		        labels: ['Pass', 'Fail'],
		        datasets: [{
		            //label: '# of Votes',
		            data: $scope.analysisScenarioArray,
		            backgroundColor: [
		            	'rgba(75, 192, 192)',
		                'rgba(255, 99, 132)'		                
		            ],
		            borderColor: [
		            	'rgba(75, 192, 192, 1)',
		                'rgba(255, 99, 132, 1)'
		                
		            ],
		            borderWidth: 1
		        }]
		    },
		});
	});
	
	}//extentReportAnalysis
	
	$scope.getTestCaseIdName=function(id)
	{
		$(".se-pre-con").fadeOut('slow');
		$scope.testID=id;
		$scope.reportManupulation();
	}
	$scope.reportManupulation=function()
	{
		$scope.reportVerify=$scope.reportData[$scope.testID][0].verify;
		$('.collection-item.test.displayed').removeClass('active');
		$('#'+$scope.reportVerify.applicationName+'_'+$scope.testID).addClass('active');

	}
	$scope.getExtentReportScreenshot=function(screenshotId)
	{
		autoReportService.getExtentReportScreenshot(screenshotId).then(function Success(response)
		{
			$(".se-pre-con").fadeOut('slow');
			$scope.screenshot=response.data.serviceResponse;
		});
	}
	
	$scope.showScreenshot=function(image)
	{
		if(image)
		{
			$scope.screenshotImage=image;
			$("#show-screenshot").modal('show');
		}
	}
	$scope.exportTestCases=function()
	{
		autoReportService.exportToExcel($scope.urnIdCurrent).then(function Success(response)
		{
//			$(".se-pre-con").fadeOut('slow');
//			$scope.screenshot=response.data;
			const url = window.URL.createObjectURL(response.data);
  			  const a = document.createElement('a');
  	 		 a.style.display = 'none';
   			 a.href = url;
   	 // the filename you want
    			a.download = 'An-1.xlsx';
  			  document.body.appendChild(a);
  			  a.click();
   		 window.URL.revokeObjectURL(url);
		});
		
	};
	
	$(".analysis.waves-effect").click(function(){
		  $(".analysis.waves-effect").removeClass("active");
		  $(this).addClass("active");
	  });


	$scope.sort=function(keyname){
		$scope.sortKey=keyname;
		$scope.reverse= !$scope.reverse;
		
	}
	
});