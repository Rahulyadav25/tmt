demoApp.controller("defectAutoController", function($scope,$interval,$state,$stateParams,$location,$anchorScroll, $rootScope, autoReportService) {
	
	$(document).ready(function(){
		checkcookie();
		var token=localStorage.getItem('tokenID');
		if (token==null|| token=="")
		{
		window.location.href = "../login.html";
		}
		});
	
	$rootScope.checkHome();
	
	$scope.projectName = localStorage.getItem('projectName');
	$scope.projectId = localStorage.getItem('projectId');
	var pName=$scope.projectName 
	
	$scope.status=null;
	$scope.getAutomationRunReport=function(){
		autoReportService.getDefectReport($scope.projectName).then(function Success(response)
		{
			$(".se-pre-con").fadeOut('slow');
            $scope.automationReport=response.data.serviceResponse;
            $("#automationReport").DataTable({
            	"order": [[ 0, "desc" ]],
                 dom: 'lBfrtip',
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                 buttons: [
                     {
                     extend: 'collection',
                     text: 'Export',
                     buttons: [
                         {
                             extend: "excelHtml5",
                             fileName:  "CustomFileName" + ".xlsx",
                             title:pName,
                             exportOptions: {
                                 columns: ':visible'
                             },
                             //CharSet: "utf8",
                             exportData: { decodeEntities: true }
                         },
                         {
                             extend: "csvHtml5",
                             fileName:  "CustomFileName" + ".csv",
                             title:pName,
                             exportOptions: {
                                 columns: ':visible'
                             },
                             exportData: { decodeEntities: true }
                         },
                         {
                             extend: "pdfHtml5",
                             fileName:  "CustomFileName" + ".pdf",
                             title:pName,
                             exportOptions: {
                                 columns: ':visible'
                             },
                             exportData: {decodeEntities:true}
                         }
                         ]
                    }
                 ],
            data: $scope.automationReport,
             columns:[
             {title:"Run Id"},
             {title:"Test Suite"},
             {title:"Start Time"},
             {title:"End Time"},
             {title:"Total Time"},
             {title:"Run Status"},
             {title:"Report"},
             

             ],
             "columnDefs": [{ "targets": -1, "data": null, "defaultContent": "<button id='btnDetails'  class='btn btn-primary btn-sm' title='action'>View bugs</button>"}]
         });
         var table=$("#automationReport").DataTable();
         $rootScope.autoReportData=[];
      	 $('#automationReport tbody').on('click', '[id*=btnDetails]', function ()
      	 {
      		 $scope.data = table.row($(this).parents('tr')).data();
      		 console.log($scope.data[0]);
      		 console.log($scope.data[1]);
      		
      		autoReportService.getAllDefectReport($scope.data[0],$scope.projectName).then(
					function Success(response)
					 {		
				     $scope.getAllBugs=response.data.serviceResponse;
				     console.log("$scope.getAllBugs"+$scope.getAllBugs);
				     $(".se-pre-con").hide();
				    
				     document.getElementById("divReport").style.display="none";
				     document.getElementById("divAllBugReport").style.display="block";
				     
				     $("#allBugReport").DataTable({
			            	"order": [[ 0, "desc" ]],
			                 dom: 'lBfrtip',
			                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			                 buttons: [
			                     {
			                     extend: 'collection',
			                     text: 'Export',
			                     buttons: [
			                         {
			                             extend: "excelHtml5",
			                             fileName:  "CustomFileName" + ".xlsx",
			                             title:pName,
			                             exportOptions: {
			                                 columns: ':visible'
			                             },
			                             //CharSet: "utf8",
			                             exportData: { decodeEntities: true }
			                         },
			                         {
			                             extend: "csvHtml5",
			                             fileName:  "CustomFileName" + ".csv",
			                             title:pName,
			                             exportOptions: {
			                                 columns: ':visible'
			                             },
			                             exportData: { decodeEntities: true }
			                         },
			                         {
			                             extend: "pdfHtml5",
			                             fileName:  "CustomFileName" + ".pdf",
			                             title:pName,
			                             exportOptions: {
			                                 columns: ':visible'
			                             },
			                             exportData: {decodeEntities:true}
			                         }
			                         ]
			                    }
			                 ],
			            data: $scope.getAllBugs,
			             columns:[
			             {title:"Run Id"},
			             {title:"ScenarioID"},
			             {title:"TestCaseID"},
			             {title:"Description"},
			             {title:"Response Time"},
			             {title:"Expected Result"},
			             {title:"Actual Result"},
			             {title:"Status"},
			             {title:"Bug Id"},
			             {title:"Execute"},
			          
			             

			             ],
			             "columnDefs": [{ "targets": -1, "data": null, "defaultContent": "<i id='btnDetailss'  class='fa fa-tasks table-ico' style='color: #34633f' title='action'></i>"}]
			         });
				     //
				     var table=$("#allBugReport").DataTable();
			         $rootScope.autoReportData=[];
			      	 $('#allBugReport tbody').on('click', '[id*=btnDetailss]', function ()
			      	 {
			      		 $scope.data = table.row($(this).parents('tr')).data();

			      		 console.log($scope.data[0]);
			      		$scope.runId = $scope.data[0];
			      		
			      		 console.log($scope.data[1]);
			      		$scope.scenarioId = $scope.data[1];

			      		console.log($scope.data[2]);
			      		$scope.TestCaseID = $scope.data[2];
			      		
			      		console.log($scope.data[3]);
			      		$scope.Description = $scope.data[3];
			      		
			      		console.log($scope.data[4]);
			      		$scope.Response_Time = $scope.data[4];
			      		
			      		console.log($scope.data[5]);
			      		$scope.Expected_Result = $scope.data[5];
			      		
			      		console.log($scope.data[6]);
			      		$scope.Actual_Result = $scope.data[6];
			      		
			      		console.log($scope.data[7]);
			      		$scope.statusdemo = $scope.data[7];
			      		$scope.status=="";
			      		if($scope.statusdemo == "FAIL" ||$scope.statusdemo == "Fail")
		      			{
			      			$scope.status="Fail";
		      			}
			      		else
			      			{
			      			$scope.status="Pass";
			      			}
			      		
			      		
			      		console.log($scope.data[8]);
			      		$scope.bug_id = $scope.data[8];
			      		
			      		console.log($scope.data[9]);
			      		$scope.BrowserType = $scope.data[9];
			      		
			      		console.log($scope.data[10]);
			      		//$scope.TestcaseType = $scope.data[10];
			      		if($scope.data[10] == null || $scope.data[10] == undefined )
			      			{
			      			$scope.TestcaseType = "Not defined";
			      			}
			      		else
			      			{
			      			$scope.TestcaseType = $scope.data[10];
			      			}
			      		console.log($scope.data[10]);
			      		//$scope.Step = $scope.data[11];
			      		if($scope.data[11] == null || $scope.data[11] == undefined )
		      			{
			      			$scope.Step = "Not defined";
		      			}
			      		else
		      			{
			      			$scope.Step = $scope.data[11];
		      			}
			      		console.log($scope.data[12]);
			      		$scope.scenarioDescription = $scope.data[12];
			      	//	document.getElementById("divAllBugReport").style.display="none";
			      		// document.getElementById("executeTestCase").style.display="block";
			      		console.log($scope.data[13]);
			      		$scope.id = $scope.data[13];
			      		
			      		console.log($scope.data[14]);
			      		$scope.bugToolId = $scope.data[14];
			      		
			      		$scope.checkFromAllBugs($scope.bug_id);
			      		const testcase = {
				      			id: $scope.id,
				      			actualResult: $scope.Actual_Result,
				      			expectedResult: $scope.Expected_Result,
				      			testCaseSrNo: $scope.TestCaseID,
				      			testDescription: $scope.Description,
				      			testCaseAction: "Completed",
				      			status: $scope.status,
				      			bugId: $scope.bug_id,
				      			bugPriority: "Medium",
				      			bugSeverity: "Major",
				      			executionTime: $scope.Response_Time,
				      			bugType:"1",
				      			bugToolId:$scope.bugToolId,
				      			browser:$scope.BrowserType,
				      			complexity:"Need to execute",
				      			functionality:"Functionality",
				      			groupName:$scope.scenarioId,
				      			scenarioID:$scope.scenarioId,
				      			scenarios:$scope.scenarioDescription,
				      			testCaseType:$scope.TestcaseType,
				      			testData:"Please enter test data",
				      			testerComment:"Please enter comment",
				      			testID:$scope.runId,
				      			steps:$scope.Step,
				      			priority:"Medium"
				      			
				      			};
			      		console.log(testcase);
			      		
			      		$scope.executeTestCase($scope.scenarioId,testcase);
			      		
			      		
			      			$("#executeTestCase").modal();
			      			$scope.switchdiv();
			      			$scope.attachFetching();
			      			$(".se-pre-con").hide();
			      			
			      		
			      		
			      	
			      	 });
				     //
				     
				     
				     
		              });	
      		
      		
      		 
      		
      		 
      		// $rootScope.autoReportData=$scope.data;
      		// $state.transitionTo('projectDetailsAuto.exereport');
      	 });
	       	 
		});
	}
	
	$scope.executeTestCase = function(testGroup,testcase) {
        $('.time').datetimepicker({
				format: 'HH:mm:ss'

				   });
        
        
        
		$scope.executeTestCaseStatus=null;
		$scope.tcCaseStatus=testcase.status;
		$scope.tcsAction=testcase.testCaseAction;
		$scope.testGroupName=testGroup;
		$scope.testCaseEdit=testcase;//chutiya giri  started
		$scope.testCaseEdit.id=testcase.id;
		$scope.testCaseEdit.bugPriority=testcase.bugPriority;
		$scope.testCaseEdit.bugSeverity=testcase.bugSeverity;
		//$scope.testCaseEdit.testcompexity=testcase.complexity;
		//$scope.testCaseEdit.testpriority=testcase.priority;
		if($scope.testCaseEdit.executionTime !=null)
		{
		if($scope.testCaseEdit.executionTime ==="00:00:00")
		{
		$scope.testExcute="00:00:00";
		}

		else if($scope.testCaseEdit.executionTime ===null){
		$scope.testExcute="00:00:00";
		}

		else{
		$scope.secnd=$scope.testCaseEdit.executionTime;
		$scope.seconds = parseInt($scope.secnd, 10); // don't forget the second param
		$scope.hours   = Math.floor($scope.seconds / 3600);
		$scope.minutes = Math.floor(($scope.seconds - ($scope.hours * 3600)) / 60);
		$scope.seconds = $scope.seconds - ($scope.hours * 3600) - ($scope.minutes * 60);

		            if ($scope.hours   < 10) {$scope.hours   = "0"+$scope.hours;}
		            if ($scope.minutes < 10) {$scope.minutes = "0"+$scope.minutes;}
		            if ($scope.seconds < 10) {$scope.seconds = "0"+$scope.seconds;}
		            $scope.testExcute   = $scope.hours+':'+$scope.minutes+':'+$scope.seconds;
		}
		}
		else{
		$scope.testExcute="00:00:00";
		}
	//	$scope.testCaseEdit.executionTime="00:00:00";
		$('#file').val('');
	
		if($scope.testCaseEdit.status=="Fail" ||$scope.testCaseEdit.status=="FAIL"){
			$scope.divTest=true;
			$scope.isFail=true;
			
			$scope.disableOpt=true;
			$scope.raiseReopen=false;
			
			/*if($scope.testCaseEdit.bugId){
				$scope.testCaseEdit.bugType=1;
				$scope.bugid=true;
				$scope.raiseNew=false;
			}
			else{
				$scope.testCaseEdit.bugType=0;
				$scope.bugid=false;
			}*/
			
		}
		
		if($scope.testCaseEdit.testCaseAction=="Completed")
		{
			$("#statusToDo").attr('selected', true);
			$scope.notcompleted=true;
		}
		else{
			$("#statusToDo").attr('selected', false);
			$scope.notcompleted=false;
		}
		if( $scope.testCaseEdit.status=="Blocked-Showstopper" ||$scope.testCaseEdit.status=="Blocked-Undelivered" || $scope.testCaseEdit.status=="Blocked-Test data")
		{
		//$scope.disabledStatus=true;
		//$scope.disableOpt=false;
		$scope.testCaseEdit.testCaseAction==="";
		$('#testCaseAction').val('');
		$("#testCaseAction option[value='']").attr("selected", "selected");
		//$scope.notcompleted=true;
		}
		
		//$scope.testCaseEdit.testCaseAction=$scope.dbAction;
		$scope.raiseReopen=false;
		$scope.raiseClose=false;
		$("#executeTestCase").modal();
		$scope.switchdiv();
		$scope.attachFetching();
		$(".se-pre-con").hide();
		
	}
	$scope.notcompleted=true; //pass-fail
	$scope.disableOpt=false; //action-completed
	$scope.disabledStatus=false; //new stats blocked-
	$scope.divTest=false; //bug-priority
	$scope.bugTypeEnable=true;//new/existing bug
	$scope.switchdiv = function() {
		
		if($scope.testCaseEdit.bugId){
			$scope.divTest=true;
			$scope.testCaseEdit.bugType=1;
			$scope.bugid=true;
			$scope.raiseNew=false;
		}
		else{
			if($scope.testCaseEdit.status=="Fail" ||$scope.testCaseEdit.status=="FAIL")
			{
				$scope.divTest=true;
				$scope.bugid=false;
				if($scope.testCaseEdit.bugToolId){
					$scope.bugTypeEnable=true;
					$scope.testCaseEdit.bugType="";
				}
				else{
					$scope.bugTypeEnable=false;
					$scope.testCaseEdit.bugType=0;
				}
			}
			else{
				$scope.divTest=false;
				$scope.testCaseEdit.bugType="";
				$scope.bugid=false;
			}
			
		}
		
		if($scope.testCaseEdit.status=="Fail" ||$scope.testCaseEdit.status=="FAIL")
		{
				//$scope.testCaseEdit.bugType=""; 
				//$scope.testCaseEdit.bugSeverity="";
				//$scope.testCaseEdit.bugPriority="";
			
			$('#testCaseAction').val('Completed');
		
			jQuery("select#testCaseAction option[value='Completed']").show();
			$("#testCaseAction option[value='Completed']").attr("selected", "selected");
		
		
			$scope.testCaseEdit.testCaseAction="Completed";
			$scope.disableOpt=true;
			$scope.disabledStatus=false;
			if(!$scope.testCaseEdit.bugId){
				$scope.divTest=true;
			}
		
		}
		else if($scope.testCaseEdit.status=="Pass" ||$scope.testCaseEdit.status=="PASS")	
		{
			//$scope.bugid=false;
			//$scope.raiseNew=false;
			//$scope.divTest=false;
			$('#testCaseAction').val('Completed');
		
			jQuery("select#testCaseAction option[value='Completed']").show();
			$("#testCaseAction option[value='Completed']").attr("selected", "selected");
		
			$scope.testCaseEdit.testCaseAction="Completed";
			$scope.disableOpt=true;
			$scope.disabledStatus=false;
		}
		else if($scope.testCaseEdit.testCaseAction=="Completed" && ($scope.testCaseEdit.status=="Fail" || $scope.testCaseEdit.status=="FAIL" ||$scope.testCaseEdit.status=="PASS" ||$scope.testCaseEdit.status=="Pass"))
		{
			//$scope.notcompleted=true;
			if(($scope.testCaseEdit.status=="Fail" ||$scope.testCaseEdit.status=="FAIL")&&(($scope.testCaseEdit.bugid==null) || ($scope.testCaseEdit.bugid=="")))
			{
				$scope.divTest=true;
			}
			//$scope.raiseNew=false;
			//$scope.bugid=false;
		}
		
		
		else if($scope.testCaseEdit.status==null ||$scope.testCaseEdit.status=="")
		{
			//$scope.bugid=false;
			$scope.raiseNew=false;
			//$scope.divTest=false;
			$scope.disableOpt=false;
			$scope.disabledStatus=false;
			$scope.notcompleted=true;
			//$scope.testCaseEdit.testCaseAction="Not Started";
			if($scope.testCaseEdit.testCaseAction=="Not Started" )
			{
				$scope.testCaseEdit.testCaseAction=="Not Started";
			}
			else if($scope.testCaseEdit.testCaseAction=="Under Review")
			{
				$scope.testCaseEdit.testCaseAction=="Under Review"
			}
			else if($scope.testCaseEdit.testCaseAction=="On Hold")
			{
				$scope.testCaseEdit.testCaseAction=="On Hold"
			}
			else if($scope.testCaseEdit.testCaseAction=="N/A")
			{
				$scope.testCaseEdit.testCaseAction=="N/A"
			}
			else{
				/*$('#testCaseAction').val('On Hold');
				$("#testCaseAction option[value='On Hold']").attr("selected", "selected");*/
				if($scope.testCaseEdit.testCaseAction=="Incomplete")
				{
						jQuery("select#testCaseAction option[value='Completed']").prop('disabled', false);
				}
				if($scope.testCaseEdit.testCaseAction=="Fail")
				{
						jQuery("select#testCaseAction option[value='Completed']").prop('disabled', false);
				}
				//$('#testCaseAction').val('On Hold');
				$scope.testCaseEdit.testCaseAction="On Hold";
				$("#testCaseAction option[value='On Hold']").attr("selected", "selected");
			}
		}
		else if( $scope.testCaseEdit.status=="Blocked-Showstopper" ||$scope.testCaseEdit.status=="Blocked-Undelivered" || 
				$scope.testCaseEdit.status=="Blocked-Testdata" || $scope.testCaseEdit.status=="Blocked-FSD not clear" || $scope.testCaseEdit.status=="On Hold"
				)
			{
			$scope.disabledStatus=true;
			$scope.disableOpt=false;
			$scope.raiseNew=false;
			$scope.bugTypeEnable=false;
			$scope.divTest=false;
			
			
			$('#testCaseAction').val("On Hold");
			
			jQuery("select#testCaseAction option[value='On Hold']").show();
			$("#testCaseAction option[value='On Hold']").attr("selected", "selected");
		
			$scope.testCaseEdit.testCaseAction='On Hold';	
			}
		else if($scope.testCaseEdit.status=="NE" )
			{$scope.disabledStatus=true;
			$scope.disableOpt=false;
			$scope.raiseNew=false;
			$scope.bugTypeEnable=false;
			$scope.divTest=false;
			
			
			$('#testCaseAction').val("Not Started");
			
			jQuery("select#testCaseAction option[value='Not Started']").show();
			$("#testCaseAction option[value='Not Started']").attr("selected", "selected");
		
			$scope.testCaseEdit.testCaseAction='Not Started';	
				
			}
		else if($scope.testCaseEdit.status=="Deferred" || $scope.testCaseEdit.status=="NA"
			 || $scope.testCaseEdit.status=="Duplicate" || $scope.testCaseEdit.status=="Out of Scope" )
			{
			$scope.disabledStatus=true;
			$scope.disableOpt=false;
			$scope.raiseNew=false;
			$scope.bugTypeEnable=false;
			$scope.divTest=false;
			
			
			$('#testCaseAction').val("N/A");
			
			jQuery("select#testCaseAction option[value='N/A']").show();
			$("#testCaseAction option[value='N/A']").attr("selected", "selected");
		
			$scope.testCaseEdit.testCaseAction='N/A';
			}
		else if($scope.testCaseEdit.status=="Partially Pass"  )
			{
//				alert("partially passs seleced");
			$scope.disabledStatus=true;
			$scope.disableOpt=false;
			$scope.raiseNew=false;
			$scope.bugTypeEnable=false;
			$scope.divTest=false;
			
			
			$('#testCaseAction').val("Incomplete");
			
			jQuery("select#testCaseAction option[value='Incomplete']").show();
			$("#testCaseAction option[value='Incomplete']").attr("selected", "selected");
		
			$scope.testCaseEdit.testCaseAction='Incomplete';
			}
		
		else
		{
			$scope.notcompleted=true;
			// $scope.raiseNew=false;
			//$scope.bugid=false;
			$scope.divTest=false;
			$scope.disableOpt=false;
			$scope.disabledStatus=false;
		}
		
		$scope.sss=null;
		$scope.checkFromAllBugs($scope.testCaseEdit.bugId);
		$("#executeTestCase").modal();
		
		
	
	}	
	$scope.ss=[];
	$scope.fileSS=function(){/*
		function readURL(input) {
			  if (input.files && input.files[0]) {
			    var reader = new FileReader();
			    
			    reader.onload = function(e) {
			      $('#blah1').attr('src', e.target.result);
			      $scope.sss=$('#file').val();
			     
			    }
			    
			    reader.readAsDataURL(input.files[0]);
			  }
			}

			$("#file").change(function() {
			  readURL(this);
			});
	*/
		
		var files = document.getElementById("file").files;
		$scope.ss1=[];
	
        for (var i = 0; i < files.length; i++) {     
        	var reader = new FileReader();
        	var j=0;
        	 if (files[i]) {
        		
                 reader.onload = function (e) {
                	                		 var img = $('<div><img id="dynamic'+j+'" class="screenshot" width="193" height="130" style="margin-top: 10%;float: left; width: 33.33%;padding: 5px;"></div>');
                         img.appendTo('.form-group1 ');  
                         $("#dynamic"+j).attr('src', e.target.result);
                         j=j+1;
                         
                 }
                 reader.readAsDataURL(files[i]);
                }
     
		}
       

        
	}
	function base64ToArrayBuffer(base64) {
	    var binaryString = window.atob(base64);
	    var binaryLen = binaryString.length;
	    var bytes = new Uint8Array(binaryLen);
	    for (var i = 0; i < binaryLen; i++) {
	       var ascii = binaryString.charCodeAt(i);
	       bytes[i] = ascii;
	    }
	    return bytes;
	 }
	function saveByteArray(reportName, byte) {
	    var blob = new Blob([byte], {type: "application/png"});
	   /* var link = document.createElement('a');
	    link.href = window.URL.createObjectURL(blob);
	    
	    link.download = fileName;
	    link.click();*/
	    
	    var fileName = reportName;
	   
		 var a = document.createElement("a");
		 var URL =window.location.href;
		 var downloadUrl='data:image/png,'+byte;
		    var currentdate = new Date();
		    var datetime =" "+currentdate.getDate() + "/"
		                + (currentdate.getMonth()+1)  + "/"
		                + currentdate.getFullYear() + "-"
		                + currentdate.getHours() + ":"
		                + currentdate.getMinutes() + ":"
		                + currentdate.getSeconds();
		
		                    a.href = downloadUrl;
		                    a.download = fileName+'_'+datetime+'.png';
//		                    document.body.appendChild(a);
		                    a.click();
//		                    document.body.removeChild(a);
	};
	
	$scope.clickshowImg=function(id)
	{
		autoReportService.attchmentDownload(id).then(function Success(response) { 
			$(".se-pre-con").hide();
		});
		
	}
	
	 $scope.deleteScreenshots=function(testcaseid,attachemntname)
	 {
		 autoReportService.deleteScreenshot(testcaseid,attachemntname).then(
					function Success(response) {
						$scope.ss=response.data.serviceResponse;
						$(".se-pre-con").hide();
					});
	 }
	 
	 $scope.confirmRaiseIssue=function()
	 {
		 var bugtype=$("#isNewIssue").val();
		 var bugsseverity=$("#bugSeverity").val();
		 var bugids=$("#bugid").val();
		 var bugPrority=$("#bugPriority").val();
		 if(bugtype ==null || bugtype=="" || bugsseverity ===null || bugsseverity=="" || bugPrority ==null || bugPrority=="")
			 {
			 if(bugtype ==null || bugtype==""){
			 	alert("Kindly select bug type");
			 }
			 else if(bugsseverity ==null || bugsseverity =="")
			 {
				alert("Kindly select Bug Severity!!");
			 }
			 else if(bugPrority ==null || bugPrority=="")
				 {
				 alert("Kindly select Bug Priority!!");
				 }
			 }
		 else{
			 if(bugtype=='1')
				 {
				 	if(bugids ==null || bugids=="" || bugids==undefined){
				 		alert("Kindly select a Bug ID!!");
				 	}
				 	else{
						 $("#confirmation-dialog-raise-issue").modal();

				 	}				 	
				 }
			 else{
				 $("#confirmation-dialog-raise-issue").modal();

			 }
		 }
	 }
	
	 $scope.reopenBugzillaIssueConfirmModal=function(){
	 	$scope.tempBugStatus="Reopen";
		localStorage.setItem('tempBugStatus', $scope.tempBugStatus);
	 	$scope.showDialogGroup(true);
	     $scope.confirmationDialogConfig = {
	       title: "Alert!!!",
	       message: "Do you want to reopen this issue ?",
	       buttons: [{
	         label: "Yes",
	         action: "addCommentToBugzillaBugModal"
	       }]
	     };
	 }
	 $scope.saveExecution = function() {
			$scope.ss="";
			// alert("set time :"+time);
			 var bugtype=$("#isNewIssue").val();
			 var bugsseverity=$("#bugSeverity").val();
			 var bugids=$("#bugid").val();
			 var bugPrority=$("#bugPriority").val();
			 
			    var actualResult=$scope.testCaseEdit.actualResult;
				var	expectedResult=	$scope.testCaseEdit.expectedResult;
				var	testCaseSrNo=$scope.testCaseEdit.testCaseSrNo;
				var	testDescription=$scope.testCaseEdit.testDescription;
				var	testCaseAction=$scope.testCaseEdit.testCaseAction;
				var	status=$scope.testCaseEdit.status;
				var	bugId=$scope.testCaseEdit.bugId;
				var	bugPriority=$scope.testCaseEdit.bugPriority;
				var	bugSeverity=$scope.testCaseEdit.bugSeverity;
				var	executionTime=$scope.testCaseEdit.executionTime;
				var	bugType=$scope.testCaseEdit.bugType;
				var	bugToolId=$scope.testCaseEdit.bugToolId;
				var	browser=$scope.testCaseEdit.browser;
				var	complexity=$scope.testCaseEdit.complexity;
				var	functionality=$scope.testCaseEdit.functionality;
				var	groupName=$scope.testCaseEdit.groupName;
				var	scenarioID=$scope.testCaseEdit.scenarioID;
				var	scenarios=$scope.testCaseEdit.scenarios;
				var	testCaseType=$scope.testCaseEdit.testCaseType;
				var	testData=$scope.testCaseEdit.testData;
				var	testerComment=$scope.testCaseEdit.testerComment;
				var	testID=$scope.testCaseEdit.testID;
				var	steps=$scope.testCaseEdit.steps;
				var	priority=$scope.testCaseEdit.priority;
				$scope.tempBugStatus = localStorage.getItem('tempBugStatus');
				var bugStatus=$scope.tempBugStatus;	
	      		var apiBugSummary = $scope.apiBugSummary;
	      		var bugToolName = $scope.bugToolName;
	      		var projectId = $scope.projectId;
	      		var projectName = $scope.projectName
			 
			/* if(($scope.testCaseEdit.bugToolId)&&(bugtype==undefined)&&($scope.testCaseEdit.status =='Fail')){
				 bugtype="0";
			 }*/
			 
		//if($scope.testCaseEdit.status =='Fail' && $scope.testCaseEdit.testCaseAction=='Completed')
		if($scope.testCaseEdit.testCaseAction=='Completed' && ($scope.testCaseEdit.status =='Fail'||$scope.testCaseEdit.status =='FAIL'))
		{
			if(bugtype ==null || bugtype=="" || bugsseverity ===null || bugsseverity=="" || bugPrority ==null || bugPrority=="")
			{
				if(bugtype ==null || bugtype==""){
				 	alert("Kindly select bug type");
				 	return false;
				}
				else if(bugtype=='1')
				{
					 if(bugids ==null || bugids=="" || bugids==undefined){
					 	alert("Kindly select a Bug ID!!");
						 return false;

					 }
					 else if(bugsseverity ==null || bugsseverity =="")
					 {
						alert("Kindly select Bug Severity!!");
					 	return false;

					 }
					 else if(bugPrority ==null || bugPrority=="")
						 {
						 alert("Kindly select Bug Priority!!");
						 	return false;
						 }
				}
				 else{
					 if(bugsseverity ==null || bugsseverity =="")
					 {
						alert("Kindly select Bug Severity!!");
					 	return false;

					 }
					 else if(bugPrority ==null || bugPrority=="")
						 {
						 alert("Kindly select Bug Priority!!");
						 	return false;
						 }

				 }
			  }
			  else{

					var hrs = $("#exetime").val().split(':')[0];
					var min = $("#exetime").val().split(':')[1];
					var sec =$("#exetime").val().split(':')[2];
					var exetime=(+hrs) * 60 * 60 + (+min) * 60 + (+sec); 
					
					//$scope.testCaseEdit.testCaseAction=$scope.modalAction;
					$scope.testCaseEdit.executionTime=parseInt(exetime);
					
					if( $scope.testCaseEdit.status=="Blocked-Showstopper" ||$scope.testCaseEdit.status=="Blocked-Undelivered" || $scope.testCaseEdit.status=="Blocked-Testdata"
						|| $scope.testCaseEdit.status=="NA" || $scope.testCaseEdit.status=="Blocked-FSD not clear" 
							|| $scope.testCaseEdit.status=="Deferred" || $scope.testCaseEdit.status=="NE"
								|| $scope.testCaseEdit.status=="On Hold" || $scope.testCaseEdit.status=="Duplicate" || $scope.testCaseEdit.status=="Out of Scope" 
									|| $scope.testCaseEdit.status=="Partially Pass")
					{
					
						$scope.testCaseEdit.testCaseAction=='';
						//localStorage.setItem(' ',$scope.testCaseEdit.testCaseAction)
						//$("#testCaseAction").val(' ');
						//$("#testCaseAction option[value='']").attr("selected", "selected");
						//$scope.testCaseEdit.testCaseAction=localStorage.getItem(' ');
						//$scope.testCaseEdit.testCaseAction=$("#testCaseAction").val(' ');
						$('#testCaseAction').attr('value', '');
						$('#testAction').attr('value', '');
						

					}
					
				
	      	
//					alert("from todoService.saveExecution:: $scope.testCaseEdit::  "+$scope.testCaseEdit);
					autoReportService.saveExecution(actualResult,expectedResult,testCaseSrNo,testDescription,
							testCaseAction,status,bugId,bugPriority,bugSeverity,executionTime,
							bugType,bugToolId,browser,complexity,functionality,groupName,scenarioID,scenarios,testCaseType,
							testData,testerComment,testID,steps,priority,bugStatus,apiBugSummary,bugToolName,projectId,projectName).then(function Success(response) 
					 {
									
									$scope.executeTestCaseStatus=response.data.serviceResponse;
//									$scope.getTodoRunList();
									var data = new FormData();
									
									var filesInput = document.getElementById("file").files;
									for(var index = 0; index < filesInput.length; index++)
									{
										data.append("file", filesInput[index]);
									}
									data.append('file',filesInput);
									data.append('testCaseID',$scope.testCaseEdit.id);
									data.append('uploadedBy',localStorage.getItem('userid'));
									if((($scope.executeTestCaseStatus[0]=="Execution Data has been Updated and Bug Has been Updated in your existing Bug")
											||($scope.executeTestCaseStatus[0]=="Execution Data has been Updated and bug has been raised in Bugzilla") ||
											($scope.executeTestCaseStatus[0]=="Execution Data has been Updated and bug has been raised in Jira")||
											($scope.executeTestCaseStatus[0]=="Execution Data has been Updated and bug has been raised in Mantis")||
											($scope.executeTestCaseStatus[0].includes("Execution Data has been Updated"))
											||(($scope.executeTestCaseStatus[0]=="Execution data has been updated and Bug has been reopened")))&&
											($scope.executeTestCaseStatus[1]))
									{
										data.append('projectId', localStorage.getItem('projectId'));
										//$scope.getTodoRunList();
									}
									else
									{
										data.append('projectId', '0');
									}
									
									 /*if(response.data.serviceStatus==="200")
										{
											alert("Execution data has been updated");
											$("#executeTestCase").modal('hide');
											$(".se-pre-con").hide();
										}
										else{
										$scope.executeTestCaseStatus=response.data.serviceResponse;
										alert($scope.executeTestCaseStatus);
													}*/
									 $(".se-pre-con").hide();
									if(typeof filesInput !== 'undefined' && ($('#file').val()!==null))
									{
										if(filesInput[0].size<=104857600)
										{
											autoReportService.saveAttachment(data).then(function Success(response) { 
											alert("Attachment uploaded successfully ", $scope.testCaseEdit.toolName, $scope.bugToolName);
//											alert(response.toString());
												$(".se-pre-con").hide();
												data=null;
											});
										}
										else
										{
											$(".se-pre-con").hide();
											alert('Kindly provide screenshots within 10MB !!!');
										}
									}
//							$scope.getTestCaseForExec($scope.testExecutionRun);
						});
					 
					 			//$scope.getTodoRunList();
			  }
			  
			}
		else{
			if(($scope.testCaseEdit.status=="Fail"||$scope.testCaseEdit.status=="FAIL")&&((!bugsseverity)||(!bugPrority))){
				if(bugsseverity ==null || bugsseverity =="")
				 {
					alert("Kindly select Bug Severity!!");
				 	return false;

				 }
				 else if(bugPrority ==null || bugPrority=="")
				{
					 alert("Kindly select Bug Priority!!");
					 	return false;
				}
			}
			else{
				var hrs = $("#exetime").val().split(':')[0];
				var min = $("#exetime").val().split(':')[1];
				var sec =$("#exetime").val().split(':')[2];
				var exetime=(+hrs) * 60 * 60 + (+min) * 60 + (+sec); 
				
				//$scope.testCaseEdit.testCaseAction=$scope.modalAction;
				$scope.testCaseEdit.executionTime=parseInt(exetime);
				
				$scope.executeTestCaseStatus=[];
				if( $scope.testCaseEdit.status=="Blocked-Showstopper" ||$scope.testCaseEdit.status=="Blocked-Undelivered" || $scope.testCaseEdit.status=="Blocked-Testdata"
					|| $scope.testCaseEdit.status=="NA")
				{
				
				$scope.testCaseEdit.testCaseAction=="";
				//localStorage.setItem(' ',$scope.testCaseEdit.testCaseAction)
				//$("#testCaseAction").val(' ');
				
				//$scope.testCaseEdit.testCaseAction=localStorage.getItem(' ');
				//$scope.testCaseEdit.testCaseAction=$("#testCaseAction").val(' ');
				$('#testCaseAction').attr('value', '');
				}
				
				
				autoReportService.saveExecution(actualResult,expectedResult,testCaseSrNo,testDescription,
						testCaseAction,status,bugId,bugPriority,bugSeverity,executionTime,
						bugType,bugToolId,browser,complexity,functionality,groupName,scenarioID,scenarios,testCaseType,
						testData,testerComment,testID,steps,priority,bugStatus,apiBugSummary,bugToolName,projectId,projectName).then(function Success(response) 
				 {
								
					$scope.executeTestCaseStatus=response.data.serviceResponse;
				//	$scope.getTodoRunList();pranik
					var data = new FormData();
								
					var filesInput = document.getElementById("file").files;
					for(var index = 0; index < filesInput.length; index++)
					{
						data.append("file", filesInput[index]);
					}
					data.append('file',filesInput);
					data.append('testCaseID',$scope.testCaseEdit.id);
					data.append('uploadedBy',localStorage.getItem('userid'));
					if((($scope.executeTestCaseStatus[0]=="Execution Data has been Updated and Bug Has been Updated in your existing Bug")
										||($scope.executeTestCaseStatus[0]=="Execution Data has been Updated and bug has been raised in Bugzilla") ||
										($scope.executeTestCaseStatus[0]=="Execution Data has been Updated and bug has been raised in Jira")||
										($scope.executeTestCaseStatus[0]=="Execution Data has been Updated and bug has been raised in Mantis")||
										($scope.executeTestCaseStatus[0].includes("Execution Data has been Updated"))
										||($scope.executeTestCaseStatus[0]=="Execution data has been updated and Bug has been reopened"))&&
										($scope.executeTestCaseStatus[1]))
					{
						data.append('projectId', localStorage.getItem('projectId'));
					//	$scope.getTodoRunList();
					}
					else
					{
						data.append('projectId', '0');
					}
								
					/*if(response.data.serviceStatus==="200")
					{
						alert("Execution data has been updated");
						$("#executeTestCase").modal('hide');
						$(".se-pre-con").hide();
					}
					else{
						$scope.executeTestCaseStatus=response.data.serviceResponse;
						alert($scope.executeTestCaseStatus);
					}*/
					$(".se-pre-con").hide();
					if(typeof filesInput !== 'undefined' && ($('#file').val()!==null))
					{
						if(filesInput[0].size<=104857600)
						{
							alert("Attachment uploaded successfully ", $scope.testCaseEdit.toolName, $scope.bugToolName);
							autoReportService.saveAttachment(data).then(function Success(response) { 
								$(".se-pre-con").hide();
								data=null;
							});
						}
						else{
							$(".se-pre-con").hide();
							alert('Kindly provide screenshots within 10MB !!!');
						}
					}
//					$scope.getTestCaseForExec($scope.testExecutionRun);
				});
				 
			//	$scope.getTodoRunList();pranik
			}
			
			
		}
	      		localStorage.removeItem("tempBugStatus");
	};
	
	$scope.viewFulltodoList =function()
	{$("#view-all-bugs").modal();
		//$("#todolistShowss").modal();
	}
	
	$scope.clearTodo=function()
	{
	     //$("#dynamic1").val('');
	     $(".screenshot").css("display","unset");
	     
	     window.location.reload();
//	     $scope.getTestCaseForExec($scope.testExecutionRun);
	    // $scope.testCaseEdit.status=$scope.tcCaseStatus;
	     //$scope.testCaseEdit.testCaseAction= $scope.tcsAction;
		/*$(".screenshot").val("");*/
	}
	
//	$scope.attachFetching=function()
//	{
//		//alert("testCaseEdit.testID.."+$scope.testCaseEdit.testCaseSrNo);
//		/*todoService.attchmentFetch($scope.testCaseEdit.testCaseSrNo).then(function Success(response) { */
//		autoReportService.attchmentFetch($scope.testCaseEdit.id).then(function Success(response) { 
//		//alert('Kindly provide screenshots within 10MB !!!'+response.data.serviceResponse);
//		$scope.ss=response.data.serviceResponse;
//		console.log(response.data.serviceResponse);
//		//alert("$scope.ss.."+$scope.ss);
//	});
//	}
	
//	/added later
	$scope.attachFetching=function()
	{
		//alert("testCaseEdit.testID.."+$scope.testCaseEdit.testCaseSrNo);
		/*todoService.attchmentFetch($scope.testCaseEdit.testCaseSrNo).then(function Success(response) { */
		autoReportService.attchmentFetchAuto($scope.testCaseEdit.id,$scope.testCaseEdit.bugId)
		.then(function Success(response) { 
		$scope.ss=response.data.serviceResponse;
		console.log(response.data.serviceResponse);
		//alert("$scope.ss.."+$scope.ss);
	});
	}
	
	$scope.getColorCode=function(status){
		if(status=='Pass'){
			return 'green';
		}else{
			return 'red';
		}
	}
$scope.getColorCodeAction=function(status){
		
		$scope.tcActions=status;
		if(status=='Not Started'){
			return 'grey';
		}else if(status=='UnderReview'){
			return 'orange';
		}
		else if(status=='Completed' || status=="Partially Pass"){
			return 'green';
		}else if(status=='')
			return 'skyblue';
		else{
			return 'red';
		}
	}
$scope.getColorCodeAction=function(status){
	
	$scope.tcActions=status;
	if(status=='Not Started'){
		return 'grey';
	}else if(status=='UnderReview'){
		return 'orange';
	}
	else if(status=='Completed' || status=="Partially Pass"){
		return 'green';
	}else if(status=='')
		return 'skyblue';
	else{
		return 'red';
	}
}
$scope.clearFiles= function(){
	document.getElementById("file").value = "";
	$('#showsCross').fadeOut('slow');
}
$scope.clearFiles();

$scope.checkIsNew = function() {
	var isnewissue=$("#isNewIssue").val();
	if(isnewissue=="0"){
		$scope.bugid=false;
		$scope.raiseNew=true;
	}
	if(isnewissue=="1"){
		$scope.raiseNew=false;
		$scope.bugid=true;
	}
	if(isnewissue==null){
		$scope.raiseNew=false;
		$scope.bugid=false;
		}
	}
$scope.openViewAllBugsModal = function(){
	$scope.allillaBugsProductWise=[];
	autoReportService.getBugzillaBugsProductWise($scope.projectName,$scope.projectId).then(function Success(response) {
		
		$(".se-pre-con").hide();
		$scope.bugToolName=response.data.serviceResponse[0]
		if(response.data.serviceResponse[0]==="Bugzilla"){
			let allBugs=response.data.serviceResponse[1].serviceResponse.bugs;
			let allBugsLength=allBugs.length;
			for(let b=0;b<allBugsLength;b++){
				if(allBugs[b].product===$scope.projectName){
					$scope.allBugzillaBugsProductWise.push(allBugs[b]);
				}
			}
			$scope.bugtoolIP=response.data.serviceResponse[2].bugToolURL;
			$("#view-all-bugs").modal();
		}
		else if(response.data.serviceResponse[0]==="Mantis"){
			$scope.mantisProjectBugs=response.data.serviceResponse[1].serviceResponse.issues;
			$scope.bugtoolIP=response.data.serviceResponse[2].bugToolURL;
			$("#view-all-bugs-mantis").modal();
		}			 
		else if(response.data.serviceResponse[0]==="Jira"){
//			$scope.jiraProjectBugs=response.data.serviceResponse[1].serviceResponse.myArrayList;
			$scope.jiraProjectBugs=response.data.serviceResponse[1].serviceResponse;
			$scope.bugtoolIP=response.data.serviceResponse[2].bugToolURL;
			$("#view-all-bugs-jira").modal();
		}			 
				 
	});
	
}

$scope.bugDescriptionReadMore=function(bugID){
	$scope.url = $scope.bugtoolIP+'/view.php?id='+bugID;
}
	
$scope.checkFromAllBugs = function(userEnteredBugID){
		
		if(userEnteredBugID){
			autoReportService.getBugzillaBugsProductWise($scope.projectName,$scope.projectId).then(function Success(response) {
				$(".se-pre-con").hide();
				if(response.data.serviceError!="failed"){
					$scope.bugToolName=response.data.serviceResponse[0];
					
					//just to use later using reopen feature
					$scope.bugToolNameForProj=response.data.serviceResponse[0];
					
					
					if(response.data.serviceResponse[0]==="Bugzilla"){
						allBugzillaBugs=response.data.serviceResponse[1].serviceResponse.bugs;
						
						let allBugzillaBugsLen=allBugzillaBugs.length;
						let existBugCount=0;
						for(let bu=0;bu<allBugzillaBugsLen;bu++){
							if(allBugzillaBugs[bu].id==userEnteredBugID){
								existBugCount=1;
								if(allBugzillaBugs[bu].product!=$scope.projectName){
									alert("This Bug ID exists in Bugzilla but for another project. Kindly raise a new bug for this project...");
									$scope.testCaseEdit.bugId=null;
								}
								else{
									$scope.bugIsOpen=allBugzillaBugs[bu].is_open;
									$scope.apiBugStatus=allBugzillaBugs[bu].status;
									$scope.apiBugResolution=allBugzillaBugs[bu].resolution;
									$scope.testCaseEdit.bugSeverity=jsUcfirst(allBugzillaBugs[bu].severity);
									$scope.testCaseEdit.bugPriority=jsUcfirst(allBugzillaBugs[bu].priority);
								}
							}
						}
						if(existBugCount===0){
							alert("This Bug ID does not exist in Bugzilla. Kindly raise a new bug for this project...");
							$scope.testCaseEdit.bugId=null;
						}
						
						if(($scope.testCaseEdit.bugId) && ($scope.apiBugStatus=="RESOLVED") && ($scope.apiBugResolution=="FIXED") && 
								($scope.testCaseEdit.status=="Pass"||$scope.testCaseEdit.status=="PASS") && ($scope.testCaseEdit.testCaseAction=="Completed")){
							$scope.raiseClose=true;
						}
						else{
							$scope.raiseClose=false;
						}
						
						if(($scope.testCaseEdit.bugId) && (($scope.apiBugStatus=="RESOLVED") || ($scope.apiBugStatus=="VERIFIED")) && 
								($scope.apiBugResolution=="FIXED") && ($scope.testCaseEdit.status=="Fail"||$scope.testCaseEdit.status=="FAIL") && 
								($scope.testCaseEdit.testCaseAction=="Completed")){
							$scope.raiseReopen=true;
						}
						else{
							$scope.raiseReopen=false;
						}
					}//if
					else if(response.data.serviceResponse[0]==="Mantis") {
						allBugzillaBugs=response.data.serviceResponse[1].serviceResponse.issues;
						let existBugCount=0;
						let allBugzillaBugsLen=allBugzillaBugs.length;
						for(let bug=0;bug<allBugzillaBugsLen;bug++){
							if(allBugzillaBugs[bug].id==userEnteredBugID){
								existBugCount=1;
								$scope.apiBugStatus=allBugzillaBugs[bug].status.name;
								$scope.apiBugResolution=allBugzillaBugs[bug].resolution.name;
								if(allBugzillaBugs[bug].severity.name=="crash"){
									$scope.testCaseEdit.bugSeverity="Critical";
								}
								else if(allBugzillaBugs[bug].severity.name=="block"){
									$scope.testCaseEdit.bugSeverity="Blocker";
								}
								else{
									$scope.testCaseEdit.bugSeverity=jsUcfirst(allBugzillaBugs[bug].severity.name);
								}
								$scope.testCaseEdit.bugPriority=jsUcfirst(allBugzillaBugs[bug].priority.name);
								break;
							}
						}
						
						if(existBugCount===0){
							alert("This Bug ID does not exist for this project or has not been raised in Mantis!! Kindly raise a new bug for this project..");
							$scope.testCaseEdit.bugId=null;
						}
						
						if(($scope.testCaseEdit.bugId) && ($scope.apiBugStatus=="resolved") && ($scope.apiBugResolution=="fixed") && 
								($scope.testCaseEdit.status=="Pass") && ($scope.testCaseEdit.testCaseAction=="Completed")){
							$scope.raiseClose=true;
						}
						else{
							$scope.raiseClose=false;
						}
						
						if(($scope.testCaseEdit.bugId) && (($scope.apiBugStatus=="resolved") || ($scope.apiBugStatus=="closed")) && 
								($scope.apiBugResolution=="fixed") && ($scope.testCaseEdit.status=="Fail"||$scope.testCaseEdit.status=="FAIL") && 
								($scope.testCaseEdit.testCaseAction=="Completed")){
							$scope.raiseReopen=true;
						}
						else{
							$scope.raiseReopen=false;
						}
					}//else if mantis
					else if(response.data.serviceResponse[0]==="Jira") {
//						/*
//						allBugzillaBugs=response.data.serviceResponse[1].serviceResponse.issues;
//						allBugzillaBugs=response.data.serviceResponse[1].serviceResponse.myArrayList;
						allBugzillaBugs=response.data.serviceResponse[1].serviceResponse;
						let existBugCount=0;
						let allBugzillaBugsLen=allBugzillaBugs.length;
						for(let bug=0;bug<allBugzillaBugsLen;bug++){
							if(allBugzillaBugs[bug].bugId==userEnteredBugID){
								existBugCount=1; 
								$scope.apiBugStatus=allBugzillaBugs[bug].status;
								$scope.apiBugSummary=allBugzillaBugs[bug].summary;
								$scope.apiBugPriority=allBugzillaBugs[bug].priority;
//								$scope.apiBugResolution=allBugzillaBugs[bug].resolution.name;
//								$scope.testCaseEdit.bugPriority=jsUcfirst(allBugzillaBugs[bug].priority.name);
								break;
							}
						}//for all bugs
						
						if(existBugCount===0){
							alert("This Bug ID does not exist for this project or has not been raised in Jira!! Kindly raise a new bug for this project..");
							$scope.testCaseEdit.bugId=null;
						}
						
						if(($scope.testCaseEdit.bugId) && (($scope.apiBugStatus=="To Do") || ($scope.apiBugStatus=="Open")) &&
								($scope.testCaseEdit.status=="Pass") 
										){
									$scope.raiseClose=true;
									$scope.raiseReopen=false;
								}
								
								else if(($scope.testCaseEdit.bugId) && (($scope.apiBugStatus=="Done") || ($scope.apiBugStatus=="Close"))  &&
								($scope.testCaseEdit.status=="Fail") 
										){
									$scope.raiseClose=false;
									$scope.raiseReopen=true;
								}
								
								else{
									$scope.raiseClose=false;
									$scope.raiseReopen=false;
								}
						
//				*/
					}//else if
				}
				
			});
		}
				
	}

	
$scope.uncheckBug=function(bugTool){
	if(bugTool=="Mantis"){
		$("input[name='mantisBugsProjectWise']").prop("checked", false);
	}
	if(bugTool=="Jira"){
		$("input[name='jiraBugsProjectWise']").prop("checked", false);
	}
}
$scope.closeViewAllBugsModal = function(bugtool){
	$scope.bugToolName=bugtool;
	
	if(bugtool==="Bugzilla"){
		let bugzillaBugId=[];
		$("#viewAllBugs input[name=bugzillaBugsProductWise]:checked").closest('tr').find("td").each(function(){
			bugzillaBugId.push($(this).text());
		});
		$scope.testCaseEdit.bugSeverity=jsUcfirst(bugzillaBugId[3]);
		$scope.testCaseEdit.bugPriority=jsUcfirst(bugzillaBugId[4]);
		if($scope.testCaseEdit.bugId){
			/*if(bugzillaBugId[9]=="true"){
				$scope.raiseReopen=false;
			}
			else{
				$scope.raiseReopen=true;
			}*/
			if((bugzillaBugId[9]=="RESOLVED") && (bugzillaBugId[10]=="FIXED") && 
					($scope.testCaseEdit.status=="Pass"||$scope.testCaseEdit.status=="PASS") && ($scope.testCaseEdit.testCaseAction=="Completed")){
				$scope.raiseClose=true;
			}
			else{
				$scope.raiseClose=false;
			}
			
			if(((bugzillaBugId[9]=="RESOLVED") || (bugzillaBugId[9]=="VERIFIED")) && (bugzillaBugId[10]=="FIXED") && 
					($scope.testCaseEdit.status=="Fail"||$scope.testCaseEdit.status=="FAIL") && ($scope.testCaseEdit.testCaseAction=="Completed")){
				$scope.raiseReopen=true;
			}
			else{
				$scope.raiseReopen=false;
			}
		}
		$("#view-all-bugs").modal('hide');
	}
	else if(bugtool==="Mantis"){
		if (!$("#viewAllBugsMantis input[name=mantisBugsProjectWise]:checked").val()) {
			alert('Kindly select a Bug to be linked!!');
	        return false;
		}
		else{
		       
			let mantisBugId=[];
			$("#viewAllBugsMantis input[name=mantisBugsProjectWise]:checked").closest('tr').find("td").each(function(){
				mantisBugId.push($(this).text());
			});
			$scope.testCaseEdit.bugId=mantisBugId[1];
			if(mantisBugId[4]=="crash"){
				$scope.testCaseEdit.bugSeverity="Critical";
			}
			else if(mantisBugId[4]=="block"){
				$scope.testCaseEdit.bugSeverity="Blocker";
			}
			else{
				$scope.testCaseEdit.bugSeverity=jsUcfirst(mantisBugId[4]);
			}			
			$scope.testCaseEdit.bugPriority=jsUcfirst(mantisBugId[5]);
			if($scope.testCaseEdit.bugId){
				if((mantisBugId[7]=="resolved") && (mantisBugId[8]=="fixed") && 
						($scope.testCaseEdit.status=="Pass"||$scope.testCaseEdit.status=="PASS") && ($scope.testCaseEdit.testCaseAction=="Completed")){
					$scope.raiseClose=true;
				}
				else{
					$scope.raiseClose=false;
				}
				
				if(((mantisBugId[7]=="resolved") || (mantisBugId[7]=="closed")) && (mantisBugId[8]=="fixed") && 
						($scope.testCaseEdit.status=="Fail"||$scope.testCaseEdit.status=="FAIL") && ($scope.testCaseEdit.testCaseAction=="Completed")){
					$scope.raiseReopen=true;
				}
				else{
					$scope.raiseReopen=false;
				}
			
			}
			$("#view-all-bugs-mantis").modal('hide');
		}
		
	}//mentis
	
	//for jira
	
	else if(bugtool==="Jira"){
		if (!$("#viewAllBugsJira input[name=jiraBugsProjectWise]:checked").val()) {
			alert('Kindly select a Bug to be linked!! in Jira');
	        return false;
		}
		else{
		       
			let jiraBugId=[];
			$("#viewAllBugsJira input[name=jiraBugsProjectWise]:checked").closest('tr').find("td").each(function(){
				jiraBugId.push($(this).text());
			});
			$scope.testCaseEdit.bugId=jiraBugId[1];
			$scope.testCaseEdit.bugPriority=jsUcfirst(jiraBugId[5]);
	    	/*
			if(mantisBugId[4]=="crash"){
				$scope.testCaseEdit.bugSeverity="Critical";
			}
			else if(mantisBugId[4]=="block"){
				$scope.testCaseEdit.bugSeverity="Blocker";
			}
			else{
				$scope.testCaseEdit.bugSeverity=jsUcfirst(mantisBugId[4]);
			}	
			
			if($scope.testCaseEdit.bugId){
				if((jiraBugId[7]=="Done") 
						($scope.testCaseEdit.status=="Pass") && ($scope.testCaseEdit.testCaseAction=="Completed")){
					$scope.raiseClose=true;
				}
				else{
					$scope.raiseClose=false;
				}
				
				if(((jiraBugId[7]=="resolved") || (mantisBugId[7]=="closed")) && (mantisBugId[8]=="fixed") && 
						($scope.testCaseEdit.status=="Fail") && ($scope.testCaseEdit.testCaseAction=="Completed")){
					$scope.raiseReopen=true;
				}
				else{
					$scope.raiseReopen=false;
				}
			}
			*/		
			$("#view-all-bugs-jira").modal('hide');
		}
	}//jira
	if($scope.testCaseEdit.bugId){
		$scope.testCaseEdit.bugType=1;
		$scope.bugid=true;
		$scope.raiseNew=false;
	}
}

function jsUcfirst(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}
$scope.raiseIssue = function() {
	 
	if($scope.testCaseEdit.bugType && $scope.testCaseEdit.bugPriority && $scope.testCaseEdit.bugSeverity){
	 /*$scope.testCaseEdit={
				bugSummary: null,
				componentName: null,
				jiraPriorityId: null
	 }*/
		autoReportService
		.raiseIssue(
				$scope.projectName,$scope.projectId)
		.then(
				function Success(response) {
//					alert("raiseIssue data: "+angular.toJson(response));
					$(".se-pre-con").hide();
					console.log(response.data.serviceResponse);
					if(response.data.serviceStatus=='200'){
						$scope.productName=response.data.serviceResponse[0].projectName;
						$scope.bugToolName=response.data.serviceResponse[0].toolName;
						
						if($scope.bugToolName=='Bugzilla')
						{
							if(response.data.serviceResponse[1].products.length==0){
								alert("Kindly create a product in Bugzilla with this project name..");
							}
							$scope.bugZillaCompnent=response.data.serviceResponse[1].products[0].components;	
							$scope.testCaseEdit.bugDescription="Scenario : " + $scope.testCaseEdit.scenarios +
								"\n\n" + "Test Case Type : " + $scope.testCaseEdit.testCaseType +
								"\n\n" + "Functionality : " + $scope.testCaseEdit.functionality + 
								"\n\n" + "Browser : " + $scope.testCaseEdit.browser + 
								"\n\n" + "Steps : " + $scope.testCaseEdit.steps + 
								"\n\n" + "Test Data : " + $scope.testCaseEdit.testData + 
								"\n\n" + "Test Description : " + $scope.testCaseEdit.testDescription + 
								"\n\n" + "Expected Result : " + $scope.testCaseEdit.expectedResult + 
								"\n\n" + "Actual Result : " + $scope.testCaseEdit.actualResult;
								
							$("#issueModaal").modal();
						}
						else if($scope.bugToolName=='Jira')
						{
							if(response.data.serviceResponse[1]=="Kindly create a project in Jira")
							{
								alert(response.data.serviceResponse[1]);
							}
							else
							{
								$scope.filteredJiraIssue=[];
								$scope.filteredJiraEpics=[];
								$scope.filteredJiraPriorityName=[];
								$scope.filteredJiraPriorityId=[];
								$scope.filteredJiraPriority=[];
								$scope.jiraIssue=response.data.serviceResponse[1][0];
								$scope.jiraEpics=response.data.serviceResponse[2];
								$scope.jiraPriorityName=response.data.serviceResponse[1][1][0];
								$scope.jiraPriorityId=response.data.serviceResponse[1][1][1];
								for(var i=0; i<$scope.jiraIssue.length; i++)
								{
									if($scope.jiraIssue[i])
									{
										$scope.filteredJiraIssue.push($scope.jiraIssue[i]);
									}
								}
								for(var i=0; i<$scope.jiraEpics.length; i++)
								{
									if($scope.jiraEpics[i])
									{
										$scope.filteredJiraEpics.push($scope.jiraEpics[i]);
									}
								}
								for(var j=0; j<$scope.jiraPriorityName.length; j++)
								{
									if($scope.jiraPriorityName[j])
									{
										$scope.filteredJiraPriority.push([]);
										$scope.filteredJiraPriority[j].push($scope.jiraPriorityName[j]);
										$scope.filteredJiraPriority[j].push($scope.jiraPriorityId[j]);
									}
								}
								
								$("#jiraIssueModal").modal();
							
							}
						}
						else if($scope.bugToolName=='Mantis')
						{
							$scope.testCaseEdit.bugDescription="Scenario : " + $scope.testCaseEdit.scenarios +
								"\n\n" + "Test Case Type : " + $scope.testCaseEdit.testCaseType +
								"\n\n" + "Functionality : " + $scope.testCaseEdit.functionality + 
								"\n\n" + "Browser : " + $scope.testCaseEdit.browser + 
								"\n\n" + "Steps : " + $scope.testCaseEdit.steps + 
								"\n\n" + "Test Data : " + $scope.testCaseEdit.testData + 
								"\n\n" + "Test Description : " + $scope.testCaseEdit.testDescription + 
								"\n\n" + "Expected Result : " + $scope.testCaseEdit.expectedResult + 
								"\n\n" + "Actual Result : " + $scope.testCaseEdit.actualResult;
								
							$("#mantisIssueModal").modal();
						}
					}
					else if(response.data.serviceStatus=='201'){
						alert(response.data.serviceResponse);
						
					}
					
				});
		
	}
	else if(!$scope.testCaseEdit.bugSeverity){
		alert("Kindly select Bug Severity!!");
	}
	else if(!$scope.testCaseEdit.bugPriority){
		alert("Kindly select Bug Priority!!");
	}
}

$scope.closeBugzillaIssueConfirmModal=function(){
	$scope.tempBugStatus="Closed";
	localStorage.setItem('tempBugStatus', $scope.tempBugStatus);
	$scope.showDialogGroup(true);
    $scope.confirmationDialogConfig = {
      title: "Alert!!!",
      message: "Do you want to close this issue ?",
      buttons: [{
        label: "Yes",
        action: "addCommentToBugzillaBugModal"
      }]
    };
}
$scope.addCommentToBugzillaBugModal=function(){
	
	$scope.errorMsgAddComment="";
	if($scope.bugToolNameForProj=="Bugzilla"){
	$("#add-comment-update-bugzillaBug").modal();
		
	}else if($scope.bugToolNameForProj=="Jira"){
	$("#add-comment-update-jiraBug").modal();
	}
	
	$(".confirmation-dialog").modal('hide');
}
$scope.clearBugzillaComment=function(){
	$("#bugzillaBugComment").val("");
}
$scope.reopenBugzillaIssue=function(){
	/*todoService.reopenBugzillaIssue($scope.projectName, $scope.projectId, $scope.testCaseEdit.bugId).then(function Success(response) {
				$(".se-pre-con").hide();
	});*/
	//let bugComment=$("#bugzillaBugComment").val();
	
		if($scope.bugzillacomments !=null){
		$scope.testCaseEdit.toolName=$scope.bugToolName;
		$scope.testCaseEdit.bugStatus=$scope.tempBugStatus;
		$scope.tempBugStatus="";
		$scope.testCaseEdit.bugDescription=$scope.bugzillacomments;
		alert("Comment has been added...");
		$scope.bugzillacomments="";
		$scope.clearBugzillaComment();
		$("#add-comment-update-bugzillaBug").modal('hide');
	}
	else{
		alert("Kindly add a comment..!!");
	}
}

$scope.reopenJiraIssue=function(){
	/*todoService.reopenBugzillaIssue($scope.projectName, $scope.projectId, $scope.testCaseEdit.bugId).then(function Success(response) {
				$(".se-pre-con").hide();
	});*/
	//let bugComment=$("#bugzillaBugComment").val();
	
		if($scope.bugzillacomments !=null){
		$scope.testCaseEdit.toolName=$scope.bugToolName;
		$scope.testCaseEdit.bugStatus=$scope.tempBugStatus;
		$scope.tempBugStatus="";
		$scope.testCaseEdit.bugDescription=$scope.bugzillacomments;
		alert("Comment has been added...");
		$scope.bugzillacomments="";
		$scope.clearBugzillaComment();
		$("#add-comment-update-jiraBug").modal('hide');
	}
	else{
		alert("Kindly add a comment..!!");
	}
}

$(".openBugDescriptionBtn").click(function(){
	let $bugBtn=$(this);
	let bugID=$bugBtn.parent('tr').attr('data-bugID');
	
	let htmlComments="";
	let heading="Comment";
	if(bugID){
		autoReportService.fetchBugComment($scope.projectId, bugID).then(function Success(response) {
			$(".se-pre-con").hide();
			let bugComments=response.data.serviceResponse;
			console.log(bugComments);
			let bugCommentsLen=bugComments.length;
			for(let i=0; i<bugCommentsLen; i++){
				if(i==0){
					heading="Description";
				}
				else{
					heading="Comment "+i;
				}
				htmlComments = htmlComments + "<tr class='showBugzillaBug_"+bugID+"'>" +
												"<td></td>" +
												"<td colspan=2 class='text-center text-success'>"+heading+" : </td>" +
												"<td colspan=7>"+bugComments[i]+"</td>" +
											  "</tr>";
			}
			
			$(".bugzillaBugID_"+bugID).after(htmlComments);
			$bugBtn.after("<td><button class=\"btn btn-primary fa fa-minus closeBugDescriptionBtn\"></button></td>");
			$bugBtn.remove();
		});
	}
	
});
$(".closeBugDescriptionBtn").click(function(){
	let bugID=$(this).parent('tr').attr('data-bugID');
	$(".showBugzillaBug_"+bugID).remove();
	$(this).after("<td><button class=\"btn btn-primary fa fa-plus openBugDescriptionBtn\"></button></td>");
	$(this).remove();
});

$scope.addIssue = function(issueModalID) {
	
	if($scope.testCaseEdit.bugSummary){
		$scope.testCaseEdit.toolName=$scope.bugToolName;
		$scope.testCaseEdit.productName=$scope.productName;
		$scope.testCaseEdit.componentName=$scope.component;
		$scope.testCaseEdit.epicName=$scope.epic;
		$scope.testCaseEdit.osVersion=$scope.osVersion;
		$scope.testCaseEdit.jiraPriorityId=$scope.priority;
		if(!$scope.component){
			if($scope.bugToolName=="Bugzilla"){
				alert("Please select a Component!!")
			}
			else{
				alert("Bug details added...bugZilla");
				$("#"+issueModalID).modal('hide');
			}
		}
		else{
			alert("Bug details added...");
			
			$("#"+issueModalID).modal('hide');
		}
		
	}
	else if(!$scope.testCaseEdit.bugSummary){
		alert("Kindly provide a Bug Summary!!");
	}
	
}
$scope.confirmationDialogForRaiseIssue = function() {
	$scope.showDialogGroup(true);
    $scope.confirmationDialogConfig = {
      title: "Alert!!!",
      message: "Do you Want to Raise Issue ?",
      buttons: [{
        label: "Yes",
        action: "raiseIssueModal"
      }]
    };
}
$scope.showDialogGroup = function(flag) {
	jQuery("#confirmation-dialog-group .modal").modal(flag ? 'show' : 'hide');
}
$scope. executeDialogActionForBugRaise= function(action) {
    if(typeof $scope[action] === "function") {
    		  $scope[action]();
    	}
  } 
$scope.raiseIssueModal = function() {
    console.log("Deleting...");
   
  }

$scope.showDialog = function(flag) {
	jQuery("#confirmation-dialog .modal").modal(flag ? 'show' : 'hide');
}
	
	

	$scope.getAutomationRunReport();
	
	
});//end
