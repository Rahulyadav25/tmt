demoApp.controller("onboardAppsController", function($scope,$interval,$state,$location, $rootScope, onboardAppsService) {
	
	$rootScope.checkHome();
	
	$scope.aapsObj={};
	$scope.aoid='';
	var d = new Date();
	$scope.dateTimeNow=d.getFullYear()+'-'+("00" + (d.getMonth() + 1)).slice(-2) + "-" + 
	    ("00" + d.getDate()).slice(-2)+ " " + 
	    ("00" + d.getHours()).slice(-2) + ":" + 
	    ("00" + d.getMinutes()).slice(-2) + ":" + 
	    ("00" + d.getSeconds()).slice(-2);
	
	
	$scope.saveApplicationDetails=function(){
		$scope.aapsObj.isSaved=1;
		$scope.ownerObj.isDeleted=0;
		$scope.aapsObj.savedBy='Lokesh', 
		$scope.aapsObj.savedOn=$scope.dateTimeNow, 
		$scope.aapsObj.createdOn=$scope.dateTimeNow, 
		$scope.aapsObj.ceatedBy='Lokesh'
		console.log($scope.aapsObj);
		onboardAppsService.saveApplicationDetails($scope.aapsObj).then(function Success(response){
		$scope.aoid=response.data.serviceResponse;
		})
	}
	$scope.viewSaveedApplications=function(){
		$scope.savedBy='Lokesh';
		onboardAppsService.viewSaveedApplications($scope.savedBy).then(function Success(response){
			$scope.allAppList=response.data.serviceResponse;
			})
	}
	$scope.populateAppDetails=function(apps){
		$scope.aapsObj=apps;
		$scope.aoid=$scope.aapsObj.aoid;
	}
	
	$scope.ownerObj={}
	$scope.saveOwnerDetails=function(aoid){
		console.log(aoid);
		$scope.ownerObj.aoid=aoid;
		$scope.ownerObj.isSaved=1;
		$scope.ownerObj.isDeleted=0;
		$scope.ownerObj.savedBy='Lokesh'; 
		$scope.ownerObj.savedOn=$scope.dateTimeNow; 
		$scope.ownerObj.createdOn=$scope.dateTimeNow; 
		$scope.ownerObj.createdBy='Lokesh';
		console.log($scope.ownerObj);
		onboardAppsService.saveOwnerDetails($scope.ownerObj).then(function Success(response){
			console.log(response);
		})
	}
	$scope.accessControlObj={};
	$scope.saveAccessControlDetails=function(aoid){
		console.log(aoid);
		$scope.accessControlObj.aoid=aoid;
		$scope.accessControlObj.isSaved=1;
		$scope.accessControlObj.savedBy='Lokesh'; 
		$scope.accessControlObj.savedOn=$scope.dateTimeNow; 
		$scope.accessControlObj.createdOn=$scope.dateTimeNow; 
		$scope.accessControlObj.createdBy='Lokesh';
		$scope.accessControlObj.isDeleted=0;
		console.log($scope.accessControlObj);
		onboardAppsService.saveAccessControlDetails($scope.accessControlObj).then(function Success(response){
			console.log(response);
		})
	}
	
	$scope.authorizerObj={};
	$scope.saveAuthorizerDetails=function(aoid){
		console.log(aoid);
		$scope.authorizerObj.aoid=aoid;
		$scope.authorizerObj.isDeleted=0;
		$scope.authorizerObj.isSaved=1;
		$scope.authorizerObj.savedBy='Lokesh'; 
		$scope.authorizerObj.savedOn=$scope.dateTimeNow; 
		$scope.authorizerObj.createdOn=$scope.dateTimeNow; 
		$scope.authorizerObj.createdBy='Lokesh';
		console.log($scope.authorizerObj);
		onboardAppsService.saveAuthorizerDetails($scope.authorizerObj).then(function Success(response){
			console.log(response);
		})
	}
	
	
	
	
	
	
	
});//END OF FILE