demoApp.controller("myProfileController",function($scope, $rootScope, $state, myProfileService) {
	
	$scope.username = localStorage.getItem('name');
	$scope.email= localStorage.getItem('userName');
	$scope.userid= localStorage.getItem('userid');
	$scope.userRoleID= localStorage.getItem('userRole');
	if($scope.userRoleID){
		if($scope.userRoleID==='100'){
			$scope.userRole='Admin';
		}
		else if($scope.userRoleID==='101'){
			$scope.userRole='Manager';
		}
		else if($scope.userRoleID==='102'){
			$scope.userRole='Tester';
		}
		else if($scope.userRoleID==='103'){
			$scope.userRole='Readonly';
		}
	}
	
	$(document).ready(function(){
		checkcookie();
		var token=localStorage.getItem('tokenID');
		if (token==null|| token=="")
		{
		window.location.href = "../login.html";
		}
	});
	
	$rootScope.checkHome();
	$state.transitionTo('admin.myprofile.overview');
	
	$scope.getMyProjectsData=function(){
		if($scope.userid && $scope.userRoleID){
			myProfileService.getMyProjectsData($scope.userid, $scope.userRoleID).then(function Success(response) {
				$(".se-pre-con").fadeOut('slow');
				 if(response.data.serviceStatus=="Success"){
					 $scope.myProjectsData=response.data.serviceResponse;
				 }
			});
		}
		else{
			alert("Kindly login again..");
		}
	}
			
			
			
	
	
});