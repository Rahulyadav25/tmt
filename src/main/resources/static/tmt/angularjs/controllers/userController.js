demoApp.controller("userController", function($scope, $rootScope, userService) {
	
	var userid=null;
	var username=null;
	var userRole=null;
	$scope.success=null;
	$scope.fail=null;
	$scope.userList=[];
	$scope.isSeeAllUser=false;
	$scope.isCreateUser=true;
	$scope.isNameDisable=false;
	
	$scope.userObject=
	{
			userId:null,
			name:null,
			role:null
	}
	
	$rootScope.checkHome();
	$scope.getADUser=function(){
		
		$scope.isNameDisable=false;
		$scope.userid=document.getElementById("userid").value;
		userService.getADUser($scope.userid).then(function Success(response) {
			 
			 $scope.isNameDisable=true;
			 if(response.data.serviceStatus=="Success"){
				 $scope.user=response.data.serviceResponse;
				 document.getElementById("username").value=$scope.user.fullName;
				
			 }
			 
		});
	}
	
	
	$scope.selectMultiple=function(){
	    if($("[type='checkbox']").is(":checked")==true){
		    $(".policyList > option").prop('selected','selected').trigger('change');
		}else{
			$('.policyList').val($scope.updateList).trigger('change');
		}
	}
	
	
	$scope.saveUser=function (){
	
		 /*$scope.userid=document.getElementById("userid").value;
		 $scope.username=document.getElementById("username").value;
		 $scope.userRole=document.getElementById("userrole").value;*/
		 
		 $scope.userObject.userId=document.getElementById("userid").value;
		 $scope.userObject.name=document.getElementById("username").value;
		 $scope.userObject.role=document.getElementById("userrole").value;
		 console.log($scope.userObject);
		
		 
		 userService.saveUser($scope.userObject).then(function Success(response) {
			 			 
			 if(response.data.serviceStatus=="Success"){
				 
				 $scope.displayNotification(response.data.serviceResponse,'success');
				 document.getElementById("userid").value=null;
				 document.getElementById("username").value=null;
				 document.getElementById("userrole").value=null;
				 $scope.isNameDisable=false;
				 
			 }else if(response.data.serviceStatus=="Fail" && response.data.serviceError=="user already created"){
				 $scope.displayNotification(response.data.serviceResponse,'danger');
				
			 }else if(response.data.serviceStatus=="Fail" && response.data.serviceError=="All fields are required"){
				 $scope.displayNotification(response.data.serviceError,'danger');
				 
			 }else{
				 $scope.displayNotification(response.data.serviceError,'danger');
			 }
					
		});
	  }
	
	
	$scope.showAllUserDetails=function (){
		
		 userService.showAllUserDetails().then(function Success(response) { 
			 if(response.data.serviceStatus=="Success"){
				 $scope.userList=response.data.serviceResponse;
				
			 }else if(response.data.serviceStatus=="Fail" && response.data.serviceError=="Data Not Found"){
				 $scope.displayNotification(response.data.serviceError,'danger');
				 
			 }else{
				 $scope.displayNotification(response.data.serviceError,'danger');
			 }
			 
		});
	  }	
	
	
	$scope.updateUser=function(){
		
		$scope.success=null;
		$scope.fail=null;
		 userService.updateUser($scope.updateUserId,$scope.updateUserName,$scope.updateUserRole).then(function Success(response) {
			 if(response.data.serviceStatus=="Success"){
				 
				 $("#userUpdateModal .close").click();
				 $scope.displayNotification(response.data.serviceResponse,'success');
				 $scope.updateUserId=null;
				 $scope.updateUserName=null;
				 $scope.updateUserRole=null;
				 $scope.showUserDetails();
				 
			 }else if(response.data.serviceStatus=="Fail" && response.data.serviceError=="All fields are required"){
				 $scope.fail=response.data.serviceError;
				 
			 }else{
				
				 $scope.success=null;
				 $scope.fail=response.data.serviceResponse;
			 }		
		});
		
	}
	
	$scope.deleteUser=function(user,id){
		if (confirm("Are you sure, You want to delete this user")) {
		    
		    userService.deleteUser(user).then(function Success(response){
			    
			    if(response.data.serviceStatus=="Success"){
			    	$('tr#'+id).remove();
			    	$scope.displayNotification(response.data.serviceResponse,'success');	
			    	
			    	
			    }else{
			    	$scope.displayNotification(response.data.serviceResponse,'danger');
			    	 
			    }
		    });
		  }
		
	}
	
	$scope.openUserUpdateModal=function(user){
		
		$scope.success=null;
		$scope.fail=null;
		$scope.updateUserId=user.userId;
		$scope.updateUserName=user.name;
		$scope.updateUserRole=user.role;
		$("#userUpdateModal").modal();
		
		
	}
	
	$scope.createUser = function (){
		
		$scope.isSeeAllUser=false;
		$scope.isCreateUser=true;
		
	}
	
	$scope.seeAllUsers = function (){
		
		$scope.isSeeAllUser=true;
		$scope.isCreateUser=false;
		$scope.showAllUserDetails();
		document.getElementById("userid").value=null;
		
	}
	
	
	
	$scope.displayNotification=function(msg,type){
		
		$.notify({
			// options
			message: msg 
		},{
			// settings
			type: type,
			placement: {
				from: 'top',
				align: 'center'
			},delay:4000
		});
	}
	
	
	
});