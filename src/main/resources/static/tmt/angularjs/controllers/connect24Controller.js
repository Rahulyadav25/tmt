demoApp.controller("connect24Controller", function($scope,connect24Service) {
	

	$scope.ChannelList=null;
	$scope.CommandList=null;
	$scope.command="All";
	//document.getElementById("command").value="All";
	$scope.connect24Obj={};
	$scope.startDate='2019-03-13';
	$scope.endDate='2019-03-13';
	
	$('#startDate,#endDate').datetimepicker({
		format:'YYYY-MM-DD'
	});
	
	
	connect24Service.getAllChannel().then(function Success(response) {
		$scope.ChannelList=response.data.serviceResponse;
		$scope.getConnect24Graph();		
	});
	
	connect24Service.getTodaysTransactionCount().then(function Success(response) {
		$scope.allTransactionCount=response.data.serviceResponse[0];
		
	});
	
    $scope.commandPerChannel=function(channel){
    	
    	if(channel=="All"){
    		$scope.CommandList=null;
    		$scope.command="All";
    	}else{
    		connect24Service.getCommandPerChannel(channel).then(function Success(response) {
    			$scope.CommandList=response.data.serviceResponse;
    		});
    	}
    		
		
    };
   
    $scope.getConnect24Graph=function(){
    	
        $scope.connect24Obj.channel=$scope.getChannel;
        $scope.connect24Obj.command=$scope.command;
        $scope.connect24Obj.startDate=$scope.startDate;
        $scope.connect24Obj.endDate=$scope.endDate;
        
        connect24Service.getConnect24Graph($scope.connect24Obj).then(function Success(response){
        	
        	$scope.graphData=response.data.serviceResponse;
        	var source="Transaction Count for All Channels";
        	var xTitle="Channels"
        	if(response.data.serviceStatus="Success"){
        		$scope.showConnect24Graph($scope.graphData.channelList,$scope.graphData.countList,xTitle,source);
        	}
        	
        }) ;
        	
    };

    
	$scope.getConnect24GraphBetweenDate=function(channel,count){
		
		$scope.startDate=document.getElementById("startDate").value;
		$scope.endDate=document.getElementById("endDate").value;
		$scope.connect24Obj.channel=$scope.getChannel;
        $scope.connect24Obj.command=$scope.command;
        $scope.connect24Obj.startDate=$scope.startDate;
        $scope.connect24Obj.endDate=$scope.endDate;
        
        var d1 = moment($scope.startDate);
        var d2 = moment($scope.endDate);
        var days = d2.diff(d1, 'days');
        
		if($scope.getChannel!="All" && $scope.command=="All"){
			
			connect24Service.getCommandGraphAsPerChannelBetweenDate($scope.connect24Obj).then(function Success(response){
	        	
	        	var data=response.data.serviceResponse;
	        	var source="All Transaction for "+$scope.getChannel;
	        	if(response.data.serviceStatus="Success"){
	        		$scope.showConnect24Graph(data.dateList,data.countList,"Days",source);
	        	}
	        }) ;
			
		}else if($scope.getChannel=="All" && $scope.command=="All" && days>0){
			
			connect24Service.getAllCommandChannelGraphBetweenDate($scope.connect24Obj).then(function Success(response){
            	
            	var data=response.data.serviceResponse;
            	var source="All Transaction Between "+$scope.startDate+" && "+$scope.endDate;
            	if(response.data.serviceStatus="Success"){
            		$scope.showConnect24Graph(data.dateList,data.countList,"Days",source);
            	}
            }) ;
        	
        	
        }else if($scope.getChannel=="All" && days==0){
        	$scope.getConnect24Graph();
        	
        }else{
        	
        	connect24Service.getGraphBetweenDate($scope.connect24Obj).then(function Success(response){
            	var data=response.data.serviceResponse;
            	var source="All Transaction Between "+$scope.startDate+" & "+$scope.endDate;
            	if(response.data.serviceStatus="Success"){
            		$scope.showConnect24Graph(data.dateList,data.countList,"Days",source);
            	}
            	
            }) ;
        }
        
       
	};
	
	
	$scope.showConnect24Graph=function(datalist,countlist,xTitle,source){
		
		Highcharts.chart('container', {
		    chart: {
		        type: 'column',
		        zoomType:'x'
		    },
		    credits : {
				enabled : false
			},
		    title: {
		    	text: 'Transaction Status',
		        style: {
	                color: '#dc3908'
	            }
		    },
		    subtitle: {
		        text: source,
		        style: {
	                color: '#2776ec'
	            }
		    },
		    xAxis: {
		    	min: 0,		    
		        categories: datalist,
		        crosshair: true,
		        title: {
		            text: xTitle,
		            style: {
		                color: '#e81dcf'
		            }
		        }
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Transactions Count',
		            style: {
		                color: '#15b330'
		            }
		        }
		    },
		    tooltip: {
		        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		            '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
		        footerFormat: '</table>',
		        shared: true,
		        useHTML: true
		    },
		    plotOptions: {
		        column: {
		            pointPadding: 0.4,
		            borderWidth: 0
		        },
		        series: {
	                cursor: 'pointer',
	                point: {
	                    events: {
	                        click: function() {
	                        	
	                        	$scope.openModelforGraph(this.category,this.y);
	                        }
	                    }
	                }
	            }
		      
		    },
		    series: [{
		        name: 'Count',
		        data: countlist
		        
		    }]
		});
	}
	
	
	$scope.openModelforGraph=function(channel,count){
		
		$("#myModal").modal();
		$scope.Date=channel;
		
		 var d1 = moment($scope.startDate);
	     var d2 = moment($scope.endDate);
	     var days = d2.diff(d1, 'days');
		
		$scope.connect24Obj.channel=$scope.getChannel;
        $scope.connect24Obj.command=$scope.command;
        $scope.connect24Obj.startDate=$scope.startDate;
        $scope.connect24Obj.endDate=$scope.endDate;
	
        if($scope.getChannel!="All" && $scope.command=="All"){
			
    		$scope.connect24Obj.startDate=channel;
    	    $scope.connect24Obj.endDate=channel;
    	        
			connect24Service.getGraphBasisOnHours($scope.connect24Obj).then(function Success(response){
	        	
	        	var data=response.data.serviceResponse;
	        	var source="All Transaction Count On "+channel+" for "+$scope.getChannel;
	        	if(response.data.serviceStatus="Success"){
	        		$scope.showCommandGraphPerChannelOnHoures(data.hoursList,data.countList,"Hours",source);
	        	}
	        });
			
		}else if($scope.getChannel=="All" && $scope.command=="All" && days>0){
			
			$scope.connect24Obj.startDate=$scope.Date;
    	    $scope.connect24Obj.endDate=$scope.Date;
			
			connect24Service.getConnect24Graph($scope.connect24Obj).then(function Success(response){
	        	
	        	$scope.gDate=response.data.serviceResponse;
	        	var source="Transaction Count for All Channels on "+$scope.Date;
	        	var xTitle="Channels"
	        	if(response.data.serviceStatus="Success"){
	        		$scope.showCommandGraphPerChannel($scope.gDate.channelList,$scope.gDate.countList,xTitle,source);
	        	}
	        }) ;
			
		}else if($scope.getChannel=="All"){
			
			$scope.connect24Obj.channel=channel;
			connect24Service.getCommandDataPerChannel($scope.connect24Obj).then(function Success(response){
	        	
	        	var data=response.data.serviceResponse;
	        	if(response.data.serviceStatus="Success"){
	        		$scope.showCommandGraphPerChannel(data.commandList,data.countList,"Commands",channel);
	        	}
	        	
	        }) ;
			
		}else{
			
			$scope.connect24Obj.startDate=channel;
    	    $scope.connect24Obj.endDate=channel;
    	    
			connect24Service.getGraphBasisOnHoursForBoth($scope.connect24Obj).then(function Success(response){
	        	
	        	var data=response.data.serviceResponse;
	        	var source="All Count On "+channel+" for Channel : "+$scope.getChannel+" & Command : "+$scope.command;
	        	if(response.data.serviceStatus="Success"){
	        		$scope.showCommandGraphPerChannelOnHoures(data.commandList,data.countList,"Hours",source);
	        	}
	        	
	        }) ;
			
		}
       
	}
	

	
	$scope.showCommandGraphPerChannel=function(resList,countList,xTitle,source){
		
		Highcharts.chart('container1', {
		    chart: {
		        type: 'column',
		        zoomType:'x'
		    },
		    credits : {
				enabled : false
			},
		    title: {
		        text: 'Transaction Status',
		        style: {
	                color: '#dc3908'
	            }
		        
		    },
		    subtitle: {
		        text: source,
		        style: {
	                color: '#2776ec'
	            }
		    },
		    xAxis: {
		    	min: 0,		    
		        categories: resList,
		        crosshair: true,
		        title: {
		            text: xTitle,
		            style: {
		                color: '#e81dcf'
		            }
		        }	
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Transactions Count',
		            style: {
		                color: '#15b330'
		            }
		        }
		    },
		    tooltip: {
		        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		            '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
		        footerFormat: '</table>',
		        shared: true,
		        useHTML: true
		    },
		    plotOptions: {
		        column: {
		        	pointPadding: 0.4,
		            borderWidth: 0
		        },
		        series: {
	                cursor: 'pointer',
	                point: {
	                    events: {
	                        click: function() {
	                        	$scope.openAllChannelCommandGraphopenModel(this.category,this.y);
	                        }
	                    }
	                }
	            }
		      
		    },
		    series: [{
		        name: "count",
		        data: countList
		       
		    }]
		});
	}
	
	$scope.openAllChannelCommandGraphopenModel = function(channel,count){
		
		$scope.myChannel=channel;
		var ln=channel.length;
		if(ln<=3){
			$("#AllChannelCommandModel").modal();
			
		}else{
			return;
		}
		
		
	    $scope.connect24Obj.channel=channel;
        $scope.connect24Obj.command=$scope.command;
        $scope.connect24Obj.startDate=$scope.Date;
        $scope.connect24Obj.endDate=$scope.Date;
	        
		connect24Service.getGraphBasisOnHours($scope.connect24Obj).then(function Success(response){
        	
        	var data=response.data.serviceResponse;
        	var source="All Transaction Count On "+$scope.Date+" for "+channel;
        	if(response.data.serviceStatus="Success"){
        		$scope.showAllChannelCommandGraph(data.hoursList,data.countList,"Hours",source);
        	}
        });
		
		
	}
	
	$scope.showAllChannelCommandGraph=function(resList,countList,xTitle,source){
		
		Highcharts.chart('container3', {
		    chart: {
		        type: 'column',
		        zoomType:'x'
		    },
		    credits : {
				enabled : false
			},
		    title: {
		        text: 'Transaction Status',
		        style: {
	                color: '#dc3908'
	            }
		        
		    },
		    subtitle: {
		        text: source,
		        style: {
	                color: '#2776ec'
	            }
		    },
		    xAxis: {
		    	min: 0,		    
		        categories: resList,
		        crosshair: true,
		        title: {
		            text: xTitle,
		            style: {
		                color: '#e81dcf'
		            }
		        }	
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Transactions Count',
		            style: {
		                color: '#15b330'
		            }
		        }
		    },
		    tooltip: {
		        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		            '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
		        footerFormat: '</table>',
		        shared: true,
		        useHTML: true
		    },
		    plotOptions: {
		        column: {
		        	pointPadding: 0.4,
		            borderWidth: 0
		        },
		        series: {
	                cursor: 'pointer',
	                point: {
	                    events: {
	                        click: function() {
	                        	$scope.openHourGraphModel(this.category,this.y);
	                        }
	                    }
	                }
	            }
		      
		    },
		    series: [{
		        name: "count",
		        data: countList
		       
		    }]
		});
	}
	
	
	
	
	$scope.showCommandGraphPerChannelOnHoures=function(resList,countList,xTitle,source){
		
		Highcharts.chart('container1', {
		    chart: {
		        type: 'column',
		        zoomType:'x'
		    },
		    credits : {
				enabled : false
			},
		    title: {
		        text: 'Transaction Status',
		        style: {
	                color: '#dc3908'
	            }
		        
		    },
		    subtitle: {
		        text: source,
		        style: {
	                color: '#2776ec'
	            }
		    },
		    xAxis: {
		    	min: 0,		    
		        categories: [],
		        crosshair: true,
		        title: {
		            text: xTitle,
		            style: {
		                color: '#e81dcf'
		            }
		        }	
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Transactions Count',
		            style: {
		                color: '#15b330'
		            }
		        }
		    },
		    tooltip: {
		        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		            '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
		        footerFormat: '</table>',
		        shared: true,
		        useHTML: true
		    },
		    plotOptions: {
		        column: {
		        	pointPadding: 0.4,
		            borderWidth: 0
		        },
		        series: {
	                cursor: 'pointer',
	                point: {
	                    events: {
	                        click: function() {
	                        	$scope.openHourGraphModel(this.category,this.y);
	                        }
	                    }
	                }
	            }
		      
		    },
		    series: [{
		        name: "count",
		        data: countList
		    }]
		});
	}
	
	
	$scope.openHourGraphModel = function(hour,count){
		
		
		
		if($scope.getChannel=="All"){
			$("#hourGraphModel").modal();
			$scope.connect24Obj.channel=$scope.myChannel;
			
			$scope.connect24Obj.command=$scope.command;
	        $scope.connect24Obj.startDate=$scope.Date+" "+hour+":00:00";
	        $scope.connect24Obj.endDate=$scope.Date+" "+hour+":00:00";
	        
	        connect24Service.getGraphForChannelPerHour($scope.connect24Obj).then(function Success(response){
	        	
	        	var data=response.data.serviceResponse;
	        	var source="Command Count for "+$scope.connect24Obj.channel+" Date: "+$scope.Date+" Hour: "+hour
	        	if(response.data.serviceStatus="Success"){
	        		$scope.showGraphPerHour(data.commandList,data.countList,"Commands",source,);
	        	}
	        	
	        }) ;
		}else{
			$scope.connect24Obj.channel=$scope.getChannel;
		}
		
		
		
		
	}
	
	
	$scope.showGraphPerHour=function(resList,countList,xTitle,source){
		
		Highcharts.chart('container2', {
		    chart: {
		        type: 'column',
		        zoomType:'x'
		    },
		    credits : {
				enabled : false
			},
		    title: {
		        text: 'Transaction Status',
		        style: {
	                color: '#dc3908'
	            }
		        
		    },
		    subtitle: {
		        text: source,
		        style: {
	                color: '#2776ec'
	            }
		    },
		    xAxis: {
		    	min: 0,		    
		        categories: resList,
		        crosshair: true,
		        title: {
		            text: xTitle,
		            style: {
		                color: '#e81dcf'
		            }
		        }	
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Transactions Count',
		            style: {
		                color: '#15b330'
		            }
		        }
		    },
		    tooltip: {
		        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		            '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
		        footerFormat: '</table>',
		        shared: true,
		        useHTML: true
		    },
		    plotOptions: {
		        column: {
		        	pointPadding: 0.4,
		            borderWidth: 0
		        },
		        series: {
	                cursor: 'pointer',
	                point: {
	                    events: {
	                        click: function() {
	                        	//$scope.openModelforGraph(this.category,this.y);
	                        }
	                    }
	                }
	            }
		      
		    },
		    series: [{
		        name: "count",
		        data: countList
		    }]
		});
	}
	
	
});