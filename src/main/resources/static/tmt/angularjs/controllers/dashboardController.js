var module = angular.module('demo.controllers', []);
module
		.controller(
				"dashboardController",
				[
						"$scope",
						"$state","$location","$anchorScroll","$rootScope",
						"dashboardService","configurationservice","projectOverviewService","projectOverviewAutoService",
						function($scope, $state,$location,$anchorScroll, $rootScope, dashboardService,configurationservice,projectOverviewService,
								projectOverviewAutoService) {
							/*$location.hash('top');
						     $anchorScroll();*/
							
							$rootScope.checkHome();
							$scope.isPtype=localStorage.getItem('IsPtype');
							//$scope.isPtypeMannual=true;
							//$scope.isPtypeAuto=false;
							$scope.pageNo=0;
							$scope.PageSize=20;
							$scope.pageNo1=0;
							$scope.PageSize1=20;
							$(document).ready(function(){
								//console.log($scope.pageNo+"Page=="+$scope.PageSize);
								$scope.nextprevious=function(status){
									console.log($scope.pageNo+"  PageSize"+$scope.PageSize)
									//alert("Status is"+status);
									if($scope.isPtype=="automation"){
										console.log("Automation1212"+$scope.pageNo1);
										if(status=='next'){
											$scope.pageNo1=$scope.pageNo1+20;
											$scope.autodashboarddetails();
											
										}
										if(status=='previous'){
											$scope.pageNo1=$scope.pageNo1-20;
													$scope.autodashboarddetails();
											
											}
										
									}else{
										console.log("Mannual"+$scope.pageNo);
										
										if(status=='next'){
											$scope.pageNo=$scope.pageNo+20;
													$scope.dashboarddetails();
											
											
											
										}
										if(status=='previous'){
											$scope.pageNo=$scope.pageNo-20;
											//$scope.pageSize=$scope.pageSize-1;
												//alert("Previous"+$scope.pageNo);
													$scope.dashboarddetails();
											
												
											}
									}
									
									
									
								}
								
								checkcookie();
								var token=localStorage.getItem('tokenID');
								if (token==null|| token=="")
								{
								window.location.href = "../login.html";
								}
								});
							document.body.scrollTop = document.documentElement.scrollTop = 0;
							$scope.createProjectObj = {
								createdBy : null,
								createdTime : null,
								isActive : 'Y',
								isJiraEnable : null,
								projectLocation : null,
								projectName : null,
								senarioNumber : null,
								testCaseNumber : null,
								updatedTime : null,
								module : null,
								isAutomation : false,
								isMannual : false,
								submodule : null,
								platform : null,
								subplatform : null,
								version : null,
								updatedBy : null,
								projectManager : null,
								ProjectStartDate : null,
								ProjectEndDate : null,
								applicationName : null

							}
							$scope.EditProjectObj = {
									projectID:null,
									createdBy : null,
									createdTime : null,
									isActive : null,
									isJiraEnable : null,
									projectLocation : null,
									projectName : null,
									senarioNumber : null,
									testCaseNumber : null,
									updatedTime : null,
									module : null,
									isAutomation : null,
									isMannual : null,
									submodule : null,
									platform : null,
									subplatform : null,
									version : null,
									updatedBy : null,
									projectManager : null,
									ProjectStartDate : null,
									ProjectEndDate : null,
									applicationName : null

								}
							$scope.user = localStorage.getItem('name');
							console.log($scope.user);
							$scope.userName= localStorage.getItem('userName');
							$scope.id= localStorage.getItem('userid');
							console.log($scope.id);
							$scope.editpro=false;
							//$scope.getdashboardview=0;
							$scope.user_role= localStorage.getItem('userRole');
							var key = null;
							var value = null;
							var allCookieArray = document.cookie.split(';');
							for (var i = 0; i < allCookieArray.length; i++) {
								key = allCookieArray[i].split('=')[0];
								value = allCookieArray[i].split('=')[1];
								if (key == "access-token" & value == "") {
									window.location.href = "../login.html";

								}
							}

	$scope.alert=function(heading){
		if(heading == "projectdetails"){
			$rootScope.createAlert('', 'Project Details', 'This widget highlights the counts of Test Suite(s), Test Group(s), Test Case(s) of a project.', 
					'info', true, true, 'pageMessages');
		}						
		else if(heading == "executionstatus"){
			$rootScope.createAlert('', 'Execution Status', 'This widget represents the status of executed Test Case(s) from run perspective.', 
					'info', true, true, 'pageMessages');
		}						
		else if(heading == "rundetails"){
			$rootScope.createAlert('', 'Run Details', 'This widget highlights the counts of executed Test Case(s) according to execution action.', 
					'info', true, true, 'pageMessages');
		}						
		else if(heading == "passfailcount"){
			$rootScope.createAlert('', 'Pass/Fail', 'This widget highlights the latest status of each Test Case whether Pass/Fail.', 
					'info', true, true, 'pageMessages');
		}						
		else if(heading == "defectstatus"){
			$rootScope.createAlert('', 'Defect Status', 'This widget represents the status of failed Test Case(s).', 
					'info', true, true, 'pageMessages');
		}						
		else if(heading == "summary"){
			$rootScope.createAlert('', 'Summary', 'This widget highlights on overall information like total execution time, total bug(s), total resource(s) assigned to the project, total run(s) completed and pass percentage.', 
					'info', true, true, 'pageMessages');
		}		
		else if(heading == "executiongraph"){
			$rootScope.createAlert('', 'Execution Graph', 'This widget highlights the bug count for a run', 
					'info', true, true, 'pageMessages');
		}
	}						
							
							/* Project Folder View */
							$scope.showGraph = function() {
								
								if($scope.user_role==='100')
									{
									configurationservice.showDashboard().then(function Success(response) {
										$scope.getAccesslist=response.data.serviceResponse;
										console.log($scope.getAccesslist);
										var tempArrayDate=[];
										var tempArrayPass=[];
										var tempArrayFail=[];
										var tempArrayBlocker=[];
										var tempArrayRetest=[];
									for(var i=0;i<$scope.getAccesslist.length;i++){
										tempArrayDate.push($scope.getAccesslist[i].specificdate);
										tempArrayPass.push($scope.getAccesslist[i].pass);
										tempArrayFail.push($scope.getAccesslist[i].fail);
										tempArrayBlocker.push($scope.getAccesslist[i].blocker);
										tempArrayRetest.push($scope.getAccesslist[i].retest);
									}
									console.log(tempArrayDate);
									console.log(tempArrayPass);
									console.log(tempArrayFail);
									am4core.ready(function() {

										am4core.useTheme(am4themes_animated);
										var chart = am4core.create("chartdiv1", am4charts.XYChart);
										chart.data =$scope.getAccesslist;
										var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
										categoryAxis.dataFields.category = "specificdate";
										var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
										var series1 = chart.series.push(new am4charts.LineSeries());
										series1.dataFields.valueY = "blocker";
										series1.dataFields.categoryX = "specificdate";
										series1.name = "blocker";
										series1.strokeWidth = 3;
										series1.tensionX = 0.7;
										series1.stroke = am4core.color("#FFD700");
										series1.tooltip.getFillFromObject = false;
										series1.tooltip.background.fill = am4core.color("#FFD700");
										series1.tooltipText = "{categoryX}:Total [bold]{valueY}[/]";

										var series2 = chart.series.push(new am4charts.LineSeries());
										series2.dataFields.valueY = "pass";
										series2.dataFields.categoryX = "specificdate";
										series2.name = "pass";
										series2.strokeWidth = 3;
										series2.tensionX = 0.7;
										series2.stroke = am4core.color("#228B22");
										series2.tooltip.getFillFromObject = false;
										series2.tooltip.background.fill = am4core.color("#228B22");
										series2.tooltipText = "{categoryX}: Total [bold]{valueY}[/]";

										var series3 = chart.series.push(new am4charts.LineSeries());
										series3.dataFields.valueY = "fail";
										series3.dataFields.categoryX = "specificdate";
										series3.name = "fail";
										series3.strokeWidth = 3;
										series3.stroke = am4core.color("#FF6347");
										series3.tensionX = 0.7;
										series3.tooltip.getFillFromObject = false;
										series3.tooltip.background.fill = am4core.color("#FF6347");
										series3.tooltipText = "{categoryX}: Total [bold]{valueY}[/]";
										/* Add legend */
										chart.legend = new am4charts.Legend();
										/* Create a cursor */
										chart.cursor = new am4charts.XYCursor();
									
									});

									});
									
									}
								else
									{
									configurationservice.showDashboardAccess($scope.id).then(function Success(response) {
										$scope.getAccesslist=response.data.serviceResponse;
										console.log($scope.getAccesslist);
										var tempArrayDate=[];
										var tempArrayPass=[];
										var tempArrayFail=[];
										var tempArrayBlocker=[];
										var tempArrayRetest=[];
									for(var i=0;i<$scope.getAccesslist.length;i++){
										tempArrayDate.push($scope.getAccesslist[i].specificdate);
										tempArrayPass.push($scope.getAccesslist[i].pass);
										tempArrayFail.push($scope.getAccesslist[i].fail);
										tempArrayBlocker.push($scope.getAccesslist[i].blocker);
										tempArrayRetest.push($scope.getAccesslist[i].retest);
									}
									console.log(tempArrayDate);
									console.log(tempArrayPass);
									console.log(tempArrayFail);
									am4core.ready(function() {

										am4core.useTheme(am4themes_animated);
										var chart = am4core.create("chartdiv1", am4charts.XYChart);
										chart.data =$scope.getAccesslist;
										var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
										categoryAxis.dataFields.category = "specificdate";
										var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
										var series1 = chart.series.push(new am4charts.LineSeries());
										series1.dataFields.valueY = "blocker";
										series1.dataFields.categoryX = "specificdate";
										series1.name = "blocker";
										series1.strokeWidth = 3;
										series1.tensionX = 0.7;
										series1.stroke = am4core.color("#FFD700");
										series1.tooltip.getFillFromObject = false;
										series1.tooltip.background.fill = am4core.color("#FFD700");
										series1.tooltipText = "{categoryX}: [bold]{valueY}[/]";

										var series2 = chart.series.push(new am4charts.LineSeries());
										series2.dataFields.valueY = "pass";
										series2.dataFields.categoryX = "specificdate";
										series2.name = "pass";
										series2.strokeWidth = 3;
										series2.tensionX = 0.7;
										series2.stroke = am4core.color("#228B22");
										series2.tooltip.getFillFromObject = false;
										series2.tooltip.background.fill = am4core.color("#228B22");
										series2.tooltipText = "{categoryX}: [bold]{valueY}[/]";

										var series3 = chart.series.push(new am4charts.LineSeries());
										series3.dataFields.valueY = "fail";
										series3.dataFields.categoryX = "specificdate";
										series3.name = "fail";
										series3.strokeWidth = 3;
										series3.stroke = am4core.color("#FF6347");
										series3.tensionX = 0.7;
										series3.tooltip.getFillFromObject = false;
										series3.tooltip.background.fill = am4core.color("#FF6347");
										series3.tooltipText = "{categoryX}: [bold]{valueY}[/]";
										/* Add legend */
										chart.legend = new am4charts.Legend();
										/* Create a cursor */
										chart.cursor = new am4charts.XYCursor();
									
									});

									});
									}
							  }
							$scope.isActive=true;
							$scope.showModalForProject = function() {
								$('.datepicker').datepicker({
								format : 'mm/dd/yyyy',
								autoclose : true,
								changeYear: true,
								minDate: '1/1/2000',
								maxDate: '1/1/2050',
								orientation:'bottom',
								todayHighlight : true,
								});
								$scope.isActive = 'Y';
								$scope.status=null;
								$scope.projectName="";
								$scope.applicationName="";
								$scope.isAuto="false";
								$scope.isMannual="false";
								$scope.platFrom="";
								$scope.pStdAte="";
								$scope.pEndAte="";
								$scope.isActive="";
								$scope.bugTracking="";
								
								if($scope.isPtype==='automation'){
									$scope.isAuto = true;
								}
								else{
									$scope.isMannual = true;
								}
								
								$("#createProject").modal();
								
								
								//$scope.isMannual = true;

								}
							$scope.showModalForEditProject = function(projectname,projectid) {
								$('.datepicker').datepicker({
									format : 'mm/dd/yyyy',
									autoclose : true,
									changeYear: true,
						            minDate: '1/1/2000',
						            maxDate: '1/1/2050',
									orientation:'bottom',
									todayHighlight : true
								});
								dashboardService.getproject(projectname,projectid).then(function Success(response) {
									console.log(response);
									$scope.projectNameEdit=response.data.serviceResponse.projectName;
									$scope.applicationNameEdit=response.data.serviceResponse.applicationName;
									/*$scope.isAuto=response.data.serviceResponse.isAutomation;
									$scope.isMannual=response.data.serviceResponse.isMannual;*/
									console.log(response.data.serviceResponse.isMannual);
									if(response.data.serviceResponse.isAutomation=='true' && response.data.serviceResponse.isMannual=='true')
									{
										$scope.isAutoEdit = true;
										$scope.isMannualEdit = true;
										}
									else if(response.data.serviceResponse.isAutomation=='true')
										{
										$scope.isAutoEdit = true;
										$scope.isMannualEdit = false;
										}
									else if(response.data.serviceResponse.isMannual==='true')
											
										{
										$scope.isAutoEdit = false;
										$scope.isMannualEdit = true;
										}
									else
										{
										$scope.isAutoEdit = false;
										$scope.isMannualEdit = true;
										}
									
									$scope.platFromEdit=response.data.serviceResponse.platform;
									$scope.projectIDEdit=response.data.serviceResponse.projectID;
									var d=new Date(response.data.serviceResponse.projectStartDate);
									var t=("00" + (d.getMonth() + 1)).slice(-2)+"/"+ ("00" + d.getDate()).slice(-2)+"/"+d.getFullYear();
									$scope.pStdAteEdit=t;
									var d1=new Date(response.data.serviceResponse.projectEndDate);
									var t1=("00" + (d1.getMonth() + 1)).slice(-2)+"/"+ ("00" + d1.getDate()).slice(-2)+"/"+d1.getFullYear();
									$scope.pEndAteEdit=t1;
									$scope.isActiveEdit=response.data.serviceResponse.isActive;
									$scope.bugTrackingEdit=response.data.serviceResponse.isJiraEnable;
								});
								$(".se-pre-con").hide();
								$("#EditProject").modal();
								
								
								
							}	
							$scope.showModalForDeleteProject = function(projectid) {
								var txt=confirm("Click ok if you want to delete");
								if(txt== null || txt == "")
									{
									
									}
								else
									{
									localStorage.setItem('IsPtype',$scope.isPtype);
								dashboardService.deleteProjectbyid(projectid,$scope.isPtype).then(function Success(response) {
									console.log(response);
									if(response.data.serviceStatus=="200")
										{
										alert("Project deleted successfully");
										}
									else{
										alert("Project can not be deleted!!");
									}
									if($scope.isPtype=='automation'){
										$scope.autodashboarddetails();
									}
									else{
										$scope.dashboarddetails();
									}
									 
								});
								
								
									}
							}
					
							configurationservice.getroletype($scope.user_role).then(function Success(response) {
								console.log(response);
								$scope.roletype=response.data.serviceResponse;
								if($scope.roletype==="Admin")
									{
									$scope.visible=true;
									$scope.editpro=true;
									}	
								else if($scope.roletype==="Manager")
									{
									$scope.visible=true;
									
									}	
								else
									{
									$scope.visible=false;
									}
								
							   });	
							
							$scope.resetCreateProject=function()
							{
								$scope.isAuto = null;
								$scope.isMannual = true;
								if ($scope.isAuto === null) {
									$('.AutoDetail').hide();
								} else {
									$('.AutoDetail').show();

								}	
							}
							
							$("6#submit").click(function(e){

								   var startdate=$("[name='pStdAte']").val();
								   var enddate=$("[name='pEndAte']").val();
								   if(new Date(enddate)<new Date(startdate))
								   {
									   alert("Please select 'End Date' greater than 'Start Date' !!");
									   e.stopImmediatePropagation();
									   return false;  
								   }
								   
								   
								});
							
							$scope.createProject = function() {
								$scope.createProjectObj.projectName = $scope.projectName;
								$scope.createProjectObj.applicationName = $scope.applicationName;
								$scope.createProjectObj.isAutomation = $scope.isAuto;
								$scope.createProjectObj.isMannual = $scope.isMannual;
								$scope.createProjectObj.platform = $scope.platFrom;
								$scope.createProjectObj.projectStartDate = $scope.pStdAte;
								$scope.createProjectObj.projectEndDate = $scope.pEndAte;
								$scope.createProjectObj.isActive = $scope.isActive;
								$scope.createProjectObj.isJiraEnable = $scope.bugTracking;
								$scope.createProjectObj.createdBy=$scope.user;
								console.log($scope.createUserObj);
								console.log($scope.projectName);

								if (($scope.projectName == null || $scope.projectName == "")
										|| ($scope.applicationName == null || $scope.applicationName =="")
										|| ($scope.pStdAte == null || $scope.pStdAte  =="" )
										|| ($scope.pEndAte == null || $scope.pEndAte =="")
										|| ($scope.isActive == "" || $scope.isActive ==null)
										|| (($scope.isAuto == null || $scope.isAuto =="false") &&
												 ($scope.isMannual == null || $scope.isMannual ==false  ))) {
									
									alert("Kindly fill all the fields");
									return false;
								} else {
//								let regexNumAlphaSpecial=/^(?=.*[A-Za-z].*)+([A-Za-z0-9 _!@$#%]*)$/;
							let regexNumAlphaSpecial=/^(?=.*[A-Za-z].*)+([A-Za-z0-9 _!@$#%*&|\\/\\\\]*)$/;
								let regexoneAplha=/(?=.*[A-Za-z].*)+/;
									let projectName=$('#projectName').val();
									if(!(regexNumAlphaSpecial.test(projectName))){
										if(!(regexoneAplha).test(projectName)){
												alert("First letter should be alphabet..");
											}
											else{
												alert("Only alphabets, numbers and special characters are allowed...");
											}
											$('#projectName').val('');
											return false;
									}
	//pranik								
//									let appName=$('#applicationName').val();
//									if(!(regexNumAlphaSpecial.test(appName))){
//										if(!(regexoneAplha).test(appName)){
//											alert("First letter should be alphabet.");
//										}else{
//											alert("Only alphabets, numbers and special characters are allowed...");
//										}
//		
//										$('#applicationName').val('');
//										return false;
//									}

									if($scope.projectName.length==1)
										{
											alert("Project name should be more than one letter");
											return false;
										}
									else{
									dashboardService
									.createProject(
											$scope.createProjectObj,
											$scope.id)
									.then(
											function Success(response) {
												var status = response.data.serviceResponse;
												console
														.log($scope.status);
												$(".se-pre-con").hide();
												$('.row input')
														.val(" ");

												if (status === "Your Project has been created Successfully") {
													alert("Your project has been created successfully");
													$("#createProject")
														.modal(
																	'hide');
													$scope
															.getDashboad();
									/*				$scope.dashboarddetails();*/
												} else if (status === "Application Name and Project Name are too long") {
													alert(status);

													return false;
												} else if (status === "Project Name is too long") {
													alert(status);

													return false;
												} else if (status === "Application Name is too long") {
													alert(status);

													return false;
												} else if (status === "Your project is already exists") {
													alert("Project Key already exists");
													$("#createProject")
															.modal();
													$('#BugTracking option').prop('selected', function() {
												        return this.defaultSelected;
												    });
													$scope.isAuto=$scope.isMannual=$scope.isActive=false;
													
												} else if (status === "Do Not Edit Other user") {
													alert(status)
													$("#createProject")
															.modal();
												}

											});
								}
								}

							}
							
							$scope.projectCreateClose=function(status)
							{
								/*$scope.createProjectObj={};	*/
								status=null;
								
								$('#createProject').on('hidden.bs.modal', function () {
								    $(this).find('form').trigger('reset');
								})
							}
							
							$scope.EditProject = function() {
								$scope.EditProjectObj.projectID = $scope.projectIDEdit;
								$scope.EditProjectObj.projectName = $scope.projectNameEdit;
								$scope.EditProjectObj.projectName = $scope.projectNameEdit;
								$scope.EditProjectObj.applicationName = $scope.applicationNameEdit;
								$scope.EditProjectObj.isAutomation = $scope.isAutoEdit;
								$scope.EditProjectObj.isMannual = $scope.isMannualEdit;
								$scope.EditProjectObj.platform = $scope.platFromEdit;
								$scope.EditProjectObj.projectStartDate = $scope.pStdAteEdit;
								$scope.EditProjectObj.projectEndDate = $scope.pEndAteEdit;
								$scope.EditProjectObj.isActive = $scope.isActiveEdit;
								$scope.EditProjectObj.isJiraEnable = $scope.bugTrackingEdit;
								console.log($scope.EditProjectObj);
								console.log($scope.projectNameEdit);
								dashboardService.EditProject(
										$scope.EditProjectObj).then(
										function Success(response) {
											$scope.status=response.data.serviceResponse;
											
											$('.row input').val(" ");
											$("#EditProject").modal('hide');
											$scope.dashboarddetails();
											$scope.autodashboarddetails();
										});
								
								$(".se-pre-con").hide();
							}
							$scope.getAllProject = function() {
								dashboardService.getAllProject().then(
										function Success(response) {
											$(".se-pre-con").hide();
											console.log(response);
										});

							}
							/*$scope.getOverviewProject = function() {
								dashboardService.getOverviewProject().then(
										function Success(response) {
											$(".se-pre-con").hide();
											$scope.dashboardProjectDetails=response.data.serviceResponse;
										});
							}*/
							$scope.getOverviewProject = function() {
								/*configurationservice.getUserAccessList($scope.id,$scope.user_role).then(*/
								dashboardService.getOverviewProject($scope.id,$scope.user_role).then(
										function Success(response)
										 {		
										console.log(response);
									     $scope.dashboardProjectDetails=response.data.serviceResponse;
									     $(".se-pre-con").hide();
							              });
								
							}
							$scope.showBugToolActive = function() {
								dashboardService.showBugToolActive().then(
										function Success(response) {
											//$(".se-pre-con").hide();
											//console.log(response);
											$scope.showActiveBugTracker=response.data.serviceResponse;
										});
							}
							$scope.showAutomationData= function() {
							//	$('#createpjtPlatform').show();

								 if($scope.isAuto)
									 {
									 $('.AutoDetail').hide();
									 }
								 else 
									 {
									 $('.AutoDetail').show();
									
									 }
							}
							
							
							$scope.closepESD=function()
							{
								 $('#summeryTSModel').modal("hide");
								 $('#summeryModel').modal("hide");
								 
							}

							/*$('#project-details')
									.jstree(
											{ // config object start

												"core" : { // core config
													// object
													"mulitple" : false, // disallow
													// multiple
													// selection
													"animation" : 100, // 200ms
													// is
													// default
													// value
													"check_callback" : true, // this
													// make
													// contextmenu
													// plugin
													// to
													// work
													"themes" : {
														"variant" : "small",
														"dots" : false
													},

													"data" : [
															// The required JSON
															// format for
															// populating a tree
															{
																"text" : "Feba",
																"state" : {
																	"opened" : false
																},
																"type" : "demo",
																"children" : [ // "children"
																		// key
																		// is
																		// an
																		// array
																		// of
																		// objects
																		{
																			"text" : "Fundtransfer",
																			"li_attr" : {
																				"class" : "li-style"
																			},
																			"a_attr" : {
																				"class" : "a-style"
																			}
																		},
																		{
																			"text" : "Internet Banking",
																			"state" : {
																				"opened" : true
																			},
																			"children" : [
																					{
																						"text" : "IMPS",
																						"state" : {
																							// "disabled":
																							// true,
																							"selected" : true
																						}
																					},
																					{
																						"text" : "NEFT",
																						"state" : {
																							"disabled" : true
																						},
																						"children" : [
																								"NEFT Inter",
																								"NEFT Intra" ]
																					},
																					{
																						"text" : "UPI",
																						"icon" : "glyphicon glyphicon-upload"
																					}, ]
																		}, ]
															},
															{
																"text" : "IDP",// Another
																// Root
																"state" : {
																	"opened" : false
																}
															} // root node
													// end, end of
													// JSON
													]
												// data core options end

												}, // core end

												// Types plugin
												"types" : {
													"default" : {
														"icon" : "fa fa-folder-open"
													},
													"demo" : {
														"icon" : "fa fa-folder-open"
													}
												},

												// config object for Checkbox
												// plugin (declared below at
												// plugins options)
												"checkbox" : {
													"keep_selected_style" : false, // default:
													// false
													"three_state" : true, // default:
													// true
													"whole_node" : false
												// default: true
												},

												"conditionalselect" : function(
														node, event) {
													return false;
												},

												// injecting plugins
												"plugins" : [ "checkbox",
														"contextmenu",
														// "dnd",
														// "massload",
														"search",
														// "sort",
														// "state",
														"types",
														// Unique plugin has no
														// options, it just
														// prevents renaming and
														// moving nodes
														// to a parent, which
														// already contains a
														// node with the same
														// name.
														"unique",
														// "wholerow",
														// "conditionalselect",
														"changed" ]
											}); // config object end

							// AJAX loading JSON Example:
							$('#jstree_ajax_demo')
									.jstree(
											{
												'core' : {
													'data' : {
														"url" : "https://codepen.io/stefanradivojevic/pen/dWLZOb.js",
														"dataType" : "json" // needed only if you do not supply JSON headers
													}
												},
												// Types plugin
												"types" : {
													"default" : {
														"icon" : "glyphicon glyphicon-record"
													}
												},
												"plugins" : [ "types", "unique" ]
											});

							// Listen for events - example
							$('#project-details').on(
									"changed.jstree",
									function(e, data) {
										// changed.jstree is a event
										// console.log(data.selected);
										console.log('ds: '
												+ data.changed.deselected);
									});*/
							$scope.passProject = function(projectName,projectId,pType,applicationName) {
								 localStorage.setItem('projectName',projectName);
								 localStorage.setItem('projectId',projectId);
								 localStorage.setItem('ptype',pType);
								 localStorage.setItem('applicationName',applicationName);

								 dashboardService.getProjectToolDetails(projectId).then(function Success(response){
									let temp =response.data.serviceResponse;
									localStorage.setItem('toolName',temp[0].toolName);
								});
								 
								 //localStorage.setItem("projectDetails", JSON.stringify(projectDet));
								 								 
							}
							$scope.summeryDetails = function(projectdetails) {
								$scope.projectDetails=projectdetails;
								projectOverviewService.testrunwisedetails(projectdetails.projectid).then(function Success(response) {
									  
									$scope.testresults=response.data.serviceResponse;
									console.log($scope.testresults);
									$(".se-pre-con").hide();
									 $('#summeryModel').modal();
								/** Graph Data Start */
								});
								
							}
							$scope.isActive = null;
							
							$scope.autodashboarddetails= function() {
								dashboardService.getAutoDashboardView($scope.id,$scope.user_role,$scope.pageNo1).then(
										function Success(response)
										 {		
									     $scope.getAutodashboardview=response.data.serviceResponse;
									     $(".se-pre-con").hide();
							              });			
								
							}
							
							$scope.dashboarddetails= function() {
								$scope.data=[];
								console.log("PageSize"+$scope.PageSize);
								console.log("pageSNo"+$scope.pageNo);
								dashboardService.getdashboardview($scope.id,$scope.user_role,$scope.isActive,$scope.pageNo,$scope.PageSize).then(
										function Success(response)
										 {		
											$scope.getdashboardview=response.data.serviceResponse;
											$scope.data=$scope.getdashboardview;
											console.log("$scope.getdashboardview******"+$scope.getdashboardview);
											console.log(JSON.stringify($scope.getdashboardview))
											
											
											
											$(".se-pre-con").hide();
							              });		
								
							}	
							/*$scope.getdetaildashboardprojectwise=function(details) {
								dashboardService.projectviewdata(details.projectid,$scope.user_role).then(
										function Success(response)
										 {			p
										console.log(response);
									   $scope.getdashboardview=response.data.serviceResponse;
									     $(".se-pre-con").hide();
							              });
								dashboardService.projectviewdata(details.projectid,$scope.user_role).then(
										function Success(response)
										 {			
										console.log(response);
									   $scope.getdashboardview=response.data.serviceResponse;
									     $(".se-pre-con").hide();
							              });
							}*/
							$scope.loadDashboardData = function() {
								$scope.dashboarddetails();
								//$scope.autodashboarddetails();
								//$scope.getOverviewProject();
								//$scope.showGraph();
							}
							var ctx=[];
							var myChart=[];
							var ctx1=[];
							var myChart1=[];
							$scope.getChartData = function(){
								for(var i=0;i<$scope.getdashboardview.length;i++){
								var id="Donut_Project";
								var id1="bugChart_graph";
								id=id+i;
								id1=id1+i;
								var noData=0;
								var noData1=0;
								let disabletooltip={
										enabled: true
								}
								let disabletooltip1={
										enabled: true
								}
								/*if($scope.getdashboardview[i].totalcompleted==0 && $scope.getdashboardview[i].totalcompleted==0
										&& $scope.getdashboardview[i].totalcompleted==0 && $scope.getdashboardview[i].totalcompleted==0)
									*/
								if($scope.getdashboardview[i].totalRunTestcases==0)
								{
										noData=1;
										disabletooltip={
												enabled: false
										}
								}
								if($scope.getdashboardview[i].openBug==0 && $scope.getdashboardview[i].closeBug==0
										&& $scope.getdashboardview[i].reOpenBug==0 && $scope.getdashboardview[i].resolvedBug==0)
									{
										noData1=1;
										disabletooltip1={
												enabled: false
										}
									}
								ctx =  document.getElementById(id).getContext('2d');
								ctx1 =  document.getElementById(id1).getContext('2d');
								//console.log(ctx[index]);
								Chart.defaults.global.defaultFontSize=10;
								myChart= new Chart(ctx, {
								    type: 'doughnut',
								    data: {
								        labels: ["Completed",	"On Hold",	"Under Review",	"Not Started","N/A" ,"Total Incomplete", "No Data"],
								        datasets: [{    
								            data: [$scope.getdashboardview[i].totalcompleted,	$scope.getdashboardview[i].totalonhold,$scope.getdashboardview[i].totalunderreview,$scope.getdashboardview[i].totalnotstarted,$scope.getdashboardview[i].totalNA,$scope.getdashboardview[i].totalactionincomplete,noData], 
								          	borderColor: ['#2196f38c', '#f443368c', '#3f51b570','#00968896','#FFD700','#00ffff','grey'],
								            backgroundColor: ['green', '#f443368c', '#3f51b570','#b8c7e4f0','#FFD700', '#00ffff','grey'], 
								            borderWidth: 1 // Specify bar border width
								        }]},         
								    options: {
								      //responsive: true, // Instruct chart js to respond nicely.
								      maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height
								      legend: {
								    	
								    	  boxWidth:20,
								    	   display:false,
								            position: 'top',
								            	
							               
								        },
								      tooltips:disabletooltip
								    }
								});
								var myChart1 = new Chart(ctx1, {
								    type: 'doughnut',
								    data: {
								        labels: ["Open","Closed","ReOpen", "Resolved", "No Data"],
								        datasets: [{    
								            data: [$scope.getdashboardview[i].openBug,$scope.getdashboardview[i].closeBug,$scope.getdashboardview[i].reOpenBug, $scope.getdashboardview[i].resolvedBug, noData1], 
								          	borderColor: ['#2196f38c', '#f443368c', '#3f51b570', '#006400', 'grey'],
								    /*        backgroundColor: ['#ff0000d9', '#f443368c', '#ff9900d9','grey'], */
								            backgroundColor: ['#ff0000d9', '#90ee90', '#ff9900d9', '#006400', 'grey'], 
								            borderWidth: 1 // Specify bar border width
								        }]},         
								    options: {
								      //responsive: true, // Instruct chart js to respond nicely.
								      maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height
								      rotation: 1 * Math.PI,
								      circumference: 1 * Math.PI,
								      legend: {
								            display:true
								        },
								      tooltips:disabletooltip1
								    }
								});
							}}
							$scope.getDashboad = function(){
								localStorage.setItem('IsPtype',$scope.isPtype);
									if($scope.isPtype=="mannual"){
										console.log(""+$scope.pageNo+"Page Size"+$scope.PageSize);
										$scope.dashboarddetails();
										//$scope.autodashboarddetails();
										$scope.showMannualDiv=true;
										$scope.showautoDiv=false;
									}
									else if($scope.isPtype=="automation"){
										$scope.autodashboarddetails();
										$scope.showautoDiv=true;
										$scope.showMannualDiv=false;
									}
									else{
										$scope.isPtype=="mannual"
											$scope.dashboarddetails();
										//$scope.autodashboarddetails();
										$scope.showMannualDiv=true;
										$scope.showautoDiv=false;
									}
								
							}
						

						
					
							
							$scope.resetProject=function()
							{
								 $scope.projectName="";
								$scope.applicationName="";	
								 $scope.isAuto="";
								 $scope.isMannual="";
								 $scope.platFrom="";
								 $scope.pStdAte="";
								 $scope.pEndAte="";
								 $scope.isActive="";
								$scope.bugTracking="";
								
							}
							
							
							/*$scope.getOverviewProject = function() {
								dashboardService.getOverviewProject().then(
										function Success(response) {
											$(".se-pre-con").hide();
											$scope.dashboardProjectDetails=response.data.serviceResponse;
										});
							}*/
							
							$scope.summeryTestDetails = function(projectdetails) {
								$scope.projectDetailsAuto=projectdetails;
								dashboardService.getSummaryDetails($scope.projectDetailsAuto.projectid,$scope.id,$scope.user_role,$scope.fromDate=="",$scope.toDate=="").then(function Success(response) {
									$scope.testSuitesresults=response.data.serviceResponse; 
									$(".se-pre-con").hide();
									
									console.log($scope.testSuitesresults);
									 $('#summeryTSModel').modal();
								/** Graph Data Start */
								});
								
								
							}
							
							
						/*	$scope.getTestCaseResult =  function() {
								for(var i=0;i<$scope.getAutodashboardview.length;i++){
									dashboardService.getRunStatus($scope.getAutodashboardview[i].projectid,$scope.id,$scope.user_role).then(
											function Success(response)
											 {		
										     $scope.getTestCaseStatus=response.data.serviceResponse;
										     $scope.getAutodashboardview[i].projTestSuitesList= $scope.getTestCaseStatus;
										    
								              });
								}
										
								 $(".se-pre-con").hide();
							}
							*/
							//p$scope.getAutodashboardview=[[]];
							var ctxAutoExe=[];
							var ctxAuto=null;
							var chartAutoExw=[];
							 $scope.getTestCaseStatus=[];
							$scope.indexExeDonut;
							
							$scope.autoExeDonutData = function(){
								for(var i=0;i<$scope.getAutodashboardview.length;i++){
									/*dashboardService.getRunStatus($scope.getAutodashboardview[i].projectid,$scope.id,$scope.user_role).then(
											function Success(response)
											 {		
										     $scope.getTestCaseStatus=response.data.serviceResponse;
										     $scope.getAutodashboardview[i][projTestSuitesList]= $scope.getTestCaseStatus;
										     $(".se-pre-con").hide();
								              });*/
									$scope.indexExeDonut = i;
								var id="AutoExeDonutChart";
								id=id+i;
								
								var noData=0;
								let disabletooltip={
										enabled: true
								}
								/*if($scope.getAutodashboardview[i].totalCompletedRun==0 )*/
								if( $scope.getAutodashboardview[i].totalCompletedRun==0 && $scope.getAutodashboardview[i].totalFailedRun==0 
										&& $scope.getAutodashboardview[i].running==0 &&  $scope.getAutodashboardview[i].stoppedRun==0)
								{
										noData=1;
										disabletooltip= {
										    enabled: false
										  }
								}
								
								//var id_dummy = $("#"+id);
							//	ctxAutoExe =  document.getElementById(id).getContext('2d');
								ctxAutoExe= $("#"+id)[0].getContext('2d');
								
								//console.log(ctxAutoExe[index]);
								Chart.defaults.global.defaultFontSize=14;
								myChart= new Chart(ctxAutoExe, {
								    type: 'doughnut',
								   
								    data: {
								    	
								        labels: ["Success",	"Fail",	"Running","Stopped", "No Data"],
								       //indexLabelFontSize: 40,
								        datasets: [{ 
								        	//indexLabelFontSize: 25,
								            data: [$scope.getAutodashboardview[i].totalCompletedRun,	$scope.getAutodashboardview[i].totalFailedRun, $scope.getAutodashboardview[i].running, $scope.getAutodashboardview[i].stoppedRun, noData], 
								            borderColor: ['#2196f38c', , '#3f51b570','#f443368c','#863613','grey'],
								            backgroundColor: ['green', '#3f51b570','#f443368c', '#863613','grey'],
								            borderWidth: 1 // Specify bar border width
								        }]},         
								    options: {
								      //responsive: true, // Instruct chart js to respond nicely.
								      maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height
								      legend: {
								    	
								    	  boxWidth:20,
								    	   display:false,
								            position: 'top',
								            	
							               
								        },
								      tooltips:disabletooltip
								        
								    }
								});
								 
								var tempLogs=[];
								var tempTotalPassed=[];
								var tempTotalFailed=[];
								var tempArrayRunName=[];
								var myChart=null;
								$scope.list=[];
									$scope.list =$scope.getAutodashboardview[i].projTestSuitesList;
								for(var j=0;j<$scope.list.length;j++){
									 if(j<=2){                
//									tempArrayRunName.push("RunId"+$scope.list[j].runId+"_"+$scope.list[j].scenarioID);
									tempArrayRunName.push("RunId_"+$scope.list[j].runId+"_");
									tempLogs.push($scope.list[j].totalTestSuites);
									tempTotalPassed.push($scope.list[j].totalPassed);
									tempTotalFailed.push($scope.list[j].totalFailed);
									 }
									}
								
								var ctx = document.getElementById("Chart"+i);
								var barOptions_stacked = {
										tooltips: {
											//enabled:false,
									            displayColors:true,
									            backgroundColor: '#227799'
									             },
									    hover :{
									        animationDuration:0
									    },
									    scales: {
									       
									        xAxes: [{
									              barThickness:14,
									            ticks: {
									                beginAtZero:true,
									                fontFamily: "'Open Sans Bold', sans-serif",
									                fontSize:8
									            },
									            scaleLabel:{
									                display:false
									            },
									            gridLines: {
									            }, 
									            stacked: true
									        }],
									        yAxes: [{
									          
									            gridLines: {
									                display:false,
									                color: "#fff",
									                zeroLineColor: "#fff",
									                zeroLineWidth: 0
									            },
									            ticks: {
									                fontFamily: "'Open Sans Bold', sans-serif",
									                fontSize:8
									            },
									            barPercentage: 0.5,
									            stacked: true
									        }]
									    },
									    legend:{
									      display:false,
									      position:'bottom'
									    },
									    pointLabelFontFamily : "Quadon Extra Bold",
									    scaleFontFamily : "Quadon Extra Bold",
									    
									};
								myChart = new Chart(ctx, {
								    type: 'bar',
								    data: {
								        labels: tempArrayRunName,
								        
								        datasets: [{
								            label:'Total Logs',
								            data: tempLogs,
								            backgroundColor: "#334CFF ",
								            hoverBackgroundColor: "#334CFF "
								        },/*{
								            label:'Passed Logs',
								            data: tempTotalPassed,
								            backgroundColor: "Green",
								            hoverBackgroundColor: "#036903"
								        },*/{
								            label:'Failed Logs',
								            data: tempTotalFailed,
								            backgroundColor: "#d73e4d",
								            hoverBackgroundColor: "#d7523e"
								    }]
								    },
								
								    options: barOptions_stacked,
								});}

							}
							/*$scope.getdetaildashboardprojectwise=function(details) {
								dashboardService.projectviewdata(details.projectid,$scope.user_role).then(
										function Success(response)
										 {			
										console.log(response);
									   $scope.getdashboardview=response.data.serviceResponse;
									     $(".se-pre-con").hide();
							              });
								dashboardService.projectviewdata(details.projectid,$scope.user_role).then(
										function Success(response)
										 {			
										console.log(response);
									   $scope.getdashboardview=response.data.serviceResponse;
									     $(".se-pre-con").hide();
							              });
							}*/
							
							$scope.getDashboad();	
							
						} ]);//End Of File

						