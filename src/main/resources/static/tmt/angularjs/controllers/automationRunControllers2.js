demoApp.controller("automationRunController", function($scope,$interval,$state,$stateParams,$location,$anchorScroll, $rootScope, automationRunService) {
	
	$(document).ready(function(){
		checkcookie();
		var token=localStorage.getItem('tokenID');
		if (token==null|| token=="")
		{
		window.location.href = "../login.html";
		}
		});
	$scope.img=false;
	$rootScope.checkHome();
	$scope.projectName = localStorage.getItem('projectName');
	$scope.projectId = localStorage.getItem('projectId');
	$scope.start=true;
	$scope.stop=false;
	$scope.slavemachine='';
	var projectDetails={
			pId:null,
			pName:null,
			slaveMachineName:null,
			osType:null
	}	
	$scope.getTestSuitesName = function() {
		automationRunService.getTestSuitesName($scope.projectId,$scope.projectName).then(
				function Success(response) {
					var datasheets=[];
					var allDataSheets=[];
					$scope.allDataSheets="";
					$(".se-pre-con").hide();
					$scope.testSuitesName=response.data.serviceResponse;
					if($scope.testSuitesName.length!=0)
					{
					
						for (var r = 0; r < $scope.testSuitesName.length; r++) 
						{
							allDataSheets.push($scope.testSuitesName[r].testSuitesName);
							if($scope.testSuitesName[r].isRun=='Y'){
								datasheets.push($scope.testSuitesName[r].testSuitesName);
							}
						}
						$scope.allDataSheets=allDataSheets;
						$scope.updateList=datasheets;
						console.log($scope.allDataSheets);
						$('.sheetsList').val($scope.updateList).trigger('change');
						//$(".sheetsList > option").prop('selected','selected').trigger('change');
					}
				});

		}
	$scope.getslavemachine = function() {
		automationRunService.getslavemachine($scope.projectId,$scope.projectName).then(
				function Success(response) {
					$(".se-pre-con").hide();
					$scope.machineName=response.data.serviceResponse;
				});

		}
	$scope.updateController =function(){
		alert("jjj");
	}
	$scope.updateJenkinsProject = function(slavemachine1) {
		projectDetails.pId=$scope.projectId;
		projectDetails.pName=$scope.projectName;
		//projectDetails.slaveMachineName=$("#smachine").val();
		projectDetails.slaveMachineName=slavemachine1;
		projectDetails.osType='window';
		automationRunService.updateJenkinsProject(projectDetails).then(
				function Success(response) {
					$(".se-pre-con").hide();
				});

		}
	$scope.startRun = function() {
		$scope.statusmsg="";
		$scope.statusimage="images/running.gif";
		$scope.img=true;
		$scope.start=false;
		$scope.stop=true;
		var datasheetsListForRun=$('#datasheets').val();
		automationRunService.startscript($scope.projectId,$scope.projectName,datasheetsListForRun,projectDetails).then(
				function Success(response) {
					
					console.log(response);
				});

		}
	$scope.stopRun = function() {
	automationRunService.stopscript($scope.projectId,$scope.projectName).then(
			function Success(response) {
				
				console.log(response);
			});

	}
	$scope.getCurrentRun = function() {
		automationRunService.getCurrentRun($scope.projectId,$scope.projectName).then(
				function Success(response) {
					//$(".se-pre-con").hide();
					console.log(response);
				});

	}
	$scope.currentRunDetails = function() {
		automationRunService.currentRunDetails($scope.projectId,$scope.projectName).then(
				function Success(response) {
					//$(".se-pre-con").hide();
					$scope.run=response.data.serviceResponse[0];
					$scope.runLogs=response.data.serviceResponse[1];
					if($scope.run.status=="Success"){
						$scope.statusmsg="Last run "+$scope.run.runId+" was Successfully Runned";
						$scope.statuscolor="green";
						$scope.statusimage="images/pass.png";
						$scope.stop=false;
						$scope.start=true;
					}
					else if($scope.run.status=="RUNNING")
					{
						$scope.statusmsg="Last run "+$scope.run.runId+" is Running";
						$scope.statuscolor="green";
						$scope.statusimage="images/running.gif";
						$scope.stop=true;
						$scope.start=false;
					}
					else if($scope.run.status=="")
					{
						$scope.statusmsg="Last run "+$scope.run.runId+" is Running";
						$scope.statuscolor="green";
						$scope.statusimage="images/running.gif";
						$scope.stop=true;
						$scope.start=false;
					}
					else if($scope.run.status=="Stopped"){
						
						$scope.statusmsg="Last run "+$scope.run.runId+" was stopped by User";
						$scope.statuscolor="red";
						$scope.statusimage="images/fail.png";
						$scope.stop=false;
						$scope.start=true;
					}
					else if($scope.run.status=="Fail"){
						$scope.statusmsg="Last run "+$scope.run.runId+" was Failed";
						$scope.statuscolor="red";
						$scope.statusimage="images/fail.png";
						$scope.stop=false;
						$scope.start=true;
					}
					$scope.img=true;
				});

	}	
	 $scope.stopcall = function() {
		  $interval.cancel(getLogs);

		}
	var getLogs=$interval(function(){$scope.currentRunDetails()},10000);
	
	  $scope.$on('$destroy', function() {
	      $scope.stopcall();
	    });
	  $scope.getslavemachine();
	  $scope.getTestSuitesName();
	$scope.currentRunDetails();
});
