demoApp.controller("scriptConfigController",function($http, $scope, $rootScope, scriptConfigService, projectDetailsService) {
	$scope.projectName=localStorage.getItem('projectName');
	
	$http.get('../src/main/resources/application.properties').then(function (response) {
	    $scope.automationPath = response.data.TestString;
	   console.log($scope.automationPath);
	});
	
	var sheetData=null;
	
	$scope.checkControllerFile=function()
	{
		var fileType=['xlsx','xls'];
		var extension=$("#controllerSheetFile").val().split(".").pop().toLowerCase();
		console.log(extension);
		if($("#controllerSheetFile").val())
		{
			if(fileType.indexOf(extension)== -1)
			{
				alert('Please Select .xlsx/.xls file type...');
				return true;
			}
			else
			{
				var projectName=localStorage.getItem('projectName');
				sheetData="test";
				scriptConfigService.checkControllerFile(projectName,sheetData).then(function Success(response)
				{
					$(".se-pre-con").fadeOut('slow');
					var checkCtrl=response.data.serviceResponse;
					if(checkCtrl=='Controller found')
						$('#controller-confirmation').modal('show');
					else
						$scope.uploadControllerFile();
				});
			}
		}
		else
		{
			alert("Add an excel file")
		}
	}
	
	
	//main controller sheet upload to project folder
	$scope.uploadControllerFile=function()
	{
		var projectName=localStorage.getItem('projectName');
		var projectid= localStorage.getItem('projectId');
		var fileData=document.getElementById("controllerSheetFile").files[0];
		
		var data=new FormData();
		data.append("file", fileData);
		data.append('projectName', projectName);
		data.append('projectid', projectid);
		
		scriptConfigService.uploadControllerFile(data).then(function Success(response){
	
		
			$("#controllerSheetFile").val(null);
			$('#controller-confirmation').modal('hide');
			
			alert(response.data.serviceResponse);
			$scope.getProjectAutoFilesafterupload();
			$(".se-pre-con").fadeOut('slow');
			//$scope.getSheetsName();.,m
		});
	}
	
	//datasheet upload to project folder
	$scope.uploadDataSheets=function()
	{
		var fileType=['xlsx','xls'];
		var extension=$("#dataSheetFile").val().split(".").pop().toLowerCase();
		if($('#dataSheetFile').val())
		{
			if(fileType.indexOf(extension)== -1)
			{
				alert('Please Select .xlsx/.xls file type...');
				return true;
			}
			else
			{			 sheetData=document.getElementById('dataSheetFile').files[0];

				var projectName=localStorage.getItem('projectName');
				$scope.shettname=sheetData.name;
				scriptConfigService.checkControllerFile(projectName,sheetData.name).then(function Success(response)
				{
					$(".se-pre-con").fadeOut('slow');
					var checkCtrl=response.data.serviceResponse;
					if(checkCtrl=='Datasheet found')
						$('#datasheet-confirmation').modal('show');
					else
						{
						  $scope.dataSheetUpload();
						}
				});
				
				
			}
		}
		else
		{
			alert("Add an excel file")
		}
		
	}
	
	$scope.dataSheetUpload=function()
	{
		$('#datasheet-confirmation').modal('hide');

		var sheetData=document.getElementById('dataSheetFile').files[0];
		var projectName=localStorage.getItem('projectName');
	    var projectid= localStorage.getItem('projectId');;
		var fd = new FormData();
		fd.append('datasheetFile',sheetData);
		fd.append('projectName',projectName);
		fd.append('projectid',projectid);
		scriptConfigService.uploadDataSheets(fd).then(function Success(response) {
			
		
			$('#dataSheetFile').val(null);
			alert(response.data.serviceResponse);
			//$scope.getSheetsName();
			$scope.getProjectAutoFilesafterupload();
			$(".se-pre-con").fadeOut('slow');
		});
		
	}
	var sheetName=[];
	$scope.getProjectAutoFilesafterupload=function()
	{
		var projectName=localStorage.getItem('projectName');
		projectDetailsService.getProjectAutoFiles(projectName).then(function Success(response)
		{
	
			sheetName=response.data.serviceResponse;
			$scope.dataSheetName=[];	
			$scope.controllerName=sheetName[0];
			if(sheetName[1]!='No datasheet found')
			{
				for(var i=1;i<sheetName.length;i++)
				{
					if(sheetName[i]!=null)
					{
						$scope.dataSheetName.push(sheetName[i]);
					}
				}
			}
			$(".se-pre-con").hide();
		});
	}
/*	$scope.checkFileType=function()
	{
		var fileEle=document.getElementById('controllerSheetFile');
		fileEle.addEventListener('change', function(e) 
		{
			fileData = fileEle.files[0];
			var fileType=['xlsx','xls'];
			var extension=$("#controllerSheetFile").val().split(".").pop().toLowerCase();
			if(fileType.indexOf(extension)== -1)
			{
				alert('Please Select .xlsx/.xls file type...');
				return true;
			}
			
		});
	}
	*/
/*$scope.downloadAutoFile=function(type,fileName)
{
	scriptConfigService.downloadAutoFile(type,fileName,$scope.projectName).then(function Success(response) {
		$(".se-pre-con").fadeOut('slow');
	});
}*/
	$scope.sheetType=null;
	$scope.deleteFileConfirmation=function(type, fileName){
		$scope.sheetType=type;
		$scope.toBeDeleted=fileName;
		$('#confirm-file-delete').modal('show');
		
	}
	$scope.deleteFile=function(){
		$('#confirm-file-delete').modal('hide');
		scriptConfigService.deleteAutomationFile($scope.sheetType,$scope.toBeDeleted,$scope.projectName).then(function Success(response){
			$scope.getProjectAutoFilesafterupload();
			$(".se-pre-con").fadeOut('slow');
			alert(response.data.serviceResponse);
		});
	}
	
});
