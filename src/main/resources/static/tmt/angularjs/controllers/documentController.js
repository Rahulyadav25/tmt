demoApp.controller("documentController", function($scope,$interval,$state,$stateParams,$location, $rootScope, $anchorScroll,documentService,configurationservice) {
	$(document).ready(function(){
		//alert("hi");
		checkcookie();
		var token=localStorage.getItem('tokenID');
		if (token==null|| token=="")
		{
		window.location.href = "../login.html";
		}
		});	
	$rootScope.checkHome();

	$scope.projectName=localStorage.getItem('projectName');
	$scope.projectId = localStorage.getItem('projectId');
	$scope.uId= localStorage.getItem('userid');
	$scope.user_role= localStorage.getItem('userRole');
	$scope.roletype="";
	$scope.buttonvisible=false;
	$scope.documentstatus="";
	$scope.isPublic="N";
	$scope.projectwiserole=localStorage.getItem('projectwiserole');
	 if($scope.user_role!=undefined)
	 {
	 if($scope.user_role==="100")
		{
		$("#testRun").css("display","unset");
		$("#todo").css("display","unset");
		$("#testSuites").css("display","unset");
		$("#Automation").css("display","unset");
		}
	if($scope.projectwiserole=="Manager" || $scope.projectwiserole==="Tester")
	{
		$("#todo").css("display","unset");
		$("#testSuites").css("display","unset");
		$("#Automation").css("display","unset");
		if($scope.projectwiserole==="Manager")
		{
			$("#testRun").css("display","unset");
	
		}
		else if($scope.projectwiserole==="Tester")
		{
	
			if($scope.url=='/testRun')
			{
				$state.transitionTo('error');
			}
		
		}
		else if($scope.projectwiserole==="Readonly")
		{
	
		}

	}
	 }
	
else
{
	$state.transitionTo('error');
}
	$("#iconList .nav-link").each(function(){
		$(this).removeClass("active");
	});
	$("#document a.nav-link").addClass("active");

	$scope.showModalForDocuments = function() {
		document.getElementById("file").value = "";
		$scope.documentstatus=null;
		$scope.documentType='';	
		$scope.documentTypeName=null;
		$scope.isPublic='N';
		$("#uploadDocuments").modal();
		
	}
	
	function handleFileInput() 
	{
		//alert("    kkkkkkkkkk     ")
		 if(document.getElementById("file").files[0])
		{
			 $('#showCross').show();
		}
	}
	//document.getElementById("file").addEventListener("load", handleFileInput, true);
	document.getElementById("file").addEventListener("change", handleFileInput, true);
	//$scope.datas
	$scope.publicDocuments = function()
	{		
		documentService.publicDocument().then(function Success(response) {
			$(".se-pre-con").hide();		
			$scope.publicDocument=response.data.serviceResponse;
		});
	}
	
	$scope.confirmUploadDocuments=function()
	{
		var pName=$scope.projectName;
		var pId=$scope.projectId;
		var documentsName=$scope.documentTypeName;	
		if(documentsName==undefined)
			{
			alert('Please select a document type');
			return false;
			}
		var isPublic=$scope.isPublic;
		var data = new FormData();
		var attachment=document.getElementById("file").files[0];
		var att2=document.getElementById("file").files[1];
		console.log("pid "+ pId);
		console.log("documentsName "+documentsName);
			console.log("attachment "+attachment);
			var fileType=['pdf','xlsx','xls','doc','docx','csv','txt'];
			var extension=$('#file').val().split(".").pop().toLowerCase();		
			var temp =$('#file').val();
			var count = (temp.match(/[.]/g) || []).length;
			console.log("count " +count);
		if(pId!="" && documentsName!="" && isPublic!="" && attachment!=""){
			if(att2)
				{
					alert("Please upload only one document");
					return false;
				}
			if(fileType.indexOf(extension)== -1)
			{
				if(count==0){
					alert('Please Select a File Type');
				}else{
					alert('Please Select a valid File Type');
				}
			return false;
			}
			if(count>1 ){
				alert('Please check the extension of Selected File');
			return false;
			}
		
		
		if(attachment.size<=10485760)
		{	
			$("#confirmation-dialog-upload-doument").modal();
	}
	else{
		alert('Kindly check file size');
	}
	}
	else{
		$scope.documentstatus="Kindly provide All Required Details"
	}
		
	
		
	}
	
	$scope.confirmDeleteDocument=function()
	{
		$("#confirmation-dialog-delete-document").modal();
	}
	$scope.confirmDeleteDocuments=function()
	{
		$("#confirmation-dialog-delete-project-document").modal();
	}
	
	$scope.uploadDocuments = function()
	{
		var pName=$scope.projectName;
		var pId=$scope.projectId;
		var documentsName=$scope.documentTypeName;	
		if(documentsName==undefined)
			{
			alert('Please select a document type');
			return false;
			}
		var isPublic=$scope.isPublic;
		var data = new FormData();
		var attachment=document.getElementById("file").files[0];
		console.log("pid "+ pId);
		console.log("documentsName "+documentsName);
			console.log("attachment "+attachment);
			var fileType=['pdf','xlsx','xls','doc','docx','csv','txt'];
			var extension=$('#file').val().split(".").pop().toLowerCase();		
			var temp =$('#file').val();
			var count = (temp.match(/[.]/g) || []).length;
			console.log("count " +count);
		if(pId!="" && documentsName!="" && isPublic!="" && attachment!=""){
			if(fileType.indexOf(extension)== -1)
			{
			alert('Please select a File Type');
			return false;
			}
			if(count>1 ){
				alert('Please check the extension of Selected File');
			return false;
			}
			
			
		data.append("file", attachment);
		data.append("pName", pName);
		data.append("pId", pId);
		data.append("documentsName", documentsName);
		data.append("isPublic", isPublic);
		data.append("createdBy",$scope.uId)
		
		if(attachment.size<=10485760)
		{
	documentService.uploadDocuments(data).then(function Success(response) {
		$scope.datas=data;
		$scope.error==null;
		
		$scope.documentstatuss=response.data.serviceResponse;
		console.log($scope.documentstatus);
		$(".se-pre-con").hide();
		if($scope.documentstatuss=="File uploaded successfully")
			{
			alert("File uploaded successfully");
			$("#uploadDocuments").modal('hide');
			$("#successStatusDocu").delay(5000).fadeOut();
			}
		
		if($scope.documentstatuss=="Duplicate")
			{
			alert("File Already Exists");
			  $scope.error="File Already Exists";
				  $("#errormsg").delay(1000).fadeOut();
			}
			else {
				if($scope.documentstatuss=="Max Number Of Occurences")
					{
					$scope.error="You have reached maximum number of same Document type";
					alert("You have reached maximum number of same Document type");
						$("#errormsg").delay(3000).fadeOut();
					}
						else{
							/*$("#uploadDocuments").modal('hide');*/
							$scope.getDocument();
						}
			}
		//alert($scope.documentstatus);
	});
	}
	else{
		alert('Kindly check file size');
	}
	}
	else{
		$scope.documentstatus="Kindly provide All Required Details"
	}
		$scope.publicDocuments();
		$("#successStatusDocu").delay(5000).fadeOut();
		$("#erroStatusDoc").delay(5000).fadeOut();
	}
	
	
	$scope.resetDocumnetUpload=function()
	{
		document.getElementById("file").value = "";
		$('#showCross').fadeOut('slow');
		$scope.documentTypeName=null;
		$scope.documentstatus=null;
	}
	
	
	/*$scope.publicDocument = function()
	{		
		documentService.publicDocument().then(function Success(response) {
			$(".se-pre-con").hide();
			$scope.publicDocument=response.data.serviceResponse;
		});
	}*/
	$scope.projectDocuments = function()
	{		
		documentService.projectDocument($scope.projectId).then(function Success(response) {
			$(".se-pre-con").hide();
			$scope.projectDocument=response.data.serviceResponse;
		});
	    
		
	}   
	
	$scope.clearFile = function(){
		document.getElementById("file").value = "";
		$('#showCross').fadeOut('slow');
		
	
	}
	$scope.clearFile();
	
	$scope.downloadDocument = function(filedetails){
		documentService.downloadDocument(filedetails).then(function Success(response) {
			$(".se-pre-con").hide();
			/* var anchor = angular.element('<a/>');
		        anchor.attr({
		            href: 'data:application/octet-stream;base64,' + response.data,
		            target: '_self',
		            download: response.headers.filename        });

		        angular.element(document.body).append(anchor);
		        anchor[0].click();*/
		});
	}
	$scope.getAllDocumentTypes= function(){
		documentService.allDocumentTypes().then(function Success(response) {
			$(".se-pre-con").hide();
			$scope.allDocumentList=response.data.serviceResponse;
			
		});
		
	}
	$scope.deleteDocument =function(id,filePath,documentName){
		var txt=confirm("Click ok if you want to delete");
		if(txt== null || txt == "")
		{
		
		}
	else
		{
		documentService.deleteDocument(id,filePath,documentName).then(function Success(response){
			$(".se-pre-con").hide();	
			$scope.publicDocuments();
			$scope.successStatus=response.data.serviceResponse;
			if(response.data.serviceStatus=='Success')
				{
					alert("File deleted successfully");
				}
			//alert($scope.successStatus);
			$("#successStatusDocu").delay(5000).fadeOut();
			$scope.getDocument();
		});
		}
	}
	
	$scope.getDocument = function() {
		
		configurationservice.getroletype($scope.user_role).then(function Success(response) {
			$(".se-pre-con").hide();
			$scope.roletype=response.data.serviceResponse;
			
			if($scope.roletype=="Admin")
			{
				$scope.buttonvisible=true;
			}
			

		});
		$scope.projectDocuments();
		$scope.publicDocuments();
	}
	$scope.getDocument();
	
	
});