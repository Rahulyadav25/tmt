demoApp.controller("testSuitesController", function($scope,$interval,$state,$stateParams,$location,$anchorScroll, $rootScope, testSuitesService) {
	$(document).ready(function(){
		checkcookie();
		var token=localStorage.getItem('tokenID');
		if (token==null|| token=="")
		{
			window.location.href = "../login.html";
		}
	});
	/*
	 * //$location.hash('top'); $anchorScroll();
	 */
	$rootScope.checkHome();
	$scope.projectName=localStorage.getItem('projectName');
	$scope.projectId = localStorage.getItem('projectId');
	$scope.userid = localStorage.getItem('userid');
	
	
	
	if($scope.roleId==="100")
		{
		$scope.projectwiserole="Manager";
		}
	else{
		$scope.projectwiserole=localStorage.getItem('projectwiserole');
	}
	
	$scope.status="";
	$scope.status1="";
	//var fileType=['pdf','xlsx','xls'];
	var fileType=['xlsx','xls'];
	$scope.roleId= localStorage.getItem('userRole');
	if($scope.roleId!=undefined)
	{
		if($scope.roleId==="100")
		{
			$("#testRun").css("display","unset");
			$("#todo").css("display","unset");
			$("#testSuites").css("display","unset");
			$("#Automation").css("display","unset");
		}
		if($scope.projectwiserole=="Manager" || $scope.projectwiserole==="Tester")
		{
			$("#todo").css("display","unset");
			$("#testSuites").css("display","unset");
			$("#Automation").css("display","unset");
			if($scope.projectwiserole==="Manager")
			{
				$("#testRun").css("display","unset");

			}
			else if($scope.projectwiserole==="Tester")
			{

				if($scope.url=='/testRun')
				{
					$state.transitionTo('error');
				}

			}
			else if($scope.projectwiserole==="Readonly")
			{

			}

		}
	}

	else
	{
		$state.transitionTo('error');
	}
	/*
	 * if($scope.projectwiserole=="Manager" || $scope.projectwiserole==="Tester") {
	 * if($scope.projectwiserole==="Manager") {
	 * $("#testRun").css("display","unset"); $("#todo").css("display","unset");
	 * $("#testSuites").css("display","unset");
	 * $("#Automation").css("display","unset"); } else
	 * if($scope.projectwiserole==="Tester") { $("#todo").css("display","unset");
	 * $("#testSuites").css("display","unset");
	 * $("#Automation").css("display","unset"); } else
	 * if($scope.projectwiserole==="Readonly") { } } else {
	 * $state.transitionTo('error'); }
	 */
	console.log($scope.projectwiserole);
	$("#iconList .nav-link").each(function(){
		$(this).removeClass("active");
	});
	$("#testSuites a.nav-link").addClass("active");

	$scope.showAddCaseBox=[];
	$scope.caseDescription=[];
//	$scope.caseAddStatus=[];

	$scope.failStatus=[];
	$scope.updateTestCaseStatus=null;

	$scope.toggleTable =  function(index) {
//		alert(index);
		$("#tableData"+index).slideToggle("slow");
	}
	var createTestCaseObj = {
			testSuitesId: null,
			testSuitesName: null,
			testGroupName: null,
			testGroupId: null,
			srNo: null,
			module: null,
			subModule: null,
			scenarioID: null,
			scenarios: null,
			functionality: null,
			testID: null,
			testCaseType: null,
			testDescription: null,
			dateOfExec: null,
			browser: null,
			testData: null,
			steps: null,
			expectedResult: null,
			actualRsult: null,
			status: null,
			executionTime: null,
			testerComment: null,
			testerName: null,
			jiraID: null,
			srID: null
	}
	var createTestSuitesObj = {
			testSuitesId:null,
			projectId : null,
			testSuitesName : null,
			createdBy : null,
			updatedBy : null,
			isActive : 'Y',
			testerName:null,
		}
	var createTestGroupsObj={
			testSuitesId:null,
			groupName:null,
			createdDate:null,
			updatedDate:null,
			createdBy:null,
			updatedBy:null,
			isActive : 'Y'
	}
	$scope.suitesName=null;
	$scope.suitsId=null;
	$scope.backToTestSuites = function(suitesName,suitsId) {
		$scope.showSuitesname=false;
		$scope.showSuitesDetails = false;
		$scope.suitesName = suitesName;
		$scope.suitsId = suitsId;
		if($scope.projectId!=null && $scope.projectId !=undefined)
		$scope.getAllTestSuitesByProject($scope.projectId);
	}
	// Getting Suites Details Specific Data
	$scope.showSuitesname=false;
	$scope.showSuitesDetails = false;
	$scope.getSuitesDetails = function(suitesName, suitsId,isActive) 
	{
		if(isActive === 'Y')
		{
			$scope.showSuitesname=true;
			$scope.suitesName = suitesName;
			$scope.suitsId = suitsId;
			$scope.getTestCasesBysuites(suitsId);
			$scope.showSuitesDetails = true;
			$scope.showSuitesname=true;
			$scope.showAddCaseBox[$scope.indexs]=false;
		}
		else{
			$scope.showSuitesname=false;
			alert(suitesName+" is currently inactive!!!!")
		}
		
		
	}	
	$("#selectAllCase").change(function() {
		if ($(this).is(':checked')) {
			$(".caseCheckbox").each(function() {
				if (!$(this).is(':checked')) {
					$(this).prop('checked', true);
					$(this).closest('tr').addClass("ui-selected");
				}
			});
		} else {
			$(".caseCheckbox").each(function() {
				$(this).prop('checked', false);
				$(this).closest('tr').removeClass("ui-selected");
			});
		}
	});

	$scope.testcaseSrID=[];
	$scope.selectTestCaseCheckBox=function(groupId)
	{

		if($scope.testcaseSrID.length==0){
			$scope.groupId=groupId;
		}

		if($scope.groupId==groupId){

			$scope.matchedGroupId1=$scope.groupId;
			var mainCheckBox="#selectAllCase"+groupId;
			var childCheckBox=".caseCheckbox"+groupId;
			if(groupId!==null)
			{
				if($(mainCheckBox).is(':checked'))
				{
					$(childCheckBox).each(function () {
						$(this).prop('checked',true);
						var trid = $(this).closest('tr').attr('id');
						if(!$scope.testcaseSrID.includes(parseInt(trid)))
						{
							$scope.testcaseSrID.push(parseInt(trid));
						}
					});
				}
				else
				{
					$(childCheckBox).each(function () {
						$(this).prop('checked',false);
						$scope.testcaseSrID=[];
					});

				}
			}
		}

		if($scope.groupId!=groupId){
			$("#selectAllCase"+groupId).prop('checked',false);
			alert("Kindly select testcases from single test group only......");
		}

		if($scope.testcaseSrID.length==0){

			$("#selectAllCase"+groupId).prop('checked',false);

		}
//		$scope.testcaseSrID=testcaseSrID;
//		alert($scope.testcaseSrID);
	}

	$scope.selectEachTestCaseCheckBox=function(groupId,testCaseId,sizeOfTestCaseId)
	{

		$scope.sizeOfTestCaseId1=sizeOfTestCaseId.length;
		if($scope.testcaseSrID.length==0){
			$scope.groupId=groupId;

		}
		if($scope.groupId==groupId){

			$scope.sizeOfTestCaseId=$scope.sizeOfTestCaseId1;
			$scope.matchedGroupId1=$scope.groupId;
			var checkBox="#checkBoxID"+testCaseId;
			if($(checkBox).is(':checked'))
			{
//				var trid = $(checkBox).closest('tr').attr('id');
				if(!$scope.testcaseSrID.includes(testCaseId))
				{
					$scope.testcaseSrID.push(testCaseId);
				}
			}
			else
			{
				var index = $scope.testcaseSrID.indexOf(testCaseId);
				$scope.testcaseSrID.splice(index, 1);
			}

		}
		if($scope.groupId!=groupId){
			$("#checkBoxID"+testCaseId).prop('checked',false);
			alert("Kindly select testcases from single test group only......");
		}

//		console.log(sizeOfTestCaseId.length+" 1");
//		console.log($scope.sizeOfTestCaseId+" 2");
//		console.log($scope.testcaseSrID.length+" 3");
		if($scope.testcaseSrID.length==$scope.sizeOfTestCaseId){
			$("#selectAllCase"+$scope.matchedGroupId1).prop('checked',true);
		}
		if($scope.testcaseSrID.length!=$scope.sizeOfTestCaseId){
			$("#selectAllCase"+$scope.matchedGroupId1).prop('checked',false);
		}
//		alert($scope.testcaseSrID);
	}
	$scope.confirmationDialogForDeleteMul = function(groupId) {

		$scope.matchedGroupId2=groupId;
		if($scope.matchedGroupId1==$scope.matchedGroupId2){
			$("#confirmation-dialog-Mul-Del").modal();
			
		}
		if($scope.matchedGroupId1!=$scope.matchedGroupId2){
			alert("Testcase(s) cannot be deleted.Kindly select the respective testcases and click on respective delete button to be deleted");
		}
		
	}
	$scope.deleteMulTestCase = function() {
		/*
		 * alert($scope.matchedGroupId1); alert($scope.matchedGroupId2);
		 */
		if($scope.matchedGroupId1==$scope.matchedGroupId2){
     //    alert($scope.testcaseSrID.length+"$scope.testcaseSrID.length=")
			if($scope.testcaseSrID.length==1){
				console.log($scope.testcaseSrID);
				testSuitesService.deleteMulTestCase($scope.testcaseSrID).then(
						function Success(response) {
							
							$(".se-pre-con").hide();
							$scope.getTestCasesBysuites($scope.suitsId);
							if(response.data.serviceResponse==="Test Cases Deleted Successfully")
								{
								alert("Test Case Deleted Successfully")
								
								$("#confirmation-dialog-Mul-Del").modal('hide');
								}
							else{
								alert(response.data.serviceResponse);
							}
							$scope.testcaseSrID=[];
						});

			}
		else if(!$scope.testcaseSrID.length==0)
			
			{
				console.log($scope.testcaseSrID);
				testSuitesService.deleteMulTestCase($scope.testcaseSrID).then(
						function Success(response) {
							
							$(".se-pre-con").hide();
							$scope.getTestCasesBysuites($scope.suitsId);
							if(response.data.serviceResponse==="Test Cases Deleted Successfully")
								{
								alert("Test Cases Deleted Successfully")
								$("#confirmation-dialog-Mul-Del").modal('hide');
								}
							else{
								alert(response.data.serviceResponse);
							}
							$scope.testcaseSrID=[];
						});

			}
		}
		if($scope.testcaseSrID.length==0)
		{
			$("#confirmation-dialog-Mul-Del").modal('hide');
			alert("Kindly select atleast one testcase !!!")
		}
	}

	$scope.shortTable = function() {
		  var oldList, newList, item,  caseId;
		$(".sortable tbody").sortable(
				{
					axis : "y",
					cursor : "grabbing",
					handle : ".handle",
					 start: function(event, ui) {
			                item = ui.item;
			                newList = oldList = ui.item.parent().parent();
			                caseId=$('.ui-sortable-helper').siblings(":first").text();
			                console.log(caseId,'1');
			            },
			            stop: function(event, ui) {          
			               // alert("Moved " + item.text() + " from " + oldList.attr('id') + " to " + newList.attr('id'));
			            /*  if(oldList.attr('id') != newList.attr('id')){
			                ui.item.clone().remove(oldList);
			                ui.item.clone().appendTo(newList);
			                        $(this).sortable('cancel');
			                        console.log(item.text(),'1.1');
			                        console.log(oldList.attr('id'),'1.21');
			                        console.log(newList.attr('id'),'1.22');
			                        }*/
			            	var oldGroupId=oldList.attr('id');
			            	var newGroupId=newList.attr('id');
			            	var caseId=item.attr('id');
			            	if(oldGroupId!==newGroupId)
		                	{
				                testSuitesService.updateTestGroupID(caseId,newGroupId).then(function Success(response) 
					            {
					                	$(".se-pre-con").hide();
					                	$scope.getTestCasesBysuites($scope.suitsId);
					                		
								});	  
		                	}
			            },
			            update : function(event, ui) {
			            				            	
			            },
			            change: function(event, ui) {  
			                
			                newList = ui.placeholder.parent().parent();
			                //alert(newList.srID);
			            },
			            connectWith: ".sortable tbody"
					/*helper : function(e, item) {
						console.log("Parent-Helper: ", item);
						if (!item.hasClass("ui-selected")) {
							item.addClass("ui-selected");
						}
						// Clone selected elements
						var elements = $(".ui-selected").not(
								'.ui-sortable-placeholder').clone();
						console.log("Making helper: ", elements);
						// Hide selected Elements
						item.siblings(".ui-selected")
								.addClass("hidden");
						var helper = $("<table />");
						helper.append(elements);
						console.log("Helper: ", helper);
						return helper;
					},
					start : function(e, ui) {
						var elements = ui.item.siblings(
								".ui-selected.hidden").not(
								'.ui-sortable-placeholder');
						ui.item.data("items", elements);
					},
					update : function(e, ui) {
						console.log("Receiving: ", ui.item
								.data("items"));
						$.each(ui.item.data("items"), function(k, v) {
							console.log("Appending ", v, " before ",
									ui.item);
							ui.item.before(v);
						});
					},
					stop : function(e, ui) {
						$(".ui-selected td input", this).each(
								function() {
									this.checked = !this.checked;
								});
						ui.item.siblings(".ui-selected").removeClass(
								"hidden");
						$("tr.ui-selected").removeClass("ui-selected");
					}*/
				}).disableSelection().selectable(
				{
            	    
        	        filter: ".descp",
        	        selected: function( event, ui ) {
        	            var row = $(ui.selected).parents('tr').index(),
        	                col = $(ui.selected).parents('td').index();
        	          //  $( ".sortable" ).each(function(){
        	            $(this).find('tr').eq(row)
        	                        .children('td')
        	                        .children('input').addClass('checked').prop('checked', true);
        	            $('.sortable tbody input:not(.checked)').prop('checked', false);
        	           // });
        	        },
        	        unselected: function( event, ui ) {
        	            var row = $(ui.unselected).parents('tr').index(),
        	                col = $(ui.unselected).parents('td').index();
        	            $(this).find('tr').eq(row)
        	                        .children('td')
        	                        .children('input').removeClass('checked').prop('checked', false);
        	        },
        	        cancel: '.other'
        	    });	
	}
	
	$scope.showModalForTestsuites = function() {
		//$scope.status='';
		$scope.isActive="Y";
		$scope.newTestSuiteStatus="";
		$scope.testSuitesName='';
		$scope.status="";
		$("#createTestSuites").modal();
		
	}
	$scope.createTestSuites = function(testSuitesObj) 
	{
		createTestSuitesObj.projectId = $scope.projectId;
		createTestSuitesObj.testSuitesName = $scope.testSuitesName;
		createTestSuitesObj.createdBy = localStorage.getItem('userName');
		createTestSuitesObj.updatedBy = null;
		createTestSuitesObj.testerName=localStorage.getItem('userid');
		createTestSuitesObj.isActive = $scope.isActive;		
		if(createTestSuitesObj.projectId==null)
		{
			$scope.status="Kindly provide project Id";
			$("#createTestSuites").modal();
			$('#modalResultTSuit').delay(5000).fadeOut();
			return false;
		}
		if(createTestSuitesObj.testSuitesName===null || createTestSuitesObj.testSuitesName==="")
		{
			$scope.status="Kindly provide Test Suite Name";
			$("#createTestSuites").modal();
			$('#modalResultTSuit').delay(5000).fadeOut();
			return false;
		}
		if(createTestSuitesObj.isActive===null || createTestSuitesObj.isActive==="")
		{
			$scope.status="Kindly provide all required field";
			$("#createTestSuites").modal();
			$('#modalResultTSuit').delay(5000).fadeOut();
			return false;
		}
		
		var attachment=document.getElementById("file").files[0];
		//alert(attachment);
		if(createTestSuitesObj.testSuitesName.length>=46)
		{
			alert("Test suitename is too long");
			return false;
		}
		else{
		if(typeof attachment == 'undefined'){
			
			
			testSuitesService.createTestSuitesWithoutFile(createTestSuitesObj).then(
					function Success(response) 
					{
						$(".se-pre-con").hide();	
					
						$scope.newTestSuiteStatus=response.data.serviceResponse;
						if($scope.newTestSuiteStatus ==="New TestSuites has been created Successfully")
						{
							$("#createTestSuites").modal('hide');
							alert("New Test Suite has been created successfully");
						}
						if($scope.newTestSuiteStatus ==="TestSuite already exists");
						{
							$scope.newTestSuiteStatus="Test Suite already exists";
						}
						$('.status').delay(5000).fadeOut();
						$('#modalResultTSuit').delay(5000).fadeOut();
						$scope.getAllTestSuitesByProject($scope.projectId);
						/*else if($scope.newTestSuiteStatus ==="previously deleted")
						{
							$("#confirmation-dialog-retrieve-deleted-test-case").modal('show');
							$scope.retrieveTestSuite=createTestSuitesObj;
						}*/
						
					});
		}
		else
		{		
		/*var extension=$('#file').val().split(".").pop().toLowerCase();			
				if( fileType.indexOf(extension)== -1)
				{
				alert('Please Select A valid File Type');
				return false;
				}*/
			var data = new FormData();
			var fileType=['xlsx','xls'];
			var extension=$('#file').val().split(".").pop().toLowerCase();		
			var temp =$('#file').val();
			var count = (temp.match(/[.]/g) || []).length;
		
		
			if(fileType.indexOf(extension)== -1)
			{
			alert('Please select a valid File (xlsx or xls) type');
			return false;
			}
			if(count>1 ){
				alert('Please check the extension of Selected File');
			return false;
			}
			data.append("file", attachment);
			data.append("projectId", createTestSuitesObj.projectId);
			data.append("testSuitesName", createTestSuitesObj.testSuitesName);
			data.append("createdBy", createTestSuitesObj.createdBy);
			data.append("isActive", createTestSuitesObj.isActive);
			
			if(attachment.size<=10485760)
			{
					testSuitesService.createTestSuites(data).then(
							function Success(response) {
								$(".se-pre-con").hide();
								if($scope.status=="New TestSuites has been created Successfully" || response.data.serviceResponse==="Testcases Uploaded")
								{
									$scope.status=response.data.serviceResponse;
									alert("New Test Suite has been created successfully.")
									$("#createTestSuites").modal('hide');
								}
								else{
									$scope.status=response.data.serviceResponse;
									alert(response.data.serviceResponse);
									
								}
								
								$scope.getAllTestSuitesByProject($scope.projectId);
								$('.status').delay(5000).fadeOut();
								$('#modalResultTSuit').delay(5000).fadeOut();
								$('.status').delay(5000).fadeOut();
								document.getElementById("file").value = "";
							});
			}
			else
			{
				alert('Kindly check file size');
			}
		}
	}
	}

	/*$scope.createTestSuites = function(testSuitesObj) 
	{
		createTestSuitesObj.projectId = $scope.projectId;
		createTestSuitesObj.testSuitesName = $scope.testSuitesName;
		createTestSuitesObj.createdBy = localStorage.getItem('userName');
		createTestSuitesObj.updatedBy = null;
		createTestSuitesObj.testerName=localStorage.getItem('userid');
		createTestSuitesObj.isActive = $scope.isActive;		
		if(createTestSuitesObj.projectId==null)
		{
			$scope.status="Kindly provide project Id";
			$("#createTestSuites").modal();
			$('#modalResultTSuit').delay(5000).fadeOut();
			return false;
		}
		if(createTestSuitesObj.testSuitesName===null || createTestSuitesObj.testSuitesName==="")
		{
			$scope.status="Kindly provide TestSuites Name";
			$("#createTestSuites").modal();
			$('#modalResultTSuit').delay(5000).fadeOut();
			return false;
		}
		if(createTestSuitesObj.isActive===null || createTestSuitesObj.isActive==="")
		{
			$scope.status="Kindly provide all Required Field";
			$("#createTestSuites").modal();
			$('#modalResultTSuit').delay(5000).fadeOut();
			return false;
		}
		
		var attachment=document.getElementById("file").files[0];
		//alert(attachment);
		if(typeof attachment == 'undefined'){
			testSuitesService.createTestSuitesWithoutFile(createTestSuitesObj).then(
					function Success(response) 
					{
						$(".se-pre-con").hide();	
						$scope.newTestSuiteStatus=response.data.serviceResponse;
						if($scope.newTestSuiteStatus ==="New TestSuites has been created Successfully")
						{
							$("#createTestSuites").modal('hide');
						}
						$('.status').delay(5000).fadeOut();
						$('#modalResultTSuit').delay(5000).fadeOut();
						$scope.getAllTestSuitesByProject($scope.projectId);
						else if($scope.newTestSuiteStatus ==="previously deleted")
						{
							$("#confirmation-dialog-retrieve-deleted-test-case").modal('show');
							$scope.retrieveTestSuite=createTestSuitesObj;
						}
						
					});
		}
		else
		{		
		var extension=$('#file').val().split(".").pop().toLowerCase();			
				if( fileType.indexOf(extension)== -1)
				{
				alert('Please Select A valid File Type');
				return false;
				}
			var data = new FormData();
			var fileType=['xlsx','xls'];
			var extension=$('#file').val().split(".").pop().toLowerCase();		
			var temp =$('#file').val();
			var count = (temp.match(/[.]/g) || []).length;
		
		
			if(fileType.indexOf(extension)== -1)
			{
			alert('Please Select A valid File Type');
			return false;
			}
			if(count>1 ){
				alert('Please check the extension of Selected File');
			return false;
			}
			data.append("file", attachment);
			data.append("projectId", createTestSuitesObj.projectId);
			data.append("testSuitesName", createTestSuitesObj.testSuitesName);
			data.append("createdBy", createTestSuitesObj.createdBy);
			data.append("isActive", createTestSuitesObj.isActive);
			
			if(attachment.size<=10485760)
			{
					testSuitesService.createTestSuites(data).then(
							function Success(response) {
								$(".se-pre-con").hide();
								if($scope.status=="New TestSuites has been created Successfully" || response.data.serviceResponse==="Testcases Uploaded")
								{
									$scope.status=response.data.serviceResponse;
									alert("New testSuites has been created successfully")
									$("#createTestSuites").modal('hide');
								}
								else{
									$scope.status=response.data.serviceResponse;
									
								}
								
								$scope.getAllTestSuitesByProject($scope.projectId);
								$('.status').delay(5000).fadeOut();
								$('#modalResultTSuit').delay(5000).fadeOut();
								$('.status').delay(5000).fadeOut();
								document.getElementById("file").value = "";
							});
			}
			else
			{
				alert('Kindly check file size');
			}
		}
	}*/
	$scope.retrieveDeletedTestCase=function(decision)
	{
		retieveTestSuite=$scope.retrieveTestSuite
		testSuitesService.retrieveTestSuite(retieveTestSuite, decision).then(function Success(response) 
		{
			$('.status').delay(5000).fadeOut();
			$('#modalResultTSuit').delay(5000).fadeOut();
			$scope.getAllTestSuitesByProject($scope.projectId);
		});
	
	}
	/*$scope.importTestCases = function(index,groupId){
		var uploadType=null;
		uploadType=$("input[name='uploadType"+index+"']:checked").val();
		console.log(index);
		console.log(uploadType);
		if(groupId==null)
		{
		$scope.importstatus="Unable to process your Request";
		alert($scope.importstatus);
		return false;
		}
		if(uploadType==null)
			{
			$scope.importstatus="Kindly Select Radio Button";
			alert($scope.importstatus);
			return false;
			}
		var Tcattachment=document.getElementById("Tcfile"+index).files[0];
		if(typeof Tcattachment == 'undefined'){
			$scope.importstatus="Kindly Select file";
			alert($scope.importstatus);
			return false;
		}
		else{
			var extension=$('#Tcfile'+index).val().split(".").pop().toLowerCase();			
			if( fileType.indexOf(extension)== -1)
			{
			alert('Please Select A valid File Type');
			return false;
			}
			var data = new FormData();
			data.append("file", Tcattachment);
			data.append("groupId", groupId);
			data.append("uploadType",uploadType)
			testSuitesService.importTestCases(data).then(
					function Success(response) {
						$scope.status1=response.data.serviceResponse;
						$scope.getTestCasesBysuites($scope.suitsId);
						$('.status').delay(5000).fadeOut();
						document.getElementById("Tcfile"+index).value = "";
					});
		}
		
	}
	*/
	$scope.importTestCases = function(index,groupId,suiteNameFS){
		var uploadType=null;
		
		uploadType=$("input[name='uploadType"+index+"']:checked").val();
		/*console.log(index);
		console.log(uploadType);*/
		if(groupId==null)
		{
		$scope.importstatus="Unable to process your Request";
		alert($scope.importstatus);
		return false;
		}
		if(uploadType==null)
			{
			$scope.importstatus="Kindly Select Radio Button";
			alert($scope.importstatus);
			return false;
			}
		var Tcattachment=document.getElementById("Tcfile"+index).files[0];
		if(typeof Tcattachment == 'undefined'){
			$scope.importstatus="Kindly select file";
			alert($scope.importstatus);
			return false;
		}
		else{
			var extension=$('#Tcfile'+index).val().split(".").pop().toLowerCase();			
			if( fileType.indexOf(extension)== -1)
			{
			alert('Please select a valid file(xlsx or xls) type');
			return false;
			}
			var data = new FormData();
			data.append("file", Tcattachment);
			data.append("groupId", groupId);
			data.append("uploadType",uploadType);
			data.append("suiteNameFS",suiteNameFS);
			testSuitesService.importTestCases(data).then(
					function Success(response) {
						if(response.data.serviceStatus=="200")
							{
						if(uploadType=="append")
							{
							$scope.status1="New TestCases are added";
							alert("New TestCases are added")
							}
						else if(uploadType=="replace")
							{
							$scope.status1="Replaced TestCases Successfully";
							alert("Replaced TestCases Successfully");
							}
							}
						else{
							$scope.status1=response.data.serviceResponse;
						}
						
						$scope.getTestCasesBysuites($scope.suitsId);
						$('.status').delay(5000).fadeOut();
						document.getElementById("Tcfile"+index).value = "";
					});
		}
		
	}
	

	
	$scope.clearTS=function()
	{
		document.getElementById("file").value = "";
		$scope.newTestSuiteStatus="";
		
	}
	/*$scope.importTestSuites=function(testSuitesId){
		var tsUploadType=null;
		$scope.importTSstatus='';
		tsUploadType=$("input[name='uploadTypets']:checked").val();
		if(testSuitesId==null)
		{
		$scope.importTSstatus="Unable to process your Request";
		alert($scope.importTSstatus);
		return false;
		}
		if(tsUploadType==null)
			{
			$scope.importTSstatus="Kindly Select Radio Button";
			alert($scope.importTSstatus);
			return false;
			}
		var Tcattachment=document.getElementById("TsTcfile").files[0];
		if(typeof Tcattachment == 'undefined'){
			$scope.importTSstatus="Kindly Select file";
			alert($scope.importTSstatus);
			return false;
		}
		else{
			var extension=$('#TsTcfile').val().split(".").pop().toLowerCase();			
			if( fileType.indexOf(extension)== -1)
			{
			alert('Please Select A valid File Type');
			return false;
			}
			var data = new FormData();
			data.append("file", Tcattachment);
			data.append("testSuitesId", testSuitesId);
			data.append("uploadType",tsUploadType)
			testSuitesService.importTestSuites(data).then(
					function Success(response) {
						$scope.status1=response.data.serviceResponse;
						$scope.getTestCasesBysuites($scope.suitsId);
						document.getElementById("TsTcfile").value = "";
						$('.status').delay(5000).fadeOut();
					});
		}
	}*/
	$scope.importTestSuites=function(testSuitesId,testSuiteName){
		var tsUploadType=null;
		$scope.importTSstatus='';
		tsUploadType=$("input[name='uploadTypets']:checked").val();
		
		if(testSuitesId==null)
		{
		$scope.importTSstatus="Unable to process your Request";
		alert($scope.importTSstatus);
		return false;
		}
		if(tsUploadType==null)
			{
			$scope.importTSstatus="Kindly Select Radio Button";
			alert($scope.importTSstatus);
			return false;
			}
		var Tcattachment=document.getElementById("TsTcfile").files[0];
		if(typeof Tcattachment == 'undefined'){
			$scope.importTSstatus="Kindly select file";
			alert($scope.importTSstatus);
			return false;
		}
		else{
			var extension=$('#TsTcfile').val().split(".").pop().toLowerCase();			
			if( fileType.indexOf(extension)== -1)
			{
			alert('Please select a valid file (xlsx or xls) type');
			return false;
			}
			var data = new FormData();
			data.append("file", Tcattachment);
			data.append("testSuitesId", testSuitesId);
			data.append("uploadType",tsUploadType);
			data.append("testSuiteName",testSuiteName);
			testSuitesService.importTestSuites(data).then(
					function Success(response) {
						$scope.status1=response.data.serviceResponse;
						alert($scope.status1);
						$scope.getTestCasesBysuites($scope.suitsId);
						document.getElementById("TsTcfile").value = "";
						$('.status').delay(5000).fadeOut();
					});
		}
	}

	$scope.clearImportTC=function(index){
		document.getElementById("Tcfile"+index).value = "";
	}
	$scope.clearImportTS=function(index){
		document.getElementById("TsTcfile").value = "";
	}
	$scope.editTestSuites = function(testSuitesid,projectName) {
		$scope.projectNameEdit=projectName;
		testSuitesService.getTestSuitesById(testSuitesid).then(
				function Success(response) {
					var testSuites=response.data.serviceResponse;					
					$scope.testSuitesNameEdit=testSuites.testSuitesName;
					$scope.testSuitesIdEdit=testSuites.testSuitesId;
					$scope.isActiveEdit=testSuites.isActive;
					$scope.testSuitesId=testSuites.testSuitesId;	
					
					$scope.testsuiteStatus="";
					$("#editTestSuites").modal();
					$(".se-pre-con").hide();					
				});
	}
	
	$scope.updateTestSuites = function() 
	{
		createTestSuitesObj.projectId = $scope.projectId;
		createTestSuitesObj.testSuitesName = $scope.testSuitesNameEdit;
		createTestSuitesObj.testSuitesId = $scope.testSuitesId;
		createTestSuitesObj.updatedBy =  localStorage.getItem('userName');
		createTestSuitesObj.isActive = $scope.isActiveEdit;	
		if(createTestSuitesObj.testSuitesName==null)
		{
		$scope.status="Kindly provide TestSuites Name";
		return false;
		}
		if(createTestSuitesObj.isActive==null )
		{
		$scope.status="Kindly provide all required field";
		return false;
		}
		
		if(createTestSuitesObj.testSuitesName.length >= 46)
		{
		$scope.status="TestSuites name is too long";
		return false;
		}
		
		testSuitesService.updateTestSuites(createTestSuitesObj,$scope.projectwiserole).then(
				function Success(response) {	
					$scope.updateTestSuit=response.data.serviceResponse;
					$(".se-pre-con").hide();
					//$("#editTestSuites").modal('hide');
					$scope.testsuiteStatus=response.data.serviceResponse;
					$scope.getAllTestSuitesByProject($scope.projectId);
					$("#showstestsuiteupdate").delay(5000).fadeOut();
					alert($scope.updateTestSuit)
					
				});
	}
	
	$scope.getAllTestSuitesByProject = function(projectId) {
		testSuitesService.getAllTestSuitesByProject(projectId).then(
				function Success(response) {
					$(".se-pre-con").hide();
					$scope.testSuitesDetails=response.data.serviceResponse;
					
				});
	}
	
	$scope.viewInforTC=function()
	{
		$("#infoTestCase").modal('show');
	}

	
	$scope.deleteTestCase = function(testcaseSID) {
		testSuitesService.deleteTestCase(testcaseSID).then(
				function Success(response) {
					$(".se-pre-con").hide();
					if(response.data.serviceResponse == "TestCase Deleted Successfully")
						{
							alert(response.data.serviceResponse);
						}
					else{
						alert(response.data.serviceResponse);
					}
					$scope.getTestCasesBysuites($scope.suitsId);
				});
	}
	$scope.addNewTestCase = function(index) {
		$scope.indexs=index;
		$scope.caseDescription[index]="";
		
		$scope.showAddCaseBox[index]=true;
		$scope.caseAddStatus="";
		$scope.failStatus="";
	}
	/*$scope.createTestCase=false;
	$scope.updateTestCase=false;*/
	$scope.notify={};
	$scope.showUpdateModal=function(index, groupId, groupName, suitesName)
	{
		createTestCaseObj.testDescription = $scope.caseDescription[index];
		if(createTestCaseObj.testDescription!="" && createTestCaseObj.testDescription!=null)
		{
			$scope.testCaseEdit={};
			$scope.testCaseEdit.testDescription=createTestCaseObj.testDescription;
			$scope.testCaseEdit.testSuitesName=suitesName;
			$scope.testCaseEdit.testGroupName=groupName;
			$scope.updateTestCaseStatus=null;
			$("#editTestCase").modal();
			$scope.indexM=index;
			$scope.groupIdM=groupId;
			/*$scope.createTestCase=true;*/
			$("#submity").css("display", "none");
			$("#submitNew").show();
		}
		else{
			
			alert("Kindly enter test case description");
		}
		/*else
		{
			$scope.notify.title="Error!!!"
			$scope.notify.message="Please provide Testcase description..."
			$("#notify-dialog-group").modal();
		}*/
	}
	/*$scope.createTestCase = function(index,groupId) {*/
	$scope.createTestCase = function() {
		var starttime = new Date();
		var isotime = new Date((new Date(starttime)).toISOString() );
		var fixedtime = new Date(isotime.getTime()-(starttime.getTimezoneOffset()*60000));
		
		var groupId=$scope.groupIdM;
		var index=$scope.indexM;
		
		createTestCaseObj.testSuitesName=$scope.testCaseEdit.testSuitesName;
		createTestCaseObj.testGroupName=$scope.testCaseEdit.testGroupName;
		createTestCaseObj.testID=$scope.testCaseEdit.testID;
		createTestCaseObj.browser=$scope.testCaseEdit.browser;
		createTestCaseObj.scenarioID=$scope.testCaseEdit.scenarioID;
		createTestCaseObj.scenarios=$scope.testCaseEdit.scenarios;
		createTestCaseObj.testCaseType=$scope.testCaseEdit.testCaseType;
		createTestCaseObj.steps=$scope.testCaseEdit.steps;
		createTestCaseObj.functionality=$scope.testCaseEdit.functionality;
		createTestCaseObj.testData=$scope.testCaseEdit.testData;
		createTestCaseObj.expectedResult=$scope.testCaseEdit.expectedResult;
				
		createTestCaseObj.testGroupId = groupId;
		createTestCaseObj.testDescription = $scope.testCaseEdit.testDescription;
		if($scope.testExcute){
			let hrs = $("#prepTime").val().split(':')[0];
			let min = $("#prepTime").val().split(':')[1];
			let sec =$("#prepTime").val().split(':')[2];
			let exetime=(+hrs) * 60 * 60 + (+min) * 60 + (+sec);
			createTestCaseObj.preparationTime = parseInt(exetime);
		}
		else{
			createTestCaseObj.preparationTime =0;
		}
		
		createTestCaseObj.createdBy=localStorage.getItem('userid');
		createTestCaseObj.tcPreparedBy=localStorage.getItem('userid');
		//createTestCaseObj.createdDate=fixedtime.toISOString().slice(0, 19).replace('T', ' ');
		if(createTestCaseObj.testDescription!="" && createTestCaseObj.testDescription!=null && createTestCaseObj.browser!="? undefined:undefined ?")
		{
		//$("#editTestCase").modal();
		testSuitesService.addNewTestId(createTestCaseObj).then(
				function Success(response) {
					$(".se-pre-con").hide();
					$scope.status=response.data.serviceStatus;
					
					if($scope.status==200){
					$scope.caseAddStatus[index]=response.data.serviceResponse;
					$(".success").show().fadeOut(5000);
					if(response.data.serviceResponse==="TestCase Added Successfully")
						{
							alert("TestCase Added Successfully")	;
							$("#editTestCase").modal('hide');
							//$(".tableData").show();
					}
					else{
						$scope.updateTestCaseStatus=response.data.serviceResponse;
						$("#editTestCase").modal();
					}
					}
					if($scope.status==400){
						$scope.failStatus[index]=response.data.serviceResponse;
						$(".fail").show().fadeOut(5000);
						$("#editTestCase").modal('hide');
						//$(".tableData").show();
						}
					$scope.caseDescription[index]="";
					$scope.showAddCaseBox[index]=true;	
					$scope.getTestCasesBysuites($scope.suitsId);
					
					/*createTestCaseObj.srID=$scope.status.srID;
					$scope.testCaseEdit=createTestCaseObj;
					$scope.updateTestCase();
*/					
					$(".tableData").show();
				});
		}
		else
		{
			$scope.updateGroupStatus="Kindly provide TestCase Description";	
		}
		$scope.showAddCaseBox[index]=false;
	}

	
	$scope.showModalForTestGroup = function() {
		$scope.createGroupStatus="";
		// $scope.createTestGroupsObj=createTestGroupsObj;
		$scope.createGroupStatus=null;
		createTestGroupsObj.isActive="Y";
		createTestGroupsObj.groupName="";
		//createTestGroupsObj.isActive="Y";
		//$("#inlineRadio1 customRadio").val('Y');
		//createTestGroupsObj.isActive='Y';
		$scope.createTestGroupsObj=createTestGroupsObj;
		$("#createTestGroup").modal();
	}
	$scope.addNewTestGroup = function(testSuitesObj) {
		
		createTestGroupsObj=$scope.createTestGroupsObj;
		createTestGroupsObj.projectId = $scope.projectId;
		$scope.createGroupStatuserroe=null;
		//alert(createTestGroupsObj.projectId+"===");
		createTestGroupsObj.testSuitesId = $scope.suitsId;
		createTestGroupsObj.groupName=$scope.createTestGroupsObj.groupName;
		createTestGroupsObj.createdBy = localStorage.getItem('userName');
		createTestGroupsObj.updatedBy = null;
		createTestGroupsObj.isActive = $scope.createTestGroupsObj.isActive;	
		//console.log(createTestGroupsObj);
		if(createTestGroupsObj.groupName==undefined)
		{
			$scope.createGroupStatuserroe="Kindly Provide TestGroup Name!!";
			return false;
		}
		if(createTestGroupsObj.testSuitesId==null)
		{		
			return false;
		}
		if((createTestGroupsObj.isActive==null) || (createTestGroupsObj.isActive==null)||(createTestGroupsObj.groupName==null) || (createTestGroupsObj.groupName==undefined ))
		{
			$scope.createGroupStatuserroe="Kindly provide all required field(s).";
			return false;
		}
		if(createTestGroupsObj.groupName.length >=50)
			{
			$scope.createGroupStatuserroe="TestGroup Name is too long.";
			return false;
			}
		else{
		testSuitesService.createTestGroup(createTestGroupsObj).then(
				function Success(response) {
					$(".se-pre-con").hide();
					if(response.data.serviceResponse==="New Test Group has been created Successfully")
						{
						alert("New Test Group name has been created successfully");
					$scope.createGroupStatus=response.data.serviceResponse;
					$("#createTestGroup").modal('hide');
					$scope.getTestCasesBysuites($scope.suitsId);
					
						}
					else{
						alert("Test Group already exists!!!");
						$scope.createGroupStatuserroe=response.data.serviceResponse;
						$("#createTestGroup").modal();
					}// $scope.getAllTestSuitesByProject($scope.projectId);
				});
		}
	}
	$scope.closeTestG=function()
	{
		$scope.updateGroupStatus="";
		//$scope.updateGroupStatuserror="";
		$("#updategropsterror").val()="";
		//$scope.updateGroupStatuserror="";
		$("#editTestGroup").modal('hide');
	}
	
	$scope.showModalForTestGroupEdit = function(groupId,suitesName) {
		$scope.createGroupStatus="";
		$scope.createTestGroupsObj=null;
		//$scope.suitesName=suitesName;
		$scope.updateGroupStatus="";
		$scope.updateGroupStatuserror="";
		testSuitesService.getTestGroupByGroupId(groupId).then(
				function Success(response) {
					$(".se-pre-con").hide();
					$scope.createTestGroupsObj=response.data.serviceResponse;
					$("#editTestGroup").modal();
				});
		
	}
	$scope.UpdateTestGroup = function() {
		createTestGroupsObj=$scope.createTestGroupsObj;
	
		createTestGroupsObj.updatedBy = localStorage.getItem('userName');
		console.log(createTestGroupsObj);
		$scope.updateGroupStatuserror=null;
		if(createTestGroupsObj.groupName==null)
		{	
		$scope.updateGroupStatuserror="Kindly provide Test Group Name";
		return false;
		}
		if(createTestGroupsObj.isActive==null )
		{
		$scope.updateGroupStatuserror="Kindly provide all required field";
		return false;
		
		}
		if(createTestGroupsObj.groupName.length >=50)
			{
			$scope.updateGroupStatuserror="Test Group Name is too long";
			return false;
			}
		else{
		testSuitesService.updateTestGroup(createTestGroupsObj).then(
				function Success(response) {
					$(".se-pre-con").hide();
					if(response.data.serviceResponse==="Test Group Updated Sucessfully")
						{
							alert("Test Group Updated Successfully")
							$scope.updateGroupStatus=response.data.serviceResponse;
							$("#editTestGroup").modal('hide');
						}
					else if(response.data.serviceResponse=="Duplicate Test Group Name")
						{
						alert("Test group name already exists!!!")
						$scope.updateGroupStatuserror="Test group name already exists!!!";
						$("#editTestGroup").modal()
						}
					else{
						$scope.updateGroupStatuserror="Test group name already exists!!!";
						$("#editTestGroup").modal();
					}
					
					$scope.getTestCasesBysuites($scope.suitsId);
				});
		}
	}
	$scope.clear = function(index) {
		$scope.caseDescription[index]="";
		$scope.showAddCaseBox[index]=false;
		$scope.caseAddStatus="";
		$scope.failStatus="";
		
	}
	$scope.saveTestCase = function(index,groupId) {
		alert($scope.caseDescription[index]);
		alert(groupId);
		$scope.caseDescription[index]="";				
	}
	
	/*---clear test sutes file--*/
$scope.clearFile = function(){
		document.getElementById("file").value = "";
		$('#cleanFile').fadeOut('slow');
	}
//	$scope.clearAll = function(){
//		 const file =
//             document.querySelector('.file');
//         file.value = '';
//	}
	
	//open update-testcase-modal
	$scope.editTestCase = function(testcase) {
		$scope.updateTestCaseStatus=null;
		$scope.testCaseEdit=testcase;
		if($scope.testCaseEdit.preparationTime)
		{
			if($scope.testCaseEdit.preparationTime ==="00:00:00")
			{
				$scope.testExcute="00:00:00";
			}
			else if($scope.testCaseEdit.preparationTime ===null){
				$scope.testExcute="00:00:00";
			}
			else{
				$scope.secnd=$scope.testCaseEdit.preparationTime;
				$scope.seconds = parseInt($scope.secnd, 10); // don't forget the second param
				$scope.hours   = Math.floor($scope.seconds / 3600);
				$scope.minutes = Math.floor(($scope.seconds - ($scope.hours * 3600)) / 60);
				$scope.seconds = $scope.seconds - ($scope.hours * 3600) - ($scope.minutes * 60);
							if ($scope.hours   < 10) {$scope.hours   = "0"+$scope.hours;}
				            if ($scope.minutes < 10) {$scope.minutes = "0"+$scope.minutes;}
				            if ($scope.seconds < 10) {$scope.seconds = "0"+$scope.seconds;}
				            $scope.testExcute   = $scope.hours+':'+$scope.minutes+':'+$scope.seconds;
			}
		}
		else{
			$scope.testExcute="00:00:00";
		}
		
		$("#editTestCase").modal();
		/*$scope.updateTestCase=true;*/
		$("#submitNew").css("display", "none");
		$("#submity").show();
	}
	
	$scope.confirmTestCaseUpdate=function()
	{
		$("#confirmation-dialog-update-test-case").modal();
	}
	//update testcase
	$scope.updateTestCase = function() {
		
		/*var starttime = new Date();
		var isotime = new Date((new Date(starttime)).toISOString() );
		var fixedtime = new Date(isotime.getTime()-(starttime.getTimezoneOffset()*60000));
*/
		var testCaseEdit=$scope.testCaseEdit;
		testCaseEdit.updatedBy=localStorage.getItem('userid');
		//testCaseEdit.updatedDate=fixedtime.toISOString().slice(0, 19).replace('T', ' ');

		if(testCaseEdit.testDescription!=null){
		testSuitesService.updateTestCase(testCaseEdit).then(
				function Success(response) {
					$(".se-pre-con").hide();
					$scope.updateTestCaseStatus=response.data.serviceResponse;
					if(response.data.serviceResponse==="Test Case Added Successfully")
						{
							alert("Testcase Updated Successfully ");
							$("#editTestCase").modal('hide');
						}
					else{
						$scope.updateTestCaseStatus=response.data.serviceResponse;
						$("#editTestCase").modal();
					}
					$("#UpdateTestCaseStatus").delay(5000).fadeOut();
				});
		}
	}
	/*$scope.updateTestCase = function() {
		var testCaseEdit=$scope.testCaseEdit;
		if(testCaseEdit.testDescription!=null){
		testSuitesService.updateTestCase(testCaseEdit).then(
				function Success(response) {
					$(".se-pre-con").hide();
					$scope.updateTestCaseStatus=response.data.serviceResponse;
					
				});
		}
			
	}*/
	
	$scope.clearTestCaseEdit=function()  //dayaedit
	{
	     $("#dynamic11").val('');
	    
	     $scope.getTestCasesBysuites($scope.suitsId);	
	     //$scope.getAllTestSuitesByProject($scope.projectId);
			console.log($scope.getTestCasesBysuites($scope.suitsId));
	     
	}
	
	
	$scope.getTestCasesBysuites = function(suitesId) {
		$scope.testCasesData="";
		testSuitesService.getTestCasesBysuites(suitesId).then(
				function Success(response) {
					$(".se-pre-con").hide();
					$scope.testCasesData=response.data.serviceResponse;
				});
	}
	$scope.confirmationDialogConfig = {};	  
	  $scope.confirmationDialogForDelete = function(testcase) {
		$scope.testcase=testcase;
	    $scope.confirmationDialogConfig = {
	      title: "Alert!!!",
	      message: "Are you sure you want to delete TC_"+testcase.srNo+" ?",
	      buttons: [{
	        label: "Yes",
	        action: "delete"
	      }]
	    };
	    $scope.showDialog(true);
	    
	  }
	  
	  $scope.executeDialogAction = function(action,testcase) {
		    if(typeof $scope[action] === "function") {
		    		  $scope[action](testcase.srID);
		    	}
		  } 
	  $scope.deleteTestSuites = function(testsuiteid, testsuitename) {
		  console.log(testsuiteid);
		
			  var txt=confirm("Click ok if you want to delete");
				
				if(txt== null || txt == "")
				{
					
				}
				else
				{
				testSuitesService.deleteTestSuites(localStorage.getItem('userName'), testsuiteid, testsuitename).then(
						function Success(response) {
							$(".se-pre-con").hide();
							if(response.data.serviceResponse =='Success')
								{
									alert("Test suites deleted successfully");
								}
							else{
								alert("Test suites not deleted");
							}
							$scope.getAllTestSuitesByProject($scope.projectId);
						});
				}
		    
		  }
		  $scope.delete = function(testcaseSID) {
		    console.log("Deleting...");
		    $scope.showDialog();
		    $scope.deleteTestCase(testcaseSID);
		  }
		
		  $scope.confirmationDialogForDeleteGroup = function(groupName,groupId) {
				$scope.groupId=groupId;
				$scope.showDialogGroup(true);
			    $scope.confirmationDialogConfig = {
			      title: "Alert!!!",
			      message: "Are you sure you want to delete "+groupName+" ?",
			      buttons: [{
			        label: "Yes",
			        action: "deleteGroup"
			      }]
			    };
			    $scope.testcaseSrID=[];
			  }
		  
		  $scope.executeDialogActionGroupDelete = function(action,groupId) {
			    if(typeof $scope[action] === "function") {
			    		  $scope[action](groupId);
			    	}
			  } 
		  $scope.deleteGroup = function(groupId) {
			    console.log("Deleting...");
			    testSuitesService.deletegroup(groupId).then(
						function Success(response) {
							if(response.data.serviceResponse =='Success')
							{
								alert("Test group deleted successfully");
							}
						else{
							alert("Test group not deleted");
						}
							$scope.showDialogGroup(false);
							$(".se-pre-con").hide();
							$scope.getTestCasesBysuites($scope.suitsId);
							
						}); 
			  }
		  
		  $scope.showDialog = function(flag) {
		    jQuery("#confirmation-dialog .modal").modal(flag ? 'show' : 'hide');
		  }
		  $scope.showDialogGroup = function(flag) {
			  jQuery("#confirmation-dialog-group .modal").modal(flag ? 'show' : 'hide');
		  }
		  
	$scope.getAllTestSuitesByProject($scope.projectId);
});