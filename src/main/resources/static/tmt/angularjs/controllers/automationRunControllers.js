demoApp.controller("automationRunController", function($scope,$interval,$state,$stateParams,$location,$anchorScroll, $rootScope, automationRunService, autoReportService) {
	
	$(document).ready(function(){
		checkcookie();
		var token=localStorage.getItem('tokenID');
		if (token==null|| token=="")
		{
		window.location.href = "../login.html";
		}
		});
	$rootScope.testCaseID='';
	$scope.img=false;
	$rootScope.checkHome();
	$scope.projectName = localStorage.getItem('projectName');
	$scope.projectId = localStorage.getItem('projectId');
	$scope.applicationName = localStorage.getItem('applicationName');
	$scope.start=true;
	$scope.stop=false;
	$scope.slavemachine='';
	var projectDetails={
			pId:null,
			pName:null,
			slaveMachineName:null,
			osType:null
	}	
	$scope.getTestSuitesName = function() {
		automationRunService.getTestSuitesName($scope.projectId,$scope.projectName).then(
				function Success(response) {
					var datasheets=[];
					var allDataSheets=[];
					$scope.allDataSheets="";
					$(".se-pre-con").hide();
					$scope.testSuitesName=response.data.serviceResponse;
					if($scope.testSuitesName.length!=0)
					{
					
						for (var r = 0; r < $scope.testSuitesName.length; r++) 
						{
							allDataSheets.push($scope.testSuitesName[r].testSuitesName);
							if($scope.testSuitesName[r].isRun=='Y'){
								datasheets.push($scope.testSuitesName[r].testSuitesName);
							}
						}
						$scope.allDataSheets=allDataSheets;
						$scope.updateList=datasheets;
						console.log($scope.allDataSheets);
						$('.sheetsList').val($scope.updateList).trigger('change');
						//$(".sheetsList > option").prop('selected','selected').trigger('change');
					}
				});

		}
	$scope.getslavemachine = function() {
		automationRunService.getslavemachine($scope.projectId,$scope.projectName).then(
				function Success(response) {
					$(".se-pre-con").hide();
					$(".loader").hide();
					$scope.machineName=response.data.serviceResponse;
				});

		}
	$scope.updateController =function(){
		alert("jjj");
	}
	$scope.updateJenkinsProject = function(slavemachine1) {
		projectDetails.pId=$scope.projectId;
		projectDetails.pName=$scope.projectName;
		//projectDetails.slaveMachineName=$("#smachine").val();
		projectDetails.slaveMachineName=slavemachine1;
		projectDetails.osType='window';
		automationRunService.updateJenkinsProject(projectDetails).then(
				function Success(response) {
					$(".se-pre-con").hide();
				});

		}
		
	$scope.disableTCButton = function(){
		$scope.y="FAIL";
		$('.start').attr("disabled", true);
			if($scope.datasheets && $scope.slavemachine){
					$('#tc').attr("disabled", false);
			}
			else{
				$('#tc').attr("disabled", true);	
			}
	}
	$scope.y="FAIL";
	$scope.disableRunButton = function(bolnVal,checkVal){ //1st parameter as bol value for disabled as true or false
		
		if(checkVal){									  //2nd parameter as bol value for checking datasheet and node machine
			if(bolnVal && $scope.y!="SUCCESS"){
				$('.start').attr("disabled", true);
			}
			else{
				if($scope.datasheets && $scope.slavemachine && $scope.y==="SUCCESS"){
					$('.start').attr("disabled", false);
				}
				else{
					/*if(!$scope.datasheets){
						alert("Kindly select a testsuite !!!");
					}
					elseif(!$scope.slavemachine){
						alert("Kindly select a slave machine !!!");
					}*/
					$('.start').attr("disabled", true);
				}
			}
		}
		else{
			if(bolnVal && $scope.y!="SUCCESS"){
				$('.start').attr("disabled", true);
			}
			else{
				$('.start').attr("disabled", false);
			}
		}
	}
	
	$scope.startRun = function() {
		
		//$('.start').attr("disabled", true);
		$scope.disableRunButton(true, true);
		
		projectDetails.slaveMachineName=null;
		var datasheetsListForRun=null;
		
		$scope.statusmsg="";
		$scope.statusimage="images/running.gif";
		$scope.img=true;
		$scope.start=false;
		$scope.stop=true;
		
		projectDetails.pId=$scope.projectId;
		projectDetails.pName=$scope.projectName;
		projectDetails.slaveMachineName=$('#smachine').val().split('--')[0];
		projectDetails.osType=$('#smachine').val().split('--')[1];
		//projectDetails.osType='window';
		
		datasheetsListForRun=$('#datasheets').val();
		
		if(datasheetsListForRun && projectDetails.slaveMachineName){
			
			automationRunService.startscript($scope.projectId,$scope.projectName,datasheetsListForRun,projectDetails).then(
					function Success(response) {
						$scope.startrun=response.data.serviceResponse;
						if($scope.startrun==="Duplicate Running")
							{
								$scope.statuscolor="green";
								$scope.statusimage="images/pass.png";
								$scope.stop=false;
								$scope.start=true;
								$scope.disableRunButton(false, true);
							}
						else if($scope.startrun==="Running"){
							$scope.statuscolor="green";
							$scope.statusimage="images/pass.png";
							$scope.stop=false;
							$scope.start=true;
							$scope.disableRunButton(false, true);
						}
						
						
					});
		}
		else{
			if(!datasheetsListForRun){
				alert("Kindly select a testsuite!!");
			}
			else if(!projectDetails.slaveMachineName){
				alert("Kindly select a slave machine!!")
			}
		}
		

	}
	$scope.stopRun = function() {
	automationRunService.stopscript($scope.projectId,$scope.projectName).then(
			function Success(response) {
				
				console.log(response);
			});

	}
	$scope.getCurrentRun = function() {
		automationRunService.getCurrentRun($scope.projectId,$scope.projectName).then(
				function Success(response) {
					//$(".se-pre-con").hide();
					console.log(response);
				});

	}
	$scope.currentRunDetails = function() {
		automationRunService.currentRunDetails($scope.projectId,$scope.projectName).then(
				function Success(response) {
					//$(".se-pre-con").hide();
					$scope.run=response.data.serviceResponse[0];
					$scope.runLogs=response.data.serviceResponse[1];
					//$(".start").attr("disabled", false);
					if($scope.run.status=="Success"){
						$scope.statusmsg="Run ID "+$scope.run.runId+" is completed successfully.";
						$scope.statuscolor="green";
						$scope.statusimage="images/pass.png";
						$scope.stop=false;
						$scope.start=true;
						$scope.disableRunButton(false, true);
					}
					else if($scope.run.status=="RUNNING")
					{
						$scope.statusmsg="Run ID "+$scope.run.runId+" is in progress.";
						$scope.statuscolor="green";
						$scope.statusimage="images/running.gif";
						$scope.stop=true;
						$scope.start=false;
						$scope.disableRunButton(true, false);
					}
					else if($scope.run.status=="")
					{
						$scope.statusmsg="Run ID "+$scope.run.runId+" is in progress";
						$scope.statuscolor="green";
						$scope.statusimage="images/running.gif";
						$scope.stop=true;
						$scope.start=false;
						$scope.disableRunButton(true, false);
					}
					else if($scope.run.status=="Stopped"){
						
						$scope.statusmsg="Run ID "+$scope.run.runId+" was stopped by User";
						$scope.statuscolor="red";
						$scope.statusimage="images/fail.png";
						$scope.stop=false;
						$scope.start=true;
						$scope.disableRunButton(false, true);
					}
					else if($scope.run.status=="Fail"){
						$scope.statusmsg="Run ID "+$scope.run.runId+" has failed.";
						$scope.statuscolor="red";
						$scope.statusimage="images/fail.png";
						$scope.stop=false;
						$scope.start=true;
						$scope.disableRunButton(false, true);
					}
					$scope.img=true;
				});

	}	
	 $scope.stopcall = function() {
		  $interval.cancel(getLogs);

		}
	var getLogs=$interval(function(){$scope.currentRunDetails()},10000);
	
	  $scope.$on('$destroy', function() {
	      $scope.stopcall();
	    });
	  $scope.getslavemachine();
	  $scope.getTestSuitesName();
	  $scope.currentRunDetails();
	  
	  $("#sortable tbody").on("click", ".testRunToReport", function(){
		  $rootScope.autoReportData=[];
		  $rootScope.prevRunIDData=[];
		  $rootScope.autoReportData[0]=$scope.run.runId;
		  $rootScope.autoReportData[1]=$scope.run.start_time;
		  $rootScope.autoReportData[2]=$scope.run.end_time;
		  $rootScope.autoReportData[3]=$scope.run.total_time;
		  $rootScope.autoReportData[4]=$scope.run.status;
		  $rootScope.testCaseID=$(this).closest('tr').children('td:eq(2)').text();
		  autoReportService.getAutomationRunReport($scope.projectName).then(function Success(response){
			  $scope.automationReport=response.data.serviceResponse;
			  for(var i=0; i<$scope.automationReport.length; i++){
	      		if($scope.automationReport[i][0] == $rootScope.autoReportData[0]){
	      			$rootScope.prevRunIDData=$scope.automationReport[i-1];
	      			break;
	      		}
	      	 }
		  });
		  
		  //$rootScope.prevRunIDData;
		  $(".nav-link").removeClass("active").removeClass("show");
		  $("#ReportsAuto .nav-link").addClass("active").addClass("show");
		  //$rootScope.getTestCaseIdName($rootScope.testCaseID);
		  $state.transitionTo('projectDetailsAuto.exereport');
		  
	  });
	  
	  $scope.screenshotRun={
				name:null,
				image:null
	  }
	  $scope.openScreenshotModal=function(screenshotName, screenshotImage){
			
			$scope.screenshotRun.name=screenshotName;
			$scope.screenshotRun.image=screenshotImage;
			$("#openScreenshot").modal('show');
			
	  }
	  
	 /* $scope.testConnectionRun=function(){
			$(".se-pre-con").show();
			automationRunService.testConnectionRun($scope.projectName,$scope.slavemachine).then(
				function Success(response){
					$(".se-pre-con").hide();
					$scope.y=response.data.serviceResponse;
					$scope.z=true;
					if($scope.y==="SUCCESS"){
				alert("Connection established..!'");
						$('.start').attr("disabled", false);
					}
					else{
				alert("Connection can't be established..!'");
						$('.start').attr("disabled", true);
					}
				});
	  }*/

$scope.testConnectionRun=function(){
			$(".se-pre-con").show();
			automationRunService.testConnectionRun($scope.projectName,$scope.slavemachine,$scope.applicationName).then(
				function Success(response){
					$(".se-pre-con").hide();
					$scope.y=response.data.serviceResponse;
					$scope.z=true;
					if($scope.y==="SUCCESS"){
						$scope.connectionmsg="Connection established..!";
						$scope.connectioncolor="text-success";
						$('.start').attr("disabled", false);
					}
					else{
						$scope.connectionmsg="Connection can't be established..!'";
						$scope.connectioncolor="text-danger";
						$('.start').attr("disabled", true);
					}
				});
	  }
	  
});
