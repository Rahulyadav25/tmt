demoApp.controller("projectOverviewController", function($scope,$state,$interval,$location,$anchorScroll, $rootScope, projectOverviewService,dashboardService,configurationservice) {
	/* //$location.hash('top');
     $anchorScroll();*/
	$(document).ready(function(){
		checkcookie();
		var token=localStorage.getItem('tokenID');
		if (token==null|| token=="")
		{
		window.location.href = "../login.html";
		}
		});
	$rootScope.checkHome();
	var countOfColumnList;
	var projectid=localStorage.getItem('projectId');
	var projectName1=localStorage.getItem('projectName');
	$scope.id= localStorage.getItem('userid');
	$scope.roleId = localStorage.getItem('userRole');
	$scope.projectwiserole=localStorage.getItem('projectwiserole');
	
	$scope.updateProjectBugStatus=function(){
		dashboardService.getProjectBugStatus(projectName1, projectid).then(function Success(response) {
			 $(".se-pre-con").fadeOut('slow');					 
		 });
	}
	 
	
	$("#iconList .nav-link").each(function(){
		$(this).removeClass("active");
	});
	
	
	
	$scope.conformDeleteDialog=function()
	{
		$("#confirmation-dialog-Edit-Project").modal();
	}
	
	
	
	
	$scope.projectNamedetail=null;
	var myChart;
	var myChart1;
	$scope.EditProjectObj = {
			projectID:null,
			createdBy : null,
			createdTime : null,
			isActive : null,
			isJiraEnable : null,
			projectLocation : null,
			projectName : null,
			senarioNumber : null,
			testCaseNumber : null,
			updatedTime : null,
			module : null,
			isAutomation : null,
			isMannual : null,
			submodule : null,
			platform : null,
			subplatform : null,
			version : null,
			updatedBy : null,
			projectManager : null,
			ProjectStartDate : null,
			ProjectEndDate : null,
			applicationName : null

		}
	$("#overViews a.nav-link").addClass("active");
	console.log(projectName1);
	$scope.showBugToolActive = function() {
		dashboardService.showBugToolActive().then(
				function Success(response) {
					//$(".se-pre-con").hide();
					//console.log(response);
					$scope.showActiveBugTracker=response.data.serviceResponse;
				});
	}
	$scope.showModalForEditProject = function() {
		$('.datepicker').datepicker({
			autoclose : true,
			format : 'mm/dd/yyyy',
			  orientation:'bottom',
				yearRange: "2000:2050",
			todayHighlight : true
		});
		dashboardService.getproject(projectName1,projectid).then(function Success(response) {
			console.log(response);
			$scope.projectNameEdit=response.data.serviceResponse.projectName;
			$scope.applicationNameEdit=response.data.serviceResponse.applicationName;
			/*$scope.isAuto=response.data.serviceResponse.isAutomation;
			$scope.isMannual=response.data.serviceResponse.isMannual;*/
			console.log(response.data.serviceResponse.isMannual);
			if(response.data.serviceResponse.isAutomation=='true' && response.data.serviceResponse.isMannual=='true')
			{
				$scope.isAutoEdit = true;
				$scope.isMannualEdit = true;
				}
			else if(response.data.serviceResponse.isAutomation=='true')
				{
				$scope.isAutoEdit = true;
				$scope.isMannualEdit = false;
				}
			else if(response.data.serviceResponse.isMannual==='true')
					
				{
				$scope.isAutoEdit = false;
				$scope.isMannualEdit = true;
				}
			else
				{
				$scope.isAutoEdit = false;
				$scope.isMannualEdit = true;
				}
			
			$scope.platFromEdit=response.data.serviceResponse.platform;
			$scope.projectIDEdit=response.data.serviceResponse.projectID;
			var d=new Date(response.data.serviceResponse.projectStartDate);
			var t=("00" + (d.getMonth() + 1)).slice(-2)+"/"+ ("00" + d.getDate()).slice(-2)+"/"+d.getFullYear();
			$scope.pStdAteEdit=t;
			var d1=new Date(response.data.serviceResponse.projectEndDate);
			var t1=("00" + (d1.getMonth() + 1)).slice(-2)+"/"+ ("00" + d1.getDate()).slice(-2)+"/"+d1.getFullYear();
			$scope.pEndAteEdit=t1;
			$scope.isActiveEdit=response.data.serviceResponse.isActive;
			$scope.bugTrackingEdit=response.data.serviceResponse.isJiraEnable;
		});
		$(".se-pre-con").hide();
		$("#EditProject").modal();
		
	}

	$scope.getProjectDetailssingle=function()
	{
		
		dashboardService.getSingleproject(projectid).then(function Success(response) {
			$scope.getprojectDetails=response.data.serviceResponse;
			//alert("response.data.serviceResponse..."+response.data.serviceResponse.delayTimeProject)
			console.log($scope.getprojectDetails);
			$scope.dlaybyproject=response.data.serviceResponse.delayTimeProject;
			$scope.projectNamedetail=response.data.serviceResponse.projectName;
			$scope.applicationNamedetail=response.data.serviceResponse.applicationName;
			console.log(response.data.serviceResponse.isMannual);
			if(response.data.serviceResponse.isActive=='Y')
			{
				$scope.projectstatusdetail="True";
			}
			else
				{
				$scope.projectstatusdetail="False";
				}
			if(response.data.serviceResponse.isAutomation=='true' && response.data.serviceResponse.isMannual=='true')
			{
				$scope.isAutodetail ="Automation";
				$scope.isMannualdetail = "Manual";
				}
			else if(response.data.serviceResponse.isAutomation=='true')
				{
				$scope.isAutodetail = "Automation"
				$scope.isMannualdetail = "";
				}
			else if(response.data.serviceResponse.isMannual==='true')
					
				{
				$scope.isAutodetail = "";
				$scope.isMannualdetail ="Manual";
				}
			else
				{
				$scope.isAutodetail =  "";
				$scope.isMannualdetail = "Manual";
				}
			
			$scope.platFromdetail=response.data.serviceResponse.platform;
			$scope.projectIDdetail=response.data.serviceResponse.projectID;
			var d=new Date(response.data.serviceResponse.projectStartDate);
			var t=("00" + (d.getMonth() + 1)).slice(-2)+"/"+ ("00" + d.getDate()).slice(-2)+"/"+d.getFullYear();
			$scope.pStdAtedetail=t;
			var d1=new Date(response.data.serviceResponse.projectEndDate);
			var t1=("00" + (d1.getMonth() + 1)).slice(-2)+"/"+ ("00" + d1.getDate()).slice(-2)+"/"+d1.getFullYear();
			$scope.pEndAtedetail=t1;
			$scope.isActivedetail=response.data.serviceResponse.isActive;
			$scope.bugTrackingdetail=response.data.serviceResponse.isJiraEnable;
			$scope.projectIDdetail=response.data.serviceResponse.projectID;
			var d=new Date(response.data.serviceResponse.projectStartDate);
			var t=("00" + (d.getMonth() + 1)).slice(-2)+"/"+ ("00" + d.getDate()).slice(-2)+"/"+d.getFullYear();
			$scope.pStdAtedetail=t;
			
			//lipsa
			localStorage.setItem('bugToolID',$scope.bugTrackingdetail);
			if($scope.bugTrackingdetail){
				$scope.updateProjectBugStatus();
			}
		});
		
		$(".se-pre-con").hide();
	}
	
	
	$scope.showMul1=function(){
		$(".test").select2({
			closeOnSelect : false,
			placeholder : "Runs",
			allowHtml: true,
			allowClear: true,
			tags: true,
			
		});
					
		};
		$scope.showMul1();	
		$scope.specificrunwisedata=function()
		{
			
			var applist=$('#columnlist').val();
			console.log(applist);
			if($scope.activerunlist==""){
				alert("No run available");
				return false;
				}
			else if(applist.length === 0)
			{
			alert("Please Select Run");
			return false;
			}

		else
			{
			projectOverviewService.runwisedetailscheck(applist).then(function Success(response)
			{
			$scope.testresultstatus=response.data.serviceResponse;
			console.log($scope.testresultstatus);
			$scope.plotRunDetailsChart();
			});
			}
			
			$(".se-pre-con").fadeOut('slow');		
			
		}
		
		$scope.selectMultiple=function(){
			//$scope.fromDate=null;
			 //$scope.toDate=null;
			 if($scope.selMul==true){
				 $('.test').val($scope.allRunList).trigger('change');
				 
				 }else{
					 
					 $(".test > option").prop('selected','selected').trigger('change');
				 }
		}
	$scope.runwisedetails=function()
	{
		projectOverviewService.getallActiveRun(projectid).then(function Success(response)
		{
			$(".se-pre-con").fadeOut('slow');
			$scope.activerunlist=response.data.serviceResponse;
			countOfColumnList = $scope.activerunlist.length;
		});
		
		projectOverviewService.projectrunwisedetails(projectid).then(function Success(response) {
			$(".se-pre-con").fadeOut('slow');
			$scope.testresultstatus=response.data.serviceResponse;
			console.log($scope.testresultstatus);
			$scope.plotRunDetailsChart();
		});
		projectOverviewService.projectwisefailstatus(projectid).then(function Success(response)
		{
			$(".se-pre-con").fadeOut('slow');
			$scope.projectwisefailstatus=response.data.serviceResponse;
			console.log($scope.projectwisefailstatus);
			var tempArrayRun=[];
			var tempArrayblocker=[];
			var tempArraytotalcritical=[];
			var tempArraytotalmajor=[];
			var tempArraytotalminor=[];
			var tempOpen=[];
			var tempReOpen=[];
			var tempReclosed=[];
			
			let tempArraycriticalopen=[];
			let tempArraycriticalclosed=[];
			let tempArraycriticalreopen=[];
			let tempArraycriticalresolved=[];
			let tempArrayblockeropen=[];
			let tempArrayblockerclosed=[];
			let tempArrayblockerreopen=[];
			let tempArrayblockerresolved=[];
			let tempArraymajoropen=[];
			let tempArraymajorclosed=[];
			let tempArraymajorreopen=[];
			let tempArraymajorresolved=[];
			let tempArrayminoropen=[];
			let tempArrayminorclosed=[];
			let tempArrayminorreopen=[];
			let tempArrayminorresolved=[];
			
			let severity=["Blocker","Critical","Major","Minor"];
			
		for(var i=0;i<$scope.projectwisefailstatus.length;i++){
			tempArrayRun.push($scope.projectwisefailstatus[i].runname);
			tempArraycriticalopen.push($scope.projectwisefailstatus[i].totalcriticalopen);
			tempArraycriticalclosed.push($scope.projectwisefailstatus[i].totalcriticalclosed);
			tempArraycriticalreopen.push($scope.projectwisefailstatus[i].totalcriticalreopen);
			tempArraycriticalresolved.push($scope.projectwisefailstatus[i].totalcriticalresolved);
			tempArrayblockeropen.push($scope.projectwisefailstatus[i].totalblockeropen);
			tempArrayblockerclosed.push($scope.projectwisefailstatus[i].totalblockerclosed);
			tempArrayblockerreopen.push($scope.projectwisefailstatus[i].totalblockerreopen);
			tempArrayblockerresolved.push($scope.projectwisefailstatus[i].totalblockerresolved);
			tempArraymajoropen.push($scope.projectwisefailstatus[i].totalmajoropen);
			tempArraymajorclosed.push($scope.projectwisefailstatus[i].totalmajorclosed);
			tempArraymajorreopen.push($scope.projectwisefailstatus[i].totalmajorreopen);
			tempArraymajorresolved.push($scope.projectwisefailstatus[i].totalmajorresolved);
			tempArrayminoropen.push($scope.projectwisefailstatus[i].totalminoropen);
			tempArrayminorclosed.push($scope.projectwisefailstatus[i].totalminorclosed);
			tempArrayminorreopen.push($scope.projectwisefailstatus[i].totalminorreopen);
			tempArrayminorresolved.push($scope.projectwisefailstatus[i].totalminorresolved);
			/*tempArrayblocker.push($scope.projectwisefailstatus[i].totalblocker);
			tempArraytotalcritical.push($scope.projectwisefailstatus[i].totalcritical);
			tempArraytotalmajor.push($scope.projectwisefailstatus[i].totalmajor);
			tempArraytotalminor.push($scope.projectwisefailstatus[i].totalminor);
			tempOpen.push($scope.projectwisefailstatus[i].totalopen);
			tempReOpen.push($scope.projectwisefailstatus[i].totalreopen);
			tempReclosed.push($scope.projectwisefailstatus[i].totalclosed);*/
		}
			var dates =tempArrayRun;
			  var ctx = document.getElementById('bar-chart');
			  myChart1 = new Chart(ctx, {
			      type: 'bar',
			      data: {
			          labels: tempArrayRun,
			          datasets: [{
			        	  stack: '0',
			        	  backgroundColor: "red",
			              //label: 'Open',
				          data: tempArrayblockeropen
			          },
			          {
			        	  stack: '0',
			        	  backgroundColor:"#90ee90",
			              //label: 'Closed',
				              data: tempArrayblockerclosed 
			          },
			          {
			        	  stack: '0',
			        	  backgroundColor:"#FFC200",
			              //label: 'Reopen',
			              data:tempArrayblockerreopen
			          },
			          {
			        	  stack: '0',
			        	  backgroundColor:"#006400",
			              //label: 'Resolved',
			              data:tempArrayblockerresolved
			          },
			          
			          {
			        	  stack: '1',
			              backgroundColor: "red",
			              label: 'Open',
			              data: tempArraycriticalopen
			          },
			          {
			        	  stack: '1',
			              backgroundColor:"#90ee90",
			              label: 'Closed',
			              data:tempArraycriticalclosed
			          },
			          {
			        	  stack: '1',
			              backgroundColor:"#FFC200",
			              label: 'Reopen',
			              data:tempArraycriticalreopen
			          },
			          {
			        	  stack: '1',
			              backgroundColor:"#006400",
			              label: 'Resolved',
			              data:tempArraycriticalresolved
			          },
			          
			          {
			        	  stack: '2',
			        	  backgroundColor: "red",
			              //label: 'Open',
				              data: tempArraymajoropen 
			          },
			          {
			        	  stack: '2',
			        	  backgroundColor:"#90ee90",
			              //label: 'Closed',
				              data: tempArraymajorclosed 
			          },
			          {
			        	  stack: '2',
			        	  backgroundColor:"#FFC200",
			              //label: 'Reopen',
			              data:tempArraymajorreopen
			          },
			          {
			        	  stack: '2',
			        	  backgroundColor:"#006400",
			              //label: 'Resolved',
			              data:tempArraymajorresolved
			          },
			          {
			        	  stack: '3',
			        	  backgroundColor: "red",
			              //label: 'Open',
				              data: tempArrayminoropen 
			          },
			          {
			        	  stack: '3',
			        	  backgroundColor:"#90ee90",
			              //label: 'Closed',
				              data: tempArrayminorclosed 
			          },
			          {
			        	  stack: '3',
			        	  backgroundColor:"#FFC200",
			              //label: 'Reopen',
			              data:tempArrayminorreopen
			          },
			          {
			        	  stack: '3',
			        	  backgroundColor:"#006400",
			              //label: 'Resolved',
			              data:tempArrayminorresolved
			          }
			        	  /*{
			              stack: '0',
			              backgroundColor: "#d0bfff",
			              label: 'Open',
			              data: tempOpen
			          },
			          
			          {
			            stack: '0',
			              backgroundColor:"#748ffc",
			              label: 'Reopen',
			              data:tempReOpen
			          },
			          {
				            stack: '0',
				              backgroundColor: "#d73e4d",
				              label: 'Closed',
				              data: tempReclosed
				          },
				          {
				        	  stack: '1',
				             backgroundColor: '#eebefa',
				              label: 'Blocker',
				              data: tempArrayblocker
				          },
			              {
				        	  stack: '1',
				              backgroundColor: '#FF4500',
				              label: 'Critical',
				              data: tempArraytotalcritical
				          },
				          {
				        	  stack: '1',
				              backgroundColor: '#f59f00',
				              label: 'Major',
				              data: tempArraytotalmajor
				          },
				          {
				        	  stack: '1',
				              backgroundColor: '#bac8ff',
				              label: 'Minor',
				              data: tempArraytotalminor
				          }
			          */]
			      },
			      options: {
			          scales: {
			              yAxes: [{
			                  ticks: {
			                      beginAtZero:true
			                  },
			                  scaleLabel: {
			                      display: true,
			                      scaleFontSize: 40,
					               fontColor :'chocolate',	
			                      labelString: 'Test Run Defect Count'
			                    },
			                 stacked: true
			              }],
			              xAxes: [{
			                  ticks: {
			                      beginAtZero:true
			                  },
			                  scaleLabel: {
			                      display: true,
			                      scaleFontSize: 40,
					               fontColor :'chocolate',	
			                      labelString: 'Test Run(Top 5) '
			                    },
			                 stacked: true
			              }],

			          },
			      //responsive: true,
			      legend:{
				      display:true,
				      position:'bottom',
				      labels: {
			                filter: function(item, chart) {
			                    // Logic to remove a particular legend item goes here
			                    return !(item.text==null);
			                }
			            }
				    },
		            "hover": {
		                "animationDuration": 0
		              },
		               "animation": {
		                  "duration": 1,
		                "onComplete": function() {
		                  var chartInstance = this.chart,
		                    ctx = chartInstance.ctx;
		   
		                  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
		                  ctx.textAlign = 'center';
		                  ctx.textBaseline = 'top';
		                  
		                  let count=0;
		                  this.data.datasets.forEach(function(dataset, i) {
		                    var meta = chartInstance.controller.getDatasetMeta(i);
		                    let severityStack=meta.stack;
		                    count++;
		                    meta.data.forEach(function(bar, index) {
		                    	if((index==0)&&(count==4)){
		                    		let data = severity[severityStack];
		                    		ctx.fillText(data, bar._model.x, bar._model.y - 6);
		                    		count=0;
		                 	    }
		                    });
		                  });
		                }
		              },
		              title: {
		                  display: false,
		                  text: ''
		              },
			      },
			  });
			 
		});
		projectOverviewService.getProjectDetails(projectid).then(function Success(response)
		{
			$(".se-pre-con").fadeOut('slow');
			$scope.getProjectDetails=response.data.serviceResponse;
			
		});
		
			projectOverviewService.testrunwisedetailsoverview(projectid).then(function Success(response) {
				var acmArray=[];
				$scope.testresults=response.data.serviceResponse;
				acmArray=$scope.testresults;
				console.log(acmArray);
				var table = $('#example').DataTable();
				table.destroy();
				$("#example").DataTable({
					 scrollX: true,
			        data:acmArray,
			        "order": [[ 0, "desc" ]],
			       
			       // "pagingType": "full_numbers",
			        dom: 'Bfrtip',
			        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			        "paging": false,
			        select: true,
			        buttons: [
			        	{
			           extend: 'collection',
			           text: 'Export',
						buttons: [
						   {
						       extend: "excelHtml5",
						       fileName:  "CustomFileName" + ".xlsx",
								title: projectName1+'Overview',
						      
						   },
						   {
						       extend: "csvHtml5",
						       fileName:  "CustomFileName" + ".csv",
								title: projectName1+'Overview',
						   },
						   {
						       extend: "pdfHtml5",
						       fileName:  "CustomFileName" + ".pdf",
								title: projectName1+'Overview',
						       orientation : 'landscape',
				                pageSize : 'LEGAL'
						       /*exportData: {decodeEntities:true}*/
				                	
						   }
						  ]
			        	}
			    ],
			    "paging": true,
			    });
			/** Graph Data Start */
			});
			
	}
	
	$scope.plotRunDetailsChart=function(testresultstatus){
		
			var tempArrayRun=[];
			var tempArraytotalcases=[];
			var tempArraycompleted=[];
			var tempArraytotalnotstarted=[];
			var tempArrayonhold=[];
			var tempArrayunderreview=[];
			var tempArrayNe=[];
			var tempArrayNA=[];
//			var temarraypass=[];
			var temarrayincomplete=[];
		for(var i=0;i<$scope.testresultstatus.length;i++){
			tempArrayRun.push($scope.testresultstatus[i].runname);
			tempArraytotalcases.push($scope.testresultstatus[i].totalcases);
			tempArraycompleted.push($scope.testresultstatus[i].totalcompleted);
			tempArraytotalnotstarted.push($scope.testresultstatus[i].totalnotstarted);
			tempArrayonhold.push($scope.testresultstatus[i].totalonhold);
			tempArrayunderreview.push($scope.testresultstatus[i].totalunderreview);
			tempArrayNe.push($scope.testresultstatus[i].totalNe);
			tempArrayNA.push($scope.testresultstatus[i].totalNA)
			temarrayincomplete.push($scope.testresultstatus[i].incomplete)

		}
		var barOptions_stacked = {
				tooltips: {
					//enabled:false,
			            displayColors:true,
			            backgroundColor: '#227799'
			             },
			    hover :{
			        animationDuration:0
			    },
			    scales: {
			       
			        xAxes: [{
			              barThickness:1,
			            ticks: {
			                beginAtZero:true,
			                fontFamily: "'Open Sans Bold', sans-serif",
			                fontSize:11
			            },
			            scaleLabel:{
			                display:false
			            },
			            gridLines: {
			            }, 
			            stacked: true
			        }],
			        yAxes: [{
			          
			            gridLines: {
			                display:false,
			                color: "#fff",
			                zeroLineColor: "#fff",
			                zeroLineWidth: 0
			            },
			            ticks: {
			                fontFamily: "'Open Sans Bold', sans-serif",
			                fontSize:11
			            },
			            barPercentage: 0.5,
			            stacked: true
			        }]
			    },
			    legend:{
			      display:true,
			      position:'bottom'
			    },
			    pointLabelFontFamily : "Quadon Extra Bold",
			    scaleFontFamily : "Quadon Extra Bold",
			    
			};

			var ctx = document.getElementById("Chart1");
			myChart = new Chart(ctx, {
			    type: 'horizontalBar',
			    data: {
			        labels: tempArrayRun,
			        
			        datasets: [{
			            label:'Completed',
			            data: tempArraycompleted,
			            backgroundColor: "Green",
			            hoverBackgroundColor: "#036903"
			        },{
			            label:'Not Started',
			            data: tempArraytotalnotstarted,
			            backgroundColor: "gray",
			            //hoverBackgroundColor: "rgba(140,85,100,1)"
			        },{
			            label:'On Hold',
			            data: tempArrayonhold,
			            backgroundColor: "#ffbf00",
			            //hoverBackgroundColor: "rgba(46,185,235,1)"
			        },{
			            label:'Under Review',
			            data: tempArrayunderreview,
			            backgroundColor: "#9C27B0",
			            //hoverBackgroundColor: "rgba(46,185,235,1)"
			        },{
			            label:'N/A',
			            data: tempArrayNA,
			            backgroundColor: "#1131DF",
			            //hoverBackgroundColor: "rgba(46,185,235,1)"
			        }, {
				            label:'incomplete',
				            data: temarrayincomplete,
				            backgroundColor: "#aa483b",
				            //hoverBackgroundColor: "rgba(46,185,235,1)"
				        //hoverBackgroundColor: "rgba(46,185,235,1)"
				       
			        }]
			    },
			    options : {
					   scales: {
					     
					       xAxes: [{
					             barThickness:1,
					           ticks: {
					               beginAtZero:true,
					               fontFamily: "'Open Sans Bold', sans-serif",
					               fontSize:11
					           },
					           scaleLabel:{
					               display:true,
					               scaleFontSize: 40,
					               fontColor :'chocolate',	
					               labelString :"Test Run Count(Top 5)",
					           },
					           gridLines: {
					           },
					           stacked: true
					       }],
					       yAxes: [{
					         
					           gridLines: {
					               display:true,
					               color: "#0000",
					               
					           },
					           scaleLabel:{
					               display:true,
					               scaleFontSize: 40,
					               fontColor :'chocolate',	
					               labelString :"Test Run(Top 5)",
					           },
					           ticks: {
					               fontFamily: "'Open Sans Bold', sans-serif",
					               fontSize:11
					           },
					           barPercentage: 0.5,
					           stacked: true
					       }]
					   },
					  
					   legend:{
					     display:true,
					     position:'bottom'
					   },
					   barOptions_stacked,
					   },
			    
			});
			/*var tempArrayRun=[];
			var tempArraytotalcases=[];
			var tempArraycompleted=[];
			var tempArraytotalnotstarted=[];
			var tempArrayonhold=[];
			var tempArrayunderreview=[];
		for(var i=0;i<$scope.testresultstatus.length;i++){
			tempArrayRun.push($scope.testresultstatus[i].runname);
			tempArraytotalcases.push($scope.testresultstatus[i].totalcases);
			tempArraycompleted.push('{Y:'+$scope.testresultstatus[i].totalcompleted+',label:'+ $scope.testresultstatus[i].runname+'}');
			tempArraytotalnotstarted.push('{Y:'+$scope.testresultstatus[i].totalnotstarted+',label:'+ $scope.testresultstatus[i].runname+'}');
			tempArrayonhold.push('{Y:'+$scope.testresultstatus[i].totalonhold+',label:'+ $scope.testresultstatus[i].runname+'}');
			tempArrayunderreview.push('{Y:'+$scope.testresultstatus[i].totalunderreview+',label:'+ $scope.testresultstatus[i].runname+'}');
		}*/
	}

	$("select").on("select2:select", function (evt) {
		 // $scope.fromDate=null;
		//	 $scope.toDate=null;
		var element = evt.params.data.element;
		  var $element = $(element);
		  $element.detach();
		  $(this).append($element);
		  $(this).trigger("change");
		  var count = $(this).select2('data').length 
	
		 if(count==countOfColumnList){
			$("#selectCheckBox").prop('checked', true);
		}else{
			$("#selectCheckBox").prop('checked', false);
		}
		
		});
	$("select").on("select2:unselect", function (evt) {
		//$scope.fromDate=null;
		 //$scope.toDate=null;
		$("#selectCheckBox").prop('checked', false);
		
		  
		});	
	
/*	$("select").on("change", function (evt) {
		  
	      alert("sfsdfsd change");
		$("#selectCheckBox").prop('checked', false);
		  
		});	*/
	$("select").on('select2:close', function (evt) {
		  // $scope.fromDate=null;
		   //   $scope.toDate=null;
	       $('#select').blur();
	       $scope.specificrunwisedata();
	    
	})
	.on('select2:blur', function(evt) {
	     
	}); 
		
	$('#selectCheckBox').change(function() {
		//   $scope.fromDate=null;
		 //     $scope.toDate=null;
        if(this.checked) {
         
        	 $(".test > option").prop('selected','selected').trigger('change');
		 
        }else{
        	 $('.test').val($scope.allRunList).trigger('change');
			
		 }
    });
	
		/*configurationservice.showDashboardprojectwise(projectid).then(function Success(response) {
			$scope.getAccesslist=response.data.serviceResponse;
		console.log($scope.getAccesslist);
			var tempArrayDate=[];
			var tempArrayPass=[];
			var tempArrayFail=[];
			var tempArrayBlocker=[];
			var tempArrayRetest=[];
		for(var i=0;i<$scope.getAccesslist.length;i++){
			tempArrayDate.push($scope.getAccesslist[i].specificdate);
			tempArrayPass.push($scope.getAccesslist[i].pass);
			tempArrayFail.push($scope.getAccesslist[i].fail);
			tempArrayBlocker.push($scope.getAccesslist[i].blocker);
			tempArrayRetest.push($scope.getAccesslist[i].retest);
		}
		console.log(tempArrayDate);
		console.log(tempArrayPass);
		console.log(tempArrayFail);
		am4core.ready(function() {
			
	
/*	$scope.showGraph = function() {
		
		configurationservice.showDashboardprojectwise(projectid).then(function Success(response) {
			$scope.getAccesslist=response.data.serviceResponse;
		console.log($scope.getAccesslist);
			var tempArrayDate=[];
			var tempArrayPass=[];
			var tempArrayFail=[];
			var tempArrayBlocker=[];
			var tempArrayRetest=[];
		for(var i=0;i<$scope.getAccesslist.length;i++){
			tempArrayDate.push($scope.getAccesslist[i].specificdate);
			tempArrayPass.push($scope.getAccesslist[i].pass);
			tempArrayFail.push($scope.getAccesslist[i].fail);
			tempArrayBlocker.push($scope.getAccesslist[i].blocker);
			tempArrayRetest.push($scope.getAccesslist[i].retest);
		}
		console.log(tempArrayDate);
		console.log(tempArrayPass);
		console.log(tempArrayFail);
		
		});
		configurationservice.showrunwise(projectid).then(function Success(response) {
			$scope.getRunwisedata=response.data.serviceResponse;
			console.log($scope.getRunwisedata);
		
		});
	}*/
	//$scope.showGraph();
	$('.datepicker').datepicker({
		autoclose : true,
		format : 'mm/dd/yyyy',
		 orientation:'bottom',
			yearRange: "2000:2050",
		todayHighlight : true
	});	

	$scope.Access=function(){
	configurationservice.getRoletypebyproject(projectid,$scope.id).then(function Success(response) {
		$scope.roletype=response.data.serviceResponse;
		console.log($scope.roletype);

		localStorage.setItem('projectwiserole',$scope.roletype);
		 $scope.user_role= localStorage.getItem('userRole');
			if($scope.user_role==="100")
			{
			$("#testRun").css("display","unset");
			$("#todo").css("display","unset");
			$("#testSuites").css("display","unset");
			$("#Automation").css("display","unset");
			}
			else
				{
				if($scope.roletype==="Manager")
				{
				$("#testRun").css("display","unset");
				$("#todo").css("display","unset");
				$("#testSuites").css("display","unset");
				$("#Automation").css("display","unset");
				}	
			else if($scope.roletype==="Tester")
			{
				$("#todo").css("display","unset");
				$("#testSuites").css("display","unset");
				$("#Automation").css("display","unset");
				$("#testRun").css("display","none");
			}
			else if($scope.roletype==="Readonly")
			{
				$("#testRun").css("display","none");
		        $("#testSuites").css("display","none");
		        $("#todo").css("display","none");
			}
		}
	
		});
	}
	
	$scope.runwisedetails();
	$scope.Access();
	$scope.EditProject = function() {
		$scope.EditProjectObj.projectID =projectid;
		$scope.EditProjectObj.projectName = $scope.projectNameEdit;
		$scope.EditProjectObj.applicationName = $scope.applicationNameEdit;
		$scope.EditProjectObj.isAutomation = $scope.isAutoEdit;
		$scope.EditProjectObj.isMannual = $scope.isMannualEdit;
		$scope.EditProjectObj.platform = $scope.platFromEdit;
		
		$scope.EditProjectObj.projectStartDate = $scope.pStdAteEdit;
		$scope.EditProjectObj.projectEndDate = $scope.pEndAteEdit;
		$scope.EditProjectObj.isActive = $scope.isActiveEdit;
		$scope.EditProjectObj.isJiraEnable = $scope.bugTrackingEdit;
		
		if(projectid !=null && $scope.projectNameEdit !=null && $scope.applicationNameEdit !=null &&($scope.isAutoEdit !=false || $scope.isMannualEdit !=false)
				&& $scope.pStdAteEdit !=null && $scope.pEndAteEdit !=null && $scope.isActiveEdit !=null && $scope.bugTrackingEdit !=null)
			{
		
		var txt=confirm("Click ok if you want to edit");
		if(txt== null || txt == "")
			{
			
			}
		else
			{		
			if(projectid !=null && $scope.projectNameEdit !=null && $scope.applicationNameEdit !=null &&($scope.isAutoEdit !=false || $scope.isMannualEdit !=false)
					&& $scope.pStdAteEdit !=null && $scope.pEndAteEdit !=null && $scope.isActiveEdit !=null && $scope.bugTrackingEdit !=null)
				{
			dashboardService.EditProject($scope.EditProjectObj).then(
				function Success(response) {
					
					$scope.status=response.data.serviceResponse;
					console.log($scope.status);
					//$('.row input').val(" ");
					$scope.reloadprojectoverview();
					if($scope.status==='Success')
						{
						localStorage.removeItem('projectName');
						localStorage.setItem('projectName',$scope.EditProjectObj.projectName);
						localStorage.setItem('projectNames',$scope.EditProjectObj.projectName);
						
						alert("Project edited successfully");
						$scope.refereshprojectName();
					/*	$scope
						.getDashboad();*/
						}
					else{
						alert("Project Name already exists");
					}
					
					
					$("#EditProject").modal('hide');
					
				
					$scope.reloadprojectoverview();
					//$state.transitionTo('dashboard');
					//$(".modal-backdrop").modal('hide');
					
					/*	$(".se-pre-con").hide();
				*/
				});
				}
			else{
				$("#EditProject").modal();

				alert("Kindly fill all the required fields");
			}
			}
		 
		}
		else{
			$("#EditProject").modal();
			alert("Kindly fill all the required fields");
		}
	}

	
	$scope.cleardata=function(){
		$('.test').val(null).trigger('change');
		$('#selectCheckBox').prop('checked',false);
	}
	$scope.datewiserun=function(){
	//	var applist=$('#columnlist').val();
		
		$('.test').val(null).trigger('change');
		$('#selectCheckBox').prop('checked',false);
		var projectid=localStorage.getItem('projectId');
		
		/*if($scope.activerunlist==""){
		alert("No run available");
		return false;
		}
		else if(applist.length === 0)
		{
			alert("Please Select Run");
			return false;
		}*/
		
		if($scope.fromDate==null){
		alert("Kindly select project start date");
		return false;

		}
		if($scope.toDate==null){
		alert("Kindly select project end date");
		return false;
		}
		
		if($scope.fromDate>$scope.toDate){
		alert("Start date should not be greater than end date");
		return false;
		}
		
		projectOverviewService.datewiserun($scope.fromDate,$scope.toDate,projectid).then(function Success(response) {

			$scope.testresultstatus=response.data.serviceResponse;
			console.log($scope.testresultstatus);
			$scope.plotRunDetailsChart();
			
		$(".se-pre-con").hide();
		});
		projectOverviewService.datewiserunfailstatus($scope.fromDate,$scope.toDate,projectid).then(function Success(response)
		{

			$(".se-pre-con").fadeOut('slow');
			$scope.projectwisefailstatus=response.data.serviceResponse;
			console.log($scope.projectwisefailstatus);
			var tempArrayRun=[];
			var tempArrayblocker=[];
			var tempArraytotalcritical=[];
			var tempArraytotalmajor=[];
			var tempArraytotalminor=[];
			var tempOpen=[];
			var tempReOpen=[];
			var tempReclosed=[];
			
			let tempArraycriticalopen=[];
			let tempArraycriticalclosed=[];
			let tempArraycriticalreopen=[];
			let tempArraycriticalresolved=[];
			let tempArrayblockeropen=[];
			let tempArrayblockerclosed=[];
			let tempArrayblockerreopen=[];
			let tempArrayblockerresolved=[];
			let tempArraymajoropen=[];
			let tempArraymajorclosed=[];
			let tempArraymajorreopen=[];
			let tempArraymajorresolved=[];
			let tempArrayminoropen=[];
			let tempArrayminorclosed=[];
			let tempArrayminorreopen=[];
			let tempArrayminorresolved=[];
			
			let severity=["Blocker","Critical","Major","Minor"];
			
		for(var i=0;i<$scope.projectwisefailstatus.length;i++){
			tempArrayRun.push($scope.projectwisefailstatus[i].runname);
			tempArraycriticalopen.push($scope.projectwisefailstatus[i].totalcriticalopen);
			tempArraycriticalclosed.push($scope.projectwisefailstatus[i].totalcriticalclosed);
			tempArraycriticalreopen.push($scope.projectwisefailstatus[i].totalcriticalreopen);
			tempArraycriticalresolved.push($scope.projectwisefailstatus[i].totalcriticalresolved);
			tempArrayblockeropen.push($scope.projectwisefailstatus[i].totalblockeropen);
			tempArrayblockerclosed.push($scope.projectwisefailstatus[i].totalblockerclosed);
			tempArrayblockerreopen.push($scope.projectwisefailstatus[i].totalblockerreopen);
			tempArrayblockerresolved.push($scope.projectwisefailstatus[i].totalblockerresolved);
			tempArraymajoropen.push($scope.projectwisefailstatus[i].totalmajoropen);
			tempArraymajorclosed.push($scope.projectwisefailstatus[i].totalmajorclosed);
			tempArraymajorreopen.push($scope.projectwisefailstatus[i].totalmajorreopen);
			tempArraymajorresolved.push($scope.projectwisefailstatus[i].totalmajorresolved);
			tempArrayminoropen.push($scope.projectwisefailstatus[i].totalminoropen);
			tempArrayminorclosed.push($scope.projectwisefailstatus[i].totalminorclosed);
			tempArrayminorreopen.push($scope.projectwisefailstatus[i].totalminorreopen);
			tempArrayminorresolved.push($scope.projectwisefailstatus[i].totalminorresolved);
			/*tempArrayRun.push($scope.projectwisefailstatus[i].runname);
			tempArrayblocker.push($scope.projectwisefailstatus[i].totalblocker);
			tempArraytotalcritical.push($scope.projectwisefailstatus[i].totalcritical);
			tempArraytotalmajor.push($scope.projectwisefailstatus[i].totalmajor);
			tempArraytotalminor.push($scope.projectwisefailstatus[i].totalminor);
			tempOpen.push($scope.projectwisefailstatus[i].totalopen);
			tempReOpen.push($scope.projectwisefailstatus[i].totalreopen);
			tempReclosed.push($scope.projectwisefailstatus[i].totalclosed);*/
		}
		var dates =tempArrayRun;
		  var ctx = document.getElementById('bar-chart');
		  myChart1 = new Chart(ctx, {
		      type: 'bar',
		      data: {
		          labels: tempArrayRun,
		          datasets: [{
		        	  stack: '0',
		        	  backgroundColor: "red",
		              //label: 'Open',
			          data: tempArrayblockeropen
		          },
		          {
		        	  stack: '0',
		        	  backgroundColor:"#90ee90",
		              //label: 'Closed',
			              data: tempArrayblockerclosed 
		          },
		          {
		        	  stack: '0',
		        	  backgroundColor:"#FFC200",
		              //label: 'Reopen',
		              data:tempArrayblockerreopen
		          },
		          {
		        	  stack: '0',
		        	  backgroundColor:"#006400",
		              //label: 'Resolved',
		              data:tempArrayblockerresolved
		          },
		          
		          {
		        	  stack: '1',
		              backgroundColor: "red",
		            //  label: 'Open',
		              data: tempArraycriticalopen
		          },
		          {
		        	  stack: '1',
		              backgroundColor:"#90ee90",
		              //label: 'Closed',
		              data:tempArraycriticalclosed
		          },
		          {
		        	  stack: '1',
		              backgroundColor:"#FFC200",
		             // label: 'Reopen',
		              data:tempArraycriticalreopen
		          },
		          {
		        	  stack: '1',
		              backgroundColor:"#006400",
		              //label: 'Resolved',
		              data:tempArraycriticalresolved
		          },
		          
		          {
		        	  stack: '2',
		        	  backgroundColor: "red",
		              //label: 'Open',
			              data: tempArraymajoropen 
		          },
		          {
		        	  stack: '2',
		        	  backgroundColor:"#90ee90",
		              //label: 'Closed',
			              data: tempArraymajorclosed 
		          },
		          {
		        	  stack: '2',
		        	  backgroundColor:"#FFC200",
		              //label: 'Reopen',
		              data:tempArraymajorreopen
		          },
		          {
		        	  stack: '2',
		        	  backgroundColor:"#006400",
		              //label: 'Resolved',
		              data:tempArraymajorresolved
		          },
		          {
		        	  stack: '3',
		        	  backgroundColor: "red",
		              //label: 'Open',
			              data: tempArrayminoropen 
		          },
		          {
		        	  stack: '3',
		        	  backgroundColor:"#90ee90",
		              //label: 'Closed',
			              data: tempArrayminorclosed 
		          },
		          {
		        	  stack: '3',
		        	  backgroundColor:"#FFC200",
		              //label: 'Reopen',
		              data:tempArrayminorreopen
		          },
		          {
		        	  stack: '3',
		        	  backgroundColor:"#006400",
		              //label: 'Resolved',
		              data:tempArrayminorresolved
		          }
		        	  /*{
		              stack: '0',
		              backgroundColor: "#d0bfff",
		              label: 'Open',
		              data: tempOpen
		          },
		          
		          {
		            stack: '0',
		              backgroundColor:"#748ffc",
		              label: 'Reopen',
		              data:tempReOpen
		          },
		          {
			            stack: '0',
			              backgroundColor: "#d73e4d",
			              label: 'Closed',
			              data: tempReclosed
			          },
			          {
			        	  stack: '1',
			             backgroundColor: '#eebefa',
			              label: 'Blocker',
			              data: tempArrayblocker
			          },
		              {
			        	  stack: '1',
			              backgroundColor: '#FF4500',
			              label: 'Critical',
			              data: tempArraytotalcritical
			          },
			          {
			        	  stack: '1',
			              backgroundColor: '#f59f00',
			              label: 'Major',
			              data: tempArraytotalmajor
			          },
			          {
			        	  stack: '1',
			              backgroundColor: '#bac8ff',
			              label: 'Minor',
			              data: tempArraytotalminor
			          }
		          */]
		      },
		      options: {
		          scales: {
		              yAxes: [{
		                  ticks: {
		                      beginAtZero:true
		                  },
		                  scaleLabel: {
		                      display: true,
		                      scaleFontSize: 40,
				               fontColor :'chocolate',	
		                      labelString: 'Test Run Defect Count'
		                    },
		                 stacked: true
		              }],
		              xAxes: [{
		                  ticks: {
		                      beginAtZero:true
		                  },
		                  scaleLabel: {
		                      display: true,
		                      scaleFontSize: 40,
				               fontColor :'chocolate',	
		                      labelString: 'Test Run(Top 5) '
		                    },
		                 stacked: true
		              }],

		          },
		      //responsive: true,
		      legend:{
			      display:true,
			      position:'bottom',
			      labels: {
		                filter: function(item, chart) {
		                    // Logic to remove a particular legend item goes here
		                    return !(item.text==null);
		                }
		            }
			    },
	            "hover": {
	                "animationDuration": 0
	              },
	               "animation": {
	                  "duration": 1,
	                "onComplete": function() {
	                  var chartInstance = this.chart,
	                    ctx = chartInstance.ctx;
	   
	                  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
	                  ctx.textAlign = 'center';
	                  ctx.textBaseline = 'top';
	                  
	                  let count=0;
	                  this.data.datasets.forEach(function(dataset, i) {
	                    var meta = chartInstance.controller.getDatasetMeta(i);
	                    let severityStack=meta.stack;
	                    count++;
	                    meta.data.forEach(function(bar, index) {
	                    	if((index==0)&&(count==4)){
	                    		let data = severity[severityStack];
	                    		ctx.fillText(data, bar._model.x, bar._model.y - 6);
	                    		count=0;
	                 	    }
	                    });
	                  });
	                }
	              },
	              title: {
	                  display: false,
	                  text: ''
	              },
		      },
		  });
		});
		
	}
	$scope.reloadprojectoverview=function()
	{
		$scope.getProjectDetailssingle();
		$scope.runwisedetails();
		$scope.Access();
		$(".se-pre-con").hide();
	}
	
	$scope.hidefmReadonly =false;
	localStorage.setItem('projectwiserole',$scope.roletype);
	$scope.projectwiserole=localStorage.getItem('projectwiserole');
	
	$scope.chckTabAssign=function()
	{
		 if($scope.roleId==="100" || localStorage.getItem('projectwiserole')==="Manager" || localStorage.getItem('projectwiserole')==="Tester")
			{
			 
			 $scope.url=='/testSuites'
				 $state.transitionTo('projectDetails.testSuites');  
			 $scope.hidefmReadonly =true;
			}
			else if($scope.projectwiserole==="Readonly")
			{
				$scope.hidefmReadonly =false;
			}
		
	}
	
	
	$scope.chckTRTabAssign=function()
	{
		 if($scope.roleId==="100" || localStorage.getItem('projectwiserole')==="Manager" )
			{
			 $scope.url=='/testRun'
				 $state.transitionTo('projectDetails.testRun');  
			 $scope.hidefmReadonly =true;
			}
			else if(localStorage.getItem('projectwiserole')==="Readonly" || localStorage.getItem('projectwiserole')==="Tester")
			{
				$scope.hidefmReadonly =false;
			}
	}
	
	

});