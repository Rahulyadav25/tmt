demoApp.controller("sidemenuController", function($scope,$interval,$state, $location,$http, $rootScope, userService) {
	
	$rootScope.createAlert=function(title, summary, details, severity, 
			dismissible, autoDismiss, appendToId) {
		  var iconMap = {
		    info: "fa fa-info-circle",
		    success: "fa fa-thumbs-up",
		    warning: "fa fa-exclamation-triangle",
		    danger: "fa ffa fa-exclamation-circle"
		  };

		  var iconAdded = false;

		  var alertClasses = ["alert", "animated", "flipInX"];
		  alertClasses.push("alert-" + severity.toLowerCase());

		  if (dismissible) {
		    alertClasses.push("alert-dismissible");
		  }

		  var msgIcon = $("<i />", {
		    "class": iconMap[severity] // you need to quote "class" since it's a reserved keyword
		  });

		  var msg = $("<div />", {
		    "class": alertClasses.join(" ") // you need to quote "class" since it's a reserved keyword
		  });

		  if (title) {
		    var msgTitle = $("<h4 />", {
		      html: title
		    }).appendTo(msg);
		    
		    if(!iconAdded){
		      msgTitle.prepend(msgIcon);
		      iconAdded = true;
		    }
		  }

		  if (summary) {
		    var msgSummary = $("<strong />", {
		      html: summary
		    }).appendTo(msg);
		    
		    if(!iconAdded){
		      msgSummary.prepend(msgIcon);
		      iconAdded = true;
		    }
		  }

		  if (details) {
		    var msgDetails = $("<p />", {
		      html: details
		    }).appendTo(msg);
		    
		    if(!iconAdded){
		      msgDetails.prepend(msgIcon);
		      iconAdded = true;
		    }
		  }
		    
		    
		  if (dismissible) {		    		    
		    var msgClose = $("<span />", {
		      "class": "close", 		    // you need to quote "class" since it's a reserved keyword
		      "data-dismiss": "alert",
		      html: "<i class='fa fa-times-circle'></i>"
		    }).appendTo(msg);
		  }
		  
		  $('#' + appendToId).prepend(msg);
		  
		  if(autoDismiss){
		    setTimeout(function(){
		      msg.addClass("flipOutX");
		      setTimeout(function(){
		        msg.remove();
		      },1000);
		    }, 5000);
		  }
		}

	
	/*$rootScope.chechlicenseexpred=function()
	{
		userService.checklicenseexpired.then(function Success(response){
			$scope.check=response.data.serviceResponse;
			alert($scope.check);
			
		})
		
	}*/
	$rootScope.checkHome=function()
	{
		$scope.url = $location.url();
		if($scope.url=='/dashboard')
		{
			$('#random').addClass('hideHome');
		}
		else
		{
			$('#random').removeClass('hideHome');
		}
	}
	$rootScope.checkHome();
	$("#showIco1").click(function()
	{
		$(this).toggleClass('fa-eye-slash fa-eye');
		if($(this).hasClass("fa-eye")){
		$("#current-password").attr('type','text');
		}else{
		$("#current-password").attr('type','password');
		}
	});
	$("#showIco2").click(function()
	{
		$(this).toggleClass('fa-eye-slash fa-eye');
		if($(this).hasClass("fa-eye")){
					$("#new-password").attr('type','text');
		}else{
					$("#new-password").attr('type','password');
		}
	});
	$("#showIco3").click(function()
	{
		$(this).toggleClass('fa-eye-slash fa-eye');
		if($(this).hasClass("fa-eye")){
			$("#confirm-new-password").attr('type','text');
		}else{
			$("#confirm-new-password").attr('type','password');
			}
	});
	$scope.showUpdatePasswordModal=function()
	{
		$("#password-update-form input").each(function() {
		      this.value = "";
		 });
		$scope.passwordChangeStatus='';
		$('#change-password').modal('show');
	}
	var countCheck=false;
	$scope.checkDBpassword=function ()
	{
		
		var current=$('#current-password').val();
		if(countCheck==false)
		{
			userService.checkDBpassword(current).then(function Success(response){
				$(".se-pre-con").fadeOut('slow');
				if(response.data.serviceResponse=='False')
				{
					alert('Please provide with correct password');
				
					$('#current-password').focus();
					countCheck==false;
				}
				else if(response.data.serviceResponse=='True')
				{
					//$scope.passwordChangeStatus='';
					countCheck=true;
				}
			});
		}
	}
	
	$scope.updatePassword=function()
	{
		var current=$('#current-password').val();
		var newPass=$('#new-password').val();
		var newPasslength=$('#new-password').val().length;
		var renewPass=$('#confirm-new-password').val();
		var renewPasslength=$('#confirm-new-password').val().length;
		
		if(current)
		{
			if(newPass)
			{
				if(newPasslength >=5)
				{
				if(renewPass)
				{
					if(renewPasslength >=5)
					{
					if((current==newPass)&&(current==renewPass))
					{
						$scope.passwordChangeStatus="'Current Password' and 'New Password'/'Confirm New Password' are same. Kindly change your 'New Password' !!!"
					}
					else
					{
						if(newPass==renewPass)
						{
							userService.updatePassword(current, newPass).then(function Success(response){
								$scope.passwordChangeStatus=response.data.serviceResponse;
								
								if(response.data.serviceResponse =="Password changed successfully")
								{
									alert("Password changed successfully");
									$('#change-password').modal('hide');
								}
								else{
									alert("Password not changed ")
								}
								$(".se-pre-con").fadeOut('slow');
								/*current=newPass=renewPass=null;
								$('#current-password').val()=$('#new-password').val()=$('#confirm-new-password').val()="";*/
								$("#password-update-form input").each(function() {
								      this.value = "";
								 });
								$('#showCross').fadeOut('slow');
								
								//$scope.passwordChangeStatus='';
							});
						}
						else
						{
							$scope.passwordChangeStatus="'New Password' and 'Confirm New Password' are NOT same. Kindly match your new passwords !!!"
						}
					}
				}else{
					alert("Please provide confirm password length equal or more than 5 character")

				}
				}
				else
				{
					$scope.passwordChangeStatus="Kindly fill your 'Confirm New Password' !!!"
				}
					
			}
			else{
				alert("Please provide new password length equal or more than 5 character")
			}
			}
			else
			{
				$scope.passwordChangeStatus="Kindly fill your 'New Password' !!!"
			}
		}
		else
		{
			$scope.passwordChangeStatus="Kindly fill your 'Current Password' !!!"
		}
		$('#passstatus').delay(5000).fadeOut();

	}

	
	$scope.showAccountView=function()
	{
		//jQuery.noConflict();
		$('#accountview').modal('show');
		$scope.showAccountDetails();
		$scope.getMyEntitlement();
	}
	
	
	$scope.managerID=localStorage.getItem('managerID');
	$scope.roleId = localStorage.getItem('userRole');	
	$scope.userName = localStorage.getItem('userName');
	$scope.userId = localStorage.getItem('userid');
	$scope.CheckLicenseexpiredornotsttaus=localStorage.getItem('CheckLicenseexpiredornotStatus');
	$scope.CheckLicenseexpiredornotResponse=localStorage.getItem('CheckLicenseexpiredornotResponse');

	if($scope.CheckLicenseexpiredornotsttaus=="200")
		{
		
		//alert("License is expired "+$scope.CheckLicenseexpiredornotResponse+"days");
		$("#checklicense").modal();
		
		}
	
	$scope.showAccountDetails=function()
	{
		userService.getAccountDetails($scope.userName,$scope.roleId,$scope.managerID).then(function Success(response){
			$scope.accountDetailsView=response.data.serviceResponse;
			$(".se-pre-con").fadeOut('slow');
	});
	}
	
	
	$scope.getMyEntitlement=function()
	{
		userService.getMyEntitlementFnct($scope.userId,$scope.roleId).then(function Success(response){
			$scope.showMyEntitlement=response.data.serviceResponse;
			$(".se-pre-con").fadeOut('slow');
		});
	}
	
	var key = null;
	var value = null;
	var allCookieArray = document.cookie.split(';');
	for (var i = 0; i < allCookieArray.length; i++) {
		key = allCookieArray[i].split('=')[0];
		value = allCookieArray[i].split('=')[1];
		if (key == "access-token" & value == "") {
			window.location.href = "/TMT/logout";

		}
	}
   
	$scope.userName = localStorage.getItem('userName');
	$scope.user = localStorage.getItem('name');
	$scope.lastLogin = localStorage.getItem('lastLogin');
	$scope.userTPID = localStorage.getItem('userTPID');
	$scope.role=localStorage.getItem('role');
	$scope.isAdmin=false;
	$scope.userObject=null;
	if($scope.role=='admin'){
		$scope.isAdmin=true;
		
	}
	
	$scope.profile=null;
	$scope.userProfile=function(){
		
		
		userService.getADUser($scope.userTPID).then(function Success(response){
		    if(response.data.serviceStatus=="Success"){
		    	$scope.userObject =response.data.serviceResponse;	
		    	console.log($scope.userObject);
		    	//var name=$scope.userObject.fullName;
		    	var nameArray=$scope.userObject.fullName.split(" ");
		    	console.log(nameArray);
		    	$scope.profile=nameArray[0].charAt(0)+nameArray.pop().charAt(0);
		    	
		    	console.log($scope.profile);
		    	
		    }
	    });
		
		$("#userProfileModal").modal();
	}
	
	var valid = false;
	$scope.logout = function() {
		if (confirm("Are you sure to logout?") == true) {
			localStorage.clear();
			userService.sessionKills().then(function Success(response)
			{
				
			});

			document.cookie = 'access-token=; Path=/;'
			window.location.href = "/TMT/logout";
		} else {
			return false;
		}
		
	}
	$(window).on('mouseover', (function () {
	    window.onbeforeunload = null;
	}));
	$(window).on('mouseout', (function () {
	    window.onbeforeunload = ConfirmLeave;
	}));
	function ConfirmLeave() {
	    return "";
	}
	var prevKey="";
	$(document).keydown(function (e) {            
	    if (e.key=="F5") {
	        window.onbeforeunload = ConfirmLeave;
	    }
	    else if (e.key.toUpperCase() == "W" && prevKey == "CONTROL") {                
	        window.onbeforeunload = ConfirmLeave;   
	    }
	    else if (e.key.toUpperCase() == "R" && prevKey == "CONTROL") {
	        window.onbeforeunload = ConfirmLeave;
	    }
	    else if (e.key.toUpperCase() == "F4" && (prevKey == "ALT" || prevKey == "CONTROL")) {
	        window.onbeforeunload = ConfirmLeave;
	    }
	    prevKey = e.key.toUpperCase();
	});
/*	window.onbeforeunload = function(e) {    
            console.log(e);
            
             if( !valid ){
            	 logout();
             }
            } 
        
        $("button").on("click", function() {
               valid = true;            
             }); 
     
       $("a").bind("click", function() {
           valid = true;            
         }); 
       
         

	

	*/
	
	
	/*AutoLogout*/  
	var autoLogoutTime=1800;
	var TimeCounter=0;
	document.onClick=function(){
	 TimeCounter=0;
	}
	document.onmousemove=function(){
		 TimeCounter=0;
		}
		document.onkeypress=function(){
		 TimeCounter=0;
		}
	
	var autologout=function autoLogout(){
//	alert("dxfg1");
	TimeCounter++;
	//console.log(TimeCounter);
	if(TimeCounter >=autoLogoutTime){
		document.cookie = 'access-token=; Path=/;'
		window.location.href = "/TMT/logout";
	  }
	}
	var sendSessionAlive=function sendSessionAlive(){
//	alert("dxfg");

userService.updateSession().then(function Success(response)
			{
				
			});
	
//	alert("dxfg1");
		}
	$interval(autologout,1000)
	$interval(sendSessionAlive,10000)
	
    });