demoApp
		.controller(
				"testRunController",
				function($scope, $interval, $state, $stateParams,$location,$anchorScroll, $rootScope, 
						testRunService,dashboardService) {
					$(document).ready(function(){
						checkcookie();
						var token=localStorage.getItem('tokenID');
						if (token==null|| token=="")
						{
						window.location.href = "../login.html";
						}
						});
				//	alert("Hello estren")
					
					$rootScope.checkHome();
					/* $location.hash('top'); */
				    
					$('.datepicker').datepicker({
						autoclose : true,
						format : 'mm/dd/yyyy',
						 orientation:'bottom',
						todayHighlight : true,
					});
					 $scope.isValidTableVisible = false;
					 $scope.isNoteVisible = false;
					 $scope.isInValidTableVisible = false;
					 $scope.isLoading = true;
//					$scope.projectName = localStorage.getItem('projectName');
					$scope.projectId = localStorage.getItem('projectId');
					$scope.userId = localStorage.getItem('userid');
					$scope.roleId = localStorage.getItem('userRole');
					$scope.projectwiserole=localStorage.getItem('projectwiserole');
					$scope.projectBugToolID=localStorage.getItem('bugToolID');
					$scope.selectedListstart=[];
					$scope.status="";
					 if($scope.roleId!=undefined)
						 {
						 if($scope.roleId==="100")
							{
							$("#testRun").css("display","unset");
							$("#todo").css("display","unset");
							$("#testSuites").css("display","unset");
							$("#Automation").css("display","unset");
							}
						if($scope.projectwiserole=="Manager" || $scope.projectwiserole==="Tester")
						{
							$("#todo").css("display","unset");
							$("#testSuites").css("display","unset");
							$("#Automation").css("display","unset");
							if($scope.projectwiserole==="Manager")
							{
								$("#testRun").css("display","unset");
						
							}
							else if($scope.projectwiserole==="Tester")
							{
						
								if($scope.url=='/testRun')
								{
									$state.transitionTo('error');
								}
							
							}
							else if($scope.projectwiserole==="Readonly")
							{
						
							}

						}
						 }
						
					else
					{
						$state.transitionTo('error');
					}
					
					/*		if($scope.projectwiserole=="Manager" || $scope.projectwiserole==="Tester")
                    {
					
					if($scope.projectwiserole==="Manager")
					{
					$("#testRun").css("display","unset");
					$("#todo").css("display","unset");
					$("#testSuites").css("display","unset");
					$("#Automation").css("display","unset");
					}	
				else if($scope.projectwiserole==="Tester")
				{
					$("#todo").css("display","unset");
					$("#testSuites").css("display","unset");
					$("#Automation").css("display","unset");
				}
				else if($scope.projectwiserole==="Readonly")
					{

					}
                    }
                    else
                    {
                        $state.transitionTo('error');    
                    }*/
					$scope.dropdownselect=false;
					
					
					 $scope.selectedList1 = []; 
					$scope.selectAllCheckBox=function(selectallid,logsId){
						 var char="#selectall"+selectallid;
						 var char1=".selectAllCase"+selectallid;
						if(selectallid=='')
							{
							 /*if($('#'+logsId).is(':checked'))
								{
								 $('#'+logsId).prop('checked',false);	
								}
							 else{
								 $('#'+logsId).prop('checked',true);
							 }*/
							}
						else
							{
						  if($(char).is(':checked'))
							{
							  $scope.dropdownselect=true;
							  $scope.selectedList1 = []; 
							  console.log($scope.selectedList1);
							$(char1).each(function () {
								  $(this).prop('checked',true);									 
							});
							
							
						
							}
						else
							{							 
							$(char1).each(function () {
								  $(this).prop('checked',false);	
							});
						}
							}
						}

					$scope.testCaseList=null;
					$scope.statusMsg=null;
					$scope.activeRunList=null;
	$scope.inactiveRunList = null;
	$scope.doneUpload = null;
	$scope.failUpload = null;
					$scope.completeRunList=null;
					$scope.IsAllChecked=[];	
					$scope.mulAssiginName=null;
					$scope.assgId="";
					$("#iconList .nav-link").each(function(){
						$(this).removeClass("active");
					});
					$("#testRun a.nav-link").addClass("active");
					$scope.createRunObj = {
						id : null,
						reRunId:null,
						projectId : null,
						testSuitesId : null,
						runName : null,
						startDate : null,
						targetEndDate : null,
						actualEndDate : null,
						status : null,
						isActive : 'Y',
						createdBy : null,
						updatedBy : null,
						createdDate : null,
						updatedDate : null,
						description : null,
						assignTo : null,
						testCaseList:null,
						round:null
					}
					$scope.uploadTestExecutionObj = {
							id: null,
							projectId: null,
							suiteName:null,
                             testSuitesId: null,
							runName: null,
							startDate: null,
							targetEndDate: null,
							actualEndDate: null,
							status: null,
							isActive: 'Y',
							createdBy: null,
							updatedBy: null,
							createdDate: null,
							updatedDate: null,
							description: null,
							assignTo: null,
							testCaseList: null,
							round: null,
							attachment: null
						}

					var createReRunObj = {
							id : null,
							reRunId:null,
							projectId : null,
							testSuitesId : null,
							runName : null,
							startDate : null,
							targetEndDate : null,
							actualEndDate : null,
							status : null,
							isActive : 'Y',
							createdBy : null,
							updatedBy : null,
							createdDate : null,
							updatedDate : null,
							description : null,
							assignTo : null,
							testCaseList:null,
							round:null
						}
					$scope.getOverallRunStatus = function(runId) {
						testRunService
								.getOverallRunStatus(runId)
								.then(
										function Success(response) {
											// $(".se-pre-con").hide();
											$scope.runStatusList = response.data.serviceResponse;
											 $scope.getChartDataForTCStatus($scope.runStatusList);
											
										});
					}
					$scope.getOverallActionStatus = function(runId) {
						testRunService
								.getOverallActionStatus(runId)
								.then(
										function Success(response) {
											// $(".se-pre-con").hide();
											$scope.actionStatusList = response.data.serviceResponse;							
											 $scope.getChartDataForTCAction($scope.actionStatusList);
										});
					}
					
					$scope.getOverallDefectStatus = function(runId) {
						testRunService.getOverallDefectStatus(runId)
								.then(
										function Success(response) {
											
											// $(".se-pre-con").hide();
											$scope.defectStatusList = response.data.serviceResponse;							
											 $scope.getChartDataForTCDefect($scope.defectStatusList);
										});
					}
					var myChart;		
					$scope.getChartDataForTCStatus = function(runStatusList){
								var data=[];								
								var ctx=document.getElementById(
								"chartContainer");
								var data = {
									    labels: ["Pass", "Fail"],
									    datasets: [{
									      backgroundColor: [
									        "#2ecc71",
									        "red",
									        "#95a5a6",
									        "#9b59b6",
									        "#f1c40f",
									        "#e74c3c",
									        "#34495e"
									      ],
									      data: [runStatusList.passCount,runStatusList.failCount]
									    }]
									  };
								var options = {
										 responsive: true,
									        legend: {
									        	  labels: {
									                    fontColor: 'rgb(255, 99, 132)',									            
											            position: 'right',
											            fontColor:'black',
											            boxWidth:40
									        	  }
									          
									        },
									// cutoutPercentage : 80,
								
								}					
								 if (myChart) {
									 myChart.destroy();
								      }								
								myChart = new Chart(ctx, {
									type : 'pie',
									data : data,
									
								});
								/** Pie Chart JS END */
			
			
							}
					var myChart1;		
					$scope.getChartDataForTCAction = function(runStatusList){
								var data=[];								
								var ctx=document.getElementById(
								"actionchartContainer");
								var data = {
									    labels: ["Completed", "Not-Started", "On Hold","Under Review","N/A","Total Action Incomplete"],
									    datasets: [{
									      backgroundColor: [
									        "#2ecc71",
									        "#95a5a6",
									        "#9b59b6",
									        "#f1c40f",
									        "#e74c3c",
									        "#34495e",
									        "#aa483b",
									      ],
									      data: [runStatusList.completed,runStatusList.unTested,runStatusList.onHold,runStatusList.underReview,runStatusList.na,runStatusList.totalactionincomplete]
									    }]
									  };
								var options = {
										 responsive: true,
									        legend: {
									            position: "right"
									          
									        },
									// cutoutPercentage : 80,
									 title: {
								            display: true,
								            text: 'TestCase Action Status'
								        }
								
								}					
								 if (myChart1) {
									 myChart1.destroy();
								      }								
								myChart1 = new Chart(ctx, {
									type : 'pie',
									data : data,
									
								});
								/** Pie Chart JS END */
			
			
							}
					var myChart3;		
					$scope.getChartDataForTCDefect = function(defectStatusList){
								var data=[];								
								var ctx=document.getElementById(
								"defectchartContainer");
								var data = {
									    labels: ["Critical", "Major", "Minor","Blocker"],
									    datasets: [{
									      backgroundColor: [
									    	 "red",
									        "#9b59b6",
									        "#f1c40f",
									        "#34495e"
									      ],
									      data: [defectStatusList.critical,defectStatusList.major,defectStatusList.minor,defectStatusList.blocker]
									    }]
									  };
								var options = {
										 responsive: true,
									        legend: {
									            position: "right"
									          
									        },
									// cutoutPercentage : 80,
									 title: {
								            display: true,
								            text: 'Defect Status'
								        }
								
								}					
								 if (myChart3) {
									 myChart3.destroy();
								      }								
								myChart3 = new Chart(ctx, {
									type : 'pie',
									data : data,
									
								});
								/** Pie Chart JS END */
			
			
							}
					$scope.getstatusChart = function(index,passVal,completedval){
						
						 if(completedval==null || completedval=="")
							 {
							 console.log(index);
							 $("#bar"+index).css({
							        transform: "rotate("+ (45) +"deg)", 
							        border: "5px solid #6c757d",
							 });
							 }
						 else{
						var perc = parseInt( (passVal/completedval)*100, 10);
						 $({p:0}).animate({p:perc}, {
							    duration: 6000,
							    easing: "swing",
							    step: function(p) {
							      $("#bar"+index).css({
							        transform: "rotate("+ (45+(p*1.8)) +"deg)", // 100%=180°
																				// so:
																				// ° =
																				// % *
																				// 1.8
							        // 45 is to add the needed rotation to have
									// the green borders at the bottom
							      });
							    /* $val.text(p|0); */
							    }
							  });
						 }/*
							 * var data = { labels: [ "Pass", "Fail", ],
							 * datasets: [ { data: [60, 40], backgroundColor: [
							 * "green", "red" ] }] }; //console.log(index); var
							 * ctx = $("#statusChart_"+index);
							 * 
							 * console.log(ctx); // And for a doughnut chart var
							 * myDoughnutChart1 = new Chart(ctx, { type:
							 * 'doughnut', data: data, options: { rotation: 1 *
							 * Math.PI, circumference: 1 * Math.PI } });
							 */}
					$scope.activeShowTestSutits=false;
					$scope.DetailsData=[];
					$scope.testSuitesDetails=[];
					$scope.getTestSuitesNameByPId = function() {
						$scope.includeall=false;
						$scope.testCaseList=null;
						var incr=0;
						testRunService
								.getTestSuitesNameByPId($scope.projectId)
								.then(
										function Success(response) {
											$scope.DetailsData=response.data.serviceResponse;
											
											for(i=0;i<$scope.DetailsData.length;i++)
												{
												
												//alert($scope.DetailsData[i].isActive)
													if(($scope.DetailsData[i].isActive === 'Y')&&(!($scope.DetailsData[i].isDeleted === 'Y')))
														{
														//$scope.activeShowTestSutits=false;
														
														$scope.testSuitesDetails[incr] = $scope.DetailsData[i];
														incr++;
														}
													
													console.log("incr"+incr)
												}	
											
										});
					}
					$scope.getAssignToDetails = function() {
						testRunService
								.getAssignToDetailsByPid($scope.projectId)
								.then(
										function Success(response) {
											$scope.assignToList = response.data.serviceResponse;
										});
					}					
					
					$scope.changeRunName = function() {
						$scope.includeall=false;
						$scope.statusMsg=null;
						$scope.testCaseList=[];
						$scope.selectedTCListRun=[];
						var val = Math.floor(1000 + Math.random() * 9000);
						$scope.createRunObj.runName = $scope.testsuitesSelected.testSuitesName+"_"+val;
						$scope.errormsg = "";
						/*$scope.createRunObj.runName = $scope.testsuitesSelected.testSuitesName;*/
					
					       $('.selecttestcaseinrun').prop('checked', false);

					}
					
					$scope.selectTestCases=false;
					$scope.selectAll = function() {	
						$scope.statusMsg=null;
						$scope.error="";		
						$scope.includeall = true;
						$scope.selectTestCases=false;
						var testSuites = $('select[name=testsuite]').val();
						
						if (testSuites == null || testSuites == "") {

							$scope.errormsg = "Kindly select testsuite name for testcase selection";
							return false;
						}
						
						if($scope.includeall==false)
							{
						$scope.includeall = true;
						testRunService
								.getTestCasesBysuites(
										$scope.testsuitesSelected.testSuitesId)
								.then(
										function Success(response) {
											$(".se-pre-con").hide();
											$scope.testCaseList = response.data.serviceResponse;																						
										});
							}
						else
							{
							/*$scope.includeall = false;
							$scope.testCaseList = null;	*/

							//$scope.includeall = true;
							testRunService
									.getTestCasesBysuites(
											$scope.testsuitesSelected.testSuitesId)
									.then(
											function Success(response) {
												$(".se-pre-con").hide();
												$scope.testCaseList = response.data.serviceResponse;																						
											});
							$scope.includeall = false;
							$scope.testCaseList = null;	
								
							}
					}
					/*$scope.filterTestCaseStatus=function(allTC){
						let resultTC = {};
						if(($scope.statusFilterAllTC=="")||($scope.statusFilterAllTC==undefined)){
							resultTC=allTC;
						}
						else{
							angular.forEach(items, function(value, key) {
					        	let testcaseArray=[];
					        	resultTC[key] = value;
					        	
					        	if($scope.statusFilterAllTC=="Fail"){
					        		for(let r in value.testCaseList){
						        		if(r.status=="Fail"){
						        			testcaseArray.push(r);
						        		}
						        	}
					        		resultTC[key].testCaseList = testcaseArray;
					        	}
					        	else if($scope.statusFilterAllTC=="Resolved"){
					        		for(r in value.testCaseList){
						        		if(r.bugStatus=="Resolved"){
						        			testcaseArray.push(r);
						        		}
						        	}
					        		resultTC[key].testCaseList = testcaseArray;
					        	}
					        	
					            if () {
					            	value.hasOwnProperty(key)
					                result[key] = value;
					            }
					        });
						}
				        
				        return resultTC;
					}*/
					$scope.filterTestCaseStatus=function(){
						$scope.statusMsg=null;
						// $scope.includeAll=null;
						$scope.testCaseList=null;
						$scope.includeall = false;
						$scope.selectTestCases=true;
						$scope.selectedTCListRun=[];
						$scope.testCaseList=[];
						//$(".selectAllTR").attr("checked",false);
						$(".selectAllTR").prop('checked', false);
						var testSuites = $('select[name=testsuite]').val();
						if (testSuites == null || testSuites == "") {

							$scope.errormsg = "Kindly select testsuite name for testcase selection";
							return false;
						}
						if($scope.statusFilterAllTC){
							testRunService.getGroupTestCasesBysuitesFiltered($scope.testsuitesSelected.testSuitesId,$scope.statusFilterAllTC)
								.then(function Success(response) {
									$(".se-pre-con").hide();
									$scope.testCaseList1 = response.data.serviceResponse;
							});
						}
						else{
							testRunService.getGroupTestCasesBysuites($scope.testsuitesSelected.testSuitesId).then(
								function Success(response) {
									$(".se-pre-con").hide();
									$scope.testCaseList1 = response.data.serviceResponse;
							});
						}
						
					}
					$scope.getAllTestCases = function() {
						$scope.statusMsg=null;
						// $scope.includeAll=null;
						$scope.testCaseList=null;
						$scope.includeall = false;
						$scope.selectTestCases=true;
						$scope.selectedTCListRun=[];
						$scope.testCaseList=[];
						//$(".selectAllTR").attr("checked",false);
						$(".selectAllTR").prop('checked', false);
						var testSuites = $('select[name=testsuite]').val();
						if (testSuites == null || testSuites == "") {

							$scope.errormsg = "Kindly select testsuite name for testcase selection..";
							return false;
						}
						testRunService
								.getGroupTestCasesBysuites(
										$scope.testsuitesSelected.testSuitesId)
								.then(
										function Success(response) {
											$(".se-pre-con").hide();
											$scope.testCaseList1 = response.data.serviceResponse;
											if($("#selectOneByOneTestCase").prop('checked') ===true )
											/*if($("#selectOneByOneTestCase").prop('checked', true))*/
												{
													$("#selectTestCaseModal").modal();
												}
											
										});
					}
					 $scope.CheckUncheckHeader = function (index) {
						// console.log(index)
			                $scope.IsAllChecked[index] = true;
			                for (const [key, value] of Object.entries($scope.testCaseList)) {
			                	 for (var i = 0; i < value.length; i++) {
			                    if (!$scope.value[i].Selected) {
			                        $scope.IsAllChecked[index] = false;
			                        break;
			                    }
			                	 }
			                };
			                $scope.testCaseList;
			            };
			           
			 
			            $scope.toggleTable =  function(index) {
//			        		alert(index);
			        		$("#tableDatas"+index).slideToggle("slow");
			        	}
			            $scope.CheckUncheckAll = function (index) {
			            	console.log($scope.IsAllChecked[index]);
			            	console.log($scope.testCaseList)
			            	for (const [key, value] of Object.entries($scope.testCaseList)) {
			            		 for (var i = 0; i < $scope.value.length; i++) {
			            			 $scope.value.testCaseList[i].Selected = $scope.IsAllChecked[index];
					                }
			            		}
			            	$scope.testCaseList;
			            };
			            
			            $scope.closeModel=function()
						{
							$('.selecttestcaseinrun').prop('checked', false);
						    $('.selecttestcaseinrun').prop('checked', false);
						}
			            var caseExecutionLogs=[];
			            var diffCaseExecutionLogs=[];

			            $scope.showPreview = function () {

			            	
			            	$scope.statusMsg = null;
			            	$scope.doneUpload = null;
			            	$scope.failUpload = null;
			            	if ($scope.testCaseList == null) {
			            	//$scope.error = "Kindly select TestCases for a Run";
			            		$scope.error = "Kindly select Test Suite Name";
			            	return false;
			            	}
			            	$scope.uploadTestExecutionObj.testSuitesId = $scope.testsuitesSelected.testSuitesId;
			            	$scope.uploadTestExecutionObj.createdBy = $scope.userId;
			            	$scope.uploadTestExecutionObj.testCaseList = $scope.testCaseList;
			            	$scope.uploadTestExecutionObj.projectId = $scope.projectId;
			            	$scope.uploadTestExecutionObj.round = 1;
			            	$scope.uploadTestExecutionObj.suiteName =  $scope.testsuitesSelected.testSuitesName;
			            	//$scope.uploadTestExecutionObj.description=createRunObj.description
			            	
			            	
			            	$scope.uploadTestExecutionObj.runName = $scope.createRunObj.runName;
			            	if ($scope.uploadTestExecutionObj.runName == null) {
			            	$scope.error = "Kindly select Test Suite Name";
			            	return false;
			            	}
			            	
			            	

			            	$scope.uploadTestExecutionObj.targetEndDate = $scope.createReRunObj.targetEndDate;
			            	if ($scope.uploadTestExecutionObj.targetEndDate == null) {
			            	$scope.error = "Kindly select Tentative End Date";
			            	return false;
			            	}
			            	$scope.uploadTestExecutionObj.isActive = $scope.createRunObj.isActive;
			            	if ($scope.uploadTestExecutionObj.isActive == null) {
			            	$scope.error = "Kindly select correct isActive status";
			            	return false;
			            	}
			            	$scope.uploadTestExecutionObj.description = $scope.createRunObj.description;
			            	if($scope.uploadTestExecutionObj.description ==null)
		            		{
		            		$scope.error = "Kindly enter description";
			            	return false;
		            		}
			            	var data = new FormData();
			            	var attachment = document.getElementById("file").files[0];
			            	$scope.uploadTestExecutionObj.attachment = attachment;
			            	if ($scope.uploadTestExecutionObj.attachment == null) {
			            	$scope.error = "Kindly attach RunTest Execution File";
			            	return false;
			            	}
			            	
			            	
			            	
			            	
			            	console.log("attachment " + attachment);
			            	var fileType = ['xlsx', 'xls'];
			            	var extension = $('#file').val().split(".").pop().toLowerCase();
			            	var temp = $('#file').val();
			            	var count = (temp.match(/[.]/g) || []).length;
			            	console.log("count " + count);
			            	if ($scope.projectId != "" && attachment != "") {
			            	if (fileType.indexOf(extension) == -1) {
			            	alert('Please Select A valid File Type');
			            	return false;
			            	}
			            	if (count > 1) {
			            	alert('Please check the extension of Selected File');
			            	return false;
			            	}
			            	console.log("appending  " + data);
			            	data.append("file", attachment);
			            	data.append("suiteName",$scope.uploadTestExecutionObj.suiteName);
			            	data.append("runName", $scope.uploadTestExecutionObj.runName);
			            	data.append("projectId", $scope.projectId);
			            	data.append("sameTestSuite","true");



			            	if (attachment.size <= 10485760) {

			            	testRunService.uploadPreview(data).then(
			            	function Success(response) {
			            	$(".se-pre-con").hide();
			            	// $scope.caseExecutionLogs=response.data.serviceResponse;

			            	$scope.status=response.data.serviceResponse;
			            	if(response.data.serviceStatus==200){
			            	$scope.isLoading = false;
			            	$scope.isValidTableVisible = true;
			            	$scope.caseExecutionLogs= response.data.serviceResponse;
			            	
			            	for(var i=0;i<$scope.caseExecutionLogs.length;i++){
			            		var d=null;
			            		d=$scope.caseExecutionLogs[i].dateOfExec;

			            		if(d!=null){
			            		var date = new Date(d);
			            		var year = date.getFullYear();
			            		var month = ("0" + (date.getMonth() + 1)).slice(-2);
			            		var day = ("0" + date.getDate()).slice(-2);
			            		   

			            		$scope.caseExecutionLogs[i].dateOfExec=month+"-"+day+"-"+year;

			            		}
			            		else{
			            		$scope.caseExecutionLogs[i].dateOfExec="";
			            		}

			            		}
			            	
			            	}
			            	if(response.data.serviceStatus==400){
			            	$scope.failUpload=response.data.serviceResponse;
			            	$scope.error=response.data.serviceResponse;
			            	  alert($scope.failUpload);
			            	//$("#showStatus").modal();
			            	  $("#showPreview").modal('hide');
			            	 // $("#uploadTestExecution").modal('hide');
			            	  $("#demo-form").trigger("reset");
			            	  document.getElementById("file").value = "";
			            	}


			            	$('.status').delay(5000).fadeOut();



			            	});

			            	$scope.showDiffPreview();

			            	} else {
			            	alert('Kindly check file size');
			            	}

			            	} else {
			            	$scope.documentstatus = "Kindly provide All Required Details"
			            	}

			            	$("#showPreview").modal();
			            	}
			            	$scope.showDiffPreview = function () {

			            	$scope.uploadTestExecutionObj.testSuitesId = $scope.testsuitesSelected.testSuitesId;
			            	$scope.uploadTestExecutionObj.createdBy = $scope.userId;
			            	$scope.uploadTestExecutionObj.testCaseList = $scope.testCaseList;
			            	$scope.uploadTestExecutionObj.projectId = $scope.projectId;
			            	$scope.uploadTestExecutionObj.round = 1;
			            	$scope.uploadTestExecutionObj.suiteName =  $scope.testsuitesSelected.testSuitesName;
			            	$scope.uploadTestExecutionObj.runName = $scope.createRunObj.runName;
			            	$scope.uploadTestExecutionObj.targetEndDate = $scope.createReRunObj.targetEndDate;
			            	$scope.uploadTestExecutionObj.isActive = $scope.createRunObj.isActive;
			            	$scope.uploadTestExecutionObj.description = $scope.createRunObj.description;

			            	var data = new FormData();
			            	var attachment = document.getElementById("file").files[0];
			            	$scope.uploadTestExecutionObj.attachment = attachment;
			            	console.log("attachment " + attachment);
			            	var fileType = ['xlsx', 'xls'];
			            	var extension = $('#file').val().split(".").pop().toLowerCase();
			            	var temp = $('#file').val();
			            	var count = (temp.match(/[.]/g) || []).length;
			            	console.log("count " + count);
			            	if ($scope.projectId != "" && attachment != "") {
			            	if (fileType.indexOf(extension) == -1) {
			            	alert('Please Select A valid File Type');
			            	return false;
			            	}
			            	if (count > 1) {
			            	alert('Please check the extension of Selected File');
			            	return false;
			            	}
			            	console.log("appending  " + data);
			            	data.append("file", attachment);
			            	data.append("suiteName",$scope.uploadTestExecutionObj.suiteName);
			            	data.append("runName", $scope.uploadTestExecutionObj.runName);
			            	data.append("projectId", $scope.projectId);
			            	data.append("sameTestSuite","false");



			            	if (attachment.size <= 10485760) {



			            	testRunService.uploadDiffPreview(data).then(
			            	function Success(response) {
			            	$(".se-pre-con").hide();
			            	// $scope.caseExecutionLogs=response.data.serviceResponse;


			            	$scope.status=response.data.serviceResponse;
			            	if(response.data.serviceStatus==200){
			            	$scope.isLoading = false;
			            	$scope.isNoteVisible = true;
			            	$scope.isInValidTableVisible = true;

			            	$scope.diffCaseExecutionLogs= response.data.serviceResponse;
			            	for(var i=0;i<$scope.diffCaseExecutionLogs.length;i++){
			            		var d=null;
			            		d=$scope.diffCaseExecutionLogs[i].dateOfExec;

			            		if(d!=null){
			            		var date = new Date(d);
			            		var year = date.getFullYear();
			            		var month = ("0" + (date.getMonth() + 1)).slice(-2);
			            		var day = ("0" + date.getDate()).slice(-2);
			            		   

			            		$scope.diffCaseExecutionLogs[i].dateOfExec=day+"-"+month+"-"+year;

			            		}
			            		else{
			            		$scope.diffCaseExecutionLogs[i].dateOfExec="00-00-0000";
			            		}
			            		
			            		var d1=null;
			            		/*d1=$scope.diffCaseExecutionLogs[i].executionTime;
			            		if(d1 !=null)
			            			{
			            	
			            		$scope.secnd=$scope.diffCaseExecutionLogs[i].executionTime;
			            		 var seconds = ms / 1000;
			            		    // 2- Extract hours:
			            		    var hours = parseInt( seconds / 3600 ); // 3,600 seconds in 1 hour
			            		    seconds = seconds % 3600; // seconds remaining after extracting hours
			            		    // 3- Extract minutes:
			            		    var minutes = parseInt( seconds / 60 ); // 60 seconds in 1 minute
			            		    // 4- Keep only seconds not extracted to minutes:
			            		    seconds = seconds % 60;
			    				$scope.diffCaseExecutionLogs[i].executionTime   = hours+':'+minutes+':'+seconds;
			    				alert(hours+':'+minutes+':'+seconds);
			            			
			            			}
			            		else{
			            			$scope.diffCaseExecutionLogs[i].executionTime="";
			            		}
*/
			            		}
			            	
			            	}
			            	if(response.data.serviceStatus==400){
			            	$scope.failUpload=response.data.serviceResponse;

			            	  alert($scope.failUpload);
			            	//$("#showStatus").modal();
			            	//   $("#showPreview").modal('hide');
			            	 // $("#uploadTestExecution").modal('hide');
			            	  //$("#demo-form").trigger("reset");

			            	}


			            	$('.status').delay(5000).fadeOut();



			            	});


			            	} else {
			            	alert('Kindly check file size');
			            	}

			            	} else {
			            	$scope.documentstatus = "Kindly provide All Required Details"
			            	}

			            	$("#showPreview").modal();
			            	}

			      /*      	$scope.uploadTestExecution = function () {

			            	$("#showPreview").modal('hide');
			            	$scope.statusMsg = null;
			            	$scope.error=null;
			            	$scope.doneUpload = null;
			            	$scope.failUpload = null;
			            	$scope.uploadTestExecutionObj.testSuitesId = $scope.testsuitesSelected.testSuitesId;
			            	$scope.uploadTestExecutionObj.createdBy = $scope.userId;
			            	$scope.uploadTestExecutionObj.testCaseList = $scope.testCaseList;
			            	$scope.uploadTestExecutionObj.projectId = $scope.projectId;
			            	$scope.uploadTestExecutionObj.round = 1;
			            	$scope.uploadTestExecutionObj.suiteName =  $scope.testsuitesSelected.testSuitesName;
			            	$scope.uploadTestExecutionObj.runName = $scope.createRunObj.runName;
			            	$scope.uploadTestExecutionObj.targetEndDate = $scope.createReRunObj.targetEndDate;
			            	$scope.uploadTestExecutionObj.isActive = $scope.createRunObj.isActive;
			            	$scope.uploadTestExecutionObj.description = $scope.createRunObj.description;

			            	var data = new FormData();
			            	var attachment = document.getElementById("file").files[0];
			            	$scope.uploadTestExecutionObj.attachment = attachment;
			            	if ($scope.uploadTestExecutionObj.attachment == null) {
			            	$scope.error = "Kindly attach RunTest Execution File";
			            	return false;
			            	}
			            	console.log("attachment " + attachment);
			            	var fileType = ['xlsx', 'xls'];
			            	var extension = $('#file').val().split(".").pop().toLowerCase();
			            	var temp = $('#file').val();
			            	var count = (temp.match(/[.]/g) || []).length;
			            	console.log("count " + count);
			            	if ($scope.projectId != "" && attachment != "") {
			            	if (fileType.indexOf(extension) == -1) {
			            	alert('Please Select A valid File Type');
			            	return false;
			            	}
			            	if (count > 1) {
			            	alert('Please check the extension of Selected File');
			            	return false;
			            	}

			            	console.log($scope.caseExecutionLogs);
			            	data.append("file", attachment);
			            	data.append("suiteName",$scope.uploadTestExecutionObj.suiteName);
			            	data.append("runName", $scope.uploadTestExecutionObj.runName);
			            	data.append("projectId", $scope.projectId);
			            	data.append("createdBy", $scope.userId);
			            	data.append("testSuitesId",$scope.uploadTestExecutionObj.testSuitesId);
			            	data.append("testCaseList",$scope.uploadTestExecutionObj.testCaseList);
			            	data.append("round", $scope.uploadTestExecutionObj.round );
			            	data.append("targetEndDate",$scope.uploadTestExecutionObj.targetEndDate);
			            	data.append("isActive  ",$scope.uploadTestExecutionObj.isActive );
			            	data.append("description  ",$scope.uploadTestExecutionObj.description );
			            	data.append("sameTestSuite","true");
			            	console.log("in test run controller, data is  " + data);

			            	if (attachment.size <= 10485760) {
			            	testRunService.uploadTestExecution(data).then(
			            	function Success(response) {
			            	$(".se-pre-con").hide();
			            	$scope.status=response.data.serviceResponse;
			            	if(response.data.serviceStatus==200){
			            	$scope.doneUpload = response.data.serviceResponse;
			            	alert($scope.doneUpload);
			            	}
			            	if(response.data.serviceStatus==400){
			            	$scope.failUpload=response.data.serviceResponse;
			            	alert($scope.failUpload);
			            	}
			            	$("#uploadTestExecution").modal('hide');
			            	//$("#showStatus").modal();
			            	$scope.getActiveRun();
			            	$scope.getInActiveRun();
			            	$scope.getCompletedRun();
			            	$("#demo-form").trigger("reset");
			            	$('.status').delay(5000).fadeOut();
			            	document.getElementById("file").value = "";
			            	});
			            	} else {
			            	alert('Kindly check file size');
			            	}

			            	} else {
			            	$scope.documentstatus = "Kindly provide All Required Details"
			            	}

			            	};*/
				
			            	$scope.uploadTestExecution = function () {
			            		$("#showPreview").modal('hide');
				            	$scope.statusMsg = null;
				            	$scope.error=null;
				            	$scope.doneUpload = null;
				            	$scope.failUpload = null;
				            	$scope.round = 1;
				            	console.log("new"+$scope.caseExecutionLogs);
				            	
				            	
				            	for(var i=0;i<$scope.caseExecutionLogs.length;i++){
				            		var d=null;
				            		d=$scope.caseExecutionLogs[i].dateOfExec;
				            		
				            		
				            		if(d==null || d=="")
				            			{
					            		$scope.caseExecutionLogs[i].dateOfExec="0000-00-00";

				            			}
				            		else{
				            		if(d!=null){
				            		var date = new Date(d);
				            		var year = date.getFullYear();
				            		var month = ("0" + (date.getMonth() + 1)).slice(-2);
				            		var day = ("0" + date.getDate()).slice(-2);
				            		   
				            		$scope.caseExecutionLogs[i].dateOfExec=year+"-"+month+"-"+day;
				            		//alert($scope.caseExecutionLogs[i].dateOfExec);
				            		}
				            		else{
				            		$scope.caseExecutionLogs[i].dateOfExec="0000-00-00";
				            		}
				            		}

				            	}
				            	
				            	testRunService.uploadTestExecutions($scope.caseExecutionLogs,$scope.testsuitesSelected.testSuitesId,$scope.userId,
				            			$scope.projectId,$scope.round,$scope.testsuitesSelected.testSuitesName,$scope.createRunObj.runName,
				            			$scope.createReRunObj.targetEndDate,$scope.createRunObj.description,$scope.createRunObj.isActive).then(
						            	function Success(response) {
						            	$(".se-pre-con").hide();
						            	$scope.status=response.data.serviceResponse;
						            	if(response.data.serviceStatus==200){
						            	$scope.doneUpload = response.data.serviceResponse;
						            	alert($scope.doneUpload);
						            	}
						            	if(response.data.serviceStatus==400){
						            	$scope.failUpload=response.data.serviceResponse;
						            	alert($scope.failUpload);
						            	}
						            	$("#uploadTestExecution").modal('hide');
						            	$scope.getActiveRun();
						            	$scope.getInActiveRun();
						            	$scope.getCompletedRun();
						            	$("#demo-form").trigger("reset");
						            	$('.status').delay(5000).fadeOut();
						            	document.getElementById("file").value = "";
						            	});
			            	}
			            	
			            	
			            	$scope.confirmCreateRun=function()
			            	{
			            		$scope.statusMsg="";
			            		$scope.error="";
			            		$("#confirmation-dialog-create-run").modal();
			            	}

			            $scope.createRun=function()
			            {
			            	$scope.statusMsg=null;
			            	if($scope.includeall==false)
			            		{
			            			if($scope.testCaseList==null){
				            		 $scope.error="Kindly select TestCases for Creating a Run";
				            		 return false;
			            			}
			            		}
			            	if($scope.testCaseList==null)
		            		{			            		
			            		 $scope.error="No TestCase Available for Staring a Run";
			            		 return false;
		            		}
			            	if($scope.createRunObj.targetEndDate==false)
			            		{
				            		$scope.error="No end date selected";
				            		return false;
			            		}
			            	if($scope.createRunObj.runName==null||$scope.createRunObj.runName=="")
		            		{
			            		$scope.error="please fill run name";
			            		return false;
		            		}
				            	$scope.createRunObj.testSuitesId=$scope.testsuitesSelected.testSuitesId;
				            	$scope.createRunObj.createdBy=$scope.userId;
				            	$scope.createRunObj.testCaseList=$scope.testCaseList;
				            	$scope.createRunObj.projectId=$scope.projectId;
				            	$scope.createRunObj.round=1;			            	
			            	testRunService
							.createRun(
									$scope.createRunObj)
							.then(
									function Success(response) {
										$(".se-pre-con").hide();
										if(response.data.serviceStatus=="200")
											{
											$scope.statusMsg = response.data.serviceResponse;
											}
										else{
											$scope.error = response.data.serviceResponse;
										}
										
										$scope.testsuitesSelected=null;
						            	$scope.createRunObj.runName=null;
						            	$scope.createRunObj.assignTo=null;
						            	$scope.createRunObj.description=null;
						            	$scope.createRunObj.targetEndDate=null;
										$scope.testCaseList=[];
										$scope.includeall = false;
										 $('.selecttestcaseinrun').prop('checked',false)
										$scope.selectedTCListRun=[];
							        		$('#statusMsg').delay(5000).fadeOut();


										 
									});
			            	
			            	
			            };
			            $scope.getActiveRun = function() {							
							/*testRunService
									.getActiveRun(
											$scope.projectId)
									.then(
											function Success(response) {
												$(".se-pre-con").hide();
												$scope.activeRunList = response.data.serviceResponse;	
												console.log($scope.activeRunList);
												
											});*/
			            	$scope.activeRunList=[];
			            	testRunService
							.getActiveRun(
									$scope.projectId)
							.then(
									function Success(response) {
										$(".se-pre-con").hide();
										
										$scope.activeRunList = response.data.serviceResponse;	
										
										
										console.log($scope.activeRunList);
										
										
										
									});
						};
$scope.getInActiveRun = function() {							
								
				            	testRunService
								.getInActiveRun(
										$scope.projectId)
								.then(
										function Success(response) {
											$(".se-pre-con").hide();
											$scope.inactiveRunList = response.data.serviceResponse;	
											console.log($scope.inactiveRunList);	
										});
							};
						 $scope.getCompletedRun = function() {							
								testRunService
										.getCompletedRun(
												$scope.projectId)
										.then(
												function Success(response) {
													$(".se-pre-con").hide();
													$scope.completeRunList = response.data.serviceResponse;												
													console.log($scope.completeRunList)
												});
						 };
$scope.activeToDelete;
						 $scope.getActiveRun();
		 $scope.getInActiveRun();
						 $scope.getCompletedRun();
						 $scope.deleteRun = function(active) {	
							 console.log("active run to test round "+active)
							 $scope.activeToDelete =active;
							  $scope.showDialogforStopRun(true);
									    $scope.confirmationDialogConfig = {
									      title: "Alert!!!",
									      message: "Are you sure to delete "+active.runName+"_"+active.round+" As "+active.notStarted+" TestCases are Not Started?",
									      buttons: [{
									        label: "Yes",
									        action: "deleteRunData"
									      }]
									    };
									    
									    
							 
						 };
						 $scope.deleteRunData = function(){
							 console.log("active run in deleterun data "+ $scope.activeToDelete);
							
							 $scope.erroStatus=null;
								testRunService
										.deleteRun(
												$scope.activeToDelete.id)
										.then(
												function Success(response) {
													$(".se-pre-con").hide();
													$scope.deleteStatus = response.data.serviceResponse;
													if(response.data.serviceStatus==200){
													 $scope.getActiveRun();
													$scope.getInActiveRun();
													 $scope.getCompletedRun();
													}
													else{
															if( $scope.activeToDelete.isActive==="Y"){
														$scope.erroAStatus="Unable to delete Active Run";
														$("#errorAR").delay(5000).fadeout();
															}
															
													else{
														$scope.erroInStatus="Unable to delete InActive Run";
														$("#errorstatu").delay(5000).fadeout();

													}
													}
												});
						 	if( $scope.activeToDelete.round===1)
								{	testRunService
								.deleteRoundOneRun( $scope.activeToDelete.runId)  
								.then(
										function Success(response) {
											$(".se-pre-con").hide();
											$scope.deleteStatus = response.data.serviceResponse;
											if(response.data.serviceStatus==200){
											 $scope.getActiveRun();
											 $scope.getInActiveRun();
											 $scope.getCompletedRun();
											}
											else{
												if( $scope.activeToDelete.isActive==="Y"){
												$scope.erroStatus="Unable to delete Active Run";}
											else{
												$scope.erroStatus="Unable to delete InActive Run";
											}
											}
										});
								}
								$scope.showDialogforStopRun(false);
						 };
							$scope.confirmationDialogForStopRun = function() {
								 var active=$scope.active;
								 console.log(active);
								 console.log("start");
								 if(active.notStarted>0)
								 {
									 $scope.showDialogforStopRun(true);
									    $scope.confirmationDialogConfig = {
									      title: "Alert!!!",
									      message: "Are you sure you want to change "+active.runName+"_"+active.round+" ?",
									      buttons: [{
									        label: "Yes",
									        action: "stopActiveRun"
									      }]
									    };
								 }
								 else{
									 $scope.stopActiveRun();
								 }
								    
							 }
							$scope.confirmationDialogForDeleteRun = function(active) {
							
								 console.log(active);
								 console.log("start");
								 
									 $scope.showDialogforStopRun(true);
									    $scope.confirmationDialogConfig = {
									      title: "Alert!!!",
									      message: "Are you sure to delete "+active.runName+"_"+active.round+" As "+active.notStarted+" TestCases are Not Started?",
									      buttons: [{
									        label: "Yes",
									        action: "deleteRun"
									      }]
									    };
								 
								    
							 }
						 
						 $scope.stopActiveRun = function() {
							 console.log($scope.active)
							
							 $scope.updateStatus=null;
								testRunService
										.stopActiveRun($scope.active)
										.then(
												function Success(response) {
													$(".se-pre-con").hide();
													$scope.updateStatus = response.data.serviceResponse;
													if(response.data.serviceStatus==200){
														alert("Updated Test Run Successfully");
													$scope.showDialogforStopRun(false);
													$("#editRun").modal('hide'); 
													$scope.getActiveRun();
													$scope.getInActiveRun();
													$scope.getCompletedRun();
													}
													else{
														$scope.updateStatus="Unable to delete Active Run";
													}
												});
					 };
						 $scope.confirmationDialogForDeleteOfRun = function(active) {
							 console.log(active.id);
							 console.log(active);
							 var txt=confirm("Click ok if you want to delete");
								
								if(txt== null || txt == "")
									{
									
									}
								else
									{
									testRunService.deletetestcaseid(active).then(function Success(response)
									{   
										active.id=active.reRunId;
										console.log(active.id);
										$scope.getRunDetails(active,"Running");
									});
									
								}
						 };
						 
		//11_06_2020
		$scope.openCompleteRunModal = function(testgroupName, testcaseObject) {		
								
			$scope.testCaseCompleteRun=testcaseObject;
			
			$scope.testCaseCompleteRun.testGroupName=testgroupName;
			
/*			$scope.testCaseCompleteRun.executionTime*/
			
			
			
			testRunService.attchmentFetch($scope.testCaseCompleteRun.id).then(function Success(response) {
				$(".se-pre-con").hide();
				$scope.ssCompleteRun=response.data.serviceResponse;
			});
			$('#complete-run-testcase-details').modal('show');
													
		};
						 
						 
						 $scope.showRunDetails = false;	
						 $scope.testRunName="";						
						$scope.backToTestRun = function() {
							$scope.showRunName=false;
							$scope.showRunDetails = false;
						}
						 $scope.showRunDetailsrun = false;
						$scope.isCompleted=false;
						 $scope.getRunDetails = function(activeRun,isRunning) {
							 if(activeRun.isActive==='N')
								 {
									 alert(activeRun.runName+" is currently inactive");
									 return false;
								 }
							console.log(isRunning);
							$scope.isCompleted=false;
							 $scope.testRunName=activeRun.runName;
							 $scope.testRunId=activeRun.id;
							 $scope.showRunName=true;
							 $scope.showRunDetails = true;
							 $scope.showRunDetailsrun = true;
							 if(isRunning=='Running')
								 {
								 	$scope.isCompleted=true;
								 }
							 
								testRunService
										.getRunDetails(
												activeRun.id)
										.then(
												function Success(response) {
													$(".se-pre-con").hide();
													$scope.runTestCaseList = response.data.serviceResponse;												
													
												});
								 $scope.getOverallRunStatus($scope.testRunId);
								 $scope.getOverallActionStatus($scope.testRunId);
								 $scope.getOverallDefectStatus($scope.testRunId);
						 };
						/*
						 * $scope.showModalForEditRun = function() {
						 * $("#editRun").modal(); }
						 */
						 $scope.showModalForEditRun=function(activeRun)
						 {
							 $scope.active=activeRun;
							 $scope.active.status="Running"
							 if($scope.active.isActive=='N')
							{
							 testRunService.checkTSuites(activeRun.testSuitesId).then(
										function Success(response) {
											$(".se-pre-con").hide();
							 if(response.data.serviceStatus==200)
								 {
							 		$("#editRun").modal();
								 }
							 else{
									alert(response.data.serviceResponse);
									$("#editRun").modal('hide');
							 }
							});
							}
							 else{
									$("#editRun").modal();

							 }
						 }
						 $scope.showModalForReRun=function(runId, suiteId)
						 {
							 if($scope.projectBugToolID){
								 alert("Kindly select 'Synchronize with Bugtool' before starting re-run.\n Ignore if done.");
							 }
							 $scope.createReRunObj.assignTo=="";
							 $scope.createReRunObj.targetEndDate=="";
							 
							 testRunService.findLastRunbyId(runId, suiteId).then(
										function Success(response) {
											$(".se-pre-con").hide();
											if( response.data.serviceStatus==202)
											{
												alert(response.data.serviceResponse);
											}
											else
											{
												$scope.rerunsList=response.data.serviceResponse;
												var activeRun = $scope.rerunsList[0];												
												$scope.createReRunObj=createReRunObj;
												 $scope.createReRunObj.reRunId=activeRun.id;
												 $scope.createReRunObj.id=activeRun.runId;
												 $scope.createReRunObj.testSuitesId=activeRun.testSuitesId;
												 $scope.createReRunObj.runName=activeRun.runName;
												 $scope.createReRunObj.isActive='Y';
												 $scope.createReRunObj.assignTo='';
												 $scope.createReRunObj.round=activeRun.round+1;
												 $scope.includeall = false;
												 $scope.includeFail = false;
												 $scope.testCaseList = null;
												 $scope.errormsg="";
												 $scope.reRunStatus="";
												 $("#reRun").modal();
											}
										});
							 
						 }

						 $scope.getColorCodeAction=function(status){
								if(status=='Not Started'){
									return '#95a5a6';
								}else if(status=='UnderReview'){
									return '#f1c40f';
								}
								else if(status=='Completed'){
									return '#2ecc71';
								}
								else{
									return '#9b59b6';
								}
							}
						 $scope.getColorCodeStatus=function(status){
								if(status=='Pass'){
									return 'green';
								}else if(status=='Fail'){
									return 'red';
								}
								else{
									
								}
								
							}
						 $scope.toggleTable =  function(index) {
							 // alert(index);
							 $("#tableData"+index).slideToggle("slow");
						 }
						 
						 $scope.toggleTables =  function(index) {
							 // alert(index);
							 $("#tableDatas"+index).slideToggle("slow");
						 }
						 
						 var assignsingleList=[];
						 $scope.assignsignleNAMES=[];
						 $scope.changeAssiginTo=function(newAssiginId,logsId,assignToOneTestCaseList)
						 {

						 $scope.status = null;
						 testRunService
						 .changeAssiginTo(
						 newAssiginId,logsId)
						 .then(
						 function Success(response) {
						 $(".se-pre-con").hide();
						 $scope.status = response.data.serviceResponse;

						  for(var i=0;i<assignToOneTestCaseList.length;i++)
						  {
						  assignsingleList=assignToOneTestCaseList
						   if(newAssiginId == assignsingleList[i].id)
						   {
						   $scope.assignsignleNAMES=assignsingleList[i].name;
						   }
						  }
						 alert("Successfully Assigned To "+ $scope.assignsignleNAMES)
						 $( "#caseCheckbox" ).prop( "checked", false );

						 });
						 }
						 $scope.selectedList = []; 
						 $scope.addMulTCForAssigin=function(logsId)
						 {
							 $scope.assgId="";
							 console.log(logsId);
							 var indexOfDay = $scope.selectedList.indexOf(logsId); 
							    if(indexOfDay === -1) {
							        $scope.selectedList.push(logsId)
							    } else {
							        $scope.selectedList.splice(indexOfDay, 1)
							    }
							   // console.log($scope.assgId+"--");
							 }
						 $scope.selectedTCListRun = []; 
						 $scope.addSelectTCForRun=function(testCase)
						 {
							 if($scope.selectedListstart.length!=0)
								 {
								 $scope.selectedListstart.splice($scope.selectedListstart.indexOf(testCase.srID), 1);
								 console.log($scope.selectedListstart);
								 }
							 if($scope.testCaseList.length!=0)
								 {
									var char="#selectAllCase"+testCase.testGroupId;
						        	 $(char).prop('checked',false);
								 index=0;
								 for(var i = 0; i < $scope.testCaseList.length; i++) {
									    if($scope.testCaseList[i].srID == testCase.srID) {
									        index = 1;
									        break;
									    }
									}
								    if(index > 0) {
								    	 $scope.testCaseList.splice($scope.testCaseList.indexOf(testCase), 1)
								      
								    } else {
								    	
								    	  $scope.testCaseList.push(testCase)
								    }
								    console.log($scope.testCaseList);
								 }
							 else
								 {
								 $scope.testCaseList=[];
							console.log($scope.testCaseList);
							 $scope.assgId="";
							 //var indexOfDay = $scope.selectedList.map(function(e) { return e.id; }).indexOf(testCase.id); 
							 //var i = 1;
							 var index=0;
							 if($scope.selectedTCListRun.length==0){
								 $scope.selectedTCListRun.push(testCase);
								 $scope.testCaseList= $scope.selectedTCListRun;
							 }
							 else{
							 for(var i = 0; i < $scope.selectedTCListRun.length; i++) {
								    if($scope.selectedTCListRun[i].srID == testCase.srID) {
								        index = 1;
								        break;
								    }
								}
							    if(index == 0) {
							        $scope.selectedTCListRun.push(testCase)
							    } else {
							        $scope.selectedTCListRun.splice($scope.selectedTCListRun.indexOf(testCase), 1)
							    }
							    $scope.testCaseList= $scope.selectedTCListRun;
							 }
							    console.log($scope.testCaseList);
								 }
							    
							 }
						 
						 
						 
						$scope.saveMulAssigin = function(assiginId,testRunId,groupId,assignToList)
						{
	
							$scope.selectedList1=[];
							 var char="#selectall"+groupId;
							  var char1=".selectAllCase"+groupId;
							  console.log($scope.selectedList1);
							  if($(char).is(':checked'))
								{
								  $scope.selectedList1 = []; 
								  console.log($scope.selectedList1);
								$(char1).each(function () {
										 id=parseInt($(this).attr('id'));
										 if($('#'+id).is(':checked')){
										 var indexOfDay = $scope.selectedList1.indexOf(id); 
										    if(indexOfDay === -1) {
										        $scope.selectedList1.push(id);
										    } 
										    else
										    {
										        $scope.selectedList1.splice(indexOfDay, 1);										   
										    }
										    }	
										 
								});							
								}
							else
								{								 
								$(char1).each(function () {
									  id= parseInt($(this).attr('id'));
									  if($('#'+id).is(':checked')){
										 var indexOfDay = $scope.selectedList1.indexOf(id); 
										    if(indexOfDay === -1) {
										        $scope.selectedList1.push(id);
										    } 
										    else
										    {
										        $scope.selectedList1.splice(indexOfDay, 1);
										    }
									  }
								});
							}
							if($scope.selectedList1.length>0){
									 testRunService
										.saveMulAssigin(
												$scope.selectedList1,assiginId,testRunId)
										.then(
												function Success(response) {
													$(".se-pre-con").hide();
													$scope.status = response.data.serviceResponse;
													$(char).prop('checked',false);
													//$scope.selectTCAssigin=false;
													$scope.selectedList=[];
													//$scope.assgId="";
													var assignList=[];
													 $scope.assignNAMES=[];
													 for(var i=0;i<assignToList.length;i++)
													 {
													 assignList=assignToList
													  if(assiginId == assignList[i].id)
													  {
													  $scope.assignNAMES=assignList[i].name;
													  }
													 }
													 alert("Successfully  Assign To "+ $scope.assignNAMES)
													$('#AssignDrop'+groupId).val('');
													testRunService	
													.getRunDetails(
															testRunId)
													.then(
															function Success(response) {
																$scope.selectedList1 = [];
																$(".se-pre-con").hide();
																$scope.runTestCaseList = response.data.serviceResponse;
																$scope.getAssignToDetails();
																		});
												});
						}
							else{
								alert("Please select test cases");
							}
							 
						}
					/*	$scope.confirmationDialogForStopRun = function() {
							 var active=$scope.active;
							 console.log(active);
							 console.log("start");
							 if(active.notStarted>0)
							 {
								 $scope.showDialogforStopRun(true);
								    $scope.confirmationDialogConfig = {
								      title: "Alert!!!",
								      message: "Are you sure to delete "+active.runName+"_"+active.round+" As "+active.notStarted+" TestCases are Not Started?",
								      buttons: [{
								        label: "Yes",
								        action: "stopActiveRun"
								      }]
								    };
							 }
							 else{
								 $scope.stopActiveRun();
							 }
							    
							  }*/
						 $scope.showDialogforStopRun = function(flag) {
							    jQuery("#confirmation-dialog-group .modal").modal(flag ? 'show' : 'hide');
							  }
						 $scope.executeDialogActioncompleteRun = function(action) {
							    if(typeof $scope[action] === "function") {
							    		  $scope[action]();
							    	}
							  }
						 $scope.selectAllTC = function() {
							 	$scope.testCaseList=null;
							 	$scope.includeFail = $scope.includeFailResolved = false;
								$scope.statusMsg=null;
								$scope.error="";
								$scope.errormsg="";
								var reRuntestSuites = $scope.createReRunObj.testSuitesId;
								var reRunIdIDForSelectAllTC = $scope.createReRunObj.reRunId;
								if (reRuntestSuites == null || reRuntestSuites == "") {

									$scope.errormsg = "Kindly select Test Suites for Test Case selection..";
									return false;
								}
								
								if($scope.includeall==false)
									{
								$scope.includeall = true;
								testRunService
										.getTestCasesByTestRun(reRunIdIDForSelectAllTC)
										.then(
												function Success(response) {
													$(".se-pre-con").hide();
													$scope.testCaseList = response.data.serviceResponse;																						
												});
									}
								else
								   {
									$scope.includeall = false;
									$scope.testCaseList = null;	
									}
							}
						 $scope.selectFailTCFromReId = function() {
							 	$scope.testCaseList=null;
								$scope.statusMsg=null;
								$scope.includeall=$scope.includeFailResolved=false;
								$scope.error="";
								$scope.errormsg="";
								var reRunId = $scope.createReRunObj.reRunId;
								var runId=$scope.createReRunObj.id;
								console.log(reRunId);
								if ((reRunId == null || reRunId == "") && (runId == null || runId == "")) {

									$scope.errormsg = "Unable to get Required TestCases";
									return false;
								}
								
								if($scope.includeFail==false)
									{
								$scope.includeFail = true;
								testRunService
										.getFailTestCasesByReRunId(reRunId,runId)
										.then(
												function Success(response) {
													$(".se-pre-con").hide();
													$scope.testCaseList = response.data.serviceResponse;																						
												});
									}
								else
									{
									$scope.includeFail = false;
									$scope.testCaseList = null;	
									}
							}
						 
						 $scope.selectFailResolvedTCFromReId=function(){

							 	$scope.testCaseList=null;
								$scope.statusMsg=null;
								$scope.includeall=$scope.includeFail=false;
								$scope.error="";
								$scope.errormsg="";
								let reRunId = $scope.createReRunObj.reRunId;
								let runId=$scope.createReRunObj.id;
								console.log(reRunId);
								if ((reRunId == null || reRunId == "") && (runId == null || runId == "")) {

									$scope.errormsg = "Unable to get Required TestCases";
									return false;
								}
								
								if($scope.includeFailResolved==false)
								{
									$scope.includeFailResolved = true;
									testRunService.getFailResolvedTestCasesByReRunId(reRunId,runId).then(
										function Success(response) {
											$(".se-pre-con").hide();
											$scope.testCaseList = response.data.serviceResponse;																						
									});
								}
								else
									{
									$scope.includeFailResolved = false;
									$scope.testCaseList = null;	
									}
							
						 }
						 
						 $scope.startReRun=function()
				            {
				            	$scope.statusMsg=null;
				            	  $scope.reRunStatus=null;
				            	if($scope.createReRunObj.assignTo==null ||$scope.createReRunObj.assignTo==''){
				                    alert("Assigning To field should not be empty");
				                    return false;
				                    }
				                    if($scope.createReRunObj.targetEndDate==null ||$scope.createReRunObj.targetEndDate==''){
				                    alert("End date field should not be empty");
				                    return false;
				                    }
				                    if($scope.createReRunObj.description==null ||$scope.createReRunObj.description==''){
				                    alert("Description Text field should not be empty");
				                    return false;
				                    }
				                    if($scope.includeall==false)
				                    {
					                    if($scope.includeFail==false){
					                    	if($scope.includeFailResolved==false){
					                    		alert("Kindly select TestCases for Creating a Run");
							                    return false;
					                    	}
						                    
					                    }
				                    }
				                    if($scope.testCaseList==null)
				                    {
				                    alert("No Test Case Available for Starting a Run!!");
				                   
				                    return false;
				                    }
				            	
				            	//console.log($scope.createReRunObj);
				            	$scope.createReRunObj.createdBy=$scope.userId;
				            	$scope.createReRunObj.testCaseList=$scope.testCaseList;
				            	$scope.createReRunObj.projectId=$scope.projectId;
				            	testRunService
								.createRun(
										$scope.createReRunObj)
								.then(
										function Success(response) {
											$(".se-pre-con").hide();
											$scope.reRunStatus = response.data.serviceResponse;
											 $("#reRun").modal('hide');
											$scope.getActiveRun();
												$scope.getInActiveRun();
											$scope.getCompletedRun();
											$('#reRunstatus').delay(5000).fadeOut();
										});
				            	
				            };
				            $scope.showModalForTestExecution = function () {
				                $scope.statusMsg = null;
				                $scope.doneUpload = null;
				                $scope.failUpload = null;
				                $scope.error=null;
				                $scope.createRunObj.testSuitesId='';
				            	document.getElementById("file").value = "";
				                $scope.testsuitesSelected='';
				                $scope.createRunObj.runName=null;
				                $scope.createReRunObj.targetEndDate=null;
				                $scope.createRunObj.description=null;
				    $("#uploadTestExecution").modal();
				    }
				            $scope.shortTable = function() {
				               			            	
				            	$(function() {
				            		
				            	    
				            	    	$('.sortable tbody').selectable({
				            	    
				            	        filter: "tr td.descp",
				            	        selected: function( event, ui ) {
				            	            var row = $(ui.selected).parents('tr').index(),
				            	                col = $(ui.selected).parents('td').index();
				            	          //  $( ".sortable" ).each(function(){
				            	            $(this).find('tr').eq(row)
				            	                        .children('td')
				            	                        .children('input').addClass('checked').prop('checked', true);
				            	            $('.sortable tbody input:not(.checked)').prop('checked', false);
				            	           // });
				            	        },
				            	        unselected: function( event, ui ) {
				            	            var row = $(ui.unselected).parents('tr').index(),
				            	                col = $(ui.unselected).parents('td').index();
				            	            $(this).find('tr').eq(row)
				            	                        .children('td')
				            	                        .children('input').removeClass('checked').prop('checked', false);
				            	        },
				            	        cancel: '.other'
				            	    });
				            	    
				            	   /* $('.sortable tbody input').click(function(){
				            	        var row = $(this).parents('tr').index(),
				            	            col = $(this).parents('td').index();
				            	        if($(this).prop('checked')){
				            	             $('.sortable tbody').find('tr').eq(row)
				            	                        .children('td').eq(col)
				            	                        .children('.descp').addClass('ui-selected');
				            	        }else{
				            	             $('.sortable tbody').find('tr').eq(row)
				            	                        .children('td').eq(col)
				            	                        .children('.descp').removeClass('ui-selected');
				            	        }
				            	    });*/
				            	});
				            
				        	};
				        	
				        	$scope.testCaseList=[];
				        	$scope.changeselect=function(testCaseExecutionLogs,status)
							{	
				        	/*$scope.runid=null;
							$scope.objcet=testCaseExecutionLogs;
							var dataall=Object.values(testCaseExecutionLogs);
							var keyde=Object.keys(testCaseExecutionLogs);
							$scope.runid=dataall[0].testCaseExecutionLogs[0].reRunId;*/
							testRunService.getRunDetailsFilter(
									$scope.testRunId,status)
							.then(
									function Success(response) {
										$(".se-pre-con").hide();
										$scope.runTestCaseList = response.data.serviceResponse;												
										console.log($scope.runTestCaseList);
									});
							};
				        	$scope.selectTestAllCasestart=function(groupId,logIds)
				        	{
				        		
				        	var char="#selectAllCase"+groupId;
				        	var char1=".caseCheckbox"+groupId;
				        	if($(char).is(':checked'))
				        	{
				        	$(char1).each(function () {
				        	 $(this).prop('checked',true);
				        	 id=$(this).attr('id');
				        	 /*if($('#'+id).is(':checked')){*/
				        	var indexOfDay = $scope.testCaseList.indexOf(id);
				        	   if(indexOfDay === -1) {
				        		   var myJSON = JSON.parse(id);
				        	       $scope.testCaseList.push(myJSON);
				        	   }
				        	   else
				        	   {
				        	       $scope.testCaseList.splice(indexOfDay, 1);  
				        	   }
				        	  // }
				        	});
				        	}
				        	else
				        	{
				        		var index=0;
				        	$(char1).each(function () {
				        	$(this).prop('checked',false);
				        	id= $(this).attr('id');
				        	var myJSON = JSON.parse(id);
				        	var indexOfDay = $scope.testCaseList.indexOf(myJSON.srID);
				        	 for(var i = 0; i < $scope.testCaseList.length; i++) {
								    if($scope.testCaseList[i].srID == myJSON.srID) {
								        index = 1;
								        break;
								    }
								}
				        	   if(index === -1) {
				        	       $scope.testCaseList.push(myJSON);
				        	   }
				        	   else
				        	   {
				        	       $scope.testCaseList.splice(indexOfDay, 1);
				        	   }
				        	 });  
				        	}
				        	console.log($scope.testCaseList);
				        	
				        	};
				        	
				        	$scope.assignAction=function(test)
				        	{
				        		console.log(test);
				        		console.log($scope.testCaseExecutionLogs);
				        	}
				        	/*$scope.showPreview = function () {
				        		$scope.statusMsg = null;
				        		$scope.doneUpload = null;
				        		$scope.failUpload = null;
				        		if ($scope.testCaseList == null) {
				        		$scope.error = "Kindly select TestCases for a Run";
				        		return false;
				        		}
				        		$scope.uploadTestExecutionObj.testSuitesId = $scope.testsuitesSelected.testSuitesId;
				        		$scope.uploadTestExecutionObj.createdBy = $scope.userId;
				        		$scope.uploadTestExecutionObj.testCaseList = $scope.testCaseList;
				        		$scope.uploadTestExecutionObj.projectId = $scope.projectId;
				        		$scope.uploadTestExecutionObj.round = 1;
				        		$scope.uploadTestExecutionObj.suiteName =  $scope.testsuitesSelected.testSuitesName;
				        		$scope.uploadTestExecutionObj.runName = $scope.createRunObj.runName;
				        		if ($scope.uploadTestExecutionObj.runName == null) {
				        		$scope.error = "Kindly select TestCases for  a Run";
				        		return false;
				        		}
				        		$scope.uploadTestExecutionObj.targetEndDate = $scope.createReRunObj.targetEndDate;
				        		if ($scope.uploadTestExecutionObj.targetEndDate == null) {
				        		$scope.error = "Kindly select Tentative End Date";
				        		return false;
				        		}
				        		$scope.uploadTestExecutionObj.isActive = $scope.createRunObj.isActive;
				        		if ($scope.uploadTestExecutionObj.isActive == null) {
				        		$scope.error = "Kindly select correct isActive status";
				        		return false;
				        		}
				        		$scope.uploadTestExecutionObj.description = $scope.createRunObj.description;

				        		var data = new FormData();
				        		var attachment = document.getElementById("file").files[0];
				        		$scope.uploadTestExecutionObj.attachment = attachment;
				        		if ($scope.uploadTestExecutionObj.attachment == null) {
				        		$scope.error = "Kindly attach RunTest Execution File";
				        		return false;
				        		}
				        		console.log("attachment " + attachment);
				        		var fileType = ['xlsx', 'xls'];
				        		var extension = $('#file').val().split(".").pop().toLowerCase();
				        		var temp = $('#file').val();
				        		var count = (temp.match(/[.]/g) || []).length;
				        		console.log("count " + count);
				        		if ($scope.projectId != "" && attachment != "") {
				        		if (fileType.indexOf(extension) == -1) {
				        		alert('Please Select A valid File Type');
				        		return false;
				        		}
				        		if (count > 1) {
				        		alert('Please check the extension of Selected File');
				        		return false;
				        		}
				        		console.log("appending  " + data);
				        		data.append("file", attachment);
				        		data.append("suiteName",$scope.uploadTestExecutionObj.suiteName);
				        		data.append("runName", $scope.uploadTestExecutionObj.runName);
				        		data.append("projectId", $scope.projectId);
				        		data.append("sameTestSuite","true");



				        		if (attachment.size <= 10485760) {

				        		testRunService.uploadPreview(data).then(
				        		function Success(response) {
				        		$(".se-pre-con").hide();
				        		// $scope.caseExecutionLogs=response.data.serviceResponse;


				        		$scope.status=response.data.serviceResponse;
				        		if(response.data.serviceStatus==200){
				        		$scope.caseExecutionLogs= response.data.serviceResponse;
				        		$scope.showDiffPreview();
				        		}
				        		if(response.data.serviceStatus==400){
				        		$scope.failUpload=response.data.serviceResponse;

				        		  alert($scope.failUpload);
				        		//$("#showStatus").modal();
				        		  $("#showPreview").modal('hide');
				        		 // $("#uploadTestExecution").modal('hide');
				        		  $("#demo-form").trigger("reset");
				        		//   document.getElementById("file").value = "";
				        		}


				        		$('.status').delay(5000).fadeOut();



				        		});



				        		} else {
				        		alert('Kindly check file size');
				        		}

				        		} else {
				        		$scope.documentstatus = "Kindly provide All Required Details"
				        		}

				        		$("#showPreview").modal();
				        		}
				        		$scope.showDiffPreview = function () {
				        		$scope.statusMsg = null;
				        		$scope.doneUpload = null;
				        		$scope.failUpload = null;
				        		if ($scope.testCaseList == null) {
				        		$scope.error = "Kindly select TestCases for a Run";
				        		return false;
				        		}
				        		$scope.uploadTestExecutionObj.testSuitesId = $scope.testsuitesSelected.testSuitesId;
				        		$scope.uploadTestExecutionObj.createdBy = $scope.userId;
				        		$scope.uploadTestExecutionObj.testCaseList = $scope.testCaseList;
				        		$scope.uploadTestExecutionObj.projectId = $scope.projectId;
				        		$scope.uploadTestExecutionObj.round = 1;
				        		$scope.uploadTestExecutionObj.suiteName =  $scope.testsuitesSelected.testSuitesName;
				        		$scope.uploadTestExecutionObj.runName = $scope.createRunObj.runName;
				        		if ($scope.uploadTestExecutionObj.runName == null) {
				        		$scope.error = "Kindly select TestCases for  a Run";
				        		return false;
				        		}
				        		$scope.uploadTestExecutionObj.targetEndDate = $scope.createReRunObj.targetEndDate;
				        		if ($scope.uploadTestExecutionObj.targetEndDate == null) {
				        		$scope.error = "Kindly select Tentative End Date";
				        		return false;
				        		}
				        		$scope.uploadTestExecutionObj.isActive = $scope.createRunObj.isActive;
				        		if ($scope.uploadTestExecutionObj.isActive == null) {
				        		$scope.error = "Kindly select correct isActive status";
				        		return false;
				        		}
				        		$scope.uploadTestExecutionObj.description = $scope.createRunObj.description;

				        		var data = new FormData();
				        		var attachment = document.getElementById("file").files[0];
				        		$scope.uploadTestExecutionObj.attachment = attachment;
				        		if ($scope.uploadTestExecutionObj.attachment == null) {
				        		$scope.error = "Kindly attach RunTest Execution File";
				        		return false;
				        		}
				        		console.log("attachment " + attachment);
				        		var fileType = ['xlsx', 'xls'];
				        		var extension = $('#file').val().split(".").pop().toLowerCase();
				        		var temp = $('#file').val();
				        		var count = (temp.match(/[.]/g) || []).length;
				        		console.log("count " + count);
				        		if ($scope.projectId != "" && attachment != "") {
				        		if (fileType.indexOf(extension) == -1) {
				        		alert('Please Select A valid File Type');
				        		return false;
				        		}
				        		if (count > 1) {
				        		alert('Please check the extension of Selected File');
				        		return false;
				        		}
				        		console.log("appending  " + data);
				        		data.append("file", attachment);
				        		data.append("suiteName",$scope.uploadTestExecutionObj.suiteName);
				        		data.append("runName", $scope.uploadTestExecutionObj.runName);
				        		data.append("projectId", $scope.projectId);
				        		data.append("sameTestSuite","false");



				        		if (attachment.size <= 10485760) {



				        		testRunService.uploadDiffPreview(data).then(
				        		function Success(response) {
				        		$(".se-pre-con").hide();
				        		// $scope.caseExecutionLogs=response.data.serviceResponse;


				        		$scope.status=response.data.serviceResponse;
				        		if(response.data.serviceStatus==200){
				        		$scope.diffCaseExecutionLogs= response.data.serviceResponse;
				        		}
				        		if(response.data.serviceStatus==400){
				        		$scope.failUpload=response.data.serviceResponse;

				        		  alert($scope.failUpload);
				        		//$("#showStatus").modal();
				        		  $("#showPreview").modal('hide');
				        		 // $("#uploadTestExecution").modal('hide');
				        		  $("#demo-form").trigger("reset");
				        		  document.getElementById("file").value = "";
				        		}


				        		$('.status').delay(5000).fadeOut();



				        		});


				        		} else {
				        		alert('Kindly check file size');
				        		}

				        		} else {
				        		$scope.documentstatus = "Kindly provide All Required Details"
				        		}

				        		$("#showPreview").modal();
				        		}
*/				        	$scope.findDuplicate = function (runName) {
				        		// console.log(index)
				        		$scope.statusMsg = null;
				        		testRunService
				        		.findDuplicate(
				        		runName)
				        		.then(
				        		function Success(response) {
				        		$(".se-pre-con").hide();
	if(response.data.serviceStatus==400){

				        				$scope.errormsg="response.data.serviceResponse";
				        				
				        				  alert(response.data.serviceResponse);}
				        		});
				        		$('#errormsg').delay(5000).fadeOut();

				        		           };
				        /*	$scope.updateTestrun=function()
				        	{
				        		console.log($scope.selectedListstart);
				        		$scope.createRunObj.createdBy=$scope.userId;
				            	$scope.createRunObj.testCaseList=$scope.selectedListstart
				            	$scope.createRunObj.projectId=$scope.projectId;
				            	$scope.createRunObj.round=1;	
				        		testRunService
								.createRun(
										$scope.createReRunObj)
								.then(
										function Success(response) {
											$(".se-pre-con").hide();
											$scope.reRunStatus = response.data.serviceResponse;
											 $("#reRun").modal('hide');
											$scope.getActiveRun();
												$scope.getInActiveRun();
											$scope.getCompletedRun();
											$('#reRunstatus').delay(5000).fadeOut();
										});
				        	}*/
				        		       	$scope.projectName=localStorage.getItem('projectName');

				        		           $scope.matissynchronized=function()
				        		           {
				        		        	   if($scope.projectBugToolID){
				        		        		   dashboardService.getProjectBugStatus($scope.projectName, $scope.projectId).then(function Success(response) {
					        		        			 $(".se-pre-con").fadeOut('slow');
					        		        			 if(response.data.serviceStatus=="Success"){
					        		        				 alert("Data has been synchronized.. Kindly proceed..");
					        		        			 }
					        		        			 else{
					        		        				 alert("We are unable to process your request.. Please contact with Admin..\n" +
					        		        				 		"You can be able to rerun..")
					        		        			 }
					        		        		}); 
				        		        	   }
				        		        	   
				        		        	   
				        		           }
				     
				});