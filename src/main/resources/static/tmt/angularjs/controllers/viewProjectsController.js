demoApp
.controller(
		"viewProjectsController",
		function($scope, $interval, $state, $stateParams,$location,$anchorScroll, $rootScope, dashboardService,
				viewProjectsService) {
			$(document).ready(function(){
				checkcookie();
				var token=localStorage.getItem('tokenID');
				if (token==null|| token=="")
				{
					window.location.href = "../login.html";
				}
			});
			$rootScope.checkHome();

			$scope.showAdminViewEntitlement;

			$scope.roleId = localStorage.getItem('userRole');

			//$scope.testcase={};
			//console.log($scope.testcase.accessProject);
			$scope.viewallproject=function()
			{
				/*if($scope.user_role==='100'  || $scope.user_role==='101' )
				{

					entitlementService.getadminViewEntitlement($scope.id,$scope.user_role,$scope.projectwiserole).then(function Success(response){
						$scope.projectList=response.data.serviceResponse;
						console.log($scope.projectList);
						
					

						$(".se-pre-con").fadeOut('slow');
					});
				}*/
				dashboardService.configProjects($scope.user_role,$scope.id).then(function Success(response){
					console.log(response.data);
					$scope.FailStatus=null;
							if(response.data.serviceResponse!=null && response.data.serviceStatus=="Success"){
							$scope.allproject=response.data.serviceResponse;
							$scope.newallproject=$scope.allproject;
							console.log($scope.allproject);
							
							
							console.log($scope.allproject.length);
							for(var i=0;i<$scope.allproject.length;i++){
								var d=null;
								d=$scope.allproject[i].projectStartDate;
								
								if(d!=null){
									var date = new Date(d); 
									var year = date.getFullYear();
									var month = ("0" + (date.getMonth() + 1)).slice(-2);
									var day = ("0" + date.getDate()).slice(-2);
									    
									
									$scope.allproject[i].projectStartDate=day+"-"+month+"-"+year;
									
								}
								else{
									$scope.allproject[i].projectStartDate="";
								}
								
							}
							for(var i=0;i<$scope.allproject.length;i++){
								var d=null;
								d=$scope.allproject[i].projectEndDate;
								
								if(d!=null){
									var date = new Date(d); 
									var year = date.getFullYear();
									var month = ("0" + (date.getMonth() + 1)).slice(-2);
									var day = ("0" + date.getDate()).slice(-2);
									    
									
									$scope.allproject[i].projectEndDate=day+"-"+month+"-"+year;
									
								}
								else{
									$scope.allproject[i].projectEndDate="";
								}
								
							}
							for(var i=0;i<$scope.allproject.length;i++){
								var d=null;
								d=$scope.allproject[i].createdTime;
								
								if(d!=null){
									var date = new Date(d); 
									var year = date.getFullYear();
									var month = ("0" + (date.getMonth() + 1)).slice(-2);
									var day = ("0" + date.getDate()).slice(-2);
									    
									
									$scope.allproject[i].createdTime=day+"-"+month+"-"+year;
									
								}
								else{
									$scope.allproject[i].createdTime="";
								}
								
							}
							}
							else if(response.data.serviceStatus=="Fail" && response.data.serviceError=="Data not found"){
								$scope.FailStatus=response.data.serviceError;
/*								alert("data not found");
*/							}
							else if(response.data.serviceResponse=="SomeThings Went Wrong"){
								$scope.FailStatus=response.data.serviceResponse;
/*								alert("SomeThings Went Wrong");
*/							}
							else{
								$scope.FailStatus="Something Went Wrong";
/*								alert("SomeThings Went Wrong");
*/							}
							
							$(".se-pre-con").fadeOut('slow');
							
						});
				/*dashboardService.getAllProject().then(
								function Success(response) {
									$(".se-pre-con").hide();
									$scope.allproject=response.data.serviceResponse;

								});*/

			}

			$scope.changeEvent=function(valuee){
				console.log(valuee);
			}

			$scope.projectName = localStorage.getItem('projectName');
			$scope.projectId = localStorage.getItem('projectId');
			$scope.id= localStorage.getItem('userid');
			$scope.managerID= localStorage.getItem('managerID');
			$scope.user_role= localStorage.getItem('userRole');
			$scope.projectwiserole=localStorage.getItem('projectwiserole');
			if($scope.user_role!=undefined)
			{
				if($scope.user_role==="100")
				{
					$("#testRun").css("display","unset");
					$("#todo").css("display","unset");
					$("#testSuites").css("display","unset");
					$("#Automation").css("display","unset");
				}
				if($scope.projectwiserole=="Manager" || $scope.projectwiserole==="Tester")
				{
					$("#todo").css("display","unset");
					$("#testSuites").css("display","unset");
					$("#Automation").css("display","unset");
					if($scope.projectwiserole==="Manager")
					{
						$("#testRun").css("display","unset");

					}
					else if($scope.projectwiserole==="Tester")
					{

						if($scope.url=='/testRun')
						{
							$state.transitionTo('error');
						}

					}
					else if($scope.projectwiserole==="Readonly")
					{

					}

				}
			}

			else
			{
				$state.transitionTo('error');
			}
			//alert("getShowProjectDetailsss"+$scope.projectId);
			$scope.showProjectWorker=function()
			{
				entitlementService.getShowProjectDetails($scope.projectId).then(function Success(response){
					$scope.getShowProject=response.data.serviceResponse;
					$(".se-pre-con").fadeOut('slow');
				});
			}
			$scope.toggleTable =  function(index) {
				// alert(index);
				$("#tableData"+index).slideToggle("slow");
			}

			




			
			$scope.changeProjectStatus=function(projectDetaildata)
			{
				
					console.log(projectDetaildata.projectID);
					console.log(projectDetaildata.isActive);
					console.log($scope.user_role);
					
					viewProjectsService.changeProjectStatus(projectDetaildata.projectID,projectDetaildata.isActive,$scope.user_role).then(function Success(response){
						console.log(response);
						if(response.data.serviceResponse!=null && response.data.serviceStatus=="Success"){
							alert("Status Updated Successfully :"+ projectDetaildata.projectName);
							$scope.searhProjectByProjectStatus();
						}
							
						else if(response.data.serviceStatus=="Fail" && response.data.serviceStatus=="Unauthorized user"){
							alert("Fail to update Project Status");
						}
						else if(response.data.serviceStatus=="Fail" && response.data.serviceStatus=="Something Went Wrong"){
							alert("Something Went Wrong");
						}
						else{
							alert("Something Went Wrong");
						}
					});
					/*entitlementService.GiveAccessUserEntitlement(userDetaildata.projectId,userDetaildata.userid,userDetaildata.accessProject,userDetaildata.projectaccessid,userDetaildata.userrole).then(function Success(response){
						$scope.showAdminViewEntitlement=response.data.serviceResponse;

						if(response.data.serviceStatus==='Success')
						{
							alert("Successfully Given Access to :"+$scope.showAdminViewEntitlement.username);
							
						}
						else
						{
							alert("Something went wrong with Access while giving access to: "+$scope.showAdminViewEntitlement.username);
						}
						$scope.searhProject(); //changes
						$scope.adminViewEntitlement();
						$(".se-pre-con").fadeOut('slow');
					});*/

				
			}
			$scope.searhProjectByProjectStatus=function()
			{
					var j=0;
					var list=[];
					$scope.allproject=$scope.newallproject;
					if($scope.searhProjectByDropDown==""){
						$scope.allproject=$scope.newallproject;
					}
					else if($scope.searhProjectByDropDown=='Y'){
						for(var i=0;i<$scope.allproject.length;i++){

							if($scope.allproject[i].isActive=='Y'){
								
								list.push($scope.allproject[i]);
								
								
							
							}
							
						}
						
						$scope.allproject=list;
						
					}
					else if($scope.searhProjectByDropDown=='N'){
						for(var i=0;i<$scope.allproject.length;i++){
							if($scope.allproject[i].isActive=='N'){
								
								list.push($scope.allproject[i]);
							
							}
							
						}
						
						$scope.allproject=list;
					}
					else{
						$scope.viewallproject();
					}

				
			}

			
			
			
			$scope.viewallproject();

		});