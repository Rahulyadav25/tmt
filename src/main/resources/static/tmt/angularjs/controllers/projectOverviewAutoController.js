demoApp
		.controller("projectOverviewAutoController", function($scope, $interval,
				$state, $stateParams,$location, $rootScope, projectOverviewAutoService,dashboardService) {
			$(document).ready(function(){
				checkcookie();
				var token=localStorage.getItem('tokenID');
				if (token==null|| token=="")
				{
				window.location.href = "../login.html";
				}
				});
			$scope.projectid=localStorage.getItem('projectId');
			var projectName=localStorage.getItem('projectName');
			
			$scope.id= localStorage.getItem('userid');
			console.log($scope.id);
			$scope.user_role= localStorage.getItem('userRole');	
			$rootScope.checkHome();
			$scope.projectName1=localStorage.getItem('projectName');
			$("#overViewsAuto a.nav-link").addClass("active");
			var myChart=null;
			var myChart1=null;
			//Getting Suites Details Specific Data
			$scope.showMul1=function(){
				$(".test").select2({
					closeOnSelect : false,
					placeholder : "Runs",
					allowHtml: true,
					allowClear: true,
					tags: true,
					
				});
							
				};
				$scope.showMul1();	
			var countOfColumnList;
			$("select").on("select2:select", function (evt) {
				  $scope.fromDate=null;
					 $scope.toDate=null;
				var element = evt.params.data.element;
				  var $element = $(element);
				  $element.detach();
				  $(this).append($element);
				  $(this).trigger("change");
				  var count = $(this).select2('data').length 
			
				 if(count==countOfColumnList){
					$("#selectCheckBox").prop('checked', true);
				}else{
					$("#selectCheckBox").prop('checked', false);
				}
				
				});
			$("select").on("select2:unselect", function (evt) {
				$scope.fromDate=null;
				 $scope.toDate=null;
				$("#selectCheckBox").prop('checked', false);
				
				  
				});	
			$("select").on('select2:close', function (evt) {
				   $scope.fromDate=null;
				      $scope.toDate=null;
			       $('#select').blur();
			       $scope.specificrunwisedata();
			    
			})
			.on('select2:blur', function(evt) {
			     
			}); 
			$('#selectCheckBox').change(function() {
				   $scope.fromDate=null;
				      $scope.toDate=null;
		        if(this.checked) {
		         
		        	 $(".test > option").prop('selected','selected').trigger('change');
				 
		        }else{
		        	 $('.test').val("R"+$scope.activerunlist.runId+"_"+$scope.activerunlist.scenarioID).trigger('change');
					
				 }
		    });
			$scope.selectMultiple=function(){
				$scope.fromDate=null;
				 $scope.toDate=null;
				 if($scope.selMul==true){
					 $('.test').val("R"+$scope.activerunlist.runId+"_"+$scope.activerunlist.scenarioID).trigger('change');
					 
					 }else{
						 
						 $(".test > option").prop('selected','selected').trigger('change');
					 }
			}
			
			$scope.getoverviewdetails= function() {
				projectOverviewAutoService.getoverviewdetails($scope.projectid,$scope.id,$scope.user_role).then(
						function Success(response)
						 {		
					     $scope.getoverviewdetailList=response.data.serviceResponse;
					     $(".se-pre-con").hide();
			              });			
				
				
			}
			$scope.cleardata=function(){
				$('.test').val(null).trigger('change');
				$('#selectCheckBox').prop('checked',false);
			}
			$scope.activerunlist=[];
		
			$scope.getallActiveRun= function() {
				projectOverviewAutoService.getallActiveRun($scope.projectid,$scope.id,$scope.user_role).then(
						function Success(response)
						 {		
							$scope.activerunlist=response.data.serviceResponse;
							countOfColumnList = $scope.activerunlist.length;
							var acmArray=[];
							for(var k=0;k<$scope.activerunlist.length;k++){
								var acmOBJ =[];
								acmOBJ.push("R"+$scope.activerunlist[k].runId+"_"+$scope.activerunlist[k].scenarioID);
								acmOBJ.push($scope.activerunlist[k].totalTestCases);
								acmOBJ.push($scope.activerunlist[k].totalTestSuites);
								acmOBJ.push($scope.activerunlist[k].totalPassed);
								acmOBJ.push($scope.activerunlist[k].totalFailed);
								acmArray.push(acmOBJ);
								}
							var table = $('#example1').DataTable();
							table.destroy();
							$("#example1").DataTable({
						        data: acmArray,
						        dom: 'Bfrtip',
						        select: true,
						        buttons: [
						        	{
						           extend: 'collection',
						           text: 'Export',
									buttons: [
									   {
									       extend: "excelHtml5",
									       fileName:  "CustomFileName" + ".xlsx",
											title: projectName+'autoOverview',
									       exportOptions: {
									           columns: ':visible'
									       },
									       //CharSet: "utf8",
									       exportData: { decodeEntities: true }
									   },
									   {
									       extend: "csvHtml5",
									       fileName:  "CustomFileName" + ".csv",
											title: projectName+'autoOverview',
									       exportOptions: {
									           columns: ':visible'
									       },
									       exportData: {decodeEntities:true}
									   },
									   {
									       extend: "pdfHtml5",
									       fileName:  "CustomFileName" + ".pdf",
											title: projectName+'autoOverview',
									       exportOptions: {
									           columns: ':visible'
									       },
									       exportData: {decodeEntities:true}
									   }
									  ]
						        	}
						    ],
						    
						    });
							
							
							
							////////////////////////////
					    	 var tempLogs=[];
								var tempTotalPassed=[];
								var tempTotalFailed=[];	
								var tempArrayRunName=[];
							
								
								var tempArrayblocker=[];
								var tempArraytotalcritical=[];
								var tempArraytotalmajor=[];
								var tempArraytotalminor=[];
								var tempOpen=[];
								var tempReOpen=[];
								var tempReclosed=[];
								
								
							for(var i=0;i<$scope.activerunlist.length;i++){
								if(i<26){
									tempArrayRunName.push("R"+$scope.activerunlist[i].runId+"_"+$scope.activerunlist[i].scenarioID);
									tempLogs.push($scope.activerunlist[i].totalTestSuites);
									tempTotalPassed.push($scope.activerunlist[i].totalPassed);
									tempTotalFailed.push($scope.activerunlist[i].totalFailed);
									
									tempArrayblocker.push($scope.activerunlist[i].defectTrackers.totalBlocker);
									tempArraytotalcritical.push($scope.activerunlist[i].defectTrackers.totalCritical);
									tempArraytotalmajor.push($scope.activerunlist[i].defectTrackers.totalMajor);
									tempArraytotalminor.push($scope.activerunlist[i].defectTrackers.totalMinor);
									tempOpen.push($scope.activerunlist[i].defectTrackers.totalOpen);
									tempReOpen.push($scope.activerunlist[i].defectTrackers.totalReOpen);
									tempReclosed.push($scope.activerunlist[i].defectTrackers.totalClosed);
								}
								///$scope.activerunlist.push("R"+$scope.activerunlist[i].runId+"_"+$scope.activerunlist[i].scenarioID);
								//countOfColumnList = $scope.activerunlist.length;
								}
							var ctx = document.getElementById("Chart1");
							var barOptions_stacked = {
									tooltips: {
										//enabled:false,
								            displayColors:true,
								            backgroundColor: '#227799'
								             },
								    hover :{
								        animationDuration:0
								    },
								    scales: {
								       
								        xAxes: [{
								              barThickness:1,
								            ticks: {
								                beginAtZero:true,
								                fontFamily: "'Open Sans Bold', sans-serif",
								                fontSize:11
								            },
								            scaleLabel:{
								                display:false
								            },
								            gridLines: {
								            }, 
								            stacked: true
								        }],
								        yAxes: [{
								          
								            gridLines: {
								                display:false,
								                color: "#fff",
								                zeroLineColor: "#fff",
								                zeroLineWidth: 0
								            },
								            ticks: {
								                fontFamily: "'Open Sans Bold', sans-serif",
								                fontSize:11
								            },
								            barPercentage: 0.5,
								            stacked: true
								        }]
								    },
								    legend:{
								      display:true,
								      position:'bottom'
								    },
								    pointLabelFontFamily : "Quadon Extra Bold",
								    scaleFontFamily : "Quadon Extra Bold",
								    
								};
							myChart = new Chart(ctx, {
							    type: 'horizontalBar',
							    data: {
							        labels: tempArrayRunName,
							        
							        datasets: [{
							            label:'Total Logs',
							            data: tempLogs,
							            backgroundColor: "#d73e4d",
							            hoverBackgroundColor: "#d7523e"
							        },{
							            label:'Passed Logs',
							            data: tempTotalPassed,
							            backgroundColor: "Green",
							            hoverBackgroundColor: "#036903"
							        },{
							            label:'Failed Logs',
							            data: tempTotalFailed,
							            backgroundColor: "#000",
							            hoverBackgroundColor: "#b3abaa"
							        }]
							    },

							    options: barOptions_stacked,
							});
							///////////////////
							
							
						
							var dates =tempArrayRunName;
					//		myChart1.destroy();
							  var ctx = document.getElementById('bar-chart');
							  myChart1 = new Chart(ctx, {
							      type: 'bar',
							      data: {
							          labels: tempArrayRunName,
							          datasets: [{
							              stack: '0',
							              backgroundColor: "#d0bfff",
							              label: 'Open',
							              data: tempOpen
							          },
							          
							          {
							            stack: '0',
							              backgroundColor:"#748ffc",
							              label: 'Reopen',
							              data:tempReOpen
							          },
							          {
								            stack: '0',
								              backgroundColor: "#d73e4d",
								              label: 'closed',
								              data: tempReclosed
								          },
								          {
								        	  stack: '1',
								             backgroundColor: '#eebefa',
								              label: 'Blocker',
								              data: tempArrayblocker
								          },
							              {
								        	  stack: '1',
								              backgroundColor: '#f03e3e',
								              label: 'Critical',
								              data: tempArraytotalcritical
								          },
								          {
								        	  stack: '1',
								              backgroundColor: '#f59f00',
								              label: 'Major',
								              data: tempArraytotalmajor
								          },
								          {
								        	  stack: '1',
								              backgroundColor: '#bac8ff',
								              label: 'Minor',
								              data: tempArraytotalminor
								          }
							          ]
							      },
							      options: {
							          scales: {
							              yAxes: [{
							                  ticks: {
							                      beginAtZero:true
							                  },
							                 stacked: true
							              }]
							          },
							          legend:{
									      display:true,
									      position:'bottom'
									    },
							      }
							  });
							
							///////////////////
							
							$(".se-pre-con").fadeOut('slow');
					    
						 });			
				
				
			}
			
			$scope.datewiserun=function(){
				
				
				if(($scope.toDate!==null||$scope.toDate!==""||$scope.toDate!==undefined) &&
						($scope.fromDate===null||$scope.fromDate===""||$scope.fromDate===undefined)){
					alert("Please select Start Date");
						
							return false;
						}
				
				else if(($scope.fromDate!==null||$scope.fromDate!==""||$scope.fromDate==undefined) &&
						($scope.toDate===null||$scope.toDate===""||$scope.toDate===undefined)){
					alert("Please select End Date");
						
							return false;
						}
				/*
				 if(($scope.toDate!==null||$scope.toDate!==""||$scope.toDate!==undefined) &&
						($scope.fromDate===null||$scope.fromDate===""||$scope.fromDate===undefined)){
					alert("Please select Start Date");
						
							return false;
						}*/
				$('.test').val(null).trigger('change');
				$('#selectCheckBox').prop('checked',false);
				$scope.getoverviewdetails();
				$scope.getLogDetails();
			}
			$scope.getoverviewdetails();
			$scope.getallActiveRun();
			$scope.activerunlist=[];
			$scope.getLogDetails= function() {
				
				
				projectOverviewAutoService.getLogDetails($scope.projectid,$scope.id,$scope.user_role,$scope.fromDate,$scope.toDate).then(
						function Success(response)
						 {		
					     $scope.getLogDetailsList=response.data.serviceResponse;
					     if(response.data.serviceStatus==200){
					    	 var tempLogs=[];
								var tempTotalPassed=[];
								var tempTotalFailed=[];	
								var tempArrayRunName=[];
								var tempArrayblocker=[];
								var tempArraytotalcritical=[];
								var tempArraytotalmajor=[];
								var tempArraytotalminor=[];
								var tempOpen=[];
								var tempReOpen=[];
								var tempReclosed=[];
								
							for(var i=0;i<$scope.getLogDetailsList.length;i++){
								if(i<26){
									tempArrayRunName.push("R"+$scope.getLogDetailsList[i].runId+"_"+$scope.getLogDetailsList[i].scenarioID);
									tempLogs.push($scope.getLogDetailsList[i].totalTestSuites);
									tempTotalPassed.push($scope.getLogDetailsList[i].totalPassed);
									tempTotalFailed.push($scope.getLogDetailsList[i].totalFailed);
									
									tempArrayblocker.push($scope.getLogDetailsList[i].defectTrackers.totalBlocker);
									tempArraytotalcritical.push($scope.getLogDetailsList[i].defectTrackers.totalCritical);
									tempArraytotalmajor.push($scope.getLogDetailsList[i].defectTrackers.totalMajor);
									tempArraytotalminor.push($scope.getLogDetailsList[i].defectTrackers.totalMinor);
									tempOpen.push($scope.getLogDetailsList[i].defectTrackers.totalOpen);
									tempReOpen.push($scope.getLogDetailsList[i].defectTrackers.totalReOpen);
									tempReclosed.push($scope.getLogDetailsList[i].defectTrackers.totalClosed);
								}
								///$scope.activerunlist.push("R"+$scope.getLogDetailsList[i].runId+"_"+$scope.getLogDetailsList[i].scenarioID);
								//countOfColumnList = $scope.getLogDetailsList.length;
								}
							var ctx = document.getElementById("Chart1");
							var barOptions_stacked = {
									tooltips: {
										//enabled:false,
								            displayColors:true,
								            backgroundColor: '#227799'
								             },
								    hover :{
								        animationDuration:0
								    },
								    scales: {
								       
								        xAxes: [{
								              barThickness:1,
								            ticks: {
								                beginAtZero:true,
								                fontFamily: "'Open Sans Bold', sans-serif",
								                fontSize:11
								            },
								            scaleLabel:{
								                display:false
								            },
								            gridLines: {
								            }, 
								            stacked: true
								        }],
								        yAxes: [{
								          
								            gridLines: {
								                display:false,
								                color: "#fff",
								                zeroLineColor: "#fff",
								                zeroLineWidth: 0
								            },
								            ticks: {
								                fontFamily: "'Open Sans Bold', sans-serif",
								                fontSize:11
								            },
								            barPercentage: 0.5,
								            stacked: true
								        }]
								    },
								    legend:{
								      display:true,
								      position:'bottom'
								    },
								    pointLabelFontFamily : "Quadon Extra Bold",
								    scaleFontFamily : "Quadon Extra Bold",
								    
								};
							myChart = new Chart(ctx, {
							    type: 'horizontalBar',
							    data: {
							        labels: tempArrayRunName,
							        
							        datasets: [{
							            label:'Total Logs',
							            data: tempLogs,
							            backgroundColor: "#d73e4d",
							            hoverBackgroundColor: "#d7523e"
							        },{
							            label:'Passed Logs',
							            data: tempTotalPassed,
							            backgroundColor: "Green",
							            hoverBackgroundColor: "#036903"
							        },{
							            label:'Failed Logs',
							            data: tempTotalFailed,
							            backgroundColor: "gray",
							            hoverBackgroundColor: "#b3abaa"
							        }]
							    },

							    options: barOptions_stacked,
							});
							////
							
							var dates =tempArrayRunName;
					//		myChart1.destroy();
							  var ctx = document.getElementById('bar-chart');
							  myChart1 = new Chart(ctx, {
							      type: 'bar',
							      data: {
							          labels: tempArrayRunName,
							          datasets: [{
							              stack: '0',
							              backgroundColor: "#d0bfff",
							              label: 'Open',
							              data: tempOpen
							          },
							          
							          {
							            stack: '0',
							              backgroundColor:"#748ffc",
							              label: 'Reopen',
							              data:tempReOpen
							          },
							          {
								            stack: '0',
								              backgroundColor: "#d73e4d",
								              label: 'closed',
								              data: tempReclosed
								          },
								          {
								        	  stack: '1',
								             backgroundColor: '#eebefa',
								              label: 'Blocker',
								              data: tempArrayblocker
								          },
							              {
								        	  stack: '1',
								              backgroundColor: '#f03e3e',
								              label: 'Critical',
								              data: tempArraytotalcritical
								          },
								          {
								        	  stack: '1',
								              backgroundColor: '#f59f00',
								              label: 'Major',
								              data: tempArraytotalmajor
								          },
								          {
								        	  stack: '1',
								              backgroundColor: '#bac8ff',
								              label: 'Minor',
								              data: tempArraytotalminor
								          }
							          ]
							      },
							      options: {
							          scales: {
							              yAxes: [{
							                  ticks: {
							                      beginAtZero:true
							                  },
							                 stacked: true
							              }]
							          },
							          legend:{
									      display:true,
									      position:'bottom'
									    },
							      }
							  });
							////

							
							
					     }	
					     
					     $(".se-pre-con").hide();
			              });			
				
				
			}
			$scope.getLogDetails();
			$scope.specificrunwisedata=function()
			{
				
				var applist=$('#columnlist').val();
				console.log(applist);
			if(applist.length === 0)
				{
			  alert("Please select Run");
			  return false;
				}
			else
				{
				projectOverviewAutoService.runwiseAutodetails(applist,$scope.projectid,$scope.id,$scope.user_role).then(function Success(response)
				{
					 $(".se-pre-con").fadeOut('slow');
					 $scope.getLogDetailsList=response.data.serviceResponse;
				     if(response.data.serviceStatus==200){
				    	 var tempLogs=[];
							var tempTotalPassed=[];
							var tempTotalFailed=[];	
							var tempArrayRunName=[];
						
							var tempArrayblocker=[];
							var tempArraytotalcritical=[];
							var tempArraytotalmajor=[];
							var tempArraytotalminor=[];
							var tempOpen=[];
							var tempReOpen=[];
							var tempReclosed=[];

							
						for(var i=0;i<$scope.getLogDetailsList.length;i++){
							if(i<26){
								tempArrayRunName.push("R"+$scope.getLogDetailsList[i].runId+"_"+$scope.getLogDetailsList[i].scenarioID);
								tempLogs.push($scope.getLogDetailsList[i].totalTestSuites);
								tempTotalPassed.push($scope.getLogDetailsList[i].totalPassed);
								tempTotalFailed.push($scope.getLogDetailsList[i].totalFailed);
								
								tempArrayblocker.push($scope.getLogDetailsList[i].defectTrackers.totalBlocker);
								tempArraytotalcritical.push($scope.getLogDetailsList[i].defectTrackers.totalCritical);
								tempArraytotalmajor.push($scope.getLogDetailsList[i].defectTrackers.totalMajor);
								tempArraytotalminor.push($scope.getLogDetailsList[i].defectTrackers.totalMinor);
								tempOpen.push($scope.getLogDetailsList[i].defectTrackers.totalOpen);
								tempReOpen.push($scope.getLogDetailsList[i].defectTrackers.totalReOpen);
								tempReclosed.push($scope.getLogDetailsList[i].defectTrackers.totalClosed);
							}
							///$scope.activerunlist.push("R"+$scope.getLogDetailsList[i].runId+"_"+$scope.getLogDetailsList[i].scenarioID);
							//countOfColumnList = $scope.getLogDetailsList.length;
							}
						 // myChart.destroy();
						var ctx = document.getElementById("Chart1");
						var barOptions_stacked = {
								tooltips: {
									//enabled:false,
							            displayColors:true,
							            backgroundColor: '#227799'
							             },
							    hover :{
							        animationDuration:0
							    },
							    scales: {
							       
							        xAxes: [{
							              barThickness:1,
							            ticks: {
							                beginAtZero:true,
							                fontFamily: "'Open Sans Bold', sans-serif",
							                fontSize:11
							            },
							            scaleLabel:{
							                display:false
							            },
							            gridLines: {
							            }, 
							            stacked: true
							        }],
							        yAxes: [{
							          
							            gridLines: {
							                display:false,
							                color: "#fff",
							                zeroLineColor: "#fff",
							                zeroLineWidth: 0
							            },
							            ticks: {
							                fontFamily: "'Open Sans Bold', sans-serif",
							                fontSize:11
							            },
							            barPercentage: 0.5,
							            stacked: true
							        }]
							    },
							    legend:{
							      display:true,
							      position:'bottom'
							    },
							    pointLabelFontFamily : "Quadon Extra Bold",
							    scaleFontFamily : "Quadon Extra Bold",
							    
							};
						myChart = new Chart(ctx, {
						    type: 'horizontalBar',
						    data: {
						        labels: tempArrayRunName,
						        
						        datasets: [{
						            label:'Total Logs',
						            data: tempLogs,
						            backgroundColor: "#d73e4d",
						            hoverBackgroundColor: "#d7523e"
						        },{
						            label:'Passed Logs',
						            data: tempTotalPassed,
						            backgroundColor: "Green",
						            hoverBackgroundColor: "#036903"
						        },{
						            label:'Failed Logs',
						            data: tempTotalFailed,
						            backgroundColor: "gray",
						            hoverBackgroundColor: "#b3abaa"
						        }]
						    },

						    options: barOptions_stacked,
						});

						////
						
						var dates =tempArrayRunName;
				//		myChart1.destroy();
						  var ctx = document.getElementById('bar-chart');
						  myChart1 = new Chart(ctx, {
						      type: 'bar',
						      data: {
						          labels: tempArrayRunName,
						          datasets: [{
						              stack: '0',
						              backgroundColor: "#d0bfff",
						              label: 'Open',
						              data: tempOpen
						          },
						          
						          {
						            stack: '0',
						              backgroundColor:"#748ffc",
						              label: 'Reopen',
						              data:tempReOpen
						          },
						          {
							            stack: '0',
							              backgroundColor: "#d73e4d",
							              label: 'closed',
							              data: tempReclosed
							          },
							          {
							        	  stack: '1',
							             backgroundColor: '#eebefa',
							              label: 'Blocker',
							              data: tempArrayblocker
							          },
						              {
							        	  stack: '1',
							              backgroundColor: '#f03e3e',
							              label: 'Critical',
							              data: tempArraytotalcritical
							          },
							          {
							        	  stack: '1',
							              backgroundColor: '#f59f00',
							              label: 'Major',
							              data: tempArraytotalmajor
							          },
							          {
							        	  stack: '1',
							              backgroundColor: '#bac8ff',
							              label: 'Minor',
							              data: tempArraytotalminor
							          }
						          ]
						      },
						      options: {
						          scales: {
						              yAxes: [{
						                  ticks: {
						                      beginAtZero:true
						                  },
						                 stacked: true
						              }]
						          },
						          legend:{
								      display:true,
								      position:'bottom'
								    },
						      }
						  });
						////
						
						
				     } 				     				
				$(".se-pre-con").fadeOut('slow');		
				
			});

				}
			
			}
			// myChart.destroy();
		
			
			$scope.getProjectDetailssingle=function()
			{
				
				projectOverviewAutoService.getSingleproject($scope.projectid).then(function Success(response) {
					$scope.getprojectDetails=response.data.serviceResponse;
					console.log($scope.getprojectDetails);
					$scope.delayTimeProject=response.data.serviceResponse.delayTimeProject;
					$scope.projectNamedetail=response.data.serviceResponse.projectName;
					$scope.applicationNamedetail=response.data.serviceResponse.applicationName;
					console.log(response.data.serviceResponse.isMannual);
					if(response.data.serviceResponse.isActive=='Y')
					{
						$scope.projectstatusdetail="True";
					}
					else
						{
						$scope.projectstatusdetail="False";
						}
					if(response.data.serviceResponse.isAutomation=='true' && response.data.serviceResponse.isMannual=='true')
					{
						$scope.isAutodetail ="Automation";
						$scope.isMannualdetail = "Manual";
						}
					else if(response.data.serviceResponse.isAutomation=='true')
						{
						$scope.isAutodetail = "Automation"
						$scope.isMannualdetail = "";
						}
					else if(response.data.serviceResponse.isMannual==='true')
							
						{
						$scope.isAutodetail = "";
						$scope.isMannualdetail ="Manual";
						}
					else
						{
						$scope.isAutodetail =  "";
						$scope.isMannualdetail = "Manual";
						}
					
					$scope.platFromdetail=response.data.serviceResponse.platform;
					$scope.projectIDdetail=response.data.serviceResponse.projectID;
					var d=new Date(response.data.serviceResponse.projectStartDate);
					var t=("00" + (d.getMonth() + 1)).slice(-2)+"/"+ ("00" + d.getDate()).slice(-2)+"/"+d.getFullYear();
					$scope.pStdAtedetail=t;
					var d1=new Date(response.data.serviceResponse.projectEndDate);
					var t1=("00" + (d1.getMonth() + 1)).slice(-2)+"/"+ ("00" + d1.getDate()).slice(-2)+"/"+d1.getFullYear();
					$scope.pEndAtedetail=t1;
					$scope.isActivedetail=response.data.serviceResponse.isActive;
					$scope.bugTrackingdetail=response.data.serviceResponse.isJiraEnable;
					$scope.projectIDdetail=response.data.serviceResponse.projectID;
					var d=new Date(response.data.serviceResponse.projectStartDate);
					var t=("00" + (d.getMonth() + 1)).slice(-2)+"/"+ ("00" + d.getDate()).slice(-2)+"/"+d.getFullYear();
					$scope.pStdAtedetail=t;
				});
				
				$(".se-pre-con").hide();
			}
			
			
			$scope.showModalForEditProject = function() {
				$('.datepicker').datepicker({
					autoclose : true,
					format : 'mm/dd/yyyy',
					  orientation:'bottom',
						yearRange: "2000:2050",
					todayHighlight : true
				});/*projectNamedetail*/
				dashboardService.getproject($scope.projectName,$scope.projectid).then(function Success(response) {
					console.log(response);
					$scope.projectNameEdit=response.data.serviceResponse.projectName;
					$scope.EditProjectObj.oldprojectname=response.data.serviceResponse.projectName
					$scope.applicationNameEdit=response.data.serviceResponse.applicationName;
					/*$scope.isAuto=response.data.serviceResponse.isAutomation;
					$scope.isMannual=response.data.serviceResponse.isMannual;*/
					console.log(response.data.serviceResponse.isMannual);
					if(response.data.serviceResponse.isAutomation=='true' && response.data.serviceResponse.isMannual=='true')
					{
						$scope.isAutoEdit = true;
						$scope.isMannualEdit = true;
						}
					else if(response.data.serviceResponse.isAutomation=='true')
						{
						$scope.isAutoEdit = true;
						$scope.isMannualEdit = false;
						}
					else if(response.data.serviceResponse.isMannual==='true')
							
						{
						$scope.isAutoEdit = false;
						$scope.isMannualEdit = true;
						}
					else
						{
						$scope.isAutoEdit = false;
						$scope.isMannualEdit = true;
						}
					
					$scope.platFromEdit=response.data.serviceResponse.platform;
					$scope.projectIDEdit=response.data.serviceResponse.projectID;
					var d=new Date(response.data.serviceResponse.projectStartDate);
					var t=("00" + (d.getMonth() + 1)).slice(-2)+"/"+ ("00" + d.getDate()).slice(-2)+"/"+d.getFullYear();
					$scope.pStdAteEdit=t;
					var d1=new Date(response.data.serviceResponse.projectEndDate);
					var t1=("00" + (d1.getMonth() + 1)).slice(-2)+"/"+ ("00" + d1.getDate()).slice(-2)+"/"+d1.getFullYear();
					$scope.pEndAteEdit=t1;
					$scope.isActiveEdit=response.data.serviceResponse.isActive;
					$scope.bugTrackingEdit=response.data.serviceResponse.isJiraEnable;
				});
				$(".se-pre-con").hide();
				$("#EditProject").modal();
				
			}
			$scope.showBugToolActive = function() {
				dashboardService.showBugToolActive().then(
						function Success(response) {
							//$(".se-pre-con").hide();
							//console.log(response);
							$scope.showActiveBugTracker=response.data.serviceResponse;
						});
			}
			$scope.EditProjectObj = {
					projectID:null,
					createdBy : null,
					createdTime : null,
					isActive : null,
					isJiraEnable : null,
					projectLocation : null,
					projectName : null,
					senarioNumber : null,
					testCaseNumber : null,
					updatedTime : null,
					module : null,
					isAutomation : null,
					isMannual : null,
					submodule : null,
					platform : null,
					subplatform : null,
					version : null,
					updatedBy : null,
					projectManager : null,
					ProjectStartDate : null,
					ProjectEndDate : null,
					applicationName : null,
					oldprojectname: null

				}
			$scope.EditProject = function() {
				
				var startdate1=$("[name='pStdAteEdit']").val();
				 var enddate1=$("[name='pEndAteEdit']").val();
				  if(new Date(enddate1)<new Date(startdate1))
				   {
					   alert("Please select 'End Date' greater than 'Start Date' !!");
					   e.stopImmediatePropagation();
					   return false;  
				   }
				   
				$scope.EditProjectObj.projectID =$scope.projectid;
				$scope.EditProjectObj.projectName = $scope.projectNameEdit;
				$scope.EditProjectObj.applicationName = $scope.applicationNameEdit;
				$scope.EditProjectObj.isAutomation = $scope.isAutoEdit;
				$scope.EditProjectObj.isMannual = $scope.isMannualEdit;
				$scope.EditProjectObj.platform = $scope.platFromEdit;
								
				$scope.EditProjectObj.projectStartDate = $scope.pStdAteEdit;
				$scope.EditProjectObj.projectEndDate = $scope.pEndAteEdit;
				$scope.EditProjectObj.isActive = $scope.isActiveEdit;
				$scope.EditProjectObj.isJiraEnable = $scope.bugTrackingEdit;
				if($scope.projectid !=null && $scope.projectNameEdit !=null && $scope.applicationNameEdit !=null &&($scope.isAutoEdit !=false || $scope.isMannualEdit !=false)
						&& $scope.pStdAteEdit !=null && $scope.pEndAteEdit !=null && $scope.isActiveEdit !=null && $scope.bugTrackingEdit !=null)
					{
				
				var txt=confirm("Click ok if you want to edit");
				if(txt== null || txt == "")
					{
					
					}
				else
					{		
					if($scope.EditProjectObj.projectStartDate > $scope.EditProjectObj.projectEndDate)
						{
						alert("Start date must be less than End Date");
						}else{
					dashboardService.EditProject($scope.EditProjectObj).then(
						function Success(response) {
							
							$scope.status=response.data.serviceResponse;
							console.log($scope.status);
							//$('.row input').val(" ");
							$scope.reloadprojectoverview();
							$("#EditProject").modal('hide');
							 $(".se-pre-con").hide(); 
							if($scope.status==='Success')
								{
								localStorage.removeItem('projectName');
								localStorage.setItem('projectName',$scope.EditProjectObj.projectName);
								localStorage.setItem('projectNames',$scope.EditProjectObj.projectName);
								
								alert("Project edited successfully");
								$scope.refereshprojectName();
								
								}
								//$scope.reloadprojectoverview();
							});
						}
					}
					}
				else{
					$("#EditProject").modal();
					alert("Kindly fill all the required fields");
					}
				 	
			
			
		}
		$scope.reloadprojectoverview=function()
		{
			$scope.getProjectDetailssingle();
			$(".se-pre-con").hide();
		}

		});//End Of File

