	demoApp.controller("configResoucesController", function($scope,$interval,$filter,$state,$stateParams,$location,$anchorScroll,$rootScope,configResoucesServices,reportService) {
		$(document).ready(function(){
			checkcookie();
			var token=localStorage.getItem('tokenID');
			if (token==null|| token=="")
			{
			window.location.href = "../login.html";
			}
			});
		var d=new Date();
		var month=d.getMonth()+1;
		var year=d.getFullYear(); 
		var date=d.getDate();
		var countOfColumnListNew1;

		if(month<10)
			{
			frm="0"+month+"/"+date+"/"+year;
			//frm=year+"/0"+month+"/"+date;
			}
		else
			{
			frm=month+"/"+date+"/"+year;
			//frm=year+"/"+month+"/"+date;
			}
		
		 var d1 = new Date();
		 d1.setDate(d1.getDate()-5);
		 var month1=d1.getMonth()+1;
			var year1=d1.getFullYear(); 
			var date1=d1.getDate();
			if(month<10)
			{
			frm1="0"+month1+"/"+date1+"/"+year1;
			//frm1=year1+"/0"+month1+"/"+date1;
			}
		else
			{
			frm1=month1+"/"+date1+"/"+year1;
			//frm1=year1+"/"+month1+"/"+date1;
			}
		$scope.toDate=frm;
		$scope.fromDate=frm1;
		$rootScope.checkHome();
		$scope.projectName=localStorage.getItem('projectName');
		$scope.projectId = localStorage.getItem('projectId');
		$scope.uId= localStorage.getItem('userid');
		$scope.user_role= localStorage.getItem('userRole');
		$scope.roletype="";
		$scope.buttonvisible=false;
		$scope.documentstatus="";
		$scope.isPublic="N";
		$("#iconList .nav-link").each(function(){
			$(this).removeClass("active");
		});
		$("#resource a.nav-link").addClass("active");
		$scope.projectwiserole=localStorage.getItem('projectwiserole');
		 if($scope.user_role!=undefined)
			 {
			 if($scope.user_role==="100")
				{
				$("#testRun").css("display","unset");
				$("#todo").css("display","unset");
				$("#testSuites").css("display","unset");
				$("#Automation").css("display","unset");
				}
			if($scope.projectwiserole=="Manager" || $scope.projectwiserole==="Tester")
			{
				$("#todo").css("display","unset");
				$("#testSuites").css("display","unset");
				$("#Automation").css("display","unset");
				if($scope.projectwiserole==="Manager")
				{
					$("#testRun").css("display","unset");
			
				}
				else if($scope.projectwiserole==="Tester")
				{
			
					if($scope.url=='/testRun')
					{
						$state.transitionTo('error');
					}
				
				}
				else if($scope.projectwiserole==="Readonly")
				{
			
				}

			}
			 }
			
		else
		{
			$state.transitionTo('error');
		}
		$scope.showMul1=function(){
			$(".test").select2({
				closeOnSelect : false,
				placeholder : "Project Names",
				allowHtml: true,
				allowClear: true,
				tags: true,
				
			});
			$(".test1").select2({
				closeOnSelect : false,
				placeholder : "Tester Names",
				allowHtml: true,
				allowClear: true,
				tags: true,
				
			});		  
				
			};
			
			$scope.getAllRunName = function() {
				/*reportService
						.getAllRunName($scope.projectId)
						.then(
								function Success(response) {
									$(".se-pre-con").hide();
									
									$scope.allProjectList = response.data.serviceResponse;
									//$scope.projectCount=$scope.allProjectList.length;
									countOfColumnListNew1=$scope.allProjectList.length
									$scope.getUserByProjectId();
									
								});*/
				//alert("Get project name")
				if($scope.user_role=='100'){
				configResoucesServices
				.getAllProjectName()
				.then(
						function Success(response) {
							
							$(".se-pre-con").hide();
							
							$scope.allProjectList = response.data.serviceResponse;
							//$scope.projectCount=$scope.allProjectList.length;
							countOfColumnListNew1=$scope.allProjectList.length
							$scope.getUserByProjectId();
							
						});
				}
				else
					{
					configResoucesServices.getAllProjectNameForMAnager($scope.uId).then(
							function Success(response) {
								
								$(".se-pre-con").hide();
								
								$scope.allProjectList = response.data.serviceResponse;
								//$scope.projectCount=$scope.allProjectList.length;
								countOfColumnListNew1=$scope.allProjectList.length
								$scope.getUserByProjectId();
							});
					}
			}
			$('.datepicker').datepicker({
				autoclose : true,
				format : 'yyyy/mm/dd',
				 orientation:'bottom',
				todayHighlight : true
			});	
			$scope.getUserByProjectId = function() {
				
				if($scope.user_role=='100'){
				configResoucesServices
				.getUserListOfResouce()
				.then(
						function Success(response) {
							$(".se-pre-con").hide();
							$scope.userList = response.data.serviceResponse;
							//$scope.testerCount=$scope.userList.length;
							$scope.allUserList=$scope.userList;
							$scope.getResourcesGraph1();
							
						});
				}
				else{
					configResoucesServices
					.getMangetCreatedUserList($scope.uId)
					.then(
							function Success(response) {
								$(".se-pre-con").hide();
								$scope.userList = response.data.serviceResponse;
								//$scope.testerCount=$scope.userList.length;
								$scope.allUserList=$scope.userList;
								$scope.getResourcesGraph1();
								
							});
				}
			}
			var resourseReqObj={};
			$scope.completeRun=[];
			$scope.freshRun=[];
			$scope.reRun=[];
			$scope.testerName=[];
		
			var myChart;
			var chartCount=0;
			$scope.getResourcesGraph1 = function() {
				
				var projectList=$('#projectListList').val();
				console.log(projectList);
				var testerList=$('#testerList').val();
				console.log(testerList);
				chartCount++;
				resourseReqObj={};
				var projectList=$('#projectListList').val();
				console.log(projectList);
				var testerList=$('#testerList').val();
				console.log(testerList);
				var fromDate=$scope.fromDate;
				var toDate=	$scope.toDate;
				if(projectList.length==0)
				{
					for (var r = 0; r < $scope.allProjectList.length; r++) 
					{
						projectList.push($scope.allProjectList[r].projectID);
					}
					/*$(".projectList > option").prop('selected','selected').trigger('change');*/
					//$(".projectList").html("<li>"+count+" item selected</li>");
					console.log(projectList)
				}
				if(testerList.length==0)
				{
					for (var u = 0; u < $scope.userList.length; u++) 
					{
						testerList.push($scope.userList[u].id);
					}
					/*$(".testerList2 > option").prop('selected','selected').trigger('change');*/
				}

				$scope.projectCount=projectList.length;
				$scope.testerCount=testerList.length;
							
				resourseReqObj.projectList=projectList;
				resourseReqObj.testerList=testerList;
				fromDate=fromDate.toString().replace("/","-");
				fromDate=fromDate.toString().replace("/","-");
				var sfrom=fromDate.split("-");
				 var sf=sfrom[2]+ '-' +sfrom[0]+ '-' +sfrom[1];
				 resourseReqObj.fromDate=sf;
				 toDate=toDate.toString().replace("/","-");
				toDate=toDate.toString().replace("/","-");
				var tidatefrst=toDate.split("-")
				var todates=tidatefrst[2]+ '-' +tidatefrst[0]+ '-' +tidatefrst[1];
				resourseReqObj.toDate=todates;

				resourseReqObj.projectid=$scope.projectId;
//				$scope.Object1={
//					a:12,
//					b:13,
//					c:'abc'
//				};
//				Object1.a=20;
				
				$scope.statusCount=0;
				$scope.passCount=0;
				$scope.failCount=0;
				$scope.totalExeCount=0;
				/*configResoucesServices.getResourseEffortstake(resourseReqObj).then(function Success(response) 
				{*/
					configResoucesServices.getResorcesEffort(resourseReqObj).then(function Success(response) 
					{
					$(".se-pre-con").hide();
					console.log(response.data.serviceResponse);
					$scope.freshRunData=response.data.serviceResponse;
					console.log($scope.freshRunData);
					var tempArrayUserName=[];
					var tempTotalCount=[];
					let tempTotalCountPrep=[];
					var tempNotStarted=[];
					var tempCompteted=[];
					var tempNA=[];
					var tempUnderReview=[];
					var tempOnHold=[];
				
					
					if(!($scope.freshRunData=='No Data Available'))
					{
						$.each($scope.freshRunData, function(k, v) {
							$.each(v, function(key, val) {
								tempArrayUserName.push(val.userName);
								tempTotalCount.push(val.totalCount);
								tempTotalCountPrep.push(val.totalCountPrepared);
								tempCompteted.push(val.completed);
								tempNA.push(val.na);
								tempNotStarted.push(val.notStarted);
								tempOnHold.push(val.onhold);
								tempUnderReview.push(val.underReview);
								
							});
						});
				    	var ctx = document.getElementById("runChart");
						var barOptions_stacked = {
								tooltips: {
									//enabled:false,
							            displayColors:true,
							            backgroundColor: '#227799'
							             },
							    hover :{
							        animationDuration:0
							    },
							    scales: {
							       
							        xAxes: [{
							              barThickness:10,
							            ticks: {
							                beginAtZero:true,
							                fontFamily: "'Open Sans Bold', sans-serif",
							                fontSize:11
							            },
							            scaleLabel:{
							                display:false
							            },
							            gridLines: {
							            }, 
							            stacked: true
							        }],
							        yAxes: [{
							          
							            gridLines: {
							                display:false,
							                color: "#fff",
							                zeroLineColor: "#fff",
							                zeroLineWidth: 0
							            },
							            ticks: {
							                fontFamily: "'Open Sans Bold', sans-serif",
							                fontSize:11
							            },
							            barPercentage: 0.5,
							            stacked: true
							        }]
							    },
							    legend:{	
							      display:true,
							      position:'bottom'
							    },
							    pointLabelFontFamily : "Quadon Extra Bold",
							    scaleFontFamily : "Quadon Extra Bold",
							    
							};
						 if(myChart!=null){
							 myChart.destroy();
						    }
						
							myChart = new Chart(ctx, {
							    type: 'bar',
							    data: {
							    	 labels: tempArrayUserName,
							        datasets: [{
							            label:'Test Case Prepared Count',
							            data: tempTotalCountPrep,
							            backgroundColor: "Green",
							            hoverBackgroundColor: "#036903",
							        },{
							            label:'Not Started',
							            data: tempNotStarted,
							            backgroundColor: "rgba(140,85,100,1)",
							            //hoverBackgroundColor: "rgba(140,85,100,1)"
							        },{
							            label:'On Hold',
							            data: tempOnHold,
							            backgroundColor: "#ffbf00",
							            //hoverBackgroundColor: "rgba(46,185,235,1)"
							        },
							        {
							            label:'Under Review',
							            data: tempUnderReview,
							            backgroundColor: "#0000FF",
							            //hoverBackgroundColor: "#0000FF",
							        },{
							            label:'N/A',
							            data: tempNA,
							            backgroundColor: "#FF0000",
							            //hoverBackgroundColor: "rgba(140,85,100,1)"
							        },{
							            label:'Completed',
							            data: tempCompteted,
							            backgroundColor: "#FF1493",
							            //hoverBackgroundColor: "rgba(46,185,235,1)"
							        }]
							    },

							    options: barOptions_stacked,
							});
					}	
					else
						{
						 myChart.destroy();
						}
				});	
				
				$scope.selectMultipleProject=function(){

							if ($scope.selMulProject == true) {
								$('.test').val($scope.allProjectList).trigger(
										'change');
								$scope.userList=$scope.allUserList;
							} else {

								$(".test > option")
										.prop('selected', 'selected').trigger(
												'change');
							}
				}
				
				$(".test").on("select2:select", function (evt) {

							var element = evt.params.data.element;
							var $element = $(element);
							$element.detach();
							$(this).append($element);
							$(this).trigger("change");
							let pIDs=[];
							let projectData=$(this).select2('data');
							var count = $(this).select2('data').length;

							if (count == countOfColumnListNew1) {

								$('#selectCheckBoxRun').prop('checked', true);
								$scope.userList=$scope.allUserList;
							} else {

								$('#selectCheckBoxRun').prop('checked', false);
								for(let one of projectData){
									pIDs.push(one.id);
								}
								$scope.getUserListOfResouceByProjectIDs(pIDs);
							}
							
							

				});
				
				$scope.getUserListOfResouceByProjectIDs=function(pIDs){
					configResoucesServices.getUserListOfResouceByProjectIDs(pIDs, $scope.user_role, $scope.uId).then(function Success(response) 
					{
						$(".se-pre-con").hide();
						$scope.userList=response.data.serviceResponse;
					});
				}
				
				$(".test").on("select2:unselect", function (evt) {
					$scope.selMulProject=false;
					$("#selectCheckBoxRun").prop('checked', false);
					let pIDs=[];
					let projectData=$(this).select2('data');
					if((projectData.length==0)||(projectData.length==countOfColumnListNew1)){
						$scope.userList=$scope.allUserList
						
					}
					else{
						for(let one of projectData){
							pIDs.push(one.id);
						}
						$scope.getUserListOfResouceByProjectIDs(pIDs);
					}
						
					
				});

				$scope.selectMultipleUser=function(){

							if ($scope.selMulUser == true) {
								$('.test1').val($scope.allProjectList).trigger(
										'change');

							} else {

								$(".test1 > option").prop('selected',
										'selected').trigger('change');
							}
				}
				
				$(".test1").on("select2:select", function (evt) {

					var element = evt.params.data.element;
					var $element = $(element);
					$element.detach();
					$(this).append($element);
					$(this).trigger("change");
					var count = $(this).select2('data').length

					if(count==countOfColumnListNew1){


					$('#selectCheckBoxUser').prop('checked', true);
					}else{

					$('#selectCheckBoxUser').prop('checked', false);
					}

					});
					$(".test1").on("select2:unselect", function (evt) {
					$scope.selMulUser=false;
					$("#selectCheckBoxUser").prop('checked', false);


					}); 

				
			/*	configResoucesServices.getResourseEffortsFreshRun(resourseReqObj).then(function Success(response) 
				{
					$(".se-pre-con").hide();
					$scope.freshRunData=response.data.serviceResponse;
					if(!($scope.freshRunData=='No Data Available'))
					{
						$.each($scope.freshRunData, function(k, v) {
							$.each(v, function(key, val) {
								$scope.freshRun.push(val.exeCount);
								//alert($scope.freshRun);
							});
							
						});
						for (var p = 0; p < $scope.completeRun.length; p++) 
						{
							$scope.count=$scope.completeRun[p]-$scope.freshRun[p];
							$scope.reRun.push($scope.count);
							//alert($scope.reRun);
						}
						var barOptions_stacked = {
								tooltips: {
									//enabled:false,
							            displayColors:true,
							            //backgroundColor: '#227799'
							             },
							    hover :{
							        animationDuration:0
							    },
							    scales: {
							       
							        xAxes: [{
							              barThickness:20,
							            ticks: {
							                beginAtZero:true,
							                fontFamily: "'Open Sans Bold', sans-serif",
							                fontSize:11
							            },
							            scaleLabel:{
							                display:false
							            },
							            gridLines: {
							            }, 
							            stacked: true
							        }],
							        yAxes: [{
							          
							            gridLines: {
							                display:false,
							                color: "#fff",
							                zeroLineColor: "#fff",
							                zeroLineWidth: 0
							            },
							            ticks: {
							                fontFamily: "'Open Sans Bold', sans-serif",
							                fontSize:11
							            },
							            barPercentage: 0.5,
							            stacked: true
							        }]
							    },
							    legend:{	
							      display:true,
							      position:'bottom'
							    },
							    pointLabelFontFamily : "Quadon Extra Bold",
							    scaleFontFamily : "Quadon Extra Bold",
							    
							};
							if(chartCount>1)
								myChart.destroy();
							myChart = new Chart(ctx, {
							    type: 'bar',
							    data: {
							        labels: $scope.testerName,
							        
							        datasets: [{
							            label:'Execution Count',
							            data: $scope.completeRun,
							            backgroundColor: "Green",
							            hoverBackgroundColor: "#036903"
							        },{
							            label:'Fresh Run',
							            data: $scope.freshRun,
							            backgroundColor: "rgba(140,85,100,1)",
							            //hoverBackgroundColor: "rgba(140,85,100,1)"
							        },{
							            label:'ReRun',
							            data: $scope.reRun,
							            backgroundColor: "#ffbf00",
							            //hoverBackgroundColor: "rgba(46,185,235,1)"
							        }]
							    },

							    options: barOptions_stacked,
							});
					}	
				});	*/
				configResoucesServices
				.getDatewiseUserForResouces(resourseReqObj)
				.then(
						function Success(response) {
							$(".se-pre-con").hide();
							var c = [];
							var i = 0;
							console.log(response.data.serviceResponse);
							var graphObject = response.data.serviceResponse;
							console.log("              "+graphObject);
							
							var ctx = document.getElementById("runChart");
							
							if(graphObject=='No Data Available')
							{
								//myChart.destroy();
								Highcharts.setOptions({
									
									time : {
										timezoneOffset : (new Date())
												.getTimezoneOffset()
									// setting IST timezone
									},
								lang: {noData: "No Data Available"}
								});
								Highcharts.chart('ResourceChart1', {

									chart : {
										 height: 250,
										zoomType : 'x',
										 resetZoomButton: {
									            position: {
									               
									                x: 	0,
									                y: -30
									            }
									        },
										events: {
							                load: function () {
							                	//toggleChartPages();
							                }
										}
									},
									credits : {
										enabled : false
									},
									lang: {noData: "No Data Available"},
									title : {
										text : '<div style="font-weight: bolder;font-size: 0px;fill:black;">'+ +'</div>'
									},

									subtitle : {
										text : 'Resource Efforts Graph'
									},
									  legend: {
										  
										  align: 'right',
									        verticalAlign: 'top',
									        layout: 'vertical',
									        x: 0,
									        y: 20,	
									        itemStyle: {
									            fontSize:'10px',
									            fontWeight: 'bold',
									           
									        }
									    },

									tooltip : {
										valueDecimals : 2
									},
									yAxis :{
										title:{
											text: 'Execution Count'
										}
									},
									xAxis : {
										title:{
											text: 'Date'
										},
										type: 'date',
										labels: {
										    format: '{value:%Y-%m-%e}'
										  },
									},

									plotOptions : {
										line : {
											marker : {
												enabled : false
											}
										}
									},

									
								});
							}
							else
							{console.log(graphObject);
								
							$.each(graphObject, function(k, v) {
								var series = {};
								var data = [];
								var j = 0;
								$.each(v, function(key, val) {
									console.log(val.excutionDate);
									var tab_date=val.excutionDate.split("-");
									data[j] = [Date.UTC(parseInt(tab_date[0]),parseInt(tab_date[1])-1,parseInt(tab_date[2])),
											val.completed ];
									
									$scope.statusCount=parseInt($scope.statusCount)+parseInt(val.completed);
									$scope.totalExeCount=$scope.totalExeCount+parseFloat(val.completed);
									
									var timestamp=new Date(val.excutionDate).getTime();
								    var todate=new Date(timestamp).getDate();
								    var tomonth=new Date(timestamp).getMonth()+1;
								    var toyear=new Date(timestamp).getFullYear();
								    var original_date=tomonth+'/'+todate+'/'+toyear;
								    
								    
								    
									data[j] = [timestamp,
											parseFloat(val.completed) ];
									j++;
								});
								
								$scope.testerName.push(k);
								$scope.completeRun.push($scope.totalExeCount);
								//alert($scope.completeRun);
								
								// Highchart needs sorted data
								data.sort(function(a, b) {
									return a[0] - b[0];
									
								});					
								series.data = data;
								series.name = k;
								series.lineWidth = 0.5;

								c[i] = series;
								i++;
								series = null;
								
							});
							
							// drawLineChart(c);
							
							Highcharts.setOptions({
								
								time : {
									timezoneOffset : (new Date())
											.getTimezoneOffset()
								// setting IST timezone
								},
								/*scales: {
						            xAxes: [{
						                barThickness: 6,  // number (pixels) or 'flex'
						                maxBarThickness: 8 // number (pixels)
						            }]
						        },*/
							lang: {noData: "No Data Available"}
							});

							Highcharts.chart('ResourceChart1', {

								chart : {
									 height: 250,
									zoomType : 'x',
									 resetZoomButton: {
								            position: {
								               
								                x: 	0,
								                y: -30
								            }
								        },
									events: {
						                load: function () {
						                	//toggleChartPages();
						                }
									}
								},
								credits : {
									enabled : false
								},
								title : {
									text : '<div style="font-weight: bolder;font-size: 04px;fill:black;">'+ +'</div>'
								},

								subtitle : {
									text : 'Resource Efforts Graph'
								},
								  legend: {
									  
									  align: 'right',
								        verticalAlign: 'top',
								        layout: 'vertical',
								        x: 0,
								        y: 20,	
								        itemStyle: {
								            fontSize:'10px',
								            fontWeight: 'bold',
								           
								        }
								    },

								tooltip : {
									valueDecimals : 2
								},
								yAxis :{
									title:{
										text: 'Execution Count'
									}
								},
								xAxis : {
									title:{
										text: 'Date'
									},
									type: 'datetime',
									labels: {
									    format: '{value:%Y-%m-%e}'
									  },
								},

								plotOptions : {
									line : {
										marker : {
											enabled : false
										}
									}
								},

								series : c
							});					
						}
				
					
					
				});
				if($scope.user_role=='100'){
				configResoucesServices.tableResourceEffortsAdmin(resourseReqObj).then(function Success(response) 
				{
					$(".se-pre-con").hide();
					$scope.tableData=response.data.serviceResponse;
					if($scope.tableData=="No Data Available"){
						$scope.tableData=[];
					}
					for (var p = 0; p < $scope.tableData.length; p++) 
					{
						//alert($scope.tableData[p].exePass,$scope.tableData[p].exeFail)
						$scope.passCount=$scope.passCount+parseInt($scope.tableData[p][2]);
						$scope.failCount=$scope.failCount+parseInt($scope.tableData[p][3]);
					}
					
					console.log($scope.tableData);
					var table = $('#configResTable').DataTable();
					table.destroy();
					$("#configResTable").DataTable({
						data:$scope.tableData,
						/*  dom: 'Bfrtip',
					        select: true,*/
						dom: 'Bfrtip',
						select: true,
						buttons: [
							{
								extend: 'collection',
								text: 'Export',
								buttons: [
									{
										extend: "excelHtml5",
										//fileName:  "CustomFileName" + ".xlsx",
									//	title: 'Data export',
										exportOptions: {
											columns: ':visible'
										},
										//CharSet: "utf8",
										exportData: { decodeEntities: true }
									},
									{
										extend: "csvHtml5",
										exportOptions: {
											columns: ':visible'
										},
										exportData: {decodeEntities:true}
									},
									{
										extend: "pdfHtml5",
										exportOptions: {
											columns: ':visible'
										},
										exportData: {decodeEntities:true}
									}
									]
							}
							],


					});
					/** Graph Data Start */
				});
				}
				else{
					//configResoucesServices.tableResourceEffortsForManager(resourseReqObj).then(function Success(response) 
					configResoucesServices.tableResourceEffortsAdmin(resourseReqObj).then(function Success(response)
					{
						$(".se-pre-con").hide();
						$scope.tableData=response.data.serviceResponse;
						if($scope.tableData=="No Data Available"){
							$scope.tableData=[];
						}
						for (var p = 0; p < $scope.tableData.length; p++) 
						{
							//alert($scope.tableData[p].exePass,$scope.tableData[p].exeFail)
							$scope.passCount=$scope.passCount+parseInt($scope.tableData[p][2]);
							$scope.failCount=$scope.failCount+parseInt($scope.tableData[p][3]);
						}
						
						console.log($scope.projetentitle);
						var table = $('#configResTable').DataTable();
						table.destroy();
						$("#configResTable").DataTable({
							data:$scope.tableData,
							/*  dom: 'Bfrtip',
						        select: true,*/
							dom: 'Bfrtip',
							select: true,
							buttons: [
								{
									extend: 'collection',
									text: 'Export',
									buttons: [
										{
											extend: "excelHtml5",
											//fileName:  "CustomFileName" + ".xlsx",
										//	title: 'Data export',
											exportOptions: {
												columns: ':visible'
											},
											//CharSet: "utf8",
											exportData: { decodeEntities: true }
										},
										{
											extend: "csvHtml5",
											exportOptions: {
												columns: ':visible'
											},
											exportData: {decodeEntities:true}
										},
										{
											extend: "pdfHtml5",
											exportOptions: {
												columns: ':visible'
											},
											exportData: {decodeEntities:true}
										}
										]
								}
								],


						});
						/** Graph Data Start */
					});
					
				}
				

				

					
				
				
			
				
			}
			$scope.getResourcesGraph = function() {
				new Chart(document.getElementById("ResouseChart"), {
				  type: 'line',
				  data: {
				    
				    datasets: [{ 
				        data: [{
				        	 x: "Jan",
		                    y: 90
		                }, {
		                	 x: "Feb",
		                    y: 96
		                }, {
		                	 x: "Mar",
		                    y: 97
		                }],
				        label: "Tester1",
				        borderColor: "#3e95cd",
				        fill: false
				      }, { 
				        data: [
				        	{
				        		 x: "Jan",
			                    y: 190
			                }, {
			                	 x: "Feb",
			                    y: 196
			                }, {
			                	 x: "Mar",
			                    y: 197
			                }
				        ],
				        label: "Tester2",
				        borderColor: "#8e5ea2",
				        fill: false
				      }, { 
				        data: [{
		                    x: "Jan",
		                    y: 290
		                }, {
		                    x: "Feb",
		                    y: 196
		                }, {
		                    x: "Mar",
		                    y: 297
		                }],
				        label: "Tester3",
				        borderColor: "#3cba9f",
				        fill: false
				      }
				    ]
				  },
				  options: {
				    title: {
				      display: true,
				      text: 'Execuition Status'
				    }
				  }
				});
			}
			$scope.showMul1();
			$scope.getAllRunName();
			//$scope.getUserByProjectId();
			//$scope.getResourcesGraph();
			
		

		
		
	});

	