demoApp.controller("autoReportController", function($scope,$interval,$state,$stateParams,$location,$anchorScroll, $rootScope, autoReportService) {
	
	$(document).ready(function(){
		checkcookie();
		var token=localStorage.getItem('tokenID');
		if (token==null|| token=="")
		{
		window.location.href = "../login.html";
		}
		});
	
	$rootScope.checkHome();
	
	$scope.projectName = localStorage.getItem('projectName');
	$scope.projectId = localStorage.getItem('projectId');
	var pName=$scope.projectName 
	
	$scope.getAutomationRunReport=function(){
		autoReportService.getAutomationRunReport($scope.projectName).then(function Success(response)
		{
			$(".se-pre-con").fadeOut('slow');
            $scope.automationReport=response.data.serviceResponse;
            $("#automationReport").DataTable({
            	"order": [[ 0, "desc" ]],
                 dom: 'lBfrtip',
                 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                 buttons: [
                     {
                     extend: 'collection',
                     text: 'Export',
                     buttons: [
                         {
                             extend: "excelHtml5",
                             fileName:  "CustomFileName" + ".xlsx",
                             title:pName,
                             exportOptions: {
                                 columns: ':visible'
                             },
                             //CharSet: "utf8",
                             exportData: { decodeEntities: true }
                         },
                         {
                             extend: "csvHtml5",
                             fileName:  "CustomFileName" + ".csv",
                             title:pName,
                             exportOptions: {
                                 columns: ':visible'
                             },
                             exportData: { decodeEntities: true }
                         },
                         {
                             extend: "pdfHtml5",
                             fileName:  "CustomFileName" + ".pdf",
                             title:pName,
                             exportOptions: {
                                 columns: ':visible'
                             },
                             exportData: {decodeEntities:true}
                         }
                         ]
                    }
                 ],
            data: $scope.automationReport,
             columns:[
             {title:"Run Id"},
             {title:"Test Suite"},
             {title:"Start Time"},
             {title:"End Time"},
             {title:"Total Time"},
             {title:"Run Status"},
             {title:"Report"},
             

             ],
             "columnDefs": [{ "targets": -1, "data": null, "defaultContent": "<button id='btnDetails'  class='btn btn-primary btn-sm' title='action'>View</button>"}]
         });
         var table=$("#automationReport").DataTable();
         $rootScope.autoReportData=[];
      	 $('#automationReport tbody').on('click', '[id*=btnDetails]', function ()
      	 {
      		 $scope.data = table.row($(this).parents('tr')).data();
      		 console.log($scope.data[1]);
      		 
      		 for(var i=0; i<$scope.automationReport.length; i++)
      		{
      			 if($scope.automationReport[i][0]==$scope.data[0])
      			{
      				 $rootScope.prevRunIDData=$scope.automationReport[i-1];
      				 break;
      			}
      		}
      		 
      		 $rootScope.autoReportData=$scope.data;
      		 $state.transitionTo('projectDetailsAuto.exereport');
      	 });
	       	 
		});
	}
	
	$scope.viewReport = function(){
		alert("Hello");
		  $scope.data = table.row($(this).parents('tr')).data();
		  console.log($scope.data+"===");
	}
	
	$scope.getAutomationRunReport();
	
	
});
