demoApp
.controller(
		"entitlementController",
		function($scope, $interval, $state, $stateParams,$location,$anchorScroll, $rootScope, dashboardService,
				entitlementService) {
			$(document).ready(function(){
				checkcookie();
				var token=localStorage.getItem('tokenID');
				if (token==null|| token=="")
				{
					window.location.href = "../login.html";
				}
			});
			$rootScope.checkHome();

			$scope.showAdminViewEntitlement;

			$scope.roleId = localStorage.getItem('userRole');
			$scope.user = localStorage.getItem('name');

			
			//$scope.testcase={};
			//console.log($scope.testcase.accessProject);
			$scope.getallprojecttest=function()
			{
				if($scope.user_role==='100'  || $scope.user_role==='101' )
				{
					
					entitlementService.getadminViewEntitlement($scope.id,$scope.user_role,$scope.projectwiserole,$scope.user).then(function Success(response){
						$scope.projectList=response.data.serviceResponse;
						console.log($scope.projectList);
					

						$(".se-pre-con").fadeOut('slow');
					});
				}
				/*dashboardService.getAllProject().then(function Success(response){
							$scope.allproject=response.data.serviceResponse;
							$(".se-pre-con").fadeOut('slow');
						});*/
				/*dashboardService.getAllProject().then(
								function Success(response) {
									$(".se-pre-con").hide();
									$scope.allproject=response.data.serviceResponse;

								});*/

			}

			$scope.changeEvent=function(valuee){
				console.log(valuee);
			}
			 
			$scope.projectName = localStorage.getItem('projectName');
			var pName=$scope.projectName
			$scope.projectId = localStorage.getItem('projectId');
			$scope.id= localStorage.getItem('userid');
			$scope.user_role= localStorage.getItem('userRole');
			$scope.projectwiserole=localStorage.getItem('projectwiserole');
			if($scope.user_role!=undefined)
			{
				if($scope.user_role==="100")
				{
					$("#testRun").css("display","unset");
					$("#todo").css("display","unset");
					$("#testSuites").css("display","unset");
					$("#Automation").css("display","unset");
				}
				if($scope.projectwiserole=="Manager" || $scope.projectwiserole==="Tester")
				{
					$("#todo").css("display","unset");
					$("#testSuites").css("display","unset");
					$("#Automation").css("display","unset");
					if($scope.projectwiserole==="Manager")
					{
						$("#testRun").css("display","unset");

					}
					else if($scope.projectwiserole==="Tester")
					{

						if($scope.url=='/testRun')
						{
							$state.transitionTo('error');
						}

					}
					else if($scope.projectwiserole==="Readonly")
					{

					}

				}
			}

			else
			{
				$state.transitionTo('error');
			}
			//alert("getShowProjectDetailsss"+$scope.projectId);
			$scope.showProjectWorker=function()
			{
				entitlementService.getShowProjectDetails($scope.projectId).then(function Success(response){
					$scope.getShowProject=response.data.serviceResponse;
					$(".se-pre-con").hide();
				});
			}
			$scope.toggleTable =  function(index) {
				// alert(index);
				$("#tableData"+index).slideToggle("slow");
			}

			$scope.adminViewEntitlement=function()
			{
				if($scope.user_role==='100'  || $scope.user_role==='101' )
				{

					entitlementService.getadminViewEntitlement($scope.id,$scope.user_role,$scope.projectwiserole,$scope.user).then(function Success(response){

						$scope.showAdminViewEntitlement=response.data.serviceResponse;
						console.log(response.data.serviceResponse);
						$(".se-pre-con").fadeOut('slow');
					});
				}

			}




			$scope.projectID=[];
			$scope.projectEntitlementData=[];
			$scope.projectEntitleDataset=function()
			{
				entitlementService.getShowProjectDetails($scope.projectId).then(function Success(response) {
					$scope.projetentitle=[];
					$scope.projectEntitlementData=response.data.serviceResponse;
					//$scope.projetentitle=$scope.projectEntitlementData;
					for(i=0;i<$scope.projectEntitlementData.length;i++)
					{
						$scope.projectID=$scope.projectEntitlementData[i][4];
						var d=new Date($scope.projectID);
						var t=("00" + (d.getMonth() + 1)).slice(-2)+"/"+ ("00" + d.getDate()).slice(-2)+"/"+d.getFullYear();
						if($scope.projectID !=null)
						{
							$scope.projectEntitlementData[i][4]=t;
						}

					}

					console.log($scope.projetentitle);
					$("#projectEntitlement").DataTable({
						data:$scope.projectEntitlementData,
						/*  dom: 'Bfrtip',
					        select: true,*/
						scrollY: '38vh',
				        scrollCollapse: true,
				        paging: false,
						dom: 'Bfrtip',
						select: true,
						
						buttons: [
							{
								extend: 'collection',
								text: 'Export',
								buttons: [
									{
										extend: "excelHtml5",
										fileName:  "CustomFileName" + ".xlsx",
										title: pName,
										exportOptions: {
											columns: ':visible'
										},
										//CharSet: "utf8",
										exportData: { decodeEntities: true }
									},
									{
										extend: "csvHtml5",
										fileName:  "CustomFileName" + ".csv",
										title: pName,
										exportOptions: {
											columns: ':visible'
										},
										exportData: {decodeEntities:true}
									},
									{
										extend: "pdfHtml5",
										fileName:  "CustomFileName" + ".pdf",
										title: pName,
										exportOptions: {
											columns: ':visible'
										},
										exportData: {decodeEntities:true}
									}
									]
							}
							],


					});
					/** Graph Data Start */
				});
				if($scope.roleId =='102' || $scope.roleId=='103')
				{
				$(".se-pre-con").hide();
				}
			}

			$scope.GiveAccessUserEntitlement=function(userDetaildata)
			{
				if(userDetaildata.userid==$scope.id)
				{
					alert("Self data cannot be changed");
				}
				else{
				if( userDetaildata.userrole==='100')
				{
					alert("Access can not be modified for Admin user");	
					return false;							
				}
				else{
					console.log(userDetaildata);
					entitlementService.GiveAccessUserEntitlement(userDetaildata.projectId,userDetaildata.userid,userDetaildata.accessProject,userDetaildata.projectaccessid,userDetaildata.userrole).then(function Success(response){
						$scope.showAdminViewEntitlement=response.data.serviceResponse;

						if(response.data.serviceStatus==='Success')
						{
							alert("Successfully Given Access to :"+$scope.showAdminViewEntitlement.username);
							
						}
						else
						{
							alert("Something went wrong with Access while giving access to: "+$scope.showAdminViewEntitlement.username);
						}
						$scope.searhProject(); //changes
						/*$scope.adminViewEntitlement();*/
						$(".se-pre-con").fadeOut('slow');
					});

				}
				}
			}
			var count=0;
			$scope.searhProject=function()
			{
				
				/*$scope.adminViewEntitlement();*/
				var test=$scope.projectname;
				var checkProjectId=false;
				$scope.showAdminViewEntitlement1=$scope.showAdminViewEntitlement;
				var myVar=null;
				myVar=	$scope.showAdminViewEntitlement1
				var getKeysArray = null;
				getKeysArray=Object.keys(myVar);
				var getValueArray = null;
				
				getValueArray=Object.values(myVar);
				if(test==="")
				{
					$scope.adminViewEntitlement();

				}
				else
				{
					if(count>0)
					{
						
						entitlementService.getadminViewEntitlement($scope.id,$scope.user_role,$scope.projectwiserole,$scope.user).then(function Success(response){
							$scope.showAdminViewEntitlement=response.data.serviceResponse;
							
							console.log(response.data.serviceResponse);
							
							var responseKey=Object.values(response.data.serviceResponse);
							console.log(responseKey);
							myVar=$scope.showAdminViewEntitlement;
							getKeysArray=Object.keys(myVar);
							

							for(var j=0;j<getKeysArray.length;j++)
							{

								if(getKeysArray[j]===test)
								{
									count=count+1;
									checkProjectId=true;
									

								}
								else
								{
									delete myVar[getKeysArray[j]];
								}

							}
							
							if(checkProjectId==false){
								
								$scope.projectname="";
								$scope.searhProject();
							}

							$scope.showAdminViewEntitlement=myVar;
							
							$(".se-pre-con").fadeOut('slow');
						});
						

					}

					else
					{


						for(var j=0;j<getKeysArray.length;j++)
						{

							if(getKeysArray[j]===test)
							{
								count=count+1;
								

							}
							else
							{
								delete myVar[getKeysArray[j]];
							}

						}

						$scope.showAdminViewEntitlement=myVar;
						console.log($scope.showAdminViewEntitlement);
						
					}

				}
				console.log(myVar);
				
				/*$scope.adminViewEntitlement2();*/
				$scope.getallprojecttest();

			}

			$scope.deleteAccessUserEntitlement=function(userDetaildata)
			{

				if(userDetaildata.userid==$scope.id)
					{
						alert("Self data cannot be deleted");
					}
				else{
				if( userDetaildata.userrole==='100')
				{
					alert("This is an admin user ,access for this user can not be modified");	
					return false;							
				}
				else{
					var txt=confirm("Click ok if you want to delete");

					if(txt== null || txt == "")
					{

					}
					else
					{

						entitlementService.deleteGiveAccessUserEntitlement(userDetaildata.projectId,userDetaildata.userid,userDetaildata.accessProject,userDetaildata.userrole).then(function Success(response)
						{
							console.log(response.data.serviceResponse);
							if(response.data.serviceStatus==='Success')
							{
								alert("Successfully deleted access for user :"+response.data.serviceResponse.username);
								
							}
							else
							{
								alert("Something went wrong while deleting access for user :"+response.data.serviceResponse.username);
							}
							$scope.searhProject(); //changes
							
							$(".se-pre-con").hide();
						});
					}
				}
				}
			}

			$scope.adminViewEntitlement();
			$scope.getallprojecttest();

		});