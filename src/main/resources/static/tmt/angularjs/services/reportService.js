var AppURL="/TMT"; 

demoApp.factory('reportService', function($rootScope, $http) {
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
	return {
		getAllRunName : function(projectId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getAllRun',
				params : {
					"projectID" : projectId,
				},
			})
		},
		getReRunName : function(runid) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getrerunnameByrun',
				params : {
					"runid" : runid,
				},
			})
		},
		
		getRunReport : function(runId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getDetailsReportByRun',
				params : {
					"runId" : runId,
				},
			})
		},
	}
});//End Of File