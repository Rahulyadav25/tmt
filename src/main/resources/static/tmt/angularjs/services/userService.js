var AppURL="/TMT"; 

demoApp.factory('userService', function($rootScope, $http) {
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
return { 	

	getADUser : function(userId) {

		return $http({
			method : 'POST',
			url : AppURL + '/api/getADUser',
			params : {
				"userId" : userId
				
			}

		})
	},
	updateSession : function(){
		
	return $http({
				method : 'GET',
				url :AppURL + '/api/updateSession',
			})
	},

	getAccountDetails  : function(usename,userrole,managerId) {

		return $http({
			method : 'POST',
			url : AppURL + '/api/getAccountDetails',
			params : {
				"usename" : usename,
				"userrole" : userrole,
				"managerId" : managerId,
				
			}

		})
	},
	
	getMyEntitlementFnct: function(userId,role) {

		return $http({
			method : 'POST',
			url : AppURL + '/api/getMyEntitlementFnct',
			params : {
				"userId" : userId,
				"role":role,
				
			}

		})
	},
	
	sessionKills:function()
	    {
	        return $http({
	            method:'POST',
	            url :AppURL + '/api/sessionKills',
	            
	        })
	    },
	
	saveUser : function(userObject) {

		return $http({
			method : 'POST',
			url : AppURL + '/api/saveUser',
			data:userObject

		})
	},
	updateUser : function(userId, userName, userRole) {

		return $http({
			method : 'POST',
			url : AppURL + '/api/updateUser',
			params : {
				"userId" : userId,
				"userName" : userName,
				"userRole" : userRole

			}

		})
	},

	showAllUserDetails : function() {

		return $http({
			method : 'POST',
			url : AppURL + '/api/getAllUsers'

		})
	},

	deleteUser : function(user) {
		return $http({
			method : 'POST',
			url : AppURL + '/api/deleteUser',
			data : user
		})
	},
	
	getPolicyTypeAsPerUser : function(userId) {

		return $http({
			method : 'POST',
			url : AppURL + '/api/getPolicyTypeAsPerUser',
			params : {
				"userId" : userId
				
			}

		})
	},
	checkDBpassword : function(currentPass) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/checkDBpassword',
			params : {
				"currentPass" : currentPass,
			}

		})
	},
	updatePassword : function(currentPass, newPass) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/updatePassword',
			params : {
				"currentPass" : currentPass,
				"newPass" : newPass,
			}

		})
	},
	
	/*checklicenseexpired : function() {

		return $http({
			method : 'POST',
			url : AppURL + '/api/checklicenseexpired',
			

		})
	},*/
	
	}
});






