var AppURL = "/TMT";

demoApp.factory('testRunService', function($rootScope, $http) {
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
	return {
		attchmentFetch : function(testcaseID) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/attchmentFetch',
				params:{
					"testcaseID":testcaseID,
				},			
			})
		},
		getTestSuitesNameByPId : function(projectId) {
			return $http({
				method : 'POST',
				url : AppURL + '/api/getTestSuitsByPid',
				params : {
					"projectID" : projectId,
				},
			})
		},
		getAssignToDetails : function(userId, roleid) {
			return $http({
				method : 'POST',
				url : AppURL + '/api/getAllUser',
				params : {
					"id" : userId,
					"role" : roleid,
				},
			})
		},
		getAssignToDetailsByPid : function(pId) {
			return $http({
				method : 'POST',
				url : AppURL + '/api/getUserByProjectId',
				params : {
					"pid" : pId,
				},
			})
		},
		getGroupTestCasesBysuitesFiltered: function(testSuitesId, filterParam) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getGroupTestCasesBysuitesFiltered',
				params:{
					"testSuitesId":testSuitesId,
					"filterParam":filterParam
				},			
			})
		},
		getGroupTestCasesBysuites: function(testSuitesId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getTestGroupAndCases',
				params:{
					"testSuitesId":testSuitesId,				
				},			
			})
		},
		getTestCasesBysuites: function(testSuitesId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getAllTestCassesBySuitesId',
				params:{
					"testSuitesId":testSuitesId,				
				},			
			})
		},
		getTestCasesByTestRun: function(testSuitesId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getAllTestCassesByTestRun',
				params:{
					"rerunId":testSuitesId,				
				},			
			})
		},

		
		getFailTestCasesByReRunId: function(reRunId,runId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getFailTestCasesByReRunId',
				params:{
					"reRunId":reRunId,	
					"runId":runId,
				},			
			})
		},
		
		getFailResolvedTestCasesByReRunId: function(reRunId,runId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getFailResolvedTestCasesByReRunId',
				params:{
					"reRunId":reRunId,	
					"runId":runId,
				},			
			})
		},
		
		createRun: function(testRunObj) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/createRun',
				data:testRunObj			
			})
		},
		/*getActiveRun: function(projectID) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getActiveRun',
				params:{
					"projectID":projectID,				
				},			
			})
		},*/
		getActiveRun: function(projectID) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getdealayedRun',
				params:{
					"projectID":projectID,				
				},			
			})
		},
		
		checkTSuites: function(testsuiteid) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/checkTSuites',
				params:{
					"testsuiteid":testsuiteid,				
				},			
			})
		},
		
getInActiveRun: function(projectID) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getInactiveRun',
				params:{
					"projectID":projectID,				
				},			
			})
		},
		getCompletedRun: function(projectID) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getCompletedRun',
				params:{
					"projectID":projectID,				
				},			
			})
		},
		deleteRun: function(runId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/deleteActiveRun',
				params:{
					"runId":runId,				
},			
			})
		},
		deleteRoundOneRun: function(id) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/deleteRoundOneRun',
				params:{
					"id":id,
				},			
			})
		},
		getRunDetails: function(runId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getTestGroupAndCasesByRun',
				params:{
					"runId":runId,				
				},			
			})
		},
		changeAssiginTo: function(newAssiginId,logsId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/changeAssiginTo',			
				params:{
					"newAssiginId":newAssiginId,
					"logsId":logsId,
				},			
			})
		},
		saveMulAssigin: function(selectedList,mulAssiginName) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/changeMulAssiginTo',
				data:selectedList,
				params:{
					"mulAssiginName":mulAssiginName,				
				},			
			})
		},
		getOverallRunStatus: function(runId) {
			//$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getOverallRunStatus',
				params:{
					"runId":runId,				
				},			
			})
		},
		getOverallActionStatus: function(runId) {
			//$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getOverallRunActionStatus',
				params:{
					"runId":runId,				
				},			
			})
		},
		getOverallDefectStatus: function(runId) {
			//$(".se-pre-con").show();
			return $http({
				method : 'POST',
			   url : AppURL + '/api/getOverallDefectStatus',
				params:{
					"runId":runId,				
				},			
			})
		},
		deletetestcaseid:function(testcasedetails) {
			//$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/deletetestruncaseid',
				data:testcasedetails				
			})
		},
		getRunDetailsFilter: function(runId,status) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getTestGroupAndCasesByRunstutus',
				params:{
					"runId":runId,	
					"status":status
				},			
			})
		},
		stopActiveRun:function(active) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/stopActiveRun',
				data:active		
			})
		},
		findLastRunbyId:function(runId, suiteId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getLastRerunnameByrun',
				params:{
					"runid":runId,	
					"suiteId":suiteId,
				},	
			})
		},
		uploadTestExecution: function(data1) {
			$(".se-pre-con").show();
			console.log("in test run service, data is  " + data1);
			return $http({
				method : 'POST',
				url : AppURL + '/api/uploadTestExecution',
				enctype : 'multipart/form-data',
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				},
				data:data1	
			})
		},
		
		uploadTestExecutions : function(data1,testSuitesId,userId,projectId,round,testSuitesName,runName,targetEndDate,description,isActive) {
			$(".se-pre-con").show();
			//alert("uploadTestExecutions..");
			console.log("In test run service, data is  " +data1);
			return $http({
				method : 'POST',
				url : AppURL + '/api/uploadTestExecutions',
				params:{
					"testSuitesId":testSuitesId,
					"userId": userId,
					"projectId": projectId,
					"round": round,
					"testSuitesName" : testSuitesName,
					"runName" :runName ,
					"targetEndDate" : targetEndDate,
					"description" : description,
					"isActive" : isActive,
				},
				data:data1	,
			})
		},
		
		uploadPreview: function(data1) {
			$(".se-pre-con").show();
			
			return $http({
				method : 'POST',
				url : AppURL + '/api/uploadPreview',
				enctype : 'multipart/form-data',
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				},
				data:data1	
			})
		},
		getresourcewiseexecution:function(runId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getresourcewiseexecution',
				params:{
					"id":runId,				
				},	
			})
		},
	
	uploadDiffPreview:function(data1) {
		$(".se-pre-con").show();

		return $http({
		method : 'POST',
		url : AppURL + '/api/uploadDiffPreview',
		enctype : 'multipart/form-data',
		transformRequest : angular.identity,
		headers : {
		'Content-Type' : undefined
		},
		data:data1
		})
		},
		findDuplicate:function(runName) {
			$(".se-pre-con").show();
			return $http({
			method : 'POST',
			url : AppURL + '/api/findDuplicate',
			params:{
			"runName":runName,
			},
			})
			},
	
}});