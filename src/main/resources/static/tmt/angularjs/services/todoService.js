var AppURL="/TMT"; 

demoApp.factory('todoService', function($rootScope, $http) {
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
	return { 	
		getTodoRunList: function(uId,projectId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getTodoRunList',
				params:{
					"userId":uId,
					"projectId":projectId,
				},			
			})
	},
	getTestCaseForExec: function(runId,uId) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getTodoTestCaseList',
			params:{
				"runId":runId,
				"userId":uId,
			},			
		})
	},
	
	deleteScreenshot: function(testcasesid,filename) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/deleteImage',
			params:{
				"testcaseID":testcasesid,
				"attachname":filename,
			},			
		})
	},
	saveExecution: function(testCase) {
//		alert("from service: "+testCase);
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/saveTestExceution',
			data:testCase,			
		})
	},
	raiseIssue: function(projectName,projectId) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getComponetDetails',
			params:{
				"projectName":projectName,
				"projectId":projectId,
			},			
		})
	},
	reopenBugzillaIssue: function(projectName, projectId, bugID) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/reopenBugzillaIssue',
			params:{
				"projectName":projectName,
				"projectId":projectId,
				"bugID":bugID,
			},			
		})
	},
	getBugzillaBugsProductWise: function(projectName,projectID) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getBugzillaBugsProductWise',
			params:{
				"projectName":projectName,
				"projectID":projectID
			},			
		})
	},
	fetchBugComment: function(projectID, bugID) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/fetchBugComment',
			params:{
				"projectID":projectID,
				"bugID":bugID,
			},			
		})
	},
	saveAttachment : function(attachment)
	{
		//alert(attachment);
		$(".se-pre-con").show();
		return $http({
		method : 'POST',
		url : AppURL + '/api/saveAttachment',
		enctype:'multipart/form-data',
		processData:false,
		transformRequest : angular.identity,
		            headers : {
		                    'Content-Type' : undefined
		             },
		/*params:{
			"bugTool":bugTool,
		},*/
		data:attachment 
		})
	},
	
	attchmentFetch : function(testcaseID) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/attchmentFetch',
			params:{
				"testcaseID":testcaseID,
			},			
		})
	},
	attchmentDownload : function(testcaseID) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/downloadImage',
			params:{
				"testcaseID":testcaseID,
			},			
		})
	},
	showBugToolActive:function()
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/ShowbugTrackorActive',
				
				});
				
			},
	getAllRedmineStatuses:function(bugToolID)
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/getAllRedmineStatuses',
					data:{
						"toolid":bugToolID
					},
				});
				
			},
	getAllRedmineIssuePriorities:function(bugToolID)
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/getAllRedmineIssuePriorities',
					data:{
						"toolid":bugToolID
					},
				});
				
			},
	getRedmineProjectByIdentifier:function(identifier,projectId)
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/getRedmineProjectByIdentifier',
					data:{
						"identifier":identifier,
						"tmtProjectID":projectId
					},
				});
				
			},
			
}
});//End Of File