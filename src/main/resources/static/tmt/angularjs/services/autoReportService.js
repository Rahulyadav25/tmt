var AppURL="/TMT"; 

demoApp.factory('autoReportService', function($rootScope, $http) {
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
	return {
		getslavemachine : function(pid,pName) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getslavemachine',
				params : {
					"pId" : pid,
					"pName":pName
				},
			})
		},
		saveAutomationbug : function(projectDetailsdata) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/saveAutomationbug',
				params : {
					"tstatus" :projectDetailsdata.tstatus,
					"texpectedResult":projectDetailsdata.texpectedResult,
					"tseverity" : projectDetailsdata.tstatus,
					"tpriority":projectDetailsdata.tpriority,
					"tbugsummary" :projectDetailsdata.tbugsummary,
					"tbrowser":projectDetailsdata.tbrowser,	
					"tbugtool":projectDetailsdata.bugtool,	
					"tdescription":projectDetailsdata.description,	
					"project":projectDetailsdata.projectname,	
				},
			})
		},
		getAutomationRunReport : function(pName) {
			$(".se-pre-con").show();
			return $http({
				method : 'GET',
				url : AppURL + '/api/getAutomationRunReport',
				params : {
					"pName":pName
				},
			})
		},
		//pranik
		getDefectReport : function(pName) {
			$(".se-pre-con").show();
			return $http({
				method : 'GET',
				url : AppURL + '/api/getDefectReport',
				params : {
					"pName":pName
				},
			})
		},
		getAllDefectReport : function(runID,projectName) {
			$(".se-pre-con").show();
			return $http({
				method : 'GET',
				url : AppURL + '/api/getAllDefectReport',
				params : {
					"runID":runID,
					"projectName":projectName
					
				},
			})
		},
		getBugzillaBugsProductWise: function(projectName,projectID) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getBugzillaBugsProductWise',
				params:{
					"projectName":projectName,
					"projectID":projectID
				},			
			})
		},
		
		raiseIssue: function(projectName,projectId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getComponetDetails',
				params:{
					"projectName":projectName,
					"projectId":projectId,
				},			
			})
		},
		attchmentDownload : function(testcaseID) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/downloadImage',
				params:{
					"testcaseID":testcaseID,
				},			
			})
		},
		deleteScreenshot: function(testcasesid,filename) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/deleteImage',
				params:{
					"testcaseID":testcasesid,
					"attachname":filename,
				},			
			})
		},
//		saveExecution: function(testCase) {
//			console.log("autotestcase***"+testCase);
////			alert("from service: "+testCase);
//			$(".se-pre-con").show();
//			return $http({
//				method : 'POST',
//				url : AppURL + '/api/saveTestExceution',
//				data:testCase,			
//			})
//		},
		
		//new added
		saveExecution: function(actualResult,expectedResult,testCaseSrNo,testDescription,
				testCaseAction,status,bugId,bugPriority,bugSeverity,executionTime,
				bugType,bugToolId,browser,complexity,functionality,groupName,scenarioID,scenarios,testCaseType,
				testData,testerComment,testID,steps,priority,bugStatus,apiBugSummary,bugToolName,projectId,projectName) {
		//	console.log("autotestcase***"+testCase);
//		//	alert("from service: "+testCase);
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/saveTestExceution1',
				params:{
					
					"actualResult":actualResult,
					"expectedResult":expectedResult,
					"testCaseSrNo":testCaseSrNo,
					"testDescription":testDescription,
					"testCaseAction":testCaseAction,
					"status":status,
					"bugId":bugId,
					"bugPriority":bugPriority,
					"bugSeverity":bugSeverity,
				    "executionTime":executionTime,
					"bugType":bugType,
					"bugToolId":bugToolId,
					"browser":browser,
					"complexity":complexity,
					"functionality":functionality,
					"groupName":groupName,
					"scenarioID":scenarioID,
					"scenarios":scenarios,
					"testCaseType":testCaseType,
					"testData":testData,
					"testerComment":testerComment,
					"testID":testID,
					"steps":steps,
					"priority":priority,
					"bugStatus":bugStatus,
					"apiBugSummary":apiBugSummary,
					"bugToolName":bugToolName,
					"projectId":projectId,
					"projectName":projectName
					
					
				},		
			})
		},
		
		saveAttachment : function(attachment)
		{
			//alert(attachment);
			$(".se-pre-con").show();
			return $http({
			method : 'POST',
			url : AppURL + '/api/saveAttachment',
			enctype:'multipart/form-data',
			processData:false,
			transformRequest : angular.identity,
			            headers : {
			                    'Content-Type' : undefined
			             },
			/*params:{
				"bugTool":bugTool,
			},*/
			data:attachment 
			})
		},
		
		attchmentFetch : function(testcaseID) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/attchmentFetch',
				params:{
					"testcaseID":testcaseID,
				},			
			})
		},
		
		
		//new added
		attchmentFetchAuto : function(testcaseID,bugId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/attchmentFetch1',
				params:{
					"testcaseID":testcaseID,
					"bugId":bugId
				},			
			})
		},
		
		fetchBugComment: function(projectID, bugID) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/fetchBugComment',
				params:{
					"projectID":projectID,
					"bugID":bugID,
				},			
			})
		},
		getExtendedReport : function(runId,scenario) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getExtendedReport',
				params : {
					"runId":runId,
					"scenario":scenario 
				},
			})
		},
		getExtentReportScreenshot : function(screenshotId) {
			$(".se-pre-con").show();
			return $http({
				method : 'GET',
				url : AppURL + '/api/getExtentReportScreenshot',
				params : {
					"screenshotId":screenshotId,
				},
			})
		},
		getEnvironmentAnalysis : function(runId) {
//			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getEnvironmentAnalysis',
				params : {
					"runId":runId,
				},
			})
		},
		exportToExcel : function(runId) {
		//	$(".se-pre-con").show();
//		alert("sending");
			return $http({
				method : 'POST',
				url : AppURL + '/api/exportTestCaseToExcel',
				params : {
					"runId":runId,
				},
			    headers: {
       			'Content-type': 'application/json'
    	     	},
    	        responseType: 'blob'
			})
		},
		getScenarioRunwise : function(runId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getScenarioRunwise',
				params : {
					"runID":runId,
				},
			})
		},
		getTotalScenariosTestCasesSteps : function(runId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getTotalScenariosTestCasesSteps1',
				params : {
					"runID":runId,
				},
			})
		},
		
		
		
	}
});//End Of File