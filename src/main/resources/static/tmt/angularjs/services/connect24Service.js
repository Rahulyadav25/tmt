var AppURL="/TMT"; 

demoApp.factory('connect24Service', function($rootScope, $http) {
var token=localStorage.getItem('tokenID');
$http.defaults.headers.common.Authorization = token;

return { 	

		getAllChannel : function() {
	
			return $http({
				method : 'POST',
				url : AppURL + '/api/getAllChannel',
			})
		},
		
		getTodaysTransactionCount : function() {
	
			return $http({
				method : 'POST',
				url : AppURL + '/api/getTodaysTransactionCount',
			})
		},
		
		getCommandPerChannel : function(channel) {
	
			return $http({
				method : 'POST',
				url : AppURL + '/api/getCommandPerChannel',
				params : {
					"channel" : channel
					
				}
			})
		},
		getConnect24Graph : function(filterObj) {
	
			return $http({
				method : 'POST',
				url : AppURL + '/api/connect24Graph',
				data:filterObj
				})
		},
		getCommandDataPerChannel : function(filterObj) {
	
			return $http({
				method : 'POST',
				url : AppURL + '/api/getCommandDataPerChannel',
				data:filterObj
			})
		},
		
		getGraphBetweenDate : function(filterObj) {
			
			return $http({
				method : 'POST',
				url : AppURL + '/api/getGraphBetweenDate',
				data:filterObj
			})
		},
		
		getAllCommandChannelGraphBetweenDate : function(filterObj) {
			
			return $http({
				method : 'POST',
				url : AppURL + '/api/allCommandChannelGraphBetweenDate',
				data:filterObj
			})
		},
		
		getCommandGraphAsPerChannelBetweenDate : function(filterObj) {
			
			return $http({
				method : 'POST',
				url : AppURL + '/api/commandGraphAsPerChannelBetweenDate',
				data:filterObj
			})
		},
		
		getGraphBasisOnHours : function(filterObj) {
			
			return $http({
				method : 'POST',
				url : AppURL + '/api/getGraphBasisOnHours',
				data:filterObj
			})
		},
		
		getGraphBasisOnHoursForBoth : function(filterObj) {
			
			return $http({
				method : 'POST',
				url : AppURL + '/api/getGraphBasisOnHoursForBoth',
				data:filterObj
			})
		},
		
		getGraphForChannelPerHour : function(filterObj) {
			
			return $http({
				method : 'POST',
				url : AppURL + '/api/graphForChannelPerHour',
				data:filterObj
			})
		}
	}
});