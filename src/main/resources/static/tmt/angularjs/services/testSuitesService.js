var AppURL="/TMT"; 

demoApp.factory('testSuitesService', function($rootScope, $http) {

return { 	
	getAllTestSuitesByProject: function(projectId) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getTestSuits',
			params:{
				"projectID":projectId,				
			},			
		})
},
getTestGroupByGroupId: function(testGroupId) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/getTestGroupDetails',
		params:{
			"testGroupId":testGroupId,				
		},			
	})
},
getTestCasesBysuites: function(testSuitesId) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/getTestGroupAndCases',
		params:{
			"testSuitesId":testSuitesId,				
		},			
	})
},
createTestSuites: function(data1) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/createtestsuitesandcases',
		enctype : 'multipart/form-data',
		transformRequest : angular.identity,
		headers : {
			'Content-Type' : undefined
		},
		data:data1	
	})
},

createTestSuitesWithoutFile: function(testSuitesObj) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/createtestsuites',
		data:testSuitesObj		
	})
},
retrieveTestSuite: function(retieveTestSuite, decision) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/retrieveSameTestSuite',
		data:retieveTestSuite,
		params:{
			"decision":decision,
		},
	})
},

updateTestSuites: function(testSuitesObj,projectwiserole) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/updatetestsuites',
		data:testSuitesObj,
		params:{
			"projectwiserole":projectwiserole
		},
	})
},
getTestSuitesById: function(testSuitesid) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/getTestSuitsDetails',
		params:{
			"testSuitesId":testSuitesid,				
		},		
	})
},
deleteTestCase: function(testcaseSID) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/deleteTestCase',
		params:{
			"testcaseSID":testcaseSID,				
		},		
	})
},
deleteMulTestCase: function(testcaseSID) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/deleteMulTestCase',
		params:{
			"testcaseSID":testcaseSID,				
		},		
	})
},
importTestCases: function(data1) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/importTestCases',
		enctype : 'multipart/form-data',
		transformRequest : angular.identity,
		headers : {
			'Content-Type' : undefined
		},
		data:data1	
	})
},
importTestSuites: function(data1) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/importtestsuites',
		enctype : 'multipart/form-data',
		transformRequest : angular.identity,
		headers : {
			'Content-Type' : undefined
		},
		data:data1	
	})
},
addNewTestId: function(testcase) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/addNewTestId',
		data:testcase				
	})
},
updateTestCase: function(testcase) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/updateTestCase',
		data:testcase				
	})
},
createTestGroup: function(createTestGroupObj) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/createTestgroup',
		data:createTestGroupObj		
	})
},
updateTestGroup: function(testGroupsObj) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/updateTestGroup',
		data:testGroupsObj				
	})
},

deleteTestSuites: function(updatedByEmail, testsuiteid, testsuitesname) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/deleteTestSuites',
		params:{
			"updatedByEmail":updatedByEmail,
			"testsuiteid":testsuiteid,		
			"testsuitesname":testsuitesname,
		},					
	})
},

deletegroup: function(groupId) {
	$(".se-pre-con").show();
	console.log(groupId);
	return $http({
		method : 'POST',
		url : AppURL + '/api/deletegroup',
		params:{
			"groupId":groupId,				
		},					
	})
},

//Lipsa--16_09
updateTestGroupID: function(caseId, groupId)
{
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/updateTestGroupID',
		params:{
			"caseId":caseId,		
			"groupId":groupId,
		},					
	})
},

} });//End Of File