var AppURL="/TMT"; 

demoApp.factory('viewProjectsService', function($rootScope, $http) {
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
	return { 	

		changeProjectStatus : function(projectID,isActive,user_role) {
			/*alert(projectID);
			alert(isActive);
			alert(user_role);*/
			/*$(".se-pre-con").show();*/
			return $http({
				method : 'POST',
				url : AppURL + '/api/changeProjectStatus',
				params : {
					"projectID" : projectID,
					"isActive":isActive,
					"user_role":user_role
				}

			})
		},
	
	}
})