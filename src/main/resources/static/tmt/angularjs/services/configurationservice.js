demoApp.service('configurationservice', [ '$http', function($http){
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
	var AppURL="/TMT";
		return { 	
		getUser : function(id,role) {
			$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getAllUser',
			params:{
				"id":id,
				"role":role,
			}
		});
	   
		},
		getroletype : function(role) {
			$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getRoletype',
			params:{
				
				"role":role,
			}
		});
	   
		},
		getRoletypebyproject : function(projectid,id) {
			$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getRoletypebyproject',
			params:{
				"projectid":projectid,
				"id":id
			}
		});
	   
		},
		showUsersDetails:function(mailid){
			$(".se-pre-con").show();
		return $http({
		method : 'POST',
		url : AppURL+'/api/getUserbyid',
		params:{
			"userid":mailid,
		}
	        });
	    },
	    createUser : function(mailid,name,usertype,creator,userid) {
	    	$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/createUser',
				params:{
					"mailid":mailid,
					"name":name,
					"usertype":usertype,
					"creator":creator,
					"userid":userid
				}
			});
		},
		updateUser : function(userid,username,name,usertype,activation) {
			$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/updateUser',
					params:{
						"userid":userid,
						"username":username,
						"name":name,
						"usertype":usertype,
						"activation":activation,
					}
				});
			},
		deleteuser : function(userid) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/deleteUser',
				params:{
					"userid":userid
					
				}
			});
		},
		
		deleterole : function(userid) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/deleterole',
				params:{
					"userid":userid
					
				}
			});
		},
			getAllproject:function()
			{
				$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/getAllproject'
				});
			},
			getAllprojectunderUser:function(userid,userrole)
			{
				$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/getAllprojectunderuser',
					params:{
						"userid":userid,
					   "userrole":userrole
					}
				});
			},
			getUserAccessList:function(userid,roleid)
			{
				$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/getUserAccessList',
					params:{
						"userid":userid,
						"roleid":roleid,
					}
				});
			},
			
			saveaccess:function(accessObj)
			{
				
				$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/saveaccess',
					data:accessObj
				});
			},
			saverole:function(role)
			{
				$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/saveroleuser',
					params:{
						"role":role
					}
				});
				
			},
			
			getAllrole:function()
			{
				$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/getAllrole',
				});
				
			},
			getallmanager:function()
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/getallmanager',
				});
				
			},
			edituser:function(id,username,isactive,manager,name,userrole)
			{
				console.log("Edit users ")
				$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/edituser',
					params:{
						"id":id,
						"username":username,
						"isactive":isactive,
						"manager":manager,
						"name":name,
						"userrole":userrole,
					}
				});
				
			},
			removeAccess:function(projectid,userid)
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/removeAccess',
					params:{
						"projectid":projectid,
						"userid":userid
						
					}
				});
				
			},
			getdetailsbyuser:function(userid)
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/getdetailsbyuserid',
					params:{
						"userid":userid
					}
				});
			},
			createbugTrackor:function(name,ip,active,user,password,udName)
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/createbugTrackor',
					params:{
						"name":name,
						"ip":ip,
						"active":active,
						"userid":user,
						"password":password,
						"udName":udName
					}

				
				});
				
			},
			getIntegratedBugTool:function()
			{
				$(".se-pre-con").show();
				return $http({
					method : 'GET',
					url : AppURL + '/api/getIntegratedBugTool',
				
				});
				
			},
			ShowbugTrackor:function()
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/ShowbugTrackor',
				
				});
				
			},
			ShowbugTrackorbyId:function(id)
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/ShowbugTrackorbyID',
					params:{
						"id":id,
						
					}
				});
				
			},
			EditTrackorbyId:function(id,ip,name,active,userid,password,udName)
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/EditbugTrackor',
					params:{
						"id":id,
						"ip":ip,
						"name":name,
						"active":active,
						"userid":userid,
						"password":password,
						"udName":udName
						
					}
				});
				
			},
			showBugToolActive:function()
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/ShowbugTrackorActive',
				
				});
				
			},
			deleteBugTool:function(id)
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/deleteBugTool',
					params:{
						"id":id
					}
				});
				
			},
			showDashboard:function()
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/OverallGraph',
					
				});
				
			},
			showrunwise:function(id)
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/RunwiseGraph',
					params:{
						"id":id
					}
				});
				
			},
			showDashboardprojectwise:function(id)
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/ProjectWiseGraph',
					params:{
						"id":id
					}
				});
				
			},
			unLockUser:function(userId)
			{$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/unLockUser',
					params:{
						"userId":userId
					}
				});
				
			}
			
			
	    }
	}
]);//END OF FILE