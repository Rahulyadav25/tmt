demoApp.service('projectOverviewAutoService', [ '$http', function($http){
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
	var AppURL="/TMT";
		return { 	
			getoverviewdetails : function(projectid,id,user_role) {
			$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getoverviewdetails',
			params:{
				"projectid":projectid,
				"id":id,
				"user_role" :user_role

			}
		});
	   
		},

		getLogDetails : function(projectid,id,user_role,fromDate,toDate) {
			$(".se-pre-con").show();
		return $http({
			method : 'GET',
			url : AppURL + '/api/getLogDetails',
			params:{
				"projectid":projectid,
				"id":id,
				"user_role" :user_role,
				"fromDate":fromDate,
				"toDate":toDate

			}
		});
	                            
		},	
		getallActiveRun: function(projectid,id,user_role) {
			$(".se-pre-con").show();
			return $http({
				method : 'GET',
				url : AppURL + '/api/getallActiveRun',
				params:{
					"projectid":projectid,
					"id":id,
					"user_role" :user_role

				}
			});
		   
			},	
			
			runwiseAutodetails: function(runlist,projectid,id,user_role) {
				$(".se-pre-con").show();
				console.log(runlist);
				return $http({
					method : 'GET',
					url : AppURL + '/api/runwiseAutodetails',
					params:{
						"runlist":runlist,	
						"projectid":projectid,
						"id":id,
						"user_role" :user_role
					},	
				})
			},
			getSingleproject : function getSingleproject(projectid) {
				$(".se-pre-con").show();
					return $http({
						method : 'GET',
						url : AppURL + '/api/getSingleproject',
						params:{
							"projectid":projectid
						},	
					})
				
				},
			
			
	    }
	}
]);//END OF FILE