var AppURL="/TMT"; 

demoApp.factory('automationRunService', function($rootScope, $http) {
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
	return {
		getslavemachine : function(pid,pName) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getslavemachine',
				params : {
					"pId" : pid,
					"pName":pName
				},
			})
		},
		updateJenkinsProject : function(projectDetails) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/updateJenkinsProject',
				data:projectDetails
			})
		},

	
		startscript : function(pid,pName,datasheetsList,projectdetails) {

			//$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/startscript',
				params : {
					"pId" : pid,
					"pName":pName,
					"datasheetsList":datasheetsList,
					
				},

				data:projectdetails,

			})
		},	
		stopscript : function(pid,pName) {
			//$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/stopscript',
				params : {
					"pId" : pid,
					"pName":pName
				},
			})
		},
		getCurrentRun : function(pid,pName) {
			//$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getCurrentRun',
				params : {
					"pId" : pid,
					"pName":pName
				},
			})
		},
		currentRunDetails : function(pid,pName) {
			//$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/currentRunDetails',
				params : {
					"pId" : pid,
					"pName":pName
				},
			})
		},
		getTestSuitesName : function(pid,pName) {
			//$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/gettestSuitesname',
				params : {
					"pId" : pid,
					"pName":pName
				},
			})
		},
	}
});//End Of File