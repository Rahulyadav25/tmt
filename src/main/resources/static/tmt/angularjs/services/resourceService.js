var AppURL = "/TMT";

demoApp.factory('resourceService', function($rootScope, $http) {

	return {
		
		getUserByProjectId : function(pId) {
			return $http({
				method : 'POST',
				url : AppURL + '/api/getUserByProjectId',
				params : {
					"pid" : pId,
				},
			})
		},
		getResourcesGraph : function(resourseReqObj) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getresourseefforts',
				data:resourseReqObj,
			})
		},
		getResourseEffortsFreshRun : function(resourseReqObj) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getResourseEffortsFreshRun',
				data:resourseReqObj,
			})
		},
		getResourseEffortstake : function(resourseReqObj) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getResourseEffortstake',
				data:resourseReqObj,
			})
		},
		tableResourceEfforts : function(resourseReqObj) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/tableResourceEfforts',
				data:resourseReqObj,
			})
		},
		
	}
});//End Of File