var AppURL="/TMT"; 

demoApp.factory('scriptConfigService', function($rootScope, $http) {

return { 
	
	checkControllerFile: function(projectName,sheetData) {
		$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/checkControllerFile',
				params:{
					'projectName':projectName,
					'sheetData':sheetData,
				},
		})
	},
	
	uploadDataSheets: function(datasheet) {
		$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/uploadDataSheets',
				enctype:'multipart/form-data',
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				},
				data:datasheet
		})
	},
	
	uploadControllerFile:function(controllerFile)
	{
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/uploadControllerFile',
			enctype:'multipart/form-data',
			transformRequest : angular.identity,
			headers : {
				'Content-Type' : undefined
			},
			data:controllerFile,
		});
	},
	
	downloadAutoFile:function(type, fileName, projectName)
	{
		$(".se-pre-con").show();
		return $http({
			method : 'GET',
			url : URL + '/api/downloadAutoFile',
			params:{
				'type':type,
				'fileName':fileName,
				'projectName':projectName,
			}
				
		});
	},
	
	deleteAutomationFile : function(type, fileName, projectName) {
		$(".se-pre-con").show();
		return $http({
			method : 'GET',
			url : AppURL + '/api/deleteAutomationFile',
			params : {
				'type':type,
				'fileName':fileName,
				'projectName':projectName
			},
		})
	},
}

});
