var app = angular.module('demo.services', []);
app.service('dashboardService', [ '$http', function($http) {
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
	var AppURL="/TMT";
	
	this.createProject = function createProject(createProjectObj,userid) {
		$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/createproject',
				data:createProjectObj,
				params:{
					"userid":userid,
				},
			})
	},
	
	this.getProjectBugStatus = function getProjectBugStatus(projectname,projectid) {
		$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getProjectBugStatus',
				params:{
					"projectName":projectname,
					"projectID":projectid,
				},	
			})
		
		},
		
		this.getProjectBugStatusBgz = function getProjectBugStatus(projectname,projectid) {
			$(".se-pre-con").show();
				return $http({
					method : 'POST',
					url : AppURL + '/api/getProjectBugStatusBgz',
					params:{
						"projectName":projectname,
						"projectID":projectid,
					},	
				})
			
			},
	
	this.getproject = function getproject(projectname,projectid) {
		$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getprojectDetails',
				params:{
					"projectname":projectname,
					"projectid":projectid,
				},	
			})
		
		},
		
		this.getSummaryDetails = function getSummaryDetails(projectid,id,user_role) {
			$(".se-pre-con").show();
				return $http({
					method : 'GET',
					url : AppURL + '/api/getSummaryDetails',
					params:{
						"projectid":projectid,
						"id":id,
						"user_role":user_role,
					},	
				})
			
			},
this.getRunStatus = function getRunStatus(projectname,projectid) {
	$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getRunStatus',
			params:{
				"projectname":projectname,
				"projectid":projectid,
			},	
		})
		
	
	

	
	}
this.getSingleproject = function getSingleproject(projectid) {
	$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getSingleproject',
			params:{
				"projectid":projectid,
			},	
		})
	
	}
this.getAllProject = function getAllProject() {
	$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getAllproject',
			
		})
	
	}
this.getOverviewProject = function getOverviewProject(userid,roleid) {
	$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getOverViewproject',
			params:{
				"userid":userid,
				"roleid":roleid,
			},
		})
	
	}
this.showBugToolActive = function showBugToolActive() {
	$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/ShowbugTrackorActive',
			
		})
	
	}
	this.getServices = function getServices(ip) {
		
		return $http({
			method : 'POST',
			url : AppURL+'/api/getServices',
			params:{
				"ip":ip,
				
			},			
		})	
	}
	
	
	
	this.getErrordetails = function getErrordetails(applicationName,monthEnd,slackTime,peakTime,startDate,endDate) {
		
		 return $http({
			method : 'POST',
			url : AppURL+'/api/getErrorDetails',
			params:{
				"applicationName":applicationName,
				"monthEnd":monthEnd,
				"slackTime":slackTime,
				"peakTime":peakTime,
				"startDate":startDate,
				"endDate":endDate,			
			}
		})
	}
	
	
	
	this.getNagiosDetails = function getNagiosDetails(applicationName,monthEnd,slackTime,peakTime,startDate,endDate) {
		
		return $http({
			method : 'POST',
			url : AppURL+'/api/getNagiosDetails',
			params:{
				"applicationName":applicationName,
				"monthEnd":monthEnd,
				"slackTime":slackTime,
				"peakTime":peakTime,
				"startDate":startDate,
				"endDate":endDate,
				
			}
			
			
		})
	}
	
	this.configProjects = function configProjects(user_role,userid) {
		$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/configProjects',
				params:{
					"user_role":user_role,
					"userid":userid,
					
				},	
				
			})
		
		}
	
	this.getappDynamicsData = function getappDynamicsData(applicationName,monthEnd,slackTime,peakTime,startDate,endDate) {
		
		return $http({
			method : 'POST',
			url : AppURL+'/api/getappDynamicsData',
			params:{
				"applicationName":applicationName,
				"monthEnd":monthEnd,
				"slackTime":slackTime,
				"peakTime":peakTime,
				"startDate":startDate,
				"endDate":endDate,
				
			}
			
		})
	}
	this.deleteProjectbyid = function deleteProjectbyid(projectid,isPtype) {
		$(".se-pre-con").show();
			return $http({
				
				method : 'POST',
				url : AppURL + '/api/deleteProjectbyid',
				params:{
					"projectid":projectid,
					"isPtype":isPtype,
				},	
			})
		
		}
	
	this.EditProject = function EditProject(EditProjectObj) {
		$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/editprojectsave',
				data:EditProjectObj
			})
			
		
		

		
		}
	this.getdashboardview = function getdashboardview(id,userid,roleid,pageNo,pageSize) {
		console.log(pageNo+"new Pqage SIze"+pageSize+"user id"+userid+"roleid"+localStorage.getItem('userRole'));
		
		$(".se-pre-con").show();
		
		return $http({
			method : 'POST',
			url : AppURL + '/api/projectholeview',
			params:{
				"userid":localStorage.getItem('userid'),
				"roleid":localStorage.getItem('userRole'),
				"pageNo":pageNo,
				"PageSize":pageSize,
			},
		})
		
	}
	this.getAutoDashboardView= function getAutodashboardview(userid,roleid,start) {
	$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/projectholeviewAuto',
			params:{
				"userid":userid,
				"roleid":roleid,
				"start":start,
			},
		})
		
	}
	this.projectviewdata = function projectviewdata(userid,roleid) {
		$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/projectviewdata',
				params:{
					"userid":userid,
					"roleid":roleid,
					"isActive":isActive,
				},
			})
			
		}
		this.getRunStatus =function(projectid,id,user_role) {
			$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getRunStatus',
			params:{
				"projectid":projectid,
				"id":id,
				"user_role" :user_role

			},
		});
	   
		}
		this.getProjectToolDetails =function(projectid) {
			$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getProjectToolDetails',
			params:{
				"projectid":projectid,
			},
		});
	   
		}
}
 ]);