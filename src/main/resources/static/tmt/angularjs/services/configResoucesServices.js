var AppURL = "/TMT";

demoApp.factory('configResoucesServices', function($rootScope, $http) {

	return {
		
		getAllProjectName : function() {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getAllProjectName',
				
			})
		},
		
		getUserListOfResouce : function() {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getUserListOfResouce',
				
			})
		},
		
		getResorcesEffort : function(resourseReqObj) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getResorcesEffort',
				data: resourseReqObj,
			})
		},
		
		getUserByProjectId : function(pId) {
			return $http({
				method : 'POST',
				url : AppURL + '/api/getUserByProjectId',
				params : {
					"pid" : pId,
				},
			})
		},
		getDatewiseUserForResouces : function(resourseReqObj) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getDatewiseUserForResouces',
				data:resourseReqObj,
			})
		},
		getResourseEffortsFreshRun : function(resourseReqObj) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getResourseEffortsFreshRun',
				data:resourseReqObj,
			})
		},
		getResourseEffortstake : function(resourseReqObj) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getResourseEffortstake',
				data:resourseReqObj,
			})
		},
		tableResourceEffortsAdmin : function(resourseReqObj) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/tableResourceEffortsAdmin',
				data:resourseReqObj,
			})
		},
		
		getAllProjectNameForMAnager  : function(uId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getAllProjectNameForMAnager',
				params : {
					"userid":uId,
				},
				
			})
		},
		
		getMangetCreatedUserList  : function(uId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getMangetCreatedUserList',
				params : {
					"userid":uId,
				},
				
			})
		},
		
		tableResourceEffortsForManager  : function(resourseReqObj) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/tableResourceEffortsForManager',
				data:resourseReqObj,
			})
		},
		
		getUserListOfResouceByProjectIDs  : function(pIDs, userRole, userID) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getUserListOfResouceByProjectIDs',
				params : {
					"pIDs":pIDs,
					"userRole":userRole,
					"userID":userID
				},
			})
		},

		
	}
});//End Of File