demoApp.service('projectOverviewAutoService', [ '$http', function($http){
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
	var AppURL="/TMT";
		return { 
			getoverviewdetails : function(projectid,id,user_role) {
				$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + /api/getoverviewdetails',
				params:{
					"projectid":projectid,
					"id":id,
					"user_role" :user_role
				}
			});
		   
			},
		}
}
]);//END OF FILE
			