var AppURL="/TMT"; 

demoApp.factory('projectDetailsService', function($rootScope, $http) {
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
return { 	
	getAllTestSuitesByProject: function(projectId) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getTestSuits',
			params:{
				"projectID":projectId,				
			},			
		})
},
	getProjectAutoFiles: function(projectName) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getProjectAutoFiles',
			params:{
				"projectName":projectName,				
			},
		})
	},





} });//End Of File