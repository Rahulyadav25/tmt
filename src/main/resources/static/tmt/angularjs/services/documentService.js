var AppURL = "/TMT";

demoApp.factory('documentService', function($rootScope, $http) {
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
	return {
		uploadDocuments : function(attachment) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/uploadDocument',
				enctype : 'multipart/form-data',
				transformRequest : angular.identity,
				headers : {
					'Content-Type' : undefined
				},
				data : attachment
			})
		},
		publicDocument : function() {
			$(".se-pre-con").show();
			return $http({
				method : 'GET',
				url : AppURL + '/api/getPublicDocument',

			})
		},
		projectDocument : function(pId) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getProjectDocument',
				params:{
					"pId":pId,				
				},	
			})
		},
		downloadDocument : function(filedetails) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/downloadDocument',
				data : filedetails
			})
		},
		
		allDocumentTypes :function(){
			$(".se-pre-con").show();
			return $http({
				method : 'GET',
				url : AppURL + '/api/getAllDocumentTypes',

			})
		},
		
		deleteDocument :function(id,filePath,documentName){
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/deleteDocument',
				params:{
					"id":id,
					"filePath":filePath,
					"documentName":documentName,
				},	
			})
		},
	}
});//End Of File