var AppURL="/TMT"; 

demoApp.factory('projectOverviewService', function($rootScope, $http) {
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
return { 	
	saveApplicationDetails: function(AppObject) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/saveApplication',
			data:AppObject
		})
},

testrunwisedetails: function(projectid) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/testrunwisedetails',
		params:{
			"projectid":projectid,				
		},	
	})
},

testrunwisedetailsoverview: function(projectid) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/testrunwisedetailsoverview',
		params:{
			"projectid":projectid,				
		},	
	})
},
projectrunwisedetails: function(projectid) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/projectrunwisedetails',
		params:{
			"projectid":projectid,				
		},	
	})
},
runwisedetailscheck: function(runlist) {
	$(".se-pre-con").show();
	console.log(runlist);
	return $http({
		method : 'POST',
		url : AppURL + '/api/runwisedetails',
		params:{
			"runlist":runlist,				
		},	
	})
},
runwisefailstatus: function(runlist) {
	$(".se-pre-con").show();
	console.log(runlist);
	return $http({
		method : 'POST',
		url : AppURL + '/api/runwisefailstatus',
		params:{
			"runlist":runlist,				
		},	
	})
},

getallActiveRun: function(projectid) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/getallActiveRun',
		params:{
			"projectid":projectid,				
		},			
	})
},

	projectwisefailstatus: function(projectid) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/projectwisefailstatus',
			params:{
				"projectid":projectid,				
			},			
		})
},
getProjectDetails: function(projectid) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/getProjectDetails',
		params:{
			"projectid":projectid,				
		},			
	})
},
datewiserun: function(startdate,enddate,projectid) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/datewiserun',
		params:{
			"startdate":startdate,
			"enddate":enddate,
			"projectid":projectid,
		},			
	})
},
datewiserunfailstatus: function(startdate,enddate,projectid) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/datewiserunfailstatus',
		params:{
			"startdate":startdate,
			"enddate":enddate,
			"projectid":projectid,
		},			
	})
}

} });//End Of File