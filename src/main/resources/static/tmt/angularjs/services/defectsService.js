var AppURL="/TMT"; 

demoApp.factory('defectsService', function($rootScope, $http) {
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
return { 	
	saveApplicationDetails: function(AppObject) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/saveApplication',
			data:AppObject
		})
},

getDefectProjectDetails: function(projectid) {
	/*$(".se-pre-con").show();*/
	//alert("Deefect service js")
	return $http({
		method : 'POST',
		url : AppURL + '/api/getDefectProjectDetails',
		params:{
			/*"projectName":projectName*/
			"projectid":projectid,				
		},	
	})
},
updateBugDetailsAPI: function(projectName,projectId) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/updateBugDetailsAPI',
		params:{
			"projectName":projectName,
			"projectID":projectId,
			
		},	
	})
},


getdefectseveritygraph: function(projectid) {
	/*$(".se-pre-con").show();*/
	//alert("Deefect service js")
	return $http({
		method : 'POST',
		url : AppURL + '/api/getdefectseveritygraph',
		params:{
			"projectid":projectid,				
		},	
	})
},

getstuatudefectsgraph : function(projectid) {
	/*$(".se-pre-con").show();*/
	//alert("Deefect service js")
	return $http({
		method : 'POST',
		url : AppURL + '/api/getstuatudefectsgraph',
		params:{
			/*"projectName":projectName*/
			"projectid":projectid,				
		},	
	})
},
exportRunWise : function(projectid) {
	return $http({
		method : 'POST',
		url : AppURL + '/api/exportTestCaseBugsToExcel',
		params:{
			"pID":projectid,				
		},	
		  headers: {
       			'Content-type': 'application/json'
    	     	},
    	 responseType: 'blob'
	})
},
getProjectIssues: function(projectId,projectIdentifier) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/getProjectIssues',
		data:{
			tmtProjectID:projectId,
			identifier:projectIdentifier
		}
	})
},
getProjectIssuesStatus: function(projectId) {
	$(".se-pre-con").show();
	return $http({
		method : 'POST',
		url : AppURL + '/api/getProjectIssuesStatus',
		data:{
			tmtProjectID:projectId,
		}
	})
}

     

} });//End Of File