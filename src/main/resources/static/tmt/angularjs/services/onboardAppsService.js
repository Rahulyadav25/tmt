var AppURL="/TMT"; 

demoApp.factory('onboardAppsService', function($rootScope, $http) {
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
return { 	
	saveApplicationDetails: function(AppObject) {
		return $http({
			method : 'POST',
			url : AppURL + '/api/saveApplication',
			data:AppObject
		})
}, saveOwnerDetails : function(ownerObject) {
	return $http({
		method : 'POST',
		url : AppURL + '/api/saveOwnerDetails',
		data:ownerObject
	})
}, viewSaveedApplications : function(savedBy) {
	return $http({
		method : 'POST',
		url : AppURL + '/api/getSavedApplication',
		params:{
			"savedBy":savedBy
		}
	})
}, saveAccessControlDetails : function(accessControlObj) {
	return $http({
		method : 'POST',
		url : AppURL + '/api/saveAccessControlDetails',
		data:accessControlObj
	})
}, saveAuthorizerDetails : function(accessControlObj) {
	return $http({
		method : 'POST',
		url : AppURL + '/api/saveAuthorizerDetails',
		data:accessControlObj
	})
},







}
});
