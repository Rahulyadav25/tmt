var AppURL="/TMT"; 

demoApp.factory('myProfileService', function($rootScope, $http) {
	
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
	
	return { 
		getMyProjectsData: function(userid, userRoleID) {
			$(".se-pre-con").show();
			return $http({
				method : 'POST',
				url : AppURL + '/api/getMyProjectsData',
				params:{
					"userid":userid,
					"userRoleID":userRoleID,
					
				},	
			})
		},
	
	}
});  