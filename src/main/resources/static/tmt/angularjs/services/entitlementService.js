var AppURL="/TMT"; 

demoApp.factory('entitlementService', function($rootScope, $http) {
	var token=localStorage.getItem('tokenID');
	$http.defaults.headers.common.Authorization = token;
	return { 	

	getShowProjectDetails: function(projectid) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getShowProjectDetails',
			params : {
				"projectid" : projectid
				
			}

		})
	},
	
	getadminViewEntitlement : function(userId,userRole,projectwiserole,username) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/getadminViewEntitlement',
			params : {
				"userId" : userId,
				"userRole":userRole,
				"projectwiserole":projectwiserole,
				"username":username
			}

		})
	},
	GiveAccessUserEntitlement : function(projectid,userid,projectwiserole,projectaccessid,userrole) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/giveAccessUserEntitlement',
			params : {
				"projectid" : projectid,
				"userid":userid,
				"projectwiserole":projectwiserole,
				"projectaccessid":projectaccessid,
				"userrole":userrole,
			}

		})
	},
	deleteGiveAccessUserEntitlement: function(projectid,userid,projectwiserole,userrole) {
		$(".se-pre-con").show();
		return $http({
			method : 'POST',
			url : AppURL + '/api/deleteGiveAccessUserEntitlement',
			params : {
				"projectid" : projectid,
				"userid":userid,
				"projectwiserole":projectwiserole,
				"userrole":userrole
			}

		})
	},
	
	}
})